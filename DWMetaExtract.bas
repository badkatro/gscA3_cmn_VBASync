Attribute VB_Name = "DWMetaExtract"
'Option Explicit
Private Declare Function OpenClipboard Lib "user32.dll" (ByVal hWnd As Long) As Long
Private Declare Function EmptyClipboard Lib "user32.dll" () As Long
Private Declare Function CloseClipboard Lib "user32.dll" () As Long
Private Declare Function IsClipboardFormatAvailable Lib "user32.dll" (ByVal wFormat As Long) As Long
Private Declare Function GetClipboardData Lib "user32.dll" (ByVal wFormat As Long) As Long
Private Declare Function SetClipboardData Lib "user32.dll" (ByVal wFormat As Long, ByVal hMem As Long) As Long
Private Declare Function GlobalAlloc Lib "kernel32.dll" (ByVal wFlags As Long, ByVal dwBytes As Long) As Long
Private Declare Function GlobalLock Lib "kernel32.dll" (ByVal hMem As Long) As Long
Private Declare Function GlobalUnlock Lib "kernel32.dll" (ByVal hMem As Long) As Long
Private Declare Function GlobalSize Lib "kernel32" (ByVal hMem As Long) As Long
Private Declare Function lstrcpy Lib "kernel32.dll" Alias "lstrcpyW" (ByVal lpString1 As Long, ByVal lpString2 As Long) As Long

Public presult() As String      ' keeps metadata results, the captions of buttons of context menus. Some are 2 big for caption prop, limited to 255 chars
Public ExtDocID As String       ' CreateMoretus, global to hold user supplied external doc ID, when auto extraction fails (no fields)


Sub Master_CreateMoretus()

Call CreateMoretus

' Take user to Print Preview to update footers
Application.CommandBars.ExecuteMso ("PrintPreviewAndPrint")

ActiveDocument.Save

End Sub


Public Sub SetClipboard(sUniText As String)
    Dim iStrPtr As Long
    Dim iLen As Long
    Dim iLock As Long
    Const GMEM_MOVEABLE As Long = &H2
    Const GMEM_ZEROINIT As Long = &H40
    Const CF_UNICODETEXT As Long = &HD
    OpenClipboard 0&
    EmptyClipboard
    iLen = LenB(sUniText) + 2&
    iStrPtr = GlobalAlloc(GMEM_MOVEABLE Or GMEM_ZEROINIT, iLen)
    iLock = GlobalLock(iStrPtr)
    lstrcpy iLock, StrPtr(sUniText)
    GlobalUnlock iStrPtr
    SetClipboardData CF_UNICODETEXT, iStrPtr
    CloseClipboard
End Sub

Public Function GetClipboard() As String
    Dim iStrPtr As Long
    Dim iLen As Long
    Dim iLock As Long
    Dim sUniText As String
    Const CF_UNICODETEXT As Long = 13&
    OpenClipboard 0&
    If IsClipboardFormatAvailable(CF_UNICODETEXT) Then
        iStrPtr = GetClipboardData(CF_UNICODETEXT)
        If iStrPtr Then
            iLock = GlobalLock(iStrPtr)
            iLen = GlobalSize(iStrPtr)
            sUniText = String$(iLen \ 2& - 1&, vbNullChar)
            lstrcpy StrPtr(sUniText), iLock
            GlobalUnlock iStrPtr
        End If
        GetClipboard = sUniText
    End If
    CloseClipboard
End Function

' NOT main entry point for Create Moretus suite, that one is Master_CreateMoretus.
' This is the main routine though, does almost everything (does not trigger footer refresh and does not save document)
Sub CreateMoretus()

If Documents.count < 2 Then
    StatusBar = "Please open 2 docs to process (original & target)"
    Exit Sub
End If

Dim tdoc As Document, tgDoc As Document, srcDoc As Document


For i = 1 To Documents.count
    
    If Is_GSC_Doc(Documents(i).Name) Then
        
        Set srcDoc = Documents(i)
        
'        Set tgDoc = Documents(2 ^ i Mod 3)  ' will produce 2 for 1 and 1 for 2 !
        
        Select Case i
            
            Case 1
                Set tgDoc = Documents(2)
                Exit For
                
            Case 2
                Set tgDoc = Documents(1)
                Exit For
                
            Case Else
                
                StatusBar = "Error"
                Exit Sub
                
        End Select
        
    End If
    
Next i

If srcDoc Is Nothing Then
    StatusBar = "Did NOT find a GSC Doc opened !"
    Exit Sub
End If

Call DW_CopyMetadata(tgDoc, srcDoc)

tgDoc.Save

' Add necessary properties to doc so as te be recognised by DW
Call Desguise_Doc_asDW(tgDoc)

' Works nicely
Call Inject_IntroPhrases_inNewMoretus(tgDoc, srcDoc)

Call FileSaveAs_GSC(tgDoc, srcDoc.Name)

'tgDoc.activate
'tgDoc.Save
'Call DWAutoFinalize

tgDoc.activate

Call DocuWrite.DWProcessDocument

PauseForSeconds (0.6)


End Sub



Sub DWPopup_MainButtonAdd()

' routine to be called to add custom entry in context menu "Inline Picture"

' Calls GovLists_SearchMain


Dim ctpopctl As CommandBarButton
Dim tpop As CommandBar
Set tpop = Application.CommandBars("Inline Picture")    ' DW Technical table is an inline picture

On Error GoTo glpErr

If tpop.Controls(1).Tag <> "DWMetaExt" Then
    
    Set ctpopctl = tpop.Controls.Add(Type:=msoControlButton, Before:=1, Temporary:=True)
    ctpopctl.Caption = "DW Meta Extract"
    ctpopctl.Tag = "DWMetaExt"
    
    
    With ctpopctl
        .Caption = "DW &Meta Extract"
        .FaceId = 6114
        .TooltipText = "Extract all Docuwrite Metadata fields into popu-up menu so as to choose and copy to clipboard"
        .OnAction = "DWMetaExtract.DWExtractMain"
    End With

End If

Exit Sub

glpErr:
    If Err.Number <> 0 Then
        Debug.Print "DWPopup_MainButtonAdd: Error " & Err.Number & " described: " & Err.Description
        Err.Clear
        Exit Sub
    End If

End Sub



Sub DWExtractMain()

Dim DWAllTBFields As String
DWAllTBFields = DW_GetDocuwrite_TB_FieldsValues(ActiveDocument, DW_General_Field_Names)

Dim DWAllTBFields_Arr As Variant
DWAllTBFields_Arr = Split(DWAllTBFields, "|")

Dim pDWAllTBFields_Arr As Variant
pDWAllTBFields_Arr = Prune_Array(DWAllTBFields_Arr)

' Also adding Commision document subject to meta array. Imperfect. Needs clean spurious formatting tags!
If IsNull(pDWAllTBFields_Arr) Then
    pDWAllTBFields_Arr = Split("", "")
    Call AddComDocSubject_toArray(ActiveDocument, pDWAllTBFields_Arr)
Else
    Call AddComDocSubject_toArray(ActiveDocument, pDWAllTBFields_Arr)
End If

Call CustPopup_CbxShow(pDWAllTBFields_Arr)

End Sub



Sub AddComDocSubject_toArray(TargetDocument As Document, ByRef TargetArray)

Dim LWTitle As String
LWTitle = Get_ComDocSubject(TargetDocument)

If UBound(TargetArray) = 0 Then
    If TargetArray(0) = "" Then
    
    Else
        ReDim Preserve TargetArray(UBound(TargetArray) + 1)
    End If
Else
    ReDim Preserve TargetArray(UBound(TargetArray) + 1)
End If

TargetArray(UBound(TargetArray)) = LWTitle

End Sub



Function Prune_Array(InputArray) As Variant
' Supplied a string array, it returns another one where all the empty elements have been removed

' sanity check no 1
If Not IsArray(InputArray) Then
    Prune_Array = Null
    Exit Function
End If

' and no 2
If gscrolib.IsNotDimensionedArr(InputArray) Then
    Prune_Array = Null
    Exit Function
End If


Dim strResults As String
' loop all input array elements, storing non-empty only
For i = 0 To UBound(InputArray)
    
    Dim clRes As String
    clRes = TrimNonAlphaNums(CStr(InputArray(i)))     ' use em cleaned, non-alpha nums don't normally interest us anyway : TODO - Maybe, to opt this one OUT?
    
    ' if not empty, add it to result string, using separator
    If Not clRes = "" Then
        strResults = IIf(strResults = "", clRes, strResults & "|" & clRes)
    End If
    
Next i

' and return results, if any
If strResults <> "" Then
    Prune_Array = Split(strResults, "|")
Else
    Prune_Array = Null
End If


End Function


Sub CustPopup_CbxShow(PartialResultsArr As Variant)
' version 0.4
' Purpose: Routine resets a custom commandbar already created (by the auto-load routine, when we load main commandbar for the program)
' or it creates it if not and adds a number of buttons coresponding to the elements in the array received as input parameter (results of a
' search in a repository)

Dim cpop As CommandBar, clabel As CommandBarButton
Dim combo As CommandBarButton, cc As CommandBarControl

Dim evb() As evCommandBarButton

' If commandbar with proper name has already been created, just set an object reference to it
For Each cpop In Application.CommandBars
    If cpop.Name = "CustomPop" Then
        Exit For
    End If
Next cpop
' However, create it if it does not exists (it's temporary, so deleted every Word restart)
If cpop Is Nothing Then
    Set cpop = CommandBars.Add("CustomPop", msoBarPopup, , True)
    cpop.Width = 400
End If


If IsArray(PartialResultsArr) Then
    If Not gscrolib.IsNotDimensionedArr(PartialResultsArr) Then
        presult = PartialResultsArr
    End If
End If

If Not gscrolib.IsNotDimensionedArr(presult) Then
    ' we eliminate all controls (buttons) from the custom pop-up menu, so as to
    ' insert the newly found results
    If cpop.Controls.count <> 0 Then
        For Each cc In cpop.Controls
            If cc.Tag <> "lblGLSearch" Then
                cc.Delete
            End If
        Next cc
    End If
    ' we check if we need to reinsert the label button (first button) or if it's already there
    If cpop.Controls.count = 0 Then
        Set clabel = cpop.Controls.Add(msoControlButton, , , , True)
        With clabel
            .Style = msoButtonWrapCaption
            .State = msoButtonUp
            .Tag = "lblGLSearch"
            .OnAction = "GovLists_Main.GoToCSect"
            .TooltipText = "Click to show original language entries"
        End With
    ElseIf cpop.Controls.count = 1 Then
        Set clabel = cpop.Controls(1)
        clabel.TooltipText = "Click to show original language entries"
        clabel.Tag = "lblGLSearch"
    End If
    
    ' we loop through the results of the search and add buttons and set captions for each on the custom popup menu
    For j = 0 To UBound(presult)
        ReDim Preserve evb(j)
        Set evb(j) = New evCommandBarButton
        Set evb(j).btI = cpop.Controls.Add(msoControlButton)
        
        With evb(j).btI
            .Width = 400
            '.Height = 30
            
            If Len(presult(j)) < 255 Then
                .Caption = presult(j)
                .Tag = j    ' retrieve later, from click event of button !
            Else
                .Caption = Left$(presult(j), 254) & "�"
                .Tag = j
            End If
            
            .Style = msoButtonWrapCaption
            .TooltipText = CtGLPartGroup
            If j = 0 Then .BeginGroup = True
        End With
    Next j
    
    ' we add caption to the label button, to announce if there's ONE or more results found
    clabel.Caption = "DW Metadata Extraction"
    
    
    If cpop.Controls.count > 6 Then
        Call SetMousePos_C(200, 150)
    Else
        Call SetMousePos_C(450, 150)
    End If
    cpop.ShowPopup
    
End If

End Sub


Sub GovLists_SearchMain()
' version 0.35

Dim trfind As Range, trrom As Range, ctDoc As String
Dim trfindRt As String, pfindres() As String, inpText As String
Dim k As Integer, foundOdoc As Boolean
Let k = -1

On Error GoTo glSErr

If Documents.count > 0 Then
    If Selection.Type = wdSelectionNormal Then
        inpText = Selection.Text
        
        If inpText <> " " And inpText <> vbCr And Len(inpText) > 3 Then
            ' transform a bit the selected string, so as to avoid certain error while finding the string
            inpText = Trim(Replace(Replace(inpText, Chr(160), " "), vbCr, " "))
            inpText = Replace(inpText, vbTab, " ")
            inpText = LCase(Replace(Replace(inpText, "   ", " "), "  ", " "))
            
            ' we find if repository is opened, as it should be (should it !?)
            On Error Resume Next
            If odoc.Name <> "GovLists_Docs.doc" Then
                If Err <> 0 Then Err.Clear
                For Each odoc In Documents
                    If odoc.Name = "GovLists_Docs.doc" Then
                        foundOdoc = True
                        Exit For
                    End If
                Next odoc
            Else
                foundOdoc = True
            End If
            
            If foundOdoc = True Then
                ' we only go forth if nice user has already chosen a bit of text of proper style and outlining level
                ' ("Participant" and "Body Text" - or level 10)
                If Selection.Range.Paragraphs(1).OutlineLevel = wdOutlineLevelBodyText And _
                    Selection.Range.Paragraphs(1).Style = "Participant" Then
                    'MsgBox "You selected " & vbCr & inpText    ' Testing purpose
                    CtGLPartGroup = GetCtGLPartsGroup_frTDS(Selection.Range)
                    
                    ' HERE, Please finish instruction HEREHERE
                    CtGLPartGroup = switch(CtGLPartGroup = "Belgia", "Belgium", CtGLPartGroup = "Republica Ceh" & ChrW(259), "Czech", CtGLPartGroup = "Danemarca", "Denmark", _
                    CtGLPartGroup = "Germania", "Germany", CtGLPartGroup = "Irlanda", "Ireland", CtGLPartGroup = "Grecia", "Greece", CtGLPartGroup = "Spania", "Spain", _
                    CtGLPartGroup = "Fran" & ChrW(539) & "a", "France", CtGLPartGroup = "Italia", "Italy", CtGLPartGroup = "Cipru", "Cyprus", CtGLPartGroup = "Letonia", "Latvia", _
                    CtGLPartGroup = "Lituania", "Lithuania", CtGLPartGroup = "Luxemburg", "Luxembourg", CtGLPartGroup = "Ungaria", "Hungary", CtGLPartGroup = ChrW(538) & ChrW(259) & "rile de Jos", "Netherlands", _
                    CtGLPartGroup = "Polonia", "Poland", CtGLPartGroup = "Portugalia", "Portugal", CtGLPartGroup = "Rom�nia", "Romania", CtGLPartGroup = "Slovacia", "Slovakia", _
                    CtGLPartGroup = "Finlanda", "Finland", CtGLPartGroup = "Suedia", "Sweden", CtGLPartGroup = "Regatul Unit", "United Kingdom")
                    
                    
                    If odoc.Bookmarks.Exists(CStr(Split(CtGLPartGroup, " ")(0))) Then
                        
                        Set trfind = odoc.Bookmarks(Split(CtGLPartGroup, " ")(0)).Range.Duplicate
                                
                        With trfind.Find
                            .ClearFormatting
                            .ClearAllFuzzyOptions
                            .Wrap = wdFindStop
                            .Forward = True
                            .MatchCase = False
                            .Text = LCase(inpText)
                            .Replacement.Text = ""
                            
                            If .Execute = False Then
                                MsgBox "Job searched for NOT found in repository, section of " & CtGLPartGroup, vbOKOnly + vbCritical, "Search failed"
                                Exit Sub
                            End If
                            
                        End With
                        
                        Do
                            With trfind.Find
                            
                                If .found = True Then
                                    k = k + 1
                                End If
                                
                                Set trrom = trfind.Duplicate
                                Set trrom = trrom.Next(wdCell, 1)
                                trrom.MoveEnd wdCharacter, -1
                                
                                ReDim Preserve pfindres(k)
                                pfindres(k) = trrom.Text
                                                                    
                                trfind.EndOf wdRow, wdExtend: trfind.Collapse (wdCollapseEnd): trfind.MoveStart wdCharacter, 1
                                ' Cant use, since redefining range also resets attached .Find object's properties, looping thus failing
                                If trfind.Start < odoc.Bookmarks(Split(CtGLPartGroup, " ")(0)).Range.End Then
                                    Set trfind = odoc.Range(trfind.Start, odoc.Bookmarks(Split(CtGLPartGroup, " ")(0)).Range.End)
                                Else
                                    Exit Do
                                End If
                                
                            End With

                        Loop While trfind.Find.Execute(LCase(inpText), False, , , , , True, wdFindStop, False, "", wdReplaceNone) = True
                        ' This next call will reset our "CustomPop" commandbar - a popup contextual menu actually and add
                        ' a number of buttons equal to our results
                        Call CustPopup_CbxShow(pfindres)
                        
                    Else
                        ' Why is this bookmark missing ? Is the repository corrupt (missing bookmarks it should have maybe?)
                        ' or is it the identification of the participant's group eroneous !? NEED TO CHECK !!!
                        '
                    End If
                    
                Else
                    MsgBox "Please select a participant's function before activating the search", vbOKOnly + vbCritical, _
                        "Selection not in a participant paragraph"
                    
                End If
                
            Else
                ' Should we open the repository here, if it's not already ?
                'MsgBox "Repository not set ! Please restart program from the gscMenu!"
                AhMsgBox_Show "Repository not set. Reopening local repository... Please retry search afterwards", , , 3
                ctDoc = ActiveDocument.Name
                Call gscrolib.PauseForSeconds(2)
                Documents.Open (LocalBasePath & "\GovLists_Docs.doc")
                
                Documents(ctDoc).activate
            End If
            
        End If
        
    Else
        MsgBox "No working without a selection, to act as input text to search." & vbCr & "Please select function you wish to verify and retry", _
            vbOKOnly vbCritical, "No selection error"
        Exit Sub
        
    End If
Else
End If

Exit Sub

glSErr:
    If Err.Number <> 0 Then
        Debug.Print "GovLists_SearchMain: Error " & Err.Number & " described: " & Err.Description
        Err.Clear
        Exit Sub
    End If

End Sub


Sub GoToCSect()

Dim dd As Document

If Application.CommandBars("CustomPop").Controls(1).Type = msoControlButton And _
    Application.CommandBars("CustomPop").Controls(1).Tag = "lblGLSearch" Then
        
    If Documents.count > 0 Then
        For Each dd In Documents
            If dd.Name = "GovLists_Docs.doc" Then
                Exit For
            End If
        Next dd
    End If
    
    Application.ScreenUpdating = False
        
    dd.activate
    Selection.GoTo wdGoToBookmark, , , Split(Application.CommandBars("CustomPop").Controls(1).Caption, " ")(0)
    Selection.Collapse
    
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    
End If

Set dd = Nothing
End Sub


Function Unicode_Process_Moretus_SecondPhrase() As String


Dim CoverPage_Text_Insert As String

Dim sFile As New SettingsFileClass

Dim savedSecondPhrase As String


If sFile.Exists Then
    
    If sFile.IsEmpty = FileNotEmpty Then
        
        savedSecondPhrase = sFile.getOption("CreateMoretusIntro2", "GSC_A3_Options")
        CoverPage_Text_Insert = RestoreUnicode_SpecialCharacters(savedSecondPhrase)
        
    Else    ' Settings file empty or not accessible
        CoverPage_Text_Insert = ""
    End If
    
Else    ' Settings file does not exist !
    CoverPage_Text_Insert = ""
End If


Unicode_Process_Moretus_SecondPhrase = CoverPage_Text_Insert

Set sFile = Nothing

End Function

Function Unicode_Process_Moretus_IntroPhrase() As String


Dim CoverPage_Text_Insert As String


Dim sFile As New SettingsFileClass

Dim savedFirstPhrase As String


If sFile.Exists Then
    
    If sFile.IsEmpty = FileNotEmpty Then
        
        savedFirstPhrase = sFile.getOption("CreateMoretusIntro1", "GSC_A3_Options")
        CoverPage_Text_Insert = RestoreUnicode_SpecialCharacters(savedFirstPhrase)
        
    Else    ' Settings file empty or not accessible
        CoverPage_Text_Insert = frmgscroopt.tbCreateMoretusIntro1.Text
    End If
    
Else    ' Settings file does not exist !
    CoverPage_Text_Insert = frmgscroopt.tbCreateMoretusIntro1.Text
End If


'CoverPage_Text_Insert = "\u206?n anex\u259?, se pune la dispozi\u539?ia delega\u539?iilor documentul "
'CoverPage_Text_Insert = RestoreUnicode_SpecialCharacters(CoverPage_Text_Insert)

Unicode_Process_Moretus_IntroPhrase = CoverPage_Text_Insert

Set sFile = Nothing

End Function


Function DW_Get_CommissionDoc(InputDocument As Document) As String

'

Const DW_Required_FieldName As String = "md_CommissionDocuments"


If Documents.count > 0 Then
    
    DW_Get_CommissionDoc = DW_Get_MetadataField(InputDocument, DW_Required_FieldName)
    
Else
    
    DW_Get_CommissionDoc = ""
    
End If

End Function

Function DW_Get_Subject(InputDocument As Document) As String


Const DW_Required_FieldName As String = "md_Subject"


If Documents.count > 0 Then

    DW_Get_Subject = DW_Get_MetadataField(InputDocument, DW_Required_FieldName)
    
Else
    
    DW_Get_Subject = ""
    
End If

End Function


Function DW_Get_Confidentiality(InputDocument As Document) As String


Const DW_Required_FieldName As String = "md_Distribution"


If Documents.count > 0 Then

    DW_Get_Confidentiality = DW_Get_MetadataField(InputDocument, DW_Required_FieldName)
    
Else
    
    DW_Get_Confidentiality = ""
    
End If


End Function


Function DW_Get_Initials(InputDocument As Document) As String

' md_Initials

Const DW_Required_FieldName As String = "md_Initials"


If Documents.count > 0 Then

    DW_Get_Initials = DW_Get_MetadataField(InputDocument, DW_Required_FieldName)
    
Else
    
    DW_Get_Initials = ""
    
End If



End Function


Function DW_Get_DocID(InputDocument As Document) As String


Dim dt
dt = DW_Get_MetadataField(InputDocument, "md_DocumentType")
        
Dim dn
dn = DW_Get_MetadataField(InputDocument, "md_DocumentNumber")

Dim dy
dy = DW_Get_MetadataField(InputDocument, "md_YearDocumentNumber")

Dim ds
ds = DW_Get_MetadataField(InputDocument, "md_Suffixes")


Dim tmpRes As String

If Not dt = "ST " Then
    tmpRes = dt
End If

tmpRes = tmpRes & dn & "/" & Right(dy, 2) & IIf(ds <> "", ds, "")

DW_Get_DocID = tmpRes


End Function


Function DW_Get_OriginalLanguages(InputDocument As Document) As String


Const DW_Required_FieldName As String = "md_OriginalLanguages"


If Documents.count > 0 Then

    DW_Get_OriginalLanguages = DW_Get_MetadataField(InputDocument, DW_Required_FieldName)
    
Else
    
    DW_Get_OriginalLanguages = ""
    
End If


End Function


Function DW_Get_MetadataField(InputDocument As Document, Required_DWMeta_FieldName As String) As String


Dim DW_Required_FieldName As String


DW_Required_FieldName = Required_DWMeta_FieldName


Dim DW_RequiredField_Value As String


DW_RequiredField_Value = DW_GetDocuwrite_TB_FieldsValues(InputDocument, DW_Required_FieldName)


DW_Get_MetadataField = DW_RequiredField_Value


End Function


Function DW_GetDocuwrite_TB_FieldsValues(InputDocument As Document, WhichFields As String) As String
' Returns string bundling requested Technical Table fields from a DW document

' Conventions:
' Input string:
' Use pipe character (|) to concatenate multiple desired fields values into one string parameter
'
' Return string:
' Using | (pipe) to separate field name from value, Paragraph mark (Chr(13)) to
' separate the name-value pairs

Dim DW_NodesTemp As IXMLDOMNodeList
Set DW_NodesTemp = DW_GetDocuwrite_Metadata_Nodes(InputDocument)

' Necessary safeguard
If DW_NodesTemp Is Nothing Then
    DW_GetDocuwrite_TB_FieldsValues = ""
    Exit Function
End If


Dim dictTry As New Scripting.Dictionary
    'Set dictTry = New Dictionary
    
Dim dKey As String
Dim dVal As String

' Did user ask for one field's value or more? (concatenated with pipe)
If InStr(1, WhichFields, "|") > 0 Then
    
    For i = 0 To UBound(Split(WhichFields, "|"))
        dKey = Split(WhichFields, "|")(i)
        dVal = DW_GetDocuwrite_TB_FieldValue(DW_NodesTemp, CStr(Split(WhichFields, "|")(i)))
        
        dictTry.Add dKey, dVal
        
    Next i
    

    'Dim Desired_TBF_Arr(0, 0) As String
    'Desired_TBF_Arr(0) = Split(WhichFields, "|")
    'ReDim Preserve Desired_TBF_Arr(3)
    
Else
    
    dKey = WhichFields
    dVal = DW_GetDocuwrite_TB_FieldValue(DW_NodesTemp, WhichFields)
    
    dictTry.Add dKey, dVal
    
End If


Dim dictStringAll As String

Dim typeNumberYearSuffix As String

If dictTry.count > 0 Then
    For i = 1 To dictTry.count
        
        ' Showing doc Type, Number, Year and Suffix separately to the user is NOT useful, we will combine these, omitting "ST"
        If Split(WhichFields, "|")(i - 1) = "md_DocumentType" Then
            
            ' Omit ST as type, sub-understood
            If dictTry.Item(Split(WhichFields, "|")(i - 1)) <> "ST" Then
                typeNumberYearSuffix = dictTry.Item(Split(WhichFields, "|")(i - 1))
            End If
        
        ElseIf Split(WhichFields, "|")(i - 1) = "md_DocumentNumber" Then
            typeNumberYearSuffix = IIf(typeNumberYearSuffix = "", dictTry.Item(Split(WhichFields, "|")(i - 1)), _
                typeNumberYearSuffix & " " & dictTry.Item(Split(WhichFields, "|")(i - 1)))
        
        ElseIf Split(WhichFields, "|")(i - 1) = "md_YearDocumentNumber" Then
            
            Dim dy As String    ' doc year
            
            dy = Right(dictTry.Item(Split(WhichFields, "|")(i - 1)), 2)
            
            typeNumberYearSuffix = IIf(typeNumberYearSuffix = "", dy, _
                typeNumberYearSuffix & "/" & dy)
                
        ElseIf Split(WhichFields, "|")(i - 1) = "md_Suffixes" Then
            
            ' Suffixes may be empty too
            If dictTry.Item(Split(WhichFields, "|")(i - 1)) <> "" Then
            
                typeNumberYearSuffix = IIf(typeNumberYearSuffix = "", dictTry.Item(Split(WhichFields, "|")(i - 1)), _
                    typeNumberYearSuffix & " " & dictTry.Item(Split(WhichFields, "|")(i - 1)))
                    
            End If
                
            ' Add concatenated type, number, year, suffix
            dictStringAll = IIf(dictStringAll = "", typeNumberYearSuffix, dictStringAll & "|" & typeNumberYearSuffix)
        
        ElseIf Split(WhichFields, "|")(i - 1) = "md_DateOfReceipt" Then
            
            Dim ctDate As String     ' correct date - changed to format dd/mm/yyyy
            
            ctDate = Format(dictTry.Item(Split(WhichFields, "|")(i - 1)), "dd-mm-yyyy")
            
            dictStringAll = IIf(dictStringAll = "", ctDate, dictStringAll & "|" & ctDate)
                
        Else    ' we passed doc type, num ber, year and suffixes... Normal processing
            dictStringAll = IIf(dictStringAll = "", dictTry.Item(Split(WhichFields, "|")(i - 1)), dictStringAll & "|" & dictTry.Item(Split(WhichFields, "|")(i - 1)))
        End If
        
        'dictStringAll = IIf(dictStringAll = "", dictTry.Item(Split(WhichFields, "|")(i - 1)), dictStringAll & "|" & dictTry.Item(Split(WhichFields, "|")(i - 1)))
        
    Next i
    
    DW_GetDocuwrite_TB_FieldsValues = dictStringAll
    
Else
End If

End Function


Function DW_GetDocuwrite_TB_FieldValue(DWNodesCollection As IXMLDOMNodeList, WhichField As String) As String
' Function retrieves ONE value for ONE field whose key it receives as parameter
' It concatenates the vals with vbChar if the desired field is a "textList" type

Dim DWMetaNode As IXMLDOMNode

'Dim DWSubjectCodes As String

For Each DWMetaNode In DWNodesCollection
    
    If DWMetaNode.Attributes.getNamedItem("key").NodeValue = WhichField Then
        
        If DWMetaNode.ChildNodes.length > 0 Then
        
            If DWMetaNode.ChildNodes(0).BaseName = "text" Then
                
                DW_GetDocuwrite_TB_FieldValue = DWMetaNode.ChildNodes(0).Text
                
            ElseIf DWMetaNode.ChildNodes(0).BaseName = "textlist" Then    ' textlist and other strange cases...
                
                Dim textlist As IXMLDOMNodeList
                Set textlist = DWMetaNode.ChildNodes(0).ChildNodes
                
                Dim sjtext As IXMLDOMNode
                
                Dim DWTextlist_StringAll As String
                
                For Each sjtext In textlist
                    DWTextlist_StringAll = IIf(DWTextlist_StringAll = "", sjtext.Text, DWTextlist_StringAll & vbCr & sjtext.Text)
                Next sjtext
                
                DW_GetDocuwrite_TB_FieldValue = DWTextlist_StringAll
                
                Exit For
            
            ElseIf DWMetaNode.ChildNodes(0).BaseName = "xaml" Then      ' for Subject field
                ' ? Do we need the subject ?
                
                Dim textArr
                textArr = Split(DWMetaNode.ChildNodes(0).Text, ">")
                
                ' Remove all xml tags from separated parts, leaving only text itself
                For i = 0 To UBound(textArr)
                    If textArr(i) Like "<*" Then
                        textArr(i) = ""
                    ElseIf Not textArr(i) Like "" And InStr(textArr(i), "<") > 0 Then
                        textArr(i) = Left(textArr(i), InStr(textArr(i), "<") - 1)
                    End If
                Next i
                
                Dim txtSubjAll As String
                For i = 0 To UBound(textArr)
                    If Not textArr(i) Like "" Then
                        txtSubjAll = IIf(txtSubjAll = "", Trim(textArr(i)), txtSubjAll & " " & Trim(textArr(i)))
                    End If
                Next i
                
                'Debug.Print txtSubjAll
                'Selection.InsertAfter (txtSubjAll)
                
                DW_GetDocuwrite_TB_FieldValue = txtSubjAll
            
            ' for Originator and Destinator, diff type of xml node
            ElseIf DWMetaNode.ChildNodes(0).BaseName = "basicdatatype" Then
                
                Dim bdElem As IXMLDOMElement
                
                Set bdElem = DWMetaNode.ChildNodes(0).ChildNodes(0)
                
                ' TO DO: Determine if the "getAttribute" is ever used... where did we get this from?
                If bdElem.Attributes.length > 0 Then
                    If Not IsNull(bdElem.getAttribute("text")) Then
                        DW_GetDocuwrite_TB_FieldValue = bdElem.getAttribute("text")
                    Else
                        DW_GetDocuwrite_TB_FieldValue = ""
                    End If
                    
                Else
                    DW_GetDocuwrite_TB_FieldValue = bdElem.Text
                End If
            
            ElseIf DWMetaNode.ChildNodes(0).BaseName = "basicdatatypelist" Then
            
                'Set bdElem = DWMetaNode.ChildNodes(0).ChildNodes(0)
                
                Dim orLangNodes As IXMLDOMNodeList
                
                Set orLangNodes = DWMetaNode.ChildNodes(0).ChildNodes
                
                DW_GetDocuwrite_TB_FieldValue = DW_Get_BasicDataType_ListValues(orLangNodes)
                
            End If
            
        End If
            
        Exit For
        
    End If
    
Next DWMetaNode


End Function


Function DW_Get_BasicDataType_ListValues(xmlDomNodesList As IXMLDOMNodeList) As String

Dim ctNode As IXMLDOMElement

Dim tmpRes As String


For Each ctNode In xmlDomNodesList
        
    If ctNode.Attributes.length > 0 Then
        tmpRes = IIf(tmpRes = "", ctNode.getAttribute("text"), tmpRes & "," & ctNode.getAttribute("text"))
    Else
        tmpRes = IIf(tmpRes = "", ctNode.Text, tmpRes & ctNode.Text)
    End If
        
Next ctNode

DW_Get_BasicDataType_ListValues = tmpRes

End Function


Function DW_GetDocuwrite_Metadata_Nodes(InputDocument As Document) As IXMLDOMNodeList

' To get all DW metadata as a collection of nodes
' Function returns NOTHING in case the doc is not Docuwrite or some other error so that
' the DW metadata is not found!
' (Check with "Is Nothing" against that before using it !)

Dim dwMeta As MSXML2.DOMDocument
Set dwMeta = New DOMDocument

' Running on empty application? Fail and exit
If Documents.count = 0 Then
    'Set DW_GetDocuwriteMetadata = Nothing
    Exit Function
End If

' If no such propert, fail and exit
If CtDocument_HasVariableNamed(InputDocument, "DocuWriteMetaData") Then
    
    'Documents.Add.Content.InsertAfter (ActiveDocument.Variables("DocuwriteMetaData").Value)

    dwMeta.LoadXML (InputDocument.Variables("DocuWriteMetaData").Value)
    'Documents.Add.Content.InsertAfter (ActiveDocument.Variables("DocuwriteMetaData").Value)

Else
    'DW_SubjectCodes_ToClipboard = 3
    Exit Function
End If

'Dim DWMetadataNodes As IXMLDOMNodeList
Set DW_GetDocuwrite_Metadata_Nodes = dwMeta.SelectNodes("//metadataset/metadata")

End Function


Function DW_GetDocuwrite_MetaNodes_S(SourceDWMeta As String) As IXMLDOMNodeList

' To get all DW metadata as a collection of nodes
' Function returns NOTHING in case the doc is not Docuwrite or some other error so that
' the DW metadata is not found!
' (Check with "Is Nothing" against that before using it !)

Dim dwMeta As MSXML2.DOMDocument
Set dwMeta = New DOMDocument

dwMeta.LoadXML (SourceDWMeta)

Set DW_GetDocuwrite_MetaNodes_S = dwMeta.SelectNodes("//metadataset/metadata")


End Function


Function DW_GetDocuwrite_Metadata(InputDocument As Document) As String
' To get all DW metadata as a collection of nodes
' Function returns NOTHING in case the doc is not Docuwrite or some other error so that
' the DW metadata is not found!
' (Check with "Is Nothing" against that before using it !)


' Running on empty application? Fail and exit
If Documents.count = 0 Then
    DW_GetDocuwrite_Metadata = ""
    Exit Function
End If

' If no such propert, fail and exit
If CtDocument_HasVariableNamed(InputDocument, "DocuWriteMetaData") Then
    'Documents.Add.Content.InsertAfter (ActiveDocument.Variables("DocuwriteMetaData").Value)
    'DwMeta.LoadXML (InputDocument.Variables("DocuwriteMetaData").Value)
    'Documents.Add.Content.InsertAfter (ActiveDocument.Variables("DocuwriteMetaData").Value)
    
    DW_GetDocuwrite_Metadata = InputDocument.Variables("DocuwriteMetaData").Value
    
Else
    'DW_SubjectCodes_ToClipboard = 3
    DW_GetDocuwrite_Metadata = ""
    Exit Function
End If

'Dim DWMetadataNodes As IXMLDOMNodeList
'Set DW_GetDocuwrite_Metadata_Nodes = DwMeta.SelectNodes("//metadataset/metadata")


End Function


Function DW_GetDocuwrite_Metadata_DOM(InputDocument As Document) As MSXML2.DOMDocument

Dim dwMeta As MSXML2.DOMDocument
Set dwMeta = New DOMDocument

Dim docDWMetaS As String
docDWMetaS = DW_GetDocuwrite_Metadata(InputDocument)


If docDWMetaS <> "" Then

    dwMeta.LoadXML (DW_GetDocuwrite_Metadata(InputDocument))
        
    Set DW_GetDocuwrite_Metadata_DOM = dwMeta
        
Else    ' could not extract DW Meta. Function return will be nothing, prob
End If


End Function



Function DW_GetDocuwrite_Metadata_Node(InputDocument As Document, SoughtNodeName As String) As IXMLDOMNode

Dim dwMeta As MSXML2.DOMDocument

Set dwMeta = DW_GetDocuwrite_Metadata_DOM(InputDocument)

Dim tmpNode As IXMLDOMNode

Set tmpNode = dwMeta.SelectSingleNode(SoughtNodeName)

End Function



Function DW_SubjectCodes_ToClipboard() As Integer

' version 0.3   Initial commit
'               Workin flawlessly for months now... simple routine!

' Return codes:
' 0 for success, 1 for empty DW codes, 2 for no document opened, 3 for no such property in ct doc


Dim dwMeta As MSXML2.DOMDocument
Set dwMeta = New DOMDocument

' Running on empty application? Fail and exit
If Documents.count = 0 Then
    DW_SubjectCodes_ToClipboard = 2
    Exit Function
End If

' If no such propert, fail and exit
If CtDocument_HasVariableNamed(ActiveDocument, "DocuWriteMetaData") Then
    
    'Documents.Add.Content.InsertAfter (ActiveDocument.Variables("DocuwriteMetaData").Value)

    dwMeta.LoadXML (ActiveDocument.Variables("DocuwriteMetaData").Value)
    'Documents.Add.Content.InsertAfter (ActiveDocument.Variables("DocuwriteMetaData").Value)

Else
    DW_SubjectCodes_ToClipboard = 3
    Exit Function
End If

Dim dwMetadataNodes As IXMLDOMNodeList
Set dwMetadataNodes = dwMeta.SelectNodes("//metadataset/metadata")

Dim DWMetaNode As IXMLDOMNode

Dim DWSubjectCodes As String

For Each DWMetaNode In dwMetadataNodes
    
    If DWMetaNode.Attributes.getNamedItem("key").NodeValue = "md_SubjectCodes" Then
        
        Dim textlist As IXMLDOMNodeList
        Set textlist = DWMetaNode.ChildNodes(0).ChildNodes
        
        Dim sjtext As IXMLDOMNode
        
        For Each sjtext In textlist
            DWSubjectCodes = IIf(DWSubjectCodes = "", sjtext.Text, DWSubjectCodes & "|" & sjtext.Text)
        Next sjtext
        
        Exit For
    End If
    
Next DWMetaNode

' Is the final string containing the codes empty or not?
If DWSubjectCodes <> "" Then    ' not empty

    Dim vbcrSubjectCodes As String
    vbcrSubjectCodes = Replace(DWSubjectCodes, "|", vbCr)
    
    Set_Clipboard_TextContents (vbcrSubjectCodes)
    DW_SubjectCodes_ToClipboard = 0     ' Success
    
    StatusBar = "Subject codes: " & vbcrSubjectCodes & "SUCCESS! Codes copied to clipboard!"
Else    ' Empty
    DW_SubjectCodes_ToClipboard = 1     ' Error, DW codes came up empty
    StatusBar = "Document " & ActiveDocument.Name & " has NO DW subject codes: " & "FAILURE Copying codes"
End If

End Function

'*********************************************************************************************************************************************


Sub ComDocSubject_ToClipboard()

Dim tmpStr As String

If Documents.count > 0 Then

    tmpStr = Get_ComDocSubject(ActiveDocument)
    
    Set_Clipboard_TextContents (tmpStr)
    
    StatusBar = "ComDoc Subject copied to clipboard!"
    
Else
    StatusBar = "NO Doc opened, exiting..."
End If


End Sub


Function Get_ComDocSubject(TargetDocument As Document) As String

' version 0.22 - Adapted from old "Copie_CoteMat_Subject" (pre-office 2010 migration code)

' Puts together supplied documents' title using LegisWrite variables and returns it. Useful to correct deficiencies in DW
' lately and to attempt automatic ComDoc creation in near future.

' 0.2 - removing formatting tags between "<>"
' 0.21 - less checking if doc Commission, criteria... too narrow!
' 0.22 - remove manual line break char (change to space)

Dim k As Integer
Let k = 0

Dim odoc As Document
Dim cmR As Range, cmRC As Range
Dim Its_CommDoc As Boolean

Dim tGather(9) As String
Dim lSep As String

    
For Each adv In ActiveDocument.Variables
    If adv.Name = "LW_DocType" And adv.Value <> "<UNUSED>" Then
        If adv.Value = "COM" Or adv.Value = "SEC" Or _
            adv.Value = "D" Or adv.Value = "DEC" Or _
            adv.Value = "C" Or adv.Value = "NORMAL" Or _
            adv.Value = "REP" Then
            Its_CommDoc = True
        End If
        Exit For
    End If
Next adv
                

If Its_CommDoc = True Then

StillExtract:
    For Each adv In ActiveDocument.Variables
        If adv.Name = "LW_STATUT.CP" And (adv.Value <> "" And adv.Value <> "<UNUSED>") Then
            tGather(1) = adv.Value
        ElseIf adv.Name = "LW_TYPE.DOC.CP" And (adv.Value <> "" And adv.Value <> "<UNUSED>") Then
            tGather(2) = adv.Value
        ElseIf adv.Name = "LW_TYPE.DOC.CP.USERTEXT" And (adv.Value <> "" And adv.Value <> "<UNUSED>") Then
            tGather(3) = adv.Value
        ElseIf adv.Name = "LW_DATE.ADOPT.CP" And (adv.Value <> "" And adv.Value <> "<UNUSED>") Then
            tGather(4) = adv.Value
        ElseIf adv.Name = "LW_TITRE.OBJ.CP" And (adv.Value <> "" And adv.Value <> "<UNUSED>") Then
            tGather(5) = adv.Value
        ElseIf adv.Name = "LW_ACCOMPAGNANT.CP" And (adv.Value <> "" And adv.Value <> "<UNUSED>") Then
            tGather(6) = adv.Value
        ElseIf adv.Name = "LW_TYPEACTEPRINCIPAL.CP" And (adv.Value <> "" And adv.Value <> "<UNUSED>") Then
            tGather(7) = adv.Value
        ElseIf adv.Name = "LW_OBJETACTEPRINCIPAL.CP" And (adv.Value <> "" And adv.Value <> "<UNUSED>") Then
            tGather(8) = adv.Value
        ElseIf adv.Name = "LW_SOUS.TITRE.OBJ.CP" And (adv.Value <> "" And adv.Value <> "<UNUSED>") Then
            tGather(9) = adv.Value
        End If
    Next adv
                            
    Dim tGatherTxt As String
                            
    For l = 1 To UBound(tGather)
        If tGather(l) <> "" Then
            tGatherTxt = tGatherTxt & " " & tGather(l) & " "
        End If
    Next l
    
    tGatherTxt = Replace(Trim(tGatherTxt), Chr(11), " ")    ' manual line break causes parse error in xml parsing
    
    tGatherTxt = RestoreUnicode_SpecialCharacters(tGatherTxt)
    
    tGatherTxt = regexReplace(tGatherTxt, "<[^<>]+>", "")   ' removing formatting tags
    
    Get_ComDocSubject = tGatherTxt

Else
    
    If MsgBox("This document does NOT appear to be a Commisison doc. Continue?", vbOKCancel) = vbOK Then
        GoTo StillExtract
    End If
    Get_ComDocSubject = "Document NOT Commission!"

End If
        

End Function


Function Print_UnicodeCodes_ofSelection()

For i = 1 To Selection.Characters.count
    Debug.Print AscW(Mid(Selection.Text, i, 1))
Next i

End Function


Function UnicodeEscape(InputText As String) As String


Dim tmpStr As String

tmpStr = InputText

' all chars check, convert to escape sequence in unicode char

Dim i As Integer
i = 1

Do While i < Len(tmpStr)
    
    Dim diff As Integer
    
    If AscW(Mid(tmpStr, i, 1)) > 127 Or AscW(Mid(tmpStr, i, 1)) < 0 Then
        
        If Len(CStr(AscW(Mid(tmpStr, i, 1)))) = 3 Then
            ' skip over newly added chars
            diff = 6
        ElseIf Len(CStr(AscW(Mid(tmpStr, i, 1)))) = 4 Then
            diff = 7
        Else
            diff = 1
        End If
        
        tmpStr = Replace(tmpStr, Mid(tmpStr, i, 1), "\u" & AscW(Mid(tmpStr, i, 1)) & "?")
    Else ' just move on, no Unicode
        diff = 1
    End If
    
    i = i + diff
    
    If i > 15000 Then Debug.Print "UnicodeEscape: Looping out of control": Exit Do
    
Loop

UnicodeEscape = tmpStr

End Function


Function RestoreUnicode_SpecialCharacters(InputText As String) As String


Dim regexp As VBScript_RegExp_55.regexp
Set regexp = New VBScript_RegExp_55.regexp

regexp.IgnoreCase = True
regexp.Pattern = "(\\u)([0-9]{3,}?)(\?)"
regexp.Global = True


Dim tmpResult As String
tmpResult = InputText

If regexp.test(InputText) Then

    Dim matS As VBScript_RegExp_55.MatchCollection
    Set matS = regexp.Execute(InputText)
    
    If matS.count > 1 Then
        
        ' more than one match in string, replace all
        For i = 0 To matS.count - 1
            
            If InStr(1, tmpResult, matS(i)) > 0 Then
                tmpResult = Replace(tmpResult, matS(i).Value, ChrW(CLng(Mid(matS(i).Value, 3, matS(i).length - 3))))
            End If
            
        Next i
        
        
    Else     ' one result only
        
        tmpResult = Replace(tmpResult, matS(0).Value, ChrW(Mid(matS(0).Value, 3, matS(0).length - 3)))
        
    End If
    
End If

tmpResult = Replace(tmpResult, ChrW(11) & ChrW(11) & ChrW(11), ChrW(11))
tmpResult = Replace(tmpResult, ChrW(11) & ChrW(11), ChrW(11))
tmpResult = Replace(tmpResult, "  ", " ")

RestoreUnicode_SpecialCharacters = tmpResult

End Function


' Adds a new first page to document and injects 2 predefined phrases into it, to form the basis for a Cover page document
Sub Inject_IntroPhrases_inNewMoretus(TargetDocument As Document, SourceDocument As Document)
' v 0.2  - Fixing DEC Moretuses case, no field used. Probably fixing any Moretus where fields were not used to put Com Doc ID on cover page!
'        - Adding extraneous forced font size fixing on the sentences introduced, there might be surprises otherwisely!
' v 0.21 - Force adding font and alignment for the two inserted phrases, sometimes we get documents with wrong "Normal" style even! (quite a lot actually)

Dim sDoc As Document
Set sDoc = SourceDocument

Dim fpRange As Range
Set fpRange = get_FirstPageRange(sDoc)

Dim comDocID As Field
Set comDocID = get_Moretus_DocID_Field(sDoc)


' Original Moretus is a bit broken, no fields used on cover page, just text!
If comDocID Is Nothing Then
    Dim comDocIDText As String
    
    comDocIDText = DW_Get_MetadataField(sDoc, "md_CommissionDocuments")
    
    If comDocIDText = "" Then
        ' TODO: Could check DW Meta fr this info, there are some 3 pieces of it which contain it normally !
        comDocIDText = InputBox("Could not extract external doc ID from original, please input it", "Com Doc ID missing")
    End If
End If

    
    Dim tmpFirstPhrase As String
    tmpFirstPhrase = Unicode_Process_Moretus_IntroPhrase & " "
    
    
    Dim tgRange As Range
    Set tgRange = TargetDocument.Paragraphs(1).Range
    
    tgRange.Collapse (wdCollapseStart)
    tgRange.InsertBefore (vbCr): tgRange.MoveStart wdParagraph, 1
    'tgRange.Collapse (wdCollapseStart)
    tgRange.InsertBreak (wdSectionBreakNextPage)
        
    'Insert phrases proper
    
    Set tgRange = TargetDocument.Paragraphs(1).Range
    tgRange.Collapse (wdCollapseStart)
    
    ' use field if retrieved, text otherwise (for Com Doc ID)
    If comDocID Is Nothing Then
        tgRange.InsertAfter (comDocIDText)
    Else
        comDocID.Copy: tgRange.Paste
    End If
    
    tgRange.Collapse (wdCollapseStart)
    
    
    Dim tmpSecondPhrase As String
    tmpSecondPhrase = Unicode_Process_Moretus_SecondPhrase & ": "
    
    tgRange.InsertBefore (tmpSecondPhrase)
    tgRange.InsertBefore (vbCr & vbCr)
    
    
    tgRange.Collapse (wdCollapseStart)
    tgRange.InsertBefore (".")      'sentence needs a fullstop !
    tgRange.Collapse (wdCollapseStart)
    
    
        ' use field if retrieved, text otherwise (for Com Doc ID)
    If comDocID Is Nothing Then
        tgRange.InsertAfter (comDocIDText)
    Else
        comDocID.Copy: tgRange.Paste
    End If
    
    tgRange.InsertBefore (tmpFirstPhrase)
    
    ' First line paragraph, intro phrase
    With TargetDocument.Paragraphs(1)
        '.Style = "Normal"
        .Format.Space15
        .Format.SpaceBefore = 24
        .Format.SpaceAfter = 6
        .Range.Font.Name = "Times New Roman"
        .Range.Font.Size = 12
    End With
        
    ' Final line paragraph
    With TargetDocument.Paragraphs(2)
        '.Style = "Normal"
        .Format.Space15
        .Format.SpaceBefore = 18
        .Format.SpaceAfter = 6
    End With
    
    Call Format_FinalLineParagraph(TargetDocument.Paragraphs(2))
    
    ' p.j. (Annex) paragraph
    With TargetDocument.Paragraphs(3)
        '.Style = "Normal"
        .Format.LineSpacingRule = wdLineSpaceMultiple
        .Format.LineSpacing = LinesToPoints(1.15)
        .Format.SpaceBefore = 24
        .Format.SpaceAfter = 6
        .Range.Font.Name = "Times New Roman"
        .Range.Font.Size = 12
    End With



End Sub




Sub Desguise_Doc_asDW(TargetDocument As Document)
' Routine inserts the required 2 custom document properties and 1 variable,
' without which a document is not recognised as DW document


If Not CtDocument_HasPropertyNamed(TargetDocument, "Created using") Then
    
    Dim docCustomProps As DocumentProperties
    
    Set docCustomProps = TargetDocument.CustomDocumentProperties
    
    docCustomProps.Add "Created using", False, msoPropertyTypeString, "DocuWrite 4.1.10, Build 20171130"
    
Else ' Already has it
    
    Dim tempProp As DocumentProperty
    
    Set docCustomProps = TargetDocument.CustomDocumentProperties
    
    Set tempProp = docCustomProps("Created using")
    
    tempProp.Value = "DocuWrite 4.1.25, Build 20180730"
    
End If


If Not CtDocument_HasPropertyNamed(TargetDocument, "Last edited using") Then
    
    Set docCustomProps = TargetDocument.CustomDocumentProperties
    
    docCustomProps.Add "Last edited using", False, msoPropertyTypeString, "DocuWrite 4.1.25, Build 20180730"
    
Else ' Already has it
    
    
    Set docCustomProps = TargetDocument.CustomDocumentProperties
    
    Set tempProp = docCustomProps("Last edited using")
    
    tempProp.Value = "DocuWrite 4.1.25, Build 20180730"
    
End If


If Not CtDocument_HasVariableNamed(TargetDocument, "Council") Then
    
    TargetDocument.Variables.Add "Council", "true"
    
Else
    
    TargetDocument.Variables("Council").Value = "true"
    
End If



End Sub

Sub Desguise_CtDoc_DW()

Call Desguise_Doc_asDW(ActiveDocument)

End Sub


Sub DW_Inject_Initials()

If Documents.count = 0 Then StatusBar = "Inject initials: NO Document, Aborting...": Exit Sub

Dim tg As Document
Set tg = ActiveDocument

Dim users As String
users = InputBox("Please input users to inject (ie �aa/BB/cc�)", "DW Inject initials")

If Not users = "" Then
    Call DW_Inject_UserInitials(tg, users)
    StatusBar = "User initials inserted (" & users & ")"
Else
    StatusBar = "Inject initials: Aborted"
End If

Application.CommandBars.ExecuteMso ("PrintPreviewAndPrint")


End Sub


Sub DW_Inject_UserInitials(TargetDocument As Document, Optional NewUsers)
'/* Optional parameter NewUsers may specify user initals string to insert.
'* Missing parameter will have the effect of inserting current Word user initials, using Application.UserInitials (found in File-Options in MS Word) */

If Documents.count = 0 Then
    StatusBar = "NO Doc, Exiting.."
    Exit Sub
End If

If HasDWMeta(TargetDocument) Then
    
    Dim ctDWMeta As String
    ctDWMeta = DW_GetDocuwrite_Metadata(TargetDocument)
    
    If Not IsMissing(NewUsers) Then
        
        Dim newDWMeta As String
        
        newDWMeta = DW_Inject_DWMetaField(ctDWMeta, "md_Initials", CStr(NewUsers))   ' FOR TEST
        
        If Not newDWMeta = "" Then
            Call DW_SetNewMetadata(TargetDocument, newDWMeta)
        Else
            StatusBar = "Inject users: Got empty meta! Aborting..."
        End If
        
    Else
        Call DW_Inject_DWMetaField(ctDWMeta, "md_Initials", Application.UserInitials)   ' FOR TEST
    End If
    
End If

TargetDocument.Save

End Sub

Function is_DW_Doc(documentID As String) As Boolean

If Documents.count = 0 Then is_DW_Doc = False: Exit Function

If Found_Opened_Document(documentID) Then
    
    Dim tdoc As Document
    Set tdoc = Documents(documentID)
    
    If HasDWMeta(tdoc) Then
        is_DW_Doc = True
    Else
        is_DW_Doc = False
    End If
    
Else
    is_DW_Doc = False
End If

End Function

Function DW_Inject_DWMetaField(SourceDWMeta As String, NewFieldName As String, NewFieldValue As String) As String
' Returns modified metadata collection as string


Dim targetMetaDataField As IXMLDOMNode


Dim dwMetaDOM As New MSXML2.DOMDocument
dwMetaDOM.LoadXML (SourceDWMeta)

Dim dwDocLanguagesNode As IXMLDOMNode

Dim targetMetaDataFieldE As IXMLDOMElement
Set targetMetaDataFieldE = dwMetaDOM.SelectSingleNode("//*[@key='" & NewFieldName & "']")

'Set targetMetaDataField = DW_Get_MetadataNode(ctMetaNodes, NewFieldName)
'Set targetMetaDataField = ctNodesSelection.Item.SelectSingleNode("md_DocumentLanguages")
'Set targetMetaDataFieldE = dwMetaDOM.SelectSingleNode("md_DocumentLanguages")

If Not targetMetaDataFieldE Is Nothing Then
    
    If targetMetaDataFieldE.ChildNodes(0).BaseName = "xaml" Then
        
        targetMetaDataFieldE.ChildNodes(0).nodeTypedValue = ReplaceTextInXaml(targetMetaDataFieldE.ChildNodes(0).nodeTypedValue, NewFieldValue)
        targetMetaDataFieldE.ChildNodes(0).Attributes(0).nodeTypedValue = NewFieldValue     ' 'key' item
        
    ElseIf targetMetaDataFieldE.ChildNodes(0).BaseName = "basicdatatypelist" Then
        
    ' The below only works for specific kind of DW meta, type "BasicDatatypeList" with text as basic data type. So, a list of texts
    ' Need diversify to the other types of DW Meta
        
    'ctNodesSelection.GetProperty ("md_DocumentLanguages")
    'targetMetaDataFieldE.childNodes(0).
        
        targetMetaDataFieldE.ChildNodes(0).ChildNodes(0).Attributes(0).NodeValue = NewFieldValue     ' 'key' item
        targetMetaDataFieldE.ChildNodes(0).ChildNodes(0).Attributes(1).NodeValue = NewFieldValue     ' 'key' item     ' 'text' item
    
    ElseIf targetMetaDataFieldE.ChildNodes(0).BaseName = "text" Then
        
        targetMetaDataFieldE.ChildNodes(0).nodeTypedValue = NewFieldValue
        
    End If
    
    Dim ctNodesSelection As IXMLDOMSelection
    Set ctNodesSelection = dwMetaDOM.SelectNodes("*")
    
    DW_Inject_DWMetaField = ctNodesSelection.Context.XML
    
Else
    
    DW_Inject_DWMetaField = ""  ' errorm could not find requested node
    
End If


End Function


Function ReplaceTextInXaml(XAMLText As String, NewText As String) As String

Dim blownXaml

blownXaml = Split(XAMLText, ">")

Dim tmpText As String

tmpText = blownXaml(2)

If InStr(1, tmpText, "</Paragraph") > 0 Then
    
    blownXaml(2) = NewText & "</Paragraph"
    
Else
'   WHAT do ? Error? Empty return?
End If

tmpText = Join(blownXaml, ">")

ReplaceTextInXaml = tmpText


End Function


Function DW_Get_MetadataNode(NodeCollection As IXMLDOMNodeList, NodeName As String) As IXMLDOMNode


Dim tmpNode As IXMLDOMNode

For Each tmpNode In NodeCollection
    If tmpNode.Attributes.length > 0 Then
        If tmpNode.Attributes(0).NodeValue = NodeName Then
            Set DW_Get_MetadataNode = tmpNode
            Exit For
        End If
    End If
Next tmpNode


End Function

Sub DW_SetDocuwrite_Metadata(TargetDocument As Document, NewDWMetadata As String)

If Documents.count = 0 Then
    StatusBar = "DW_GetDocuwrite_Metadata: NO doc, Quitting..."
    Exit Sub
End If

If HasDWMeta(TargetDocument) Then
    TargetDocument.Variables("DocuWriteMetaData").Value = NewDWMetadata
End If


End Sub


Function HasDWMeta(TargetDocument As Document) As Boolean


For Each v In TargetDocument.Variables
    If v.Name = "DocuWriteMetaData" Then
        HasDWMeta = True
        Exit Function
    End If
Next v

HasDWMeta = False

End Function



Function XAML_UTF8_Convert(XAMLText As String) As String
' Function replaces troublesome chars in XAML text, namely <,> with the
' UTF8 conventional signs, namely "&ls;" & "&gt;"

Dim tmpRes As String

tmpRes = XAMLText

tmpRes = Replace(tmpRes, "<", "&lt;")

tmpRes = Replace(tmpRes, ">", "&gt;")

XAML_UTF8_Convert = tmpRes


End Function




Function get_Moretus_DocID_Field(TargetDocument As Document) As Field


Dim fpRange As Range

Set fpRange = get_FirstPageRange(TargetDocument)


If fpRange.Characters.count = 0 Then
    
    Set get_Moretus_DocID_Field = Nothing
    Exit Function
    
Else    ' first page range returned not empty!
    
    If fpRange.Fields.count = 0 Then
        
        Set get_Moretus_DocID_Field = Nothing
        Exit Function
        
    Else    ' NO fields found on first doc page! (Broken Moretus doc!)
        
        Dim tmpField As Field
        
        Set tmpField = fpRange.Fields(1)
        
        Dim dwComDocMeta As String
        
        dwComDocMeta = DW_Get_CommissionDoc(TargetDocument)
        
        If tmpField.result.Text = dwComDocMeta Then
            
            Set get_Moretus_DocID_Field = tmpField
            
        Else
            
            Set get_Moretus_DocID_Field = Nothing
            Exit Function
            
        End If
        
    End If
    
End If


End Function


Function get_FirstPageRange(TargetDocument As Document) As Range


Dim sRng As Range

Set sRng = TargetDocument.Paragraphs(1).Range

If sRng.Information(wdActiveEndPageNumber) = 1 Then
    
    Set sRng = TargetDocument.Characters.First
    sRng.Collapse (wdCollapseStart)
        
Else
    Set sRng = sRng.GoTo(wdGoToPage, wdGoToPrevious)
    Set sRng = sRng.GoTo(wdGoToPage, wdGoToNext, 1)

End If

'sRng.Select

Dim eRng As Range

'Set eRng = sRng
Set eRng = sRng.Duplicate

If eRng.Information(wdActiveEndPageNumber) = eRng.Information(wdNumberOfPagesInDocument) Then
    Set eRng = TargetDocument.Characters.Last
    eRng.Collapse (wdCollapseEnd)
Else
    Set eRng = eRng.GoTo(wdGoToPage, wdGoToNext)
End If

' and come back to previous page...
eRng.MoveEnd wdCharacter, -1

Set get_FirstPageRange = TargetDocument.Range(sRng.Start, eRng.End)


End Function


Sub DW_CopyMetadata(TargetDocument As Document, SourceDocument As Document)


Dim uLg As String
uLg = Get_ULg

Dim dwMeta As String
dwMeta = DW_GetDocuwrite_Metadata(SourceDocument)

' having recovered initial source language doc, replace doc languages info
dwMeta = DW_Inject_DWMetaField(dwMeta, "md_DocumentLanguages", UCase(uLg))

Dim dwComSubject As String
dwComSubject = Get_ComDocSubject(TargetDocument)
' PERHAPS NEED modify to be able to inject multiple dw meta items in one call?
dwMeta = DW_Inject_DWMetaField(dwMeta, "md_Subject", UnicodeEscape(dwComSubject))

' also insert current unser initials from Word's field
dwMeta = DW_Inject_DWMetaField(dwMeta, "md_Initials", Application.UserInitials)


Call DW_SetNewMetadata(TargetDocument, dwMeta)


End Sub


Function DW_SetNewMetadata(TargetDocument As Document, NewMetadata As String) As Boolean

Dim ctDocVars As Variables
Set ctDocVars = TargetDocument.Variables
    
Dim ctDocVar As Variable

If CtDocument_HasVariableNamed(TargetDocument, "DocuWriteMetaData") Then

    Set ctDocVar = ctDocVars.Item("DocuWriteMetaData")
    ctDocVar.Value = NewMetadata

Else    ' No such prop yet

    ctDocVars.Add Name:="DocuWriteMetaData", Value:=NewMetadata

End If

End Function



Sub Aux_ListAllPopupMenus()
' Auxiliary function, to be moved/ removed at the end


Dim cbs As CommandBars

Set cbs = Application.CommandBars

Dim cb As CommandBar

Dim ct As Integer

For Each cb In cbs
    
    If cb.Type = msoBarTypeNormal Then
        
        ct = ct + 1
        
        'Debug.Print "Showing " & cb.Name
        'cb.ShowPopup
        'If MsgBox("Click OK to continue", vbOKCancel, "Move on?") = vbCancel Then
            'Exit Sub
        'End If
        'Debug.Print cb.Name & " - " & cb.Type
        Selection.InsertAfter cb.Name & " - " & cb.Type & vbCr
        
    End If
    
Next cb

Debug.Print "Listed " & ct & " popu-up menus!"

End Sub

Sub FileSaveAs_GSC(TargetDocument As Document, OriginalDocName As String)


Dim username As String
username = Environ("username")

If Documents.count > 0 Then
                        
    StatusBar = "Custom Saving As GSC document..."
        
    If username <> "" Then
        
        If Dir("T:\Docs\" & username, vbDirectory) <> "" Then

            Dim saveFolder As String
            saveFolder = "T:\Docs\" & username
            
            Dim bname As String
            Dim unitLang As String
            
            unitLang = Get_ULg
            bname = getGSCBaseName(OriginalDocName)
            
            Dim docFilePath As String
            docFilePath = saveFolder & "\" & Replace(bname, Right$(bname, 4), unitLang & Right(bname, 2)) & ".docx"
                
            TargetDocument.SaveAs2 docFilePath, WdSaveFormat.wdFormatXMLDocument
            
        End If
    
    End If
    
End If


End Sub

Sub DWAutoFinalize()

Call DWFinalization

End Sub


Sub Format_FinalLineParagraph(TargetParagraph As Paragraph)

With TargetParagraph.Format
    
    .Alignment = wdAlignParagraphCenter
    .LeftIndent = CentimetersToPoints(6)
    .RightIndent = CentimetersToPoints(6)
    
    .Borders(wdBorderBottom).LineStyle = wdLineStyleSingle
    .Borders(wdBorderBottom).Color = wdColorBlack
    .Borders(wdBorderBottom).LineWidth = wdLineWidth050pt
    
    '.Style = "Ligne Final"
    
End With

End Sub

Sub Test_GetSections()

End Sub
