Const scriptFolder = "\Microsoft\Word\WordStartScript"
Const bckDocScriptFolder = "\WordStartScript"

Const vbsScriptFileName = "GSCA3_Word_Starter.vbs"
Const lnkFileName = "GSCA3 Word 2016 Starter.lnk"
Const installerScriptFileName = "Install_GSCA3_WordStarter.vbs"
Const lnkInstallerFileName = "Install GSCA3 Word Starter.lnk"

Const WinWord_ExeLocation = "C:\Program Files (x86)\Microsoft Office\Office16\WINWORD.EXE"
Const netSourceFolderPath = "\\consilium.eu.int\dfs\trro\10-TOOLS\badkatro\1_Sources\Word\Word_2016\GSCA3_WordStart"


'***************************************************************************************************************************

dim oWShell
Set oWShell = WScript.CreateObject("WScript.Shell")
 
' where are we to copy word starting script?
dim strDestFdr
strDestFdr = oWShell.ExpandEnvironmentStrings("%Appdata%")  & scriptFolder

dim ofs
set ofs = Wscript.CreateObject("Scripting.FileSystemObject")

' did we have mydoc env var? workaround if not
If Not ofs.FolderExists(oWShell.ExpandEnvironmentStrings("%Appdata%")) Then
	strDestFdr = oWShell.ExpandEnvironmentStrings("%MyDocuments%") & bckDocScriptFolder     
End If
 
' and from where?
dim strSourceFdr
strSourceFdr = netSourceFolderPath
' TEMP workaround, for debugging in RO Unit
'strSourceFdr = "Q:\10-TOOLS\0_Word_macros\HR_Porting\Teleworkers_WordStart"
 
' what if source folder does not exist?
if not ofs.FolderExists(strSourceFdr) then
    Wscript.Echo "Error attempting to copy and create shortcut to Word Start Script, source folder does not exist(" & strSourceFdr & ")"
    WScript.Quit(1)
end if

 ' what if destination folder does not exist?
if not ofs.FolderExists(strDestFdr) then
	ofs.CreateFolder(strDestFdr)
	'Wscript.Echo "Found no destination folder, created one"
end if

' if script file exists in source directory, copy it, if not, complain!
if ofs.FileExists(strSourceFdr & "\" & vbsScriptFileName) then
    ofs.CopyFile strSourceFdr & "\" & vbsScriptFileName, strDestFdr & "\" & vbsScriptFileName, True
else
    WScript.Echo "Attempt to copy Word Start script and create shortcut to it failed, vbs script not found at expected location (" & strSourceFdr & "\" & vbsScriptFileName & ")"
    Wscript.Quit(2)
end if
 
if ofs.FileExists(strDestFdr & "\" & vbsScriptFileName) then
    Msgbox "GSCA3 Word Start script installed successfully!",,"GSCA3 Script Installation"
else
    Msgbox "GSCA3 Word Start script installation FAILED!" & Chr(13) & "Could not copy Word start script to destination folder(" & strDestFdr & ")",,"GSCA3 Script Installation"
end if
 
 
' and create Windows short-cut on Desktop to our lovely Word Start script
destLinkFile = oWShell.SpecialFolders("Programs") & "\" & lnkFileName
Set oLink = oWShell.CreateShortcut(destLinkFile)
oLink.TargetPath = strDestFdr & "\" & vbsScriptFileName
oLink.IconLocation = WinWord_ExeLocation & ", 5"		' second parameter is icon number
oLink.Save
 
' and a Copy on Desktop!"
destLinkFile = oWShell.SpecialFolders("Desktop") & "\" & lnkFileName
Set oLink = oWShell.CreateShortcut(destLinkFile)
oLink.TargetPath = strDestFdr & "\" & vbsScriptFileName
oLink.IconLocation = WinWord_ExeLocation & ", 5"
oLink.Save

destLinkFile = oWShell.SpecialFolders("Programs") & "\" & lnkInstallerFileName
Set oLink = oWShell.CreateShortcut(destLinkFile)
oLink.TargetPath = netSourceFolderPath & "\" & installerScriptFileName
oLink.IconLocation = WinWord_ExeLocation & ", 13"
oLink.Save

' and a Copy on Desktop IF not exist!
destLinkFile = oWShell.SpecialFolders("Desktop") & "\" & lnkInstallerFileName
If not ofs.FileExists(destLinkFile) then 
    Set oLink = oWShell.CreateShortcut(destLinkFile)
    oLink.TargetPath = netSourceFolderPath & "\" & installerScriptFileName
    oLink.IconLocation = WinWord_ExeLocation & ", 13"
    oLink.Save
End If

WSCript.Echo "Shortcut to GSCA3 Word 2016 Start script created successfully!" & Chr(13) & Chr(13) & _
    "Please look for it on Desktop or in Start->All Programs" & Chr(13) & Chr(13) & "You may manually drag-and-drop-it to the Start button to pin it to the Start Menu!" & Chr(13) & Chr(13) & "Enjoy!"
 
set ofs = Nothing
set oWShell = Nothing
 
WScript.Quit(0)