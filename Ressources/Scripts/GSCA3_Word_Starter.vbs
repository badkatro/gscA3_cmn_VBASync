Const VERSION = "1.1.27 | Fixing broken Word Start script (debugging)" 'DO NOT REMOVE THIS LINE
' Perhaps also check Word location at standard? If not, ask user to change constant?
Const std_WordExe_Filepath = "C:\Program Files (x86)\Microsoft Office\Office16\WINWORD.EXE"

'************************************************************************** DECLARATIONS *********************************************************************************************************
Const roCmnTpl = "gscA3_cmn.dotm"
Const roCmnLib = "gscrolib.dotm"
Const roCmnTplEmpty = "gscA3_cmn_Empty.dotm"
Const roCmnLibEmpty = "gscrolib_Empty.dotm"

Const clickOnceAppName = "clickLauncherWA.exe"
Const VBASyncExecFilename = "VBASync.exe"
Const wordStartScriptName = "GSCA3_Word_Starter.vbs"
Const Create_WordStart_LinkName = "Install GSCA3 Word Starter"
Const lnkFileName = "GSCA3 Word 2016 Starter.lnk"

Const roDocSplitHFolderName = "DocSplitter_Help"
Const roBadTmxCFFolderName = "BadTmxCounter_Fonts"
Const vbaSyncFolderName = "VBASync_v2.2.1"

Const netSourceCOFdr			= "\\consilium.eu.int\dfs\trro\10-TOOLS\badkatro\1_Sources\Word\Word_2016"
Const netSourceMTemplatesFdr	= "\\consilium.eu.int\dfs\trro\10-TOOLS\badkatro\1_Sources\Word\Word_2016\MacroTemplates"
Const netSouceWordStartFdr      = "\\consilium.eu.int\dfs\trro\10-TOOLS\badkatro\1_Sources\Word\Word_2016\GSCA3_WordStart"

Const vbaSnycAppFolderPath      = "\\consilium.eu.int\dfs\trro\10-TOOLS\badkatro\1_Sources\Windows\Console_Apps\VBASync_v2.2.1"
Const gscA3_CRepoFolderPath 	= "\\consilium.eu.int\dfs\trro\10-TOOLS\badkatro\2_Git\gscA3_cmn_VBSync"
Const gsclib_CRepoFolderPath    = "\\consilium.eu.int\dfs\trro\10-TOOLS\badkatro\2_Git\gscrolib_VBSync"

'************************************************************************ SELF INSTALL CONSTANTS *************************************************************************************************
'Directory in which to install the script (it must end with a "\")
'Const INSTALL_LOCATION = "%HOMEDRIVE%%HOMEPATH%\Scripts\vbs\Self Installer\%USERDOMAIN%\"
Const INSTALL_LOCATION = "%APPDATA%\Microsoft\Word\WordStartScript\"
 
'When run after install is true, the installed script will be automatically run.
'When set to false, the script file will only be copied to the installation folder. 
Const RUN_AFTER_INSTALL = true
'*************************************************************************************************************************************************************************************************


'*************************************************************************************************************************************************************************************************
'************************************************************************* MAIN PART *************************************************************************************************************
'*************************************************************************************************************************************************************************************************
'
'If this script isn't being run from the install location, install it to that location
'If this script is being run from the install location then execute the main code
If ScriptLocation <> GetInstallLocation Then
			
		Dim userResponseToInstall
		
		userResponseToInstall = MsgBox("You are running this script from a different location than expected." & vbCr & _
			"Would you like to install (or update) this script on your PC now?" & vbcr & vbCr & _
			"Shortcuts to local copy will be added to your Desktop & Start Menu." & vbCr & vbCr & _
			"Press Yes to install & launch, No to launch, Cancel to quit", _
			vbYesNoCancel, "GSCA3 Word Starter AutoInstall")

		If userResponseToInstall = vbYes Then
				InstallScript RUN_AFTER_INSTALL
			ElseIf userResponseToInstall = vbNo Then	' User does not wish to install to his PC, we can try to execute normally, but... 
				Call Main
			Else	' user chose cancel, comply and quit
				WScript.Quit(0)
		End If
		
	Else	' Script IS at expected location
		Call Main
End If
 
 
'#################################################################################################################################################################################################
'                                                                        SUB ROUTINES
'#################################################################################################################################################################################################
 
'Main routine
Sub Main
    'MsgBox "My file path is " & WScript.ScriptFullName, vbInformation, "Installed Script"
	' Check existence of Word executable & several 'can't do without' folders, complain & quit if not
	Call check_EssentialFiles_andPaths()

	' Do all essential files and folders exist? No installation (copying) to do, just check and update code and start Word
	If CheckInstallationOK() = False then
		Call Install_GSCA3Cmn()
		Call CheckAndUpdateCode(True)		' Function checks newest code version date against that of the templates and updates if needed, using VBASync executable (in console mode)
	Else	' All files & folders of GSCA3 seem OK, but now check if changes to Assets justified reinstall requested by dev
		If CheckReinstallRequested() Then
			If MsgBox("Reinstallation of GSCA3 was requested by developper, please click OK to continue or Cancel to quit!", vbOkCancel+vbQuestion, "Reinstallation requested") = vbOK Then
				Call mark_RevisionAccepted()	' 
				Call Install_GSCA3Cmn()
				Call CheckAndUpdateCode(True)
			End If
		Else	' Installation seems OK (files & folders present), simply check and update code if needed, without forcing (date dependent)
			Call CheckAndUpdateCode(False)				
		End If
	End if
																
	Call StartWord()
	Wscript.Quit(0)		' NO ERROR code here, installation was done (or skipped, see above), update was done (or skipped)

End Sub

'
'****************************************************************************************************************************************************************************************************
'************************************************************************** END MAIN PART ***********************************************************************************************************
'****************************************************************************************************************************************************************************************************


'****************************************************************************************************************************************************************************************************
'************************************************************************** FUNCTIONS ***************************************************************************************************************
'****************************************************************************************************************************************************************************************************
'
' MAIN FUNCTIONS
Function check_EssentialFiles_andPaths()

	Dim fso
	Set fso = WScript.CreateObject("Scripting.FileSystemObject")

	if not fso.FileExists(GetWinWordLocation) then
		WScript.Echo "Error, WinWord.exe not found at expected location, please manually correct value of constant " & _
			chrw(171) & "std_WordExe_Filepath" & chrw(187) & " from line 2 of this vbs script (" & Wscript.ScriptFullName & ")" & vbCr & vbCr & _
			"Please do NOT use Notepad for this edit, use an editor like Notepad++, Visual Studio Code or even MS Word"
		Wscript.Quit(6)
	end if

	' we define destination folder (Word Startup folder)
	Dim wdStartupFdr
	wdStartupFdr = getWordStartupFolder()

	if wdStartupFdr = "" then
		Wscript.Echo "Word startup folder does not exist! Exiting... (" & wdStartupFdr & ")"
		Wscript.Quit(3)
	end if

	Dim wdAppdataFdr
	wdAppdataFdr = getWordAppDataFolder()

	' In standard TW we have some nice environment vars setup, but not everywhere...
	if wdAppdataFdr = "" then
		Wscript.Echo "Word appdata folder does not exist! Exiting... (" & wdAppdataFdr & ")"
		Wscript.Quit(3)
	end if
	
	dim serverAddr
	serverAddr = split(netSourceMTemplatesFdr, "\")(2)		' no \\ necessary for pinging local network shares, it seems
	
	If not serverExists(serverAddr) then
		
		' TODO: Implement new alternate installation method:
		' 1. Look for usual Git paths: 
		'	 a. 'My Documents'\Git or
		'	 b. 'My Documents'\Github
		' 2. If found any of the above, look for a subfolder named as the target repo, warn and instruct if not
		' 3. If found, check required subfolders and files, continue if found, warn and instruct if not
		
		if GetGitRepos_BadeFolder() <> "" then
			'dim repos_baseFolder
			'repos_baseFolder = GetGitRepos_BadeFolder()
		else	' BOTH locations retrievals have failed
			WScript.Echo "Error: Server " & serverAddr & " is not alive, Exiting..."
			WScript.Quit(1)
		end if
		
	end if

	' Define source folder path (from here we attempt copying source files, Word templates)
	dim emptyTemplatesSourceFolder
	emptyTemplatesSourceFolder = netSourceMTemplatesFdr

	' We check source folder existence and exit with error code 1 if not
	if not fso.FolderExists(emptyTemplatesSourceFolder) then

		'wordTemplatesSourceFolder = netSourceBckFdr
		' Workaround for testing in RO Unit...
		if not fso.FolderExists(wordTemplatesSourceFolder) then
			WScript.Echo "Error: Source folder " & emptyTemplatesSourceFolder & " does NOT exist, Exiting..."
			WScript.Quit(1)
		end if

	end if

	' We define source files full paths
	dim roCmnTplSPath, roCmnLibSPath

	' having determined the existence of network source folder, we set complete source templates paths:
	roCmnTplSPath = emptyTemplatesSourceFolder & "\" & roCmnTplEmpty
	roCmnLibSPath = emptyTemplatesSourceFolder & "\" & roCmnLibEmpty

	' and check their existence, complain if not existing
	If not fso.FileExists(roCmnTplSPath) Then
		WScript.Echo "Error: Source template " & roCmnTplSPath & " does NOT exist, Exiting..."
		WScript.Quit(2)
	End if
	' same for second source file
	If not fso.FileExists(roCmnLibSPath) Then
		WScript.Echo "Error: Source template " & roCmnLibSPath & " does NOT exist, Exiting..."
		WScript.Quit(2)
	End if

	If not fso.FolderExists(vbaSnycAppFolderPath) Then
		WScript.Echo "Error: Source vbaSync folder does NOT exist, Exiting..."
		WScript.Quit(7)
	End If

	' If find self update (this very script file), Quit early to allow user to install update version of self
	'Call CheckWordStartScriptUpdate()	// old line, trying new self update
	Call CheckWordStartScript_SelfUpdate()
	' Look for Word app running and warn, give user chance to quit (rather than encounter errors)
	Call checkWordRunning()

End Function

' Function modifies the first line of running script host file to remove, if existing, the 'R' in constant VERSION's number
' (the R signifies to installing script a developer request to reinstall, 'cause of asset files update)
Function mark_RevisionAccepted()

	Const ForReading = 1, ForWriting = 2, ForAppending = 8

	Dim rsp		' running script path
	rsp = WScript.ScriptFullName

	Dim iScriptText, uScriptText, versionLine, uVersionLine
	
	Dim fso, rsStream
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set rsStream = fso.OpenTextFile(rsp, ForReading)
	
	versionLine = rsStream.ReadLine
	uVersionLine = Replace(Split(versionLine, "|")(0), " R", "") & Split(versionLine, "|")(1)	' remove ending R - dev requested reinstallation flag

	iScriptText = rsStream.ReadAll
	uScriptText = uVersionLine & iScriptText	' concatenate the doctored version line computed above (removed trailing R from version number) with rest of script file text
	rsStream.Close

	Set rsStream = fso.CreateTextFile(rsp, True)	' Create new text replacing old one
	rsStream.Write uScriptText
	rsStream.Close

End function

' Function to signal the necessity of forced update (in case of MacroTemplate update) by adding "R" to script version
' This is called after script check and update itself, so current script, reloaded, already should sport the new version number
Function CheckReinstallRequested()
	
	' Parameter "versionInfo" of get_RunningScript_Version can be: "versionNumber", "versionDescription" or "versionNumberDescription"

	Dim runningScriptVersion
	runningScriptVersion = get_RunningScript_Version("versionNumber")
	
	if Right(runningScriptVersion, 1) = "R" Then
		CheckReinstallRequested = True
	else
		CheckReinstallRequested = False
	end if
	
End Function

' Sorta presumptious, but.. function to retrieve existence of My Documents hosted Git/ GitHub repo base folder
Function GetGitRepos_BaseFolder()

	Dim fso
	Set fso = CreateObject("Scripting.FileSystemObject")

	if fso.FolderExists(GetMyDocumentsLocation() + "\Git") then
		GetGitRepos_BaseFolder = fso.GetFolder(GetMyDocumentsLocation() + "\Git").Path
	elseif fso.FolderExists(GetMyDocumentsLocation() + "\GitHub") then
		GetGitRepos_BaseFolder = fso.GetFolder(GetMyDocumentsLocation() + "\GitHub").Path
	else
		GetGitRepos_BaseFolder() = ""
	end if

End Function

Function CheckInstallationOK()
	
	' Default value.. unless changed by checks, below
	CheckInstallationOK = False
	
	dim fso
	set fso = Wscript.CreateObject("Scripting.FileSystemObject")

	' we define destination folder (Word Startup folder)
	dim wdStartupFdr
	wdStartupFdr = getWordStartupFolder()

	Dim wdAppdataFdr
	wdAppdataFdr = getWordAppDataFolder()
	
	' If both templates, the app and the two folders all exist we quit early, nothing to do but start Word!
	' Should ANY of them not exist, we carry on, need install
	If fso.FileExists(wdStartupFdr & "\" & roCmnTpl) Then
		If fso.FileExists(wdStartupFdr & "\" & roCmnLib) Then
			If fso.FileExists(wdAppdataFdr & "\" & clickOnceAppName) Then
				If fso.FileExists(wdAppdataFdr & "\" & vbaSyncFolderName & "\" & VBASyncExecFilename) Then
					If fso.FolderExists(wdAppdataFdr & "\" & roDocSplitHFolderName) Then
						If fso.FolderExists(wdAppdataFdr & "\" & roBadTmxCFFolderName) Then
							CheckInstallationOK = True
						End if
					End if
				End if
			End if
		End if
	End if

End Function 

Function Install_GSCA3Cmn()

	' INSTALLATION NOT OK, on to installing first
	' Define source folder path (from here we attempt copying source files, Word templates)
	dim emptyTemplatesSourceFolder
	emptyTemplatesSourceFolder = netSourceMTemplatesFdr
	
	' We define source files full paths
	dim roCmnTplSPath, roCmnLibSPath

	' having determined the existence of network source folder, we set complete source templates paths:
	roCmnTplSPath = netSourceMTemplatesFdr & "\" & roCmnTplEmpty		' copy empty templates, but publish code within.
	roCmnLibSPath = netSourceMTemplatesFdr & "\" & roCmnLibEmpty		' copy empty templates, but publish code within.

	' Folder and sub-folders
	dim docSplitterHelpSource 
	docSplitterHelpSource = netSourceCOFdr &"\Assets\DocSplitter_Help"

	' Copy folder and sub-folders
	dim badTmxFontsSource
	badTmxFontsSource = netSourceCOFdr & "\Assets\BadTmxCounter_Fonts"

	dim vbaSyncSource
	vbaSyncSource = netSourceCOFdr & "\Assets\" & ""

	' and files from this one
	dim wordTemplatesSource
	wordTemplatesSource = netSourceCOFdr & "\Templates\*.*"

	' file as well
	dim clickOnceAppFilepath
	clickOnceAppFilepath = netSourceCOFdr & "\Assets\" & clickOnceAppName

	dim objWShell
	Set objWShell = WScript.CreateObject("WScript.Shell")

	dim wordTemplatesDestFdr
	wordTemplatesDestFdr = objWShell.ExpandEnvironmentStrings("%Appdata%") & "\Microsoft\Templates\"

	' we define destination folder (Word Startup folder)
	dim wdStartupFdr
	wdStartupFdr = getWordStartupFolder()
	' & secondary destination folder
	Dim wdAppdataFdr
	wdAppdataFdr = getWordAppDataFolder()

	Dim fso
	Set fso = WScript.CreateObject("Scripting.FileSystemObject")

	' and copy empty template files if they're not there already
	If not fso.FileExists(wdStartupFdr & "\" & roCmnTpl) Then
					fso.CopyFile roCmnTplSPath, wdStartupFdr & "\" & roCmnTpl
	End if
	' same for second file
	If not fso.FileExists(wdStartupFdr & "\" & roCmnLib) Then
					fso.CopyFile roCmnLibSPath, wdStartupFdr & "\" & roCmnLib
	End if
	'same for clickonce app
	if not fso.FileExists(wdAppdataFdr & "\" & clickOnceAppName) then
		fso.CopyFile clickOnceAppFilepath, wdAppdataFdr & "\" & clickOnceAppName, True
	end if

	' now on to folders
	if not fso.FolderExists(wdAppdataFdr & "\" & roDocSplitHFolderName) then
		fso.CopyFolder docSplitterHelpSource, wdAppdataFdr & "\", True
	end if

	if not fso.FolderExists(wdAppdataFdr & "\" & roBadTmxCFFolderName) then
		fso.CopyFolder badTmxFontsSource, wdAppdataFdr & "\", True
	end if

	if not fso.FolderExists(wdAppdataFdr & "\" & vbaSyncFolderName) then
		fso.CopyFolder vbaSnycAppFolderPath, wdAppdataFdr & "\", True
	end if

	' Copy Word templates and overwrite (prob need to get rid of this one...)
	fso.CopyFile wordTemplatesSource, wordTemplatesDestFdr, True

	WScript.Echo("GSCA3_Cmn is installed, you should find a new Word ribbon tab called " & chr(171) & "GSC A3" & chr(187))

End Function

Function CheckAndUpdateCode(ForceUpdate)
	
	' When ForceUpdate parameter is set to false, function checks whether un update should or not be performed (based on file last modified dates)
	' As a result, messages (of updates discovery, updates results are generated and the update execution is submitted to user approval)
	' This check and the resulting messages are not generated (silent install) if the parameter is set to True

	Dim gscA3_FilePath
	gscA3_FilePath = getWordStartupFolder  & "\" & roCmnTpl

	Dim gscLib_FilePath
	gscLib_FilePath = getWordStartupFolder & "\" &  roCmnLib

	Dim userUpdateMessage
	userUpdateMessage = ""

	Dim shouldUpdateGCSA3, shouldUpdateLib
	shouldUpdateGCSA3 = False
	shouldUpdateLib = False

	Dim repoDate, templateDate, updateMessageA3, updateMessageLib
	Dim logUpdateMessage_GSCA3, logUpdateMessage_Lib

	' check if there's a reason to update gsca3
	If ForceUpdate = True Then	' either silently
		Call UpdateTemplate_fromRepo(gscA3_FilePath, updateMessageA3)
	Else	' or with messages
		If ShouldUpdate(gscA3_FilePath, repoDate, templateDate) Then
			shouldUpdateGCSA3 = True
			userUpdateMessage = "Code update found for gscA3_cmn (" & repoDate & ")"
			'logUpdateMessage_GSCA3 = Get_Latest_UpdateLog_Message("GSCA3")
			logUpdateMessage_GSCA3 = Get_UpdateLog_Messages_NewerThan("GSCA3", templateDate)	' TODO: TEST get update log messages variant function to retrieve all update log messages newer than boundary date (last mod date of client template)
			'logUpdateMessage_GSCA3 = Get_UpdateLog_Messages_NewerThan("GSCA3", "2019-01-23 00:00:00")	' DEBUGGING
			'WScript.Echo "Update message is: " & logUpdateMessage_GSCA3
		End If
	End If

	' check if there's a reson to update gscrolib
	If ForceUpdate = True Then	' either silently (at install into empty template)
		Call UpdateTemplate_fromRepo(gscLib_FilePath, updateMessageLib)
	Else	' or with message to user
		If ShouldUpdate(gscLib_FilePath, repoDate, templateDate) Then
			shouldUpdateLib = True
			If userUpdateMessage = "" Then
				userUpdateMessage = "Code update found for gscrolib (" & repoDate & ")" & vbCr & vbCr & "Update now?"
			Else 
				userUpdateMessage = userUpdateMessage & vbCr & " & gscrolib (" & repoDate & ")" & vbCr & vbCr & " Update now?"
			End If
			'logUpdateMessage_Lib = Get_Latest_UpdateLog_Message("Lib")
			logUpdateMessage_Lib = Get_UpdateLog_Messages_NewerThan("Lib", templateDate)	' TODO, see above, previous IF
			'logUpdateMessage_Lib = Get_UpdateLog_Messages_NewerThan("Lib", "2019-01-23 00:00:00")	' TODO, see above, previous IF
		Else
			If not userUpdateMessage = "" Then userUpdateMessage = userUpdateMessage & vbCr & vbCr & "Update now?"
		End If
	End If

	' Extract update log message from one or both repos, compose it into one
	Dim logMessage
	If logUpdateMessage_GSCA3 <> "" or logUpdateMessage_Lib <> "" then
		if logUpdateMessage_GSCA3 <> "" then
			logMessage = logUpdateMessage_GSCA3
			if logUpdateMessage_Lib <> "" then
				logMessage = logMessage & vbcr & logUpdateMessage_Lib
			end if
		else
			logMessage = logUpdateMessage_Lib
		end if
	end If

	' and if non-empty, add it to user update message for user presenting
	If logMessage <> "" then userUpdateMessage = userUpdateMessage & vbcr & vbcr & "Description: " & vbcr & logMessage

	If shouldUpdateGCSA3 or shouldUpdateLib Then	' ONLY ask user consent if update not forced and some update was actually found
		' Do actual code publishing if user should approve. We bundle message building and update running for not issueing too many messages
		If Msgbox(userUpdateMessage, vbYesNo+vbQuestion, "Update approval") = vbYes Then
			if shouldUpdateGCSA3 Then Call UpdateTemplate_fromRepo(gscA3_FilePath, updateMessageA3)
			if shouldUpdateLib Then Call UpdateTemplate_fromRepo(gscLib_FilePath, updateMessageLib)
			'WScript.Echo("Update message is: " & MashUpdateMessages(updateMessageA3, updateMessageLib))	
		End If
	End If

End Function

Sub StartWord()

	Dim oShell
	Set oShell = WScript.CreateObject("WScript.Shell")

	oShell.Exec(GetWinWordLocation)

	Set oShell = Nothing

End Sub

'*****************************************************************************************************************************************************************************************************
'************************************************************************** AUXILIARY FUNCTIONS ******************************************************************************************************
'*****************************************************************************************************************************************************************************************************
'
' Using ping to determine server's existence, a lot faster than alternative network method previously used !
function serverExists(server)

	Set shell = WScript.CreateObject("WScript.Shell")

	Set shellexec = shell.Exec("ping -n 1 " & server)

	result = LCase(shellexec.StdOut.ReadAll)

	If InStr(result , "reply from") Then
		serverExists = true  
	Else
		serverExists = false
	End If
    
End Function

Function GetWinWordLocation()

	Dim WinWordRegistryPAth
	WinWordRegistryPAth = "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\Winword.exe\Path"

	GetWinWordLocation = GetRegistryKey(WinWordRegistryPAth) & "\Winword.exe"

End Function

Function GetMyDocumentsLocation()

	Dim MyDocumentsRegistryPath
	MyDocumentsRegistryPath = "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders\Personal"

	GetMyDocumentsLocation = SubstituteEnvironmentVariables(GetRegistryKey(MyDocumentsRegistryPath))

End Function

Function GetRegistryKey(RegistryPath)

	Dim objShell

	Set objShell = WScript.CreateObject("WScript.Shell")
	GetRegistryKey = objShell.RegRead(RegistryPath) 
	
	Set objShell = Nothing

End Function

Function CreateGSCA3_Shortcuts()

	dim oWShell
	Set oWShell = WScript.CreateObject("WScript.Shell")

	Dim desktopFolder, programsFolder

	desktopFolder = oWShell.SpecialFolders("Desktop")
	programsFolder = oWShell.SpecialFolders("Programs")

	Call CreateShortcutTo(desktopFolder, InstalledScriptPath, lnkFileName)
	Call CreateShortcutTo(programsFolder, InstalledScriptPath, lnkFileName)

	Set oWShell = Nothing
	
End Function

Function CreateShortcutTo(ShortcutLocation, ShortcutTarget, ShortcutName)

	Dim oWShell
	Set oWShell = WScript.CreateObject("WScript.Shell")

	Dim destkLinkFile
	destLinkFile = ShortcutLocation & "\" & ShortcutName

	Dim ofs
	Set ofs = WScript.CreateObject("Scripting.FileSystemobject")

	Dim oLink

	If not ofs.FileExists(destLinkFile) then 
	    Set oLink = oWShell.CreateShortcut(destLinkFile)
	    oLink.TargetPath = ShortcutTarget
	    oLink.IconLocation = GetWinWordLocation & ", 5"
	    oLink.Save
	End If

	Set ofs = Nothing
	Set oWShell = Nothing

End Function

Function get_RunningScript_Version(versionInfo)

	Select Case versionInfo
		Case "versionNumber"
			get_RunningScript_Version = Split(VERSION, " | ")(0)
		Case "versionDescription"
			get_RunningScript_Version = Split(VERSION, " | ")(1)
		Case "versionNumberDescription"
			get_RunningScript_Version = Replace(VERSION, " | ", vbCr & vbCr)
	End select

End Function

Function get_Version_fromScriptText(ScriptTextContents, versionInfo)

	Dim versionTextLine
	versionTextLine = Split(ScriptTextContents, vbCr & vbLf)(0)

	Dim versionText
	versionText = Split(versionTextLine, Chr(34))(1)

	Select Case versionInfo
		Case "versionNumber"
			get_Version_fromScriptText = Split(versionText, " | ")(0)
		Case "versionDescription"
			get_Version_fromScriptText = Split(versionText, " | ")(1)
		Case "versionNumberDescription"
			get_Version_fromScriptText = Replace(versionText, " | ", vbCr & vbCr)
	End select

End Function

Function CheckWordStartScript_SelfUpdate()

	Dim fso
	Set fso = WScript.CreateObject("Scripting.FileSystemObject")

	Dim source_WordStart_ScriptPath
	source_WordStart_ScriptPath = netSouceWordStartFdr & "\" & wordStartScriptName

	Dim source_WordStartScript_Date
	source_WordStartScript_Date = fso.GetFile(source_WordStart_ScriptPath).DateLastModified

	Dim ct_WordStartScript_Date
	ct_WordStartScript_Date = fso.GetFile(WScript.ScriptFullName).DateLastModified

	If source_WordStartScript_Date > ct_WordStartScript_Date Then

		If MsgBox("Found update to myself dated: " & source_WordStartScript_Date & vbCr & vbcr & _
			"Should I self-update now?" & vbcr & "(Worry not, it will be done automatically as soon as you click " & chr(171) & "Yes" & chr(187) & ")", vbYesNo, "GSCA3 Word Starter SelfUpdate") = vbYes Then

			'read updated code
			Dim tempScriptStream, updatedTextContents
			Set tempScriptStream = fso.OpenTextFile(source_WordStart_ScriptPath)
			updatedTextContents = tempScriptStream.ReadAll
			tempScriptStream.Close

			'overwrite executing script file. Self update that is, self update!
			Set tempScriptStream = fso.CreateTextFile(WScript.ScriptFullName, True)
			tempScriptStream.Write(updatedTextContents)
			tempScriptStream.Close

			' Should we NOT try extracting version number from closed & updated local file first? Itd be another function, extracting first line and removing extra text
			Msgbox "Script updated to version: " & get_Version_fromScriptText(updatedTextContents, "versionNumberDescription") & vbCr & vbCr & "Relaunching initial operation...", vbOkOnly, "Update Success"

			' and re-launch. User will have to do it manually otherwise!
			Dim oShell 
			Set oShell = WScript.CreateObject("WScript.Shell")
			oShell.Run WScript.ScriptFullName,, False			' Calling the Run method with bWaitOnReturn (third param) set to false is important, 
																' thus it launches it in a diff thread and resumes execution (quitting in fact, as we can see)

			WScript.Quit

		End If

	End if

	Set oShell = Nothing
	Set fso = Nothing

ENd Function

Function CheckWordStartScriptUpdate()

	dim source_WordStart_ScriptPath
	source_WordStart_ScriptPath = netSouceWordStartFdr & "\" & wordStartScriptName

	Dim fso
	Set fso = WScript.CreateObject("Scripting.FileSystemObject")

	Dim source_WordStartScript_Date
	source_WordStartScript_Date = fso.GetFile(source_WordStart_ScriptPath).DateLastModified

	Dim ct_WordStartScript_Date
	ct_WordStartScript_Date = fso.GetFile(WScript.ScriptFullName).DateLastModified

	If source_WordStartScript_Date > ct_WordStartScript_Date Then
		If Msgbox("Update found for " & wordStartScriptName & "." & vbCr & _
			"You should to Quit this script and run " & chr(171) & Create_WordStart_LinkName & chr(187) & vbCr & vbCr & _
			"Do you wish to do that now? (This message will go away also.. :)", vbYesNo, "GSC Word Starter Update") = vbYes then

				WScript.Quit(0)

		End if
	End if

End Function

Function isWordRunning()

	' Check Word is running and react !
	dim wmi		' Windows Management Info
	dim procs
	'
	set wmi = GetObject("winmgmts:")
	set procs = wmi.ExecQuery("select * from Win32_Process Where Name='WinWord.exe'")
	'
	If procs.Count > 0 then
		isWordRunning = True
	Else	
		isWordRunning = False	
	End if

End Function

Function UpdateTemplate_fromRepo(templatePath, returnMessage)

	Dim repoPath
	If Instr(templatePath, "gscA3_cmn")  > 0 Then
		repoPath = gscA3_CRepoFolderPath
	else
		repoPath = gsclib_CRepoFolderPath
	End If

	Dim vbaSyncExecFilePath
	vbaSyncExecFilePath = getWordAppDataFolder & "\" & vbaSyncFolderName &  "\" & VBASyncExecFilename

	Dim oWShell
	Set oWShell = WScript.CreateObject("WScript.Shell")

	Dim codePublishResult
	codePublishResult = oWShell.Run(vbaSyncExecFilePath & " -r -p -d " & repoPath & " -f " & templatePath,, True)

	Dim fso
	Set fso = WScript.CreateObject("Scripting.FileSystemObject")

	if codePublishResult = 0 Then
		returnMessage = "Template " & fso.GetFileName(templatePath) & " updated!"
	else
		returnMessage = "Error " & codePublishResult & " while updating code of " & fso.GetFileName(templatePath)
	End if

	Set fso = Nothing
	Set oWShell = Nothing

End Function

Function ShouldUpdate(templateFilePath, repoNewDate, tModifiedDate)

	Dim fso 
	Set fso = WScript.CreateObject("Scripting.FileSystemObject")

	Dim repoPath
	If Instr(templateFilePath, "gscA3_cmn") > 0 then
		repoPath = gscA3_CRepoFolderPath
	Else
		repoPath = gsclib_CRepoFolderPath
	End if

	Dim repoLastModified, templLastModified
	repoLastModified = GetRepo_LastModified(repoPath)
	templLastModified = fso.GetFile(templateFilePath).DateLastModified

	If templLastModified < repoLastModified Then
		repoNewDate = repoLastModified
		tModifiedDate = templLastModified	' gotta also return this date, need it for COMPOSED log message extraction (all log mess for this oldness)
		ShouldUpdate = True
		'WScript.Echo("Found new update for " & templateFilePath & " (" & repoLastModified & ")")
	Else
		ShouldUpdate = False
	End If

End Function

Function getWordStartupFolder()

	Dim  objWShell
	Set objWShell = WScript.Createobject("WScript.Shell")

	dim tmpResult
	tmpResult = objWShell.ExpandEnvironmentStrings("%WordStartUp%")

	dim fso
	set fso = Wscript.CreateObject("Scripting.FileSystemObject")

	' In standard TW we have some nice environment vars setup, but not everywhere...
	if not fso.FolderExists(tmpResult) then
		tmpResult = objWShell.ExpandEnvironmentStrings("%Appdata%") & "\Microsoft\Word\Startup"
		'WScript.Echo "Word startup folder = " & tmpResult
		if not fso.FolderExists(tmpResult) then
			getWordStartupFolder = ""
		else
			getWordStartupFolder = tmpResult
		end if
	else
		getWordStartupFolder = tmpResult
	end if

End Function

Function getWordAppDataFolder()
	
	Dim  objWShell
	Set objWShell = WScript.Createobject("WScript.Shell")

	Dim tmpResult
	tmpResult = objWShell.ExpandEnvironmentStrings("%Appdata%") & "\Microsoft\Word"

	dim fso
	set fso = WScript.CreateObject("Scripting.FileSystemObject")	

	if not fso.FolderExists(tmpResult) then
		getWordAppDataFolder = ""
	else	
		getWordAppDataFolder = tmpResult
	end if

End Function

' DO NOT use dot for extension, only 3 chars!
Function isAllowedExtension(fileExtension, allowedFileExtensions)

	isAllowedExtension = found_Substring_inString(fileExtension, allowedFileExtensions)

End Function

Function found_Substring_inString(subString, subStringAgregate)

	found_Substring_inString = false		' will be changed in the loop below if extension found

	dim subStrings			' restrict files taken into account to specific extension or other criteria
	subStrings = Split(subStringAgregate, "|")

	for i = 0 to Ubound(subStrings)

		if subStrings(i) <> "" then
			if subStrings(i) = subString then
				found_Substring_inString = true
				exit function
			end if
		end if

	next

End Function

Function GetNewestFile_inFolder(strFolderPath, allowedFileExtensions)

    dim fso
    set fso = WScript.CreateObject("Scripting.FileSystemObject")

	dim ffolder
    set ffolder = fso.GetFolder(CStr(strFolderPath))

    if (ffolder.Files.count = 0) Then
        GetNewestFile_inFolder = Nothing
        Exit Function
    end if
	
    dim mostRecentDateTime
    mostRecentDateTime = cdate("01/01/1900 00:00:01")      'gotta initialise the date somehow: we will make it equal to first file's one and compare to the others!

	dim ffile
	for each ffile in ffolder.Files
		if isAllowedExtension(fso.GetExtensionName(ffile.Name), allowedFileExtensions) then	' only process file names with asked for extension
        	if ffile.DateLastModified > mostRecentDateTime Then
            	mostRecentDateTime = ffile.DateLastModified
			end if
		end if
    next

    GetNewestFile_inFolder = mostRecentDateTime
End Function

Function GetRepo_LastModified(repoFolderPath)

	GetRepo_LastModified = GetNewestFile_inFolder(repoFolderPath, "bas|cls|frm|frx")

End Function

Function GetFile_LastModified(strFilePath)

	Dim fso
	Set fso = WScript.CreateObject("Scripting.FileSystemObject")

	GetFile_LastModified = fso.GetFile(strFilePath).DateLastModified

End Function

Sub checkWordRunning()

	Dim bIsWordRunning
	bIsWordRunning = isWordRunning()

	if bIsWordRunning = True then
		If Msgbox("Microsoft Word seems to be running, are you sure you wish to continue ?" & vbcr & vbcr & _
				"(Please be aware that Trados Studio sometimes launches a hidden Word window, please close Studio and look for hung processes as well)", _
					vbYesNo, "GSC A3 Common Installation Warning" ) <> vbYes Then
				WScript.Quit(7)			' Error 7 - Word found to be running, user has chosen not to proceed
		End if
	End if

End sub

Function MashUpdateMessages(Message1, Message2)

	Dim tmpMessage
	If Message1 <> "" then
		tmpMessage = Message1

		if Message2 <> "" Then
			If Left(Message2, 5) = "Error" then
				tmpMessage = tmpMessage & vbCr & vbCr & Message2
			else
				tmpMessage = Replace(Replace(tmpMessage, " updated!", ""), "Template ", "Templates ") & " & " & Replace(Message2, "Template ", "")
			end if
		end if

	else
		tmpMessage = Message2
	end if

	MashUpdateMessages = tmpMessage

End Function

Function Get_Latest_UpdateMessage_fromLog(LogFilePath)

	Dim fso
	Set fso = WScript.CreateObject("Scripting.FileSystemObject")

	Dim logContent, logStream
	set logStream = fso.OpenTextFile(LogFilePath)
	logContent = logStream.ReadAll
	logStream.Close

	Get_Latest_UpdateMessage_fromLog = Trim(Split(logContent, "Date:")(1))

	Set logStream = Nothing
	Set fso = Nothing

End Function

Function Get_UpdateMessages_fromLog_NewerThan(LogFilePath, DateThreshold)

	Dim fso
	Set fso = WScript.CreateObject("Scripting.FileSystemObject")

	Dim logContent, logStream
	set logStream = fso.OpenTextFile(LogFilePath)
	logContent = logStream.ReadAll
	logStream.Close

	Dim allLogMessages, tempResult
	allLogMessages = Split(logContent, "Date:")		' entry 0 is unusable, its before the first Date: occurence, therefore empty

	Dim logEntryDate

	For i = 1 to Ubound(allLogMessages)

		logEntryDate = FormatDateTime(Trim(Split(allLogMessages(i), vbCr & vbLf)(0)))
		'WScript.Echo "Log entry " & i & ", date: " & logEntryDate

		If DateDiff("s", FormatDateTime(DateThreshold), logEntryDate) > 0 Then
			'WScript.Echo "Previous log entry date is " & DateDiff("s", FormatDateTime(DateThreshold), logEntryDate) & " s newer than threshold! (" & DateThreshold & ")"
			If tempResult = "" Then
				tempResult = Trim(allLogMessages(i))
			else 
				tempResult = tempResult & vbCr & Trim(allLogMessages(i))
			End if
		else 
			'WScript.Echo "Previous log entry date is older by " & DateDiff("s", logEntryDate, FormatDateTime(DateThreshold)) & " s than threshold! (" & DateThreshold & ")"
			tempResult = Trim(allLogMessages(i))	' upon finding latest (first) log entry older than template, we're in special case of user having updated template 
													' in the time between commit to devel branch and pull to master branch (which is then used as latest mod date for repo) 
													' - so the "newest" commit is in fact older than the template! The template's update WILL be initiated, since the last mod date of
													' template will be checked against last mod date of newest file in repo... we only have a problem with the commit being older!
													' SHOULD WE manipulate the date of the log message as well ? (make it the date of the repo's newest file)
			exit for
		end if

	next

	Get_UpdateMessages_fromLog_NewerThan = tempResult

	Set logStream = Nothing
	Set fso = Nothing

End Function

Function Get_Latest_UpdateLog_Message(WhichLog)

	Dim fso
	Set fso = WScript.CreateObject("Scripting.FileSystemObject")
	
	If UCase(WhichLog) = "GSCA3" then
		if fso.FileExists(gscA3_CRepoFolderPath & "\update.log") then
			Get_Latest_UpdateLog_Message = Get_Latest_UpdateMessage_fromLog(gscA3_CRepoFolderPath & "\update.log")
		end if
	elseif UCase(WhichLog) = "LIB" then
		if fso.FileExists(gsclib_CRepoFolderPath & "\update.log") then
			Get_Latest_UpdateLog_Message = Get_Latest_UpdateMessage_fromLog(gsclib_CRepoFolderPath & "\update.log")
		end if
	end if

	Set fso = Nothing

End Function

Function Get_UpdateLog_Messages_NewerThan(WhichLog, DateThreshold)

	Dim fso
	Set fso = WScript.CreateObject("Scripting.FileSystemObject")

	If UCase(WhichLog) = "GSCA3" then
		if fso.FileExists(gscA3_CRepoFolderPath & "\update.log") then
			Get_UpdateLog_Messages_NewerThan = Get_UpdateMessages_fromLog_NewerThan(gscA3_CRepoFolderPath & "\update.log", DateThreshold)
		else
			Get_UpdateLog_Messages_NewerThan = ""
		end if
	elseif UCase(WhichLog) = "LIB" then
		if fso.FileExists(gsclib_CRepoFolderPath & "\update.log") then
			Get_UpdateLog_Messages_NewerThan = Get_UpdateMessages_fromLog_NewerThan(gsclib_CRepoFolderPath & "\update.log", DateThreshold)
		else
			Get_UpdateLog_Messages_NewerThan = ""
		end if
	end if

	Set fso = Nothing

End Function

'
'*****************************************************************************************************************************************************************************************************
'************************************************************************ ZIP EXTRACTION TOOLS *******************************************************************************************************
'*****************************************************************************************************************************************************************************************************

' zipExtract.vbs - 	short simple script to extract a supplied zip archive to archive host folder (by default) or to supplied destination folder.
' 					Both paths may be relative or absolute

' user can call script with one or two parameters, fault him for anything else
'if WScript.Arguments.Unnamed.Count = 1 then         ' user supplied zip file only, most likely
'		runResult = zipExtract(WScript.Arguments.Unnamed.Item(0), WScript.CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.Arguments.Unnamed.Item(0)))
'	elseif WScript.Arguments.Unnamed.Count = 2 then    ' user supplied zip file name & destination folder 
'		runResult = zipExtract(WScript.Arguments.Unnamed.Item(0), WScript.Arguments.Unnamed.Item(1))
'	else
'		WScript.Echo "Error - No zip file path supplied!" & vbCr & vbCr & "Usage: zipExtract PATH\TO\FILE.ZIP [PATH\TO\DESTINATION\FOLDER]"
'		WScript.Quit(3)
'end if

' USAGE EXAMPLE BELOW: Call main function, extractZIP and supply a string argument, zip file path and eventually the destination path, both relative or absolute
' runResult = extractZIP(WScript.Arguments.Unnamed(0))
' AND use result code as below
' present result to user
'if runResult = 0 then
'		WScript.Echo ("Zip extraction successful! (" & runResult & ")")
'	else
'		WScript.Echo ("Zip extraction failed! (" & runResult & ")")
'end if

Function extractZIP(ArgumentsString)
	' ArgumentsString is a space separated string containing either one substring or 2, meaning either the zip file or the zip file and its destination folder
	' Both can be relative paths if wished, current folder being the one hosting the current script.
	' usage example: extractZIP("Assets.zip") or extractZIP("Assets.zip" ".\"), extractZIP("c:\Docs\Assets.zip" "d:\docs")

    dim zipFile, destFolder
    dim argumentsArray

	dim objFso
	Set objFSO = CreateObject("Scripting.FileSystemObject")

    if Instr(1, ArgumentsString, " ") > 0 then			' arguments string contains at least ONE space
	    
        if instr(1, ArgumentsString, "'") > 0 then		' this use case is CORRECT: caller used single quotes to protect space-containing arguments!

			argumentsArray = Split(ArgumentsString, "'")

			if UBound(argumentsArray) = 2 then			' 1 quotes separated sub-string, only zip file path/ name supplied

				' first arguments file path must be a zip file
				if LCase(objFSO.GetExtensionName(argumentsArray(1))) <> "zip" then
            		WScript.Echo "Error, first argument string must be the name/ path of a zip file! Please enclose your space-containing paths in single quotes (')"
            		WScript.Quit(2)
        		end if

				zipFile = objFSO.GetAbsolutePathName(argumentsArray(1))
				destFolder = objFSO.GetParentFolderName(zipFile)

			elseif UBound(argumentsArray) = 4 then		' 2 quotes separated sub-strings 

				zipFile = argumentsArray(1)
				destFolder = argumentsArray(3)

			else										' INCORRECT USE - uneven number of quotes used or more than 2 pairs of them
				WScript.Echo "Error, please check that you correctly used single quotes "
				WScript.Quit(4)		' INCORRECT USE - uneven number of single quotes used
			end if

		else	' no single quotes in arguments: the number of space-separated substrings MUST be 0 or 1, no larger!
			
			argumentsArray = Split(ArgumentsString, " ")
			
			if UBound(argumentsArray) > 1 then			' incorrect use case: no single quotes to protect substrings, but spaces used!
				WScript.Echo "Error, please use single quotes (') to enclose space-containing file or folder paths!"
				WScript.Quit(3) 
			elseif UBound(argumentsArray) = 1 then		'  correct use case: no quotes but only 2 sub-strings used, as a nice user he/ she is!
				zipFile = objFSO.GetAbsolutePathName(argumentsArray(0))
				destFolder = objFSO.GetAbsolutePathName(argumentsArray(1))
			elseif UBound(argumentsArray) = 0 then
				
				zipFile = objFSO.GetAbsolutePathName(argumentsArray(0))

				if LCase(objFSO.GetExtensionName(zipFile)) <> "zip" then
            		WScript.Echo "Error, first argument string must be the name/ path of a zip file! Please enclose your space-containing paths in single quotes (')"
            		WScript.Quit(2)
        		end if
				
				destFolder = objFSO.GetParentFolderName(zipFile)

			end if
			
		end if

    else        ' NO SPACE in arguments string, either ONE substring supplied.. zip file name/ path, or
        
		zipFile = objFSO.GetAbsolutePathName(ArgumentsString)
		destFolder = objFSO.GetParentFolderName(zipFile)

    end if

	returnResult = zipExtract(zipFile, destFolder)
    extractZIP = returnResult

End Function

' @param zipFilePath:       filename of zip file to extract, name only (if in same folder), relative path or absolute path
' @param outputFolderPath:  destination folder path for extraction, absolute or relative (may even use ".\" to indicate current folder)
Function zipExtract(zipFilePath, outputFolderPath)
	' Source: 	https://github.com/jgstew/tools/blob/master/VBS/zipExtract.vbs (modified)
	' Return: 	integer Error codes: 	1: zip file does not exist (first argument)
	'					                2: destination folder does not exist (second argument)
	'			success return code:	0: no problemo

	Dim strZipFilePath, strOutputFolder, objFSO, objShell

	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objShell = CreateObject("Shell.Application")

	' Sanity check: first argument, existing zip file path
	if not objFSO.FileExists(objFSO.GetAbsolutePathName(zipFilePath)) then
		WScript.Echo "Error, zip file not found! (" & objFSO.GetAbsolutePathName(zipFilePath) & ")"
		zipExtract = 1
		Exit Function
	else					' for zip file, we also check extension!
		if objFSO.GetExtensionName(zipFilePath) <> "zip" then
			WScript.Echo "Error, first argument file extension must be ""zip!"""
			zipExtract = 1
			Exit Function
		end if
	end if

	strZipFilePath = objFSO.GetAbsolutePathName(zipFilePath)

	' Sanity check: second argument, existing folder path (default to current folder... but we're not gonna use that!)
	if not objFso.FolderExists(objFSO.GetAbsolutePathName(outputFolderPath)) then
		strOutputFolder = objFSO.GetParentFolderName(strZipFilePath)
		
		if not objFSO.FolderExists(strOutputFolder) then
			Wscript.Echo "Error, second argument must point to an existing destination folder path"
			zipExtract = 2
			Exit Function
		end if
	else
		strOutputFolder = objFSO.GetAbsolutePathName(outputFolderPath)
	end if

	'  Extract the Files:
	objShell.NameSpace(strOutputFolder).copyHere(objShell.NameSpace(strZipFilePath).Items()), 1044			' 1044: 1024 + 16 + 4 
	'																												- Do not display a user interface if an error occurs (1024)
	'																												- Respond with "Yes to All" for any dialog box that is displayed
	'																												- Do not display a progress dialog box
	zipExtract = 0          ' Succesfull return

End Function

' not useful function, it only handles arrays
Function extractZipArchive(Arguments)
	' error codes returned: 	1 - more than 2 space separated arguments array elements supplied (possibly spaces in folder/ file names, correct by enclosing in single quotes)
    '                           2 - only 2 substrings supplied

    if IsArray(Arguments) then      ' 2 arguments prob, zip file path and destination
		
		dim objFSO
		set objFSO = WScript.CreateObject("Scripting.FileSystemObject")

		dim zipFile, destFolder

		' SOME error correction necessary below, for 1 ubound and for 0 ubound as well
		if Ubound(Arguments) = 1 then			' correct use case
			
            zipFile = objFSO.GetAbsolutePathName(Arguments(0))
			destFolder = objFSO.GetAbsolutePathName(Arguments(1))

		elseif UBound(Arguments) = 0 then		' one element array, prob zip file only... check extension of last path element
			
            zipFile = objFSO.GetAbsolutePathName(argumentsArray(0))
			destFolder = objFSO.GetParentFolderName(zipFile)

		else									' no other cases allowed: more than 2 elements would involve a space-containing zip path or destination folder path
			
            WScript.Echo "Error, please enclose your zip file name (or path) and your destination folder name (path) in simple quotes (')"
			Wscript.Quit(1)

		end if

    else    ' only one argument supplied, most prob the zip file path
		
        extractZipArchive = Null
		Exit Function

    end if

	runResult = zipExtract(zipFile, destFolder)
	extractZipArchive = runResult

End Function

'
'
'*****************************************************************************************************************************************************************************************************
'*****************************************************************************************************************************************************************************************************
'*****************************************************************************************************************************************************************************************************
 
'Install the script
Sub InstallScript(p_bRunAfterInstall)
    'Initialise
    Dim objFSO, objShell
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set objShell = Wscript.CreateObject("WScript.Shell")
     
    'If the install directory does not exist, create it (using a call out to the DOS command MKDIR
    'If Not objFSO.FolderExists(GetInstallLocation) Then ExecCmd "mkdir """ & GetInstallLocation & """"
	If Not objFSO.FolderExists(GetInstallLocation) Then objFSO.CreateFolder(GetInstallLocation)
 
    'Copy the new script file into the install directory
    objFSO.CopyFile WScript.ScriptFullName, GetInstallLocation, true
     
    'Give the OS some time to copy if it is a bit laggy - seen occasional issues here
    Dim intCounter
    Const WAIT_MS = 100
    Const MAXTIME_MS = 4000
    intCounter = 0
    Do While Not(objFSO.FileExists(InstalledScriptPath) OR  intCounter > (MAXTIME_MS/WAIT_MS))
        WScript.Sleep WAIT_MS
        intCounter = intCounter + 1
    Loop
	
	' Create Desktop & Start Menu shortcuts to our script
	Call CreateGSCA3_Shortcuts()

	'If the script wasn't copied output an error message, otherwise run if required.
	If objFSO.FileExists(InstalledScriptPath) Then
		
		Dim userAfterInstallResponse

		userAfterInstallResponse = Msgbox("Installation successfull!" & vbCr & vbCr & "You may find your local copy at: " & vbCr & GetInstallLocation() & vbCr & vbCr & _
			"Shortcuts to it were added to your Desktop & Start Menu, please launch it from there henceforth! (The script is also auto-update-able!)" & vbCr & vbCr & _
			"Press OK to continue initial operation or Cancel to quit", vbOkCancel, "GSCA3 Word Starter AutoInstall")
		If userAfterInstallResponse = vbOk Then
			If p_bRunAfterInstall then objShell.Run """" & InstalledScriptPath & """"
		Else
			WScript.Quit(0)
		End If
    Else
        MsgBox "Installation timed out!", vbCritical, "GSCA3 Installation"
    End If
End Sub
 
 
'Execute a DOS command
Sub ExecCmd(p_strDOSCmd)
    Dim objCommand
    Set objCommand = New clsDOSCommandExecutor
  
    objCommand.ExecuteCommand(p_strDOSCmd)
End Sub
 
 
'#####################################################################################################################################################################################################
'#######################################################################  FUNCTIONS ##################################################################################################################
'#####################################################################################################################################################################################################
 
'Return the directory the script is in
Function ScriptLocation()
    ScriptLocation = Replace(WScript.ScriptFullName, WScript.ScriptName, "")
End Function
 
 
'Return the full path to the installed script
Function InstalledScriptPath()
    InstalledScriptPath = GetInstallLocation & WScript.ScriptName
End Function
 
 
'Return the installation directory with environment variables expanded
Function GetInstallLocation()
    GetInstallLocation = SubstituteEnvironmentVariables(INSTALL_LOCATION)
End Function
 
 
'Replace environment variables in a string (delimited by %'s) with the expanded values
Function SubstituteEnvironmentVariables(p_strInput)
    Dim objShell
    Dim astrInput, intItem
    Set objShell = Wscript.CreateObject("WScript.Shell")
     
    'Tokenise the input on percentages and initialise the return string
    astrInput = Split(p_strInput, "%")
    SubstituteEnvironmentVariables = ""
     
    'Work through the elements and carry out any substitutions
    For intItem = 0 to (UBound(astrInput))
        'If we're on an odd item it must be an environment variable
        If IsOdd(intItem) Then
            'Expand the environment variable
            SubstituteEnvironmentVariables = SubstituteEnvironmentVariables & objShell.ExpandEnvironmentStrings("%" & astrInput(intItem) & "%")
        Else
            SubstituteEnvironmentVariables = SubstituteEnvironmentVariables & astrInput(intItem)
        End If
    next
End Function
 
 
'Determine if a number is odd
Function IsOdd(p_intValue)
    'Set default
    IsOdd = false
     
    'Now check if there's a remainder from modulo 2
    If p_intValue mod 2 = 1 Then IsOdd = true
End Function
 
'############################################
' CLASSES
'############################################
 
'DOS command execution class
'Based on http://www.thoughtasylum.com/blog/2009/8/8/dos-command-class-for-vbscript.html
'Slight modification to enforce command extensions to be on as MKDIR will require this.
Class clsDOSCommandExecutor
    Dim objShell, objExec
    Dim strCommand
    Dim strError
    Dim objError
    Dim objOutput
    Dim strOutput
  
    Sub ExecuteCommand(p_strCommand)
        strCommand = "cmd /E:ON /c " & p_strCommand
        Set objShell = CreateObject("Wscript.Shell" )
  
        objShell.Exec(strCommand)
  
        Set objExec = objShell.Exec(strCommand)
  
        Do Until objExec.Status
            Wscript.Sleep 200
        Loop
  
        Set objError = objExec.StdErr
        strError = objError.ReadAll
  
        Set objOutput = objExec.stdOut
        strOutput = objOutput.ReadAll
    End Sub
  
    Function GetOutput()
        GetOutput = strOutput
    End Function
  
    Function GetError()
        GetError = strError
    End Function
  
    Function Failed()
        If strError = "" Then
            Failed = false
        Else
            Failed = true
        End If
    End Function
End Class


'****************************************************************************************************************************************************************
'****************************************************************************************************************************************************************
'****************************************************************************************************************************************************************




' HISTORY:
' 1.0.0 | Initial release
' 1.1.0 | First major update, now properly self-updating
' 1.1.2-13 | Debugging
' 1.1.14 | Debugging, testing new central script update message extraction
' 1.1.15 | Self-update mechanism functional. Small debug corrections."
' 1.1.16 | Working version, modding for proper acc to client template oldness update.log messages extraction
' 1.1.17 | Client template oldness update.log messages extraction functional
' 1.1.18 | Script is now self-installable!
' 1.1.19 | Self-installable script improvements: user messages, create shortcuts
' 1.1.21 | Speed improvement: Using ping to determine server's existence.
' 1.1.22 | Renouncing at using launching cmd prompt command because of dos window launching
' 1.1.23 | More precision in repo update detection, querrying VBA files only
' 1.1.24 | Preparing for unified installation of Word Starter, adding zip extraction
' 1.1.25 | Preparing for unified installation of Word Starter, getting registry MyDocuments path
' 1.1.26 R | Added reinstall Word Start script trigger to manage macro templates update (xml changes)