' zipExtract.vbs - 	short simple script to extract a supplied zip archive to archive host folder (by default) or to supplied destination folder.
' 					Both paths may be relative or absolute

' user can call script with one or two parameters, fault him for anything else
'if WScript.Arguments.Unnamed.Count = 1 then         ' user supplied zip file only, most likely
'		runResult = zipExtract(WScript.Arguments.Unnamed.Item(0), WScript.CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.Arguments.Unnamed.Item(0)))
'	elseif WScript.Arguments.Unnamed.Count = 2 then    ' user supplied zip file name & destination folder 
'		runResult = zipExtract(WScript.Arguments.Unnamed.Item(0), WScript.Arguments.Unnamed.Item(1))
'	else
'		WScript.Echo "Error - No zip file path supplied!" & vbCr & vbCr & "Usage: zipExtract PATH\TO\FILE.ZIP [PATH\TO\DESTINATION\FOLDER]"
'		WScript.Quit(3)
'end if

runResult = extractZIP(WScript.Arguments.Unnamed(0))

' present result to user
if runResult = 0 then
		WScript.Echo ("Zip extraction successful! (" & runResult & ")")
	else
		WScript.Echo ("Zip extraction failed! (" & runResult & ")")
end if

Function extractZIP(ArgumentsString)
	' ArgumentsString is a space separated string containing either one substring or 2, meaning either the zip file or the zip file and its destination folder
	' Both can be relative paths if wished, current folder being the one hosting the current script.
	' usage example: extractZIP("Assets.zip") or extractZIP("Assets.zip" ".\"), extractZIP("c:\Docs\Assets.zip" "d:\docs")

    dim zipFile, destFolder
    dim argumentsArray

	dim objFso
	Set objFSO = CreateObject("Scripting.FileSystemObject")

    if Instr(1, ArgumentsString, " ") > 0 then			' arguments string contains at least ONE space
	    
        if instr(1, ArgumentsString, "'") > 0 then		' this use case is CORRECT: caller used single quotes to protect space-containing arguments!

			argumentsArray = Split(ArgumentsString, "'")

			if UBound(argumentsArray) = 2 then			' 1 quotes separated sub-string, only zip file path/ name supplied

				' first arguments file path must be a zip file
				if LCase(objFSO.GetExtensionName(argumentsArray(1))) <> "zip" then
            		WScript.Echo "Error, first argument string must be the name/ path of a zip file! Please enclose your space-containing paths in single quotes (')"
            		WScript.Quit(2)
        		end if

				zipFile = objFSO.GetAbsolutePathName(argumentsArray(1))
				destFolder = objFSO.GetParentFolderName(zipFile)

			elseif UBound(argumentsArray) = 4 then		' 2 quotes separated sub-strings 

				zipFile = argumentsArray(1)
				destFolder = argumentsArray(3)

			else										' INCORRECT USE - uneven number of quotes used or more than 2 pairs of them
				WScript.Echo "Error, please check that you correctly used single quotes "
				WScript.Quit(4)		' INCORRECT USE - uneven number of single quotes used
			end if

		else	' no single quotes in arguments: the number of space-separated substrings MUST be 0 or 1, no larger!
			
			argumentsArray = Split(ArgumentsString, " ")
			
			if UBound(argumentsArray) > 1 then			' incorrect use case: no single quotes to protect substrings, but spaces used!
				WScript.Echo "Error, please use single quotes (') to enclose space-containing file or folder paths!"
				WScript.Quit(3) 
			elseif UBound(argumentsArray) = 1 then		'  correct use case: no quotes but only 2 sub-strings used, as a nice user he/ she is!
				zipFile = objFSO.GetAbsolutePathName(argumentsArray(0))
				destFolder = objFSO.GetAbsolutePathName(argumentsArray(1))
			elseif UBound(argumentsArray) = 0 then
				
				zipFile = objFSO.GetAbsolutePathName(argumentsArray(0))

				if LCase(objFSO.GetExtensionName(zipFile)) <> "zip" then
            		WScript.Echo "Error, first argument string must be the name/ path of a zip file! Please enclose your space-containing paths in single quotes (')"
            		WScript.Quit(2)
        		end if
				
				destFolder = objFSO.GetParentFolderName(zipFile)

			end if
			
		end if

    else        ' NO SPACE in arguments string, either ONE substring supplied.. zip file name/ path, or
        
		zipFile = objFSO.GetAbsolutePathName(ArgumentsString)
		destFolder = objFSO.GetParentFolderName(zipFile)

    end if

	returnResult = zipExtract(zipFile, destFolder)
    extractZIP = returnResult

End Function


Function extractZipArchive(Arguments)
	' error codes returned: 	1 - more than 2 space separated arguments array elements supplied (possibly spaces in folder/ file names, correct by enclosing in single quotes)
    '                           2 - only 2 substrings supplied

    if IsArray(Arguments) then      ' 2 arguments prob, zip file path and destination
		
		dim objFSO
		set objFSO = WScript.CreateObject("Scripting.FileSystemObject")

		dim zipFile, destFolder

		' SOME error correction necessary below, for 1 ubound and for 0 ubound as well
		if Ubound(Arguments) = 1 then			' correct use case
			
            zipFile = objFSO.GetAbsolutePathName(Arguments(0))
			destFolder = objFSO.GetAbsolutePathName(Arguments(1))

		elseif UBound(Arguments) = 0 then		' one element array, prob zip file only... check extension of last path element
			
            zipFile = objFSO.GetAbsolutePathName(argumentsArray(0))
			destFolder = objFSO.GetParentFolderName(zipFile)

		else									' no other cases allowed: more than 2 elements would involve a space-containing zip path or destination folder path
			
            WScript.Echo "Error, please enclose your zip file name (or path) and your destination folder name (path) in simple quotes (')"
			Wscript.Quit(1)

		end if

    else    ' only one argument supplied, most prob the zip file path
		
        extractZipArchive = Null
		Exit Function

    end if

	runResult = zipExtract(zipFile, destFolder)
	extractZipArchive = runResult

End Function

' @param zipFilePath:       filename of zip file to extract, name only (if in same folder), relative path or absolute path
' @param outputFolderPath:  destination folder path for extraction, absolute or relative (may even use ".\" to indicate current folder)
Function zipExtract(zipFilePath, outputFolderPath)
	' Source: 	https://github.com/jgstew/tools/blob/master/VBS/zipExtract.vbs (modified)
	' Return: 	integer Error codes: 	1: zip file does not exist (first argument)
	'					                2: destination folder does not exist (second argument)
	'			success return code:	0: no problemo

	Dim strZipFilePath, strOutputFolder, objFSO, objShell

	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objShell = CreateObject("Shell.Application")

	' Sanity check: first argument, existing zip file path
	if not objFSO.FileExists(objFSO.GetAbsolutePathName(zipFilePath)) then
		WScript.Echo "Error, zip file not found! (" & objFSO.GetAbsolutePathName(zipFilePath) & ")"
		zipExtract = 1
		Exit Function
	else					' for zip file, we also check extension!
		if objFSO.GetExtensionName(zipFilePath) <> "zip" then
			WScript.Echo "Error, first argument file extension must be ""zip!"""
			zipExtract = 1
			Exit Function
		end if
	end if

	strZipFilePath = objFSO.GetAbsolutePathName(zipFilePath)

	' Sanity check: second argument, existing folder path (default to current folder... but we're not gonna use that!)
	if not objFso.FolderExists(objFSO.GetAbsolutePathName(outputFolderPath)) then
		strOutputFolder = objFSO.GetParentFolderName(strZipFilePath)
		
		if not objFSO.FolderExists(strOutputFolder) then
			Wscript.Echo "Error, second argument must point to an existing destination folder path"
			zipExtract = 2
			Exit Function
		end if
	else
		strOutputFolder = objFSO.GetAbsolutePathName(outputFolderPath)
	end if

	'  Extract the Files:
	objShell.NameSpace(strOutputFolder).copyHere(objShell.NameSpace(strZipFilePath).Items()), 1044			' 1044: 1024 + 16 + 4 
	'																												- Do not display a user interface if an error occurs (1024)
	'																												- Respond with "Yes to All" for any dialog box that is displayed
	'																												- Do not display a progress dialog box
	zipExtract = 0          ' Succesfull return

End Function