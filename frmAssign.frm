VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmAssign 
   OleObjectBlob   =   "frmAssign.frx":0000
   Caption         =   "Assign document for"
   ClientHeight    =   5760
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   12195
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   1633
End
Attribute VB_Name = "frmAssign"
Attribute VB_Base = "0{2D6776D8-BA23-4E9E-BEB9-CEAC2AA876F6}{6FF7DCEE-5AA1-4D16-9D41-DB34F29799DB}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False





' REGULAR EXPRESSION TO USE TO PROCESS EMAIL SUBJECTS FOR GSC DOCS IDS. TOKENS 1, 2 AND 3 WILL BE SET TO TYPE, NUMBER/YEAR AND SUFFIXES !
' It doesnt care for white spaces present or missing after type, before numbers of suffixes, after each suffix, before and after the docnum/year.
' IT DOES care for space present after doc num and before doc year!
' ([ABCDEGLMNPRSTU]{2})?\s*(\d{1,5}/(?:\d{4}|\d{2}))\s*((?:(?:ADD|AMD|COR|EXT|REV|AD|AM|CO|EX|RE)\s*\d{1,2}\s*){0,4})

Private activitiesCode As Byte

Public tbaDocuments As String

Const astInit As String = "ag,ao,cg,ci,cm,pp,rg,rr,scm"
Const astUser As String = "georgan,oancevi,ganeaco,istoccl,margama,panaipa,georgma,radural,muntese"

Const keyCombosNames As String = "Form Times Disable|Form Pin|Form Skip2 Start|Form Skip2 End|Day Calendar|Day Pin|Day Advance|Day Rewind|Time Disable|Time Pin|Time Skip2 Start|Time Skip2 End|Time Hour+|Time Hour-|Time Minute+|Time Minute-"
Const keyCombosDefaultValues As String = "Control+Delete|Control+Insert|Control+Home|Control+End|Home|Insert|PageUp|PageDown|Delete|Insert|Control+PageUp|Control+PageDown|Home|End|PageUp|PageDown"

'Const adInit As String = "aa,aav,ac,cd,cl,dmg,eb,est,il,ir,irp,ivo,lc,lt,mcm,mn,nct,nt,ot,rar,rig,rm,rp,rs,sm,vv"
'Const adUser As String = "antonad,voivoar,capanad,dincaco,lavriel,gorbade,bodeman,turcsuz,lunguio,raileiu,pachiir,voitoir,costolu,teodolu,mitocma,nancami,todorcr,trutana,tihenoa,radulro,gaftara,mireura,popaade,ionesro,mantasi,vintivi"

Const sectionControlNames = "ColorLabel1,ColorLabel2,TitleLabel,Cbo,MultiAssignLabel,AssignLabel,DDayLabel,DayLabel,TimeLabel,DeadlineDay,DayPinLbl,TimePinLbl,DatePickerLbl,DeadlineTime,NoTimeLbl,StartWorkdayLbl,EndWorkdayLbl"

Const sectionNames As String = "ast,tra,rev,val,fin"

Private Const GWL_WNDPROC = -4

Public ccTo As String   ' when user prefills this after load event, add this person to "CC" (copy carbon, copy of mail to this person)
Public astInitData As Variant, traInitData As Variant, revInitData As Variant   ' if after loading (from Outlook), user sets one/ more of these, userform will be initialized to this data
Public AutoActivationDone As Boolean    ' flag so as to recognize the first time activation of userform is done
Public ActiveTextboxName As String
Public frmHwnd As Long

Const vbKeyNames As String = "vbKey0|vbKey1|vbKey2|vbKey3|vbKey4|vbKey5|vbKey6|vbKey7|vbKey8|vbKey9|vbKeyA|vbKeyAdd|vbKeyB|vbKeyBack|vbKeyC|vbKeyCancel|vbKeyCapital|vbKeyClear|vbKeyControl|vbKeyD|vbKeyDecimal|vbKeyDelete|vbKeyDivide|vbKeyDown|vbKeyE|vbKeyEnd|vbKeyEscape|vbKeyExecute|vbKeyF|vbKeyF1|vbKeyF10|vbKeyF11|vbKeyF12|vbKeyF13|vbKeyF14|vbKeyF15|vbKeyF16|vbKeyF2|vbKeyF3|vbKeyF4|vbKeyF5|vbKeyF6|vbKeyF7|vbKeyF8|vbKeyF9|vbKeyG|vbKeyH|vbKeyHelp|vbKeyHome|vbKeyI|vbKeyInsert|vbKeyJ|vbKeyK|vbKeyL|vbKeyLButton|vbKeyLeft|vbKeyM|vbKeyMButton|vbKeyMenu|vbKeyMultiply|vbKeyN|vbKeyNumlock|vbKeyNumpad0|vbKeyNumpad1|vbKeyNumpad2|vbKeyNumpad3|vbKeyNumpad4|vbKeyNumpad5|vbKeyNumpad6|vbKeyNumpad7|vbKeyNumpad8|vbKeyNumpad9|vbKeyO|vbKeyP|vbKeyPageDown|vbKeyPageUp|vbKeyPause|vbKeyPrint|vbKeyQ|vbKeyR|vbKeyRButton|vbKeyRight|vbKeyS|vbKeySelect|vbKeySeparator|vbKeyShift|vbKeySnapshot|vbKeySpace|vbKeySubtract|vbKeyT|vbKeyTab|vbKeyU|vbKeyUp|vbKeyV|vbKeyW|vbKeyX|vbKeyY|vbKeyZ"
Const vbKeyValues As String = "48|49|50|51|52|53|54|55|56|57|65|107|66|8|67|3|20|12|17|68|110|46|111|40|69|35|27|43|70|112|121|122|123|124|125|126|127|113|114|115|116|117|118|119|120|71|72|47|36|73|45|74|75|76|1|37|77|4|18|106|78|144|96|97|98|99|100|101|102|103|104|105|79|80|34|33|19|42|81|82|2|39|83|41|108|16|44|32|109|84|9|85|38|86|87|88|89|90"

Private vbKeyNamesValues() As String     ' Names and values of vbKeyCodeConstants

Const TimeButtonsColor As Long = 8421504     ' wdColorGray50
Const DayButtonsColor As Long = 8421504      ' wdColorGray50


Private Sub setAsActiveLabel(TextboxName As String)

ActiveTextboxName = TextboxName
'StatusBar = "ActiveTextboxName is now " & ActiveTextboxName

End Sub



Private Sub astDeadlineDay_Click()
            
    Call Activate_Label(astDeadlineDay.Name, wdColorRed)

End Sub


Private Sub AdListAdd_Click()

If Not InTheList(Me.cbAdUsersList, Me.cbAdUsersList.Value) Then
    Me.cbAdUsersList.AddItem
    Me.cbAdUsersList.List(Me.cbAdUsersList.listCount - 1) = Me.cbAsdUsersList.Value
End If

End Sub


Private Sub astCbo_Enter()

' 12648447   ast yellow
Call Activate_Section(EnabledState:=True, SectionName:="ast", ColorCode:=Me.PrepAccentColor)


End Sub

Private Sub astCbo_Exit(ByVal Cancel As MSForms.ReturnBoolean)

' deactivate when leaving activity section only if user did not choose to use it
If astCbo.Text = "" Then
    Call Activate_Section(EnabledState:=False, SectionName:="ast", ColorCode:=14737632)     ' gray
Else
    ' correct for accidental input of NP characters
    Dim cleanedAstCbo As String
    cleanedAstCbo = Clean_NPCharacters(Me.astCbo.Text)
    
    If cleanedAstCbo = "" Then
        Me.astCbo.Text = ""
        Call Activate_Section(EnabledState:=False, SectionName:="ast", ColorCode:=14737632)     ' gray
    End If
    
    Me.astAssignLabel.ForeColor = -2147483633
    Me.astMultiAssignLabel.ForeColor = -2147483633
    
End If

End Sub


Function Clean_NPCharacters(InputString As String) As String

Dim cstrg As String
cstrg = Replace(Replace(Replace(InputString, vbTab, ""), vbLf, ""), vbCr, "")
cstrg = Replace(Replace(cstrg, " ", ""), "_", "")

Clean_NPCharacters = cstrg

End Function


Private Sub astCbo_MouseMove(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If astCbo.Text <> "" Then
    If Not astAssignLabel.ForeColor = wdColorGray50 Then
        astAssignLabel.ForeColor = wdColorGray50
        astMultiAssignLabel.ForeColor = wdColorGray50
    End If
End If

End Sub


Private Sub astColorLabel1_Click()

Call Activate_Section(EnabledState:=True, SectionName:="ast", ColorCode:=Me.PrepAccentColor)
If Me.astCbo.Enabled Then Me.astCbo.SetFocus

End Sub


Private Sub astColorLabel2_Click()

Me.astCbo.Text = ""
Call Activate_Section(EnabledState:=False, SectionName:="ast", ColorCode:=14737632)     ' gray

End Sub


Private Sub astDatePickerLbl_Click()

'Dim ctTxtb As MSForms.ComboBox
'Set ctTxtb = Me.astCbo
'
'Dim ctDTxtb As MSForms.TextBox
'Set ctDTxtb = Me.astDeadlineDay

Call SyncMove_AllDeadlineDays_fromCalendar(Me.astDeadlineDay, Me.tbAccentColor.Value, Me.astCbo)


End Sub

Private Sub astDayLabel_Click()

Call Activate_Label(astDayLabel.Name, wdColorRed)

End Sub

Private Sub astDayPinLbl_Click()

If astDeadlineDay.Enabled Then
    Call Flip_PinnedState_ofLabel(astDayPinLbl, wdColorDarkBlue)
End If

End Sub

Private Sub astDeadlineDay_Enter()
    
    Me.astDeadlineDay.Tag = Me.astDeadlineDay.Text
    Call Activate_Textbox(astDeadlineDay.Name, wdColorRed)
    astDeadlineDay.SelStart = 0

End Sub

Private Sub astDeadlineDay_Exit(ByVal Cancel As MSForms.ReturnBoolean)

If Me.astDeadlineDay.Text <> "" Then
    
    Call ColorWarn_ScrolledInThePast(Me.astDeadlineDay)
    
    If CDate(Me.astDeadlineDay.Text) <> CDate(Me.astDeadlineDay.Tag) Then
        'Call Wind_Date(Me.Controls(SectionPrefix & "DeadlineDay").Name, wdColorRed, KeyCode, Shift)
        
        timeint = DateDiff("d", CDate(Me.astDeadlineDay.Tag), CDate(Me.astDeadlineDay.Text))
        
        Call Wind_All_DateTime_AwayFrom("Date", Me.astDeadlineDay.Name, CInt(timeint), timeint * 120, IIf(timeint > 0, vbKeyPageUp, vbKeyPageDown))
        
    End If
    
End If

Call Deactivate_Active_Textbox

End Sub

Private Sub astDeadlineDay_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
' hidden textbox, sole purpose it to activate the corresponding label (so as to
' hook it up to keys responses as well as mouse wheel moves!)

Call DeadlineDay_ProcessEvents(KeyCode, Shift, wdColorDarkBlue, "ast")

End Sub


Sub DeadlineDay_ProcessEvents(KeyCode As MSForms.ReturnInteger, Shift As Integer, PinForeColor As WdColor, SectionPrefix As String)


If (KeyCode = wdKeyPageDown Or KeyCode = wdKeyPageUp) And Shift = 0 Then
    
    Call Wind_Date(Me.Controls(SectionPrefix & "DeadlineDay").Name, wdColorRed, KeyCode, Shift)
    
    ' If user chose to, auto "scroll" to beginning/ middle of day
'    If Me.chAutoScroll_ToNextDay_Time Then
'
'        If Me.tbAutoScroll_JumpToWhat.Value = "middle" Then
'            Call Deadlines_JumpTo_Time("MOD")
'        Else
'            Call Deadlines_JumpTo_Time("SOD")
'        End If
'
'    End If

ElseIf KeyCode = vbKeyInsert Then   ' Pin it

    Call Flip_PinnedState_ofLabel(Me.Controls(SectionPrefix & "DayPinLbl"), PinForeColor)

ElseIf KeyCode = vbKeyHome Then     ' Choose day from Calendar
    
    If SectionPrefix <> "val" And SectionPrefix <> "fin" Then
        Call SyncMove_AllDeadlineDays_fromCalendar(Me.Controls(SectionPrefix & "DeadlineDay"), Me.tbAccentColor, Me.Controls(SectionPrefix & "Cbo"))
    Else
        Call SyncMove_AllDeadlineDays_fromCalendar(Me.Controls(SectionPrefix & "DeadlineDay"), Me.tbAccentColor)
    End If
    
ElseIf KeyCode = vbKeyTab And Shift = 2 Then     ' Shift = 2: Control key pressed - skip from days to times column
    
    If SectionPrefix <> "val" And SectionPrefix <> "fin" Then
        Call SkipTo_DeadlineTimesColumn(SectionPrefix)
    End If
        
End If


End Sub


Sub SkipTo_DeadlineTimesColumn(CallingSectionPrefix As String)


Select Case CallingSectionPrefix

    Case "ast"
        
        Me.Controls(CallingSectionPrefix & "DeadlineTime").SetFocus
        
    Case "tra"
        
        ' skip to next column on Control+Tab!
        If astDeadlineDay.Enabled Then
            Me.astDeadlineTime.SetFocus
        Else
            Me.traDeadlineTime.SetFocus
        End If
        
    Case "rev"
        
        If astDeadlineDay.Enabled Then
            Me.astDeadlineTime.SetFocus
        ElseIf traDeadlineTime.Enabled Then
            Me.traDeadlineTime.SetFocus
        Else
            Me.revDeadlineTime.SetFocus
        End If
        
    Case "val"  ' no skipping for these!
    Case "fin"  ' no skipping for these!
    
End Select


End Sub


Sub Activate_Label(LabelName As String, ActivationColour As WdColor)

    'Call QuickFocusSwitch
    
    Call Deactivate_PreviousLabel
        
    Call setAsActiveLabel(LabelName)
    
    'Dim ctLabel As label
    Dim ctLabel As label
    'Set ctLabel = Me.Controls(LabelName)
    Set ctLabel = Me.Controls(LabelName)
    
    ctLabel.ForeColor = ActivationColour

End Sub



Sub Activate_Textbox(TextboxName As String, ActivationColour As WdColor)

    'Call QuickFocusSwitch
    
    Call Deactivate_PreviousLabel
        
    Call setAsActiveLabel(TextboxName)
    
    'Dim ctLabel As label
    Dim ctTextbox As TextBox
    'Set ctLabel = Me.Controls(LabelName)
    Set ctTextbox = Me.Controls(TextboxName)
    
    ctTextbox.ForeColor = ActivationColour
    
    ' also make visible little buttons attached to current textbox
    If InStr(1, TextboxName, "Day") > 0 Then
        Me.Controls(Left(TextboxName, 3) & "DatePickerLbl").ForeColor = DayButtonsColor
        Me.Controls(Left(TextboxName, 3) & "DayPinLbl").ForeColor = DayButtonsColor
    Else
        Me.Controls(Left(TextboxName, 3) & "NoTimeLbl").ForeColor = TimeButtonsColor
        Me.Controls(Left(TextboxName, 3) & "TimePinLbl").ForeColor = TimeButtonsColor
        Me.Controls(Left(TextboxName, 3) & "StartWorkdayLbl").ForeColor = TimeButtonsColor
        Me.Controls(Left(TextboxName, 3) & "EndWorkdayLbl").ForeColor = TimeButtonsColor
    End If

End Sub


Sub Deactivate_PreviousLabel()

Dim activeTextboxT As TextBox
    
If ActiveTextboxName <> "" Then
    
    On Error Resume Next
    Set activeTextboxT = Me.Controls(ActiveTextboxName)
    If Err.Number <> 0 Then Err.Number = 0
    
    On Error Resume Next
    activeTextboxT.ForeColor = wdColorBlack
    If Err.Number <> 0 Then Err.Number = 0
    
End If

ActiveTextboxName = ""

End Sub


Private Sub astDeadlineDay_KeyUp(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

End Sub

Private Sub astDeadlineDay_MouseMove(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If isSectionEnabled("ast") Then

    If Not Me.astDatePickerLbl.ForeColor = DayButtonsColor Then
    
        astDatePickerLbl.ForeColor = DayButtonsColor
        astDayPinLbl.ForeColor = DayButtonsColor
        
        astDDayLabel.ForeColor = wdColorGray50
        astDayLabel.ForeColor = wdColorGray50
        
    End If

End If

End Sub

Private Sub astDeadlineTime_Enter()

astDeadlineTime.Tag = astDeadlineTime.Text
Call Activate_Textbox(astDeadlineTime.Name, wdColorRed)
astDeadlineTime.SelStart = 0

End Sub

Private Sub astDeadlineTime_Exit(ByVal Cancel As MSForms.ReturnBoolean)

If Me.astDeadlineTime.Text <> "" Then
    
    If CDate(Me.astDeadlineTime.Text) <> CDate(Me.astDeadlineTime.Tag) Then
        'Call Wind_Date(Me.Controls(SectionPrefix & "DeadlineDay").Name, wdColorRed, KeyCode, Shift)
        
        ' EXPERIMENT... Does it work ?
        timeint = DateDiff("n", CDate(Me.astDeadlineTime.Tag), CDate(Me.astDeadlineTime.Text))    ' n = minutes difference
        
        Call Wind_All_DateTime_AwayFrom("Time", Me.astDeadlineTime.Name, CInt(timeint), timeint * 120, IIf(timeint > 0, vbKeyPageUp, vbKeyPageDown))
        
    End If
    
End If


Call Deactivate_Active_Textbox

End Sub

Private Sub astDeadlineTime_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

'If (KeyCode = vbKeyPageDown Or KeyCode = vbKeyPageUp Or _
'    KeyCode = vbKeyHome Or KeyCode = vbKeyEnd) And Shift = 0 Then
'    Call Wind_Time(KeyCode)
'End If

Call DeadlineTime_ProcessEvents(KeyCode, Shift, "ast", wdColorDarkBlue, wdColorDarkRed)


End Sub

Sub Wind_Time(KeyCode)

If KeyCode = vbKeyHome Or KeyCode = wdKeyPageUp Then
    Call MouseWheel(120, KeyCode)
ElseIf KeyCode = vbKeyEnd Or KeyCode = wdKeyPageDown Then
    Call MouseWheel(-120, KeyCode)
End If

End Sub

Private Sub astDeadlineTime_MouseMove(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If isSectionEnabled("ast") Then

    If Not Me.astNoTimeLbl.ForeColor = TimeButtonsColor Then
    
        astNoTimeLbl.ForeColor = TimeButtonsColor
        astTimePinLbl.ForeColor = TimeButtonsColor
        astStartWorkdayLbl.ForeColor = TimeButtonsColor
        astEndWorkdayLbl.ForeColor = TimeButtonsColor
        
        astTimeLabel.ForeColor = wdColorGray50
        
    End If

End If

End Sub

Private Sub astEndWorkdayLbl_Click()

If astDeadlineTime.Enabled Then
    Call DeadlineTime_JumpTo("EOD", astEndWorkdayLbl)
End If

End Sub


Sub DeadlineTime_JumpTo(JumpToWhat As String, CallingLabel As MSForms.control)
' JmpToWhat: EOD or SOD (End of day or Start of day) or "End" or "Start"


'Dim ctOptionsFile As New SettingsFileClass

Dim ctEndOfDayTime As String

If JumpToWhat = "EOD" Or LCase(JumpToWhat) = "end" Then
    ctEndOfDayTime = Me.tbEndOfDayTime.Value
ElseIf JumpToWhat = "SOD" Or LCase(JumpToWhat) = "start" Then
    ctEndOfDayTime = Me.tbStartOfDayTime.Value    ' HARD coded for now? Option, anyone? Where to put it!?!
ElseIf JumpToWhat = "MOD" Or LCase(JumpToWhat) = "middle" Then
    ctEndOfDayTime = "12:00"
End If


Dim tgTextbox As TextBox
Set tgTextbox = Me.Controls(Left(CallingLabel.Name, 3) & "DeadlineTime")

If ctEndOfDayTime <> "" Then
    tgTextbox.Value = ctEndOfDayTime
Else
    tgTextbox = Me.tbEndOfDayTime
End If


End Sub


Private Sub AstListAdd_Click()

If Not InTheList(Me.cbAstUsersList, Me.cbAstUsersList.Value) Then
    Me.cbAstUsersList.AddItem
    Me.cbAstUsersList.List(Me.cbAstUsersList.listCount - 1) = Me.cbAstUsersList.Value
End If

End Sub


Private Sub astEndWorkdayLbl_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If astDeadlineTime.Enabled Then
    astEndWorkdayLbl.ForeColor = wdColorGreen
End If

End Sub


Private Sub astEndWorkdayLbl_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If astDeadlineTime.Enabled Then
    astEndWorkdayLbl.ForeColor = TimeButtonsColor
End If

End Sub

Private Sub astMultiAssignLabel_Click()

If Me.lbMultipleAssignments.Left > 500 Then
    Call Summon_MultiAssignment("ast")
Else
    Call Dismiss_MultiAssignment
End If

End Sub


Private Sub astNoTimeLbl_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

'If Button = 1 Then
'    astNoTimeLbl.ForeColor = wdColorDarkRed
'End If
'
'Call DeadlineTime_Deactivate(Button, Shift, astNoTimeLbl, astDeadlineTime, wdColorDarkRed)

'If astDeadlineTime.Enabled Then
    Call DeadlineDeactivate_ProcessEvents(Button, Shift, astNoTimeLbl, astDeadlineTime, wdColorDarkRed, TimeButtonsColor)
'End If


End Sub


Sub DeadlineTime_Deactivate(ButtonCode As Integer, ShiftCode As Integer, ByRef CallingLabel As MSForms.label, ByRef TargetTextbox As MSForms.TextBox, ActivationColor As WdColor)


'If ButtonCode = 1 Then

    If TargetTextbox.Enabled = True Then
        TargetTextbox.Enabled = False
    Else
        TargetTextbox.Enabled = True
    End If
    
'ElseIf ButtonCode = 2 Then      ' right mouse
    
    'Call Flip_PinnedState_ofLabel(CallingLabel, ActivationColor)
    
    'If astNoTimeLbl.BackStyle = fmBackStyleOpaque Then
        'astNoTimeLbl.Top = astNoTimeLbl.Top - 1
    'Else
        'astNoTimeLbl.Top = astNoTimeLbl.Top + 1
    'End If
    
'End If


End Sub

Private Sub astNoTimeLbl_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

'If astDeadlineTime.Enabled Then
    If Button = 1 Then
        astNoTimeLbl.ForeColor = TimeButtonsColor
    End If
'End If

End Sub

Private Sub astStartWorkdayLbl_Click()

If astDeadlineTime.Enabled Then
    Call DeadlineTime_JumpTo("SOD", Me.astDeadlineTime)
End If

End Sub

Private Sub astStartWorkdayLbl_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If astDeadlineTime.Enabled Then
    astStartWorkdayLbl.ForeColor = wdColorGreen
End If

End Sub

Private Sub astStartWorkdayLbl_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If astDeadlineTime.Enabled Then
    astStartWorkdayLbl.ForeColor = TimeButtonsColor
End If

End Sub

Private Sub astTimePinLbl_Click()

If astDeadlineTime.Enabled Then
    Call Flip_PinnedState_ofLabel(astTimePinLbl, wdColorDarkBlue)
End If

End Sub



Private Sub butCancel_Click()

If Me.butCancel.Caption = "Cancel" Then
    Unload Me
Else
    
    Dim res As Variant
    res = GetSelectedItems_fromListbox(Me.lbMultipleAssignments)
    
    Dim ctCbo As MSForms.ComboBox
    Set ctCbo = Me.Controls(Me.lbMultipleAssignments.Tag & "Cbo")
    
    ctCbo.Text = Join(res, ",")
    
    Call Dismiss_MultiAssignment
    
End If

End Sub

Function GetSelectedItems_fromListbox(TargetListbox As MSForms.ListBox) As Variant


Dim selStr As String

For i = 0 To TargetListbox.listCount - 1
    If TargetListbox.Selected(i) Then
        selStr = IIf(selStr = "", TargetListbox.List(i), selStr & "," & TargetListbox.List(i))
    End If
Next i

If selStr <> "" Then
    GetSelectedItems_fromListbox = Split(selStr, ",")
Else
    GetSelectedItems_fromListbox = Null
End If


End Function


Function Current_MailMode() As String

If Me.lblMailMode2.Visible Then
    Current_MailMode = "Mult Msg"
Else
    Current_MailMode = "One Msg"
End If

End Function


Private Sub butCreateMails_Click()

'Dim activitiesCode As Byte

' 000 (0) - no activity - complain
' 001 (1) - rev only
' 010 (2) - translation only
' 100 (4) - preparation only
' 101 (5) - preparation an revision only
' 110 (6) - prep and tra only (unlikely)
' 111 (7) - prep, tra and rev

If Not Me.lblPinForm.ForeColor <> wdColorGray25 Then     ' form is pinned by user
    Me.Hide
End If

'Debug.Print "Activities code computed as " & activitiesCode: Exit Sub

If activitiesCode = 0 Then MsgBox "Please fill in at least one of the PREP, TRA or REV initials text boxes!", _
    vbOKOnly + vbCritical, "NO assignment selected!": Exit Sub


If Current_MailMode = "Mult Msg" Then

    For i = 0 To 2
            
        If activitiesCode And 2 ^ i Then    ' if user chose that particular operation
            
            Select Case i
                Case 0  ' preparation activity
                    Call Mail_CtDoc_forActivity(Me.astCbo, 2 ^ i, Me.ccTo)
                Case 1  ' translation
                    
                    ' only launch a mail to translator if he is NOT doing REV too
                    If Me.revCbo.Enabled Then
                        If Me.traCbo <> Me.revCbo Then
                            Call Mail_CtDoc_forActivity(Me.traCbo, 2 ^ i, Me.ccTo)
                        Else    ' if they're the same, deffer processing, will do in rev loop!
                        End If
                    Else    ' rev NOT enabled, no need to bundle ops msg to one person (tra+rev)
                        Call Mail_CtDoc_forActivity(Me.traCbo, 2 ^ i, Me.ccTo)
                    End If
                    
                Case 2  ' revision
                    
                    If Me.traCbo.Enabled Then
                        
                        If Me.revCbo <> Me.traCbo Then
                            Call Mail_CtDoc_forActivity(Me.revCbo, 2 ^ i, Me.ccTo)
                        Else    ' HERE's the corner case, both ops enabled, same person to do both, mult msg active.
                        ' We're NOT going to follow suite and send 2 msg with 2 ops to ONE person ! (about the same doc, too!)
                            
                            Call Mail_CtDoc_forActivity(Me.revCbo, 2 ^ i + 2 ^ (i - 1), Me.ccTo)
                            
                        End If
                        
                    Else
                        Call Mail_CtDoc_forActivity(Me.revCbo, 2 ^ i, Me.ccTo)
                    End If
                    
                    
            End Select
            
        End If
        
    Next i

Else    ' One Msg

    Dim allDest As String
    
    For j = 0 To 2
        If activitiesCode And 2 ^ j Then
            Select Case j
                Case 0
                    If Me.astColorLabel1.BackColor = Me.PrepAccentColor.Value Then
                        allDest = IIf(allDest = "", Me.astCbo, allDest & "," & Me.astCbo)
                    End If
                Case 1  ' adding current op (trad) only if not already there
                    If Me.traColorLabel1.BackColor = Me.TraAccentColor.Value Or Me.traColorLabel1.BackColor = Me.TraRevAccentColor.Value Then
                        allDest = IIf(allDest = "", Me.traCbo, IIf(InStr(1, allDest, Me.traCbo) = 0, allDest & "," & Me.traCbo, allDest))
                    End If
                Case 2  ' Add current op (rev) only if not already there, causing problems
                    If Me.revColorLabel1.BackColor = Me.RevAccentColor.Value Or Me.revColorLabel1.BackColor = Me.TraRevAccentColor.Value Then
                        allDest = IIf(allDest = "", Me.revCbo, IIf(InStr(1, allDest, Me.revCbo) = 0, allDest & "," & Me.revCbo, allDest))
                    End If
            End Select
        End If
    Next j
    
    Call Mail_CtDoc_forActivity(allDest, CInt(activitiesCode), Me.ccTo)

End If


If Not Me.lblPinForm.ForeColor <> wdColorGray25 Then
    Unload Me
End If


End Sub

Private Sub astCbo_Click()

    'Debug.Print "astCboClick: Hwnd = " & LocalHwnd & "; PrevHwnd = " & LocalPrevWndProc & "; ActiveTextboxName = " & ActiveTextboxName
    'Debug.Print "astCboClick: Form WndProc Adress now: " & GetWindowLong(LocalHwnd, GWL_WNDPROC)
    'Debug.Print "cobAstClick: MatchFound = " & Me.astCbo.MatchFound
        
    ' Debug Prints its own success or failure !
    Me.traCbo.SetFocus
    
End Sub


Private Sub butOptions_Click()
' "Open" options part of form, by widening or restraining form to the right

If Application.Name = "Outlook" Then
    
    If Left$(Application.Version, 2) = "16" Then
        
        If Me.Width < 360 Then
            
            Me.Width = 620  ' Open options, retrieve settings from stored options file
            
            
            ' save current page and return to it
            Dim ctPage As Integer
            ctPage = Me.OptionsMultiPage.Value
            
            If ctPage < Me.OptionsMultiPage.Pages.count - 1 Then
                
                Me.OptionsMultiPage.Value = Me.OptionsMultiPage.Value + 1
                Me.OptionsMultiPage.Value = Me.OptionsMultiPage.Value - 1
                
            ElseIf ctPage = Me.OptionsMultiPage.Pages.count Then
                
                Me.OptionsMultiPage.Value = Me.OptionsMultiPage.Value - 1
                Me.OptionsMultiPage.Value = Me.OptionsMultiPage.Value + 1
                
            End If
            
            
        Else
            Me.Width = 354
        End If
    
    Else
    
        GoTo Unchanged
    
    End If
    
Else    ' Word and Outlook 2010 and 2016
    
Unchanged:
    
    If Left$(Application.Version, 2) = "14" Then

        If Me.Width < 350 Then
            Me.Width = 615  ' Open options, retrieve settings from stored options file
            
            ctPage = Me.OptionsMultiPage.Value
            
            If ctPage < Me.OptionsMultiPage.Pages.count - 1 Then  ' first one
                
                Me.OptionsMultiPage.Value = Me.OptionsMultiPage.Value + 1
                Me.OptionsMultiPage.Value = Me.OptionsMultiPage.Value - 1
                
            ElseIf ctPage = Me.OptionsMultiPage.Pages.count - 1 Then    ' differently based - 0 and 1
                
                Me.OptionsMultiPage.Value = Me.OptionsMultiPage.Value - 1
                Me.OptionsMultiPage.Value = Me.OptionsMultiPage.Value + 1
                
            End If

            
        Else
            Me.Width = 345
        End If
        
    Else    ' Word 2016
                
        If Me.Width < 360 Then
            Me.Width = 620  ' Open options, retrieve settings from stored options file
        Else
            Me.Width = 352
        End If

    End If
    
End If


End Sub


Sub Retrieve_Options_from_OptionsFile()


Dim ctOptions As New SettingsFileClass

If Not ctOptions.Exists Then
    ctOptions.Create_Empty
End If


If ctOptions.IsEmpty = FileNotEmpty Then
 
    Dim strSaved_Assign_Section As Variant

    ' first we retrieve saved options from options file
    strSaved_Assign_Section = ctOptions.GetSection("AssignOptions", "ReturnArray")
    
    'Debug.Print strSaved_Assign_Section
    
    ' however, if we came empty handed, we simply write the defaults to file... it's the first time user opens options
    If Not IsArray(strSaved_Assign_Section) Then    ' return of array is success, otherwise it's an empty variant

WriteDefaults:
        Dim defaultFormOptions As Variant
        ' we gather relevant options names and values into string
        defaultFormOptions = ctOptions.txtGetFormOptions(Me, "ReturnArray")
        
        ' we write default settings to options file
        Call ctOptions.setSection("AssignOptions", defaultFormOptions)
                
    Else    ' Found options in options file, we need set form options controls to saved values
    
        'Call ctOptions.SetSection("AssignOptions", strSaved_Assign_Sections)
        Call Update_FormOptions_from_Array(strSaved_Assign_Section)
    
    End If
    
Else    ' options file exists, but sought section is empty)

    GoTo WriteDefaults

End If
    

End Sub
Function Get_Control_withTag(InputUserform As MSForms.UserForm, RequiredTag As String) As MSForms.control
    
    Dim ctControl As MSForms.control
    
    For Each ctControl In InputUserform.Controls
        If ctControl.Tag = RequiredTag Then
            Set Get_Control_withTag = ctControl
            Exit Function
        End If
    Next ctControl
    
'Debug.Print ""

End Function


Sub Update_FormOptions_from_Array(InputArray)

Dim ctControl As MSForms.control

Dim ctTextbox As MSForms.TextBox
Dim ctCombobox As MSForms.ComboBox
Dim ctOptionButton As MSForms.OptionButton

For i = 0 To UBound(InputArray, 2)
    
    Set ctControl = Get_Control_withTag(Me, CStr(InputArray(0, i)))
    
    If Not ctControl Is Nothing Then
    
        Select Case Left(ctControl.Name, 2)
            
            Case "tb"   ' textbox
                Set ctTextbox = ctControl
                ctTextbox.Value = InputArray(1, i)
                ' Exception for accent color code, it's a ... textbox colored in its own value!
                If InStr(1, ctControl.Name, "Color") > 0 Then ctTextbox.BackColor = ctTextbox.Value
            Case "ob"   ' option button
                Set ctOptionButton = ctControl
                ctOptionButton = InputArray(1, i)
            Case "cb"   ' combo box
                Set ctCombobox = ctControl
                ctCombobox = InputArray(1, i)
                
        End Select
        
    Else
        Debug.Print "Update_FormOptions_from_Array: Get_Control_withTag returned Nothing for tag " & _
            InputArray(0, i)
    End If
    
Next i


End Sub



Private Sub cbAstUsersList_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)



End Sub

Function InTheList(TargetComboBox As MSForms.ComboBox, SoughtValue As String) As Boolean

If TargetComboBox.listCount = 0 Then InTheList = False: Exit Function

For i = 0 To TargetComboBox.listCount - 1
    If TargetComboBox.List(i) = SoughtValue Then
        InTheList = True
        Exit Function
    End If
Next i

InTheList = False

End Function


Private Sub cbDateformat_Change()

Call Format_DeadlineDays
Me.Repaint

End Sub

Private Sub cbMailDateFormat_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

'If Me.cbMailDateFormat.BackColor = -2147483633 Then      ' inactive grey ("Button Face")
'
'    Debug.Print "cbMailDateFormat: Enabling..."
'
'    Me.cbMailDateFormat.BackColor = wdColorWhite
'    Me.cbMailDateFormat.ForeColor = wdColorBlack
'    'Me.tbMailDateFormat.SpecialEffect = fmSpecialEffectSunken
'    Me.chFreezeMailDateFormat.Value = True
'
'Else
'
'    If Me.cbMailDateFormat.Value <> "" Then
'
'        Me.cbMailDateFormat.Value = ""
'
'        'Debug.Print "cbMailDateFormat: Disabling..."
'
'        Me.cbMailDateFormat.BackColor = -2147483633
'        Me.cbMailDateFormat.ForeColor = wdColorGray50
'        Me.cbMailDateFormat.SpecialEffect = fmSpecialEffectEtched
'        Me.chFreezeMailDateFormat.Value = False
'
'    Else
'
'        Debug.Print "cbMailDateFormat: Empty, skipping disabling it..."
'
'    End If
'
'End If


End Sub

Private Sub chDeadlineManualInput_Click()

If Me.chDeadlineManualInput.Value = True Then
    Call UnlockDeadlines(False)
Else
    Call UnlockDeadlines(True)
End If

End Sub


Sub UnlockDeadlines(SetValue As Boolean)

Me.astDeadlineDay.Locked = SetValue
Me.astDeadlineTime.Locked = SetValue
Me.traDeadlineDay.Locked = SetValue
Me.traDeadlineTime.Locked = SetValue
Me.revDeadlineDay.Locked = SetValue
Me.revDeadlineTime.Locked = sevalue

Me.valDeadlineDay.Locked = SetValue
Me.valDeadlineTime.Locked = SetValue
Me.finDeadlineDay.Locked = SetValue
Me.finDeadlineTime.Locked = SetValue


End Sub

Private Sub chFreezeMailDateFormat_Click()

If Me.chFreezeMailDateFormat.Value = False Then
    
    Me.cbMailDateFormat.Enabled = False
    Me.cbMailDateFormat.BackColor = -2147483633     ' inactive grey ("Button Face")
    'Me.tbMailDateFormat.BackColor = wdColorWhite
    Me.cbMailDateFormat.ForeColor = wdColorGray50
    Me.cbMailDateFormat.SpecialEffect = fmSpecialEffectEtched

Else
        Me.cbMailDateFormat.Enabled = True
        Me.cbMailDateFormat.BackColor = wdColorWhite
        Me.cbMailDateFormat.ForeColor = wdColorBlack
        Me.cbMailDateFormat.SpecialEffect = fmSpecialEffectSunken
    
End If


End Sub

Private Sub chNoTradValidationDeadline_Click()

'If Me.chNoTradValidationDeadline.Value = True Then
'    Call DeadlineDeactivate_ProcessEvents(1, 0, valNoTimeLbl, valDeadlineTime, wdColorDarkRed, wdColorGray25)
'Else
'    Call DeadlineDeactivate_ProcessEvents(1, 0, valNoTimeLbl, valDeadlineTime, wdColorDarkRed, wdColorGray25)
'End If

End Sub

Private Sub chUseSendAs_Click()

If Me.chUseSendAs.Value = True Then
    
    Me.tbSendAsAddress.Enabled = True
    Me.tbSendAsAddress.BackColor = wdColorWhite
    Me.tbSendAsAddress.ForeColor = wdColorBlack
    Me.tbSendAsAddress.SpecialEffect = fmSpecialEffectSunken

Else
    
    Me.tbSendAsAddress.Enabled = False
    Me.tbSendAsAddress.BackColor = -2147483633      ' Gray "Button Face"
    Me.tbSendAsAddress.ForeColor = wdColorGray50
    Me.tbSendAsAddress.SpecialEffect = fmSpecialEffectEtched
    
End If


End Sub

Function GetCtUserAddress() As String

Dim olNS As Outlook.NameSpace
Set olNS = Outlook.GetNamespace("MAPI")

GetCtUserAddress = olNS.Accounts.Item(1).SmtpAddress

Set olNS = Nothing

End Function



Private Sub finDatePickerLbl_Click()

Call SyncMove_AllDeadlineDays_fromCalendar(Me.finDeadlineDay, Me.tbAccentColor.Value)

End Sub

Private Sub finDayPinLbl_Click()

Call Flip_PinnedState_ofLabel(Me.finDayPinLbl, wdColorDarkBlue)

End Sub


Private Sub finDeadlineDay_Enter()

finDeadlineDay.Tag = finDeadlineDay.Text
Call Activate_Textbox(finDeadlineDay.Name, wdColorRed)
finDeadlineDay.SelStart = 0

End Sub


Private Sub finDeadlineDay_Exit(ByVal Cancel As MSForms.ReturnBoolean)

Call ColorWarn_ScrolledInThePast(Me.finDeadlineDay)

Call Deactivate_Active_Textbox

End Sub

Private Sub finDeadlineDay_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

'Call Wind_Date(finDeadlineDay.Name, wdColorRed, KeyCode, Shift)
Call DeadlineDay_ProcessEvents(KeyCode, Shift, wdColorDarkBlue, "fin")

End Sub


Private Sub finDeadlineDay_MouseMove(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)


If Not Me.finDatePickerLbl.ForeColor = wdColorGray25 Then

    finDatePickerLbl.ForeColor = DayButtonsColor
    finDayPinLbl.ForeColor = DayButtonsColor
    
End If


End Sub


Private Sub finDeadlineTime_Enter()

Call Activate_Textbox(finDeadlineTime.Name, wdColorRed)
finDeadlineTime.SelStart = 0

End Sub

Private Sub finDeadlineTime_Exit(ByVal Cancel As MSForms.ReturnBoolean)

Call Deactivate_Active_Textbox

End Sub

Private Sub finDeadlineTime_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

'If KeyCode = vbKeyPageDown Or KeyCode = vbKeyPageUp Or _
'    KeyCode = vbKeyHome Or KeyCode = vbKeyEnd Then
'    Call Wind_Time(KeyCode)
'End If
Call DeadlineTime_ProcessEvents(KeyCode, Shift, "fin", wdColorDarkBlue, wdColorDarkRed)

End Sub


Private Sub finDeadlineTime_MouseMove(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Not Me.finNoTimeLbl.ForeColor = TimeButtonsColor Then

    finNoTimeLbl.ForeColor = TimeButtonsColor
    finTimePinLbl.ForeColor = TimeButtonsColor
    finStartWorkdayLbl.ForeColor = TimeButtonsColor
    finEndWorkdayLbl.ForeColor = TimeButtonsColor
    
    'finTimeLabel.ForeColor = wdColorGray50
    
End If

End Sub


Private Sub finEndWorkdayLbl_Click()

Call DeadlineTime_JumpTo("EOD", finEndWorkdayLbl)

End Sub

Private Sub finNoTime_Click()

If Me.finDeadlineTime.Enabled = True Then
    Me.finDeadlineTime.Enabled = False
Else
    Me.finDeadlineTime.Enabled = True
End If


End Sub

Private Sub lblNoTimeRev_Click()

If Me.revDeadlineTime.Enabled = True Then
    Me.revDeadlineTime.Enabled = False
Else
    Me.revDeadlineTime.Enabled = True
End If


End Sub



Private Sub lblAllTimes_Click()

Call EnableAllTimes

End Sub

Sub EnableAllTimes()

' Enable all deadline times
If Me.astCbo <> "" Then Me.astDeadlineTime.Enabled = True
If Me.traCbo <> "" Then Me.traDeadlineTime.Enabled = True
If Me.revCbo <> "" Then Me.revDeadlineTime.Enabled = True
Me.valDeadlineTime.Enabled = True
'Me.finDeadlineTime.Enabled = True

End Sub






Private Sub finNoTimeLbl_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Call DeadlineDeactivate_ProcessEvents(Button, Shift, finNoTimeLbl, finDeadlineTime, wdColorDarkRed, TimeButtonsColor)

End Sub

Private Sub finNoTimeLbl_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 1 Then
    finNoTimeLbl.ForeColor = TimeButtonsColor
End If

End Sub

Private Sub finStartWorkdayLbl_Click()

Call DeadlineTime_JumpTo("SOD", Me.finDeadlineTime)

End Sub

Private Sub finTimePinLbl_Click()

Call Flip_PinnedState_ofLabel(finTimePinLbl, wdColorDarkBlue)

End Sub




Private Sub lblEndWorkdayTimes_Click()

Call Deadlines_JumpTo_Time("EOD")

End Sub


Sub Deadlines_JumpTo_Time(WhichTime As String)


Dim ctEODTime As String

If WhichTime = "EOD" Or LCase(WhichTime) = "end" Then
    ctEODTime = Me.tbEndOfDayTime
ElseIf WhichTime = "SOD" Or LCase(WhichTime) = "start" Then
    ctEODTime = Me.tbStartOfDayTime     ' hard coded for now? Option, anyone?
ElseIf WhichTime = "MOD" Or LCase(WhichTime) = "middle" Then     ' MOD - middle of day
    ctEODTime = "12:00"
End If


'If ctEODTime = "" Then ctEODTime = "17:30"  ' fallback on default

'If Me.astCbo <> "" Then Me.astDeadlineTime = ctEODTime
If Me.astDeadlineTime.Enabled Then Me.astDeadlineTime = ctEODTime
'If Me.traCbo <> "" Then Me.traDeadlineTime = ctEODTime
If Me.traDeadlineTime.Enabled Then Me.traDeadlineTime = ctEODTime
'If Me.revCbo <> "" Then Me.revDeadlineTime = ctEODTime
If Me.revDeadlineTime.Enabled Then Me.revDeadlineTime = ctEODTime
Me.valDeadlineTime = ctEODTime
'Me.finDeadlineTime = ctEODTime

End Sub




Sub DisableAllTimes()

    ' Disable all times
    Me.astDeadlineTime.Enabled = False
    Me.traDeadlineTime.Enabled = False
    Me.revDeadlineTime.Enabled = False
    Me.valDeadlineTime.Enabled = False
    ' NOT THE final deadline time, dude! Not it!
    'Me.finDeadlineTime.Enabled = False

End Sub


Private Sub lblEndWorkdayTimes_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.lblEndWorkdayTimes.ForeColor = wdColorGreen

End Sub


Private Sub lblEndWorkdayTimes_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.lblEndWorkdayTimes.ForeColor = wdColorGray25

End Sub


Private Sub lblMailMode1_Click()

Call Toggle_MailMode

End Sub


Sub Toggle_MailMode()


If Me.lblMailMode2.Visible Then     ' switch to single
        
    Call MailMode_Single_Activate

Else    ' switch to Multiple
    
    Call MailMode_Multiple_Activate

End If

End Sub

Sub MailMode_Single_Activate()
    
    If Me.lblMailMode2.Visible Then
    
        Me.lblMailMode1.Left = Me.lblMailMode1.Left - 12
        Me.lblMailMode2.Visible = False
        Me.lblMailMode3.Visible = False
        Me.lblMailMode1.ControlTipText = "Mail mode: One message with multiple destinators"
    
    End If

End Sub

Sub MailMode_Multiple_Activate()
    
    ' At design time, multiple mode is set... setting it double time does not work !
    If Me.lblMailMode2.Visible = False Then
    
        Me.lblMailMode1.Left = Me.lblMailMode1.Left + 12
        Me.lblMailMode2.Visible = True
        Me.lblMailMode3.Visible = True
        Me.lblMailMode1.ControlTipText = "Mail mode: Multiple messages each with one destinator"
        
    End If
    
End Sub


Private Sub lblMailMode2_Click()

Call Toggle_MailMode

End Sub



Private Sub lblNoTimes_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

' If left-click, disable all deadlines' times
If Button = 1 Then  ' Left button
    
    Me.lblNoTimes.ForeColor = wdColorDarkRed
    
    ' Enable or disable all times on form, on the spot
    If Me.tgNoTimes.Value = False Then
        
        If Found_One_EnabledTime Then
        
            Call DisableAllTimes
            
        Else    ' found NO enabled time, enabling all (performing "toggle")
            
            Call EnableAllTimes
            
        End If
        
    End If
    
       
ElseIf Button = 2 Then  ' Right button
    
    ' Graphically signal pinned state and disable accordingly
    'Call Flip_PinnedState_ofLabel(lblNoTimes, wdColorDarkRed)
    
    ' Is it the right one to call ?
    Call Flip_NoDeadline_Times
    
    
End If


End Sub


Sub Flip_NoDeadline_Times()


' if we're in normal mode, unpinned
'If Me.lblNoTimes.BackStyle = fmBackStyleTransparent Then
If Me.tgNoTimes.Value = False Then
    
    ' Signal Disabling of all times (until user reverts this) - a feature called "pinning" a state
    Call Set_NoTimes_PinnedState(True)
                    
    ' Disable all times textboxes on form
    Call DisableAllTimes
    
    ' also set hidden option in options part on form, so as to be active imm
    Me.tgNoTimes.Value = True
    
    ' set proper option to on in options file
    Call Set_NoTimes_Option(True)
    
Else    ' we're already pinned, unpin it
    
    ' Remove pinning, revert to default behaviour
    Call Set_NoTimes_PinnedState(False)
    
    Me.tgNoTimes.Value = False
    
    ' set proper option to off
    Call Set_NoTimes_Option(False)
    
End If


End Sub


' /**
' * Returns true if one or more enabled operation sections' "deadline times" textboxes on frmAssign are Enabled, false in none.
' */

Function Found_One_EnabledTime() As Boolean

' test each operation in turn, we need only ONE enabled operation's deadline time tb to be enabled so as to return true
If Me.astCbo.Text <> "" Then
    
    If Me.astDeadlineTime.Enabled Then
        Found_One_EnabledTime = True
        Exit Function
    End If
    
End If
    
If Me.traCbo.Text <> "" Then
    
    If Me.traDeadlineTime.Enabled Then
        Found_One_EnabledTime = True
        Exit Function
    End If
    
End If

If Me.revCbo.Text <> "" Then
    If Me.revDeadlineTime.Enabled Then
        Found_One_EnabledTime = True
        Exit Function
    End If
End If

If Me.valDeadlineTime.Enabled Then
    Found_One_EnabledTime = True
    Exit Function
End If

' DOESNT COUNT FOR "IS ENABLED" verification!
'If Me.finDeadlineTime.Enabled Then
    'Found_One_EnabledTime = True
'End If



End Function


Sub Set_NoTimes_Option(SetState As Boolean)


Dim ctSettingsFile As New SettingsFileClass
Dim svCtNoTimes As String

svCtNoTimes = ctSettingsFile.getOption("AssignOptions", Me.lblNoTimes.Tag)

If svCtNoTimes <> "" Then
    ' only set option if retrieved option is different than desired one
    If svCtNoTimes <> CStr(SetState) Then
        ' set option to true, deactivate all deadlines until user changes mind
        ctSettingsFile.setOption "AssignOptions", Me.lblNoTimes.Tag, CStr(SetState)
    End If
Else    ' retrieved option is empty... user never used it!
    ' set option to true, deactivate all deadlines until user changes mind
    ctSettingsFile.setOption "AssignOptions", Me.lblNoTimes.Tag, CStr(SetState)
End If


Set ctSettingsFile = Nothing

End Sub


Sub Set_PinnedState_ofLabel(LabelCtl As MSForms.label, SetPinned As Boolean, ForeColor As WdColor)

' Pinned sets the no times to disabled until user reverts the option
'(we're using settings file to do it)

If SetPinned = True Then

    'LabelCtl.BackStyle = fmBackStyleOpaque
    'LabelCtl.BackColor = wdColorBlack   ' black
    LabelCtl.ForeColor = ForeColor
    
Else

    'LabelCtl.BackStyle = fmBackStyleTransparent
    'LabelCtl.BackColor = wdColorBlack   ' BLACK
    LabelCtl.ForeColor = TimeButtonsColor

End If


End Sub

Sub Set_NoTimes_PinnedState(SetPinned As Boolean)
' Pinned sets the no times to disabled until user reverts the option
'(we're using settings file to do it)

If SetPinned = True Then

    'Me.lblNoTimes.BackStyle = fmBackStyleOpaque
    Me.lblNoTimes.ForeColor = wdColorDarkRed

Else

    'Me.lblNoTimes.BackStyle = fmBackStyleTransparent
    Me.lblNoTimes.ForeColor = wdColorGray25

End If

End Sub


Private Sub lblNoTimes_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 1 Then
    Me.lblNoTimes.ForeColor = wdColorGray25
End If

End Sub


Private Sub lblPinForm_Click()

Call Flip_PinnedState_ofLabel(Me.lblPinForm, wdColorDarkBlue)

End Sub


Sub Flip_PinnedState_ofLabel(TargetLabel As MSForms.label, ForeColor As WdColor)

If TargetLabel.ForeColor <> ForeColor Then
    Call Set_PinnedState_ofLabel(TargetLabel, True, ForeColor)
Else
    Call Set_PinnedState_ofLabel(TargetLabel, False, ForeColor)
End If

End Sub


Private Sub lblStartWorkdayTimes_Click()

Call Deadlines_JumpTo_Time("SOD")   ' start of day, hardcoded for 9:00 for now

End Sub


Private Sub lblStartWorkdayTimes_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 1 Then
    Me.lblStartWorkdayTimes.ForeColor = wdColorGreen
End If

End Sub

Private Sub lblStartWorkdayTimes_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 1 Then
    Me.lblStartWorkdayTimes.ForeColor = wdColorGray25
End If

End Sub


Private Sub lvwKeysCombos_Click()

Me.tbCaptureNewKey.SetFocus

End Sub

Private Sub lvwKeysCombos_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As stdole.OLE_XPOS_PIXELS, ByVal Y As stdole.OLE_YPOS_PIXELS)

Call Summon_KeysCapture_Box(Button, Shift, X, Y)

End Sub

Sub Dismiss_KeysCapture_Box()

Me.skipFrmCaptureKeys.Top = 265

End Sub

Sub Summon_KeysCapture_Box(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As stdole.OLE_XPOS_PIXELS, Y As stdole.OLE_YPOS_PIXELS)


Const lvwKCRowHeight As Single = 12.75
Const lvwHeaderHeightOffset As Single = 14.5
Const lvwListViewTopOffset As Single = 40.5

Debug.Print "Mouse top = " & 0.75 * Y & " points"

Me.skipFrmCaptureKeys.Left = Me.lvwKeysCombos.ColumnHeaders(2).Left + 3

Dim selectedRowsNumber As Single
' 16.25 is top offset of first element in list. 14.25 is height of each element in list
selectedRowsNumber = (0.75 * Y - lvwHeaderHeightOffset) / lvwKCRowHeight     '1 pixel is 0.75 points, function PixelToPoints creates problems!
' we have no "top" function! Correct for this!
selectedRowsNumber = IIf(selectedRowsNumber > Int(selectedRowsNumber), _
    Int(selectedRowsNumber) + 1, Int(selectedRowsNumber))

Debug.Print "selectedRowsNumber = " & selectedRowsNumber

Dim selectedRowsTop As Single
selectedRowsTop = lvwHeaderHeightOffset + lvwListViewTopOffset + (selectedRowsNumber - 1) * lvwKCRowHeight

Debug.Print "selectedRowsTop = " & selectedRowsTop

Me.skipFrmCaptureKeys.Top = selectedRowsTop


End Sub


Function Active_KeysCapture_Box() As Boolean

If Me.tbCaptureNewKey.Top < 250 Then
    Active_KeysCapture_Box = True
Else
    Active_KeysCapture_Box = False
End If

End Function





Private Sub lvwKeysCombos_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As stdole.OLE_XPOS_PIXELS, ByVal Y As stdole.OLE_YPOS_PIXELS)

Me.tbCaptureNewKey.SetFocus

End Sub

Private Sub obTimeUseDTimes_Click()

Call Format_DeadlineTimes

End Sub

Private Sub obTimeUseEndOfDay_Click()

Call Format_DeadlineTimes

End Sub

Private Sub obTimeUseStartOfDay_Click()

' update deadline times values in real time on form
Call Format_DeadlineTimes

End Sub

Private Sub revCbo_Enter()

' 12640511  revision pink
Call Activate_Section(EnabledState:=True, SectionName:="rev", ColorCode:=Me.RevAccentColor)

End Sub

Private Sub revCbo_Exit(ByVal Cancel As MSForms.ReturnBoolean)

If revCbo.Text = "" Then
    ' 14737632   grey
    
    ' only disable revision section if user chose to
    If Me.chAllowNoMansRevision.Value = "False" Then
        Call Activate_Section(EnabledState:=False, SectionName:="rev", ColorCode:=14737632)
    End If
    
Else
    
    ' correct for accidental input of NP characters
    Dim cleanedRevCbo As String
    cleanedRevCbo = Clean_NPCharacters(Me.revCbo.Text)
    
    If cleanedRevCbo = "" Then
        
        Me.revCbo.Text = ""
        Call Activate_Section(EnabledState:=False, SectionName:="rev", ColorCode:=14737632)
    
    Else
    
        If Me.traCbo.Text <> "" And Me.traCbo.Text = Me.revCbo.Text Then
            
            If Me.TraRevAccentColor.Value <> wdColorBlack Then
                
                Me.revColorLabel1.BackColor = Me.TraRevAccentColor
                Me.revColorLabel2.BackColor = Me.TraRevAccentColor
                Me.revCbo.BackColor = Me.TraRevAccentColor
                
                Me.traColorLabel1.BackColor = Me.TraRevAccentColor
                Me.traColorLabel2.BackColor = Me.TraRevAccentColor
                Me.traCbo.BackColor = Me.TraRevAccentColor
                
            End If
            
        End If
    
    End If
    
    Me.revAssignLabel.ForeColor = -2147483633
    Me.revMultiAssignLabel.ForeColor = -2147483633
    
End If

End Sub


Private Sub revCbo_MouseMove(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If revCbo.Text <> "" Then
    If Not revAssignLabel.ForeColor = wdColorGray50 Then
        revAssignLabel.ForeColor = wdColorGray50
        revMultiAssignLabel.ForeColor = wdColorGray50
    End If
End If

End Sub


Private Sub revColorLabel1_Click()

Call Activate_Section(EnabledState:=True, SectionName:="rev", ColorCode:=Me.RevAccentColor)
If Me.revCbo.Enabled Then Me.revCbo.SetFocus

End Sub

Private Sub revColorLabel2_Click()

Me.revCbo.Text = ""
Call Activate_Section(EnabledState:=False, SectionName:="rev", ColorCode:=14737632)

End Sub

Private Sub revDatePickerLbl_Click()

'Dim ctTxtb As MSForms.ComboBox
'Set ctTxtb = Me.revCbo
'
'Dim ctDTxtb As MSForms.TextBox
'Set ctDTxtb = Me.revDeadlineDay

Call SyncMove_AllDeadlineDays_fromCalendar(Me.revDeadlineDay, Me.tbAccentColor.Value, Me.revCbo)

End Sub

Private Sub revDayPinLbl_Click()

If revDeadlineTime.Enabled Then
    Call Flip_PinnedState_ofLabel(revDayPinLbl, wdColorDarkBlue)
End If

End Sub

Private Sub revDeadlineDay_MouseMove(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If isSectionEnabled("rev") Then
    
    If Not Me.revDatePickerLbl.ForeColor = DayButtonsColor Then
    
        revDatePickerLbl.ForeColor = DayButtonsColor
        revDayPinLbl.ForeColor = DayButtonsColor
        
        revDDayLabel.ForeColor = wdColorGray50
        revDayLabel.ForeColor = wdColorGray50
        
    End If
    
End If

End Sub

Private Sub revDeadlineTime_MouseMove(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If isSectionEnabled("rev") Then

    If Not Me.revNoTimeLbl.ForeColor = TimeButtonsColor Then
    
        revNoTimeLbl.ForeColor = TimeButtonsColor
        revTimePinLbl.ForeColor = TimeButtonsColor
        revStartWorkdayLbl.ForeColor = TimeButtonsColor
        revEndWorkdayLbl.ForeColor = TimeButtonsColor
        
        revTimeLabel.ForeColor = wdColorGray50
        
    End If

End If

End Sub

Private Sub revEndWorkdayLbl_Click()

If revDeadlineTime.Enabled Then
    Call DeadlineTime_JumpTo("EOD", revEndWorkdayLbl)
End If

End Sub

Private Sub revEndWorkdayLbl_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If revDeadlineTime.Enabled Then
    revEndWorkdayLbl.ForeColor = wdColorGreen
End If

End Sub

Private Sub revEndWorkdayLbl_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If revDeadlineTime.Enabled Then
    revEndWorkdayLbl.ForeColor = TimeButtonsColor
End If

End Sub

Private Sub revMultiAssignLabel_Click()

If Me.lbMultipleAssignments.Left > 500 Then
    Call Summon_MultiAssignment("rev")
Else
    Call Dismiss_MultiAssignment
End If

End Sub


Private Sub revNoTimeLbl_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

'If Button = 1 Then
'    revNoTimeLbl.ForeColor = wdColorDarkRed
'End If
'
'Call DeadlineTime_Deactivate(Button, Shift, revNoTimeLbl, revDeadlineTime, wdColorDarkRed)

'If revDeadlineTime.Enabled Then
    Call DeadlineDeactivate_ProcessEvents(Button, Shift, revNoTimeLbl, revDeadlineTime, wdColorDarkRed, TimeButtonsColor)
'End If

End Sub

Private Sub revNoTimeLbl_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

'If astDeadlineTime.Enabled Then
    If Button = 1 Then
        revNoTimeLbl.ForeColor = TimeButtonsColor
    End If
'End If

End Sub

Private Sub revStartWorkdayLbl_Click()

If revDeadlineTime.Enabled Then
    Call DeadlineTime_JumpTo("SOD", Me.revDeadlineTime)
End If

End Sub

Private Sub revStartWorkdayLbl_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If revDeadlineTime.Enabled Then
    revStartWorkdayLbl.ForeColor = wdColorGreen
End If

End Sub

Private Sub revStartWorkdayLbl_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If revDeadlineTime.Enabled Then
    revStartWorkdayLbl.ForeColor = TimeButtonsColor
End If

End Sub

Private Sub revTimePinLbl_Click()

If revDeadlineTime.Enabled Then
    Call Flip_PinnedState_ofLabel(revTimePinLbl, wdColorDarkBlue)
End If

End Sub

Private Sub sbMailMode_SpinDown()

If Me.tbMailMode = "One Msg" Then
    Me.tbMailMode = "Mult Msg"
Else
    Me.tbMailMode = "One Msg"
End If

End Sub

Private Sub sbMailMode_SpinUp()

If Me.tbMailMode = "One Msg" Then
    Me.tbMailMode = "Mult Msg"
Else
    Me.tbMailMode = "One Msg"
End If

End Sub

Private Sub sbMinsScroll_SpinDown()
' Decrease timer interval value

If CInt(Me.tbMinsScroll) > 5 Then
    Me.tbMinsScroll = CInt(Me.tbMinsScroll) - 5
ElseIf CInt(Me.tbMinsScroll) >= 1 Then
    Me.tbMinsScroll = CInt(Me.tbMinsScroll) - 1
Else
    Me.tbMinsScroll = 0
End If

End Sub

Private Sub sbMinsScroll_SpinUp()
' Increase timer value

If Me.tbMinsScroll <= 55 Then
    Me.tbMinsScroll = CInt(Me.tbMinsScroll) + 5
End If


End Sub


Private Sub skipButOptionsSave_Click()

Dim ctOptionsFile As New SettingsFileClass

If ctOptionsFile.IsEmpty = ErrorFindingFile Then
    ctOptionsFile.Create_Empty
End If

Dim ctFormOptions As Variant

ctFormOptions = ctOptionsFile.txtGetFormOptions(Me, "ReturnArray")
ctOptionsFile.setSection "AssignOptions", ctFormOptions


Set ctOptionsFile = Nothing

End Sub



Private Sub skipLblKeyCombosRevert_Click()

Call Initialize_KShortcuts_from_Defaults
Me.Repaint

End Sub


Private Sub skipLblKeyCombosRevert_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.skipLblKeyCombosRevert.SpecialEffect = fmSpecialEffectSunken

End Sub


Private Sub skipLblKeyCombosRevert_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.skipLblKeyCombosRevert.SpecialEffect = fmSpecialEffectFlat

End Sub


Private Sub skipLblOptionsSave_Click()


Dim ctOptionsFile As New SettingsFileClass

If ctOptionsFile.IsEmpty = ErrorFindingFile Then
    ctOptionsFile.Create_Empty
End If

Dim ctFormOptions As Variant

ctFormOptions = ctOptionsFile.txtGetFormOptions(Me, "ReturnArray")
ctOptionsFile.setSection "AssignOptions", ctFormOptions

Set ctOptionsFile = Nothing


End Sub


Private Sub skipLblOptionsSave2_Click()

Dim ctOptionsFile As New SettingsFileClass

If ctOptionsFile.IsEmpty = ErrorFindingFile Then
    ctOptionsFile.Create_Empty
End If

Dim ctFormOptions As Variant

ctFormOptions = ctOptionsFile.txtGetFormOptions(Me, "ReturnArray")
ctOptionsFile.setSection "AssignOptions", ctFormOptions

Me.Repaint

Set ctOptionsFile = Nothing

End Sub

Private Sub skipLblOptionsSave2_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.skipLblOptionsSave2.SpecialEffect = fmSpecialEffectSunken

End Sub


Private Sub skipLblOptionsSave2_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.skipLblOptionsSave2.SpecialEffect = fmSpecialEffectFlat

End Sub

Private Sub skipLblOptionsSave_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.skipLblOptionsSave.SpecialEffect = fmSpecialEffectSunken

End Sub


Private Sub skipLblOptionsSave_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.skipLblOptionsSave.SpecialEffect = fmSpecialEffectFlat

End Sub

Private Sub skipRevertToDefaults_Click()

Call Blank_Current_AssignOptions

End Sub


Private Sub skipRevertToDefaults_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.skipRevertToDefaults.SpecialEffect = fmSpecialEffectSunken

End Sub


Private Sub skipRevertToDefaults_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.skipRevertToDefaults.SpecialEffect = fmSpecialEffectFlat

End Sub



Sub Blank_Current_AssignOptions()


Dim ctOpts As New SettingsFileClass

If ctOpts.IsEmpty = FileNotEmpty Then
    
    Dim ctAssSect As String
    
    ctAssSect = ctOpts.GetSection("AssignOptions")
    
    If ctAssSect <> "" Then
        
        Dim ctOptionsNames As String
        ctOptionsNames = ctOpts.txtGetFormOptions(Me)
        
        Dim blankedOptions As String
        blankedOptions = BlankOptions_InString(ctOptionsNames)
        
        Dim blankOptsValues As String
        blankOptsValues = String(UBound(Split(blankedOptions, "|")), "|")
        
        ctOpts.setOption "AssignOptions", blankedOptions, blankOptsValues
        
        ' and reload !
        Me.Hide
        If Me.Width > 310 Then MouseWheelMod.StartWidenedForm = True
        Unload Me
        Load frmAssign
        frmAssign.Show
        
    End If
    
End If

End Sub


Function BlankOptions_InString(InputString As String) As String

Dim helperArr As Variant

helperArr = Split(InputString, vbCr)

If UBound(helperArr) <= 0 Then
    BlankOptions_InString = ""
    Exit Function
End If

Dim tempNames As String

For j = 0 To UBound(helperArr)
    If helperArr(j) <> "" Then
        
        ctname = Split(helperArr(j), "=")(0)
        
        If ctname <> "" Then
        
            tempNames = IIf(tempNames = "", ctname, tempNames & "|" & ctname)
        
        End If
        
    End If
Next j

BlankOptions_InString = tempNames


End Function


Private Sub tbAccentColor_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Dim chColor As Long

chColor = ColorPicker(frmHwnd)

If chColor <> 0 Then
    tbAccentColor.BackColor = chColor
    tbAccentColor.Value = CStr(chColor)

    Me.lblClockBack.BackColor = tbAccentColor.BackColor
    Me.lblClockLine.BackColor = tbAccentColor.BackColor
    Me.lblValidationLine.BackColor = tbAccentColor.BackColor
    Me.lblValidationVertLine.BackColor = tbAccentColor.BackColor
    Me.lblOptionsLine.BackColor = tbAccentColor.BackColor
    Me.skipRevertToDefaults.ForeColor = tbAccentColor.BackColor

End If


End Sub


Private Sub tbAdInitList_Change()

Call Initialize_UsersComboboxes("ad")

End Sub

Private Sub tbAdUsersList_Change()

Call Initialize_UsersComboboxes("ad")

End Sub

Private Sub tbAstInitList_Change()

Call Initialize_UsersComboboxes("ast")   ' update ast users

End Sub

Private Sub tbAstUsersList_Change()

Call Initialize_UsersComboboxes("ast")

End Sub



Private Sub tbAutoScroll_JumpToWhat_DblClick(ByVal Cancel As MSForms.ReturnBoolean)

If Me.tbAutoScroll_JumpToWhat.Value = "start" Then
    Me.tbAutoScroll_JumpToWhat.Value = "middle"
    'Me.chAutoScroll_ToNextDay_Time.Tag = Replace(Me.chAutoScroll_ToNextDay_Time.Tag, "start", "middle")
Else
    Me.tbAutoScroll_JumpToWhat.Value = "start"
    'Me.chAutoScroll_ToNextDay_Time.Tag = Replace(Me.chAutoScroll_ToNextDay_Time.Tag, "middle", "start")
End If

End Sub



Private Sub tbCaptureNewKey_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

If KeyCode = vbKeyTab Then
    Call Dismiss_KeysCapture_Box

Else    ' other keys, capture code and dismiss "floating" textbox
    
    Dim kIdx As Integer
    kIdx = Get_Index_ofBDArray_Entry(vbKeyNamesValues, 2, CStr(KeyCode))
    
    ' if user chose one key only, process and dismiss textbox
    If Shift = 0 Then
        ' key capture textbox is empty
        If Me.tbCaptureNewKey.Text = "" Then
            ' human usage key names, "vbKey" bothers
            Me.lvwKeysCombos.selectedItem.ListSubItems(1).Text = Replace(vbKeyNamesValues(0, kIdx), "vbKey", "")
            Call Dismiss_KeysCapture_Box
        Else    ' key capture textbox contains a modifier key, it's not empty. Need combine with new key
            Me.lvwKeysCombos.selectedItem.ListSubItems(1).Text = Me.tbCaptureNewKey.Text & _
                Replace(vbKeyNamesValues(0, kIdx), "vbKey", "")
            Call Dismiss_KeysCapture_Box
        End If
    Else    ' but stay in place otherwise to wait for the other key
        Me.tbCaptureNewKey.Text = Replace(vbKeyNamesValues(0, kIdx), "vbKey", "") & "+"
    End If
    
End If

End Sub



Private Sub tbCaptureNewKey_KeyUp(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

If Me.skipFrmCaptureKeys.Top > 200 Then
    Me.tbCaptureNewKey.Text = ""
End If

End Sub

Private Sub tbEndOfDayTime_Change()

If Me.obTimeUseEndOfDay And InStr(1, tbEndOfDayTime.Text, ":") > 0 Then
    If UBound(Split(tbEndOfDayTime.Text, ":")) = 1 And Len(Split(tbEndOfDayTime, ":")(0)) = 2 And _
    Len(Split(tbEndOfDayTime, ":")(1)) = 2 Then
        Call Format_DeadlineTimes
    End If
End If

End Sub

Private Sub tbPrepMsg_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 2 Then
    
    Dim chosenColor As Long
    
    chosenColor = ColorChooseMod.ColorPicker(frmHwnd)
    
    Me.tbPrepMsg.BackColor = chosenColor    ' visually, for user to know/ check
    
    Me.PrepAccentColor.Value = chosenColor
    
    ' color corresp section if it's active straight away
    If astCbo.Text <> "" Then
        Me.astColorLabel1.BackColor = chosenColor
        Me.astColorLabel2.BackColor = chosenColor
        Me.astCbo.BackColor = chosenColor
    End If
    
End If


End Sub


Private Sub tbRevMsg_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 2 Then
    
    Dim chosenColor As Long
    
    chosenColor = ColorChooseMod.ColorPicker(frmHwnd)
    
    Me.tbRevMsg.BackColor = chosenColor    ' visually, for user to know/ check
    
    Me.RevAccentColor.Value = chosenColor
    
    ' color corresp section if it's active straight away
    If revCbo.Text <> "" Then
        Me.revColorLabel1.BackColor = chosenColor
        Me.revColorLabel2.BackColor = chosenColor
        Me.revCbo.BackColor = chosenColor
    End If
    
End If


End Sub

Private Sub tbSendAsAddress_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

'Debug.Print "MouseDown: Button code = " & Button & ", Shift code = " & Shift
'
'If Me.tbSendAsAddress.BackColor = -2147483633 Then      ' inactive grey ("Button Face")
'    'Me.tbSendAsAddress.BackColor = wdColorWhite
'    'Me.tbSendAsAddress.ForeColor = wdColorBlack
'    Me.tbSendAsAddress.SpecialEffect = fmSpecialEffectSunken
'Else
'    'Me.tbSendAsAddress.BackColor = -2147483633
'    'Me.tbSendAsAddress.ForeColor = wdcolorgray50
'    Me.tbSendAsAddress.SpecialEffect = fmSpecialEffectEtched
'End If


End Sub




Private Sub tbTimeSeparation_Change()

If Me.obTimeUseDTimes Then
    
    If tbTimeSeparation.Text <> "" Then
        If InStr(1, tbTimeSeparation.Text, ",") > 0 Then
            If UBound(Split(tbTimeSeparation.Text, ",")) = 3 Then
                If Len(Split(tbTimeSeparation.Text, ",")(0)) > 0 And _
                    Len(Split(tbTimeSeparation.Text, ",")(1)) > 0 And _
                    Len(Split(tbTimeSeparation.Text, ",")(2)) > 0 And _
                    Len(Split(tbTimeSeparation.Text, ",")(3)) > 0 Then
                    
                    Call Format_DeadlineTimes
                    
                End If
            End If
        End If
    End If
    
End If

End Sub

Private Sub tbTraMsg_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 2 Then
    
    Dim chosenColor As Long
    
    chosenColor = ColorChooseMod.ColorPicker(frmHwnd)
    
    Me.tbTraMsg.BackColor = chosenColor    ' visually, for user to know/ check
    
    Me.TraAccentColor.Value = chosenColor     ' and technically, for programming purposes
    
    ' color corresp section if it's active straight away
    If traCbo.Text <> "" Then
        Me.traColorLabel1.BackColor = chosenColor
        Me.traColorLabel2.BackColor = chosenColor
        Me.traCbo.BackColor = chosenColor
    End If
    
End If


End Sub


Private Sub tbTraRevMsg_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 2 Then
    
    Dim chosenColor As Long
    
    chosenColor = ColorChooseMod.ColorPicker(frmHwnd)
    
    Me.tbTraRevMsg.BackColor = chosenColor    ' visually, for user to know/ check
    
    Me.TraRevAccentColor.Value = chosenColor
        
End If


End Sub


Private Sub TextBox1_Change()



End Sub

Private Sub TextBox1_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)


Debug.Print KeyCode
TextBox1.Text = ""


End Sub

Private Sub traCbo_Click()

    'Debug.Print "traCboClick: Hwnd = " & LocalHwnd & "; PrevHwnd = " & LocalPrevWndProc & "; ActiveTextboxName = " & ActiveTextboxName
    'Debug.Print "traCboClick: Form WndProc Adress now: " & GetWindowLong(LocalHwnd, GWL_WNDPROC)
    'Debug.Print "cobAstClick: MatchFound = " & Me.astCbo.MatchFound
    
    ' Debug Prints its own success or failure !
    
'    If Shift = 2 Then   ' Control key
'
'        If Me.traCbo.Text = "" Then
'            Me.traCbo.Text = Me.traCbo.SelText
'            Me.traCbo.DropDown
'        Else
'            Me.traCbo.Text = Me.traCbo.Text & "," & Me.traCbo.List(Me.traCbo.ListIndex)
'            Me.traCbo.DropDown
'        End If
'
'    End If

    
    Me.revCbo.SetFocus

End Sub


Private Sub revCbo_Click()

    'MsgBox "astCbo Clicked !"
    Debug.Print "revCboClick: Hwnd = " & LocalHwnd & "; PrevHwnd = " & LocalPrevWndProc & "; ActiveTextboxName = " & ActiveTextboxName
    Debug.Print "revCboClick: Form WndProc Adress now: " & GetWindowLong(LocalHwnd, GWL_WNDPROC)
    'Debug.Print "cobAstClick: MatchFound = " & Me.astCbo.MatchFound
    
    ' Debug Prints its own success or failure !
    'Call QuickFocusSwitch
    If Not astDeadlineDay.Enabled = False Then
        Me.astDeadlineDay.SetFocus
    Else
        If Not traDeadlineDay.Enabled = False Then
            Me.traDeadlineDay.SetFocus
        Else
            Me.revDeadlineDay.SetFocus
        End If
    End If
    
End Sub


Private Sub revDeadlineDay_Click()
    
    Call Activate_Label(revDeadlineDay.Name, wdColorRed)

End Sub




Private Sub lblRealTime_Click()

Dim ret As Long

ret = KillTimer(frmHwnd, TimerID)
'Call EndTimer
Debug.Print "RealTime Click: TimerID = " & MouseWheelMod.TimerID & " KillTimer returned " & ret

End Sub

Private Sub revDeadlineDay_Enter()
    
    Me.revDeadlineDay.Tag = Me.revDeadlineDay.Text
    Call Activate_Textbox(revDeadlineDay.Name, wdColorRed)
    revDeadlineDay.SelStart = 0

End Sub


Private Sub revDeadlineDay_Exit(ByVal Cancel As MSForms.ReturnBoolean)

If Me.revDeadlineDay.Text <> "" Then
    Call ColorWarn_ScrolledInThePast(Me.revDeadlineDay)

    If CDate(Me.revDeadlineDay.Text) <> CDate(Me.revDeadlineDay.Tag) Then
        'Call Wind_Date(Me.Controls(SectionPrefix & "DeadlineDay").Name, wdColorRed, KeyCode, Shift)
        
        ' EXPERIMENT... Does it work ?
        timeint = DateDiff("d", CDate(Me.revDeadlineDay.Tag), CDate(Me.revDeadlineDay.Text))
        
        Call Wind_All_DateTime_AwayFrom("Date", Me.revDeadlineDay.Name, CInt(timeint), timeint * 120, IIf(timeint > 0, vbKeyPageUp, vbKeyPageDown))
        
    End If

End If


Call Deactivate_Active_Textbox

End Sub


Private Sub revDeadlineDay_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

'If (KeyCode = vbKeyPageDown Or KeyCode = vbKeyPageUp) And Shift = fmCtrlMask Then
'    Call Wind_Date(revDeadlineDay.Name, wdColorRed, KeyCode, Shift)
'ElseIf (KeyCode = vbKeyTab) And Shift = 2 Then
'    Call SkipTo_DeadlineTimesColumn("rev")
'End If

Call DeadlineDay_ProcessEvents(KeyCode, Shift, wdColorDarkBlue, "rev")

End Sub


Sub Wind_Date(TextboxName As String, Color As WdColor, KeyCode, Shift)


Dim scrollVal As Integer

If KeyCode = vbKeyPageDown Then
    scrollVal = -120    ' standard for scrolling once down, mousewheel
ElseIf KeyCode = vbKeyPageUp Then
    scrollVal = 120
Else
    Exit Sub
End If

' first we activate calling textbox (color red, name saved in global var)
Call Activate_Textbox(TextboxName, Color)

' and then we scroll back IF we're not going into the past!
Call Check_and_Wind_Date(TextboxName, scrollVal, KeyCode)

End Sub


Sub Check_and_Wind_Date(Target_TextboxName As String, ScrollValue As Integer, KeyCode)


Dim actTb As MSForms.TextBox

On Error Resume Next
Set actTb = Me.Controls(Target_TextboxName)

' Error checking in case sought label control is not to be found
If Err.Number > 0 Then
    MsgBox "Error number " & Err.Number & " occured in " & Err.Source & " with description " & Err.Description & "!"
    Err.Clear
    Exit Sub
End If

If ScrollValue < 0 Then
    
    ' if user tries to scroll below today, warn!
    If CDate(actTb.Value) = Date Then
        
        Call ColorWarn_DateInThePast(activate:=True, TargetTextbox:=actTb, KeyCode:=KeyCode, ScrollValue:=ScrollValue)
        
    Else    ' even if user does not scroll back from current day, first scroll can still land us below today, for.. weekend days!
        
        Dim ctInt As Integer
        ctInt = get_TimeInterval_forRotation(actTb.Name, CLng(ScrollValue), KeyCode)
        
        ' if first scroll lands us under today, we still warn!
        If DateAdd("d", ctInt, CDate(actTb.Text)) < Date Then
            Call ColorWarn_DateInThePast(activate:=True, TargetTextbox:=actTb, KeyCode:=KeyCode, ScrollValue:=ScrollValue)
        Else
            Call MouseWheel(ScrollValue, KeyCode)
        End If
        
    End If
    
Else    ' scroll positive
    
    If DateAdd("d", 1, CDate(actTb.Value)) <= Date Then
        Call ColorWarn_DateInThePast(activate:=False, TargetTextbox:=actTb, KeyCode:=KeyCode, ScrollValue:=ScrollValue)
    Else
        Call MouseWheel(ScrollValue, KeyCode)
    End If
    
End If


End Sub

Sub ColorWarn_ScrolledInThePast(TargetTextbox As MSForms.TextBox)

If CDate(TargetTextbox.Text) < Date Then
    
    ' First stage warning, color border in orange, DO NOT scroll into past. User has to repeat to break through!
    
    'If TargetTextbox.BorderStyle = fmBorderStyleNone Then
        'TargetTextbox.BorderStyle = fmBorderStyleSingle
        'TargetTextbox.BorderColor = wdColorOrange      ' hard-coded, for now?
    'Else    ' second stage of warning, we color border red, but we DO scroll into the past. User had ONE chance to stop, at stage 1 warning!
        
        
        
        TargetTextbox.BorderStyle = fmBorderStyleSingle
        TargetTextbox.BorderColor = wdColorRed
        
        'Call MouseWheel(ScrollValue, KeyCode)
    'End If
    
Else    ' Disable date in the past warning, user is scrolling back into present - future!
    
    'If TargetTextbox.BorderStyle = fmBorderStyleSingle Then
        
        TargetTextbox.BorderStyle = fmBorderStyleNone
        TargetTextbox.BorderColor = wdColorBlack
        'Call MouseWheel(ScrollValue, KeyCode)
        
    'Else
        'Call MouseWheel(ScrollValue, KeyCode)
    'End If
        
    
End If

End Sub

Sub ColorWarn_DateInThePast(activate As Boolean, TargetTextbox As MSForms.TextBox, KeyCode, ScrollValue)

If activate Then
    
    ' First stage warning, color border in orange, DO NOT scroll into past. User has to repeat to break through!
    If TargetTextbox.BorderStyle = fmBorderStyleNone Then
        TargetTextbox.BorderStyle = fmBorderStyleSingle
        TargetTextbox.BorderColor = wdColorOrange      ' hard-coded, for now?
    Else    ' second stage of warning, we color border red, but we DO scroll into the past. User had ONE chance to stop, at stage 1 warning!
        'TargetTextbox.BorderStyle = fmBorderStyleSingle
        TargetTextbox.BorderColor = wdColorRed
        Call MouseWheel(ScrollValue, KeyCode)
    End If
    
Else    ' Disable date in the past warning, user is scrolling back into present - future!
    
    If TargetTextbox.BorderStyle = fmBorderStyleSingle Then
        
        TargetTextbox.BorderStyle = fmBorderStyleNone
        TargetTextbox.BorderColor = wdColorBlack
        Call MouseWheel(ScrollValue, KeyCode)
        
    Else
        Call MouseWheel(ScrollValue, KeyCode)
    End If
        
    
End If


End Sub


Private Sub revDeadlineHour_Click()

Call Activate_Label(revDeadlineHour.Name, wdColorViolet)

End Sub


Private Sub revDeadlineMinutes_Click()

Call Activate_Label(revDeadlineMinutes.Name, wdColorViolet)

End Sub


Private Sub TextBox2_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)

Call Activate_Label(traDeadlineDay.Name, wdColorRed)

End Sub



Private Sub TextBox3_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)

Call Activate_Label(revDeadlineDay.Name, wdColorRed)

End Sub



Private Sub revDeadlineTime_Enter()

Me.revDeadlineTime.Tag = Me.revDeadlineTime.Text
Call Activate_Textbox(revDeadlineTime.Name, wdColorRed)
revDeadlineTime.SelStart = 0

End Sub

Private Sub revDeadlineTime_Exit(ByVal Cancel As MSForms.ReturnBoolean)

If Me.revDeadlineTime.Text <> "" Then
    
    If CDate(Me.revDeadlineTime.Text) <> CDate(Me.revDeadlineTime.Tag) Then
        'Call Wind_Date(Me.Controls(SectionPrefix & "DeadlineDay").Name, wdColorRed, KeyCode, Shift)
        
        ' EXPERIMENT... Does it work ?
        timeint = DateDiff("n", CDate(Me.revDeadlineTime.Tag), CDate(Me.revDeadlineTime.Text))    ' n = minutes difference
        
        Call Wind_All_DateTime_AwayFrom("Time", Me.revDeadlineTime.Name, CInt(timeint), timeint * 120, IIf(timeint > 0, vbKeyPageUp, vbKeyPageDown))
        
    End If
    
End If

Call Deactivate_Active_Textbox

End Sub

Private Sub revDeadlineTime_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

'Call Wind_Time(KeyCode)

Call DeadlineTime_ProcessEvents(KeyCode, Shift, "rev", wdColorDarkBlue, wdColorDarkRed)

End Sub


Private Sub traCbo_Enter()

' 12648384 green
' 16777152 clock blue

Call Activate_Section(EnabledState:=True, SectionName:="tra", ColorCode:=Me.TraAccentColor)


End Sub


Sub Activate_Section(EnabledState As Boolean, SectionName As String, ColorCode As Double)
' Used by mouse click on section name backgound label, to simply activate section

' -2147483633 - "Button face" kind of gray used to blend some controls in the background
' 16761024 - Clock blue background

Dim controlNames
controlNames = Split(sectionControlNames, ",")

Dim sectControlName As String

Dim ctLabel As MSForms.label
Dim ctTextbox As MSForms.TextBox
Dim ctCombobox As MSForms.ComboBox

Dim ctControl As MSForms.control


For i = 0 To UBound(controlNames)
    
    sectControlName = SectionName & controlNames(i)
    
    Set ctControl = Me.Controls(sectControlName)
    
    ' Label
    If InStr(1, ctControl.Name, "Label") > 0 Then
       'InStr(1, ctControl.Name, "Lbl") > 0
        
        Set ctLabel = ctControl
            
        'If i > 1 Then ctLabel.Enabled = EnabledState
        ' colour the important labels, distinguishable
        If i <= 3 Then
            ctLabel.BackColor = ColorCode
        Else
            If InStr(1, ctLabel.Name, "Assign") > 0 Then
                ctLabel.ForeColor = IIf(EnabledState = True, wdColorGray50, -2147483633)
            Else
                ctLabel.ForeColor = IIf(EnabledState = False, -2147483633, ctLabel.ForeColor)
            End If
        End If
        
        ' and reinject black into title font colour
        If InStr(1, ctLabel.Name, "Title") > 0 Then ctLabel.ForeColor = IIf(EnabledState = True, wdColorBlack, wdColorGray30)
        
        
    ' Combo box
    ElseIf InStr(1, ctControl.Name, "Cbo") > 0 Then
        
        Set ctCombobox = ctControl
        
        ' we grey all excluding the astCbo, traCbo and revCbo... we cannot move focus to 'em anymore otherwise...
        'ctComboBox.Enabled = EnabledState
        
        If i <= 3 Then ctCombobox.BackColor = ColorCode     ' light green, we grouped colored ones at beginning
        
    ' Textbox
    Else
        
        If Not InStr(1, ctControl.Name, "Lbl") > 0 Then
        
            Set ctTextbox = ctControl
            
            If InStr(1, ctTextbox.Name, "Time") > 0 Then
                
                ' if back style is opaque, user has pinned the no times setting, act accordingly
                If Me.tgNoTimes.Value = False Then
                    
                    ctTextbox.BackColor = IIf(EnabledState = True, 16761024, -2147483633)       ' 16761024 - clock blue
                    ctTextbox.ForeColor = IIf(EnabledState = True, wdColorGray70, -2147483633)
                    
                    ctTextbox.Enabled = EnabledState      ' we avoid disabling colored operation (section) labels, horiz and vert, we use em to click-activate-disactivate sections too !
                    ctTextbox.Text = IIf(EnabledState = True, ctTextbox.Tag, "")
                    ctLabel.BackColor = 16777152        ' clock blue
                    
                End If
                
            Else    'day
                
                ctTextbox.BackColor = IIf(EnabledState = True, wdColorWhite, -2147483633)
                ctTextbox.ForeColor = IIf(EnabledState = True, wdColorBlack, -2147483633)
                
                ctTextbox.Enabled = EnabledState      ' we avoid disabling colored operation (section) labels, horiz and vert, we use em to click-activate-disactivate sections too !
                
                ctTextbox.Text = IIf(EnabledState = True, ctTextbox.Tag, "")
                
            End If
        
        Else    ' Lbl's
            
            If InStr(1, ctControl.Name, "DayPin") > 0 Or _
                InStr(1, ctControl.Name, "DatePicker") > 0 Then
                
                Set ctLabel = ctControl
                
                ctLabel.ForeColor = IIf(EnabledState = True, wdColorWhite, -2147483633)
                
            ElseIf InStr(1, ctControl.Name, "NoTime") > 0 Or _
                InStr(1, ctControl.Name, "TimePin") > 0 Or _
                InStr(1, ctControl.Name, "Workday") > 0 Then
                
                Set ctLabel = ctControl
                
                ' if tgNoTimes is active, user has pinned the NO TIMES state... little controls don't get colored on section activation!
                If Me.tgNoTimes.Value = False Then
                    ctLabel.ForeColor = IIf(EnabledState = True, 16761024, -2147483633)
                End If
                
            End If
            
        End If
        
    End If
    
Next i

' and modify form caption dynamically
' First get complete section name of current section
Select Case SectionName
    
    Case "ast"
        Dim captionAdd As String
        captionAdd = "Preparation"
    Case "tra"
        captionAdd = "Translation"
    Case "rev"
        captionAdd = "Revision"
        
End Select

' We add section name to form caption for activation
If EnabledState = True Then
    
    If Right(Me.Caption, 3) = "for" Then
        Me.Caption = Me.Caption & " " & captionAdd
    Else
        If Not (InStr(1, Me.Caption, captionAdd)) > 0 Then
            Me.Caption = Me.Caption & " & " & captionAdd
        End If
    End If

Else     ' we just disabled a section, remove resp section name from form caption
    
    Dim captionWords
    captionWords = Split(Me.Caption, " ")
    
    ' Lastly activated section is just being disabled
    If captionWords(UBound(captionWords)) = captionAdd Then
        Me.Caption = Replace(Me.Caption, " " & captionAdd, "")
        If Right(Me.Caption, 1) = "&" Then
            Me.Caption = Left(Me.Caption, Len(Me.Caption) - 2)
        End If
    Else    ' we're disabling a section which we previously enabled, but not lastly...
    ' the looked for section name is inside... like removing "Translation" from "Assign doc for Translation and Revision"
        
        If InStr(1, Me.Caption, " for " & captionAdd) > 0 Then
            Me.Caption = Replace(Me.Caption, " " & captionAdd, "")
            If InStr(1, Me.Caption, "for &") > 0 Then Me.Caption = Replace(Me.Caption, "for &", "for")
        ElseIf InStr(1, Me.Caption, " & " & captionAdd) > 0 Then
            Me.Caption = Replace(Me.Caption, " & " & captionAdd, "")
        End If
        
    End If
End If

' sort operations in proper order
If InStr(1, Me.Caption, "Translation & Preparation") > 0 Then
    Me.Caption = Replace(Me.Caption, "Translation & Preparation", "Preparation & Translation")
ElseIf InStr(1, Me.Caption, "Revision & Translation") > 0 Then
    Me.Caption = Replace(Me.Caption, "Revision & Translation", "Translation & Revision")
ElseIf InStr(1, Me.Caption, "Revision & Preparation") > 0 Then
    Me.Caption = Replace(Me.Caption, "Revision & Preparation", "Preparation & Revision")
End If



End Sub


Private Sub traCbo_Exit(ByVal Cancel As MSForms.ReturnBoolean)

If traCbo.Text = "" Then
    ' 14737632
    Call Activate_Section(EnabledState:=False, SectionName:="tra", ColorCode:=14737632)
Else    ' Color translation section in "Translate & Finalise" color, if case be
    
    ' correct for accidental input of NP characters
    Dim cleanedTraCbo As String
    cleanedTraCbo = Clean_NPCharacters(Me.traCbo.Text)
    
    If cleanedTraCbo = "" Then
        Me.traCbo.Text = ""
        Call Activate_Section(EnabledState:=False, SectionName:="tra", ColorCode:=14737632)
    Else
    
        If Me.revCbo.Text <> "" And Me.revCbo.Text = Me.traCbo.Text Then
            If Me.TraRevAccentColor <> wdColorBlack Then
                Me.traColorLabel1.BackColor = Me.TraRevAccentColor
                Me.traColorLabel2.BackColor = Me.TraRevAccentColor
                Me.traCbo.BackColor = Me.TraRevAccentColor
            
                Me.revColorLabel1.BackColor = Me.TraRevAccentColor
                Me.revColorLabel2.BackColor = Me.TraRevAccentColor
                Me.revCbo.BackColor = Me.TraRevAccentColor
            
            End If
        End If
        
    End If
    
    ' hide the label of traCbo and the little scissors button
    Me.traAssignLabel.ForeColor = -2147483633
    Me.traMultiAssignLabel.ForeColor = -2147483633
        
End If

End Sub


Private Sub traCbo_MouseMove(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If traCbo.Text <> "" Then
    If Not traAssignLabel.ForeColor = wdColorGray50 Then
        traAssignLabel.ForeColor = wdColorGray50
        traMultiAssignLabel.ForeColor = wdColorGray50
    End If
End If

End Sub


Private Sub traColorLabel1_Click()

Call Activate_Section(EnabledState:=True, SectionName:="tra", ColorCode:=Me.TraAccentColor)
If Me.traCbo.Enabled Then Me.traCbo.SetFocus

End Sub


Private Sub traColorLabel2_Click()

Me.traCbo.Text = ""
Call Activate_Section(EnabledState:=False, SectionName:="tra", ColorCode:=14737632)

End Sub



Private Sub traDatePickerLbl_Click()

'Dim ctTxtbN As String           ' name
'ctTxtbN = Me.traCbo.Name
'
'Dim ctDTxtbN As String           ' name
'ctDTxtbN = Me.traDeadlineDay.Name

' Function, baby ! Elegant!
Call SyncMove_AllDeadlineDays_fromCalendar(Me.traDeadlineDay, Me.tbAccentColor.Value, Me.traCbo)

End Sub


Sub SyncMove_AllDeadlineDays_fromCalendar(Calling_Textbox As MSForms.TextBox, HeaderColor As WdColor, Optional Calling_Initials_Combobox)

'Dim callingCombo As MSForms.ComboBox
'Set callingCombo = Me.Controls(Calling_Initials_ComboboxName)

If Not IsMissing(Calling_Initials_Combobox) Then
    
    If Calling_Initials_Combobox.Value = "" Then
        
        Exit Sub
        
    End If
    
End If


'Dim CallingTextbox As MSForms.TextBox
'Set CallingTextbox = Me.Controls(Calling_TextboxName)

' no workin winding days without this, man !
Call Activate_Textbox(Calling_Textbox.Name, wdColorRed)

Dim newPickedDeadlineDay As Date

' HeaderColor=RGB(84, 130, 53)

newPickedDeadlineDay = CalendarForm.GetDate( _
    DateFontSize:=11, _
    BackgroundColor:=RGB(242, 248, 238), _
    HeaderColor:=HeaderColor, _
    HeaderFontColor:=RGB(255, 255, 255), _
    SubHeaderColor:=RGB(226, 239, 218), _
    SubHeaderFontColor:=RGB(55, 86, 35), _
    DateColor:=RGB(242, 248, 238), _
    DateFontColor:=RGB(55, 86, 35), _
    SaturdayFontColor:=RGB(55, 86, 35), _
    SundayFontColor:=RGB(55, 86, 35), _
    TrailingMonthFontColor:=RGB(106, 163, 67), _
    DateHoverColor:=RGB(198, 224, 180), _
    DateSelectedColor:=RGB(169, 208, 142), _
    TodayFontColor:=RGB(255, 0, 0))

' What if user canceled date picker?
If newPickedDeadlineDay <> 0 Then

    'Dim ctOptsF As New SettingsFileClass
    Dim ctDateF As String

    'ctDateF = ctOptsF.getOption("DateFormat", "AssignOptions")
    ctDateF = Me.cbDateformat.Value
    
    Dim daysScrolled As Integer      ' to hold diff in days numbers for setting all subsq days deadlines just now
    daysScrolled = DateDiff("d", CDate(Calling_Textbox.Value), CDate(newPickedDeadlineDay))
    
    Calling_Textbox.Value = CaptDate(Format(newPickedDeadlineDay, ctDateF))
    
    Call SyncMove_All_DeadlineDays(daysScrolled, 120 * daysScrolled, IIf(daysScrolled > 0, vbKeyPageUp, vbKeyPageDown))
    
Else
    
    ' Nothin to do, just exit
    Call Deactivate_Active_Textbox
    
End If


End Sub

Private Sub traDayPinLbl_Click()

If traDeadlineDay.Enabled Then
    Call Flip_PinnedState_ofLabel(traDayPinLbl, wdColorDarkBlue)
End If

End Sub

Private Sub traDeadlineDay_Enter()
    
    Me.traDeadlineDay.Tag = Me.traDeadlineDay.Text
    Call Activate_Textbox(traDeadlineDay.Name, wdColorRed)
    traDeadlineDay.SelStart = 0
    
End Sub


Private Sub traDeadlineDay_Exit(ByVal Cancel As MSForms.ReturnBoolean)

' TEST, for manual input functioning
If Me.traDeadlineDay.Text <> "" Then
    
    Call ColorWarn_ScrolledInThePast(Me.traDeadlineDay)
    
    If CDate(Me.traDeadlineDay.Text) <> CDate(Me.traDeadlineDay.Tag) Then
        'Call Wind_Date(Me.Controls(SectionPrefix & "DeadlineDay").Name, wdColorRed, KeyCode, Shift)
        
        ' EXPERIMENT... Does it work ?
        timeint = DateDiff("d", CDate(Me.traDeadlineDay.Tag), CDate(Me.traDeadlineDay.Text))
        Call Wind_All_DateTime_AwayFrom("Date", Me.traDeadlineDay.Name, CInt(timeint), timeint * 120, IIf(timeint > 0, vbKeyPageUp, vbKeyPageDown))
        
    End If

    
End If

Call Deactivate_Active_Textbox

End Sub


Private Sub traDeadlineDay_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

'If (KeyCode = vbKeyPageDown Or KeyCode = vbKeyPageUp) And Shift = 0 Then
'    Call Wind_Date(traDeadlineDay.Name, wdColorRed, KeyCode, Shift)
'ElseIf (KeyCode = vbKeyTab) And Shift = 2 Then  ' shift = Control key
'    Call SkipTo_DeadlineTimesColumn("tra")
'End If

Call DeadlineDay_ProcessEvents(KeyCode, Shift, wdColorDarkBlue, "tra")

End Sub



Private Sub traDeadlineDay_MouseMove(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If isSectionEnabled("tra") Then
    
    If Not Me.traDatePickerLbl.ForeColor = DayButtonsColor Then
    
        traDatePickerLbl.ForeColor = DayButtonsColor
        traDayPinLbl.ForeColor = DayButtonsColor
        
        traDDayLabel.ForeColor = wdColorGray50
        traDayLabel.ForeColor = wdColorGray50
        
    End If
End If

End Sub



Private Sub traDeadlineTime_Enter()

Me.traDeadlineTime.Tag = Me.traDeadlineTime.Text
Call Activate_Textbox(traDeadlineTime.Name, wdColorRed)
traDeadlineTime.SelStart = 0

End Sub

Private Sub traDeadlineTime_Exit(ByVal Cancel As MSForms.ReturnBoolean)

If Me.traDeadlineTime.Text <> "" Then
    
    If CDate(Me.traDeadlineTime.Text) <> CDate(Me.traDeadlineTime.Tag) Then
        'Call Wind_Date(Me.Controls(SectionPrefix & "DeadlineDay").Name, wdColorRed, KeyCode, Shift)
        
        ' EXPERIMENT... Does it work ?
        timeint = DateDiff("n", CDate(Me.traDeadlineTime.Tag), CDate(Me.traDeadlineTime.Text))    ' n = minutes difference
        
        Call Wind_All_DateTime_AwayFrom("Time", Me.traDeadlineTime.Name, CInt(timeint), timeint * 120, IIf(timeint > 0, vbKeyPageUp, vbKeyPageDown))
        
    End If
    
End If


Call Deactivate_Active_Textbox

End Sub


Private Sub traDeadlineTime_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

' Shift 1 = Shift
' Shift 0 = Null
' Shift 2 = Control
' Shift 4 = Alt
' Shift 3 = Ctrl+Shift
' Shift 6 = Ctrl+Alt
' Shift 5 = Alt+Shift
' Shift 7 = Ctrl+Alt+Shift


Call DeadlineTime_ProcessEvents(KeyCode, Shift, "tra", wdColorDarkBlue, wdColorDarkRed)


End Sub


Sub DeadlineTime_ProcessEvents(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer, SectionPrefix As String, _
    PinForeColor As WdColor, DeactivateForeColor As WdColor)

' SectionPrefix = "ast", "tra", "rev", "val" or "fin"


Select Case KeyCode
    
    Case vbKeyPageDown, vbKeyPageUp
        
        If Shift = 2 Then    'Control modifier
            
            Dim TimeDestination As String
            TimeDestination = IIf(KeyCode = vbKeyPageUp, "SOD", "EOD")
            
            
            Dim CallingLabel_NameRoot As String
            CallingLabel_NameRoot = IIf(KeyCode = vbKeyPageUp, "StartWorkdayLbl", "EndWorkdayLbl")
            
            ' visual effect, user will see button activation
            Me.Controls(SectionPrefix & CallingLabel_NameRoot).ForeColor = wdColorGreen
            PauseForSeconds (0.1)
            
            Call DeadlineTime_JumpTo(TimeDestination, Me.Controls(SectionPrefix & CallingLabel_NameRoot))
            
            ' but we come back to starting color
            Me.Controls(SectionPrefix & CallingLabel_NameRoot).ForeColor = wdColorGray25
            
        
        Else    ' All other combos, including no shift (0)
            
            ' Wind time manually, hours or minutes
            Call Wind_Time(KeyCode)
            
        End If

    Case vbKeyHome, vbKeyEnd
    
        ' If it was home or end scroll time also
        Call Wind_Time(KeyCode)
        
    Case vbKeyInsert
    
        Call Flip_PinnedState_ofLabel(Me.Controls(SectionPrefix & "TimePinLbl"), PinForeColor)
    
    Case vbKeyDelete
        
        ' TEMPORARY manual deactivation of Delete key combo acting as deactivation of deadline time,
        ' in case of Manual Input allowance! (We need it to edit!)
        ' TODO: LATER on, when having finished key combos implementation, simply remove key as action combo and
        ' signal to user and empty corr field in key combos list in settings tab 3!
        
        If Not Me.chDeadlineManualInput Then
        
            ' Deactivate current deadline time
            Call DeadlineTime_Deactivate(1, 0, Me.Controls(SectionPrefix & "NoTimeLbl"), Me.Controls(SectionPrefix & "DeadlineTime"), DeactivateForeColor)
    
        End If
        
End Select


End Sub


Private Sub traDeadlineTime_MouseMove(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If isSectionEnabled("tra") Then

    If Not Me.traNoTimeLbl.ForeColor = TimeButtonsColor Then
    
        traNoTimeLbl.ForeColor = TimeButtonsColor
        traTimePinLbl.ForeColor = TimeButtonsColor
        traStartWorkdayLbl.ForeColor = TimeButtonsColor
        traEndWorkdayLbl.ForeColor = TimeButtonsColor
        
        traTimeLabel.ForeColor = wdColorGray50
        
    End If

End If

End Sub

Private Sub traEndWorkdayLbl_Click()

If traDeadlineTime.Enabled Then
    Call DeadlineTime_JumpTo("EOD", traEndWorkdayLbl)
End If

End Sub


Private Sub traEndWorkdayLbl_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If traDeadlineTime.Enabled Then
    traEndWorkdayLbl.ForeColor = wdColorGreen
End If

End Sub


Private Sub traEndWorkdayLbl_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If traDeadlineTime.Enabled Then
    traEndWorkdayLbl.ForeColor = TimeButtonsColor
End If

End Sub


Private Sub traMultiAssignLabel_Click()

If Me.lbMultipleAssignments.Left > 500 Then
    Call Summon_MultiAssignment("tra")
Else
    Call Dismiss_MultiAssignment
End If


End Sub

Sub Summon_MultiAssignment(CallingSectionName As String)

Select Case CallingSectionName
    
    Case "ast"
        Me.astCbo.Text = "�"
        Me.lbMultipleAssignments.Tag = "ast"    ' so as to signal to "Assign" button where I'm calling it from...
        Me.lbMultipleAssignments.List = Me.astCbo.List
    Case "tra"
        Me.traCbo.Text = "�"
        Me.lbMultipleAssignments.Tag = "tra"    ' so as to signal to "Assign" button where I'm calling it from...
        ' show multiple assignments list box in user view
        Me.lbMultipleAssignments.List = Me.traCbo.List
    Case "rev"
        Me.revCbo.Text = "�"
        Me.lbMultipleAssignments.Tag = "rev"    ' so as to signal to "Assign" button where I'm calling it from...
        Me.lbMultipleAssignments.List = Me.revCbo.List
    
End Select

' and bring into glorious view!
Me.lbMultipleAssignments.Left = 276

' and disguise our cancel button in festive clothing! Youppiii!
Me.butCancel.Caption = "Assign"


End Sub


Sub Dismiss_MultiAssignment()


Me.lbMultipleAssignments.Left = 620


Me.lbMultipleAssignments.Clear

Select Case lbMultipleAssignments.Tag
    
    Case "ast"
        If Me.astCbo.Text = "�" Then Me.astCbo.Text = ""
    Case "tra"
        If Me.traCbo.Text = "�" Then Me.traCbo.Text = ""
    Case "rev"
        If Me.revCbo.Text = "�" Then Me.revCbo.Text = ""
    
End Select


Me.lbMultipleAssignments.Tag = ""   ' clear tag as well

Me.butCancel.Caption = "Cancel"


End Sub




Private Sub traNoTimeLbl_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

'If traDeadlineTime.Enabled Then
'Button, Shift, traNoTimeLbl, traDeadlineTime, wdColorDarkRed
    Call DeadlineDeactivate_ProcessEvents(Button, Shift, traNoTimeLbl, traDeadlineTime, wdColorDarkRed, TimeButtonsColor)
'End If

End Sub


Private Sub traNoTimeLbl_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

'If Not traDeadlineTime.Enabled Then
    If Button = 1 Then
        traNoTimeLbl.ForeColor = TimeButtonsColor
    End If
'End If

End Sub


Sub DeadlineDeactivate_ProcessEvents(Button As Integer, Shift As Integer, CallingLabel As MSForms.label, _
    TargetTextbox As MSForms.TextBox, ActivationColor As WdColor, DeactivationColor As WdColor)


If Button = 1 Then  ' Mouse button Left
    
    CallingLabel.ForeColor = ActivationColor
    Call DeadlineTime_Deactivate(Button, Shift, CallingLabel, TargetTextbox, ActivationColor)
    
ElseIf Button = 2 Then      ' Mouse button Right
    
    If CallingLabel.ForeColor = ActivationColor Then
        CallingLabel.ForeColor = DeactivationColor
    ElseIf CallingLabel.ForeColor = DeactivationColor Then
        CallingLabel.ForeColor = ActivationColor
    End If
    
    If TargetTextbox.Enabled Then
        Call DeadlineTime_Deactivate(Button, Shift, CallingLabel, TargetTextbox, ActivationColor)
    End If
    
End If


End Sub


Private Sub traStartWorkdayLbl_Click()

' Don' act on a disabled textbox !
If traDeadlineTime.Enabled Then
    Call DeadlineTime_JumpTo("SOD", Me.traDeadlineTime)
End If

End Sub

Private Sub traStartWorkdayLbl_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If traDeadlineTime.Enabled Then
    traStartWorkdayLbl.ForeColor = wdColorGreen
End If

End Sub

Private Sub traStartWorkdayLbl_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If traDeadlineTime.Enabled Then
    traStartWorkdayLbl.ForeColor = TimeButtonsColor
End If

End Sub

Private Sub traTimePinLbl_Click()

If traDeadlineTime.Enabled Then
    Call Flip_PinnedState_ofLabel(traTimePinLbl, wdColorDarkBlue)
End If

End Sub

Function isSectionEnabled(SectionPrefix As String) As Boolean
' SectionPrefix = ast,tra,rev

Select Case SectionPrefix
    
    Case "ast"
        
        If Me.astColorLabel1.BackColor <> Me.PrepAccentColor Then
            isSectionEnabled = False
        Else
            isSectionEnabled = True
        End If
        
    Case "tra"
            
        If Me.traColorLabel1.BackColor <> Me.TraAccentColor And _
            Me.traColorLabel1.BackColor <> Me.TraRevAccentColor Then
            
            isSectionEnabled = False
            
        Else
            isSectionEnabled = True
        End If
            
    Case "rev"
        
        If Me.revColorLabel1.BackColor <> Me.RevAccentColor And _
            Me.revColorLabel1.BackColor <> Me.TraRevAccentColor Then
            
            isSectionEnabled = False
            
        Else
            isSectionEnabled = True
        End If
    
End Select


End Function

Private Sub UserForm_Activate()
    
    'WheelHook Me.Caption 'For scrolling support
    'StatusBar = "Userform Activate RAN! LocalHwnd is " & MouseWheelMod.LocalHwnd
    
    ' quickly switch focus back and forth so we may battle the mouse wheel hooking
    ' temp loss !
    'Call QuickFocusSwitch
    
    If Me.tbAstUsersList.Text = "" Or Me.tbAdUsersList.Text = "" Then
        MsgBox "This seems to be the first time you run this program." & vbCr & vbrc & _
            "For this program to run as intended, you are required to input your unit's WF initials" & _
            "and Windows user names for the 2 categories of unit members into the apropriate" & _
            "text boxes (See options, tab 2, first 4 text boxes)" & vbCr & vbCr & _
            "Such as: <aa,bb,cc> into the left upper and lower textboxes and <antonad,bunicbu,creanco>" & _
            "into the right upper and lower textboxes"
        
        Exit Sub
        
    End If
    
    ' HERETOFILLIN
    Call Initialize_PreFilled_Sections

    Call Initialize_tbaDocuments
    
    
    ' First activation is when user starts userform. We want to focus on user-chosen operation then.
    ' Other activation events can happen if user uses some other form while using this one, such as the included calendar form. We want NO focus switch then!
    If Not AutoActivationDone Then

        If Me.astInitData = "" And Me.traInitData = "" And revInitData = "" Then

            Dim firstOp As String
            firstOp = Me.cbInitialOperation.Value

            Select Case firstOp

                Case "Preparation"
                    If Me.astCbo = "" Then Me.astCbo.SetFocus
                Case "Translation"
                    If Me.traCbo = "" Then Me.traCbo.SetFocus
                Case "Revision"
                    If Me.revCbo = "" Then Me.revCbo.SetFocus
            End Select

            AutoActivationDone = True

        Else    ' User has pre-filled in ast, trad and/ or rev initialization data.

            ' we don't really care about astInitData being empty or not, we only care about tradInitData
            ' (there's no use scenario where we would give doc for prep and when returned we give it for trad...)

            If astInitData <> "" Then

' we come here for even if ast Init data is empty, we still care for whether trad is or not empty...
CheckTrad:
                If traInitData <> "" Then

                    If revInitData <> "" Then
                    Else    ' revInitData empty
                        
                        ' enable trad time if user did not disable it, so as to be inserted in rev mail
                        'If Me.chNoTradTimesInRev.Value = False Then
                            'Me.traDeadlineTime.Enabled = True
                            'Me.traDeadlineTime.ForeColor = wdColorGray25
                        'End If
                        
                        If Me.revCbo = "" Then Me.revCbo.SetFocus
                        AutoActivationDone = True

                    End If

                Else    ' tradInitData empty

                    If Me.traCbo = "" Then Me.traCbo.SetFocus
                    AutoActivationDone = True

                End If

            Else    ' astInitData empty

                GoTo CheckTrad

            End If

        End If

    Else
    End If

' only initialize once !
If Me.valDeadlineDay.Text = "" Then
    ' and retrieve computed validation and finalise days from hidden tags
    Me.valDeadlineDay.Text = Me.valDeadlineDay.Tag
    Me.finDeadlineDay.Text = Me.finDeadlineDay.Tag
    ' time as well, but not for finalisation
    Me.valDeadlineTime.Text = Me.valDeadlineTime.Tag
End If
    
' Doesn't work!
Me.Top = Me.Top - 1
Me.Top = Me.Top + 1
    
    
End Sub


Sub Initialize_tbaDocuments()

    ' when we come back to form after having lost focus, auto paste from clipboard (changing format on the way) a or the docs found there!
    
    If Application.Name = "Outlook" Then    ' we have no current doc
    
        If Me.tbDocuments.Text = "" Or Me.tbDocuments.Text = "Document number(s)" Then
            
            If IsClipboard_AListOf_CouncilDocuments Then
                
                If Me.tbaDocuments = "" Then
                    Me.tbDocuments.Text = Get_Clipboard_GSCDocsString
                Else    ' Prefilled-In document name, prob called from "Assign revision" routine!
                    Me.tbDocuments.Text = Me.tbaDocuments
                End If
                
            Else    ' NO Council doc in clip
                
                If Not Me.tbaDocuments = "" Then
                    Me.tbDocuments.Text = Me.tbaDocuments
                End If
                
            End If
            
        End If
    
    Else    ' word's code
        
        If Me.tbDocuments.Text = "" Or Me.tbDocuments.Text = "Document number(s)" Then
            
            If Documents.count > 0 Then
                
                If Is_GSC_Doc(ActiveDocument, Name) Then
                    Me.tbDocuments.Text = GSCFileName_ToStandardName(ActiveDocument, Name)
                Else
                    GoTo ClipCheck
                End If
                
            Else

ClipCheck:
                If IsClipboard_AListOf_CouncilDocuments Then
                    
                    If Me.tbaDocuments = "" Then
                        Me.tbDocuments.Text = Get_Clipboard_GSCDocsString
                    Else    ' Prefilled-In document name, prob called from "Assign revision" routine!
                        Me.tbDocuments.Text = Me.tbaDocuments
                    End If
                    
                Else
                    
                    If Me.tbaDocuments <> "" Then
                        Me.tbDocuments.Text = Me.tbaDocuments
                    End If
                    
                End If
                
            End If
            
        End If
        
        
    End If


End Sub
        
        
Private Sub UserForm_Click()

' if one textbox (date or time) is active, deactivate it
Call Deactivate_Active_Textbox

' if keys combo capture textbox is active, dismiss it
If Active_KeysCapture_Box Then
    Call Dismiss_KeysCapture_Box
End If

' when we come back to form after having lost focus, auto paste from clipboard (changing format on the way) a or the docs found there!
If Me.tbDocuments.Text = "" Or Me.tbDocuments.Text = "Document number(s)" Then
    If IsClipboard_AListOf_CouncilDocuments Then
        Me.tbDocuments.Text = Get_Clipboard_GSCDocsString
    End If
End If

Me.butCreateMails.SetFocus

End Sub



Private Sub UserForm_QueryClose(Cancel As Integer, CloseMode As Integer)

'Call WheelUnHook     'For scrolling support
Call KillTimer(frmHwnd, TimerID)
'...
End Sub


Private Sub UserForm_Deactivate()
'WheelUnHook     'For scrolling support
'...
End Sub


Sub Deactivate_Active_Textbox()

' -2147483633 - "Button face" kind of gray used to blend some controls in the background
' 16761024 - Clock blue background

'Dim activeLabel As control
Dim activeTextboxT As TextBox

If ActiveTextboxName <> "" Then

    Set activeTextboxT = Me.Controls(ActiveTextboxName)
    'Set activeLabelL = activeLabel
    
    'If InStr(1, ActiveTextboxName, "Time") > 0 Then
        'activeTextboxT.ForeColor = wdColorDarkBlue
    'Else
        activeTextboxT.ForeColor = wdColorBlack
    'End If
    
    ' remove border and color if orange was
    If activeTextboxT.BorderStyle = fmBorderStyleSingle And _
        activeTextboxT.BorderColor = wdColorOrange Then
        
        activeTextboxT.BorderStyle = fmBorderStyleNone
        activeTextboxT.BorderColor = wdColorBlack
        
    End If
    
    ' also make invisible little buttons attached to day and time texboxes
    If InStr(1, ActiveTextboxName, "Day") > 0 Then
        Me.Controls(Left(ActiveTextboxName, 3) & "DatePickerLbl").ForeColor = wdColorWhite
        Me.Controls(Left(ActiveTextboxName, 3) & "DayPinLbl").ForeColor = wdColorWhite
    Else
        Me.Controls(Left(ActiveTextboxName, 3) & "NoTimeLbl").ForeColor = 16761024
        Me.Controls(Left(ActiveTextboxName, 3) & "TimePinLbl").ForeColor = 16761024
        Me.Controls(Left(ActiveTextboxName, 3) & "StartWorkdayLbl").ForeColor = 16761024
        Me.Controls(Left(ActiveTextboxName, 3) & "EndWorkdayLbl").ForeColor = 16761024
    End If
    
End If

ActiveTextboxName = ""
' Debug
'If ActiveTextboxName = "" Then StatusBar = "ActiveTextboxName is now <Empty>"

End Sub



Private Sub astCbo_Change()
    
    If Me.astCbo.Value <> "" Then
        Me.astCbo.Value = Clean_NPCharacters(Me.astCbo.Value)   ' clean string from accidents
        
        If IsActivatedSection("ast") Then
            activitiesCode = activitiesCode Or CByte(1)    ' AST is bit 0, 2^0 = 1
        End If
    Else    ' User just turned it off (by deleting the value in the textbox !)
        activitiesCode = activitiesCode And Not (CByte(1))   ' AST is bit 0, 2^0 = 1
    End If
    
    
End Sub


Private Sub traCbo_Change()

    If Me.traCbo.Value <> "" Then
        Me.traCbo.Value = Clean_NPCharacters(Me.traCbo.Value)   ' clean string from accidents
        
        ' so as to avoid creating a multiple-operation code when in fact we're just prefilling other sections
        If IsActivatedSection("tra") Then
            activitiesCode = activitiesCode Or CByte(2)    ' AD is bit 1, 2^1 = 2
        End If
        
    Else
        activitiesCode = activitiesCode And Not CByte(2)    ' AD is bit 1, 2^1 = 2
    End If
    
    'StatusBar = "traCboChange: LocalHwnd = " & LocalHwnd & "; ActiveTextboxName = " & ActiveTextboxName

    
End Sub


Function IsActivatedSection(SectionPrefix As String) As Boolean
' SectionPrefix can only be "ast", "tra" or "rev"

Dim ctSectTitleLabel As MSForms.label

Set ctSectTitleLabel = Me.Controls(SectionPrefix & "ColorLabel1")

Select Case SectionPrefix
    Case "ast"
        If ctSectTitleLabel.BackColor = Me.PrepAccentColor.Value Then
            IsActivatedSection = True
        End If
    Case "tra"
        If ctSectTitleLabel.BackColor = Me.TraAccentColor.Value Then
            IsActivatedSection = True
        End If
    Case "rev"
        If ctSectTitleLabel.BackColor = Me.RevAccentColor.Value Then
            IsActivatedSection = True
        End If
End Select


End Function

Private Sub revCbo_Change()

    If Me.revCbo.Value <> "" Then
        Me.revCbo.Value = Clean_NPCharacters(Me.revCbo.Value)   ' clean string from accidents
        
        If IsActivatedSection("rev") Then
            activitiesCode = activitiesCode Or CByte(4)    ' REV is bit 2, 2^2 = 4
        End If
    Else
        activitiesCode = activitiesCode And Not CByte(4)    ' REV is bit 2, 2^2 = 4
    End If
    
    'StatusBar = "revCboChange: LocalHwnd = " & LocalHwnd & "; ActiveTextboxName = " & ActiveTextboxName
    
End Sub


Private Sub clockIcon_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

    Me.clockIcon.SpecialEffect = fmSpecialEffectSunken

End Sub

Private Sub clockIconAst_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

    Me.clockIconAst.SpecialEffect = fmSpecialEffectFlat

End Sub






Sub Initialize_UsersComboboxes(WhichOne As String)
' Only accepting "ast" or "ad" or caps versions

If LCase(WhichOne) = "ast" Then

    ' break apart the long string with ad users (to move that into a setting in a setting file,
    ' for transparency and and ease of use!)
    Dim astInitList As Variant
    astInitList = Split(Me.tbAstInitList, ",")
    
    Me.astCbo.List = astInitList
    
    ' debug
    'Me.lbMultipleAssignments.List = astInitList
    
ElseIf LCase(WhichOne) = "ad" Then

    ' Break apart the long string with ad users
    Dim adInitList As Variant
    adInitList = Split(Me.tbAdInitList, ",")
    
    Me.traCbo.List = adInitList
    Me.revCbo.List = adInitList
    

End If

End Sub

Sub Initialize_DateFormats()

Dim dateformats As Variant

' Populate dateformat combo box with predecided options
dateformats = Array("dd/mm", "d/mm", "mmm dd", "dd mmm")
'cbDateformat.AddItem dateformats(0): cbDateformat.AddItem dateformats(1)
'cbDateformat.AddItem dateformats(2): cbDateformat.AddItem dateformats(3)
cbDateformat.List = dateformats
cbDateformat.ListIndex = 0      ' will be changed a little later, if user options file exists

' and maildateformat combobox as well (for freezing e-mail date format to ONE format, for consistency)
'cbMailDateFormat.AddItem dateformats(0): cbMailDateFormat.AddItem dateformats(1)
'cbMailDateFormat.AddItem dateformats(2): cbMailDateFormat.AddItem dateformats(3)
'cbMailDateFormat.ListIndex = 0      ' will be changed a little later, if user options file exists
cbMailDateFormat.List = dateformats
cbMailDateFormat.ListIndex = 0

End Sub

Function Get_Clipboard_GSCDocsString()

Dim clDocList As String

clDocList = TrimNonAlphaNums(Get_Clipboard_TextContents)
clDocList = Replace(Replace(clDocList, vbCr, ", "), vbLf, "")

Dim clfDocs As Variant

If InStr(1, clDocList, ",") > 0 Then
    
    clfDocs = Split(clDocList, ", ")
    
    For i = 0 To UBound(clfDocs)
        clfDocs(i) = GSCFileName_ToStandardName(GSC_StandardName_ToFileName(CStr(clfDocs(i))))
    Next i
    
    'Dim srClfDocs As String
    clfDocs = Join(clfDocs, ", ")

Else
    clfDocs = GSCFileName_ToStandardName(GSC_StandardName_ToFileName(clDocList))
End If

Get_Clipboard_GSCDocsString = CStr(clfDocs)


End Function


Sub Save_WordAndForm_Handles()

If Application.Name = "Microsoft Word" Then
    'Save Word's window handle in a global var, we need it just a bit later
    If Documents.count > 0 Then
        WordHwnd = FindWindow("OpusApp", ActiveDocument.Name & " - Microsoft Word")
    Else
        WordHwnd = FindWindow("OpusApp", "Microsoft Word")
    End If
End If

' Save Hwnd in global var, need for timer kill function
frmHwnd = FindWindow("ThunderDFrame", "Assign document for")
'MsgBox "FrmHwnd = " & frmHwnd


End Sub


Function GetOption_fromMemory(ByRef TargetArray, SoughtValue As String) As String

Dim Idx As Integer

Idx = GetArray_Index_forValue(TargetArray, SoughtValue)


If Idx = -1 Then    ' if we fail to get the index of the sought value
    GetOption_fromMemory = ""
    Exit Function
Else    ' FOund index, sought value is in array!
    
    GetOption_fromMemory = TargetArray(1, Idx)
    
End If

End Function

Private Sub UserForm_Initialize()

'MsgBox "UserForm_Initialize has been RAN!"

' we sometimes need (one case only) to start form widened (options part opened)
Call SetForm_Width_atStart

' Save the Word handle and userform handle to global variables
Call Save_WordAndForm_Handles

' two different sets of actions below, so we separate:
' first we initialize options wich have no prewritten values (like comboboxes)
' and retrieve EVENTUAL saved options from saved options file by user (by ckicking "Save" button)
Call Sync_AssignOptions_toOptionsFile

' and then, from those restored options, populate the different controls in the normal visible
' part of the userform.. such as users lists comboboxes, deadline days and times, mail mode, colors
Call Restore_AssignForm_UserPreferences

' Display clock in specialized label
Call StartTimer

' Replace Window Procedure of userform with a custom one ('hook' the window procedure as it's called)
' so as to be able to perform custom code in response to mouse wheel scrolling action from user !!!
' WheelHook; Me.Caption 'For scrolling support
'Debug.Print "Userform Initialize RAN! LocalHwnd is " & MouseWheelMod.LocalHwnd


End Sub


Sub SetForm_Width_atStart()

If Not MouseWheelMod.StartWidenedForm Then
    If Application.Name = "Outlook" Then
        If Left$(Application.Version, 2) = "14" Then
            Me.Width = 345  ' auto close options at start of form, in design mode they are opened !
        Else    ' only Outlook 2016 is freaky! 2010 Outlook and Word think alike!
            Me.Width = 354  ' auto close options at start of form, in design mode they are opened !
        End If
    ElseIf Application.Name = "Microsoft Word" Then
        If Left$(Application.Version, 2) = "14" Then
            Me.Width = 348  ' auto close options at start of form, in design mode they are opened !
        Else    ' only Outlook 2016 is freaky! 2010 Outlook and Word think alike!
            Me.Width = 352  ' auto close options at start of form, in design mode they are opened !
        End If
    End If
    
Else    ' Used only when resetting settings, to display form widened, just that once upon re-displaying it.
        ' we didn't reduce width of userform, but we make sure it's only one time!
    MouseWheelMod.StartWidenedForm = False
End If

End Sub


Sub Keyboard_Shortcuts_Initialize(Memassignoptions_array)

If Not Saved_Custom_KeyCombos(Memassignoptions_array) Then
    ' populate keyboard shortcuts listview control with default keys combinations (from 2 constants)
    Call Initialize_KShortcuts_from_Defaults
Else
    Call Initialize_KShortcuts_from_SettingsFile(Memassignoptions_array)
End If

Me.Top = Me.Top - 1
Me.Top = Me.Top + 1

End Sub


Function Saved_Custom_KeyCombos(Memassignoptions_array) As Boolean

Dim savedCK As String

savedCK = GetOption_fromMemory(Memassignoptions_array, "keyCombinations")

If savedCK <> "" Then
    Saved_Custom_KeyCombos = True
Else
    Saved_Custom_KeyCombos = False
End If


End Function


Function KeyCodeInt_toKeyCodeStr(KeyCode As MSForms.ReturnInteger) As String

Debug.Print ""

End Function


Function get_keyCombo_Name_for_KeyCodeString(KeyCode As String) As String

Dim tryIdx As Integer

tryIdx = Get_Index_ofBDArray_Entry(vbKeyNamesValues, 2, KeyCode)

If tryIdx <> -1 Then
    get_keyCombo_Name_for_KeyCodeString = tryIdx
Else
    get_keyCombo_Name_for_KeyCodeString = -1
End If

End Function


Sub Initialize_KeysPool()


Dim vbKN As Variant
vbKN = Split(vbKeyNames, "|")

Dim vbKV As Variant
vbKV = Split(vbKeyValues, "|")


ReDim vbKeyNamesValues(1, 0)

For i = 0 To UBound(vbKN)
    vbKeyNamesValues(0, i) = vbKN(i)
    vbKeyNamesValues(1, i) = vbKV(i)
    If i < UBound(vbKN) Then ReDim Preserve vbKeyNamesValues(1, i + 1)
Next i


End Sub


Sub Initialize_KShortcuts_from_SettingsFile(Memassignoptions_array)


Dim savedKeyShortcuts As String

savedKeyShortcuts = GetOption_fromMemory(Memassignoptions_array, "keyCombinations")

If savedKeyShortcuts <> "" Then
    'Me.chUseSingleMailTemplate = savedKeyShortcuts
    Call Write_ListView_fromString(Me.lvwKeysCombos, savedKeyShortcuts)
End If


End Sub


Sub Write_ListView_fromString(TargetListView As ListView, SettingsString As String)

' Create array with keys combinations names by splitting constant
Dim arr_keyCombosNames As Variant
arr_keyCombosNames = Split(Split(SettingsString, ":")(0), ",")

' and another one for default keys values
Dim arr_keyCombosDefValues As Variant
arr_keyCombosDefValues = Split(Split(SettingsString, ":")(1), ",")

Call Initialize_and_Write_ListView_from_Arrays(arr_keyCombosNames, arr_keyCombosDefValues)


End Sub


Sub Initialize_KShortcuts_from_Defaults()

' Create array with keys combinations names by splitting constant
Dim arr_keyCombosNames As Variant
arr_keyCombosNames = Split(keyCombosNames, "|")

' and another one for default keys values
Dim arr_keyCombosDefValues As Variant
arr_keyCombosDefValues = Split(keyCombosDefaultValues, "|")

Call Initialize_and_Write_ListView_from_Arrays(arr_keyCombosNames, arr_keyCombosDefValues)

End Sub


Sub Initialize_and_Write_ListView_from_Arrays(ListViewNames, ListViewValues)

' Sanity check
If UBound(ListViewNames) <> UBound(ListViewValues) Then
    Debug.Print "Keyboard_Shortcuts_Initialize: ListViewNames & ListViewValues are unequal, check numbers of elements, split by |"
    Exit Sub
End If

' set up List View as we need it
With Me.lvwKeysCombos
    
    ' first clear existing items and columns
    If .ListItems.count Then
        .ListItems.Clear
        .ColumnHeaders.Clear
    End If
    
    ' Refresh and set report view on listview control
    .Refresh
    .View = lvwReport
    .FullRowSelect = True
    .GridLines = True

    'Create the two columns
    .ColumnHeaders.Add Text:="Action", Width:=118
    .ColumnHeaders.Add Text:="Combination", Width:=100

End With

' and add names and default values to list view control
For i = 0 To UBound(ListViewNames)
    Me.lvwKeysCombos.ListItems.Add Key:=ListViewNames(i) & "_N", Text:=ListViewNames(i)
    Me.lvwKeysCombos.ListItems(i + 1).ListSubItems.Add Key:=ListViewValues(i) & "_V", Text:=ListViewValues(i)
Next i

Call Initialize_KeysPool

End Sub


Sub Sync_AssignOptions_toOptionsFile()

' These following 2 controls are comboboxes, they have no design time values written in.

' We need write the pre-decided values
' Populate date formats list with pre-chosen formats
Call Initialize_DateFormats

' Populate operations too
Call Initialize_Operations


' The next options are not pre-decided, we
Dim ctOptionsFile As New SettingsFileClass
' Lets get current assign options
If ctOptionsFile.IsEmpty = FileNotEmpty Then
    
    Dim savedAssignOptions As Variant
    savedAssignOptions = ctOptionsFile.GetSection("AssignOptions", "ReturnArray")
    
    If IsArray(savedAssignOptions) Then     ' Could not get settings, nothin to do
        
        ' Populate AST users list
        Call Sync_UserLists_Options(savedAssignOptions, "ast")
        ' populate AD user list
        Call Sync_UserLists_Options(savedAssignOptions, "ad")
        
        ' and do same to deadline days and times options
        Call Sync_Deadlines_Options(savedAssignOptions)
        
        ' using copy of concerned settings section saved into memory, sync date formats into options on form to saved user prefs
        Call Sync_DateFormats_Options(savedAssignOptions)
        
        ' and minutes scroll value
        Call Sync_MinutesScroll_Options(savedAssignOptions)
        
        ' time for mail mode option
        Call Sync_MailMode_Option(savedAssignOptions)
        
        ' and send as activation and address options
        Call Sync_SendAs_Options(savedAssignOptions)
        
        ' accent color too
        Call Sync_AccentColor_Option(savedAssignOptions)
        
        ' color me happy
        Me.skipRevertToDefaults.ForeColor = Me.tbAccentColor.BackColor
        
        ' and operations strings, if user chose to mod
        Call Sync_OperationsStrings_Options(savedAssignOptions)
        
        ' and operations accent colors
        Call Sync_OperationsColors_Options(savedAssignOptions)
        
        ' and Initial operation to start on
        Call Sync_InitialOperation_Option(savedAssignOptions)
        
        ' use single GENERAL template instead of activity color shaded ones
        Call Sync_TemplateOption(savedAssignOptions)
        
        ' allow no mans revision option !!!
        Call Sync_NoMansRevisionOption(savedAssignOptions)
        
        ' should we auto scroll time when scrolling days?
        Call Sync_AutoScrollOption(savedAssignOptions)
        
        Call Sync_NoTradTimesInRev(savedAssignOptions)
            
        ' the below routine disables validation if saved corr option is True, regardless (before)
        ' ANY operation is even assigned! NOT what we wanted!
        
        ' TO DO: Find a way to go around this, smth to do with entering/ exiting translation and revision sections,
        ' the only 2 involved into the state of such
        
        ' Call Sync_NoValidationTrad(savedAssignOptions)
                
        ' Fill in custom keys listview control with current key combos
        Call Keyboard_Shortcuts_Initialize(savedAssignOptions)
        
        Call Sync_ManualInputOption(savedAssignOptions)
        
    End If
    
End If

End Sub



Sub Sync_NoValidationTrad(Memassignoptions_array)

Dim savedNoValidation As String
savedNoValidation = GetOption_fromMemory(Memassignoptions_array, "NoTradValidationDeadline")

If savedNoValidation <> "" Then
    Me.chNoTradValidationDeadline = savedNoValidation
End If

End Sub




Sub Sync_NoTradTimesInRev(Memassignoptions_array)

Dim savedNoTimesInRevMail As String

savedNoTimesInRevMail = GetOption_fromMemory(Memassignoptions_array, "NoTradTimesInRev")

If savedNoTimesInRevMail <> "" Then
    Me.chNoTradTimesInRev.Value = savedNoTimesInRevMail
End If

End Sub


Sub Sync_AutoScrollOption(Memassignoptions_array)

Dim savedUseAutoscroll As String
Dim savedScrollToWhat As String

savedUseAutoscroll = GetOption_fromMemory(Memassignoptions_array, "AutoScroll_ToNextDay_Time")
savedScrollToWhat = GetOption_fromMemory(Memassignoptions_array, "AutoScroll_WhatTime")


If savedUseAutoscroll <> "" Then
    Me.chAutoScroll_ToNextDay_Time.Value = savedUseAutoscroll
End If

' IT HAS a default design time value, no need break it !
If savedScrollToWhat <> "" Then
    Me.tbAutoScroll_JumpToWhat.Value = savedScrollToWhat
End If


End Sub


Sub Sync_NoMansRevisionOption(Memassignoptions_array)

Dim savedUseNoMansRevision As String

savedUseNoMansRevision = GetOption_fromMemory(Memassignoptions_array, "AllowNoMansRevision")

If savedUseNoMansRevision <> "" Then
    Me.chAllowNoMansRevision = savedUseNoMansRevision
End If

End Sub



Sub Sync_TemplateOption(Memassignoptions_array)

Dim savedUseSingleTemplate As String

savedUseSingleTemplate = GetOption_fromMemory(Memassignoptions_array, "UseSingleMailTemplate")

If savedUseSingleTemplate <> "" Then
    Me.chUseSingleMailTemplate = savedUseSingleTemplate
End If


End Sub


Sub Format_DeadlineDays()


Dim ctDateFormat As String
ctDateFormat = Me.cbDateformat.Value

' Initialize the 3 operations' deadline day for... today !
' Date format is CONFIGURABLE - Such as in use "June 16" or "16 Iulie" if one wishes
Me.astDeadlineDay.Tag = CaptDate(Format(Date, ctDateFormat))
Me.traDeadlineDay.Tag = CaptDate(Format(Date, ctDateFormat))
Me.revDeadlineDay.Tag = CaptDate(Format(Date, ctDateFormat))
Me.valDeadlineDay.Tag = CaptDate(Format(Date, ctDateFormat))
Me.finDeadlineDay.Tag = CaptDate(Format(Date, ctDateFormat))


End Sub


Sub Format_DeadlineTimes()


Dim UseDifferedTimes As String
UseDifferedTimes = CStr(Me.obTimeUseDTimes.Value)

Dim UseEODTime As String
UseEODTime = CStr(Me.obTimeUseEndOfDay.Value)

Dim UseWhatTime As String
UseWhatTime = IIf(UseDifferedTimes = "True", "UseDifferedTimes", IIf(UseEODTime = "True", "UseEndOfDay", "UseStartOfDay"))


Dim astDTime, tradDtime, revDTime, valDTime, finDTime
If UseWhatTime = "UseDifferedTimes" Then
    
    If Not Check_DifferedTimeFormat_andWarn Then Exit Sub
    
    Dim savedDifferedTimes As String
    savedDifferedTimes = Me.tbTimeSeparation.Value

    astDTime = CInt(Split(savedDifferedTimes, ",")(0)): tradDtime = CInt(Split(savedDifferedTimes, ",")(1))
    revDTime = CInt(Split(savedDifferedTimes, ",")(2)): valDTime = CInt(Split(savedDifferedTimes, ",")(3))
    ' HERE to decide on what the final deadline defaults to !
    ' ()
    finDTime = CInt(Split(savedDifferedTimes, ",")(3))
    
Else    ' End of day chosen
    
    If UseWhatTime = "UseEndOfDay" Then
    
        ' Check completeness of time
        If Not Check_TimeFormat_andWarn Then Exit Sub
        
        UseWhatTime = Me.tbEndOfDayTime.Value
        
        astDTime = 0: tradDtime = 0: revDTime = 0: valDTime = 0: finDTime = 0
    
    Else    ' User chose start of day
        
        ' Check completeness of time
        If Not Check_TimeFormat_andWarn Then Exit Sub
        
        UseWhatTime = Me.tbStartOfDayTime.Value
        
        astDTime = 0: tradDtime = 0: revDTime = 0: valDTime = 0: finDTime = 0

    End If
    
End If


' Initialise deadline times, timeOffset is in minutes
Call Initialise_Deadline_Time(astDeadlineTime, UseWhatTime, CInt(astDTime))
Call Initialise_Deadline_Time(traDeadlineTime, UseWhatTime, CInt(tradDtime))
Call Initialise_Deadline_Time(revDeadlineTime, UseWhatTime, CInt(revDTime))
Call Initialise_Deadline_Time(valDeadlineTime, UseWhatTime, CInt(valDTime))
'Call Initialise_Deadline_Time(finDeadlineTime, UseWhatTime, CInt(finDTime))

End Sub


Function Check_DifferedTimeFormat_andWarn() As Boolean   ' returning False is wrong format string, check and quit in calling code


' Check and stop if we don't have all time differences
If UBound(Split(Me.tbTimeSeparation, ",")) <> 3 Then
    
CheckFailed:

    MsgBox "Please check the time differences string, it must contain 4 increasing (or equal) values separated by commas (" & _
        Chr(34) & "," & Chr(34) & ")", vbOKOnly + vbInformation, "Error in time separations options text!"
    ' switch back to the other time options, this one's faulty!
    'Me.obTimeUseEndOfDay = True    '
    Check_DifferedTimeFormat_andWarn = False
    Exit Function
    
Else
    
    If CInt(Split(Me.tbTimeSeparation, ",")(3)) < CInt(Split(Me.tbTimeSeparation, ",")(2)) Or _
        CInt(Split(Me.tbTimeSeparation, ",")(3)) < CInt(Split(Me.tbTimeSeparation, ",")(1)) Or _
        CInt(Split(Me.tbTimeSeparation, ",")(3)) < CInt(Split(Me.tbTimeSeparation, ",")(0)) Then
        
        GoTo CheckFailed
            
    Else
        
        If CInt(Split(Me.tbTimeSeparation, ",")(2)) < CInt(Split(Me.tbTimeSeparation, ",")(1)) Or _
            CInt(Split(Me.tbTimeSeparation, ",")(2)) < CInt(Split(Me.tbTimeSeparation, ",")(0)) Then
            
            GoTo CheckFailed
        Else
            If CInt(Split(Me.tbTimeSeparation, ",")(1)) < CInt(Split(Me.tbTimeSeparation, ",")(0)) Then
                GoTo CheckFailed
            Else
                Check_DifferedTimeFormat_andWarn = True
            End If
        End If
    End If
End If


End Function


Function Check_TimeFormat_andWarn() As Boolean      ' returning False is wrong format string, check and quit in calling code

' Guard against wrong time string format! (presence of semicolon)
If InStr(1, Me.tbEndOfDayTime.Value, ":") = 0 Then
    MsgBox "Please check and correct time value for " & Chr(34) & "End of day time" & Chr(34) & " options textbox!" & vbCr & vbCr & _
        "It must contain a time value of format " & Chr(34) & "00:00" & Chr(34) & "!", vbOKOnly + vbInformation, "Error in time definition!"
    'Me.obTimeUseDTimes.Value = True
    Check_TimeFormat_andWarn = False
    Exit Function
End If

' Guard against wrong time format (
If UBound(Split(Me.tbEndOfDayTime.Value, ":")) <> 1 Then
    MsgBox "Please check and correct time value for " & Chr(34) & "End of day time" & Chr(34) & " options textbox!" & vbCr & vbCr & _
        "It must contain a time value of format " & Chr(34) & "00:00" & Chr(34) & "!", vbOKOnly + vbInformation, "Error in time definition!"
    'Me.obTimeUseDTimes.Value = True
    Check_TimeFormat_andWarn = False
    Exit Function
    
Else    ' Even if time string does contain the semicolon, we must still check for two-digit values!

    If Len(Me.tbEndOfDayTime.Value) <> 5 Then
        MsgBox "Please check and correct time value for " & Chr(34) & "End of day time" & Chr(34) & " options textbox." & vbCr & vbCr & _
        "Hour and minutes values must both be 2 digits, please. (" & Chr(34) & "00:00" & Chr(34) & ")", vbOKOnly + vbInformation, "Error in time definition!"
        'Me.obTimeUseDTimes.Value = True
        Check_TimeFormat_andWarn = False
        Exit Function
    End If

End If

' both checks being done, the string correct it is, we set function to True
Check_TimeFormat_andWarn = True

End Function


Sub Format_Accents()

' Extract preff accent color
Dim savedPrefColor As String
savedPrefColor = Me.tbAccentColor.Value

' and paint' em!
Me.lblClockBack.BackColor = savedPrefColor
Me.lblClockLine.BackColor = savedPrefColor
Me.lblValidationLine.BackColor = savedPrefColor
Me.lblValidationVertLine.BackColor = savedPrefColor
Me.lblOptionsLine.BackColor = savedPrefColor
Me.skipLblKeyCombosRevert.ForeColor = savedPrefColor

End Sub


Sub Initialize_Users()

'and users comboboxes
Call Initialize_UsersComboboxes("ast")
Call Initialize_UsersComboboxes("ad")

End Sub


Sub Restore_AssignForm_UserPreferences()


'Debug.Print "Restore_AssignForm_UserPreferences has been RAN!"

' To be assigned document(s).
' DEACTIVATED, We solve it in "Activate" event handler
'Call Initialize_tbaDocuments

' display user-chosen date format into deadline days text boxes
Call Format_DeadlineDays

' display user-chosen time formats for deadlines into proper tbs
Call Format_DeadlineTimes

' and initialize the current mail mode to the saved default
Call Initialize_MailMode

' and display accent colors everywhere
Call Format_Accents

' and users combos
Call Initialize_Users

' as well as deadline times disabling pinning state
Call Initialize_NoTimes_State

' and set validation deadline time state acc to user pref
Call Initialize_NoValForTrad_State

Call Initialize_ManualInput_Option

'Call Paint_Operations_inUserColors

End Sub


Sub Initialize_NoValForTrad_State()


If Me.chNoTradValidationDeadline Then
    Call DeadlineDeactivate_ProcessEvents(1, 0, Me.valNoTimeLbl, Me.valDeadlineTime, wdColorDarkRed, wdColorGray25)
End If

End Sub

Sub Initialize_MailMode()

Dim ctDefMM As String
ctDefMM = Me.tbMailMode.Value

If ctDefMM = "One Msg" Then
    Call MailMode_Single_Activate
ElseIf ctDefMM = "Mult Msg" Then
    Call MailMode_Multiple_Activate
End If

End Sub

Sub Initialize_PreFilled_Sections()


If Me.astInitData <> "" Then
    'Call Activate_Section(EnabledState:=True, SectionName:="ast", ColorCode:=Me.PrepAccentColor)
    'If Me.traCbo.Enabled Then Me.traCbo.SetFocus
    Me.astCbo = Split(astInitData, "|")(0)
    Me.astCbo.ForeColor = wdColorGray50
    
    Me.astDeadlineDay = Format(Split(Split(astInitData, "|")(1), " - ")(0), Me.cbDateformat)
    
    ' only attempt to set deadline time if time was present in source string
    If InStr(1, Split(astInitData, "|")(1), "-") > 0 Then
        Me.astDeadlineTime = Split(Split(astInitData, "|")(1), " - ")(1)
    End If
    
End If


If Me.traInitData <> "" Then
    
    If InStr(1, traInitData, "|") > 0 Then
    
        'Call Activate_Section(EnabledState:=True, SectionName:="tra", ColorCode:=Me.TraAccentColor)
        'If Me.traCbo.Enabled Then Me.traCbo.SetFocus
        Me.traCbo = Split(traInitData, "|")(0)
        Me.traCbo.ForeColor = wdColorGray50
    
        Me.traDeadlineDay = Format(Split(Split(traInitData, "|")(1), " - ")(0), Me.cbDateformat)
        
        ' only attempt to set deadline time if time was present in source string!
        If InStr(1, Split(traInitData, "|")(1), "-") > 0 Then
            Me.traDeadlineTime = Split(Split(traInitData, "|")(1), " - ")(1)
        End If
        
        
    Else
        
    End If
    
End If


End Sub


Sub Initialize_NoTimes_State()

Call Set_NoTimes_PinnedState(Me.tgNoTimes.Value)

End Sub


Sub Sync_InitialOperation_Option(ByRef Memassignoptions_array)

Dim savedInitOp As String
savedInitOp = GetOption_fromMemory(Memassignoptions_array, "InitialOperation")

If savedInitOp <> "" Then
    Me.cbInitialOperation.Value = savedInitOp
End If


End Sub


Sub Paint_Operations_inUserColors()


Me.astColorLbl1.BackColor = Me.PrepAccentColor
Me.astColorLbl2.BackColor = Me.PrepAccentColor
Me.astCbo.BackColor = Me.PrepAccentColor

Me.traColorLbl1.BackColor = Me.TraAccentColor
Me.traColorLbl2.BackColor = Me.TraAccentColor
Me.traCbo.BackColor = Me.TraAccentColor

Me.revCbo.BackColor = Me.RevAccentColor
Me.revColorLbl1.BackColor = Me.RevAccentColor
Me.revColorLbl2.BackColor = Me.RevAccentColor


End Sub

Sub Sync_OperationsStrings_Options(ByRef Memassignoptions_array)

Dim svPStr, svTStr, svRStr, svTRStr

svPStr = GetOption_fromMemory(Memassignoptions_array, "PreparationMessage")
svTStr = GetOption_fromMemory(Memassignoptions_array, "TranslationMessage")
svRStr = GetOption_fromMemory(Memassignoptions_array, "RevisionMessage")
svTRStr = GetOption_fromMemory(Memassignoptions_array, "Translation&RevisionMessage")

If svPStr <> "" Then
    If Me.tbPrepMsg <> svPStr Then Me.tbPrepMsg.Value = svPStr
End If
If svTStr <> "" Then
    If Me.tbTraMsg <> svTStr Then Me.tbTraMsg.Value = svTStr
End If
If svRStr <> "" Then
    If Me.tbRevMsg <> svRStr Then Me.tbRevMsg.Value = svRStr
End If
If svTRStr <> "" Then
    If Me.tbTraRevMsg <> svTRStr Then Me.tbTraRevMsg.Value = svTRStr
End If

End Sub


Sub Sync_ManualInputOption(ByRef Memassignoptions_array)

Dim ctManualOption As String

ctManualOption = GetOption_fromMemory(Memassignoptions_array, "DeadlineManualInput")

If ctManualOption <> "" Then
    Me.chDeadlineManualInput.Value = CBool(ctManualOption)
Else    ' design time default is false anyway...
End If

End Sub


Sub Initialize_ManualInput_Option()

If Me.chDeadlineManualInput.Value = True Then
    Call UnlockDeadlines(False)
Else
    Call UnlockDeadlines(True)
End If

End Sub


Sub Sync_OperationsColors_Options(ByRef Memassignoptions_array)

Dim svPColor, svTColor, svRColor, svTRColor

svPColor = GetOption_fromMemory(Memassignoptions_array, "PreparationAccentColor")
svTColor = GetOption_fromMemory(Memassignoptions_array, "TranslationAccentColor")
svRColor = GetOption_fromMemory(Memassignoptions_array, "RevisionAccentColor")
svTRColor = GetOption_fromMemory(Memassignoptions_array, "Trad&RevAccentColor")

' now sync desired color codes to host labels
If svPColor <> "" Then
    If Me.PrepAccentColor.Value <> svPColor Then Me.PrepAccentColor.Value = svPColor
    If Me.tbPrepMsg.BackColor <> svPColor Then Me.tbPrepMsg.BackColor = svPColor
End If

If svTColor <> "" Then
    If Me.TraAccentColor.Value <> svTColor Then Me.TraAccentColor.Value = svTColor
    If Me.tbTraMsg.BackColor <> svTColor Then Me.tbTraMsg.BackColor = svTColor
End If

If svRColor <> "" Then
    If Me.RevAccentColor.Value <> svRColor Then Me.RevAccentColor.Value = svRColor
    If Me.tbRevMsg.BackColor <> svRColor Then Me.tbRevMsg.BackColor = svRColor
End If

If svTRColor <> "" Then
    If Me.TraRevAccentColor.Value <> svTRColor Then Me.TraRevAccentColor.Value = svTRColor
    If Me.tbTraRevMsg.BackColor <> svTRColor Then Me.tbTraRevMsg.BackColor = svTRColor
End If


End Sub


Sub Sync_DateFormats_Options(ByRef Memassignoptions_array)

Dim ctDateFormat As String
'ctDateFormat = ctOptionsFile.getOption("DateFormat", "AssignOptions")
ctDateFormat = GetOption_fromMemory(Memassignoptions_array, "DateFormat")
' somehow, the above "savedAssignOptions" call does return an array, 2 arms and one element each, but empty!
' INVESTIGATE!
If ctDateFormat <> "" Then
    If Me.cbDateformat.Value <> ctDateFormat Then Me.cbDateformat.Value = ctDateFormat
End If


Dim useMailDateFormat As String
useMailDateFormat = GetOption_fromMemory(Memassignoptions_array, "FreezeMailDateFormat")

If useMailDateFormat = "True" Then
    
    Me.chFreezeMailDateFormat.Value = True
    
    'If Not Me.cbMailDateFormat.Enabled Then Me.cbMailDateFormat.Enabled = True
    
    Dim ctMailDateFormat As String
    ctMailDateFormat = GetOption_fromMemory(Memassignoptions_array, "MailDateFormat")

    If ctMailDateFormat <> "" Then
        If Me.cbMailDateFormat <> ctMailDateFormat Then Me.cbMailDateFormat = ctMailDateFormat
    
    End If
    
End If


End Sub


Sub Sync_Deadlines_Options(ByRef Memassignoptions_array)


Dim UseDifferedTimes As String
UseDifferedTimes = GetOption_fromMemory(Memassignoptions_array, Me.obTimeUseDTimes.Tag)

Dim UseEndOfDay As String
UseEndOfDay = GetOption_fromMemory(Memassignoptions_array, Me.obTimeUseEndOfDay.Tag)

'
Dim UseWhatTime As String
UseWhatTime = IIf(UseDifferedTimes = "True", "UseDifferedTimes", IIf(UseEndOfDay = "True", "UseEndOfDay", "UseStartOfDay"))


' update differed times combobox and textbox acc to user prefs
If UseWhatTime = "UseDifferedTimes" Then
    
    ' update option button
    Me.obTimeUseDTimes.Value = True
    
    Dim savedDifferedTimes As String
    savedDifferedTimes = GetOption_fromMemory(Memassignoptions_array, "TimeSeparations")
    
    ' overwrite time differences values if user saved others
    If savedDifferedTimes <> "" Then
        If Me.tbTimeSeparation.Value <> savedDifferedTimes Then Me.tbTimeSeparation.Value = savedDifferedTimes
    End If
    
Else    ' User chose to use EOD
    
   If UseWhatTime = "UseEndOfDay" Then
    
        ' update option button
        Me.obTimeUseEndOfDay.Value = True
        
        Dim savedEODTime As String
        
        savedEODTime = GetOption_fromMemory(Memassignoptions_array, "EndOfDayTime")
        
        ' overwrite End Of Time form option if needed
        If savedEODTime <> "" Then
            If Me.tbEndOfDayTime.Value <> savedEODTime Then Me.tbEndOfDayTime.Value = savedEODTime
        End If
    
    Else    ' User chose Start of Day
        
        ' update option button
        Me.obTimeUseStartOfDay.Value = True
        
        Dim savedSODTime As String
        
        savedSODTime = GetOption_fromMemory(Memassignoptions_array, "StartOfDayTime")
        
        ' overwrite Start Of Time form option if needed
        If savedSODTime <> "" Then
            If Me.tbStartOfDayTime.Value <> savedSODTime Then Me.tbStartOfDayTime.Value = savedSODTime
        End If

        
    End If
    
End If

' And disable all times if case be
Dim tempNoTimes As String
tempNoTimes = GetOption_fromMemory(Memassignoptions_array, Me.lblNoTimes.Tag)

If tempNoTimes <> "" Then
    
    If tempNoTimes = "True" Then
        Call Flip_NoDeadline_Times
    Else
    End If
    
End If


End Sub


Sub Sync_MailMode_Option(ByRef Memassignoptions_array)

 Dim savedDefMailMode As String   ' mail mode
' extract currently saved default mail mode
savedDefMailMode = GetOption_fromMemory(Memassignoptions_array, "DefaultMailMode")

'
If savedDefMailMode <> "" Then
    ' we already saved there the design time default, no need use literals...
    If Me.tbMailMode.Value <> savedDefMailMode Then Me.tbMailMode.Value = savedDefMailMode
End If

End Sub


Sub Sync_SendAs_Options(ByRef Memassignoptions_array)

' extract and initialize send-as state and address
Dim savedSendAsActive As String
savedSendAsActive = GetOption_fromMemory(Memassignoptions_array, "UseSendAs")

Dim savedSendAsAddress As String
savedSendAsAddress = GetOption_fromMemory(Memassignoptions_array, "SendAsAddress")

' and put it in the form if needed
If savedSendAsActive <> "" Then
    If Me.chUseSendAs <> savedSendAsActive Then Me.chUseSendAs.Value = savedSendAsActive
    
    ' if user chose to use special send-as address then put it up in the form
    If savedSendAsActive = "True" Then
        If Me.tbSendAsAddress.Value <> savedSendAsAddress Then Me.tbSendAsAddress = savedSendAsAddress
    End If
    
End If

End Sub


Sub Sync_AccentColor_Option(Memassignoptions_array)

' Extract preffered accent color and put it up in options accent color option
Dim savedPrefColor As String
savedPrefColor = GetOption_fromMemory(Memassignoptions_array, "AccentColorCode")
' put user prefd accent color up in options in form
If savedPrefColor <> "" Then
    If Me.tbAccentColor.Value <> savedPrefColor Or Me.tbAccentColor.BackColor <> savedPrefColor Then
        Me.tbAccentColor.Value = savedPrefColor
        Me.tbAccentColor.BackColor = savedPrefColor
    End If
End If


End Sub


Sub Initialize_Operations()

Dim operations As Variant
operations = Array("Preparation", "Translation", "Revision")
'Me.cbInitialOperation.AddItem (operations(0)): Me.cbInitialOperation.AddItem (operations(1))
'Me.cbInitialOperation.AddItem (operations(2))
Me.cbInitialOperation.List = operations
Me.cbInitialOperation.ListIndex = 0   ' default

End Sub


Sub Sync_UserLists_Options(ByRef Memassignoptions_array, WhichList As String)
' In this one, we only target the two text boxes in options containing the ast and ad users, comma separated

   
Dim savedCtUsers As String

savedCtInits = GetOption_fromMemory(Memassignoptions_array, IIf(WhichList = "ast", "AstInitList", "AdInitList"))
savedCtUsers = GetOption_fromMemory(Memassignoptions_array, IIf(WhichList = "ast", "AstUsersList", "AdUsersList"))

If savedCtUsers <> "" And savedCtUsers <> "" Then
    If WhichList = "ast" Then
        If Me.tbAstInitList.Value <> savedCtInits Then Me.tbAstInitList.Value = savedCtInits
        If Me.tbAstUsersList.Value <> savedCtUsers Then Me.tbAstUsersList.Value = savedCtUsers
    Else
        If Me.tbAdInitList.Value <> savedCtInits Then Me.tbAdInitList.Value = savedCtInits
        If Me.tbAdUsersList.Value <> savedCtUsers Then Me.tbAdUsersList.Value = savedCtUsers
    End If
End If
    

End Sub


Sub Sync_MinutesScroll_Options(ByRef MemAssignsOptions_Array)

Dim savedMinsScr As String

savedMinsScr = GetOption_fromMemory(MemAssignsOptions_Array, "MinutesScroll")

If savedMinsScr <> "" Then
    If Me.tbMinsScroll.Value <> savedMinsScr Then Me.tbMinsScroll.Value = savedMinsScr
End If

End Sub


Sub Initialize_SettingsFile()


Dim ctopt As New SettingsFileClass

If ctopt.IsEmpty = ErrorFindingFile Then
    ctopt.Create_Empty
End If

Me.OptionsFile = ctopt

End Sub


Function CaptDate(DateString As String) As String
' Capitalise date string, accepts short dates, "1 jan" or "1 january" or "jan 1" or "january 1"
' and capitalizes the month word

Dim dateSep As Variant

dateSep = Split(DateString, " ")

For i = 0 To UBound(dateSep)
    If dateSep(i) Like "[a-z]*" Then
        dateSep(i) = UCase(Left(dateSep(i), 1)) & Mid(dateSep(i), 2)
    Else
    End If
Next i

Dim CD As String

For j = 0 To UBound(dateSep)
    CD = IIf(CD = "", dateSep(j), CD & " " & dateSep(j))
Next j

CaptDate = CD
    
End Function


Sub Initialise_Deadline_Time(TargetTextbox As TextBox, UseWhatTime As String, timeOffset As Integer)

If UseWhatTime = "UseDifferedTimes" Then
    ' Initialize the operations' time to current time + 30 minutes (some of these intervals should be user Configurable!)
    Dim minutesDifference As String
    minutesDifference = timeOffset + (5 * (Int(DatePart("n", Time) / 5) + 1)) - DatePart("n", Time)
    '
    Dim deadlineProposedTime As String
    deadlineProposedTime = DateAdd("n", minutesDifference, Format(Time, "hh:nn"))
    '
    TargetTextbox.Tag = Format(deadlineProposedTime, "hh:nn")
    If TargetTextbox.Enabled And TargetTextbox.Text <> "" Then TargetTextbox.Text = TargetTextbox.Tag
    'Me.astDeadlineMinutes = Format(deadlineProposedTime, "nn")    ' Round to next five mins before adding the 15 mins!
Else
    TargetTextbox.Tag = UseWhatTime
    If TargetTextbox.Enabled And TargetTextbox.Text <> "" Then TargetTextbox.Text = TargetTextbox.Tag
End If

End Sub


Function GetAllStaff() As Variant

' Split the proper options values with unit members initials and user names into array
Dim astInitList As Variant, adInitList As Variant ' Staff Initials, all
Dim astUserList As Variant, adUserList As Variant

' We guard against having no input for AST inits, AD inits or win usernames
If Me.tbAstInitList = "" Or InStr(1, Me.tbAstInitList, ",") = 0 Or _
    Me.tbAdInitList = "" Or InStr(1, Me.tbAdInitList, ",") = 0 Or _
    Me.tbAstUsersList = "" Or InStr(1, Me.tbAstUsersList, ",") = 0 Or _
    Me.tbAdUsersList = "" Or InStr(1, Me.tbAdUsersList, ",") = 0 Then
    
        MsgBox "Error in AST or AD Initials or Windows user names lists! Please check options and correct!" & vbCr & vbCr & _
            "One (or more) of the users lists is empty or do not contain the separator character (" & Chr(34) & "," & Chr(34) & _
            ") !", vbOKOnly + vbCritical, "Users lists error!"
        GetAllStaff = Null
        Exit Function
        
Else    ' And we split, but we guard against different numbers of user initials/ usernames
        
    astInitList = Split(Me.tbAstInitList, ","): adInitList = Split(Me.tbAdInitList, ",")
    astUserList = Split(Me.tbAstUsersList, ","): adUserList = Split(Me.tbAdUsersList, ",")
    
    ' check baby, check!
    If UBound(astInitList) <> UBound(astUserList) Then
        MsgBox "Please check AST initials textbox and AST user names textbox, they contain a different number of separators!" & vbCr & vbCr & _
            "The two strings are expected to contain the Workflow initials and the Windows user names, separated by commas (" & Chr(34) & Chr(44) & Chr(34) & "), no spaces.", _
                vbOKOnly + vbCritical, "AST users error!"
        GetAllStaff = Null
        Exit Function
    End If
    '
    If UBound(adInitList) <> UBound(adUserList) Then
        MsgBox "Please check AD initials textbox and AST user names textbox, the number of commas is different!" & vbCr & vbCr & _
            "The two strings are expected to contain the Workflow initials and the Windows user names, separated by commas (" & Chr(34) & Chr(44) & Chr(34) & "), no spaces.", _
                vbOKOnly + vbCritical, "AD users error!"
        GetAllStaff = Null
        Exit Function
    End If
    
End If


Dim staffList() As String
' And mash them together into bidimensional array
ReDim staffList(1, UBound(astInitList) + UBound(adInitList) + 1)
' which you fill up nicely
For i = 0 To UBound(astInitList)
    staffList(0, i) = astInitList(i): staffList(1, i) = astUserList(i)
Next i
'
For i = UBound(astInitList) + 1 To UBound(staffList, 2)
    staffList(0, i) = adInitList(i - (UBound(astInitList) + 1)): staffList(1, i) = adUserList(i - (UBound(astInitList) + 1))
Next i


GetAllStaff = staffList

End Function


Function GetProperTemplate(OperationsCode As Integer) As String

' define templates folder
Dim templatesFolder As String

' and initialize it
If Application.Name = "Outlook" Then
    templatesFolder = Environ("APPDATA") & "\Microsoft\Templates\"
ElseIf Application.Name = "Microsoft Word" Then
    templatesFolder = Application.Options.DefaultFilePath(wdUserTemplatesPath)
End If

Dim properTemplate As String

' Gabi required, skip using personalised colored templates, use a general one, all colored
If Me.chUseSingleMailTemplate Then
    properTemplate = templatesFolder & "\" & MailTemplateRoot & "7.oft"
    
Else
    ' and decide which one depending on operations code used (code 3 and 5 don't have own template - rarely if ever used cases)
    
    ' exception for no mans revision mode, when deadline for rev is allowed even if no revisor !!!
    If Me.chAllowNoMansRevision.Value = "True" Then
        If (OperationsCode = 1 Or OperationsCode = 2 Or OperationsCode = 3) And _
            (Me.revCbo.Text = "" And (Me.revDeadlineDay.Enabled Or Me.revDeadlineTime.Enabled)) Then
            
                properTemplate = templatesFolder & "\" & MailTemplateRoot & OperationsCode & "_NMR.oft"
                
        Else
            properTemplate = templatesFolder & "\" & MailTemplateRoot & OperationsCode & ".oft"
        End If
    Else
        properTemplate = templatesFolder & "\" & MailTemplateRoot & OperationsCode & ".oft"
    End If
    
End If

' Should templates not exist, we are forced to forfeit
If Dir(properTemplate) = "" Then
    MsgBox "Outlook template " & vbCr & properTemplate & _
        " does not exist. Please place the proper templates in their location and retry." & vbCr & _
        "Call margama for help if needed (2942)"
    Exit Function
End If

GetProperTemplate = properTemplate

End Function


Function CreateOutlookApplication() As Outlook.Application

Dim outApp As Outlook.Application
'
If Application.Name = "Outlook" Then
    Set outApp = Application
Else    ' in Word
' Start a new Outlook App
    Set outApp = New Outlook.Application
End If

Set CreateOutlookApplication = outApp

End Function


Function CreateNewOutlookMail(Outlook_Application As Outlook.Application, OperationsCode As Integer) As Outlook.MailItem


' Define an Outlook template path string
Dim properTemplate As String
properTemplate = GetProperTemplate(OperationsCode)

' and create a new mail message
Dim newmail As Outlook.MailItem
' now bring new mail message to life, create it using the template
Set newmail = Outlook_Application.CreateItemFromTemplate(properTemplate)


Set CreateNewOutlookMail = newmail

End Function


Sub SetMailMsg_Subject(TargetMailMsg As Outlook.MailItem, recipientWFInitials As String, OperationsCode As Integer)


Dim prepMsg, tradMsg, revMsg, trad_revMsg

prepMsg = Me.tbPrepMsg: tradMsg = Me.tbTraMsg
revMsg = Me.tbRevMsg: trad_revMsg = Me.tbTraRevMsg

' Now on to define message subject, from field (if activated)
With TargetMailMsg
                
    ' If user asked us to send "One Msg" to multiple destinators, we listen and add all of em
    If InStr(1, recipientWFInitials, ",") > 0 Then
        
        Select Case OperationsCode
            
            ' single activity codes cases MAY appear even multiple destinataries! (Multiple people assignments!)
            Case 1  ' Preparation
                .Subject = Me.tbDocuments & " - " & prepMsg
            Case 2  ' Translation
                .Subject = Me.tbDocuments & " - " & tradMsg
            Case 4  ' Revision
                .Subject = Me.tbDocuments & " - " & revMsg

            Case 3  ' unlikely, prep & trad
                .Subject = Me.tbDocuments & " - " & prepMsg & " & " & tradMsg
            Case 5  'unlikely, prep & rev
                .Subject = Me.tbDocuments & " - " & prepMsg & " & " & revMsg
            Case 6  ' likely, trad & rev
                .Subject = Me.tbDocuments & " - " & tradMsg & " & " & revMsg
            Case 7  ' kinda likely, prep + trad + rev
                .Subject = Me.tbDocuments & " - " & prepMsg & ", " & tradMsg & " & " & revMsg
                
        End Select
        
    Else    ' or just the one, for the mode "Mult Msg"
        
        Select Case OperationsCode
            
            Case 1
                .Subject = Me.tbDocuments & " - " & Me.tbPrepMsg
            Case 2
                .Subject = Me.tbDocuments & " - " & Me.tbTraMsg
            Case 4
                .Subject = Me.tbDocuments & " - " & Me.tbRevMsg
            Case 6  ' one user only, two operations: trad+rev
                .Subject = Me.tbDocuments & " - " & Me.tbTraRevMsg
                
        End Select
        
    End If
    
End With


End Sub


Sub SetMailMsg_Recipients(TargetMailMsg As Outlook.MailItem, recipientWFInitials As String, staffList As Variant, Optional ccTo)


With TargetMailMsg

    ' If user asked us to send "One Msg" to multiple destinators, we listen and add all of em
    If InStr(1, recipientWFInitials, ",") > 0 Then
            
        Dim tryIdx As Integer
        
        ' and add recipients
        For k = 0 To UBound(Split(recipientWFInitials, ","))
            
            tryIdx = GetArray_Index_forValue(staffList, CStr(Split(recipientWFInitials, ",")(k)))
            
                    ' if sought user initials are not in storage...
            If tryIdx = -1 Then    ' if index is real, meaning current user was found
                MsgBox CStr(Split(recipientWFInitials, ",")(k)) & " is not in either group in users list (see Options)!" & vbCr & vbCr & _
                "Please be aware that this user will be missing from recipients in the email message(s)!", vbExclamation, "User not known!"
            End If
            
            ' and add recipient if
            If tryIdx <> -1 Then
                .Recipients.Add staffList(1, tryIdx)
            End If
            
            ' and add optional copy carbon if user used it
            If Not IsMissing(ccTo) Then
                
                If ccTo <> "" Then
                
                    Dim ccRec As Outlook.Recipient
                    
                    Set ccRec = .Recipients.Add(ccTo)
                    ccRec.Type = olCC
                
                End If
                
            End If
            
        Next k
        
    Else    ' or just the one, for the mode "Mult Msg"
        
        ' and get the index of the desired user name from array corresponding to his/ her initials, supplied by user
        tryIdx = GetArray_Index_forValue(staffList, recipientWFInitials)
        
        ' if sought user initials are not in storage...
        If tryIdx = -1 Then    ' if index is real, meaning current user was found
            MsgBox recipientWFInitials & " is not in either group in users list (see Options)!" & vbCr & vbCr & _
                "Please be aware that this user will be missing from recipients in the email message(s)!", vbExclamation, "User not known!"
        End If
        
        If tryIdx <> -1 Then
            .Recipients.Add staffList(1, tryIdx)
        End If
        
        ' and add optional copy carbon if user used it
        If Not IsMissing(ccTo) Then
            
            If ccTo <> "" Then
            
                'Dim ccRec As Outlook.Recipient
                
                Set ccRec = .Recipients.Add(ccTo)
                ccRec.Type = olCC
            
            End If
            
        End If
        
    End If
    
End With

End Sub


Sub Setup_MailMessage(TargetMailMsg As Outlook.MailItem, recipientWFInitials As String, OperationsCode As Integer, staffList As Variant, ccTo)

' Add recipients
Call SetMailMsg_Recipients(TargetMailMsg, recipientWFInitials, staffList, ccTo)
' and subject
Call SetMailMsg_Subject(TargetMailMsg, recipientWFInitials, OperationsCode)
' as well as send as option
Call SetMailMsg_SendAs(TargetMailMsg)

End Sub

Sub SetMailMsg_SendAs(TargetMailMsg As Outlook.MailItem)

' Error message in case of lack of permissions !
Dim ctSendAs As Variant
ctSendAs = Me.chUseSendAs

If ctSendAs = True Then
    
    Dim ctSendAsAddress As String
 
    ctSendAsAddress = Me.tbSendAsAddress
    
    TargetMailMsg.SentOnBehalfOfName = ctSendAsAddress

Else
    
    TargetMailMsg.SentOnBehalfOfName = GetCtUserAddress
    
End If

End Sub

Sub Mail_CtDoc_forActivity(recipientsWFInitials As String, Prep_tra_AndOr_Rev As Integer, Optional ccTo)

' Prep_tra_AndOr_Rev values:
'
' 1 for AST only
' 2 for tra only
' 4 for REV only
' 3 for AST & tra
' 5 for AST & REV
' 6 for tra & REV
' 7 for all

' create new outlook app, need for later
Dim outApp As Outlook.Application
Set outApp = CreateOutlookApplication

' string together all initials and set up in bidimensional array
Dim staffList As Variant
staffList = GetAllStaff

' create new mail message
Dim newmail As Outlook.MailItem
Set newmail = CreateNewOutlookMail(outApp, Prep_tra_AndOr_Rev)

' add recipients, subject and send as option
Call Setup_MailMessage(newmail, recipientsWFInitials, Prep_tra_AndOr_Rev, staffList, ccTo)

' and show message
newmail.Display

' time to insert data into it (users, deadlines)
Call Insert_MailData(outApp, newmail)


End Sub


Sub Insert_MailData(OutlookApp As Outlook.Application, TargetMailMsg As Outlook.MailItem)

' now after email display we need to fill in all data (initials, deadlines)
Dim actInsp As Outlook.Inspector
' so we get the inspector object (application which hosts email message, use to get mail body)
Set actInsp = OutlookApp.ActiveInspector

' and the message body object
Dim messageBody As Word.Document
Set messageBody = actInsp.WordEditor

Dim ctBk As Bookmark
' and we loop through its bookmarks
If messageBody.Bookmarks.count > 0 Then
    
    Dim DayFormat As String
    DayFormat = IIf(Me.chFreezeMailDateFormat = True, Me.cbMailDateFormat, Me.cbDateformat)
    
    If Me.astCbo <> "" Then
        ' set ast inits
        messageBody.Bookmarks("astInit").Range.InsertBefore (Me.astCbo)
        ' and delete the three dots placeholder without destroying bookmark!
        messageBody.Bookmarks("astInit").Range.Characters(Len(Me.astCbo) + 1) = ""
        
        Dim ctDeadline As String
        ctDeadline = Format(Me.astDeadlineDay, DayFormat) & IIf(Me.astDeadlineTime.Enabled, " - " & Me.astDeadlineTime, "")
        
        messageBody.Bookmarks("astDeadline").Range.InsertBefore (ctDeadline)
        ' and blacnk three dots placeholder without destroying bookmark!
        messageBody.Bookmarks("astDeadline").Range.Characters(Len(ctDeadline) + 1) = ""
        
    End If
    
    If Me.traCbo <> "" Then
        messageBody.Bookmarks("tradInit").Range.InsertBefore (Me.traCbo)
        'and delete the three dots placeholder
        messageBody.Bookmarks("tradInit").Range.Characters(Len(Me.traCbo) + 1) = ""
        
        Dim tradd As String
        
        ' for true revision assignment, take into account "no translation times in rev mail" option
        If IsActivatedSection("rev") And Me.revCbo <> "" Then
            tradd = IIf(Me.chNoTradTimesInRev = True, "", (IIf(InStr(1, Me.traInitData, "-") > 0, " - " & Me.traDeadlineTime, "")))
        Else    ' but for other cases (translation assignment for instance), DISREGARD it !
            tradd = IIf(Me.traDeadlineTime.Enabled, " - " & Me.traDeadlineTime, "")
        End If
        
        
        ' get together combined deadline string
        'ctDeadline = Format(Me.traDeadlineDay, DayFormat) & IIf(Me.traDeadlineTime.Enabled, " - " & Me.traDeadlineTime, "")
        ctDeadline = Format(Me.traDeadlineDay, DayFormat) & tradd
        
        messageBody.Bookmarks("tradDeadline").Range.InsertBefore (ctDeadline)
        messageBody.Bookmarks("tradDeadline").Range.Characters(Len(ctDeadline) + 1) = ""
        
    End If
    
    If Me.chAllowNoMansRevision Then
                
        If Me.revDeadlineDay.Enabled Then
            
            ctDeadline = Format(Me.revDeadlineDay, DayFormat) & IIf(Me.revDeadlineTime.Enabled, " - " & Me.revDeadlineTime, "")
            'ctDeadline = Format(Me.revDeadlineDay, DayFormat) & tradd
        
            messageBody.Bookmarks("revDeadline").Range.InsertBefore (ctDeadline)
            messageBody.Bookmarks("revDeadline").Range.Characters(Len(ctDeadline) + 1) = ""
            
            ' on "allow no mans revision" setting, we only insert rev person if one is supplied... no worries
            If Me.revCbo <> "" Then
                messageBody.Bookmarks("revInit").Range.InsertBefore (UCase(Me.revCbo))
                messageBody.Bookmarks("revInit").Range.Characters(Len(Me.revCbo) + 1) = ""
            End If

            
        End If
        
    Else
    
        If Me.revCbo <> "" Then
            
            messageBody.Bookmarks("revInit").Range.InsertBefore (UCase(Me.revCbo))
            messageBody.Bookmarks("revInit").Range.Characters(Len(Me.revCbo) + 1) = ""
            
            ctDeadline = Format(Me.revDeadlineDay, DayFormat) & IIf(Me.revDeadlineTime.Enabled, " - " & Me.revDeadlineTime, "")
            
            messageBody.Bookmarks("revDeadline").Range.InsertBefore (ctDeadline)
            messageBody.Bookmarks("revDeadline").Range.Characters(Len(ctDeadline) + 1) = ""
            
        End If
    
    End If
    
    ' only write validation deadline if it's case
    If Me.traCbo <> "" Or Me.revCbo <> "" Then
        
        ' some users may choose to NOT show validation deadline if we're assigning anything else than
        If Me.chNoTradValidationDeadline Then
            
            ' if user chooses "no validation deadline for trad" - we're then only filling validation deadl in case of rev assignment!
            If Me.revCbo <> "" And (Not IsActivatedSection("tra") And Not IsActivatedSection("ast")) Then
                
                ctDeadline = Format(Me.valDeadlineDay, DayFormat) & IIf(Me.valDeadlineTime.Enabled, " - " & Me.valDeadlineTime, "")
        
            Else
            
                ctDeadline = Format(Me.valDeadlineDay, DayFormat)
                
            End If
            
        Else
        
            ctDeadline = Format(Me.valDeadlineDay, DayFormat) & IIf(Me.valDeadlineTime.Enabled, " - " & Me.valDeadlineTime, "")
        
        End If
        
        messageBody.Bookmarks("valDeadline").Range.InsertBefore (ctDeadline)
        messageBody.Bookmarks("valDeadline").Range.Characters(Len(ctDeadline) + 1) = ""
        
    End If
    
    ctDeadline = Format(Me.finDeadlineDay, DayFormat) & IIf(Me.finDeadlineTime.Enabled, " - " & Me.finDeadlineTime, "")
    ' We write final deadline in any case, no matter which operation we assign
    messageBody.Bookmarks("finDeadline").Range.InsertBefore (ctDeadline)
    messageBody.Bookmarks("finDeadline").Range.Characters(Len(ctDeadline) + 1) = ""
    
End If

End Sub


Private Sub UserForm_Terminate()

'WheelUnHook
ActiveTextboxName = ""
'MouseWheelMod.StartWidenedForm = False
LocalHwnd = 0
LocalPrevWndProc = 0

If IsClipboard_AListOf_CouncilDocuments Then
    ' and clean clipboard... sorry, too dangerous to leave it!
    Call Set_Clipboard_TextContents("")
End If

End Sub


Public Sub MouseWheel(ByVal Rotation As Long, Optional KeyCode)
'************************************************
' To respond from MouseWheel event
' Scroll accordingly to direction
'
' Made by:  Mathieu Plante, modded by badkatro
' Date:     July 2004, Mai 2016 !
'************************************************

' Debugging
'StatusBar = "BeenWheeled, Rotation " & Rotation
If ActiveTextboxName = "" Then Exit Sub

' get necessary time interval for advancing/ rewinding current time label
Dim timeInterval As Integer
timeInterval = get_TimeInterval_forRotation(ActiveTextboxName, Rotation, KeyCode)
    
    
Dim ctTextbox As TextBox
' get a label object to pass it on to the sync wind routine
On Error Resume Next
Set ctTextbox = Me.Controls(ActiveTextboxName)

' Error checking in case sought label control is not to be found
If Err.Number > 0 Then
    MsgBox "Error number " & Err.Number & " occured in " & Err.Source & " with description " & Err.Description & "!"
    Err.Clear
    Exit Sub
End If

' call the advancing/ rewinding master routine for syncronised chaging of days/ hour/ minutes
Call SyncMove_All_Deadlines(ctTextbox, timeInterval, Rotation, KeyCode)


End Sub




Sub SyncMove_All_Deadlines(ctTextbox As TextBox, timeInterval As Integer, Rotation As Long, KeyCode)
' Master sync winding/ rewinding routine, calls others for days/ hours/ minutes winding processing


Dim ctDateFormat As String

ctDateFormat = Me.cbDateformat.Value

' Modify selected day deadline label, but in sync with the other ops as well...
' If we're dealing with a day label
If InStr(1, ActiveTextboxName, "Day") > 0 Then
    
    ctTextbox = CaptDate(Format(DateAdd("d", timeInterval, ctTextbox), IIf(ctDateFormat = "", "dd/mm", ctDateFormat)))
    
    ' TODO: MOVE THE BELOW TO A FUNCTION? (MAYBE "AUTOSCROLL_TIME_FORSECTION(CALLINGSECTION AS STRING) ?
    Dim ctTimebox As TextBox
    Set ctTimebox = Me.Controls(Replace(ctTextbox.Name, "Date", "Time"))
    
    If Left(ctTextbox.Name, 3) <> "fin" Then
        If Me.chAutoScroll_ToNextDay_Time Then
            If Me.tbAutoScroll_JumpToWhat = "middle" Then
                Call DeadlineTime_JumpTo("MOD", Me.Controls(Left(ctTextbox.Name, 3) & "StartWorkdayLbl"))
            Else
                Call DeadlineTime_JumpTo("SOD", Me.Controls(Left(ctTextbox.Name, 3) & "StartWorkdayLbl"))
            End If
        End If
    End If
    ' TODO: MOVE TO FUNCTION ENDS HERE, I GUESS!
    
    ' no sync moving other sections if we're on the last one !
    If Left(ctTextbox.Name, 3) <> "fin" Then
        Call SyncMove_All_DeadlineDays(timeInterval, Rotation, KeyCode)
    Else    ' we're on fin(alisation) op! Only workin for negatives!
        If Rotation < 0 Then
            Call SyncMove_All_DeadlineDays(timeInterval, Rotation, KeyCode)
        End If
    End If
    

' The hours labels as well
ElseIf InStr(1, ActiveTextboxName, "Time") > 0 Then
    
    If KeyCode = wdKeyHome Or KeyCode = wdKeyEnd Then
        ctTextbox = Format(DateAdd("h", timeInterval, CDate(ctTextbox)), "hh:nn")
    ElseIf KeyCode = wdKeyPageDown Or KeyCode = wdKeyPageUp Then
        ctTextbox.Value = Format(DateAdd("n", timeInterval, CDate(ctTextbox.Value)), "Hh:Nn")
        
        'Dim ctTbControl As control
        'Set ctTbControl = ctTextbox

        'Call Wind_Hour_If_Minutes_Full_Circle(ctTbControl.Name, Rotation, KeyCode)
        
    End If
    
    ' we only launch sync time winding if we're NOT winding final deadline!
    If Left(ActiveTextboxName, 3) <> "fin" Then
        Call SyncMove_All_DeadlineTimes(timeInterval, Rotation, KeyCode)
    End If
    
    
Else    ' No other case?
End If

End Sub


Sub SyncMove_All_DeadlineDays(timeInterval As Integer, Rotation As Long, KeyCode)

' REALY, REALY MASTER? Could this be? AAAaaaaaal this'ere code replaced by one function ? Ha!
Call Wind_All_DateTime_AwayFrom("Date", ActiveTextboxName, timeInterval, Rotation, KeyCode)


End Sub


Sub Wind_All_DateTime_AwayFrom(DateOrTime As String, SourceOperation_Name As String, timeInterval As Integer, Rotation As Long, KeyCode)
' DateOrTime can be either "Date" or "date" or "Time" or "time"
' It will determine if we add "d" or "h" or "m" to the current textbox value (since it will contain either a date or a time)


Dim sectionNamesPrefix
sectionNamesPrefix = Split(sectionNames, ",")   ' section names in sequence (ast, tra, rev, val, fin)

' if rotation is negative, the timeinterval parameter will be negative. It will be -1 or +1 in the other case.
' We may use this as step for the for loop we'll initate so as to go forward or backwards in the array with control names...

Dim loopStep As Integer
loopStep = IIf(timeInterval > 0, 1, -1)

Dim j As Integer
j = Get_Index_ofArray_Entry(sectionNamesPrefix, Left$(SourceOperation_Name, 3)) + loopStep
If j < 0 Then Exit Sub  ' we were called from first position (ast) and rotation is negative, someone is trying to
                        ' wind back days, we only wind back current one then, which is not the purpose of this routine

Dim z As Integer
z = IIf(Rotation > 0, UBound(sectionNamesPrefix), 0)


Dim ctControl As MSForms.control
Dim ctTextbox As MSForms.TextBox

For i = j To z Step loopStep
    
    Set ctControl = Me.Controls(sectionNamesPrefix(i) & "Deadline" & IIf(DateOrTime = "Date", "Day", "Time"))
    Set ctTextbox = ctControl
    
    Dim tt As String    ' time to add (day or hour)
    tt = IIf(DateOrTime = "Date", "d", IIf(KeyCode = vbKeyHome Or KeyCode = vbKeyEnd, "h", "n"))
    
    Dim ctDateFormat As String
    
    ctDateFormat = Me.cbDateformat.Value
    
    Dim sourceDayTxtb As MSForms.TextBox, targetDayTxtb As MSForms.TextBox
    Set sourceDayTxtb = Me.Controls(Replace(SourceOperation_Name, "Time", "Day"))
    Set targetDayTxtb = Me.Controls(sectionNamesPrefix(i) & "DeadlineDay")
    
    ' negative rotation, we're decreasing hours/mins/days
    If timeInterval < 0 Then
    
        If DateOrTime = "Date" Then
            
            If ctTextbox.Text <> "" Then
            
                If CDate(Me.Controls(SourceOperation_Name)) < CDate(ctTextbox) Then
                    ctTextbox = CaptDate(Format(DateAdd(tt, timeInterval, ctTextbox), IIf(DateOrTime = "Date", ctDateFormat, "hh:nn")))
                End If
            
            End If
        
        ElseIf DateOrTime = "Time" Then
                        
            If ctTextbox.Text <> "" Then
                        
                ' only sync other ops hours if we're talkin' same day!
                If sourceDayTxtb = targetDayTxtb Then

                    If CDate(Me.Controls(SourceOperation_Name)) < CDate(ctTextbox) Then
                        ctTextbox = CaptDate(Format(DateAdd(tt, timeInterval, ctTextbox), IIf(DateOrTime = "Date", ctDateFormat, "hh:nn")))
                    End If
                End If
                
            End If

            
        End If
        
    Else 'time interval positive, going upward, positive rotation
        
        If DateOrTime = "Date" Then
            
            If ctTextbox.Text <> "" Then
            
                If CDate(Me.Controls(SourceOperation_Name)) > CDate(ctTextbox) Then
                    ctTextbox = CaptDate(Format(DateAdd(tt, timeInterval, ctTextbox), IIf(DateOrTime = "Date", ctDateFormat, "hh:nn")))
                    
                    ' TODO: RIGHT HERE, BUILDING CODE TO AUTOSCROLL TIME FOR EACH OPERATION AT A TIME, TO MOVE INTO A FUNCTION!
                    
                    If Left(ctTextbox.Name, 3) <> "fin" Then
                    
                        If Me.chAutoScroll_ToNextDay_Time Then
                            
                            Dim ctTimebox As MSForms.control
                            Set ctTimebox = Me.Controls(Left(targetDayTxtb.Name, 3) & "StartWorkdayLbl")
                            
                            If Me.tbAutoScroll_JumpToWhat = "middle" Then
                                Call DeadlineTime_JumpTo("MOD", ctTimebox)
                            Else
                                Call DeadlineTime_JumpTo("SOD", ctTimebox)
                            End If
                            
                        End If
                    
                    End If
                
                End If
                
                ' TODO ENDS HERE, I GUESS!
                
            End If
        
        ElseIf DateOrTime = "Time" Then
                        
            ' only scroll hours in sync for other operations if we're talkin' same day!
            If Left(targetDayTxtb.Name, 3) <> "fin" Then
                If sourceDayTxtb = targetDayTxtb Then
                    If CDate(Me.Controls(SourceOperation_Name)) > CDate(ctTextbox) Then
                        ctTextbox = CaptDate(Format(DateAdd(tt, timeInterval, ctTextbox), IIf(DateOrTime = "Date", ctDateFormat, "hh:nn")))
                    End If
                End If
            End If
            
        End If
    
    End If
    
Next i


'Set ctControl = Nothing
'Set ctTextbox = Nothing

End Sub



Sub SyncMove_All_DeadlineTimes(timeInterval As Integer, Rotation As Long, KeyCode)

Call Wind_All_DateTime_AwayFrom("Time", ActiveTextboxName, timeInterval, Rotation, KeyCode)

End Sub


Sub SyncMove_All_DeadlineMinutes(timeInterval As Integer, Rotation As Long)

' To write it, similar to the hour advance, but... more checkin to do !

' Since now we check:
' 1. for ast, that ast and tra and rev activities share the deadline day
' - if so, we check if the three activities share the deadline hour !
' - if so, we advance of course, tra and rev as well
' - if not, prob only ast and tra share a day and an hour, so we advance only tra deadline minutes
' 2. for tra same as above, not caring for ast activity - EXCEPT for negative rotation !!!
' 3. for rev, same as above, not caring for ast and tra, except for negative rotation !!

' YAY, a lotta work !

End Sub


Sub Wind_Hour_If_Minutes_Full_Circle(ctTextboxName As String, Rotation As Long, KeyCode)


Dim timeInterval As Integer


Select Case Left(ctTextboxName, 3)
    
    Case "ast"
        Dim targetTbName As String
        targetTbName = "astDeadlineTime"
    Case "tra"
        targetTbName = "traDeadlineTime"
    Case "rev"
        targetTbName = "revDeadlineTime"
End Select

timeInterval = get_TimeInterval_forRotation(targetTbName, Rotation, KeyCode)


' Advance corresponding hour if minutes are rising
If Rotation > 0 Then

    If DatePart("n", CDate(Me.Controls(ctTextboxName))) = "00" Then
        
        Select Case Left(ctTextboxName, 3)
            
            Case "ast"
                Me.astDeadlineTime = Format(DateAdd("h", timeInterval, CDate(astDeadlineTime)), "hh:nn")
            Case "tra"
                Me.traDeadlineTime = Format(DateAdd("h", timeInterval, CDate(traDeadlineTime)), "hh:nn")
            Case "rev"
                Me.revDeadlineTime = Format(DateAdd("h", timeInterval, CDate(revDeadlineTime)), "hh:nn")
        End Select
        
    End If

Else ' BUT if rotation is negative and we've just descended minutes to 55...
    
        If DatePart("n", CDate(Me.Controls(ctTextboxName))) = "55" Then
        
        Select Case Left(ctTextboxName, 3)
            
            Case "ast"
                Me.astDeadlineTime = Format(DateAdd("h", timeInterval, CDate(astDeadlineTime)), "hh:nn")
            Case "tra"
                Me.traDeadlineTime = Format(DateAdd("h", timeInterval, CDate(traDeadlineTime)), "hh:nn")
            Case "rev"
                Me.revDeadlineTime = Format(DateAdd("h", timeInterval, CDate(revDeadlineTime)), "hh:nn")
        End Select
        
    End If

    
End If


    
End Sub




Function get_TimeInterval_forRotation(AdvanceWhichTextbox As String, Rotation As Long, Optional KeyCode) As Integer
' Purpose: given a positive or negative mouse wheel rotation amount (we only care for the sign of it, the
' actual window procedure routine takes care of the rest) and a target label (labels contain each a date - day/month),
' we read the displayed date and compute which is the next working day (so as we can skip week-ends).
' For some other labels, we simply return 1 or -1 (those contain hours) or 5 / -5 (minutes)

Dim ctTextbox As TextBox

On Error Resume Next
Set ctTextbox = Me.Controls(AdvanceWhichTextbox)

' Error checking in case sought label control is not to be found
If Err.Number > 0 Then
    MsgBox "Error number " & Err.Number & " occured in " & Err.Source & " with description " & Err.Description & "!"
    Err.Number = 0
    
    get_TimeInterval_forRotation = 0
    Exit Function
End If


Dim ctOptions As New SettingsFileClass

Dim minsInt As Variant
' retrieve saved pref for minutes scroll
minsInt = Me.tbMinsScroll
' and convert it to integer (was string)
minsInt = IIf(minsInt = "", 0, CInt(minsInt))


If Rotation > 0 Then    ' Rotation positive, upwards
    
    If InStr(1, AdvanceWhichTextbox, "Day") > 0 Then
        
        Dim cDay As Integer
        ' Extract currently displayed date's week day
        cDay = Weekday(ctTextbox)
        
        ' compute distance to next working day
        If cDay = vbFriday Then    ' if we're on a friday
            get_TimeInterval_forRotation = 3
        ElseIf cDay = vbSaturday Then    ' saturday
            get_TimeInterval_forRotation = 2
        Else
            get_TimeInterval_forRotation = 1
        End If
        
    ElseIf InStr(1, AdvanceWhichTextbox, "Time") > 0 Then
        
        If minsInt = 0 Then
            minsInt = 5  ' Revert to Default
        End If
        
        If Not IsMissing(KeyCode) Then
            ' User is add-scrolling hours
            If KeyCode = vbKeyHome Then
                get_TimeInterval_forRotation = 1
            ElseIf KeyCode = vbKeyPageUp Then
            
                ' finalisation deadline times scroll only by whole hours !
                If Left(AdvanceWhichTextbox, 3) <> "fin" Then
                    get_TimeInterval_forRotation = minsInt    ' scroll by 5 min, CONFIGURABLE
                Else    ' finalisation deadline time, special case! scroll by hours!
                    If Split(ctTextbox.Value, ":")(1) <> "00" Then   ' first we scroll back to the next hour (we are on 17.30 first)
                        get_TimeInterval_forRotation = 60 - Split(ctTextbox.Value, ":")(1)    ' scroll by 5 min, CONFIGURABLE
                    Else    ' then by one hour!
                        get_TimeInterval_forRotation = 60      ' even minutes scroll scrolls by one hour!
                    End If
                End If

            End If
        Else
            get_TimeInterval_forRotation = 1    ' Default to scrolling hours? KeyCode should be present each time we scroll time!
        End If
        
    'ElseIf InStr(1, AdvanceWhichTextbox, "Minutes") > 0 Then
        'get_TimeInterval_forRotation = 5
    
    End If

Else ' Rotation negative, downwards
    
    If InStr(1, AdvanceWhichTextbox, "Day") > 0 Then
        
        cDay = Weekday(ctTextbox)
        
        ' Compute time distance to previous working day
        If cDay = vbMonday Then        ' if we're on a Monday
            get_TimeInterval_forRotation = -3
        ElseIf cDay = vbSunday Then    ' Sunday
            get_TimeInterval_forRotation = -2
        Else
            get_TimeInterval_forRotation = -1
        End If
    
    ElseIf InStr(1, AdvanceWhichTextbox, "Time") > 0 Then
        
        ' In case we could not retrieve saved pref
        If minsInt = 0 Then
            minsInt = -5    ' Default
        Else
            minsInt = -1 * minsInt  ' we only display and store a positive generic value for scrolling minutes...
        End If
        
        If Not IsMissing(KeyCode) Then
            ' User is add-scrolling hours
            If KeyCode = vbKeyEnd Then
                get_TimeInterval_forRotation = -1
            ElseIf KeyCode = vbKeyPageDown Then
                
                ' finalisation deadline times scroll only by whole hours !
                If Left(AdvanceWhichTextbox, 3) <> "fin" Then
                    get_TimeInterval_forRotation = minsInt    ' scroll by 5 min, CONFIGURABLE
                Else    ' finalisation deadline time, special case! scroll by hours!
                    If Split(ctTextbox.Value, ":")(1) = "30" Then   ' first we scroll back to the next hour (we are on 17.30 first)
                        get_TimeInterval_forRotation = -30    ' scroll by 5 min, CONFIGURABLE
                    Else    ' then by one hour!
                        get_TimeInterval_forRotation = -60      ' even minutes scroll scrolls by one hour!
                    End If
                End If
                
            End If
        Else
            get_TimeInterval_forRotation = -1    ' Default to scrolling hours? KeyCode should be present each time we scroll time!
        End If
    
    'ElseIf InStr(1, AdvanceWhichTextbox, "Minutes") > 0 Then
        'get_TimeInterval_forRotation = -5
    
    End If
    
End If

End Function


Sub Hide_OperationPart(OperationName As String)
' OperationName values:
' "Preparation", "Translation" and "Revision"

Select Case OperationName
    
    Case "Preparation"
        
    Case "Translation"
        
    Case "Revision"
        
        
End Select


End Sub



Private Sub valDatePickerLbl_Click()

Call SyncMove_AllDeadlineDays_fromCalendar(Me.valDeadlineDay, Me.tbAccentColor.Value)

End Sub

Private Sub valDayPinLbl_Click()

Call Flip_PinnedState_ofLabel(valDayPinLbl, wdColorDarkBlue)

End Sub

Private Sub valDeadlineDay_Change()

End Sub

Private Sub valDeadlineDay_Enter()

valDeadlineDay.Tag = valDeadlineDay.Text
Call Activate_Textbox(valDeadlineDay.Name, wdColorRed)
valDeadlineDay.SelStart = 0

End Sub


Private Sub valDeadlineDay_Exit(ByVal Cancel As MSForms.ReturnBoolean)

If Me.valDeadlineDay.Text <> "" Then
    
    Call ColorWarn_ScrolledInThePast(Me.valDeadlineDay)

    If CDate(Me.valDeadlineDay.Text) <> CDate(Me.valDeadlineDay.Tag) Then
        'Call Wind_Date(Me.Controls(SectionPrefix & "DeadlineDay").Name, wdColorRed, KeyCode, Shift)
        
        ' EXPERIMENT... Does it work ?
        timeint = DateDiff("d", CDate(Me.valDeadlineDay.Tag), CDate(Me.valDeadlineDay.Text))
        
        Call Wind_All_DateTime_AwayFrom("Date", Me.valDeadlineDay.Name, CInt(timeint), timeint * 120, IIf(timeint > 0, vbKeyPageUp, vbKeyPageDown))
        
    End If

End If


Call Deactivate_Active_Textbox


End Sub

Private Sub valDeadlineDay_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

'Call Wind_Date(valDeadlineDay.Name, wdColorRed, KeyCode, Shift)

Call DeadlineDay_ProcessEvents(KeyCode, Shift, wdColorDarkBlue, "val")

End Sub


Private Sub valDeadlineDay_MouseMove(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Not Me.valDatePickerLbl.ForeColor = DayButtonsColor Then

    valDatePickerLbl.ForeColor = DayButtonsColor
    valDayPinLbl.ForeColor = DayButtonsColor
    
End If


End Sub


Private Sub valDeadlineTime_Enter()

Me.valDeadlineTime.Tag = Me.valDeadlineTime.Text
Call Activate_Textbox(valDeadlineTime.Name, wdColorRed)
valDeadlineTime.SelStart = 0

End Sub

Private Sub valDeadlineTime_Exit(ByVal Cancel As MSForms.ReturnBoolean)

If Me.valDeadlineTime.Text <> "" Then
    
    If CDate(Me.valDeadlineTime.Text) <> CDate(Me.valDeadlineTime.Tag) Then
        'Call Wind_Date(Me.Controls(SectionPrefix & "DeadlineDay").Name, wdColorRed, KeyCode, Shift)
        
        ' EXPERIMENT... Does it work ?
        timeint = DateDiff("n", CDate(Me.valDeadlineTime.Tag), CDate(Me.valDeadlineTime.Text))    ' n = minutes difference
        
        Call Wind_All_DateTime_AwayFrom("Time", Me.valDeadlineTime.Name, CInt(timeint), timeint * 120, IIf(timeint > 0, vbKeyPageUp, vbKeyPageDown))
        
    End If
    
End If

Call Deactivate_Active_Textbox

End Sub

Private Sub valDeadlineTime_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

Call DeadlineTime_ProcessEvents(KeyCode, Shift, "val", wdColorDarkBlue, wdColorDarkRed)

End Sub


Private Sub valDeadlineTime_MouseMove(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)


If Not Me.valNoTimeLbl.ForeColor = TimeButtonsColor Then

    valNoTimeLbl.ForeColor = TimeButtonsColor
    valTimePinLbl.ForeColor = TimeButtonsColor
    valStartWorkdayLbl.ForeColor = TimeButtonsColor
    valEndWorkdayLbl.ForeColor = TimeButtonsColor
    
    'finTimeLabel.ForeColor = wdColorGray50
    
End If


End Sub


Private Sub valEndWorkdayLbl_Click()

Call DeadlineTime_JumpTo("EOD", valEndWorkdayLbl)

End Sub





Private Sub valNoTimeLbl_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Call DeadlineDeactivate_ProcessEvents(Button, Shift, valNoTimeLbl, valDeadlineTime, wdColorDarkRed, TimeButtonsColor)

End Sub

Private Sub valNoTimeLbl_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 1 Then
    valNoTimeLbl.ForeColor = TimeButtonsColor
End If

End Sub

Private Sub valStartWorkdayLbl_Click()

Call DeadlineTime_JumpTo("SOD", Me.valDeadlineTime)

End Sub

Private Sub valTimePinLbl_Click()

Call Flip_PinnedState_ofLabel(Me.valTimePinLbl, wdColorDarkBlue)

End Sub
