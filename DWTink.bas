Attribute VB_Name = "DWTink"
Public Const DWVersion As String = "DocuWrite 4.1.18, Build 20180416"

Sub DocumentVariables_DocumentCreate()

Dim v As Variable
Dim odoc As Document

Dim HasDWMeta As Boolean

If Documents.count > 0 Then
    
    If ActiveDocument.Variables.count > 0 Then
    
        Set odoc = ActiveDocument
    
        Dim tgDoc As Document
        Set tgDoc = Documents.Add
        
        tgDoc.Paragraphs(1).Range.Text = "Variables in document " & odoc.Name & vbCr & vbCr
        tgDoc.Paragraphs(1).Style = "Heading 1"
        
        tgDoc.Paragraphs(3).Range.Text = "Variable name" & vbTab & "Variable value" & vbCr
        tgDoc.Paragraphs(3).Style = "Heading 3"
        tgDoc.Paragraphs(3).Range.Bold = True
        
        tgDoc.SaveAs "Variables of " & odoc.Name, AddToRecentFiles:=False, FileFormat:=wdFormatDocumentDefault
        odoc.activate
            
        For Each v In ActiveDocument.Variables
            If v.Name <> "DocuWriteMetaData" Then
                tgDoc.Paragraphs(tgDoc.Paragraphs.count).Range.InsertAfter (v.Name & vbTab & v.Value & vbCr)
            Else
                HasDWMeta = True
            End If
        Next v
    End If
    
    Dim p As Paragraph
    
    For Each p In tgDoc.Paragraphs
        p.TabStops.Add CentimetersToPoints(8), WdAlignmentTabAlignment.wdLeft, WdTabLeader.wdTabLeaderSpaces
    Next p
    
    If HasDWMeta Then
        
        tgDoc.Paragraphs(tgDoc.Paragraphs.count).Range.InsertAfter (Chr(12) & vbCr)    ' page break
        tgDoc.Paragraphs(tgDoc.Paragraphs.count).Range.Text = "DW Metadata Set"
        tgDoc.Paragraphs(tgDoc.Paragraphs.count).Range.Style = "Heading 2"
        tgDoc.Paragraphs(tgDoc.Paragraphs.count).Range.InsertAfter (vbCr)
        tgDoc.Paragraphs(tgDoc.Paragraphs.count).Range.Style = "Normal"
        tgDoc.Paragraphs(tgDoc.Paragraphs.count).Range.Text = ActiveDocument.Variables("DocuWriteMetadata").Value
        
    End If
    
    tgDoc.Sections(1).Footers(wdHeaderFooterPrimary).PageNumbers.Add (wdAlignPageNumberLeft)
    tgDoc.Sections(1).Footers(wdHeaderFooterPrimary).Range.Paragraphs(1).Borders(wdBorderTop).LineStyle = wdLineStyleDouble
    
    
    tgDoc.Sections(1).Footers(wdHeaderFooterPrimary).Range.Paragraphs.Add
    
    Dim inserthere As Range
    'Set inserthere = tgdoc.Range(tgdoc.Sections(1).Footers(wdHeaderFooterPrimary).Range.Paragraphs(3).Range)
    'inserthere.Collapse
    
    tgDoc.Sections(1).Footers(wdHeaderFooterPrimary).Range.Paragraphs(3).Range.Text = vbTab & tgDoc.Name
    
    
    tgDoc.SpellingChecked = True
    tgDoc.Save
    
End If

End Sub

Sub DW_DocumentUncouple(TargetDocument As Document)


If CtDocument_HasVariableNamed(TargetDocument, "Council") Then
    TargetDocument.Variables("Council").Delete
End If

If CtDocument_HasPropertyNamed(TargetDocument, "Created using") Then
    TargetDocument.CustomDocumentProperties("Created using").Delete
End If

If CtDocument_HasPropertyNamed(TargetDocument, "Last edited using") Then
    TargetDocument.CustomDocumentProperties("Last edited using").Delete
End If

TargetDocument.Save
StatusBar = "Document has LOST Docuwrite connection!"


End Sub


Sub DW_CurrentDocument_Uncouple()

Call DW_DocumentUncouple(ActiveDocument)

End Sub


Sub DW_DocumentCouple(TargetDocument As Document)


If CtDocument_HasVariableNamed(TargetDocument, "Council") Then
    TargetDocument.Variables("Council").Value = "true"
Else
    TargetDocument.Variables.Add "Council", "true"
End If


If CtDocument_HasPropertyNamed(TargetDocument, "Created using") Then
    
    Dim tgProp As DocumentProperty
    
    Set tgProp = TargetDocument.CustomDocumentProperties("Created using")
    tgProp.Value = DWVersion
    
Else    ' create prop and fill it in
    
    Dim tgprops As DocumentProperties
    
    Set tgprops = TargetDocument.CustomDocumentProperties
    
    tgprops.Add "Created using", False, msoPropertyTypeString, DWVersion
    
End If


If CtDocument_HasPropertyNamed(TargetDocument, "Last edited using") Then
    
    Dim tgProp As DocumentProperty
    
    Set tgProp = TargetDocument.CustomDocumentProperties("Last edited using")
    tgProp.Value = DWVersion
    
Else    ' create prop and fill it in
    
    Dim tgprops As DocumentProperties
    
    Set tgprops = TargetDocument.CustomDocumentProperties
    
    tgprops.Add "Last edited using", False, msoPropertyTypeString, DWVersion
    
End If

TargetDocument.Save
StatusBar = "Document Docuwrite connection restored!"


End Sub


Sub DW_CurrentDocument_Couple()

Call DW_DocumentUncouple(ActiveDocument)

End Sub



Sub Print_SubsNames_and_Desc(projectName As String, Optional PrintProceduresDeclarations, Optional PrintProceduresDescriptions, Optional PrintGeneralDeclarations)


Dim dwTpl As VBIDE.VBProject


For p = 1 To VBE.VBProjects.count
    
    If VBE.VBProjects(p).Name = projectName Then
        Set dwTpl = VBE.VBProjects(p)
        Exit For
    End If

Next p

' report failure to find demanded vbproject
If dwTpl Is Nothing Then
    MsgBox "Sorry, VBA project " & projectName & " was not found among available opened projects! Are you sure it's opened?", vbOKOnly, "VBA Project NOT found!"
    Exit Sub
End If


If dwTpl.Protection = vbext_pp_locked Then
    MsgBox "Sorry, VBA project " & projectName & " is locked! Please try to crack it and open it, then repeat!", vbOKOnly, "VBA Project is LOCKED!"
    Exit Sub
End If


Dim dwVBComps As VBIDE.VBComponents

Set dwVBComps = dwTpl.VBComponents

' current module
Dim ctMod As VBIDE.CodeModule


Dim ctvbc As VBIDE.VBComponent

Dim AllProcs As String

AllProcs = "VBA Project " & dwTpl.Name & " is composed of the following procedures: " & vbCr & vbCr


Dim ProcsNames As String    ' current module's procedure names

Dim ctProc As String


For i = 1 To dwVBComps.count
    
    Set ctvbc = dwVBComps.Item(i)
    
    ' Process only VBA Modules and classes, see below
    If ctvbc.Type = vbext_ct_StdModule Then
        
        Set ctMod = ctvbc.CodeModule
        
        ' clear subs names variable for current module
        ProcsNames = vbCr & "Module " & ctvbc.Name & ": " & vbCr
        
        ' iterate through all lines of code module
        For j = 1 To ctMod.CountOfLines
            
            ' capture current procedure name
            ctProc = ctMod.ProcOfLine(CLng(j), vbext_pk_Proc)
            ctLine = ctMod.Lines(j, 1)
            
            If Not IsMissing(PrintProceduresDeclarations) Then
                ' add current procedure to result string if not already there
                If InStr(1, ProcsNames, ctProc) = 0 Then
                    If Not (ctLine = "" Or ctLine = "'" Or (Left$(ctLine, 1) = "'")) Then
                        ' Some functions and routines take multiple parameters, using _ to
                        ' spread the said parameters over multiple lines
                        If Not Right$(ctLine, 1) = "_" Then
                        
                            ProcsNames = ProcsNames & vbLf & ctLine
                            
                        Else    ' we found a line ending in "_", incomplete declaration of function/ sub
                            
                            ProcsNames = ProcsNames & vbLf & ctLine
                            
                            Do While Right$(ctMod.Lines(j + 1, 1), 1) = "_" Or Right$(ctMod.Lines(j + 1, 1), 1) = ")"
                                ' gather next line as well
                                ProcsNames = ProcsNames & vbLf & ctMod.Lines(j + 1, 1)
                                j = j + 1   ' will this work to skip a loop ?
                            Loop
                            
                        End If
                        
                    End If
                End If
            Else
                ' add current procedure to result string if not already there
                If InStr(1, ProcsNames, ctProc) = 0 Then
                    ProcsNames = ProcsNames & vbLf & ctProc
                End If
            End If
                        
        Next j
        
        AllProcs = AllProcs & ProcsNames
    
    ' And extract procedures from classes as well !
    ElseIf ctvbc.Type = vbext_ct_ClassModule Then
        
        Set ctMod = ctvbc.CodeModule
        
        ' clear subs names variable for current module
        ProcsNames = vbCr & "Class " & ctvbc.Name & ": " & vbCr
        
        ' iterate through all lines of code module
        For j = 1 To ctMod.CountOfLines
            
            ' capture current procedure name
            ctProc = ctMod.ProcOfLine(CLng(j), vbext_pk_Proc)
            ctLine = ctMod.Lines(j, 1)
            
            
            If Not ctProc = "" Then     ' got a current procedure, we're in!
            
                If Not IsMissing(PrintProceduresDeclarations) Then
                    ' add current procedure to result string if not already there
                    If InStr(1, ProcsNames, ctProc) = 0 Then
                        If Not (ctLine = "" Or ctLine = "'" Or (Left$(ctLine, 1) = "'")) Then
                        
                            ' Some functions and routines take multiple parameters, using _ to
                            ' spread the said parameters over multiple lines
                            If Not Right$(ctLine, 1) = "_" Then
                            
                                ProcsNames = ProcsNames & vbLf & ctLine
                                
                            Else    ' we found a line ending in "_", incomplete declaration of function/ sub
                                
                                ProcsNames = ProcsNames & vbLf & ctLine
                                
                                Do While Right$(ctMod.Lines(j + 1, 1), 1) = "_" Or Right$(ctMod.Lines(j + 1, 1), 1) = ")"
                                    ' gather next line as well
                                    ProcsNames = ProcsNames & vbLf & ctMod.Lines(j + 1, 1)
                                    j = j + 1   ' will this work to skip a loop ?
                                Loop
                                
                            End If
                        
                        
                        End If
                    End If
                Else
                    ' add current procedure to result string if not already there
                    If InStr(1, ProcsNames, ctProc) = 0 Then
                        ProcsNames = ProcsNames & vbLf & ctProc
                    End If
                End If
            
            Else    ' we might be in gen dec, descriptions, etc
            
            End If
                        
        Next j
        
        AllProcs = AllProcs & ProcsNames

        
    End If
    
Next i


Documents.Add.Content.InsertAfter (AllProcs)



End Sub


Sub Get_OriginalDocSub()

Get_OriginalDoc

End Sub


Function Get_OriginalDoc() As Document



End Function



Function CountFiles_fromMaskFolder(TargetFolder As String, SearchMask As String) As Integer

Dim filesCol As New Collection

gobjObjects.Utils.ListFiles TargetFolder, filesCol, True, True, SearchMask

CountFiles_fromMaskFolder = filesCol.count


End Function
