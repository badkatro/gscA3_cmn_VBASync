Attribute VB_Name = "Functions"
'Sub AutoExec()
'' Switch some annoying auto-set settings back to the way WE like it!
'
'Application.ShowWindowsInTaskbar = False
'
'End Sub
'______________________________________________________________________________________________________


Const externalReviewSubPath As String = "\Studio\External Review"
Const studioSubPath As String = "\Studio"
Const externalReviewFileMask As String = "\*review*.doc*"
Const xliffFileMask As String = "\*.sdlxliff"

Public Const tmxExportsFolder_onT As String = "T:\TMs - Export\Revised translations pending import\CouncilMaster\"
Public Const tmxBadExportsFolder_onT As String = "T:\Prepared originals\QS_Errors\"

Public Const tmxAlignmentsFolder_onT As String = "T:\TMs - Export\Alignments pending import\CouncilMaster\"
Public Const tmxBadAlignmentsFolder_onT As String = "T:\Prepared originals\QS_Errors\"

Public Const tmxQuickSaveFolder_onT As String = "T:\TMs - Export\QuickSave\"
Public Const tmxQuickSaveFolderTemp_onT As String = "T:\TMs - Export\QuickSave\temp\"

' Below constant to be used to get all those different DW fields values from DW metadata of opened document using function DW_GetDocuwrite_TB_FieldsValues
Public Const DW_TB_Moretus_Field_Names As String = "md_DocumentNumber|md_YearDocumentNumber|md_Suffixes|md_SuffixLanguagesInvolved|md_SubjectCodes|md_InterInstitutionalFiles|md_CommissionDocuments|md_PrecedingDocuments|md_Subject|md_DG"
Public Const DW_General_Field_Names As String = "md_DocumentType|md_DocumentNumber|md_YearDocumentNumber|md_Suffixes|md_SuffixLanguagesInvolved|md_HeadingText|md_SubjectCodes|md_InterinstitutionalFiles|md_Originator|md_DateOfReceipt|md_Recipient|md_PrecedingDocuments|md_CommissionDocuments|md_Subject|md_DG"

Private Declare Function GetVolumeInformation Lib "kernel32" Alias "GetVolumeInformationA" (ByVal lpRootPathName As String, ByVal lpVolumeNameBuffer As String, _
    ByVal nVolumeNameSize As Long, lpVolumeSerialNumber As Long, lpMaximumComponentLength As Long, lpFileSystemFlags As Long, ByVal lpFileSystemNameBuffer As String, _
    ByVal nFileSystemNameSize As Long) As Long

Declare Function WNetGetConnection32 Lib "MPR.DLL" Alias "WNetGetConnectionA" (ByVal lpszLocalName As String, ByVal lpszRemoteName As String, lSize As Long) As Long '#visible


Public Enum badTmxSituation
    NotBadTmx = 0
    
    IsBadTmx = 1
    IsBadTmx_NoLog = 2
    IsBadTmx_WrongFolder = 3
    
    NotBadTmx_DoesNotApply = -1
End Enum



Function GetNetworkDrive_Label(DriveLetter As String, Optional GetFullRemoteName)


Dim fullRemoteName As String
fullRemoteName = GetNetPath(DriveLetter & ":")

If IsMissing(GetFullRemoteName) Then

    Dim pN  ' partial name - remove leading network double slashes
    pN = Replace(fullRemoteName, "\\", "")

    Dim sN  ' simple name, only local label, no complete net path
    sN = TrimNonAlphaNums(CStr(Split(pN, "\")(UBound(Split(pN, "\")))))

    GetNetworkDrive_Label = sN
    
Else
    GetNetworkDrive_Label = fullRemoteName
    
End If

End Function


Function GetNetPath(lpszLocalName As String) As String ' pass "Z:",
'letter and colon
Dim lpszRemoteName As String, lStatus&
GetNetPath = lpszLocalName ' default return same string if invalid

' Use for the return value of WNetGetConnection() API.
Const NO_ERROR As Long = 0

' The size used for the string buffer. Adjust this if you need a larger buffer.
Const lBUFFER_SIZE As Long = 255

If UCase(lpszLocalName) Like "[A-Z]:*" Then ' syntax check first

' Prepare a string variable by padding spaces.
lpszRemoteName = Space$(lBUFFER_SIZE)

' Return the UNC path (\\Server\Share).
lStatus& = WNetGetConnection32(lpszLocalName, lpszRemoteName, lBUFFER_SIZE)

' Verify that the WNetGetConnection() succeeded.
' WNetGetConnection() returns 0 (NO_ERROR) if it successfully retrieves the UNC path.
If lStatus& = NO_ERROR Then

GetNetPath = Trim$(lpszRemoteName) ' the UNC path is in lpszRemoteName, trim padded chrw$(0) away

End If

End If

End Function


Function GetDriveLabel(DriveRoot As String) As String
'KPD-Team 1998 'URL: http://www.allapi.net/ 'E-Mail: KPDTeam@Allapi.net

Dim Serial As Long, VName As String, FSName As String
'Create buffers
VName = String$(255, Chr$(0))
FSName = String$(255, Chr$(0))
'Get the volume information
GetVolumeInformation DriveRoot & ":", VName, 255, Serial, 0, 0, FSName, 255
'Strip the extra chr$(0)'s
VName = Left$(VName, InStr(1, VName, Chr$(0)) - 1)
FSName = Left$(FSName, InStr(1, FSName, Chr$(0)) - 1)
GetDriveLabel = VName

End Function



' onAction routine, called when clicking the switching auto pilot button
Sub FlipSpRpAutopilot_Worker(control As IRibbonControl)


Dim sFile As New SettingsFileClass

If sFile.Exists Then
    If Not sFile.IsEmpty Then
        
        Dim storedAPState As String
        
        storedAPState = sFile.getOption("AutoPilotState", "SpaceReplaceOptions")
        
        If Not storedAPState = "" Then
            
            Select Case storedAPState
                
                Case "on"
                    sFile.setOption "SpaceReplaceOptions", "AutoPilotState", "off"
                    Call RefreshRibbon
                Case "off"
                    sFile.setOption "SpaceReplaceOptions", "AutoPilotState", "on"
                    Call RefreshRibbon
            End Select
            
        Else
        End If
        
    Else
    End If
    
Else
End If


Set sFile = Nothing

End Sub

Sub DW_InterInst_Files_ToClipboard()

Dim dwMeta As MSXML2.DOMDocument
Set dwMeta = New DOMDocument

dwMeta.LoadXML (ActiveDocument.Variables("DocuwriteMetaData").Value)

Dim dwMetadataNodes As IXMLDOMNodeList
Set dwMetadataNodes = dwMeta.SelectNodes("//metadataset/metadata")

Dim DWMetaNode As IXMLDOMNode

Dim DWSubjectCodes As String

For Each DWMetaNode In dwMetadataNodes
    
    If DWMetaNode.Attributes.getNamedItem("key").NodeValue = "md_InterinstitutionalFiles" Then
        
        Dim textlist As IXMLDOMNodeList
        Set textlist = DWMetaNode.ChildNodes(0).ChildNodes
        
        Dim sjtext As IXMLDOMNode
        
        For Each sjtext In textlist
            DWSubjectCodes = IIf(DWSubjectCodes = "", sjtext.Text, DWSubjectCodes & "|" & sjtext.Text)
        Next sjtext
        
        Exit For
    End If
    
Next DWMetaNode

If DWSubjectCodes <> "" Then

    Dim vbcrSubjectCodes As String
    vbcrSubjectCodes = Replace(DWSubjectCodes, "|", vbCr)
    
    Set_Clipboard_TextContents (vbcrSubjectCodes)
    
    MsgBox "Document " & ActiveDocument.Name & " has Interinstitutional files: " & vbCr & vbCr & vbcrSubjectCodes, vbOKOnly, "SUCCESS! Files copied to clipboard!"
Else
    MsgBox "Document " & ActiveDocument.Name & " has NO DW interinstitutional files!", vbOKOnly, "FAILURE!"
End If

End Sub


Function countUsers_XAlignments(ctUser As String) As Integer
' Count users X* Brexit alignments in QSErrors folder!

Dim iXAlignm_InQSErrors As Integer
iXAlignm_InQSErrors = bGDirAll(tmxBadAlignmentsFolder_onT & "PEDIT*" & ctUser & "*_xm*BADTMX" & "|" & tmxBadAlignmentsFolder_onT & "PEDIT*" & ctUser & "*_xt*BADTMX").count

Dim iXCleanup_InQSErrors As Integer
iXCleanup_InQSErrors = bGDirAll(tmxBadAlignmentsFolder_onT & "CLEUP*" & ctUser & "*_xm*BADTMX" & "|" & tmxBadAlignmentsFolder_onT & "CLEUP*" & ctUser & "*_xt*BADTMX").count

countUsers_XAlignments = iXAlignm_InQSErrors + iXCleanup_InQSErrors


End Function

Function getUsers_XAlignments(ctUser As String) As foundFilesStruct
' Get users X* Brexit alignments in QSErrors folder!

Dim iXAlignm_Files_InQSErrors As foundFilesStruct
iXAlignm_Files_InQSErrors = bGDirAll(tmxBadAlignmentsFolder_onT & "PEDIT*" & ctUser & "*_xm*BADTMX" & "|" & tmxBadAlignmentsFolder_onT & "PEDIT*" & ctUser & "*_xt*BADTMX" & "|" & _
   tmxBadAlignmentsFolder_onT & "CLEUP*" & ctUser & "*_xm*BADTMX" & "|" & tmxBadAlignmentsFolder_onT & "CLEUP*" & ctUser & "*_xt*BADTMX")

getUsers_XAlignments = iXAlignm_Files_InQSErrors

End Function


Function countUsers_BadAlignments(ctUser As String) As Integer


'Dim iBadAlignm_InCouncilMaster As Integer
'iBadAlignm_InCouncilMaster = bGDirAll(tmxAlignmentsFolder_onT & "PEDIT*" & ctUser & "*BADTMX").count

Dim iBadAlignm_inQSErrors As Integer
iBadAlignm_inQSErrors = bGDirAll(tmxBadAlignmentsFolder_onT & "PEDIT*" & ctUser & "*BADTMX" & "|" & tmxBadAlignmentsFolder_onT & "SAVEPE*" & ctUser & "*BADTMX").count

countUsers_BadAlignments = iBadAlignm_inQSErrors


End Function


Function countUsers_BadExports(ctUser As String) As Integer


Dim iBadTmxs_InCouncilMaster As Integer
iBadTmxs_InCouncilMaster = bGDirAll(tmxExportsFolder_onT & "CLEUP*" & ctUser & "*BADTMX").count

Dim iBadTmxs_OutCouncilMaster As Integer
iBadTmxs_OutCouncilMaster = bGDirAll(tmxBadExportsFolder_onT & "CLEUP*" & ctUser & "*BADTMX").count


countUsers_BadExports = iBadTmxs_InCouncilMaster + iBadTmxs_OutCouncilMaster


End Function


Function getTemplateIndex(GlobalTemplateName As String) As Integer

If Application.Templates.count = 0 Then
    getTemplateIndex = -1
    Exit Function
End If


Dim gt As Template

For i = 1 To Application.Templates.count
    
    Set gt = Application.Templates(i)
    
    If gt.Name = GlobalTemplateName Then
        getTemplateIndex = i
        Exit Function
    End If
    
Next i

' Normal exit, 0 means template is prob not loaded or you're looking for wrong one
getTemplateIndex = 0

End Function


Function count_Users_Bad_Tmxs(ctUser As String, ByRef sbMessage As String) As Integer


Dim ictUser_Bad_Alignments As Integer
ictUser_Bad_Alignments = countUsers_BadAlignments(ctUser)

Dim ictUser_Bad_Exports As Integer
ictUser_Bad_Exports = countUsers_BadExports(ctUser)


Dim tempTotalBTmx As Integer

tempTotalBTmx = ictUser_Bad_Alignments + ictUser_Bad_Exports


If tempTotalBTmx > 21 Then tempTotalBTmx = 21     ' it's all we provided for!


sbMessage = "Count TMXs: Got " & ictUser_Bad_Alignments & " bALN | " & ictUser_Bad_Exports & " bEXP | "
'PauseForSeconds (1.3)

count_Users_Bad_Tmxs = tempTotalBTmx


End Function





Function exportTmx_existsOnT(GSCFilename As String) As Boolean

' Function reports if file st12345.en13.ro.tmx exists in T:\TMs - Export\Revised translations pending import\CouncilMaster or
' in T:\TMs - Export\Revised translations pending import (as a ".tmxBADTMX" file or in
' T:\TMs - Export\QuickSave\temp (should quickly skip from there automatically, sometimes it takes a bit longer)

Dim unitLang As String
Dim exportTmxFileName As String

'unitLang = LCase(Mid$(Environ("computername"), 3, 2))
unitLang = Get_ULg

Dim originalLang As String


If Is_GSC_Doc(GSCFilename) Then
    
    originalLang = GetOriginalLanguage_fromMProd(GSCFilename)
    
    ' Get language from MProd can also bring back empty string, so checks it
    If IsValidLangIso(originalLang) Then
        exportTmxFileName = getExportFolder(GSCFilename) & "\" & Replace(getGSCBaseName(GSCFilename), unitLang, originalLang) & "." & unitLang & ".WholeDoc.tmx"
    Else   ' No language code, wing it !
        exportTmxFileName = getExportFolder(GSCFilename) & "\" & Replace(getGSCBaseName(GSCFilename), unitLang, "*") & "." & unitLang & ".WholeDoc.tmx"
    End If
    
    ' And final check... is the file there or not?
    If Dir(exportTmxFileName) <> "" Then
        'Debug.Print "Export TMX found, as in " & exportTmxFileName
        exportTmx_existsOnT = True
    Else
        'Debug.Print "Export TMX NOT found!"
        exportTmx_existsOnT = False
    End If
    
Else
    StatusBar = "Export TMX verification: Supplied document NOT Council, Exiting..."
    Exit Function
End If

End Function


Function exportTmx_IsBADTMX(GSCFilename As String) As badTmxSituation
' Should modifyto return strings, such as "IsBadTmx" or "BadTmx_butNoLog" or "IsNOTBadTmx" and so on?
' Could use a custom enumeration as return type

Dim unitLang As String
Dim exportTmxFileName As String

Dim badTmx_FileMask As String
Dim badTmx_LogMask As String

'unitLang = LCase(Mid$(Environ("computername"), 3, 2))
unitLang = Get_ULg

' Should modify to allow launching with no opened doc, with a form waiting doc number?

If Is_GSC_Doc(GSCFilename) Then
    
    badTmx_FileMask = tmxBADTMXFolder_onT & "*" & getGSCBaseName(GSCFilename) & "*.tmxBADTMX"
    badTmx_LogMask = Replace(badTmx_FileMask, ".tmxBADTMX", ".txt")
    
    If Dir(badTmx_FileMask) <> "" Then
        
        Dim badTmx_Message As String
        Dim fs As New Scripting.FileSystemObject

' we're coming back here so as to check bad tmx log for other folders...
checkLogAsWell:

        If Dir(badTmx_LogMask) <> "" Then
            
            Dim stream As TextStream
            
            ' Try to open the log file from the right folder, in or out of the CouncilMaster folder...
            If InStr(1, badTmx_LogMask, "CouncilMaster") = 0 Then
                Set stream = fs.OpenTextFile(tmxBADTMXFolder_onT & Dir(badTmx_LogMask))
            Else
                Set stream = fs.OpenTextFile(tmxExportsFolder_onT & Dir(badTmx_LogMask))
            End If
            
            badTmx_Message = stream.ReadAll
            stream.Close
            
            If badTmx_Message <> "" Then
            
                exportTmx_IsBADTMX = IsBadTmx
                MsgBox "Cleanup Memory export failed for " & GSCFilename & " with following error:" & vbCr & vbCr & _
                    "Here are the contents of the badTMX log file:" & vbCr & vbCr & _
                    badTmx_Message, vbOKOnly, "Failure: Export produced a badTMX!"
            Else
                ' Should not return also "IsBadTmx_NoLog" from this one also ?
            End If
        Else
            exportTmx_IsBADTMX = IsBadTmx_NoLog
            Debug.Print "exportTmx_ISBADTMX Error: found badtmx file, but not log file for " & GSCFilename
        End If
    Else
        
        ' Let's also check the CouncilMaster folder, sometimes they land over there
        badTmx_FileMask = Replace(badTmx_FileMask, tmxBADTMXFolder_onT, tmxExportsFolder_onT)
        badTmx_LogMask = Replace(badTmx_FileMask, ".tmxBADTMX", ".txt")
        
        ' if found still in CouncilMaster folder, it's still a badtmx !
        If Dir(badTmx_FileMask) <> "" Then
            exportTmx_IsBADTMX = IsBadTmx
            GoTo checkLogAsWell
        Else
            exportTmx_IsBADTMX = NotBadTmx
        End If
        
    End If
    
Else
    StatusBar = "BADTMX verification: Supplied document NOT GSC Document, Exiting..."
    exportTmx_IsBADTMX = NotBadTmx_DoesNotApply
    Exit Function
End If

Set fs = Nothing
End Function


Function getUserformIndex(UserformName As String) As Integer


If VBA.UserForms.count > 0 Then
    
    Dim tusf As MSForms.UserForm
    
    For i = 0 To VBA.UserForms.count - 1
        
        Set tusf = VBA.UserForms.Item(i)
        
        If tusf.Name = UserformName Then
            getUserformIndex = i
            Exit Function
        End If
        
    Next i
    
    getUserformIndex = -1   ' not found !
    
Else
    getUserformIndex = -1   ' no userforms loaded!
End If


End Function


Function exportTmx_StillInQuickSave(GSCFilename As String) As Boolean

'CLEUP_margama_CouncilMemory_st18037.en13.ro.WholeDoc.tmx

Dim unitLang As String
Dim username As String
Dim exportTmxQuickName As String

'unitLang = LCase(Mid$(Environ("computername"), 3, 2))
unitLang = Get_ULg
username = LCase(Environ("username"))

Dim quickSaveFileMask As String
'quickSaveFileMask = Replace("CLEUP_#un#*", "#un#", username)
quickSaveFileMask = "CLEUP_*"

If Is_GSC_Doc(GSCFilename) Then
    
    originalLang = GetOriginalLanguage_fromMProd(GSCFilename)
    
    If IsValidLangIso(CStr(originalLang)) Then
        exportTmxQuickName = tmxQuickSaveFolder_onT & quickSaveFileMask & Replace(getGSCBaseName(GSCFilename), unitLang, originalLang) & "." & unitLang & ".WholeDoc.tmx"
    Else
        exportTmxQuickName = tmxQuickSaveFolder_onT & quickSaveFileMask & Replace(getGSCBaseName(GSCFilename), unitLang, "*") & "." & unitLang & ".WholeDoc.tmx"
    End If
    
    
    If Dir(exportTmxQuickName) <> "" Then
        'Debug.Print "Export TMX found, as in " & exportTmxFileName
        exportTmx_StillInQuickSave = True
    Else
        exportTmxQuickName = Replace(exportTmxQuickName, tmxQuickSaveFolder_onT, tmxQuickSaveFolderTemp_onT)
        
        ' second attempt, if not in quicksave folder, it could be in quicksave temp folder...
        If Dir(exportTmxQuickName) <> "" Then
            exportTmx_StillInQuickSave = True
        Else
            'Debug.Print "Export TMX NOT found!"
            exportTmx_StillInQuickSave = False
        End If
    End If
    
Else
    exportTmx_StillInQuickSave = False
    StatusBar = "Export TMX verification: Supplied document NOT Council, Exiting..."
    Exit Function
End If


End Function


Sub Open_XLF_Preview_Links()
' Unlike its sister function


If Documents.count > 0 Then
    
    If Is_GSC_Doc(ActiveDocument.Name) Then
            
            If UBound(Split(ActiveDocument.Name, ".")) >= 2 Then    ' no unsaved documents accepted
                
                If Split(ActiveDocument.Name, ".")(2) = "txt" Then  ' xlf files are converted to text by the routine WF starts
                    
                    Dim ctXLFFilename As String
                    ctXLFFilename = ActiveDocument.Name
                    
                    Dim ctXLFStandardName As String
                    ctXLFStandardName = GSCFileName_ToStandardName(ctXLFText)
                    
                    'Documents(ctXLFText).Close (wdDoNotSaveChanges)
                    
                    Dim olRCode As Integer
                    olRCode = Open_XLF_PreviewURL(ctXLFStandardName)

                Else
                    GoTo noGoodDoc
                End If
                
            Else
                GoTo noGoodDoc
            End If
            
    Else
        GoTo noGoodDoc
    End If
    
Else

noGoodDoc:
    olRCode = Open_XLF_PreviewURL()
    
End If

' Probably not needed, I think routine prints messages on Statusbar by itself!
'Select Case olRCode
'    Case 0  ' Success
'        StatusBar = "Open Preview Link: Operation performed successfully"
'    Case 1  ' Project folder retrieval failed
'        StatusBar = "Open Preview Link: Could not retrieve project folder!"
'    Case 2  ' Original xlf file retrieval failed
'        StatusBar = "Open Preview Link: Could not retrieve original xlf file!"
'    Case 3  ' Copy operation verification of the original xlf did not succeed...
'        StatusBar = "Open Preview Link: Could not copy original xlf to temp location!"
'    Case 4  ' Got empty link string from xlf file... could be a bundle xlf file, no links there
'        StatusBar = "Open Preview Link: XLF File is bundle, no preview URL!"
'    Case 5  ' Chrome AND IE not found at expected locations error
'        StatusBar = "Open Preview Link: Could not retrieve original xlf file!"
'    Case 6  ' Preview URL came back empty, title is not empty, but not bundle!
'        StatusBar = "Open Preview Link: Preview URL empty, Title not bundle!"
'    Case 7  ' Preview URL and Title came back empty strings !
'        StatusBar = "Open Preview Link: Preview URL and TITLE are empty!"
'    Case Else   ' Including error code 8, caught by error handler as unknown error
'        StatusBar = "Open Preview Link: Unknown error!"
'End Select

End Sub



Sub Open_Alignments_Folder()
' Modding, new alignments folder location

'Dim targetFolder As String
'targetFolder = projectsBaseFolder

Dim foundAlFolder As String

Dim tempDoc As String
Dim luckyDoc As String


If Documents.count > 0 Then
    If Is_GSC_Doc(ActiveDocument.Name) Then
        
        tempDoc = ActiveDocument.Name
        
        foundAlFolder = getAlignmentFolderOnT(tempDoc)
        
    Else
        StatusBar = "Open Alignments Folder: Current document NOT GSC Document! Exiting..."
        Exit Sub
    End If
Else
    
    ' When used without a current doc, show user last used project, if any in history
    If UBound(Split(latestOpenedDocs, ",")) <> -1 Then
        luckyDoc = Split(latestOpenedDocs, ",")(UBound(Split(latestOpenedDocs, ",")))
    Else
        luckyDoc = "ST 12345/13 REV 1 EN"
    End If
    
    
    tempDoc = InputBox("Please provide original language document standard name to check, up to second dot and original's language, if different from �EN�", _
            "Open which ?", luckyDoc)
            
    ' Correct for user not having supplied original's language, guess for english
    If Not IsValidLangIso(Right$(tempDoc, 2)) Then
        tempDoc = Trim(tempDoc) & " EN"
    End If
        
        
    '
    If Is_GSC_Doc(GSC_StandardName_WithLang_toFileName(tempDoc)) Then
        
        If InStr(1, latestOpenedDocs, tempDoc) = 0 Then
            latestOpenedDocs = latestOpenedDocs & IIf(latestOpenedDocs <> "", ",", "") & tempDoc
        End If
        
        foundAlFolder = getAlignmentFolderOnT(GSC_StandardName_WithLang_toFileName(tempDoc))
    Else
        StatusBar = "Open Alignments Folder: Supplied document ID NOT GSC Format! Exiting..."
        Exit Sub
    End If
            
End If

' Evaluate and act or warn
If foundAlFolder <> "" Then
    
    TargetFolder = foundAlFolder
    
    StatusBar = "Open Alignments Folder: Alignment folder " & foundAlFolder & " FOUND! Opening..."
    
    ' So finally launch folder in explorer
    Call Shell("explorer.exe " & TargetFolder, vbNormalFocus)
Else
    
    StatusBar = "Open Alignments Folder: Could not find an alignment folder for " & tempDoc & "! Exiting..."
    
End If


End Sub


Sub Run_CopieFootere()

frm_CopieFootere.Show

End Sub


Sub CopieFootere()
' version 0.95
' main routine
'
' 0.87 - Modificat rutina sa foloseasca numai obiectul range, in locul lui "selection", pentru a nu produce defilarea
' rapida a documentului, ne-necesara.
' 0.88 - Modificat sa noteze locatia bookmarkului "Init" (initiale), daca exista in footer si sa-l refaca dupa copiere, in footerul primei sectiuni
' 0.90 - Remediere eroare de a copia mai multe rinduri din paragraful care contine "Anexa...", la inceputul fiecarei sectiuni,
' in cazul in care exista "Shift+Enter", adica "LineFeed" (Chr(11)) in cadrul acestuia
' 0.92 - Modificata pozitionarea ultimului tab, gresita. Legat footerul unei sectiuni de cel anterior si re-dezlegat, daca este sectiunea >=2,
' si primul paragraf nu contine "anexa*". Produce footerele cu indicatia corecta, chiar si daca sint section-breakuri in cadrul unei anexe. :)
' 0.93 - "Captuseste" cu indent (left sau right) paragrafele din componenta footerelor, daca marginile stg sau drpt sint mai mici de 2cm.
' Muta taburile corespunzator daca modificam marginea stanga.
' 0.935 - Nu aplica indent daca trebuie sa aplici negativ ! (vezi nota de mai sus)
' 0.95 - Versiune post migrare, reimplementare. Vom schimba

Dim mRng As Range
Dim cfTrng As Range
Dim lfIdx As Integer

If Documents.count = 0 Then
    StatusBar = "Copy Footers: NO document opened, Exiting..."
    Exit Sub
End If

' Capture sections number in variable s
frm_CopieFootere.Hide
If ActiveDocument.Sections.count >= 2 Then
    s = ActiveDocument.Sections.count
Else
    MsgBox "Documentul are decat o singura sectiune, Uitatu-v-ati daca footerul e corect?", , "Decat Una!": Exit Sub
End If

' If first section was indicated as source, link all sections' footers to previous and then unlink
If frm_CopieFootere.OptionButton1.Value = True Then

LinkM:
    For i = 2 To s
        ActiveDocument.Sections(i).Footers(wdHeaderFooterPrimary).LinkToPrevious = True
    Next i

    For i = 2 To s
        ActiveDocument.Sections(i).Footers(wdHeaderFooterPrimary).LinkToPrevious = False
    Next i

Else    ' However, if last section's footer is source, we need to copy last section's footer to the first and then redo the above
    
    Dim ls_rng As Range ' last section's footer range
    Set ls_rng = ActiveDocument.Sections(s).Footers(wdHeaderFooterPrimary).Range    ' set range to last section footer
    
    ls_rng.Copy     ' which content we copy
    ActiveDocument.Sections(1).Footers(wdHeaderFooterPrimary).Range.Paste   ' and paste to first section
    
    ' and unfortunatelly we also need to delete the last empty paragraph which also results extra...
    Set cfTrng = ActiveDocument.Sections(1).Footers(wdHeaderFooterPrimary).Range
    cfTrng.Collapse Direction:=wdCollapseEnd
    cfTrng.Delete wdCharacter, -1
    Set cfTrng = Nothing
    
    ' But now we are free to again link and unlink all sections, as above
    ' So, Link
    For i = 2 To s - 1
        ActiveDocument.Sections(i).Footers(wdHeaderFooterPrimary).LinkToPrevious = True
    Next i
    ' and Unlink
    For i = 2 To s - 1
        ActiveDocument.Sections(i).Footers(wdHeaderFooterPrimary).LinkToPrevious = False
    Next i
End If

' Now we switch to print layout view
ActiveWindow.View.Type = wdPrintView
ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument  ' and we go to main body
ActiveWindow.View.Type = wdPrintView
' and set a range to first character of document
Set mRng = ActiveDocument.Range(0, 0)

' We now loop throug all sections from the second to the last
For i = 1 To s - 1
    ' So, we skip from the first character of document to next section, thus getting second section's first char
    ' We then expand to first paragraph. We are looking to see whether these sections contain "Anexa" and other similar
    ' as one the firt paragraphs
    Set mRng = mRng.GoToNext(wdGoToSection)
    mRng.Expand wdParagraph
    'mRng.Select
            
        ' Having identified the first paragraph of second section, we skip the empty ones as many times as it takes
isO:    If StrComp(mRng.Text, vbCr) = 0 Or StrComp(mRng.Text, Chr(11)) = 0 Then
            mRng.MoveEndUntil vbCr
            mRng.MoveEnd wdCharacter, 1
            mRng.MoveStart wdCharacter, 1
            'mRng.Select
            GoTo isO
        Else    ' else we exclude the last char from out range, we found the first non-empty char
            mRng.MoveEnd wdCharacter, -1
            'mRng.Select
        End If
    
    ' Having identified it, we capture its text
    mTxt = LCase(mRng.Text)
    'MsgBox "mTxt = " & mTxt    ' debugging
    
    ' If paragraph in question spans multiple lines (contains "linefeed"), we retain first line only
    lfIdx = InStr(1, mTxt, Chr(11)) ' Pozitia lui "shift+enter" in string
    If lfIdx > 0 Then
            mTxt = Left(mTxt, (lfIdx - 1))
    End If
    
        ' Now we compare the text of the first paragraph (or the first line of it) to our desired strings in RO
        If mTxt Like "anex?" Or mTxt Like "anexa #" Or mTxt Like "anexa #*" Or mTxt Like "anexa ##" _
            Or mTxt Like "anex? la anex?*" Or mTxt Like "anex?*" Then       ' These are all identified necessary cases
            
            'mRng.Copy
            ' Having found Annex or such in this section, we need to set a range to the proper place in the footer of it
            Set mRng = ActiveDocument.Sections(i + 1).Footers(wdHeaderFooterPrimary).Range
            mRng.Collapse wdCollapseStart
            mRng.Move wdParagraph, 2
            nrm = mRng.MoveEndUntil(vbTab)
            'mRng.Select
            
            ' We then check to determine if the second paragraph of footer does NOT already contain the needed text
            If Len(nrm) > 4 Then
                If LCase(mRng.Text) <> mTxt Then
                    mRng.Collapse (wdCollapseStart)
                    mRng.Delete wdCharacter, nrm
                    mRng.InsertAfter UCase(mTxt)
                    mTxt = ""
                    mRng.Move wdParagraph, -2
                End If
            Else    ' If it doesn't, we simply insert copied text from first non-empty par
                'mRng.PasteSpecial DataType:=wdPasteText
                'mRng.Delete
                mRng.Collapse (wdCollapseStart)
                mRng.InsertAfter UCase(mTxt)
                mTxt = ""
                mRng.Move wdParagraph, -2
            End If
        Else
            ' Daca primul paragraf nu contine "anex*" in sectiunea curenta, nu lasam gol footerul acesteia
            ' - cel mai probabil trebuie legat de al sectiunii precedente!
            ActiveDocument.Sections(i + 1).Footers(wdHeaderFooterPrimary).LinkToPrevious = True
            ActiveDocument.Sections(i + 1).Footers(wdHeaderFooterPrimary).LinkToPrevious = False
        End If
Next i

' For all sections from second on, if they're on landscape, we move the tabs
' If they have non-standard left-right white margins, we compensate with left-right indent, so that white space should result at standard 2cm; we then re-align tabs
Dim par1 As Paragraph, par2 As Paragraph
For i = 1 To s - 1  ' looping through all sections from second onward
    
    With ActiveDocument.Sections(i + 1).PageSetup
                    
        ' Compensam reducerea marginii din dreapta a paginii cu adaugarea aceleiasi dimensiuni de "right indent" la footer,
        ' dar numai daca rezultatul este pozitiv (nu dorim sa aplicam indent negativ)
        If CentimetersToPoints(2) - .RightMargin > 0 Then
            .Parent.Footers(wdHeaderFooterPrimary).Range.ParagraphFormat.RightIndent = _
                CentimetersToPoints(2) - .RightMargin
        End If
        
        Set par1 = .Parent.Footers(wdHeaderFooterPrimary).Range.Paragraphs(2)
        Set par2 = .Parent.Footers(wdHeaderFooterPrimary).Range.Paragraphs(3)
        
        ' Incercam sa facem la fel pentru marginea stanga, dar aici trebuie de asemenea recalculate pozitiile taburilor
        If .LeftMargin < CentimetersToPoints(2) Then
            
            ' Capturam diferenta in puncte pentru a o adauga ca left indent
            lmd = CentimetersToPoints(2) - .LeftMargin
            
            ' Adaugam intii left indent corespunzator, apoi restul
            .Parent.Footers(wdHeaderFooterPrimary).Range.ParagraphFormat.LeftIndent = _
            CentimetersToPoints(2) - .LeftMargin
            
            ' Pozitiile taburilor au valori diferite, fct de orientarea paginii
            If .Orientation = wdOrientPortrait Then
                
                With par1.TabStops
                    .ClearAll
                    .Add CentimetersToPoints(8.5) + lmd, wdAlignTabCenter
                    .Add CentimetersToPoints(13) + lmd, wdAlignTabCenter
                    .Add CentimetersToPoints(17) + lmd, wdAlignTabRight
                End With
                                
                With par2.TabStops
                    .ClearAll
                    .Add CentimetersToPoints(8.5) + lmd, wdAlignTabCenter
                    .Add CentimetersToPoints(17) + lmd, wdAlignTabRight
                End With

                                
            ElseIf .Orientation = wdOrientLandscape Then
                                              
                With par1.TabStops
                    .ClearAll
                    .Add CentimetersToPoints(12.75) + lmd, wdAlignTabCenter
                    .Add CentimetersToPoints(21) + lmd, wdAlignTabCenter
                    .Add CentimetersToPoints(25.7) + lmd, wdAlignTabRight
                End With
                                
                With par2.TabStops
                    .ClearAll
                    .Add CentimetersToPoints(12.75) + lmd, wdAlignTabCenter
                    .Add CentimetersToPoints(25.7) + lmd, wdAlignTabRight
                End With
            
            End If
        Else    ' Daca marginea stanga a paginii este egala cu 2 (sau mai mare !?!), atunci setam taburile la valorile normale pt landscape
            If .Orientation = wdOrientLandscape Then
                With par1.TabStops
                    .ClearAll
                    .Add CentimetersToPoints(12.75), wdAlignTabCenter
                    .Add CentimetersToPoints(21), wdAlignTabCenter
                    .Add CentimetersToPoints(25.7), wdAlignTabRight
                End With
                                
                With par2.TabStops
                    .ClearAll
                    .Add CentimetersToPoints(12.75), wdAlignTabCenter
                    .Add CentimetersToPoints(25.7), wdAlignTabRight
                End With
            End If

        End If
    End With
Next i

' Tabs realingnment finised, we again switch to print layout view and close any extra panes
ActiveWindow.View.Type = wdPrintView
ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument
ActiveWindow.ActivePane.View.Type = wdPrintView
ActiveWindow.ActivePane.View.Zoom.PageFit = wdPageFitFullPage
' and move selection to first character of document
Selection.HomeKey wdStory, wdMove

End Sub

Function Get_AuthInitials() As String


Dim authI As String

authI = InputBox("Would you like to input the author's initials?", "Who dunit?")

If Split(authI, "/")(UBound(Split(authI, "/"))) = Application.UserInitials Then
    Get_AuthInitials = authI
Else
    Get_AuthInitials = authI & "/" & Application.UserInitials
End If


End Function

Private Sub Windows_ForceRedraw()
    Application.ScreenUpdating = False
    ActiveWindow.SmallScroll Down:=-100
    ActiveWindow.SmallScroll Up:=100
    Application.ScreenUpdating = True
End Sub








' Callback to retrieve icon of Bad Tmx Main Counter
'Sub getTBTmxImage(control As IRibbonControl, ByRef image)
'
'
'Dim targetFolder As String
'targetFolder = ""
'
'
'Dim baseFolder As String
'baseFolder = "T:\Tmx-Exports"
'
'Dim alignFolder As String
'alignFolder = "\Alignments pending import\"
'
'Dim exportsFolder As String
'exportsFolder = "\Translations pending import\"
'
'' Workaround for workin at home instead of at work
'If Dir(baseFolder) = "" Then
'    baseFolder = "C:\Users\margama\AppData\Roaming\Microsoft\Word\BadTmxCounter_Fonts\Unicorn"
'    alignFolder = ""
'    exportsFolder = ""
'Else
'End If
'
'
'Dim badTmx_count As Integer
'
''badTmx_count = Count_Users_BadTmxFiles
''
''
''If Not badTmx_count = -1 Then   ' error of some sorts, unlikely though
''
''Else
''
''End If
'
'
'If Dir(baseFolder & "\C*.txt") <> "" Then
'
'    chosen = Dir(baseFolder & "\C*.txt")
'
'    Dim fso As New Scripting.FileSystemObject
'    Dim fsf As File
'
'    Set fsf = fso.GetFile(baseFolder & "\" & chosen)
'
'    Debug.Print "getTBTmxImage: Read " & chosen & " as input file..."
'    chosen = Replace(Split(chosen, ".")(0), "C", "")
'
'    fso.DeleteFile (fsf.Path) ' fsf.Name)
'
'    fso.CreateTextFile (baseFolder & "\C" & Int(20 * Rnd) & ".txt")
'
'Else
'    chosen = "0"
'    fso.CreateTextFile (baseFolder & "\C0.txt")
'End If
'
'
'Set image = LoadPicture(baseFolder & "\Num_" & chosen & "S.gif")
'
'Debug.Print "getTBTmxImage: Launching new timer for " & Now + TimeValue("0:30:00") & "..."
'Application.OnTime Now + TimeValue("0:30:00"), "RefreshRibbon"
'
'
'End Sub



Sub getBcqImage(control As IRibbonControl, ByRef image)
    
    If Dir(Environ("LOCALAPPDATA") & "\Microsoft\Office", vbDirectory) <> "" Then
        Dim LAD_OfficeFolder As String
        LAD_OfficeFolder = Environ("LOCALAPPDATA") & "\Microsoft\Office"
    Else
        LAD_OfficeFolder = Environ("LOCALAPPDATA")
    End If
    
    
    If sUserName = "oancevi" Then
        Set image = LoadPictureGDI(LAD_OfficeFolder & "\butterfly_clock_o.jpg")
    Else
        Set image = LoadPictureGDI(LAD_OfficeFolder & "\icon-38674.png")
    End If
    
'    Select Case control.ID
'    Case "customButton1"
'        Set image = LoadPictureGDI(ThisWorkbook.Path & "\" & "Lighton.png")
'    Case "customButton2"
'        Set image = LoadPictureGDI(ThisWorkbook.Path & "\" & "Lightoff.png")
'    End Select
    
End Sub


Sub Activate_GSCRoTab()
    SendKeys "%y1{ENTER}"
End Sub


Function RplAll(TxtToRpl, RplTxt, _
                Optional CurrentSectionOnly, _
                Optional Forward As Boolean, _
                Optional Wrap As Integer, _
                Optional Format As Boolean, _
                Optional MCase As Boolean, _
                Optional MWholeWord As Boolean, _
                Optional MWildcards As Boolean, _
                Optional MSoundsLike As Boolean, _
                Optional MAWForms As Boolean)
 
Dim rng As Range

Call GoToMain

' If user requests, process current section only
If Not IsMissing(CurrentSectionOnly) Then
    
    Dim ctSel As Integer
    ctSel = Selection.Information(wdActiveEndSectionNumber)
    
    ActiveDocument.Sections(ctSel).Range.Select
    Selection.MoveEnd wdCharacter, -1
    
Else
    Selection.HomeKey Unit:=wdStory
End If

'Search Main Story:
 FndRpl TxtToRpl, RplTxt, _
   Forward, Wrap, Format, MCase, MWholeWord, MWildcards, MSoundsLike, MAWForms
 Selection.Collapse wdCollapseStart

' Keep on and go to other stories if needed, but not in "current section only" mode
If IsMissing(CurrentSectionOnly) Then

    'Search all the other stories:
    For Each rng In ActiveDocument.StoryRanges
 
        If rng.StoryType <> wdMainTextStory And rng.Text <> vbCr Then
        
            rng.Select
            FndRpl TxtToRpl, RplTxt, _
              Forward, Wrap, Format, MCase, MWholeWord, MWildcards, MSoundsLike, MAWForms
            Selection.Collapse wdCollapseStart
            
            Do While Not (rng.NextStoryRange Is Nothing)
                Set rng = rng.NextStoryRange
                If rng.Text <> vbCr Then
                    rng.Select
                    FndRpl TxtToRpl, RplTxt, _
                      Forward, Wrap, Format, MCase, MWholeWord, MWildcards, MSoundsLike, MAWForms
                    Selection.Collapse wdCollapseStart
                End If
            Loop
        
        End If

    Next rng
    
End If

Call GoToMain

End Function


Function FndRpl(TxtToRpl, RplTxt, _
                Optional Forward As Boolean, _
                Optional Wrap As Integer, _
                Optional Format As Boolean, _
                Optional MCase As Boolean, _
                Optional MWholeWord As Boolean, _
                Optional MWildcards As Boolean, _
                Optional MSoundsLike As Boolean, _
                Optional MAWForms As Boolean)

 With Selection.Find
 
    .Text = TxtToRpl
    .Replacement.Text = RplTxt
    .Forward = Forward
    .Wrap = Wrap
    .Format = Format
    .MatchCase = MCase
    .MatchWholeWord = MWholeWord
    .MatchWildcards = MWildcards
    .MatchSoundsLike = MSoundsLike
    .MatchAllWordForms = MAWForms

 End With

 Selection.Find.Execute Replace:=wdReplaceAll

End Function






Function Get_Clipboard_TextContents() As String

On Error GoTo ErrorHandler


Dim clipText As String
Dim mydata As New MSForms.DataObject
mydata.GetFromClipboard        ' get contents of clipboard into "mydata" data object

If mydata.GetFormat(1) Then     ' format 1 means format text
    clipText = mydata.GetText(1)
Else
    clipText = ""  ' clear variable, so that next if is able to thus expedite exit
End If
'
If clipText <> "" Then
    Get_Clipboard_TextContents = clipText
Else
    Get_Clipboard_TextContents = ""
End If

Exit Function

'**********************************************************************************************
ErrorHandler:
    If Err <> 0 Then
        MsgBox "Error " & Err.Number & " has occured in " & Err.Source & vbCrCr & _
            "Error has following description: " & Err.Description, vbOKOnly + vbCritical, "Get_Clipboard_TextContents Error"
        Err.Clear
        Get_Clipboard_TextContents = ""
    End If
End Function

Function Set_Clipboard_TextContents(NewClipText As String) As Boolean
' 0.2 - modding to use Windows API calls for talking to clipboard, as the
' MSForms DataObject is now... broken ! Thanks, Microsoft !

On Error GoTo ErrorHandler


Dim clipText As String

'Dim mydata As New MSForms.DataObject

If NewClipText <> "" Then
    'mydata.SetText (NewClipText)    ' set contents of clipboard using "mydata" data object
    SetClipboard (NewClipText)
Else
    'mydata.SetText ""
    SetClipboard ("")   ' Hope this deletes clipboard?
End If

'If mydata.GetFormat(1) Then     ' format 1 means format text
'    clipText = mydata.GetText(1)
'Else
'    clipText = ""  ' clear variable, so that next if is able to thus expedite exit
'End If
'
'If clipText <> "" Then
'    Get_Clipboard_TextContents = clipText
'Else
'    Get_Clipboard_TextContents = ""
'End If
'mydata.PutInClipboard


'If Get_Clipboard_TextContents = NewClipText Then
If GetClipboard = NewClipText Then
    Set_Clipboard_TextContents = True
Else
    Set_Clipboard_TextContents = False
End If


Exit Function

'**********************************************************************************************
ErrorHandler:
    If Err <> 0 Then
        MsgBox "Error " & Err.Number & " has occured in " & Err.Source & vbCrCr & _
            "Error has following description: " & Err.Description, vbOKOnly + vbCritical, "Get_Clipboard_TextContents Error"
        Err.Clear
        Set_Clipboard_TextContents = False
    End If
End Function



Function Arabic_toRoman_Convert(ArabicNumbersString) As String

'Sanity check
If IsNumeric(ArabicNumbersString) Then
    
    Dim arNum As Integer
    arNum = CInt(ArabicNumbersString)
    
    Dim arRomArr(1, 6) As String
    Call RomanNumerals_Initialize(arRomArr)
    
Else
    Arabic_to_Roman_Convert = ""
    Exit Function
End If


Dim romNum As String
romNum = ""


Dim csn As Integer      'current symbol number

For i = 0 To 6
    
    csn = arNum \ arRomArr(0, i)    ' does the current roman value fit inside our number ?
    
    ' we only update romNum and decrease arNum if we have a positive result in csn, meaning if current value fits in our number
    If csn >= 1 Then
        If i > 0 And csn = 4 Then
            ' to avoid trying to do the difference simplification for M symbol, we condition for i positive
            ' and if we're about to write 4 symbols of the same kind, replace with one of that kind and one of immediately bigger kind: IIII = IV, XXXX = XL
            romNum = romNum & arRomArr(1, i) & arRomArr(1, i - 1)
            arNum = arNum - csn * arRomArr(0, i)
        ElseIf i > 0 And Left(arNum, 1) = 9 Then    ' Never are 9-hundreds or 9-tees or 9s composed of 500's or 50's or 5's and rest... 9 is not VIV but IX and 92 is not LXLII, but XCII
            romNum = romNum & arRomArr(1, i + 1) & arRomArr(1, i - 1)   ' so we use one symbol smaller (some unit symbol, like I or X or C) and one tens power symbol (X, C, M), since we're surely already positioned at some 5s multiple symbol (D, L, V)
            arNum = arNum - (arRomArr(0, i - 1) - arRomArr(0, i + 1))   ' and then we extract both of the used symbols value, with difference and all (XC or IX or somethin')
        Else    ' when i = 0 (we write M's) and all other cases
            romNum = romNum & String(csn, arRomArr(1, i))
            arNum = arNum - csn * arRomArr(0, i)
        End If
    End If
    
Next i

Arabic_toRoman_Convert = romNum

End Function


Function Roman_toArabic_Convert(RomanNumbersString) As String

' Sanity check
For i = 1 To Len(RomanNumbersString)
    If InStr(1, "IVXLCDM", Mid$(RomanNumbersString, i, 1)) = 0 Then
        Roman_toArabic_Convert = ""
    Else
        Dim romNum As String
        romNum = RomanNumbersString
        
        Dim arRomArr(1, 13) As String
        Call RomanNumerals_Ext_Initialize(arRomArr)
        
    End If
Next i

Dim arNum As Integer
arNum = 0


Do
    For i = 0 To 13
        If CStr(Left(romNum, Len(arRomArr(1, i))) = CStr(arRomArr(1, i))) Then
            
            arNum = arNum + CInt(arRomArr(0, i))
            romNum = Mid$(romNum, Len(arRomArr(1, i)) + 1, Len(romNum) - Len(arRomArr(1, i)))
            Exit For
        Else
        End If
    Next i
Loop While (Len(romNum) > 0)


Roman_toArabic_Convert = arNum

End Function


Sub RomanNumerals_Ext_Initialize(ByRef StorageArray)


If Not IsArray(StorageArray) Then
    Exit Sub
End If

If UBound(StorageArray, 1) <> 1 Or _
   UBound(StorageArray, 2) <> 13 Then

    Exit Sub
End If


StorageArray(0, 0) = "1000": StorageArray(1, 0) = "M"
StorageArray(0, 1) = "900":  StorageArray(1, 1) = "CM"

StorageArray(0, 2) = "500": StorageArray(1, 2) = "D"
StorageArray(0, 3) = "400": StorageArray(1, 3) = "CD"

StorageArray(0, 4) = "100": StorageArray(1, 4) = "C"
StorageArray(0, 5) = "90":  StorageArray(1, 5) = "XC"

StorageArray(0, 6) = "50": StorageArray(1, 6) = "L"
StorageArray(0, 7) = "40": StorageArray(1, 7) = "XL"

StorageArray(0, 8) = "10": StorageArray(1, 8) = "X"
StorageArray(0, 9) = "9":  StorageArray(1, 9) = "IX"

StorageArray(0, 10) = "5": StorageArray(1, 10) = "V"
StorageArray(0, 11) = "4": StorageArray(1, 11) = "IV"
StorageArray(0, 12) = "1": StorageArray(1, 12) = "I"


End Sub


Sub RomanNumerals_Initialize(ByRef StorageArray)

If Not IsArray(StorageArray) Then
    Exit Sub
End If

If UBound(StorageArray, 1) <> 1 Or _
   UBound(StorageArray, 2) <> 6 Then

    Exit Sub
End If


StorageArray(0, 6) = "1": StorageArray(1, 6) = "I"
StorageArray(0, 5) = "5": StorageArray(1, 5) = "V"
StorageArray(0, 4) = "10": StorageArray(1, 4) = "X"
StorageArray(0, 3) = "50": StorageArray(1, 3) = "L"
StorageArray(0, 2) = "100": StorageArray(1, 2) = "C"
StorageArray(0, 1) = "500": StorageArray(1, 1) = "D"
StorageArray(0, 0) = "1000": StorageArray(1, 0) = "M"


End Sub



Sub CurrentDocument_hasAlignments()


If Documents.count > 0 Then
    
    If Is_GSC_Doc(ActiveDocument.Name) Then
        
        Dim ctAlignmentFolder As String
        ctAlignmentFolder = getAlignmentFolderOnT(ActiveDocument.Name)
        
        If Not ctAlignmentFolder = "" Then
            
            Dim ctrawfiles As rawalignments
            ctrawfiles = get_RawAlignmentFiles_NamesAndTotalSize(ActiveDocument.Name)
            
            If Not ctrawfiles.rawAlignmentsCount = 0 Then
            ' Found at least ONE raw alignments files
                
                If MsgBox("Found " & ctrawfiles.rawAlignmentsCount & " raw alignment files totalling " & Round(ctrawfiles.totalRawAlignmentsSize / 1024, 0) & " KB!" & vbCr & _
                    "Do you want to open the folder?", vbYesNo + vbInformation, "Alignments found!") = vbYes Then
                    
                        Call Shell("explorer.exe " & ctAlignmentFolder, vbNormalFocus)
                    
                End If
                
            Else    ' NO raws found !
                Application.Caption = ":)"
                GSCRo_Lib.PauseForSeconds (1)
                Application.Caption = ""
            End If
            
        Else
            Application.Caption = ":)"
            GSCRo_Lib.PauseForSeconds (1)
            Application.Caption = ""
        End If
        
    Else
        StatusBar = "NOT GSC Document, Exiting..."
    End If
    
Else
    StatusBar = "NO Doc opened, Exiting..."
End If

End Sub

Function Clear_Clipboard_TextContents() As Boolean  ' Returns True if text portion of clipboard is successfully nulled, False otherwise

On Error GoTo ErrorHandler

Dim mydata As New DataObject
mydata.SetText ""

mydata.PutInClipboard

' Avoid calling Get_Clipboard function twice
If Get_Clipboard_TextContents = "" Then
    Clear_Clipboard_TextContents = True
Else
    Clear_Clipboard_TextContents = False
End If

Exit Function

'******************************************************************************************************
ErrorHandler:
    If Err <> 0 Then
        MsgBox "Error " & Err.Number & " has occured in " & Err.Source & vbCrCr & _
            "Error has following description: " & Err.Description, vbOKOnly + vbCritical, "Clear_Clipboard_TextContents Error"
        Err.Clear
        Clear_Clipboard_TextContents = False
    End If

End Function






Function hasPrimaryFooterStory() As Boolean

If Documents.count = 0 Then
    hasPrimaryFooterStory = False
    Exit Function
End If

On Error GoTo errorPFS

If ActiveDocument.StoryRanges(wdPrimaryFooterStory).Characters.count > 0 Then
    
    Dim tmpTxt As String
    
    tmpTxt = ActiveDocument.StoryRanges(wdPrimaryFooterStory).Text
    tmpTxt = Replace(Replace(Replace(tmpTxt, vbCr, ""), " ", ""), Chr(160), "")
    
    If tmpTxt <> "" Then
            
        hasPrimaryFooterStory = True
            
    End If
    
End If


'******************************************************************
errorPFS:
    If Err.Number <> 0 Then
        ' Requested member of the collection not found
        Err.Clear
    End If

End Function


Function hasPrimaryHeaderStory() As Boolean

If Documents.count = 0 Then
    hasPrimaryHeaderStory = False
    Exit Function
End If

On Error GoTo errorPHS

If ActiveDocument.StoryRanges(wdPrimaryHeaderStory).Characters.count > 0 Then
    
    Dim tmpTxt As String
    
    tmpTxt = ActiveDocument.StoryRanges(wdPrimaryFooterStory).Text
    tmpTxt = Replace(Replace(Replace(tmpTxt, vbCr, ""), " ", ""), Chr(160), "")
    
    If tmpTxt <> "" Then
            
        hasPrimaryHeaderStory = True
            
    End If
    
End If


'******************************************************************
errorPHS:
    If Err.Number <> 0 Then
        ' Requested member of the collection not found
        Err.Clear
    End If

End Function


Function hasFirstPageFooterStory() As Boolean

If Documents.count = 0 Then
    hasFirstPageFooterStory = False
    Exit Function
End If

On Error GoTo errorFPFS

If ActiveDocument.StoryRanges(wdFirstPageFooterStory).Characters.count > 0 Then
    
    Dim tmpTxt As String
    
    tmpTxt = ActiveDocument.StoryRanges(wdFirstPageFooterStory).Text
    tmpTxt = Replace(Replace(Replace(tmpTxt, vbCr, ""), " ", ""), Chr(160), "")
    
    If tmpTxt <> "" Then
            
        hasFirstPageFooterStory = True
            
    End If
    
End If


'******************************************************************
errorFPFS:
    If Err.Number <> 0 Then
        ' Requested member of the collection not found
        Err.Clear
    End If

End Function


Function hasTextboxStory() As Boolean

If Documents.count = 0 Then
    hasTextboxStory = False
    Exit Function
End If

On Error GoTo errorTBS

If ActiveDocument.StoryRanges(wdTextFrameStory).Characters.count > 0 Then
    
'    Dim tmpTxt As String
'
'    tmpTxt = ActiveDocument.StoryRanges(wdTextFrameStory).Text
'    tmpTxt = Replace(Replace(Replace(tmpTxt, vbCr, ""), " ", ""), Chr(160), "")
'
'    If tmpTxt <> "" Then
        hasTextboxStory = True
'    End If
    
End If


'******************************************************************
errorTBS:
    If Err.Number <> 0 Then
        ' Requested member of the collection not found
        Err.Clear
    End If


End Function


Function UnicodeFind_inActiveDocument(UnicodeToFind) As Boolean

'Safety net. Function works on Activedocument only, so...
If Documents.count = 0 Then
    StatusBar = "Find unicode: NOT working with NO document opened, Exiting..."
    UnicodeFind_inActiveDocument = False
    Exit Function
End If

Dim tgRange As Range
Set tgRange = ActiveDocument.StoryRanges(wdMainTextStory).Characters(1)
tgRange.Collapse

If Not UnicodeFind(UnicodeToFind, tgRange) Then
    ' Try for footnotes story existence and presence of culprit
    If ActiveDocument.Footnotes.count > 0 Then
        Set tgRange = ActiveDocument.StoryRanges(wdFootnotesStory)
        If UnicodeFind(UnicodeToFind, tgRange) Then
CodeFound:  UnicodeFind_inActiveDocument = True
            Exit Function
        End If
    End If
    ' Try for endnotes story, too
    If ActiveDocument.Endnotes.count > 0 Then
        Set tgRange = ActiveDocument.StoryRanges(wdEndnotesStory)
        If UnicodeFind(UnicodeToFind, tgRange) Then
            GoTo CodeFound
        End If
    End If
    ' And primary footer story
    If hasPrimaryFooterStory Then
        Set tgRange = ActiveDocument.StoryRanges(wdPrimaryFooterStory)
        If UnicodeFind(UnicodeToFind, tgRange) Then
            GoTo CodeFound
        End If
    End If
    ' Headers are important too
    If hasPrimaryHeaderStory Then
        Set tgRange = ActiveDocument.StoryRanges(wdPrimaryHeaderStory)
        If UnicodeFind(UnicodeToFind, tgRange) Then
            GoTo CodeFound
        End If
    End If
    ' Text boxes are not as uncommon as one might think...
    If hasTextboxStory Then
        
        Dim sh As Shape
        For Each sh In ActiveDocument.Shapes
            If sh.Type = msoTextBox Then
                Set tgRange = sh.TextFrame.ContainingRange
                If UnicodeFind(UnicodeToFind, tgRange) Then
                    GoTo CodeFound
                End If
            End If
        Next sh
        
    End If  ' A boolean defaults to false, anyway. No need to explicitly set it to anythnig but true
Else
    GoTo CodeFound
End If  ' Same-o same-o


End Function

Function UnicodeFind(UnicodeToFind, RangeToSearch As Range, _
              Optional Forward As Boolean, _
              Optional Wrap As Integer, _
              Optional Format As Boolean, _
              Optional MWholeWord As Boolean, _
              Optional MWildcards As Boolean, _
              Optional MSoundsLike As Boolean, _
              Optional MAWForms As Boolean) As Boolean

Dim TargetRange As Range
Dim origRange As Range

UnicodeFind = False
 
Set TargetRange = RangeToSearch
Set origRange = TargetRange.Duplicate

Set TargetRange = TargetRange.Characters(1): TargetRange.Collapse

Searchagain:

With TargetRange.Find

   .Text = ChrW(UnicodeToFind)
   .Forward = True
   .Wrap = wdFindStop
   .Format = False
   .MatchCase = True
   .MatchWholeWord = MWholeWord
   .MatchWildcards = MWildcards
   .MatchSoundsLike = MSoundsLike
   .MatchAllWordForms = MAWForms
   
 
   Do While .Execute
       
        'targetRange.Select
       
        If StrComp(ChrW(UnicodeToFind), TargetRange.Text, vbBinaryCompare) = 0 Then
            UnicodeFind = True
            Exit Do
        End If
       
        TargetRange.Collapse wdCollapseEnd
        'targetRange.Collapse wdCollapseEnd
        'Set targetRange = ActiveDocument.Range(targetRange.Start, origRange.End)
        
        GoTo Searchagain
        
   Loop

End With

End Function

Sub Wrong_ToRightDiacritics_Replace()
' vers 0.6  ' using strcomp or ascw to check for character codes when finding RO diacritics, since s with cedilla and with comma are one and the same for Gates!

' 0.5 - trying to fix wrong identification of small diacritics sh and tz as wrong even when they're not, depending on the region settings! (RO is the culprit)
' - will be trying to use StrComp in binary mode comparison

Dim UniDiaCodes As Variant
Dim sbMsg As String

Dim sbUpdated As Boolean
Dim foundOne As Boolean
sbUpdated = False
foundOne = False

sbMsg = "CorrectDia: Searching... "
StatusBar = sbMsg

UniDiaCodes = Split("0,355,539,351,537,350,536,354,538", ",")   ' odd indexes - wrong ones. even - right ones

' Capture initial user selection & view mode
Application.ScreenUpdating = False
Dim orsel As Range
Set orsel = Selection.Range

Dim orview As WdViewType
orview = Application.ActiveWindow.View.Type

For i = 1 To 7 Step 2
    
    If UnicodeFind_inActiveDocument(UniDiaCodes(i)) Then
        
        foundOne = True
        
        ' Update statusbar, but on first iteration only !
        If Not sbUpdated Then
            sbMsg = sbMsg & " |  Wrong diacritics found, Replacing..."
            StatusBar = sbMsg
            sbUpdated = True
        End If
        
        ActiveDocument.Characters(1).Select: Selection.Collapse (wdCollapseStart)
        RplAll ChrW(UniDiaCodes(i)), ChrW(UniDiaCodes(i + 1)), , wdFindContinue, False, True, True, False, False, False
        
    End If
    
    ClrFndRpl
    
Next i


' Redo all original settings
orsel.Select
If Selection.Information(wdInHeaderFooter) Then ActiveWindow.ActivePane.Close
ActiveWindow.View.Type = orview

Application.ScreenRefresh
Application.ScreenUpdating = True


If Not foundOne Then
    sbMsg = Replace(sbMsg, "Searching...", "Cedilla RO diacritics NOT FOUND !")
    StatusBar = sbMsg
Else
    StatusBar = sbMsg & " |  DONE !"
End If

End Sub

Sub tryAttribForm()

On Error GoTo UnhookAndExit

'Dim newAssign As New frmAssign

Load frmAssign

frmAssign.Show

Exit Sub

'********************************************
UnhookAndExit:
    If Err.Number <> 0 Then
        Debug.Print "Error no " & Err.Number & " occured in " & Err.Source & " with desc " & Err.Description
        Err.Number = 0
    End If
    WheelUnHook
    Exit Sub


End Sub


Sub ReplaceDash_toNonBreakingDash()

On Error GoTo errorDashes

'[a-zA-Z]-[a-zA-Z]

Dim ll As Integer
Dim fl As Integer

Dim rMainText As Range
Dim foundDash As Boolean
Dim foundDNumber As String

Dim rCounter As Integer

Dim lookForWhat As String
lookForWhat = "-"

If Documents.count = 0 Then
    StatusBar = "ReplaceDashes: No document, Exiting..."
End If

Set rMainText = ActiveDocument.StoryRanges(wdMainTextStory)

foundDash = foundChar(rMainText, lookForWhat)
If Not foundDash Then
    StatusBar = "Replace dashes: NO dash found, Exiting..."
    Exit Sub
End If

' Debugging
'foundDNumber = IIf(foundDash, "at least one", "no")
'Debug.Print "Found " & foundDNumber & " occurences of �" & lookForWhat & "� within!"

With rMainText.Find
    .ClearFormatting
    .Format = False
    .Forward = True
    .Wrap = wdFindStop
        
    StatusBar = "Replace dashes: Searching... Please wait..."
    Application.ScreenUpdating = False
        
    Do While .Execute(FindText:="[a-zA-Z]-[a-zA-Z]", Replace:=wdReplaceNone, ReplaceWith:="", MatchWildcards:=True, MatchWholeWord:=False)
        
        fl = rMainText.Information(wdFirstCharacterLineNumber)
        
        rMainText.MoveStart wdCharacter, 2
        ll = rMainText.Information(wdFirstCharacterLineNumber)
        rMainText.MoveStart wdCharacter, -2
        
        If fl <> ll Then
            rMainText.Select
            rMainText.Characters(2) = Chr(30)
            rCounter = rCounter + 1
        End If
        
        rMainText.Collapse (wdCollapseEnd)
        
    Loop
    
End With

Application.ScreenUpdating = True
Application.ScreenRefresh

If rCounter > 0 Then
    StatusBar = "Replace dashes: Replaced " & rCounter & " occurences!"
Else
    StatusBar = "Replace dashes: No replacements necesarry!"
End If

Set rMainText = Nothing

Exit Sub

'__________________________________________________________________________________
'
errorDashes:
    If Err.Number <> 0 Then
        Debug.Print "Error " & Err.Number & ", with description " & Err.Description & " in " & Err.Source
        Err.Clear
        StatusBar = "Replace dashes: Encountered VBA Error, please call margama (2942). Exiting... "
    End If

End Sub

Function foundChar(TargetRange As Range, LookforCharacter As String) As Boolean

' returns number of occurences of LookforCharacter in TargetRange

Dim ch As String
ch = LookforCharacter

Dim rngT As Range
Set rngT = TargetRange.Duplicate

Dim found As Boolean

found = rngT.Find.HitHighlight(FindText:=ch, MatchWholeWord:=False)
rngT.Find.ClearHitHighlight

foundChar = found

End Function

Sub Sort_Paragraphs_inSelection()

Dim p As Paragraph
Dim ctp_text As String
Dim np_text As String

Dim s_rng As Range

Dim targetSorted As Boolean
Dim noSwapThisRound As Boolean
noSwapThisRound = True

Dim smallest As Integer
smallest = 1

Dim buffer As Range

Dim originalSel As Range


If Selection.Type = wdSelectionNormal Then
        
    Set originalSel = Selection.Range.Duplicate
    Set s_rng = originalSel.Duplicate
    
    Dim odoc As Document
    Set odoc = ActiveDocument
    Dim bufferdoc As Document
    Set bufferdoc = Documents.Add
    Set tmpRng = bufferdoc.StoryRanges(wdMainTextStory)
    odoc.activate
        
Dim originalSelParCount As Integer
originalSelParCount = s_rng.Paragraphs.count
        
    Do
                
        For i = (smallest + 1) To originalSelParCount
            
            ctp_text = Replace(Replace(Replace(s_rng.Paragraphs(smallest).Range.Text, Chr(1), ""), Chr(160), ""), vbCr, "")
            np_text = Replace(Replace(Replace(s_rng.Paragraphs(i).Range.Text, Chr(1), ""), Chr(160), ""), vbCr, "")
            
            If ctp_text > np_text Then
                
                noSwapThisRound = False
                
                ' swap the two paragraphs
                s_rng.Paragraphs(smallest).Range.Copy
                tmpRng.Delete: tmpRng.Paste
                
                s_rng.Paragraphs(i).Range.Copy
                s_rng.Paragraphs(smallest).Range.Paste
                
                tmpRng.Copy
                s_rng.Paragraphs(i).Range.Paste
                tmpRng.Delete
                
                'Exit For
            End If
        Next i
      
        If noSwapThisRound Then
            If smallest = s_rng.Paragraphs.count Then
                targetSorted = True
            Else
                smallest = smallest + 1
            End If
        Else
            noSwapThisRound = True
        End If
    
    ActiveDocument.UndoClear
    Loop While Not targetSorted
    
originalSel.Select
End If


bufferdoc.Close (wdDoNotSaveChanges)
End Sub


Function CtDocument_HasVariableNamed(InputDocument As Document, NameOfVariable As String) As Boolean
' Looks for document property NameOfProperty in current document and returns whether it finds it or not

' Fail and exit if no document is opened
If Documents.count = 0 Then
    CtDocument_HasVariableNamed = False
    Exit Function
End If

' Fail and exit if ct doc has no variables
If InputDocument.Variables.count = 0 Then
    CtDocument_HasVariableNamed = False
    Exit Function
End If

Dim foundIt As Boolean

' Loop all vars and sniff the desired
Dim var As Variable
For Each var In InputDocument.Variables
    If var.Name = NameOfVariable Then
        foundIt = True
        Exit For
    End If
Next var

' Have we got property and set the final return value of function
If foundIt Then
    CtDocument_HasVariableNamed = True
Else
    CtDocument_HasVariableNamed = False
End If


End Function


Function CtDocument_HasPropertyNamed(InputDocument As Document, NameOfProperty As String) As Boolean
' Looks for document property NameOfProperty in current document and returns whether it finds it or not

' Fail and exit if no document is opened
If Documents.count = 0 Then
    CtDocument_HasPropertyNamed = False
    Exit Function
End If

' Fail and exit if ct doc has no variables
If InputDocument.CustomDocumentProperties.count = 0 Then
    CtDocument_HasPropertyNamed = False
    Exit Function
End If

Dim foundIt As Boolean

' Loop all vars and sniff the desired
Dim prop As DocumentProperty
For Each prop In InputDocument.CustomDocumentProperties
    If prop.Name = NameOfProperty Then
        foundIt = True
        Exit For
    End If
Next prop

' Have we got property and set the final return value of function
If foundIt Then
    CtDocument_HasPropertyNamed = True
Else
    CtDocument_HasPropertyNamed = False
End If


End Function


Sub List_Environment_Variables()

Dim EnvString As String
Indx = 1

Do
    EnvString = Environ(Indx)
    Debug.Print EnvString
    Indx = Indx + 1
Loop Until EnvString = ""

End Sub


Sub List_All_Variables()

Dim var As Variable

If ActiveDocument.Variables.count > 0 Then
    Debug.Print "Variables in " & ActiveDocument.Name:
    For Each var In ActiveDocument.Variables
        
        Debug.Print var.Name & " - " & var.Value
        
    Next var
Else
    Debug.Print ActiveDocument.Name & " has NO variables !"
End If


End Sub

Function Count_Files_fromFolder(TargetFolder As String, SearchMask As String) As Integer


Dim rescol As New Collection

gobjObjects.Utils.ListFiles TargetFolder, rescol, True, True, SearchMask

Count_Files_fromFolder = rescol.count


End Function

Function ClrFndRpl()

 With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
 End With

 FndRpl "", "", _
  True, 1, False, False, False, False, False, False

End Function

Function GoToMain()

If ActiveDocument.ActiveWindow.View = wdPageView Then
 ActiveDocument.ActiveWindow.View.SeekView = wdSeekMainDocument
End If

If ActiveDocument.ActiveWindow.View.SplitSpecial <> wdPaneNone Then
 ActiveDocument.ActiveWindow.View.SplitSpecial = wdPaneNone
End If

End Function

Function Find(TxtToFnd, _
              Optional Forward As Boolean, _
              Optional Wrap As Integer, _
              Optional Format As Boolean, _
              Optional MCase As Boolean, _
              Optional MWholeWord As Boolean, _
              Optional MWildcards As Boolean, _
              Optional MSoundsLike As Boolean, _
              Optional MAWForms As Boolean) As Boolean

 Find = True
 
 With Selection.Find
 
  .Text = TxtToFnd
  .Forward = Forward
  .Wrap = Wrap
  .Format = Format
  .MatchCase = MCase
  .MatchWholeWord = MWholeWord
  .MatchWildcards = MWildcards
  .MatchSoundsLike = MSoundsLike
  .MatchAllWordForms = MAWForms
  .Execute
  
  If Not .found Then Find = False

 End With

End Function

Function DoubleSplit(InputString As String, FirstDelimiter As String, SecondDelimiter As String, RetrievePart As Integer) As Variant
' FirstDelimiter is the separator which delimits the future aray's elements, SecondDelimiter is the delimiter which we need separate
' each array element, determining the 2 or more parts; RetrievePart represents which part of each array element to retrieve, positionally.
' (It could be 0, for the first part, 1 for the second, etc)

If InStr(1, InputString, FirstDelimiter) > 0 Then
    
    Dim tmpArr
    tmpArr = Split(InputString, FirstDelimiter)
    
    Dim tmpRes  ' will be made an array
    
    tmpRes = Split("", "")  ' empty one element array
    
    
    For i = 0 To UBound(tmpArr)
        
        If tmpArr(i) <> "" Then
        
            If UBound(Split(tmpArr(i), SecondDelimiter)) >= RetrievePart Then
                
                ' before doing any writing, check if we wrote the last element of array
                If tmpRes(UBound(tmpRes)) <> "" Then
                    ReDim Preserve tmpRes(UBound(tmpRes) + 1)
                End If
                
                If InStr(1, tmpArr(i), SecondDelimiter) > 0 Then
            
                    tmpRes(UBound(tmpRes)) = Split(TrimNonAlphaNums(CStr(tmpArr(i))), SecondDelimiter)(RetrievePart)
                
                Else
                    
                    If RetrievePart = 0 Then
                        tmpRes(UBound(tmpRes)) = tmpArr(i)
                    End If
                    
                End If
                    
            Else    ' user asking for part nonexistent in current string split, or second delimiter not existent at all in current string. Skip element.
            End If
        
        End If
        
    Next i
    
    DoubleSplit = tmpRes
    
End If

End Function


Function regexReplace(InputString As String, FindPattern As String, ReplacePattern As String) As String


Dim tmpResult As String

Dim regE As New regexp

With regE
    
    .Global = True
    .IgnoreCase = False
    .Pattern = FindPattern
    
    If .test(InputString) Then
        tmpResult = .Replace(InputString, ReplacePattern)
    Else
        tmpResult = InputString
    End If
    
End With
    
regexReplace = tmpResult

End Function


Function Test_GetINISection() As String

Dim sF As New SettingsFileClass

If sF.Exists Then
    
    Dim tmpS As String
    
    tmpS = sF.GetINISection(sF.Path, "GSC_A3_Options")
    
    Debug.Print "Get INI Section: " & tmpS
    Test_GetINISection = tmpS
    
End If

End Function


Function Is_ULg_Doc(FileName As String) As Boolean
' version 0.6
' independent
'
' 0.5 - help one TW user, their laptops are not named acc to scheme. WHY would they, of course.
' 0.6 - extracting unit language from "clientname" env var as well, for TW

Dim edr_PointPos As Byte
Dim unitLang As String

unitLang = Get_ULg

If Not IsValidLangIso(unitLang) Then
        Is_ULg_Doc = False
        MsgBox "Language unit setting invalid (" & unitLang & ")" & vbCr & "Please set it using GSC A3 options form and retry", vbOKOnly, "Invalid language unit "
End If

edr_PointPos = InStr(1, FileName, ".")

If Mid$(FileName, edr_PointPos + 1, 2) = unitLang Then
    Is_ULg_Doc = True
Else
    Is_ULg_Doc = False
End If


End Function

'/******************************************************************************************************************
'* Purpose: function to retrieve ISO language code of language unit where macro is running.
'* 3.0 - Upgrade for language-unit independent running. Using settings file instead of auto-detecting unit language, using various methods - unrealiable
'*/
Function Get_ULg() As String
' Retrieving value associated with option key "LanguageUnit" from "GSC_A3_Options" section


Dim sFile As New SettingsFileClass

If Not sFile.Exists Then
    Get_ULg = ""
    GoTo MsgUser
End If

If sFile.IsEmpty = FileEmpty Or sFile.IsEmpty = ErrorFindingFile Then
    Get_ULg = ""
    GoTo MsgUser
End If

Dim storedLg As String
storedLg = sFile.getOption("LanguageUnit", "GSC_A3_Options")

If storedLg = "" Or LCase(storedLg) = "xx" Then
    Get_ULg = ""
    GoTo MsgUser
Else
    Get_ULg = LCase(storedLg)
End If

Exit Function


'*************************************************************************************
MsgUser:
    MsgBox "It seems this is the first time running this program (or you have reset its settings)" & vbCr & _
        "Please choose a language unit from the GSC A3 Settings form to continue" & vbCr & vbCr & "(using �Preferences� button from �GSC A3� tab)", vbOKOnly, "Missing language unit setting"
    'Call Show_GSCA3_Options
    End

End Function


Function Get_AutoUlg()

Dim gDriveLabel As String

gDriveLabel = GetNetworkDrive_Label("G")

If gDriveLabel <> "" Then
    Get_AutoUlg = LCase(Right(gDriveLabel, 2))
End If

End Function


' Hopefully improved version of getProjectFolderOf, to handle multi-language projects as well
' Optional parameter allows usage of "CM 1235/18" type of project ID, no language needed (will simply retrieve all language folders found on network repository
' Optional parameter ReturnArray returns array of strings (folder paths) instead of comma-separated string as it would do otherwise
Function NgetProjectFolderOf(projectID As String, Optional UseStandardName, Optional ReturnArray) As Variant


Dim suppliedLanguage As String
suppliedLanguage = getLanguageFromProjectID(projectID, UseStandardName)

Dim anonProjectID As String
anonProjectID = getAnonymous_ProjectFilename(projectID, UseStandardName, "UseQuestionMark")

Dim candidateFolders As foundFilesStruct
candidateFolders = bDir.bGDirAllFolders(anonProjectID, ProjectsBaseFolder)

Dim tmpResult

Select Case candidateFolders.count
        
    Case 0
        NgetProjectFolderOf = ""
    
    Case 1
        If IsMissing(ReturnArray) Then
            NgetProjectFolderOf = candidateFolders.files(0)
        Else
            ReDim tmpResult(0)
            tmpResult(0) = candidateFolders.files(0)
            NgetProjectFolderOf = tmpResult
        End If
    
    Case Is > 1
        
        If IsMissing(ReturnArray) Then
            
            For i = 0 To candidateFolders.count - 1
                If isUsefulProjectFolder(candidateFolders.files(i)) Then
                    tmpResult = IIf(tmpResult = "", candidateFolders.files(i), tmpResult & "," & candidateFolders.files(i))
                End If
            Next i
            
            NgetProjectFolderOf = tmpResult
                
        Else    ' user wants array returned
            
            For i = 0 To candidateFolders.count - 1
                If isUsefulProjectFolder(candidateFolders.files(i)) Then
                    
                    If i = 0 Then
                        ReDim tmpResult(0)
                    Else
                        ReDim Preserve tmpResult(i)
                    End If
                    
                    tmpResult(i) = candidateFolders.files(i)
                    
                End If
            Next i

            NgetProjectFolderOf = tmpResult
            
        End If
        
End Select


End Function



Function getAnonymous_ProjectFilename(projectID As String, Optional UseStandardName, Optional UseQuestionMark) As String


Dim suppliedLanguage As String
suppliedLanguage = getLanguageFromProjectID(projectID, UseStandardName)


If IsMissing(UseStandardName) Then
' Using project file name
    
    If Is_GSC_Doc(projectID) Then
        ' we anonimise by making language agnostic
        getAnonymous_ProjectFilename = Replace(projectID, "." & suppliedLanguage, "." & "xx")
        If Not IsMissing(UseQuestionMark) Then getAnonymous_ProjectFilename = Replace(getAnonymous_ProjectFilename, ".xx", ".??")
        
    Else    ' ERROR ?
        getAnonymous_ProjectFilename = ""
    End If
    
Else    ' Using standard name
    
    Dim projFilename As String
    Dim projectIDCopy As String
    
    ' if user did not supply language code, we add it
    If Not IsValidLangIso(Right(projectID, 2)) Then
        projectIDCopy = projectID & " XX"
    Else
        projectIDCopy = projectID
    End If
    
    ' And we transform it into gsc filename
    projFilename = GSC_StandardName_WithLang_toFileName(projectIDCopy, , "IgnoreSuppliedLanguage")
    
    ' allow usage of lower or upper case
    If InStr(1, projFilename, LCase(suppliedLanguage)) > 0 Then
        getAnonymous_ProjectFilename = Replace(projFilename, "." & LCase(suppliedLanguage), "." & "xx")
    ElseIf InStr(1, projFilename, UCase(suppliedLanguage)) > 0 Then
        getAnonymous_ProjectFilename = Replace(projFilename, "." & UCase(suppliedLanguage), "." & "xx")
    End If
    If Not IsMissing(UseQuestionMark) Then getAnonymous_ProjectFilename = Replace(getAnonymous_ProjectFilename, ".xx", ".??")
    
End If
    
    
End Function


' in contrast to function getLanguage_fromDocName, we are accepting either standard project ID info (CARS Search client format ie: "CM 1234 2018 REV 1 EN" - where language is last)
' or simply a GSC file name: "cm01234.en18" - with or without extension, cause ignored - where language is extracted from file name
Function getLanguageFromProjectID(projectID As String, Optional UseStandardName) As String

Dim tempLanguage As String

' Did user supply us with language info of original?
If Not IsMissing(UseStandardName) Then
    
    If IsValidLangIso(Right(projectID, 2)) Then
        tempLanguage = LCase(Right(projectID, 2))
    Else
        tempLanguage = "xx"     ' dafaulting to xx, not usefull anyway
    End If
Else    ' user probably supplied a gsc file name as parameter, not standard project ID
    
    tempLanguage = getLanguage_fromDocName(projectID)
    
End If

getLanguageFromProjectID = tempLanguage

End Function


' is target paragraph consisting of a page break par?
Function isPageBreakParagraph(InputParagraph As Paragraph) As Boolean

Dim parText As String
parText = Replace(Replace(InputParagraph.Range.Text, " ", ""), Chr(160), "")


If Replace(Replace(parText, Chr(12), ""), Chr(13), "") = "" Then
    isPageBreakParagraph = True
End If


End Function


Function GetFolder_FileNames_List(TargetFolder As String) As Variant

'\\DM4\HOME\Docs\gscA3_cmn.dotm\customUI\images

Dim fso As New Scripting.FileSystemObject
Dim tFso As Scripting.Folder

If fso.FolderExists(TargetFolder) Then
    Set tFso = fso.GetFolder(TargetFolder)
Else
    Debug.Print "GetFolder_FileNames_List: Supplied folder (" & TargetFolder & ") does NOT exist, Exiting.."
    Exit Function
End If

Dim tffo As Scripting.File

Dim tmpArr() As String
ReDim tmpArr(tFso.files.count - 1)

Dim k As Integer
k = -1

For Each tffo In tFso.files
    
    k = k + 1
    tmpArr(k) = tffo.Name
    
Next tffo

Call gscrolib.Sort_Array(tmpArr)    ' sort in place

GetFolder_FileNames_List = tmpArr

End Function
