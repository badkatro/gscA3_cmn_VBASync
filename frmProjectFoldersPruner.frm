VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmProjectFoldersPruner 
   OleObjectBlob   =   "frmProjectFoldersPruner.frx":0000
   Caption         =   "Project folders prune"
   ClientHeight    =   2445
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   2715
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   15
End
Attribute VB_Name = "frmProjectFoldersPruner"
Attribute VB_Base = "0{83046305-8113-41F1-9101-CF1DC5F4FCEC}{38D92522-728C-443B-BA29-500462BACA76}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False
Private Sub cmdDelete_Click()

If MsgBox("Warning! All project folders older than " & Me.tbDateThreshold.Text & " will be deleted!" & vbCr & vbCr & _
    "Are you sure you wish to continue?", vbOKCancel + vbQuestion, "Multiple folders deletion!") = vbOK Then
    
    Call RUETitlesExtract.Delete_TPrepared_ProjectFolders_OT(Me.tbDateThreshold.Text, , "MessageToForm")    ' simply do not use optional parameter MessageToForm when calling from immediate window!

End If

End Sub

Private Sub cmdFind_Click()

Dim result As String

result = RUETitlesExtract.Print_Tprepared_ProjFolders_OT(Me.tbDateThreshold.Text)

' heighten form to show message
frmProjectFoldersPruner.Height = frmProjectFoldersPruner.Height + 40
frmProjectFoldersPruner.lbMessage.Caption = result

End Sub

Private Sub lbCalendar_Click()

Me.tbDateThreshold.Text = CalendarForm.GetDate

End Sub


Private Sub lbMessage_DblClick(ByVal Cancel As MSForms.ReturnBoolean)

lbMessage.Caption = ""
frmProjectFoldersPruner.Height = 114

End Sub

Private Sub UserForm_Initialize()

' hide message form part at startup
frmProjectFoldersPruner.Height = 114
' and clear it as well
frmProjectFoldersPruner.lbMessage.Caption = ""

End Sub
