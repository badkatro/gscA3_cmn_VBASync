VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} DocSplit 
   OleObjectBlob   =   "DocSplit.frx":0000
   Caption         =   "DocSplitter (v1.3 -  16/10/2014) - badkat version"
   ClientHeight    =   3435
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   6345
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   441
End
Attribute VB_Name = "DocSplit"
Attribute VB_Base = "0{9A26A7B9-0E0A-4B05-B0CD-4AD85AE251CB}{07C324DC-387B-442F-BE58-1C1304C9ED0C}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False




Dim ctDocNumPages As Integer                ' current document's number of pages
Dim ctDocCharCount As Long

Dim ctDocFootnotesCount As Long             ' current document's footnotes story character count
Dim ctDocMaintextCount As Long              ' current document's main text story character count

Dim prefferedMaxDocPart As Integer          ' preffered number of pages in a doc piece

Dim autoPartsNum As String
Dim custPartsNum As String
Const tHomeFolderBase As String = "T:\Docs"
Const partsNumTemplate As String = "%document% has ## pages, the parts shall have #|# pages"

Const optionsBaseFolder As String = "C:\Users\#user#\AppData\Roaming\Microsoft\Word"
Const optionsFilename As String = "gscA3_cmn.gscro.ini"
Const optionsFileSectionName  As String = "DocSplitterOptions"

Dim ctPropPages As Variant
Dim ctLineRng As Range

' save initial users doc view type, for restoration. For DocSplit.
Public iView As WdViewType



' Document splitter by Tarmo Kuub
' modified by Claudiu Margarint for GUI improvements, translation and automatic splitting proposal
' v1.1 - 3/7/2014
' v1.2 - 5/8/2014


' *************************************************************************************************************************************************
' FOR INFO, text file with name stored in optionsFilename is to contain the following options with following possible values, in order, in
' Windows 95 "INI" format:
'
' IdealDocPartSize=30                   (30 is default, we allow any number, user discretion)
' CreateFolderForParts=False            (or "True")
' SaveToLocation=SaveToCustomFolder     (as saved in "CustomFolderPath", or, if not set or incorrectly set, call browsing function)
'   or          =SaveToTUserFolder      (T:\Docs\<windowsusername>)
'   or          =SaveToDocumentsFolder  (Word's documents folder, as set in "")
'   or          =SaveToCurrentDocHost   (Current documents' location)
' CustomFolderPath=<FolderPath>         (complete path to a folder of choice to which user has write privileges)
' ***************************************************************************************************************************************************



Public Sub UserForm_Initialize()
' initialize event handler for user form
    
    ' Re-snap to right size, even if developer left you wide opened.
    ' If Me.Width > 330 Then Me.Width = 325
    
    
    If Documents.count > 0 Then
            
        ' Save current user's document view type, we'll need change that to PrintPreview
        
        iView = ActiveWindow.ActivePane.View.Type
        ' otherwise the routines wont work
        ActiveWindow.ActivePane.View = wdPrintPreview
            
            
        Call setMaxPrefferredDocPart
        
        If getOption("UseCharacterCount") = "True" Then
            
            Dim tmpR As Range
            Dim ctdc As Long
            
            Set tmpR = ActiveDocument.StoryRanges(wdMainTextStory)
            ctDocMaintextCount = tmpR.Characters.count              ' note down main text char number in doc
            ctdc = ctdc + tmpR.Characters.count
            
            Set tmpR = ActiveDocument.StoryRanges(wdFootnotesStory)
            ctDocFootnotesCount = tmpR.Characters.count             ' note down footnotes char number in doc
            ctdc = ctdc + tmpR.Characters.count
            
            ctDocCharCount = ctdc
            
        End If
        
        ' get and store number of pages in doc
        ctDocNumPages = ActiveDocument.StoryRanges(wdMainTextStory).Information(wdNumberOfPagesInDocument)
        
        ' and select text of first part's ending page
        txtStart1.Text = Format("1", "000")
        txtEnd1.Text = Format(ctDocNumPages, "000")

        MultiPage1.Value = 0    ' Select Auto page... we like presenting an auto proposal... so elegant !
        
        ' update label presenting statistics.. doc name, pages number, parts page numbers
        If InStr(1, lblMainMessage.Caption, "#") > 0 Or InStr(1, lblPages.Caption, "#") > 0 Then
            Call UpdateLblStatistics(0) ' for auto page
        End If
                
        ' We might set "DS" bookmarks just below, so clean document before that
        If CountBookmarksNamed("DS_*") > 0 Then
            Delete_AllBookmarksNamed ("DS_*")
        End If
                
        Dim splitpages As Variant
        If getOption("UseCharacterCount") = "True" Then
            splitpages = Calculate_AutosplitPages_ByCharacter
        Else
            splitpages = CalculateAutosplitPages  ' and calculate and retrieve number of proposed parts and corresponding starting pages
        End If
        
        ' populate textboxes for custom parts with automatic split pages, TAKING care not to use more than 10 parts...
        Call populatePgCustom(splitpages)
        
        ' get and display custom folder, if in use
        Dim storedCustomFolder As String
        storedCustomFolder = getOption("CustomFolderPath")
        
        If Not storedCustomFolder = "" Then
            Me.lblCustomFolder = storedCustomFolder
            'MacroContainer.Save
        Else
        End If
        
    End If
    
End Sub


Private Sub cbDeleteAllBk_Click()
' Command button, delete all bookmarks

If Documents.count = 0 Then
    Exit Sub
End If

If ActiveDocument.Bookmarks.count = 0 Then
    Exit Sub
End If

Dim ctBk As Bookmark


For Each ctBk In ActiveDocument.Bookmarks
    If Left(ctBk.Name, 3) = "DS_" Then
        ctBk.Delete
    End If
Next ctBk

Dim tmpTB As TextBox
Dim hm As Integer

hm = getLastVisibleRow


' Remove color from foreground of textboxes
If MultiPage1.selectedItem.Name = "pgCustom" Then
    For i = 0 To hm - 1
    
        Set tmpTB = getControlNamed("txtStart" & i + 1)
        tmpTB.ForeColor = wdColorBlack
        Set tmpTB = getControlNamed("txtEnd" & i + 1)
        tmpTB.ForeColor = wdColorBlack
    
    Next i
Else
    Me.lblPages.Caption = Replace(Me.lblPages, "*", "")
End If

StatusBar = "All DocuSplit bookmarks DELETED!"


End Sub


Private Sub cbInteractive_Click()
'Turn on or off the interactive navigation and split point adjusting module

' Check whether we need enable or disable the module (based on the "Next" button being on or off
If Not Me.cbNext.Enabled Then
    'Me.Height = 232
    
    Me.cbNext.Enabled = True
    Me.cbPrev.Enabled = True
    Me.cbModify.Enabled = True
                
    Me.Height = Me.Height + 45
        
        
    ctPropPages = getCt_SplitPages
    
        
    ' Upon opening up the interactive pagination mod tool, navigate to first split point
    If Not IsNull(ctPropPages) Then
        Selection.GoTo What:=wdGoToPage, which:=wdGoToAbsolute, count:=(ctPropPages(0, 0))
        
        Me.cbNext.Tag = "1"   ' the index of the next entry following first ! WE NEED NOT store actual page values in here !
        'Debug.Print "cbNext.Tag = " & cbNext.Tag
        Me.cbPrev.Tag = UBound(ctPropPages, 2)
        'Debug.Print "cbPrev.Tag = " & cbPrev.Tag
            
        Me.lblCtPage.Caption = Format(ctPropPages(0, 0), "###")
            
    End If
    
    Me.frmOptions.Height = Me.Height - 38
    
Else    ' shut down interactive mode

    Me.cbPrev.Enabled = False
    Me.cbNext.Enabled = False
    Me.cbModify.Enabled = False
    
    Me.Height = Me.Height - 45
    
    ' un-color current line
    If Not ctLineRng Is Nothing Then
        ctLineRng.Shading.BackgroundPatternColor = wdColorAutomatic
    End If
    
    ' Dont delete bookmarks when switching off interactive mode
    'Call Delete_AllBookmarksNamed("DS_*")
    
    ctAutoPropPages = Null
    ctCustPropPages = Null
    
    Me.frmOptions.Height = Me.Height - 38
    
End If


End Sub


Function getCt_SplitPages() As String()

Dim autoSPArr As Variant
Dim spR() As String

If Me.MultiPage1.selectedItem.Name = "pgAuto" Then
' we're using the auto proposal
    
    autoSPArr = GetAutosplitPages
    
    
    ReDim spR(1, UBound(autoSPArr))
    
    For j = 0 To UBound(autoSPArr)
        spR(0, j) = autoSPArr(j)
    Next j
    
    'For j = 0 To UBound(spResult)
        'spR(1, j) = ""
    'Next j
    
    getCt_SplitPages = spR     ' retrieve page numbers calculated and displayed in the auto proposal tab
    
    ReDim autoSPArr(0)
    ReDim spR(0, 0)
    
Else
' we're using the custom proposal tab

    Dim completePagesBreakdown As Variant
    completePagesBreakdown = GetCustomSplitPages
    
    'Dim ctCustPropPages As Variant
    'ctPropPages = GetCustomSplitPages   ' retrieve custom inputted pages number from custom tab
    
    'Dim startingPagesBreakdown() As String
    'ReDim startingPagesBreakdown(0)
    
    'For i = 0 To UBound(completePagesBreakdown, 2)
        'ReDim Preserve startingPagesBreakdown(i)
        'startingPagesBreakdown(i) = completePagesBreakdown(0, i)
    'Next i
    
    getCt_SplitPages = completePagesBreakdown
    
    ReDim completePagesBreakdown(0)
    'ReDim startingPagesBreakdown(0)
    
End If


End Function



Function CountBookmarksNamed(BookmarkNameMask As String) As Integer ' retrieve number of bookmarks in document called as the mask demands

If ActiveDocument.Bookmarks.count = 0 Then
    CountBookmarksNamed = 0
    Exit Function
End If

Dim bkm As Bookmark
For Each bkm In ActiveDocument.Bookmarks
    If bkm.Name Like "DS_#*" Then
        Dim tCounter As Integer
        tCounter = tCounter + 1
    End If
Next bkm

CountBookmarksNamed = tCounter

End Function


Sub Delete_AllBookmarksNamed(BookmarkNameMask As String)

If ActiveDocument.Bookmarks.count = 0 Then
    Exit Sub
End If

Dim bkm As Bookmark
For Each bkm In ActiveDocument.Bookmarks
    If bkm.Name Like BookmarkNameMask Then
        bkm.Delete
    End If
Next bkm

End Sub



Private Sub cbModify_Click()
' Set a bookmark to selection point named


Dim ctI As String
ctI = ctPage_VSplitIndex(Me.lblCtPage.Caption)

If ctI = "" Then
    MsgBox "ctPage_VSplitIndex function returned empty string while attempting to determine the index of " & lblCtPage.Caption, vbOKOnly, "Error in cbModify_Click"
    Exit Sub
End If

' IF we're on a STARTING SPLITTING POINT, discontinuous or continuous...
If Not Is_Ending_SplitPoint Then
    
    ' if it's a CONTINUOUS one, we delete ending of preceding and starting of current, recreate them both
    If Not Is_Discontinuos_StartingSplitPoint(CInt(ctI)) Then
        
        ' if starting point of current doc part bookmark exists
        If Exists_bookmarkLike("DS_" & ctI & "_S_*") Then
            
            Dim ctSP As Bookmark
            ' retrieve it into variable
            Set ctSP = Get_BookmarkLike("DS_" & ctI & "_S_*")
            
            If Not ctSP Is Nothing Then
                ' and delete it
                ctSP.Delete
                
                ' if ending of previous doc part exists
                If Exists_bookmarkLike("DS_" & CInt(ctI) - 1 & "_E_*") Then
                    
                    ' retrieve its bookmark
                    Set ctSP = Get_BookmarkLike("DS_" & CInt(ctI) - 1 & "_E_*")
                    ' and delete it
                    ctSP.Delete

                End If
                
            Else
                ' Error message or what?
                MsgBox "Error, could not retrieve current split point bookmark into variable, though " & vbCr & _
                    "DS_" & ctI & "_S_*" & " is reported as existant !"
                Exit Sub
                
            End If
            
        End If
        
        ' and add a new STARTING one in its place at cursor position
        Selection.Bookmarks.Add ("DS_" & ctI & "_S_" & Format(Selection.Information(wdActiveEndAdjustedPageNumber), "000"))
        ' and a new ENDING one for the preceding part, should really move selection one character back
        Selection.Bookmarks.Add ("DS_" & CInt(ctI) - 1 & "_E_" & Format(Selection.Information(wdActiveEndAdjustedPageNumber), "000"))
        
        If MultiPage1.selectedItem.Name = "pgAuto" Then
            ' and signal to the user that this part has been tainted by a custom modification
            Me.lblPages.Caption = Replace(Me.lblPages.Caption, ctPropPages(0, ctI), ctPropPages(0, ctI) & "*")
        Else
            Dim ctTextB As TextBox
            Set ctTextB = Me.getControlNamed("txtStart" & CInt(ctI) + 1)
            
            ctTextB.ForeColor = wdColorRed
        End If
        
    Else    ' if STARTING and DISCONTINUOUS
        
        If Exists_bookmarkLike("DS_" & ctI & "_S_*") Then
            
            Set ctSP = Get_BookmarkLike("DS_" & ctI & "_S_*")
            
            If Not ctSP Is Nothing Then
                
                ctSP.Delete
                
            Else
                ' ????????
            End If
            
        End If
        
        ' and add a new STARTING one in its place at cursor position
        Selection.Bookmarks.Add ("DS_" & ctI & "_S_" & Format(Selection.Information(wdActiveEndAdjustedPageNumber), "000"))
        
        If Me.MultiPage1.selectedItem.Name = "pgAuto" Then
            ' and signal user modification
            Me.lblPages.Caption = Replace(Me.lblPages.Caption, ctPropPages(0, ctI), ctPropPages(0, ctI) & "*")
        Else
            Set ctTextB = Me.getControlNamed("txtStart" & CInt(ctI) + 1)
            ctTextB.ForeColor = wdColorRed
        End If

        
    End If
    
    
Else    ' IF ENDING SplitPoint (Next starting will ONLY be discontinuous)
    
    If Exists_bookmarkLike("DS_" & ctI & "_E_*") Then
    
        Set ctSP = Get_BookmarkLike("DS_" & ctI & "_E_*")
    
        If Not ctSP Is Nothing Then
            
            ctSP.Delete
            
        Else
            ' ????????
        End If
    
    End If
    
    ' and add a new STARTING one in its place at cursor position
    Selection.Bookmarks.Add ("DS_" & ctI & "_E_" & Format(Selection.Information(wdActiveEndAdjustedPageNumber), "000"))


    Dim ctTxtBox As TextBox
    
    Set ctTxtBox = getControlNamed("txtEnd" & CInt(ctI))
    
    ctTxtBox.BackColor = wdColorRed

End If


'if old attention line is still shaded, undo it
If Not ctLineRng Is Nothing Then
    ctLineRng.Shading.BackgroundPatternColor = wdColorAutomatic
End If

' Lets signal to user where we are
Selection.HomeKey
Selection.EndKey wdLine, wdExtend
Set ctLineRng = Selection.Range
ctLineRng.MoveEndWhile vbCr, wdBackward
Selection.Collapse
ctLineRng.Shading.BackgroundPatternColor = wdColorLightOrange


End Sub


Function ctPage_HSplitIndex(currentPage As String) As String
' supply function with a current page and it will return the horizontal index of that page in the
' ctPropPages global userform variable (the list of all starting - and ending pages, in case of custom chop)

If Not IsEmpty(ctPropPages) Then
    
    For i = 0 To UBound(ctPropPages, 2)
        If StrComp(currentPage, CInt(ctPropPages(0, i)), vbTextCompare) = 0 Then
            ctPage_HSplitIndex = "0"
            Exit Function
        End If
    Next i
    
    ' if we have a second dimension of array, lets also look for input into the ENDING ranges...
    If Not ctPropPages(1, 0) = "" Then
        For i = 0 To UBound(ctPropPages, 2)
            If StrComp(currentPage, CInt(ctPropPages(1, i)), vbTextCompare) = 0 Then
                ctPage_HSplitIndex = "1"
                Exit Function
            End If
        Next i
    Else
        ' special case for auto split, where we decided ... whose fault was that !?! to leave
        ' second arm of array empty !
        For i = 0 To UBound(ctPropPages, 2)
            If StrComp(currentPage, CInt(ctPropPages(0, i)) - 1, vbTextCompare) = 0 Then
            ctPage_HSplitIndex = "1"
            Exit Function
        End If
    Next i
    
    End If
    
    ' if we reach here without having jumped out, we haven't found the input
    ctPage_HSplitIndex = ""
    
Else
    ctPage_HSplitIndex = ""
End If



End Function


Function ctPage_VSplitIndex(currentPage As String) As String
' supply function with a current page and it will return the vertical index of that page in the
' ctPropPages global userform variable (the list of all starting - and ending pages, in case of custom chop)


If Not IsEmpty(ctPropPages) Then
    
    For i = 0 To UBound(ctPropPages, 2)
        If StrComp(currentPage, Format(ctPropPages(0, i), "###"), vbTextCompare) = 0 Then
            ctPage_VSplitIndex = i
            Exit Function
        End If
    Next i
    
    ' if we have a second dimension of array, lets also look for input into the ENDING ranges...
    If Not ctPropPages(1, 0) = "" Then
        For i = 0 To UBound(ctPropPages, 2)
            If StrComp(currentPage, Format(ctPropPages(1, i), "###"), vbTextCompare) = 0 Then
                ctPage_VSplitIndex = i
                Exit Function
            End If
        Next i
    Else
        ' Special case for auto split, where second arm is empty...
        ' BUT we calculate easily ending parts split point by using "-1" trick
        For i = 0 To UBound(ctPropPages, 2)
            If StrComp(currentPage, Format(ctPropPages(0, i) - 1, "###"), vbTextCompare) = 0 Then
                ctPage_VSplitIndex = i
                Exit Function
            End If
        Next i
        
    End If
    
    ' if we reach here without having jumped out, we haven't found the input
    ctPage_VSplitIndex = ""
    
Else
    ctPage_VSplitIndex = ""
End If


End Function


Function Is_Ending_SplitPoint() As Boolean
' checks whether current splitting poing is a starting one or an ending one

Dim ctSP_HIdx As Integer    ' current splitting point horizontal index (first level index in a bidimensional array, that is the number of the "arm" of the array)


ctSP_HIdx = CInt(ctPage_HSplitIndex(CInt(Me.lblCtPage.Caption)))

If ctSP_HIdx = 1 Then
    Is_Ending_SplitPoint = True
Else
    Is_Ending_SplitPoint = False
End If


End Function


Function Is_Discontinuos_StartingSplitPoint(startingPage_Splitpoint_Index As Integer) As Boolean
' checks whether a given index splitting point is discontinuous or not (a starting one)

Dim ctSI As Integer     ' current Splitpoint Starting Index

ctSI = startingPage_Splitpoint_Index


If ctSI - 1 >= 0 Then
    If ctPropPages(1, 0) <> "" Then
        If CInt(ctPropPages(0, ctSI)) = CInt(ctPropPages(1, ctSI - 1)) + 1 Then     ' if ending page value of preceding doc part is equal to starting page value of next splitting point then it's a continuos splitting point
            ' it's continuous
            Is_Discontinuos_StartingSplitPoint = False
        Else
            ' it's a discountinuous one ! (preceding doc part ending page value is different than one less from current starting page value)
            Is_Discontinuos_StartingSplitPoint = True
        End If
    Else
        Is_Discontinuos_StartingSplitPoint = False
    End If

Else    ' we were already on first index (0), so.. there's no preceding part
        
    Is_Discontinuos_StartingSplitPoint = False  ' should we return integers instead ? (0 for not, 1 for yes, -1 for not applicable)
    
End If


End Function


Function CustomSplitPoint_Exists_forCt_DocPart(WhichKind As String, WhichDirection As WdGoToDirection) As Boolean

' WhichKind should be "E" for Ending and "S" for Starting
' WhichDirection should be wdGoToNext or wdGoToPrevious (0 or -1 in fact)

' Function returns whether current document part has already an ending splitpoint bookmark set.
' Used when wishing to jump to ending point (discontinuous) instead of jumping to next starting point (and, of course, instead of
' jumping to next ending splitpoint, it should jump to this bookmark... that's why the user created (modified) it in the first place)


Dim directionDiferential As Integer     '
directionDiferential = IIf(WhichDirection = wdGoToNext, 0, -1)      ' because we add it to the value of current doc part index so as to go forth or back


Dim ctVIdx As String

ctVIdx = ctPage_VSplitIndex(Me.lblCtPage.Caption)

If Not ctVIdx = "" Then
    ctVIdx = ctVIdx + directionDiferential
Else
    CustomSplitPoint_Exists_forCt_DocPart = False
    Exit Function
End If


If Exists_bookmarkLike("DS_" & ctVIdx & "_" & UCase(WhichKind) & "_*") Then
    CustomSplitPoint_Exists_forCt_DocPart = True
    Exit Function
End If

End Function


Function Get_BookmarkLike(BookmarkMask As String) As Bookmark

' BookmarkMask is a wildcard expression normally, using what the Like function in VBA supports,
' that is ?, * and #

If ActiveDocument.Bookmarks.count = 0 Then
    Set Get_BookmarkLike = Nothing
End If


Dim bk As Bookmark

For Each bk In ActiveDocument.Bookmarks
    If bk.Name Like BookmarkMask Then
        Set Get_BookmarkLike = bk
        Exit Function
    End If
Next bk

Set Get_BookmarkLike = Nothing


End Function


Function Exists_bookmarkLike(BookmarkMask As String) As Boolean
' BookmarkMask is a wildcard expression normally, using what the Like function in VBA supports,
' that is ?, * and #

If ActiveDocument.Bookmarks.count = 0 Then
    Exists_bookmarkLike = False
End If


Dim bk As Bookmark

For Each bk In ActiveDocument.Bookmarks
    If bk.Name Like BookmarkMask Then
        Exists_bookmarkLike = True
        Exit Function
    End If
Next bk

Exists_bookmarkLike = False


End Function


Private Sub cbNext_Click()

If Me.cbNext.Tag <> "" Then
         
    'if ctLine range is set, remove its shading before taking off
    If Not ctLineRng Is Nothing Then
        ctLineRng.Shading.BackgroundPatternColorIndex = wdNoHighlight
    End If
                
    Me.cbPrev.Tag = IIf(Me.cbNext.Tag = "0", UBound(ctPropPages, 2), CStr(CInt(Me.cbNext.Tag) - 1))   ' store last ct index into prev button prop and move on
    Debug.Print "cbPrev.Tag = " & cbPrev.Tag
    
    
    ' before moving on to next recorded starting splitpoint, check whether we need to stop to the current one's ending point (if next starting point is discontinuous)
    If Not Is_Ending_SplitPoint And Is_Discontinuos_StartingSplitPoint(CInt(Me.cbNext.Tag)) Then
        ' if user has already redefined the ending point of current (there's a custom bookmark for it), stop to that instead of the end of range
        If CustomSplitPoint_Exists_forCt_DocPart("E", wdGoToNext) Then
            
            Dim nepbk As Bookmark
            Set nepbk = Get_BookmarkLike("DS_*" & "_E_*")
            
            If Not nepbk Is Nothing Then
                Selection.GoTo What:=wdGoToBookmark, Name:=nepbk.Name
                Selection.HomeKey wdLine, wdExtend
            End If
            
        Else    ' THERE's no such bookmark, just go ahead and go to the end of the current one
        
            Dim vIdx As Integer     ' vertical Index
            vIdx = CInt(ctPage_VSplitIndex(Me.lblCtPage.Caption))
            
            Dim ctSplitPart_EndingPage As Integer       '
            ctSplitPart_EndingPage = CInt(ctPropPages(1, vIdx))
            
            Dim ctSplitPart_EndingChar As Range
            ' Cannot go to "next" docpart starting point if we're already at end, there's no such thing
            If vIdx = UBound(ctPropPages, 2) Then   ' to see if ubound with two parameters works in this situation, ctPropPages is a Variant data type !
                Selection.EndKey wdStory, wdMove
                Selection.MoveEndWhile Chr(12) & vbCr, wdBackward
                Selection.HomeKey wdLine, wdExtend
                
            Else    ' there's a next docpart, use its starting to determine the ending of previous (one char back)
                Set ctSplitPart_EndingChar = ActiveDocument.Range.GoTo(wdGoToPage, wdGoToAbsolute, ctSplitPart_EndingPage + 1)
                ' now trace back one char, you're at the beginning of the next page
                ctSplitPart_EndingChar.MoveEndWhile Chr(12) & vbCr, wdBackward
                
                Me.lblCtPage.Caption = CStr(ctSplitPart_EndingChar.Information(wdActiveEndPageNumber))
                ' and select current destination split point range
                ctSplitPart_EndingChar.Select
                Selection.HomeKey wdLine, wdExtend
                
                Me.cbNext.Tag = CStr((CInt(Me.cbNext.Tag)) Mod (UBound(ctPropPages, 2) + 1))
                Debug.Print "cbNext.Tag = " & cbNext.Tag

            End If
        End If
        
        Set ctLineRng = Selection.Range
        ctLineRng.MoveEndWhile vbCr, wdBackward
        Selection.Collapse wdCollapseEnd

        
    Else    ' If we're on an ending splitpoint or a starting with next one continuous
        
        ' Before moving selection to the next starting splitpoint, check whether it has been redefined (in both cases... if we're on an
        ' ending one or a starting one)
        
        If Exists_bookmarkLike("DS_" & Me.cbNext.Tag & "_S_*") Then
            
            ' GO TO redefined (bookmarked) next start instead of the normal start of page
            Dim nsSPbk As Bookmark
            ' Retrieve bookmark object
            Set nsSPbk = Get_BookmarkLike("DS_" & Me.cbNext.Tag & "_S_*")
            
            If Not nsSPbk Is Nothing Then
                Selection.GoTo What:=wdGoToBookmark, Name:=nsSPbk.Name
                Selection.EndKey wdLine, wdExtend
            Else
                '????
            End If
            
            ' And display the current page in special label
            Me.lblCtPage.Caption = CInt(ctPropPages(0, Me.cbNext.Tag Mod (UBound(ctPropPages, 2) + 1)))
            Me.cbNext.Tag = CStr((CInt(Me.cbNext.Tag) + 1) Mod (UBound(ctPropPages, 2) + 1))
            Debug.Print "cbNext.Tag = " & cbNext.Tag

            
        Else
            Selection.GoTo What:=wdGoToPage, which:=wdGoToAbsolute, count:=ctPropPages(0, (Me.cbNext.Tag) Mod (UBound(ctPropPages, 2) + 1))
            Selection.EndKey wdLine, wdExtend
            ' And display the current page in special label
            Me.lblCtPage.Caption = CInt(ctPropPages(0, Me.cbNext.Tag Mod (UBound(ctPropPages, 2) + 1)))
             'and now lets store the next starting page splitting point into next button�s tag
            Me.cbNext.Tag = CStr((CInt(Me.cbNext.Tag) + 1) Mod (UBound(ctPropPages, 2) + 1))
            Debug.Print "cbNext.Tag = " & cbNext.Tag
        End If
        
        Set ctLineRng = Selection.Range
        ctLineRng.MoveEndWhile vbCr, wdBackward
        Selection.Collapse
        
    End If
    
    ' Lets signal to user where we are
    'Selection.HomeKey
    'Selection.EndKey wdLine, wdExtend
    
    ctLineRng.Shading.BackgroundPatternColor = wdColorLightOrange
    
    
Else
    
    'if ctLine range is set, remove its shading before taking off
    If Not ctLineRng Is Nothing Then
        ctLineRng.Shading.BackgroundPatternColorIndex = wdNoHighlight
    End If
    
    Selection.GoTo What:=wdGoToPage, which:=wdGoToAbsolute, count:=ctPropPages(0, 0)
    
        
    Me.lblCtPage.Caption = CInt(ctPropPages(0, 0))
    
    Me.cbNext.Tag = "1"
    'Debug.Print "cbNext.Tag = " & cbNext.Tag
    Me.cbPrev.Tag = UBound(ctPropPages, 2)
    'Debug.Print "cbPrev.Tag = " & cbPrev.Tag
End If

End Sub


Private Sub cbPrev_Click()


If Me.cbPrev.Tag <> "" Then
    
    'if ctLine range is set, remove its shading before taking off
    If Not ctLineRng Is Nothing Then
        ctLineRng.Shading.BackgroundPatternColorIndex = wdNoHighlight
    End If
    
    Me.cbNext.Tag = IIf(Me.cbNext.Tag = "0", UBound(ctPropPages, 2), CStr(CInt(Me.cbNext.Tag) - 1))   ' store last ct index into prev button prop and move on
    
    ' separate case when we have to jump to something else than normal, meaning the beginning splitpoint of previous doc part
    ' THAT would be when we have to jump to an ending of the previous one, since the starting one we're on is discontinuos
    
    Dim vIdx As String
    vIdx = Me.cbPrev.Tag    ' HAVE to make an exception for the index of the previous doc part for when we're on the 1st part already
                            ' (Cause)
    
    ' Jumping to ending of previous doc part only when we're on a starting splitpoint and it's discontinuos
    If Not Is_Ending_SplitPoint And Is_Discontinuos_StartingSplitPoint(ctPage_VSplitIndex(Me.lblCtPage.Caption)) Then
        
        Dim prevSplitPart_EndingPage As Integer
        prevSplitPart_EndingPage = CInt(ctPropPages(1, vIdx))
        
        ' before we jump to the end of previous doc part, check whether user has not already redefined it... he'll be mad otherwise"
        If Exists_bookmarkLike("DS_" & vIdx & "_E_*") Then
            
            Dim pEndBk As Bookmark
            
            Set pEndBk = Get_BookmarkLike("DS_" & vIdx & "_E_*")
            
            
            If Not pEndBk Is Nothing Then
                Selection.GoTo What:=wdGoToBookmark, Name:=pEndBk.Name
                Selection.EndKey wdLine, wdExtend
            Else
                ' ????????????
            End If
            
            
        Else
            
            ' GO to the end of the previous section, which exists and hasn't been redefined by user
            
            Dim prevDPEndingPage As Integer
            
            prevDPEndingPage = CInt(ctPropPages(1, vIdx))
            
            ' if we were on first doc part and we get as previous part the last one, as wraparound works, we can't employ normal method, as below
            If prevDPEndingPage = ctDocNumPages Then
                Selection.GoTo What:=WdGoToItem.wdGoToLine, which:=wdGoToLast
            Else
                Selection.GoTo What:=wdGoToPage, which:=wdGoToAbsolute, count:=prevDPEndingPage + 1
                Selection.MoveEndWhile Chr(12) & vbCr, wdBackward
                Selection.HomeKey wdLine, wdExtend
            End If

        End If
        
        ' And display the current page in special label
        Me.lblCtPage.Caption = CStr(Selection.Information(wdActiveEndPageNumber))
        'Me.cbPrev.Tag = CStr((CInt(Me.cbPrev.Tag) - 1) Mod (UBound(ctPropPages, 2) + 1))
        
        Set ctLineRng = Selection.Range
        ctLineRng.MoveEndWhile vbCr, wdBackward
        Selection.Collapse wdCollapseEnd
        
        
    Else    ' If we're on a continuous starting one or on an ending one, we'll go, in both cases, to the previous' starting point
        
        ' BUT have to see whether it has somehow been redefined
        If Exists_bookmarkLike("DS_" & vIdx & "_S_*") Then
        
            Dim psBk As Bookmark
        
            Set psBk = Get_BookmarkLike("DS_" & vIdx & "_S_*")
            
            
            If Not psBk Is Nothing Then
                
                Selection.GoTo What:=wdGoToBookmark, Name:=psBk.Name
                Selection.EndKey wdLine, wdExtend
            Else
                ' Error message ???????????????
            End If
            
            
        Else
            
            Selection.GoTo What:=wdGoToPage, which:=wdGoToAbsolute, count:=ctPropPages(0, (Me.cbPrev.Tag) Mod (UBound(ctPropPages, 2) + 1))
            Selection.EndKey wdLine, wdExtend
            
        End If
        
        Me.lblCtPage.Caption = CInt(ctPropPages(0, Me.cbPrev.Tag Mod (UBound(ctPropPages, 2) + 1)))
        Me.cbPrev.Tag = IIf(Me.cbPrev.Tag = "0", CStr(UBound(ctPropPages, 2)), CStr(CInt(Me.cbPrev.Tag) - 1))
        
        Set ctLineRng = Selection.Range
        ctLineRng.MoveEndWhile vbCr, wdBackward
        Selection.Collapse
        
    End If
    
    ' Lets signal to user where we are
    'Selection.HomeKey
    'Selection.EndKey wdLine, wdExtend
    
    ctLineRng.Shading.BackgroundPatternColor = wdColorLightOrange
    
    'Me.lblCtPage.Caption = ctPropPages(0, (Me.cbPrev.Tag) Mod (UBound(ctPropPages, 2) + 1))
    'Me.cbPrev.Tag = IIf(Me.cbPrev.Tag = "0", CStr(UBound(ctPropPages, 2)), CStr(CInt(Me.cbPrev.Tag) - 1))
    
    'Debug.Print "cbPrev.Tag = " & cbPrev.Tag
    'Debug.Print "cbNext.Tag = " & cbNext.Tag
        
Else
    
    'if ctLine range is set, remove its shading before taking off
    If Not ctLineRng Is Nothing Then
        ctLineRng.Shading.BackgroundPatternColorIndex = wdNoHighlight
    End If
    
    Selection.GoTo What:=wdGoToPage, which:=wdGoToAbsolute, count:=ctPropPages(0, 0)
    Me.lblCtPage.Caption = ctPropPages(0)
    
    Me.cbNext.Tag = "1"
    'Debug.Print "cbNext.Tag = " & cbNext.Tag
    Me.cbPrev.Tag = "0"
    'Debug.Print "cbPrev.Tag = " & cbPrev.Tag
End If


End Sub


Function GetAutosplitPages() As Variant
' Function returns an array with border pages of page ranges from the pgAuto page,
' using the presented-to-user string, in the form "1|20|40". Pair of GetCustomSplitPages which does the same
' for pgCustom page

' defend against being called from the wrong page (pgCustom for instance)
If Me.MultiPage1.selectedItem.Name <> "pgAuto" Then
    GetAutosplitPages = Null
    Exit Function
End If

' defend against the target label having not been updated with info
If InStr(1, Me.lblPages.Caption, "#") > 0 Then
    GetAutosplitPages = Null
    Exit Function
End If

Dim pagesProposal As String
pagesProposal = Me.lblPages.Caption

Dim arrPgProp As Variant
arrPgProp = Split(pagesProposal, " | ")

' clean elements of array of stars
For i = 0 To UBound(arrPgProp)
    arrPgProp(i) = CStr(Replace(arrPgProp(i), "*", ""))
Next i


GetAutosplitPages = arrPgProp

End Function


Private Sub cbOptions_Click()

' enlarge so user may see options part of form
If Me.Width <= 330 Then
    ' but quickly set options on form according to user preferences in options file (options persistence) before
    Me.Width = 585
    Call updateFormOptions_fromOptionsFile
Else    ' on the contrary, no need to re-do the above again, now user is closing options part
    Me.Width = 330
End If


End Sub


Function updateFormOptions_fromOptionsFile() As Boolean

' CustomFolderPath=<FolderPath>
' SaveToLocation=SaveToCustomFolder     (as saved in "CustomFolderPath", or, if not set or incorrectly set, call browsing function)
'   or          =SaveToTUserFolder      (T:\Docs\<windowsusername>)
'   or          =SaveToDocumentsFolder  (Word's documents folder, as set in "")
'   or          =SaveToCurrentDocHost   (Current documents' location)
'   or          =SaveToProjectFolder    (Current documents' project folder in T:\Prepared originals)
' IdealDocPartSize=30
' CreateFolderForParts=False

Dim savedVal As String

Dim optionsList As Variant
optionsList = Array("IdealDocPartSize", "UseCharacterCount", "CreateFolderForParts", "SaveToLocation", "CustomFolderPath")

If optionsFileExists Then
    
    For i = 0 To UBound(optionsList)
    
        savedVal = getOption(CStr(optionsList(i)))
        
        If Not savedVal = "" Then
        
            Select Case optionsList(i)
                
                Case "IdealDocPartSize"
                    If Not txtIdealDocPart.Value = savedVal Then txtIdealDocPart.Value = savedVal
                Case "UseCharacterCount"
                    If Not cbUseCharacterCount.Value = savedVal Then cbUseCharacterCount.Value = savedVal
                Case "CreateFolderForParts"
                    If Not cbCreateFolderForParts.Value = savedVal Then cbCreateFolderForParts.Value = savedVal
                Case "SaveToLocation"
                    Select Case savedVal
                        Case "SaveToTUserFolder"
                            obSavetoUserFolder.Value = True
                        Case "SaveToDocumentsFolder"
                            obSavetoDocsFolder = True
                        Case "SaveToCurrentDocHost"
                            obSavetoCurrentFolder = True
                        Case "SaveToProjectFolder"
                            obSaveToProjectFolder = True
                        Case "SaveToCustomFolder"
                            obSavetoCustomFolder = True
                        End Select
                Case "CustomFolderPath"
                    If Not lblCustomFolder.Caption = savedVal Then lblCustomFolder.Caption = savedVal
                Case Else
                
            End Select
        
        End If

    Next i
    
End If

End Function


Private Sub cmdClearAll_Click()
' Clear all button, clear all text boxes except first and last
' and reduce parts number to 1

Dim tmptextbox As TextBox

' to decrease
makeVisible_WhichPart (1)

For i = 2 To 10
    Set tmptextbox = getControlNamed("txtStart" & i)
    tmptextbox.Text = ""
Next i

For i = 2 To 9
    Set tmptextbox = getControlNamed("txtEnd" & i)
    tmptextbox.Text = ""
Next i

Call UpdateLblStatistics(1)

End Sub


Public Sub cmdEqualize_Click()
' Equalise button, user just redefined number of parts, therefore renouncing our claim to not touch the magical number.
' SO they want a set number of parts, part size is to be determined and equalised


Dim userPartsNumber As Integer
' Let's retrieve new number of parts
userPartsNumber = getLastVisibleRow


Dim ucNumP As Integer       ' user chosen number of pages

ucNumP = ctDocNumPages

Dim boff As Integer

If CInt(Me.txtStart1.Text) <> 1 Then
    ' if user chose to leave a part off from beginning of first part, lets accomodate
    ucNumP = ctDocNumPages - (CInt(Me.txtStart1.Text) - 1)
    boff = CInt(Me.txtStart1.Text) - 1
End If


Dim lETxb As TextBox    ' last ending text box
' same goes for last part and the end of document
Set lETxb = getControlNamed("txtEnd" & userPartsNumber)

If CInt(lETxb.Text) <> ctDocNumPages Then
    ucNumP = ucNumP - (ctDocNumPages - CInt(lETxb.Text))
End If


Dim equalisedSplitPages As Variant
' and use our fresly modified function to calculate a new array of starting pages
If boff > 0 Then
    equalisedSplitPages = CalculateAutosplitPages(userPartsNumber, ucNumP, boff)
Else
    equalisedSplitPages = CalculateAutosplitPages(userPartsNumber, ucNumP)
End If

' with which we populate the text boxes from the Custom Split page... AHHH (or Aaaarrrghhhh... as Tarzan was saying...), I DO love well written software !
Call populatePgCustom(equalisedSplitPages)

' and properly update label statistics, number of pages of each part that is
Call UpdateLblStatistics(1)


End Sub


Private Sub cbOptionsApply_Click()
' If options file with predefined name (constant "optionsFilename") at predefined location (constant "optionsBaseFolder") exists,
' simply update all its options according to user input in userform, see possible options and their possible values on top of form
' If it does not, create it and do the same... write user input to options text file

' IdealDocPartSize=30
' CreateFolderForParts=False
' SaveToLocation=SaveToCustomFolder
'   or          =SaveToTUserFolder
'   or          =SaveToDocumentsFolder
'   or          =SaveToCurrentDocHost
'   or          =SaveToProjectFolder
' CustomFolderPath=<FolderPath>
' UseCharacterCount=False
        
Dim optionsNames As String
Dim userOptionsValues As String

' put options names into separated string, for "setOption" function
optionsNames = "IdealDocPartSize|UseCharacterCount|CreateFolderForParts|SaveToLocation|CustomFolderPath"

' step by step, build optvions values string as supplied by user
Dim changedIdealPart As Boolean
Dim savedIdealDocPart As String

' retrieve old (saved in options file) value for ideal part size, so we can determine if user just changed it !
savedIdealDocPart = getOption("IdealDocPartSize")
changedIdealPart = IIf(txtIdealDocPart.Value = savedIdealDocPart And savedIdealDocPart <> "", False, True)

userOptionsValues = txtIdealDocPart.Value & "|" & cbUseCharacterCount.Value & "|" & cbCreateFolderForParts.Value & "|"

Dim userSaveLocationChoice As String

Select Case True
    Case obSavetoUserFolder.Value
        userSaveLocationChoice = "SaveToTUserFolder"
    Case obSavetoDocsFolder.Value
        userSaveLocationChoice = "SaveToDocumentsFolder"
    Case obSavetoCurrentFolder.Value
        userSaveLocationChoice = "SaveToCurrentDocHost"
    Case obSavetoCustomFolder.Value
        userSaveLocationChoice = "SaveToCustomFolder"
    Case obSaveToProjectFolder.Value
        userSaveLocationChoice = "SaveToProjectFolder"
End Select

userOptionsValues = userOptionsValues & userSaveLocationChoice & "|"

userOptionsValues = userOptionsValues & IIf(lblCustomFolder.Caption = "N/A", "", lblCustomFolder.Caption)
        
' now to properly write the options and values to options file, if can we
If Not setOption(optionsNames, userOptionsValues) = 1 Then
        
    MsgBox "Failed to set selected options into options file " & optionsBaseFolder & "\" & optionsFilename & vbCrCr & _
        "Please check if file is not (b)locked and retry !", vbOKOnly + vbCritical, "Error writing options !"
        
End If

' Lets see if it works to recalculate in real time if user changes ideal doc part size !
If changedIdealPart Then
    Unload Me: Load DocSplit: Call updateFormOptions_fromOptionsFile: DocSplit.Show
Else
    Call updateFormOptions_fromOptionsFile
End If


End Sub

Private Sub Image1_Click()


Call MakeForm_Resizable(frmDocSplitterHelp)

frmDocSplitterHelp.Show


End Sub

Private Sub imgChangeCustomFolder_Click()
'
If Not setCustomFolderOption(True) Then     ' function, prompts user for folder selection and returns selected folder path
    MsgBox "Could not set SaveToCustomFolder option, please contact margama at 2942!", vbOKOnly, "Error setting custom folder option"
End If

End Sub

Private Sub imgChangeCustomFolder_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.Image2.SpecialEffect = fmSpecialEffectSunken


End Sub

Private Sub imgChangeCustomFolder_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.Image2.SpecialEffect = fmSpecialEffectFlat

End Sub


Private Sub lblVerticalOptionsLine_Click()

End Sub

Private Sub obSavetoCustomFolder_Click()
' option button, Save to custom folder

'setoption("SaveLocation", True)

If Me.lblCustomFolder = "N/A" Then
    If Not setCustomFolderOption(False) Then     ' function, prompts user for folder selection and returns selected folder path
        'MsgBox "Could not set SaveToCustomFolder option, please contact margama at 2942!", vbOKOnly, "Error setting custom folder option"
    End If
End If


End Sub


Function optionsFileExists() As Boolean

Dim username As String
username = sUserName

' safety measure, get userName from user if cannot determine it
If username = "" Then
    username = InputBox("Please supply your Windows user name", "Missing user name")
End If

Dim fo As New Scripting.FileSystemObject    ' filesystem object
Dim fd As Scripting.Folder                            ' folDer

Dim optionsFolderPath As String
optionsFolderPath = Replace(optionsBaseFolder, "#user#", username)

If fo.FolderExists(optionsFolderPath) Then
    Set fd = fo.GetFolder(Replace(optionsBaseFolder, "#user#", username))
Else
    
    If fo.FolderExists(Environ("Appdata") & "\Microsoft\Word") Then
        Set fd = fo.GetFolder(Environ("Appdata") & "\Microsoft\Word")
    Else
        ' Return error and leave ? What do?
        optionsFileExists = False
        Debug.Print "optionsFileExists failed to get options file base folder, using either standard string optionsBaseFolder and Environ(Appdata) global var!"
        Exit Function
    End If
    
    
End If

' now on to verify options file existence
Dim optionsFilePath As String
optionsFilePath = fd.Path & "\" & optionsFilename

If Not fo.FileExists(optionsFilePath) Then
    
    'MsgBox "File " & optionsFilePath & " does not exist !", vbOKOnly
    
    optionsFileExists = False
    Exit Function
Else
    optionsFileExists = True
End If


End Function


Function getOption(OptionName As String, Optional OptionValueIsAFolder As Boolean) As String
' Returns empty string if option not set


Dim username As String
username = sUserName

' safety measure, get userName from user if cannot determine it
If username = "" Then
    username = InputBox("Please supply your Windows user name", "Missing user name")
End If

Dim fo As New Scripting.FileSystemObject    ' filesystem object
Dim fd As Scripting.Folder                            ' folDer

Dim optionsFolderPath As String
optionsFolderPath = Replace(optionsBaseFolder, "#user#", username)

If fo.FolderExists(optionsFolderPath) Then
    Set fd = fo.GetFolder(optionsFolderPath)
Else
    
    If fo.FolderExists(Environ("Appdata") & "\Microsoft\Word") Then
        Set fd = fo.GetFolder(Environ("Appdata") & "\Microsoft\Word")
    Else
        ' Return error and leave ? What do?
        getOption = ""
        Debug.Print "getOption failed to get options file base folder, using either standard string optionsBaseFolder and Environ(Appdata) global var!"
        Exit Function
    End If
End If


' now on to verify options file existence
Dim optionsFilePath As String
optionsFilePath = fd.Path & "\" & optionsFilename

If Not fo.FileExists(optionsFilePath) Then
    
    'MsgBox "File " & optionsFilePath & " does not exist !", vbOKOnly
    
    getOption = ""
    Exit Function
End If

Dim storedOptionValue
storedOptionValue = System.PrivateProfileString(optionsFilePath, optionsFileSectionName, OptionName)



If storedOptionValue = "" Then      ' no such key or key empty
    
    ' MsgBox "Option file was empty or value was empty !", vbOKOnly
    getOption = ""
    
Else
    
    ' if user supplied the optional parameter, presumably True
    If Not IsMissing(OptionValueIsAFolder) Then
    
        If OptionValueIsAFolder Then
            If fo.FolderExists(storedOptionValue) Then
                'MsgBox "Option " & OptionName & " was already set as " & storedOptionValue, vbOKOnly
                
                getOption = storedOptionValue
                Exit Function
            Else
                'MsgBox "Option " & OptionName & " was incorrectly set as inexistent folder " & storedOptionValue, vbOKOnly
                getOption = ""
                Exit Function
            End If
        Else
            getOption = storedOptionValue
        End If
        
    Else    ' Folder non-existent, Could also jump to above, where we actually re-demand folder from user and set it
                
        getOption = storedOptionValue
        
    End If
    
End If


End Function


Function setOption(OptionNamesList As String, OptionValuesList As String) As Integer
' The two arguments, OptionsNamesList and OptionValuesList may contain one option and one corresponding value, both sype string, or
' may contain a list of options to be set, simply bundle option names (and values) together using "|" as separator, no spaces.

' Returning 0 for not setting ANY option from supplied ones,
'           1 for setting ALL of them to desired values
'          -1 for errors (file acces, folder not found)
'           2 for partial success.. no errors, but not all options set (unlikely)


' extract username for current user
Dim username As String
username = sUserName

' safety measure, get userName from user if cannot determine it
If username = "" Then
    username = InputBox("Please supply your Windows user name", "Missing user name")
End If


' set a reference to host folder for options file
Dim fo As New Scripting.FileSystemObject    ' filesystem object
Dim fd As Scripting.Folder                            ' folDer

Dim optionsFolderPath As String
optionsFolderPath = Replace(optionsBaseFolder, "#user#", username)

If fo.FolderExists(optionsFolderPath) Then
    Set fd = fo.GetFolder(Replace(optionsBaseFolder, "#user#", username))
Else

    If fo.FolderExists(Environ("Appdata") & "\Microsoft\Word") Then
        Set fd = fo.GetFolder(Environ("Appdata") & "\Microsoft\Word")
    Else
        ' Return error and leave ? What do?
        setOption = -1
        Debug.Print "setOption failed to get options file base folder, using either standard string optionsBaseFolder and Environ(Appdata) global var!"
        Exit Function
    End If
End If


' now on to verify options file existence and create if necessary
Dim optionsFilePath As String
optionsFilePath = fd.Path & "\" & optionsFilename
    
' create options file if not exist
If Not fo.FileExists(optionsFilePath) Then
    fo.CreateTextFile (optionsFilePath)
End If


Dim suppliedOptionsList As Variant

suppliedOptionsList = Split(OptionNamesList, "|")


Dim suppliedOptionsValuesList As Variant

suppliedOptionsValuesList = Split(OptionValuesList, "|")


' array to record results of attempts to set individual options
Dim results() As Integer     ' 0 for failure, 1 for success
'ReDim results(0)

Dim storedOptionValue As String

For i = 0 To UBound(suppliedOptionsList)

    ' get stored option's value from file
    storedOptionValue = System.PrivateProfileString(optionsFilePath, optionsFileSectionName, suppliedOptionsList(i))
    
    'if not already set, write the new value
    If Not storedOptionValue = suppliedOptionsValuesList(i) Then
        System.PrivateProfileString(optionsFilePath, optionsFileSectionName, suppliedOptionsList(i)) = suppliedOptionsValuesList(i)
    End If
    
    
    ' now we check newly written value so we can communicate success or failure through result
    If System.PrivateProfileString(optionsFilePath, optionsFileSectionName, suppliedOptionsList(i)) = suppliedOptionsValuesList(i) Then
        
        ReDim Preserve results(i)
        results(i) = 1
        
        'MsgBox "Selected folder, " & userSelectedFolder & " EXISTS and was set successfully !"
    Else
        
        ReDim Preserve results(i)
        results(i) = 0
        'MsgBox "Function setOption failed to set option " & suppliedOptionsList(i) & " to value " & suppliedOptionsValuesList(i) & _
            " into " & optionsFilePath, vbOKOnly + vbCritical, "Error, please debug!"
    End If

Next i


' now to check all results and return proper code
For i = 0 To UBound(results)
    If results(i) <> 1 Then
        setOption = 2
        Exit Function
    End If
Next i

' if we succeed to get to this line, above loop did not execute "Exit function". So SUCCESS.
setOption = 1

Set fo = Nothing
Set fd = Nothing
End Function


Function setCustomFolderOption(ForceSetting As Boolean) As Boolean
' using setOption function above, set a custom folder path acc to user pref and write it in userform
'(do not change it if already set unless ForceSetting is set to True)


' TRY writing it again using setOption & getOption functions, which write and read string into options file,
' and getOption even checks if the specified option value, if you say it's a folder, if that exists !
' I'M A LAZY BUM, that's for sure !

Dim username As String
username = sUserName

' safety measure, get userName from user if cannot determine it
If username = "" Then
    username = InputBox("Please supply your Windows user name", "Missing user name")
End If

Dim fo As New Scripting.FileSystemObject    ' filesystem object
Dim fd As Scripting.Folder                            ' folDer


Dim optionsFolderPath As String
optionsFolderPath = Replace(optionsBaseFolder, "#user#", username)

If fo.FolderExists(optionsFolderPath) Then
    Set fd = fo.GetFolder(optionsFolderPath)
Else
    If fo.FolderExists(Environ("Appdata") & "\Microsoft\Word") Then
        Set fd = fo.GetFolder(Environ("Appdata") & "\Microsoft\Word")
    Else
        ' Return error and leave ? What do?
        Debug.Print "setCustomFolderOption: Error, folder " & Replace(optionsBaseFolder, "#user#", username) & " NOT found!"
        setCustomFolderOption = False
        Exit Function
    End If
End If



' now on to verify options file existence and create if necessary
Dim optionsFilePath As String
optionsFilePath = fd.Path & "\" & optionsFilename
    
' create options file if not exist check file creation
If Not fo.FileExists(optionsFilePath) Then
    
    Dim optxts As TextStream
    
    optxts = fo.CreateTextFile(optionsFilePath)
    
    If optxts Is Nothing Then
        Debug.Print "setCustomFolderOption: Error, could not create file " & optionsFilePath & "!"
        setCustomFolderOption = False
        Exit Function
    End If
    
End If


' and continue by retrieving actual saved custom folder path from file, see if it's empty or not
storedOptionValue = System.PrivateProfileString(optionsFilePath, "DocSplitterOptions", "CustomFolderPath")
    

If storedOptionValue = "" Then      ' no such key or key empty
    
forceSettingHere:
    
    Dim userSelectedFolder As String
    userSelectedFolder = gscrolib.GetDirectory("Please select a folder to save your document parts in")
    
    If Not userSelectedFolder = "" Then     ' (User may simply click cancel.. that returns empty string !)
        
        ' now we are reasonably sure that settings file exists, along with its path. Write new SaveLocation method to options file
        System.PrivateProfileString(optionsFilePath, "DocSplitterOptions", "SaveToLocation") = "SaveToCustomFolder"
        
        Me.lblCustomFolder.Caption = userSelectedFolder
        'MsgBox "User selected folder " & userSelectedFolder
    
        System.PrivateProfileString(optionsFilePath, "DocSplitterOptions", "CustomFolderPath") = userSelectedFolder
    
        If System.PrivateProfileString(optionsFilePath, "DocSplitterOptions", "CustomFolderPath") <> "" And _
            fo.FolderExists(System.PrivateProfileString(optionsFilePath, "DocSplitterOptions", "CustomFolderPath")) Then
            
            setCustomFolderOption = True
            
            'MsgBox "Selected folder, " & userSelectedFolder & " EXISTS and was set successfully !"
            
        End If
    Else
        
        If fo.FolderExists(storedOptionValue) Then
            'MsgBox "Option " & OptionName & " was already set as " & storedOptionValue, vbOKOnly
            setCustomFolderOption = True
            Exit Function
        
        Else
        
            MsgBox "A Custom folder path was NOT successfully retrieved from options file and you did not select one!" & vbCr & vbCr & _
                "Please click OK to select default saving location", vbOKOnly + vbInformation, "NO custom folder to set!"
        
        
            ' now we are reasonably sure that settings file exists, along with its path. Write new SaveLocation method to options file
            System.PrivateProfileString(optionsFilePath, "DocSplitterOptions", "SaveToLocation") = "SaveToDocumentsFolder"
            Me.obSavetoDocsFolder.Value = True
        
            setCustomFolderOption = False
            Exit Function
        
        End If
        
    End If
        
Else    ' WE DID retrieve a stored custom folder path from file, let's see if user wants us to change it or not
    
    
    If ForceSetting Then
        GoTo forceSettingHere
    Else
    
        If fo.FolderExists(storedOptionValue) Then
            'MsgBox "Option " & OptionName & " was already set as " & storedOptionValue, vbOKOnly
            setCustomFolderOption = True
            Exit Function
        Else    ' user does not wants us to force changing folder path, we retrieved a non-empty path string, such folder DOES NOT exist though !!
                                            
            ' Folder non-existent, we jump to above, where we actually re-demand folder from user and set it
            GoTo forceSettingHere
            'MsgBox "Option " & OptionName & " was incorrectly set as inexistent folder " & storedOptionValue, vbOKOnly
            'Debug.Print "setCustomFolderOption: Error, folder " & storedOptionValue & " was found in options file, but folder DOES NOT exist !"
            'setCustomFolderOption = False
            'Exit Function
        End If
    End If
    
End If
        
    
Set fo = Nothing
Set fd = Nothing
End Function


Sub Update_Userform_fromOptionsFile()
' to be run on userform initialize, perhaps even from another event, to set user options
' in accordance with options file

' Need to decide if it's going to create options file if it does not exist or not...

' read eventually stored value for IdealDocPartSize and update form
Dim storedIdealDocPartSize As String
storedIdealDocPartSize = getOption("IdealDocPartSize")
'
If Not storedIdealDocPartSize = "" Then
    Me.txtIdealDocPart.Text = storedIdealDocPartSize
End If

' read eventually stored value for CreateFolderForParts and update form
Dim storedCreateFolderForDocParts As String
storedCreateFolderForDocParts = getOption("CreateFolderForDocParts")
'
If Not storedCreateFolderForDocParts = "" Then
    If storedCreateFolderForDocParts = "True" Then
        Me.cbCreateFolderForParts.Value = True
    End If
End If

Dim storedSaveToLocation As String
storedSaveToLocation = getOption("SaveToLocation")
'
If Not storedSaveToLocation = "" Then
    Select Case storedSaveToLocation
        
        Case "SaveToUserTFolder"
            Me.obSavetoUserFolder.Value = 1
        Case "SaveToWordDocumentsFolder"
            Me.obSavetoDocsFolder = 1
        Case "SaveToCurrentDocFolder"
            Me.obSavetoCurrentFolder = 1
        Case "SaveToCustomFolder"
            Me.obSavetoCustomFolder = 1
            
    End Select
End If


End Sub


Sub setMaxPrefferredDocPart()

' Let's retrieve and set the ideal doc part size, acc to user prefs
Dim savedMaxPart As String
savedMaxPart = getOption("IdealDocPartSize")
' and fallback, if not stored, we get value in user form, options part, default 30
If savedMaxPart = "" Then
    If IsNumeric(Me.txtIdealDocPart.Value) Then
        If CInt(Me.txtIdealDocPart.Value) > 0 Then
            savedMaxPart = Me.txtIdealDocPart.Value
        Else
            savedMaxPart = "30"
        End If
    Else
        savedMaxPart = "30"
    End If
End If

prefferedMaxDocPart = CInt(savedMaxPart)

End Sub


Private Sub obSavetoDocsFolder_Click()

Dim registryDocsPathSetting As String
registryDocsPathSetting = System.PrivateProfileString("", "HKEY_CURRENT_USER\Software\Microsoft\Office\14.0\Word\Options", "doc-path")

If Dir(registryDocsPathSetting, vbDirectory) = "" Then
    MsgBox "Directory " & registryDocsPathSetting & " NOT FOUND!"
End If

End Sub


Private Sub obSaveToProjectFolder_Click()


Dim ctDocBaseName As String
ctDocBaseName = getGSCBaseName(ActiveDocument.Name)

Dim TargetFolder As String
TargetFolder = "T:\Prepared originals\" & ctDocBaseName

If Dir(TargetFolder, vbDirectory) = "" Then
    
    obSavetoDocsFolder.Value = True
    
    MsgBox "Folder " & TargetFolder & " DOESN'T EXIST !" & vbCr & vbCr & _
        "Please click OK to select default Save Location", vbOKOnly + vbInformation, "No such folder"
    
End If


End Sub


Private Sub obSavetoUserFolder_Click()


Dim username As String

username = sUserName

Dim TargetFolder As String

TargetFolder = tHomeFolderBase & "\" & username    ' that would be T:\Docs\margama

If Dir(TargetFolder, vbDirectory) = "" Then
    
    MsgBox "Folder " & TargetFolder & " DOESN'T EXIST !" & vbCr & vbCr & _
        "Please click OK to select default Save Location", vbOKOnly + vbInformation, "No such folder"
    
    obSavetoDocsFolder.Value = True
    
End If

End Sub


Sub UpdateLblStatistics(WhichPage As Integer)
' Given a current page (pgAuto or pgCustom), will update the label containing document name, pages number, parts page numbers.
' Also needs to be called to dynamically do the same when modifying textboxes on pgCustom

' not working without opened document
If Documents.count > 0 Then

    If WhichPage = 0 Then       ' Auto proposal page
                
        ' and now, for same lblStatistics, also calculate number of pages of each proposed document parts
        Dim pagesNum As String
        
        ' Define an array and populate it with border pages for proposed splitting
        Dim splitpages As Variant
        If getOption("UseCharacterCount") = "True" Then
            splitpages = Calculate_AutosplitPages_ByCharacter
        Else
            splitpages = CalculateAutosplitPages
        End If
        
        ' Put number of parts into lblMainMessage's caption
        Me.lblMainMessage.Caption = Replace(lblMainMessage.Caption, "##", UBound(splitpages) + 1)
        
        ' And build a string with vertical bar separator for user presentation
        Dim splitPagesString As String
        
        For i = 0 To UBound(splitpages)
            splitPagesString = IIf(i < UBound(splitpages), splitPagesString & splitpages(i) & " | ", splitPagesString & splitpages(i))
        Next i
        ' finally put it into lblPages's caption
        Me.lblPages.Caption = splitPagesString
        
        ' To build it by populating it with elements..
        Dim lblStatString As String
        
        ' Also populate lblStatistics with document name
        lblStatString = Replace(partsNumTemplate, "%document%", ActiveDocument.Name)
        'Me.lblStatistics.Caption = Replace(partsNumTemplate, "%document%", ActiveDocument.Name)
        ' and number of pages in document
        'Me.lblStatistics.Caption = Replace(partsNumTemplate, "##", ctDocNumPages)
        lblStatString = Replace(lblStatString, "##", ctDocNumPages)
                
        For i = 0 To UBound(splitpages)
            If i < UBound(splitpages) Then
                pagesNum = pagesNum & splitpages(i + 1) - splitpages(i) & " | "
            Else
                pagesNum = pagesNum & (ctDocNumPages - (splitpages(i) - 1))
            End If
        Next i
        
        autoPartsNum = Replace(lblStatString, "#|#", pagesNum)
        Me.lblStatistics.Caption = autoPartsNum
        
    Else    ' Custom Split page
                
        splitpages = GetCustomSplitPages
        lblStatString = Replace(partsNumTemplate, "%document%", ActiveDocument.Name)
        
        Dim ctSNP As Integer
        ' lets calculate how many pages the array we got from user indicates in total. Maybe he keft some out
        ctSNP = Calculate_TotalPages_OfArray(splitpages)
        ' if he did, we signal we know it
        If ctSNP <> ctDocNumPages Then
            lblStatString = Replace(lblStatString, "##", ctSNP)
        Else
            lblStatString = Replace(lblStatString, "##", ctDocNumPages)
        End If
        
        If Not IsNull(splitpages) Then
            For i = 0 To UBound(splitpages, 2)
                pagesNum = pagesNum & splitpages(1, i) - IIf(splitpages(0, i) > 0, splitpages(0, i) - 1, 0) & IIf(i < UBound(splitpages, 2), " | ", "")
            Next i
            
            custPartsNum = Replace(lblStatString, "#|#", pagesNum)
            Me.lblStatistics.Caption = custPartsNum
        End If
        
    End If

End If

End Sub


Function Calculate_TotalPages_OfArray(PagesRangeArray) As Integer

If Not IsArray(PagesRangeArray) Then
    Calculate_TotalPages_OfArray = -1   ' error, supplied parameter is not array
    Exit Function
End If

If UBound(PagesRangeArray) <> 1 Then    ' the first level ubound has to be 1, meaning a we use a bidimensional array
    Calculate_TotalPages_OfArray = -1   ' error, supplied parameter is not array
    Exit Function
End If

Dim tmpR As Integer 'temp result

For i = 0 To UBound(PagesRangeArray, 2)
    tmpR = tmpR + CInt(PagesRangeArray(1, i)) - (CInt(PagesRangeArray(0, i)) - 1)
Next i

Calculate_TotalPages_OfArray = tmpR

End Function


Sub populatePgCustom(beginningPageRange_Array)
' Populate all textboxes txtStart1, txtEnd1 and so on, so as to
' also have the automatically proposed ranges border pages into pgCustom, when you switch to it


If Not IsArray(beginningPageRange_Array) Then
    MsgBox beginningPageRange_Array & " is NOT an array", vbOKOnly, "Error, please fix!"
    Exit Sub
Else
    Dim bp As Variant
    bp = beginningPageRange_Array
End If

Dim howManyParts As Integer
howManyParts = UBound(beginningPageRange_Array) + 1     ' array is indexed starting at 0

' It's fine for auto splitting module, but NOT allowed in the custom page, cause... we don't have more than 10 sets of buttons available !
If howManyParts > 10 Then
    howManyParts = 10
    Dim forceAutoEqualise_inCustomSplit As Boolean
    forceAutoEqualise_inCustomSplit = True
End If

Call makeVisible_whichRow(howManyParts)

Dim tmpControl As control
Dim tmptextbox As TextBox

If getControlNamed("txtEnd" & howManyParts).Visible = True Then
    
    For i = 1 To howManyParts
        
        ' write each parts' starting page first
        Set tmpControl = getControlNamed("txtStart" & i)
        Set tmptextbox = tmpControl
        
        tmptextbox.Text = Format(bp(i - 1), "000")
        
        ' and then each parts' last page, taking care of special case for last part
        Set tmpControl = getControlNamed("txtEnd" & i)
        Set tmptextbox = tmpControl
        
        If i < howManyParts Then
            tmptextbox.Text = Format(bp(i) - 1, "000")
        Else
            ' is it going to work to allow already user-changed custom ending of last part?
            If tmptextbox.Text = "" Then
                tmptextbox.Text = Format(ctDocNumPages, "000")
            End If
        End If
        
    Next i

' If we have a raised flag, autoEqualise
If forceAutoEqualise_inCustomSplit Then
    ' Force equalise parts, since we know
    Call cmdEqualize_Click
End If

End If

End Sub


Private Sub cbCancel_Click()
' Cancel button

Unload Me

End Sub


Sub makeVisible_whichRow(whichRow)


Dim wp As Integer
wp = whichRow

Dim lastVisible As Integer

lastVisible = getLastVisibleRow

If lastVisible = wp Then
    Exit Sub
Else
    ' what if current rules require us to split into more than 10 parts?
    ' Obviously, descend to 10 parts...
'    If wp > 10 Then
'        wp = 10
'    End If
End If


If lastVisible = 10 Then     ' All option buttons are visible
    MsgBox "Maximum number of document parts reached!" & vbCr & vbCr & _
        "Please ask for more from margama, 2942 !", vbOKOnly + vbCritical, "No more parts!"
    Exit Sub
ElseIf lastVisible = 9 Then
    
    makeEverythingBigger
    
    makeVisible_WhichPart (lastVisible + 1)
    
    Me.cmdMore.ControlTipText = "Maximum number of document parts reached!"
    Me.cmdMore.Enabled = False
    
    Exit Sub
ElseIf lastVisible >= 3 Then
    
    For j = lastVisible + 1 To wp
    
        makeEverythingBigger
        makeVisible_WhichPart (j)
        'wp = wp + 1
        
    Next j
    
    Exit Sub
Else
    For k = lastVisible + 1 To 3
        makeVisible_WhichPart (k)
    Next k
    
    For l = 4 To wp
        makeEverythingBigger
        makeVisible_WhichPart (l)
    Next l
        
    Exit Sub
End If


End Sub


Private Sub cmdMore_Click()
' Create a new set of start, end and dash textboxes and labels.
' Add one more document part to split into, that is

Dim lastVisible As Integer

lastVisible = getLastVisibleRow


If lastVisible = 10 Then     ' All option buttons are visible
    
    MsgBox "Maximum number of document parts reached!" & vbCr & vbCr & _
        "Please ask for more from margama, 2942 !", vbOKOnly + vbCritical, "No more parts!"
    Exit Sub
    
ElseIf lastVisible = 9 Then
    
    makeEverythingBigger
    
    makeVisible_WhichPart (lastVisible + 1)
    
    Me.cmdMore.ControlTipText = "Maximum number of document parts reached!"
    Me.cmdMore.Enabled = False
    
    Exit Sub
    
ElseIf lastVisible >= 3 Then
    
    makeEverythingBigger
    
    makeVisible_WhichPart (lastVisible + 1)
    Exit Sub
    
Else

    makeVisible_WhichPart (lastVisible + 1)
    Exit Sub
    
End If


End Sub


Sub makeEverythingBigger()

    Me.Height = Me.Height + 24
    
    Me.cbCancel.Top = Me.cbCancel.Top + 24
    
    If Me.Height < 196 Then
        Me.Image1.Top = Me.cbCancel.Top - 32
    Else
        Me.Image1.Top = Me.cbCancel.Top - 34
    End If
    
    Me.lblStatistics.Top = Me.lblStatistics.Top + 24
    Me.MultiPage1.Height = Me.MultiPage1.Height + 24
    Me.frmPageBreakdown.Height = Me.frmPageBreakdown.Height + 24
    Me.cmdEqualize.Top = Me.cmdEqualize.Top + 24
    Me.cmdClearAll.Top = Me.cmdClearAll.Top + 24
    
    Me.lblHLine.Top = Me.lblHLine.Top + 24
    Me.lblCtPage.Top = Me.lblCtPage.Top + 24
    Me.cbPrev.Top = Me.cbPrev.Top + 24
    Me.cbNext.Top = Me.cbNext.Top + 24
    Me.cbModify.Top = Me.cbModify.Top + 24
    Me.cbDeleteAllBk.Top = Me.cbDeleteAllBk.Top + 24
    
    Me.frmOptions.Height = Me.Height - 30

End Sub


Sub makeEverythingSmaller()

    Me.Height = Me.Height - 24
    
    Me.cbCancel.Top = Me.cbCancel.Top - 24
    
    If Me.Height < 196 Then
        Me.Image1.Top = Me.cbCancel.Top - 32
    Else
        Me.Image1.Top = Me.cbCancel.Top - 34
    End If
    
    Me.lblStatistics.Top = Me.lblStatistics.Top - 24
    Me.MultiPage1.Height = Me.MultiPage1.Height - 24
    Me.frmPageBreakdown.Height = Me.frmPageBreakdown.Height - 24
    Me.cmdEqualize.Top = Me.cmdEqualize.Top - 24
    Me.cmdClearAll.Top = Me.cmdClearAll.Top - 24
    
    Me.lblHLine.Top = Me.lblHLine.Top - 24
    Me.lblCtPage.Top = Me.lblCtPage.Top - 24
    Me.cbPrev.Top = Me.cbPrev.Top - 24
    Me.cbNext.Top = Me.cbNext.Top - 24
    Me.cbModify.Top = Me.cbModify.Top - 24
    Me.cbDeleteAllBk.Top = Me.cbDeleteAllBk.Top - 24

    
    Me.frmOptions.Height = Me.Height - 30

End Sub

Function getLastVisibleRow() As Integer
' function to return last visible row of textboxes,
' to avoid having to make sure the option button which is selected is
' always the last.

Dim c As control

For i = 1 To 9
    
    Set c = getControlNamed("txtStart" & i + 1)
    
    If c.Visible = False Then
        getLastVisibleRow = i
        Exit Function
    End If
    
Next i

getLastVisibleRow = 10


End Function


Sub makeVisible_WhichPart(PartNumber As Integer)


Dim tmpControl As control

Set tmpControl = getControlNamed("obPart" & PartNumber)
tmpControl.Visible = True

Dim ctOptionButton As OptionButton
Set ctOptionButton = tmpControl

ctOptionButton.Value = 1


Set tmpControl = getControlNamed("txtStart" & PartNumber)
tmpControl.Visible = True

Set tmpControl = getControlNamed("lblDash" & PartNumber)
tmpControl.Visible = True

Set tmpControl = getControlNamed("txtEnd" & PartNumber)
tmpControl.Visible = True


On Error Resume Next
tmpControl.SetFocus
If Err.Number = 2110 Then Err.Clear ' could not set focus

Dim tmptextbox As TextBox


Set tmptextbox = tmpControl
tmptextbox.SelStart = 0
tmptextbox.SelLength = Len(tmptextbox.Text)


End Sub

Function getLastRowsPosition() As controlPosition
' iterate over all textbox controls in preset prenamed Frame control
' and get position of last of em (higher named)

Dim c As control

Dim maxFound As Integer
maxFound = 1    ' there's no textbox named 0...


For Each c In Me.frmPageBreakdown.Controls
    If Left$(c.Name, 8) = "txtStart" Then
        If Extr_Numbers(c.Name)(0) > maxFound Then
            maxFound = Extr_Numbers(c.Name)(0)
            
        End If
    End If
Next c

Set c = getControlNamed("txtStart" & maxFound)

getLastRowsPosition.Left = c.Left
getLastRowsPosition.Top = c.Top

End Function


Private Sub MultiPage1_Change()
' Change Event handler for multipage1, activates when changin pages,
' to update stats label with doc name, pages number, parts page numbers

    ' if we've switched to Custom page, select ending page of first part
    ' and update stats label
    If MultiPage1.selectedItem.Name = "pgCustom" Then

        Me.txtEnd1.SetFocus    ' pgCustom page (tab) is not visible, cannot set focus to a control on it
        Me.txtEnd1.SelStart = 0
        Me.txtEnd1.SelLength = Len(Me.txtEnd1.Text)

        If Me.cbNext.Enabled = True Then
            Call cbInteractive_Click    ' deactivate the interactive mode if switching pages (tabs)
        End If

        Call UpdateLblStatistics(1)

    Else    ' no textboxes to select here - we in automatics land ! :D

        If Me.cbNext.Enabled = True Then
            Call cbInteractive_Click    ' deactivate the interactive mode if switching pages (tabs)
        End If

        Call UpdateLblStatistics(0)
    End If
    
    ' Upon switching from auto cu custom tabs, delete all custom bookmarks, user wants to start over
    'Call Delete_AllBookmarksNamed("DS_*")

End Sub


' option buttons "obPartx" click event handlers
'
' TO shrink custom parts number to user-clicked part.
' Can decide to shrink multiple units this way, from... 9 to... 6 parts, no clicking 3 times !
'

Private Sub obPart1_Click()
    
    Dim lastVisible As Integer
    Dim ctRowIndex As Integer
    
    ctRowIndex = 1
    lastVisible = getLastVisibleRow
    
    ' shrink form multiple times, acc to what were the visible parts numbers and user's selection
    For i = 1 To lastVisible - 3    ' Why 3? Well, by design, we have reserved place for 3 rows of textboxes
        If lastVisible > ctRowIndex Then
            makeEverythingSmaller
        End If
    Next i

    If txtStart1.Value <> "" Then txtEnd1.Value = Format(ctDocNumPages, "000")

    Call optionButtons_DisplayHandle(obPart1)
    
    ' if cmdMore button was disabled, reenable it. User disabled it when reaching 10 parts
    If cmdMore.Enabled = False Then cmdMore.Enabled = True

End Sub


Private Sub obPart2_Click()
    
    Dim lastVisible As Integer
    Dim ctRowIndex As Integer
    
    ctRowIndex = 2
    lastVisible = getLastVisibleRow
    
    ' shrink form multiple times, acc to what were the visible parts numbers and user's selection
    For i = 1 To lastVisible - 3
        If lastVisible > ctRowIndex Then
            makeEverythingSmaller
        End If
    Next i
    
    If txtStart2.Value <> "" Then txtEnd2.Value = Format(ctDocNumPages, "000")
    
    Call optionButtons_DisplayHandle(obPart2)
    
    Call UpdateLblStatistics(1)
    ' if cmdMore button was disabled, reenable it. User disabled it when reaching 10 parts
    If cmdMore.Enabled = False Then cmdMore.Enabled = True

End Sub


Private Sub obPart3_Click()
    
    Dim lastVisible As Integer
    Dim ctRowIndex As Integer
    
    ctRowIndex = 3
    lastVisible = getLastVisibleRow
    
    ' shrink form multiple times, acc to what were the visible parts numbers and user's selection
    For i = 1 To lastVisible - ctRowIndex
        If lastVisible > ctRowIndex Then makeEverythingSmaller
    Next i
    
    ' Populate end of document
    If txtStart3.Value <> "" Then txtEnd3.Value = Format(ctDocNumPages, "000")
    
    Call optionButtons_DisplayHandle(obPart3)
    
    Call UpdateLblStatistics(1)
    ' Re-enable cmdMore button, just in case.
    If cmdMore.Enabled = False Then cmdMore.Enabled = True
    
End Sub


Private Sub obPart4_Click()
    
    Dim lastVisible As Integer
    Dim ctRowIndex As Integer
    
    ctRowIndex = 4
    lastVisible = getLastVisibleRow
    
    For i = 1 To lastVisible - ctRowIndex
        If lastVisible > ctRowIndex Then
            makeEverythingSmaller
        End If
    Next i

    If txtStart4.Value <> "" Then txtEnd4.Value = Format(ctDocNumPages, "000")

    Call optionButtons_DisplayHandle(obPart4)
    
    Call UpdateLblStatistics(1)
    
    If cmdMore.Enabled = False Then cmdMore.Enabled = True

End Sub


Private Sub obPart5_Click()

    Dim lastVisible As Integer
    Dim ctRowIndex As Integer
    
    ctRowIndex = 5
    lastVisible = getLastVisibleRow
    
    For i = 1 To lastVisible - ctRowIndex
        If lastVisible > ctRowIndex Then
            makeEverythingSmaller
        End If
    Next i
    
    If txtStart5.Value <> "" Then txtEnd5.Value = Format(ctDocNumPages, "000")

    Call optionButtons_DisplayHandle(obPart5)
    
    Call UpdateLblStatistics(1)
    
    If cmdMore.Enabled = False Then cmdMore.Enabled = True

End Sub

Private Sub obPart6_Click()

    Dim lastVisible As Integer
    Dim ctRowIndex As Integer
    
    ctRowIndex = 6
    lastVisible = getLastVisibleRow
    
    For i = 1 To lastVisible - ctRowIndex
        If lastVisible > ctRowIndex Then
            makeEverythingSmaller
        End If
    Next i

    If txtStart6.Value <> "" Then txtEnd6.Value = Format(ctDocNumPages, "000")

    Call optionButtons_DisplayHandle(obPart6)
    
    Call UpdateLblStatistics(1)
    
    If cmdMore.Enabled = False Then cmdMore.Enabled = True

End Sub

Private Sub obPart7_Click()

    Dim lastVisible As Integer
    Dim ctRowIndex As Integer
    
    ctRowIndex = 7
    lastVisible = getLastVisibleRow
    
    For i = 1 To lastVisible - ctRowIndex
        If lastVisible > ctRowIndex Then
            makeEverythingSmaller
        End If
    Next i

    If txtStart7.Value <> "" Then txtEnd7.Value = Format(ctDocNumPages, "000")

    Call optionButtons_DisplayHandle(obPart7)
    
    Call UpdateLblStatistics(1)
    
    If cmdMore.Enabled = False Then cmdMore.Enabled = True

End Sub

Private Sub obPart8_Click()

    Dim lastVisible As Integer
    Dim ctRowIndex As Integer
    
    ctRowIndex = 8
    lastVisible = getLastVisibleRow
    
    For i = 1 To lastVisible - ctRowIndex
        If lastVisible > ctRowIndex Then
            makeEverythingSmaller
        End If
    Next i

    If txtStart8.Value <> "" Then txtEnd8.Value = Format(ctDocNumPages, "000")

    Call optionButtons_DisplayHandle(obPart8)
    
    Call UpdateLblStatistics(1)
    
    If cmdMore.Enabled = False Then cmdMore.Enabled = True

End Sub

Private Sub obPart9_Click()

    Dim lastVisible As Integer
    Dim ctRowIndex As Integer
    
    ctRowIndex = 9
    lastVisible = getLastVisibleRow
    
    For i = 1 To lastVisible - ctRowIndex
        If lastVisible > ctRowIndex Then makeEverythingSmaller
    Next i

    If txtStart9.Value <> "" Then txtEnd9.Value = Format(ctDocNumPages, "000")

    Call optionButtons_DisplayHandle(obPart9)
    
    Call UpdateLblStatistics(1)
    
    If cmdMore.Enabled = False Then cmdMore.Enabled = True

End Sub

' END OF options buttons click event handlers



' The whole sheboyng down here are the event handlers charged with calculating the beginning of the next part and formatting it
' and putting it into the proper textbox. Also put the max page into the ending textbox

Private Sub txtStart1_Exit(ByVal Cancel As MSForms.ReturnBoolean)
    
    txtStart1.Text = Format(txtStart1.Text, "000")
    Call UpdateLblStatistics(1)
    
End Sub

Private Sub txtStart2_Exit(ByVal Cancel As MSForms.ReturnBoolean)

    txtStart2.Text = Format(txtStart2.Text, "000")
    Call UpdateLblStatistics(1)

End Sub

Private Sub txtStart3_Exit(ByVal Cancel As MSForms.ReturnBoolean)

    txtStart3.Text = Format(txtStart3.Text, "000")
    Call UpdateLblStatistics(1)

End Sub

Private Sub txtStart4_Exit(ByVal Cancel As MSForms.ReturnBoolean)

    txtStart4.Text = Format(txtStart4.Text, "000")
    Call UpdateLblStatistics(1)

End Sub

Private Sub txtStart5_Exit(ByVal Cancel As MSForms.ReturnBoolean)

    txtStart5.Text = Format(txtStart5.Text, "000")
    Call UpdateLblStatistics(1)

End Sub

Private Sub txtStart6_Exit(ByVal Cancel As MSForms.ReturnBoolean)

    txtStart6.Text = Format(txtStart6.Text, "000")
    Call UpdateLblStatistics(1)

End Sub

Private Sub txtStart7_Exit(ByVal Cancel As MSForms.ReturnBoolean)

    txtStart7.Text = Format(txtStart7.Text, "000")
    Call UpdateLblStatistics(1)

End Sub

Private Sub txtStart8_Exit(ByVal Cancel As MSForms.ReturnBoolean)

    txtStart8.Text = Format(txtStart8.Text, "000")
    Call UpdateLblStatistics(1)

End Sub

Private Sub txtStart9_Exit(ByVal Cancel As MSForms.ReturnBoolean)

    txtStart9.Text = Format(txtStart9.Text, "000")
    Call UpdateLblStatistics(1)

End Sub

Private Sub txtStart10_Exit(ByVal Cancel As MSForms.ReturnBoolean)

    txtStart10.Text = Format(txtStart10.Text, "000")
    Call UpdateLblStatistics(1)

End Sub

Private Sub txtEnd1_KeyUp(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

On Error GoTo ErrKeyUp

    If Not txtEnd1.Text = "" Then
        If CInt(txtEnd1.Text) < ctDocNumPages Then
            strEnd1 = Format(CLng(txtEnd1.Text) + 1, "000")
            'If txtStart2.Visible Then txtStart2.Text = strEnd1
            txtStart2.Text = strEnd1
        End If
    Else
        txtStart2.Text = ""
    End If
    
    If txtEnd3.Visible = False And txtStart2 <> "" Then
        If txtEnd2.Text = "" Then
            txtEnd2.Text = Format(ctDocNumPages, "000")
        End If
    End If

    Exit Sub
    
ErrKeyUp:
    If Err.Number <> 0 Then
        Err.Clear
        Exit Sub
    End If


End Sub


Private Sub txtEnd1_Exit(ByVal Cancel As MSForms.ReturnBoolean)
    
    txtEnd1.Text = Format(txtEnd1.Text, "000")
    Call UpdateLblStatistics(1)
    
End Sub


Private Sub txtEnd2_KeyUp(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    
On Error GoTo ErrKeyUp
    
    If Not txtEnd2.Text = "" Then
        If CInt(txtEnd2.Text) < ctDocNumPages Then
            strEnd2 = Format(CLng(txtEnd2.Text) + 1, "000")
            txtStart3.Text = strEnd2
        End If
    Else
        txtStart3.Text = ""
    
    End If
    
    If txtEnd4.Visible = False And txtStart3.Text <> "" Then
        If txtEnd3.Text = "" Then txtEnd3.Text = Format(ctDocNumPages, "000")
    End If

    Exit Sub
    
ErrKeyUp:
    If Err.Number <> 0 Then
        Err.Clear
        Exit Sub
    End If

End Sub

Private Sub txtEnd2_Exit(ByVal Cancel As MSForms.ReturnBoolean)
    txtEnd2.Text = Format(txtEnd2, "000")
    Call UpdateLblStatistics(1)
End Sub

Private Sub txtEnd3_KeyUp(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    
On Error GoTo ErrKeyUp
    
    If Not txtEnd3.Text = "" Then
        If CInt(txtEnd3.Text) < ctDocNumPages Then
            strEnd3 = Format(CLng(txtEnd3.Text) + 1, "000")
            txtStart4.Text = strEnd3
        End If
    Else
        txtStart4.Text = ""
    End If
    
    If txtEnd5.Visible = False And txtStart4.Text <> "" Then
        If txtEnd4.Text = "" Then txtEnd4.Text = Format(ctDocNumPages, "000")
    End If
    
    Exit Sub
    
ErrKeyUp:
    If Err.Number <> 0 Then
        Err.Clear
        Exit Sub
    End If

End Sub

Private Sub txtEnd3_Exit(ByVal Cancel As MSForms.ReturnBoolean)
    txtEnd3.Text = Format(txtEnd3, "000")
    Call UpdateLblStatistics(1)
End Sub

Private Sub txtEnd4_KeyUp(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    
On Error GoTo ErrKeyUp
    
    If Not txtEnd4.Text = "" Then
        If CInt(txtEnd4.Text) < ctDocNumPages Then
            strEnd4 = Format(CLng(txtEnd4.Text) + 1, "000")
            txtStart5.Text = strEnd4
        End If
    Else
        txtStart5.Text = ""
    End If
    
    If txtEnd6.Visible = False And txtStart5 <> "" Then
        If txtEnd5.Text = "" Then txtEnd5.Text = Format(ctDocNumPages, "000")
    End If
    
    Exit Sub
    
ErrKeyUp:
    If Err.Number <> 0 Then
        Err.Clear
        Exit Sub
    End If

End Sub

Private Sub txtEnd4_Exit(ByVal Cancel As MSForms.ReturnBoolean)
    txtEnd4.Text = Format(txtEnd4, "000")
    Call UpdateLblStatistics(1)
End Sub

Private Sub txtEnd5_KeyUp(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

On Error GoTo ErrKeyUp

    If Not txtEnd5.Text = "" Then
        If CInt(txtEnd5.Text) < ctDocNumPages Then
            strEnd5 = Format(CLng(txtEnd5.Text) + 1, "000")
            txtStart6.Text = strEnd5
        End If
    Else
        txtStart6.Text = ""
    End If
    
    If txtEnd7.Visible = False And txtStart6.Text <> "" Then
        If txtEnd6.Text = "" Then txtEnd6.Text = Format(ctDocNumPages, "000")
    End If
    
    Exit Sub
    
ErrKeyUp:
    If Err.Number <> 0 Then
        Err.Clear
        Exit Sub
    End If

    Exit Sub
    
End Sub

Private Sub txtEnd5_Exit(ByVal Cancel As MSForms.ReturnBoolean)
    txtEnd5.Text = Format(txtEnd5, "000")
    Call UpdateLblStatistics(1)
End Sub

Private Sub txtEnd6_KeyUp(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

On Error GoTo ErrKeyUp

    If Not txtEnd6.Text = "" Then
        If CInt(txtEnd6.Text) < ctDocNumPages Then
            strEnd6 = Format(CLng(txtEnd6.Text) + 1, "000")
            txtStart7.Text = strEnd6
        End If
    Else
        txtStart7.Text = ""
    End If
    
    If txtEnd8.Visible = False And txtStart7.Text <> "" Then
        If txtEnd7.Text = "" Then txtEnd7.Text = Format(ctDocNumPages, "000")
    End If
    
    Exit Sub
    
ErrKeyUp:
    If Err.Number <> 0 Then
        Err.Clear
        Exit Sub
    End If

End Sub

Private Sub txtEnd6_Exit(ByVal Cancel As MSForms.ReturnBoolean)
    txtEnd6.Text = Format(txtEnd6, "000")
    Call UpdateLblStatistics(1)
End Sub

Private Sub txtEnd7_KeyUp(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

On Error GoTo ErrKeyUp

    If Not txtEnd7.Text = "" Then
        If CInt(txtEnd7.Text) < ctDocNumPages Then
            strEnd7 = Format(CLng(txtEnd7.Text) + 1, "000")
            txtStart8.Text = strEnd7
        End If
    Else
        txtStart8.Text = ""
    End If
    
    If txtEnd9.Visible = False And txtStart8.Text <> "" Then
        If txtEnd8.Text = "" Then txtEnd8.Text = Format(ctDocNumPages, "000")
    End If
    
    Exit Sub
    
ErrKeyUp:
    If Err.Number <> 0 Then
        Err.Clear
        Exit Sub
    End If

End Sub

Private Sub txtEnd7_Exit(ByVal Cancel As MSForms.ReturnBoolean)
    txtEnd7.Text = Format(txtEnd7, "000")
    Call UpdateLblStatistics(1)
End Sub

Private Sub txtEnd8_KeyUp(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    
On Error GoTo ErrKeyUp
    
    If Not txtEnd8.Text = "" Then
        If CInt(txtEnd8.Text) < ctDocNumPages Then
            strEnd8 = Format(CLng(txtEnd8.Text) + 1, "000")
            txtStart9.Text = strEnd8
        End If
    Else
        txtStart9.Text = ""
    End If
    
    If txtEnd10.Visible = False And txtStart9.Text <> "" Then
        If txtEnd9.Text = "" Then txtEnd9.Text = Format(ctDocNumPages, "000")
    End If
    
    Exit Sub
    
ErrKeyUp:
    If Err.Number <> 0 Then
        Err.Clear
        Exit Sub
    End If

End Sub

Private Sub txtEnd8_Exit(ByVal Cancel As MSForms.ReturnBoolean)
    txtEnd8.Text = Format(txtEnd8, "000")
    Call UpdateLblStatistics(1)
End Sub

Private Sub txtEnd9_KeyUp(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    
On Error GoTo ErrKeyUp
    
    If Not txtEnd9.Text = "" Then
        If CInt(txtEnd9.Text) < ctDocNumPages Then
            strEnd9 = Format(CLng(txtEnd9.Text) + 1, "000")
            txtStart10.Text = strEnd9
        End If
    Else
        txtStart10.Text = ""
    End If
     
    txtEnd10.Text = Format(ctDocNumPages, "000")
    
    Exit Sub
    
ErrKeyUp:
    If Err.Number <> 0 Then
        Err.Clear
        Exit Sub
    End If

End Sub

Private Sub txtEnd9_Exit(ByVal Cancel As MSForms.ReturnBoolean)
    txtEnd9.Text = Format(txtEnd9, "000")
    Call UpdateLblStatistics(1)
End Sub
'___________________________________________________________________________________________________________________
'
' END of text boxes key up and exit event handlers
'____________________________________________________________________________________________________________________
'
'


Function GetCustomSplitPages() As Variant
' simpler version of CalculateAutosplitPages below, but only picks user prefs up from textboxes

If MultiPage1.selectedItem.Name = "pgAuto" Then
    GetCustomSplitPages = Null
    Exit Function
End If


Dim nParts As Integer

nParts = getLastVisibleRow

Dim arrSplitPages() As String

ReDim arrSplitPages(1, nParts - 1) ' we index arrays from 0, we index option buttons showable to public from 1 !


Dim tmptextbox As TextBox

For i = 0 To nParts - 1
    Set tmptextbox = getControlNamed("txtStart" & i + 1)
    
    If tmptextbox.Text <> "" Then
        arrSplitPages(0, i) = tmptextbox.Text
    Else
        arrSplitPages(0, i) = 0
    End If
    
    Set tmptextbox = getControlNamed("txtEnd" & i + 1)
    
    If tmptextbox.Text <> "" Then
        arrSplitPages(1, i) = tmptextbox.Text
    Else
        arrSplitPages(1, i) = 0
    End If

Next i

GetCustomSplitPages = arrSplitPages

End Function


Function CalculateAutosplitPages(Optional docPartsNumber, Optional UseOnlyThisManyPages, Optional BeginningPageOffset) As Variant

' version 0.66

' for the automatic document split feature, function used to calculate a proposal of splitting the doc as well as
' the beginning pages of every part

' 0.50 - initial version
' 0.55 - corrected a bug that made the result not behave as intended when the number of pages in doc was a multiple of them magical number... no remainder to the division
' 0.60 - modified function to take first optional parameter, meaning the number of parts and calculate the other, of course.
'        Modification to be used in the context of using same function to supply a range of starting pages for a known number of parts (split this 144 page doc in 5 somewhat equal parts)
' 0.66 - Inserted second optional parameter - Mod to allow user to specify only a part of document (by choosing to leave some parts of document off from first and last part in custom
'        split mode)

Dim npg As Integer  ' number of pages
Dim dp As Integer   ' doc part preffered size

If IsMissing(UseOnlyThisManyPages) Then
    npg = ctDocNumPages
Else
    npg = UseOnlyThisManyPages
End If


' Lets see if we need to determine the number of parts (default use scenario, in auto-split feature)
If IsMissing(docPartsNumber) Then

    dp = prefferedMaxDocPart

    If dp = 0 Then setMaxPrefferredDocPart: dp = prefferedMaxDocPart
    
Else    ' or, used by the "Equalise" button from the custom-split feature, maybe we KNOW the number of parts (optional parameter docPartsNumber) and need determine doc parts size
    
    dp = npg \ CInt(docPartsNumber)
    
End If


If Not IsMissing(BeginningPageOffset) Then
    Dim boff As Integer
    boff = CInt(BeginningPageOffset)
Else
    boff = 0
End If


Dim hm As Integer           ' high max document part
hm = npg \ dp

'__________________________________________________________________________
'

Dim hmx As Integer          ' high maximised document part

' Need to determine if parts size is ceiled or not, we judge from optional parameter being supplied or not.
If IsMissing(docPartsNumber) Then
    If npg Mod dp > 0 Then
        hmx = Int(hm) + 1
    Else
        hmx = Int(hm)
    End If
Else    ' we are not allowed to modify the number of parts if user supplied it, we will modify the parts sizes further along. hmx has to stay equal to hm !
    hmx = Int(hm)
End If


Dim cdp As Integer          ' current document part size
cdp = Int(npg / hmx)

Dim exp As Integer           ' extrapages
exp = npg - (cdp * hmx)

Dim arrSplitPages() As Integer

Dim cexp As Integer             ' count exp usage counter
' But only use exp if exp is supra-zero
If exp > 0 Then
    cexp = 1
Else
    cexp = 0
End If



For i = 0 To hmx - 1    ' since we're indexing from 0
        
    ReDim Preserve arrSplitPages(i)
        
    If i > 0 Then
        If exp > 0 Then
            arrSplitPages(i) = i * cdp + cexp * 1 + 1 + boff    ' also add beginning offset, if it's non-zero
            exp = exp - 1
            If exp > 0 Then cexp = cexp + 1     ' increase count exp usage
        Else
            arrSplitPages(i) = i * cdp + cexp * 1 + 1 + boff
        End If
        
    Else
        arrSplitPages(i) = (i * cdp + 1) + boff
    End If
Next i

CalculateAutosplitPages = arrSplitPages

End Function



Function Calculate_AutosplitPages_ByCharacter(Optional docPartsNumber, Optional UseOnlyThisManyPages, Optional BeginningPageOffset) As Variant
' Kinda mirror function of CalculateAutosplitPages above, using a character count of the document and its parts, not pages count.
' Returns, however, an array of page numbers... since we're going to still present this to the user (pages)

Dim tnchar As Long
tnchar = ctDocCharCount     ' that's the total character count from from all non-empty storyranges (prob main text and footnotes)

'_________________________________________________________________________________________________________________________________________________

Dim fnchar As Long
fnchar = ctDocFootnotesCount

Dim mnchar As Long
mnchar = ctDocMaintextCount

Dim aFnPg As Long               ' average footnotes per page
aFnPg = fnchar / ctDocNumPages



'_________________________________________________________________________________________________________________________________________________


If IsMissing(docPartsNumber) Then
    
    Dim pdppag As Long
    pdppag = prefferedMaxDocPart
    
    Dim ccp As Long                 ' current characters per page
    ccp = tnchar / ctDocNumPages    ' number of characters per page, average for current document
    
    Dim cdpc As Long        ' current document part characters number
    cdpc = CLng(ccp * pdppag)
    
    Dim dp As Integer       ' doc parts number
    dp = Round(CInt(tnchar / cdpc))
    
Else
    
    dp = docPartsNumber
    
End If

Dim SplitCharsArr() As Long
'ReDim SplitCharsArr(0)

Dim ccdpc As Long       ' corrected currend document part chars number
ccdpc = tnchar \ dp


Dim ctCharRng As Range

' Calculate character numbers for doc parts borders
For i = 0 To dp - 1
    ReDim Preserve SplitCharsArr(i)
    SplitCharsArr(i) = 1 + (i * ccdpc)
    
    Set ctCharRng = ActiveDocument.Characters(SplitCharsArr(i))
    SplitCharsArr(i) = SplitCharsArr(i) + Round(ctCharRng.Information(wdActiveEndAdjustedPageNumber) * aFnPg)
    
Next i


' After getting the border character for all parts, set bookmarks to those
' (allowance for next/ previous paragraph/ fullstop included)
Call Set_CharSplit_Bookmarks(SplitCharsArr)


Dim SplitCharPagesNum() As Integer


' Now lets also retrieve pages numbers for all char numbers concerned
For i = 0 To dp - 1
    
    Set ctCharRng = ActiveDocument.Characters(SplitCharsArr(i))
    
    ReDim Preserve SplitCharPagesNum(i)
    ' Let's try to see how well this method adjusts
    SplitCharPagesNum(i) = ctCharRng.Information(wdActiveEndAdjustedPageNumber)
    
Next i

Calculate_AutosplitPages_ByCharacter = SplitCharPagesNum

End Function



Sub Set_CharSplit_Bookmarks(BorderCharactersArray)


If Not IsArray(BorderCharactersArray) Then
    MsgBox "Boo!"
    Exit Sub
End If


Dim ctDPB As Range

For i = 0 To UBound(BorderCharactersArray)
    
    Set ctDPB = ActiveDocument.Range(BorderCharactersArray(i), BorderCharactersArray(i))
    
    ActiveDocument.Bookmarks.Add "DS_" & i & "_S_" & Format(ctDPB.Information(wdActiveEndAdjustedPageNumber), "000"), ctDPB
    
Next i


End Sub



Private Sub cbSplit_Click()
' Split button click handler, MAIN ACTION happens here !

If Documents.count = 0 Then Exit Sub

' clear current line range if it was left colored by user
If Not ctLineRng Is Nothing Then
    ctLineRng.Shading.BackgroundPatternColorIndex = wdNoHighlight
End If

Dim objFile As Scripting.FileSystemObject
Set objFile = CreateObject("Scripting.FileSystemObject")

Application.ScreenUpdating = False
    
    strFileName = ActiveDocument.Name
    
    ' Watch out, will have trouble if using doc otherwisely named, such as having TWO EXTENSIONS !
    strDocNo = objFile.GetBaseName(strFileName) & "."
    strDocType = "." & objFile.GetExtensionName(strFileName)
    
    ' user in charge, as nice it is... we decide as to in which folder to save parts, acc to user choice in options !
    ' such as T:\Docs\username, or users default word documents folder, or... user's browse result! (function already ready...)
        
    Dim userBaseFolderChoice As String
    userBaseFolderChoice = getOption("SaveToLocation")
    
    Select Case userBaseFolderChoice
        Case ""     ' gotta have a default...
            strBaseFolder = ActiveDocument.Path
        Case "SaveToTUserFolder"
            strBaseFolder = "T:\Docs\" & sUserName
        Case "SaveToDocumentsFolder"
            strBaseFolder = System.PrivateProfileString("", "HKEY_CURRENT_USER\Software\Microsoft\Office\14.0\Word\Options", "doc-path")
        Case "SaveToCurrentDocHost"
            strBaseFolder = ActiveDocument.Path
        Case "SaveToProjectFolder"
            strBaseFolder = "T:\Prepared originals\" & getGSCBaseName(ActiveDocument.Name)
        Case "SaveToCustomFolder"
            strBaseFolder = getOption("CustomFolderPath")
    End Select
    
    ' Fetch user preference for having a sub-folder created and ammend the base folder accordingly
    Dim createSubFolderForParts As String
    createSubFolderForParts = getOption("CreateFolderForParts")
    
    If createSubFolderForParts = "True" Then
        strBaseFolder = strBaseFolder & "\SplitDoc_" & objFile.GetBaseName(strFileName)
        
        If Not objFile.FolderExists(strBaseFolder) Then
            objFile.CreateFolder (strBaseFolder)
        End If
        
        If Not objFile.FolderExists(strBaseFolder) Then
            MsgBox "Could not create destination folder, " & strBaseFolder & "\" & objFile.GetBaseName(strFileName) & vbCrCr & _
                "Are you sure you have writing privileges over there? Try creating it manually, see what gives", vbOKOnly + vbCritical, "Could not create folder!"
            Exit Sub
        End If
    End If
        
    ChangeFileOpenDirectory strBaseFolder
    
    ' strings arrays
    Dim sDocPartFilenames() As String      ' complete document parts names (path included, but using current doc's folder)
    Dim sPartStart() As String             ' starting ranges page values
    Dim sPartEnd() As String               ' ending ranges page values
    
    ' hiding Split form
    DocSplit.Hide
    
    ' Lets differentiate between custom user defined page ranges and the auto proposal, shall we ?
    If MultiPage1.selectedItem.Name = "pgCustom" Then
    
        Dim iPartsNumber As Integer
        ' desired number of parts, provided by user
        iPartsNumber = getLastVisibleRow
        
        ReDim sPartStart(iPartsNumber - 1)
        ReDim sPartEnd(iPartsNumber - 1)
        ReDim sDocPartFilenames(iPartsNumber - 1)
        
        Dim custPg As Variant
        
        custPg = GetCustomSplitPages
        
        ' filling in string arrays sPartStart and sPartEnd from above with user values
        ' lets loop through all start and end text boxes, we like not multiple multiple ifs...
        For i = 0 To iPartsNumber - 1
            
            sPartStart(i) = custPg(0, i)
            sPartEnd(i) = custPg(1, i)
                        
        Next i
        
    Else    ' this means multipage1 is on "pgAuto"...
        
            Dim autoSPg As Variant
            
            'If getOption("UseCharacterCount") Then
                
            'Else
                autoSPg = GetAutosplitPages     ' the starting pages
            'End If
            
            iPartsNumber = UBound(autoSPg) + 1  ' to be on same phase as other case code, where it's indexed from 1
            
            ReDim sPartStart(iPartsNumber - 1)
            ReDim sPartEnd(iPartsNumber - 1)
            ReDim sDocPartFilenames(iPartsNumber - 1)
            
            For i = 0 To UBound(autoSPg)
                sPartStart(i) = Format(autoSPg(i), "000")
            Next i
            
            For i = 1 To UBound(autoSPg)
                sPartEnd(i - 1) = Format(autoSPg(i) - 1, "000")
            Next i
            sPartEnd(i - 1) = Format(ctDocNumPages, "000")   ' now i is just beyond Ubound(autoSPg),
            ' sDocPartFilenames     -- above, only filled with empty string. Why?
        
    End If
    
        
    ' checking all the user-supplied ending page ranges for consistency
    For i = 0 To iPartsNumber - 1    '9
        If CInt(sPartStart(i)) > CInt(sPartEnd(i)) Then
            MsgBox "Page range " & i + 1 & " is not correct!", vbCritical, "Wrong page numbers"
            Exit Sub
        End If
    Next i
    
    ' filling all document parts complete names into sDocPartFilenames array
    For i = 0 To iPartsNumber - 1 '9
        If sPartStart(i) <> "0" Then sDocPartFilenames(i) = strBaseFolder & "\" & strDocNo & sPartStart(i) & "-" & sPartEnd(i) & strDocType
    Next i
    
    sLastPartName = sDocPartFilenames(iPartsNumber - 1)     ' saving in variable complete path and name of last doc part
    iLastPartStart = CInt(sPartStart(iPartsNumber - 1))     ' saving in integer variable the beginning of last doc part
    
    ' custom page split is as configurable as it gets... it lest you even not end your last part at last page of doc...
    ' provide for that by arranging a variable containing desired ending page
    Dim iLastPartEnd As Integer
    If CInt(sPartEnd(iPartsNumber - 1)) <> ctDocNumPages Then iLastPartEnd = sPartEnd(iPartsNumber - 1)
    
    ' provide for first part beginning at something else than 1
    Dim iFirstPartStart As Integer
    If CInt(sPartStart(0)) <> 1 Then iFirstPartStart = sPartStart(0)
    
    ' unlink all footers, will make them show up in parts
    Call UnlinkAllFooters
    
    ' Before getting to the actual multiple saving-as of copies and to the splitting sub-routine, which will be separated with the below code,
    ' lets set the proper bookmarks (by using the autoPage's lblPages label or custPage's textboxes and
    ' setting bookmarks to the beginnings of those pages unless already set by humans already
    Call Set_Splitting_Bookmarks
    
    ActiveDocument.Save
    
    ' saving last part of document. Copy of whole document is saved.
    ActiveDocument.SaveAs2 FileName:=sLastPartName, FileFormat:=wdFormatXMLDocument, _
        LockComments:=False, Password:="", AddToRecentFiles:=True, _
        WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:=False, _
        SaveNativePictureFormat:=False, SaveFormsData:=False, _
        SaveAsAOCELetter:=False, CompatibilityMode:=11
        
    ' juggle back and forth for footerheader main text displacement problem resolution
    'ActiveWindow.ActivePane.View.SeekView = wdSeekCurrentPageHeader
    'ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument
    
    ' saving all the middle parts of docs. Copy of whole document is saved.
    If iPartsNumber > 2 Then
        For i = 1 To iPartsNumber - 2
            ActiveDocument.SaveAs2 FileName:=sDocPartFilenames(i), FileFormat:=wdFormatXMLDocument, _
            LockComments:=False, Password:="", AddToRecentFiles:=True, _
            WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:=False, _
            SaveNativePictureFormat:=False, SaveFormsData:=False, _
            SaveAsAOCELetter:=False, CompatibilityMode:=11
            
            ' juggle back and forth for footerheader main text displacement problem resolution
            'ActiveWindow.ActivePane.View.SeekView = wdSeekCurrentPageHeader
            'ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument
            
        Next i
    End If
    
    ' saving first part of doc
    ActiveDocument.SaveAs2 FileName:=sDocPartFilenames(0), FileFormat:=wdFormatXMLDocument, _
        LockComments:=False, Password:="", AddToRecentFiles:=True, _
        WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:=False, _
        SaveNativePictureFormat:=False, SaveFormsData:=False, _
        SaveAsAOCELetter:=False, CompatibilityMode:=11
        
        ' juggle back and forth for footerheader main text displacement problem resolution
    'ActiveWindow.ActivePane.View.SeekView = wdSeekCurrentPageHeader
    'ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument
    
    Call SplitDocument_inParts(iPartsNumber, sPartStart, sPartEnd, iFirstPartStart, iLastPartStart, iLastPartEnd, sLastPartName, sDocPartFilenames)


' opening document's folder, if it exists
If objFile.FolderExists(strBaseFolder) Then
    Shell "C:\WINDOWS\Explorer.exe " & strBaseFolder, vbNormalFocus
End If

Unload DocSplit

Set objFile = Nothing
Application.ScreenUpdating = True

Exit Sub

'_________________________________________________________________________________________________________________________
' Error handler
errorCBSplit:
    If Err.Number <> 0 Then
        MsgBox "Error" & Err.Number & " occured in" & Err.Source & " with message: " & Err.Description, vbOKOnly, "Error in CBSplit"
        Err.Clear
    End If

End Sub


Sub SplitDocument_inParts(iPartsNumber, sPartStart() As String, sPartEnd() As String, iFirstPartStart, iLastPartStart, iLastPartEnd, sLastPartName, sDocPartFilenames() As String)


' removing intersection from part 1, which had been left opened. Closing it. Using ending integers array, iPartEnd
    
    Dim tmpEBk As Bookmark
    
    If Exists_bookmarkLike("DS_0_E*") Then
        Set tmpEBk = Get_BookmarkLike("DS_0_E*")
    Else
        If Exists_bookmarkLike("DS_1_S*") Then
            Set tmpEBk = Get_BookmarkLike("DS_1_S*")
        Else
            '????
        End If
    End If
    
    ' what if we could not secure a chop bookmark?
    If tmpEBk Is Nothing Then
        MsgBox "BOO!"
        Exit Sub
    End If
    
    
    Selection.GoTo What:=wdGoToBookmark, Name:=tmpEBk.Name ' going to n+1 page because we need actually keep page(n)
    
    
    ' when we're in the first cell of first row of a table, selecting backwards (to beginning of doc) results in selecting current row as well... deleting that results
    ' in losing current row, which of course we wanted kept !
    Selection.MoveEndWhile Chr(12) & vbCr, wdBackward   ' go to previous char, last of previous page, so as not to have an empty page at the end of doc part
   
    Selection.EndKey Unit:=wdStory, Extend:=wdExtend
    Selection.Delete Unit:=wdCharacter, count:=1
    
    ' note down page to start for doc parts where we remove from beginning of 'em
    Dim pageToStart As Integer
    
    ' provide for first part starting from another page than 1
    If Exists_bookmarkLike("DS_0_S*") Then
        
        Dim tmpSBk As Bookmark
        Set tmpSBk = Get_BookmarkLike("DS_0_S*")
        
        If Not tmpSBk Is Nothing Then
        
            If (tmpSBk.Range.Information(wdActiveEndAdjustedPageNumber) > 1 Or _
                tmpSBk.Range.Information(wdFirstCharacterLineNumber) > 1) Then
            
                Selection.GoTo What:=wdGoToBookmark, Name:=tmpSBk
            
                pageToStart = Selection.Range.Information(wdActiveEndAdjustedPageNumber)
                
                ' when we're in the first cell of first row of a table, selecting backwards (to beginning of doc) results in
                ' selecting current row as well... deleting that results in losing current row, which of course we wanted kept !
                If Selection.Information(wdWithInTable) Then
                    Selection.StartOf wdCell, wdMove
                    Selection.MoveEnd wdCharacter, -1
                End If
                    
                Selection.HomeKey Unit:=wdStory, Extend:=wdExtend
                Selection.Delete Unit:=wdCharacter, count:=1
                
            
                ' write modified page to start right back to doc part
                ActiveDocument.Sections(1).Footers(wdHeaderFooterPrimary).PageNumbers.RestartNumberingAtSection = True
                ActiveDocument.Sections(1).Footers(wdHeaderFooterPrimary).PageNumbers.StartingNumber = pageToStart
                
            End If
        
        End If
        
    End If
        
    Delete_AllBookmarksNamed ("DS_*")
    ActiveDocument.Save
    ActiveDocument.Close
    
    
    ' Removing intersecting part from last doc part after opening it.
    Documents.Open FileName:=sLastPartName, ConfirmConversions:=False, _
                ReadOnly:=False, Format:=wdOpenFormatAuto, Encoding:=1252
    
    ' remove doc part after last part's ending page, if that differs from last doc page
'    Dim numParts As String
'    numParts = UBound(ctPropPages, 2)
    
    If Exists_bookmarkLike("DS_" & iPartsNumber - 1 & "_E_*") Then
        
        Set tmpEBk = Get_BookmarkLike("DS_" & iPartsNumber - 1 & "_E_*")
        
        Selection.GoTo What:=wdGoToBookmark, Name:=tmpEBk       ' as above, need keep page we go to (goto method takes one to the beginning of a page)
        
        'Selection.MoveEnd wdCharacter, -1   ' go to previous char, last of previous page, so as not to have an empty page at the end of doc part
        Selection.EndKey Unit:=wdStory, Extend:=wdExtend
        Selection.Delete Unit:=wdCharacter, count:=1
    
    Else    'User did not redefine ending splitpoint for last part, but we still check if (s)he specified a different page than the last
        
        ' only if we're on custom split can we do the following
        If MultiPage1.selectedItem.Name = "pgCustom" Then
            
            ' user specified custom ending page for last split part is not identical to number of pages in document
            If sPartEnd(iPartsNumber - 1) <> ctDocNumPages Then
                
                Selection.GoTo What:=wdGoToPage, which:=wdGoToAbsolute, count:=sPartEnd(iPartsNumber - 1) + 1
                Selection.MoveEndWhile Chr(12) & vbCr, wdBackward
                
                Selection.EndKey wdStory, wdExtend
                Selection.Delete wdCharacter, 1
                
            End If
            
        End If
    End If
    
    
    ' Go to and remove everything from before the beginning of last part
    
    Set tmpEBk = Get_BookmarkLike("DS_" & iPartsNumber - 1 & "_S_*")
    
    Selection.GoTo What:=wdGoToBookmark, Name:=tmpEBk
    
    pageToStart = Selection.Range.Information(wdActiveEndAdjustedPageNumber)
    
    ' when we're in the first cell of first row of a table, selecting backwards (to beginning of doc) results in
    ' selecting current row as well... deleting that results in losing current row, which of course we wanted kept !
    If Selection.Information(wdWithInTable) Then
        Selection.StartOf wdCell, wdMove
        Selection.MoveEnd wdCharacter, -1
    End If

    
    Selection.HomeKey Unit:=wdStory, Extend:=wdExtend
    Selection.Delete Unit:=wdCharacter, count:=1
            
    ActiveDocument.Sections(1).Footers(wdHeaderFooterPrimary).PageNumbers.RestartNumberingAtSection = True
    ActiveDocument.Sections(1).Footers(wdHeaderFooterPrimary).PageNumbers.StartingNumber = pageToStart
        
    Delete_AllBookmarksNamed ("DS_*")
    ActiveDocument.Save
    ActiveDocument.Close
    
    
    ' Removing intersecting parts of middle parts, if any.
    If iPartsNumber > 2 Then
        For i = 1 To iPartsNumber - 2
           Documents.Open FileName:=sDocPartFilenames(i), ConfirmConversions:=False, _
                    ReadOnly:=False, Format:=wdOpenFormatAuto, Encoding:=1252
            
            ' For intermediate parts, provide for the ending bookmark of previous part to be missing, if continuous splitting point
            If Exists_bookmarkLike("DS_" & i & "_E_*") Then
                Set tmpEBk = Get_BookmarkLike("DS_" & i & "_E_*")
                Selection.GoTo What:=wdGoToBookmark, Name:=tmpEBk

            Else
                Set tmpEBk = Get_BookmarkLike("DS_" & i + 1 & "_S_*")
                Selection.GoTo What:=wdGoToBookmark, Name:=tmpEBk
                Selection.MoveEndWhile Chr(12) & vbCr, wdBackward
            End If
            
            'Selection.GoTo What:=wdGoToBookmark, Name:=tmpEBk
            'Selection.MoveEnd wdCharacter, -1   ' go to previous char, last of previous page, so as not to have an empty page at the end of doc part

            Selection.EndKey Unit:=wdStory, Extend:=wdExtend
            Selection.Delete Unit:=wdCharacter, count:=1
        
            ' and continue by identifying and deleting the excedent from the start of it
            Set tmpSBk = Get_BookmarkLike("DS_" & i & "_S_*")
            
            Selection.GoTo What:=wdGoToBookmark, Name:=tmpSBk
    
            ' note down page number at breaking point
            pageToStart = Selection.Range.Information(wdActiveEndAdjustedPageNumber)
            
            ' when we're in the first cell of first row of a table, selecting backwards (to beginning of doc) results in
            ' selecting current row as well... deleting that results in losing current row, which of course we wanted kept !
            If Selection.Information(wdWithInTable) Then
                Selection.StartOf wdCell, wdMove
                Selection.MoveEnd wdCharacter, -1
            End If
            
            Selection.HomeKey Unit:=wdStory, Extend:=wdExtend
            Selection.Delete Unit:=wdCharacter, count:=1
                            
            ' write starting page of section back
            ActiveDocument.Sections(1).Footers(wdHeaderFooterPrimary).PageNumbers.RestartNumberingAtSection = True
            ActiveDocument.Sections(1).Footers(wdHeaderFooterPrimary).PageNumbers.StartingNumber = CLng(pageToStart)
                                            
            Delete_AllBookmarksNamed ("DS_*")
            ActiveDocument.Save
            ActiveDocument.Close
        Next i
    End If



End Sub



Sub Set_Splitting_Bookmarks()
' Used during the execution of the Split button to set the bookmarks necessary for splitting the doc if not already present

Dim ctSplittingPages As Variant
' retrieve current split points in variable (from user form)
ctSplittingPages = getCt_SplitPages

If Not IsArray(ctSplittingPages) Then
    MsgBox "Boo!"
    Exit Sub
Else
End If


Dim ctPage As Range
' Setting all starting pages bookmarks
For i = 0 To UBound(ctSplittingPages, 2)
    If Not (ActiveDocument.Bookmarks.Exists("DS_" & i & "_S_" & Format(ctSplittingPages(0, i), "000")) Or _
       ActiveDocument.Bookmarks.Exists("DS_" & i & "_S_" & CStr(Format(CInt(ctSplittingPages(0, i)) - 1, "000")))) Then
        
        Set ctPage = ActiveDocument.Range.GoTo(wdGoToPage, wdGoToAbsolute, CInt(ctSplittingPages(0, i)))
        ActiveDocument.Bookmarks.Add "DS_" & i & "_S_" & Format(ctSplittingPages(0, i), "000"), ctPage
        
    End If
Next i

' and same for ending ranges bookmarks, unless we're in the automatic splitting case
If Not ctSplittingPages(1, 0) = "" Then
    For i = 0 To UBound(ctSplittingPages, 2)
        If Not (ActiveDocument.Bookmarks.Exists("DS_" & i & "_E_" & Format(ctSplittingPages(1, i), "000")) Or _
            ActiveDocument.Bookmarks.Exists("DS_" & i & "_E_" & Format(CInt(ctSplittingPages(1, i)) + 1, "000"))) Then
            
            If i < UBound(ctSplittingPages, 2) Then
                Set ctPage = ActiveDocument.Range.GoTo(wdGoToPage, wdGoToAbsolute, CInt(ctSplittingPages(1, i)) + 1)
                ctPage.MoveEndWhile Chr(12) & vbCr, wdBackward     ' beginning of next page is not good enough... need go one character earlier
            Else
                
                If CInt(ctSplittingPages(1, UBound(ctSplittingPages, 2))) <> ctDocNumPages Then
                    Set ctPage = ActiveDocument.Range.GoTo(wdGoToPage, wdGoToAbsolute, CInt(ctSplittingPages(1, UBound(ctSplittingPages, 2)) + 1))
                    ctPage.MoveEndWhile Chr(12) & vbCr, wdBackward
                Else
                    Set ctPage = ActiveDocument.Characters(1)
                    ctPage.EndOf wdStory, wdMove
                End If
                
            End If
            
            ActiveDocument.Bookmarks.Add "DS_" & i & "_E_" & Format(ctSplittingPages(1, i), "000"), ctPage
            
        End If
    Next i
End If


End Sub



Function getControlNamed(ControlName As String) As control

Dim c As control

For Each c In Me.Controls
    If c.Name = ControlName Then
        Set getControlNamed = c
        Exit Function
        'Exit Function
    End If
Next c

Set getControlNamed = Nothing

End Function


Sub optionButtons_DisplayHandle(tgOptionButton As OptionButton)
' To complement obOsa_Click event handlers
    
    Dim ctOb As String
        
    Dim tmpControl As control
    Set tmpControl = tgOptionButton
    
    ctOb = gscrolib.Extr_Numbers(tmpControl.Name)(0)
    
    'Make all controls before the current one as well as the current one, visible
    For i = 1 To ctOb
        Set tmpControl = getControlNamed("obPart" & i)
        tmpControl.Visible = True
        Set tmpControl = getControlNamed("txtStart" & i)
        tmpControl.Visible = True
        Set tmpControl = getControlNamed("lblDash" & i)
        tmpControl.Visible = True
        Set tmpControl = getControlNamed("txtEnd" & i)
        tmpControl.Visible = True
    Next i
    
    ' And now focus to the current control and select its text
    On Error Resume Next
    tmpControl.SetFocus
    
    If Err.Number = 2110 Then Err.Clear
    
    Dim tmptextbox As TextBox
    Set tmptextbox = getControlNamed("txtEnd" & ctOb)
    
    tmptextbox.SelStart = 0
    tmptextbox.SelLength = Len(tmptextbox.Value)
    
    ' Hide all other controls except
    For i = ctOb + 1 To 10 ' current max number of parts
        
        Set tmpControl = getControlNamed("obPart" & i)
        tmpControl.Visible = False
                
        Set tmpControl = getControlNamed("txtStart" & i)
        Set tmptextbox = tmpControl
        tmpControl.Visible = False
        tmptextbox.Text = ""
        
        Set tmpControl = getControlNamed("txtEnd" & i)
        Set tmptextbox = tmpControl
        tmpControl.Visible = False
        tmptextbox.Text = ""
        
        Set tmpControl = getControlNamed("lblDash" & i)
        tmpControl.Visible = False
        
    Next i
    
Set tmpControl = Nothing
Set tmptextbox = Nothing
    
End Sub

Sub UnlinkAllFooters()
' helps for doc parts's footers to show footers...

If Documents.count > 0 Then
    If ActiveDocument.Sections.count > 1 Then
        Dim s As Section
        
        For j = 2 To ActiveDocument.Sections.count
            
            Set s = ActiveDocument.Sections(j)
            
            If s.Footers(wdHeaderFooterPrimary).LinkToPrevious = True Then
                s.Footers(wdHeaderFooterPrimary).LinkToPrevious = False
            End If
        
        Next j
        
        ' juggle for footer opening text overflowing problem solving
        ActiveWindow.ActivePane.View.SeekView = wdSeekCurrentPageHeader
        ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument
        
    End If
End If

End Sub

Private Sub UserForm_Terminate()

If Documents.count > 0 Then
    ' and set the initial users doc view back
    ActiveWindow.ActivePane.View = iView
    iView = 0   ' and re-set to no view after using it!
End If

Unload Me

End Sub
