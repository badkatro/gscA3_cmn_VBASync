Attribute VB_Name = "StWizard"
Public studioWizard_PID As Long
Public triedLaunching As Boolean
Public latestOpenedDocs As String
    

Sub CARS_Automate_Launch()


Dim shellID As Long

shellID = Shell("CARS_Automate.exe", vbNormalFocus)



End Sub


'***************************************************************************************************************************************
'**************  CALLBACKS for Ribbon buttons, do not change Sub name or remove existing parameter without also altering underlying xml!
'***************************************************************************************************************************************
'
'
'***************************************************************************************************************************************
'**************  CALLBACKS for Ribbon buttons, do not change Sub name or remove existing parameter without also altering underlying xml!
'***************************************************************************************************************************************


Sub Launch_orSwitch_toStudioWizard()

On Error GoTo PID_Wrong

Dim NumLock As New NumLockClass
Dim numLockInitial As Boolean

' Note initial value of num lock key, we have problems changing it inadvertently
' after sendink keys around !!!
numLockInitial = NumLock.Value

' switch if PID found and launch it if not
If Not Switch_to_StudioWizard Then
    Launch_StudioWizard
End If
    

If getProcessPID("Studio_Wizard.exe") <> 0 Then
    
    'studioWizard_PID = shellID
    StatusBar = "Studio Wizard launched"
    
Else
    'studioWizard_PID = 0
    StatusBar = "Studio Wizard COULD NOT be launched. Please launch it yourself..."
End If

' Change back to initial value if case may be
If NumLock.Value <> numLockInitial Then
    NumLock.Toggle
End If

gscrolib.PauseForSeconds (0.3)

Exit Sub

'_____________________________________________________________________
'
PID_Wrong:
    
    If Err.Number = 53 Then
        Err.Clear
        StatusBar = "Studio Wizard not found in standard location, Please check installation. Exiting..."
    ElseIf Err.Number = 5 Then
        Err.Clear
    
'        If studioWizard_PID <> 0 Then
'            studioWizard_PID = 0
'            GoTo LaunchIt
'        End If

    End If

End Sub

Function Switch_to_StudioWizard() As Boolean

Dim studioPID As String

studioPID = getProcessPID("Studio_Wizard.exe")

If studioPID <> "" Then
    
    If Switch_to_RunningProcess(CLng(studioPID)) = False Then
        StatusBar = "Studio Wizard found and activated!"
        Switch_to_StudioWizard = True
    Else
        StatusBar = "Studio Wizard PID was found, but we could not activate it!"
        Switch_to_StudioWizard = False
    End If
    
Else
    StatusBar = "Studio Wizard PID was NOT found!"
    Switch_to_StudioWizard = False
End If

End Function


Function Switch_to_RunningProcess(ProcessID As Long) As Boolean


On Error GoTo NoSuchProcess

AppActivate (ProcessID)
' TEST -  Deactivating the below, does not seem necessary!



'SendKeys ("% r")
' DEACTIVATED the BELOW, prob an old attempt to counter the numlock status toggleing!
'SendKeys "{NUMLOCK}"

Switch_to_RunningProcess = True

Exit Function

'____________________________________________________________________________
'
NoSuchProcess:
    Err.Clear
    Switch_to_RunningProcess = False
    
End Function

Function getProcessPID(processName As String) As String
''On Error Resume Next

Dim objProcess, process, strNameOfUser

ComputerName = Environ("computername")
strNameOfUser = Environ("username")

Set objProcess = GetObject("winmgmts:{impersonationLevel=impersonate}\\" _
      & ComputerName & "\root\cimv2").ExecQuery("Select * From Win32_Process")
For Each process In objProcess
    If process.Name <> "System Idle Process" And process.Name <> "System" Then
        ''Debug.Print process.Name
        If process.Name Like processName Then
            'Debug.Print process.Name & "," & process.executablepath _
                & "," & process.Priority & "," & process.sessionid _
                & "," & strNameOfUser & "," & process.handlecount _
                & "," & process.ThreadCount
            getProcessPID = process.Handle
            Exit Function
            
        End If
    End If
Next

getProcessPID = ""

Set objProcess = Nothing
End Function

Function Launch_StudioWizard() As Boolean

On Error GoTo errLaunchingSW

Dim shellID As Long
Dim studioPID As String

studioPID = getProcessPID("Studio_Wizard.exe")


If studioPID = "" Then
    
    ' We launch Studio Wizard, we found it not running
    shellID = Shell("C:\Program Files (x86)\SDL\SDL Trados Studio\Studio2\Studio_Wizard.exe", vbNormalFocus)

    If shellID <> 0 Then
        
        'studioWizard_PID = shellID
        StatusBar = "Studio Wizard launched and its PID captured"
        Launch_StudioWizard = True
        gscrolib.PauseForSeconds (0.3)
        
    Else
        'studioWizard_PID = 0
        StatusBar = "Studio Wizard COULD NOT be launched. Please launch it yourself..."
        Launch_StudioWizard = False
    End If

Else
    
    ' Else we switch to Studio Wizard process ID
    If Not Switch_to_RunningProcess(CLng(studioPID)) Then
        StatusBar = "Studio Wizard ProcessID was found, we could not switch to it !"
        Launch_StudioWizard = False
    End If
    
    StatusBar = "Studio Wizard found and activated!"
    Launch_StudioWizard = True

End If

Exit Function

'__________________________________________________________
'
errLaunchingSW:
    If Err.Number = 53 Then
        Err.Clear
        StatusBar = "Studio Wizard not found in standard location, Please check installation. Exiting..."
    End If

End Function



Sub getdoc_with_StudioWizard()
' vers 0.5

On Error GoTo NoSuchID

Dim doctoGetF As String
Dim doctoGet As String
Dim docFileName As String
Dim openedWithNoDoc As Boolean      ' So we know if to supply foldername/ filename to openProjectFolder, at the end

Dim NumLock As New NumLockClass
Dim numLockInitial As Boolean

' Note initial value of num lock key, we have problems changing it inadvertently
' after sendink keys around !!!
numLockInitial = NumLock.Value

' Try getting doc number from activedoc or text box input
If Documents.count > 0 Then
    
    If Is_GSC_Doc(ActiveDocument.Name, SilentMode) Then
        
        docFileName = ActiveDocument.Name
        doctoGetF = getGSCBaseName(ActiveDocument.Name)
        doctoGet = preProcessedWBaseFolder & doctoGetF & "\" & docFileName
    Else
        GoTo StillEmpty
    End If
    
Else
    
StillEmpty:
    
    Dim doctogetS As String         ' Doc to get standard name
    doctogetS = InputBox("Please provide original language document standard name to get, up to second dot and original's language", "Get which document", "ST 12345/13 REV 1 EN")
    
    doctoGetF = GSC_StandardName_WithLang_toFileName(doctogetS)
    
    If Not Is_GSC_Doc(doctoGetF) Then
        StatusBar = "Supplied string NOT valid GSC document name and language string, Please retry!"
        Exit Sub
    End If
    
    
    docFileName = doctoGetF & ".doc"
    doctoGet = preProcessedWBaseFolder & doctoGetF & "\" & docFileName
    openedWithNoDoc = True
    
End If


' Sometimes the desired document to prepare (to get) is NOT on W !
' When its too late to wait, automatically detect and switch to originals Path, M:\Prod\...
If Dir(doctoGet) = "" Then
    ' Switch to MProd base
    If Build_MProd_Path(GSCFileName_ToStandardName(docFileName)) <> "" Then
        
        If MsgBox("Preprocessed document version of " & doctoGet & " was NOT found on W drive!" & vbCr & vbCr & _
            "Do you wish to get original version from M Prod path?", vbOKCancel, "Preprocessed version MISSING on W!") = vbOK Then
            
            doctoGet = Build_MProd_Path(GSCFileName_ToStandardName(docFileName))
            
            ' Failsafe, document CAN be missing from MProd path ALSO ! Kwaaayzy peopleus are woeeking at the Council !
            If doctoGet = "" Then
                StatusBar = "Document was NOT fount in W or in MProd ! Nothing to do, Exiting..."
                Exit Sub
            End If
            
        Else
            Exit Sub
        End If
        
    End If
Else
End If


' Now see if you can switch to running StWiz process or need launch it
If Not Switch_to_StudioWizard Then
    Launch_StudioWizard
End If
    

' Activate window menu (left upper corner) and "restore" window
' (deminimize it if is)
'SendKeys ("% r")
'SendKeys "{NUMLOCK}"

' Choose command finalize
SendKeys "%g"
' And hit enter
SendKeys "{ENTER}", True
' Pause for operation completion in Studio Wizard
PauseForSeconds 0.9

' Now give it req project folder name
If doctoGet <> "" Then
    
    Set_Clipboard_TextContents doctoGet
    
    SendKeys "^v", True
    SendKeys "{ENTER}", True
    ' Pause to let it open folder
    PauseForSeconds 0.9
    ' And send it proper original name
    
    'SendKeys doctoGet
    'SendKeys "{ENTER}"
    DoEvents
End If

' Change back to initial value if case may be
If NumLock.Value <> numLockInitial Then
    NumLock.Toggle
End If

' And pause longer, since operations are more complex this time
PauseForSeconds 0.9

' Verification and communication to user.
If Dir(ProjectsBaseFolder & doctoGetF & "\" & docFileName) <> "" Then
    Dim choice As Integer
    choice = MsgBox("Get doc FOUND and copied the pre-processed original document to newly created project folder!" & vbCr & vbCr & _
        "Would you like to open the project folder now? ", vbYesNo, "Get Doc SUCCESS")
    'StatusBar = "Get Doc: SUCCES! Original preprocessed document found and copied to project folder!"
    
    If choice = vbYes Then
        If openedWithNoDoc Then
            Call openProjectFolder(doctoGetF)
        Else
            Call openProjectFolder
        End If
    Else
    End If
    
Else
    StatusBar = "Get Doc: FAIL! Original preprocessed document NOT found! RETRY MANUALLY!"
End If

Exit Sub

'_________________________________________

NoSuchID:
    'studioWizard_PID = 0
    Err.Clear
    
'    If Not triedLaunching Then
'        triedLaunching = True
'        Launch_StudioWizard
'        GoTo oncemore
'    Else
'        triedLaunching = False
'        GoTo oncemore
'    End If

End Sub

Sub TestClipboardStudioWizard()

' Attempt using clipboard to insert an ANSI string determined in VBA to Stud Wiz
' without it being altered by current keyboard layout

If Not Switch_to_StudioWizard Then
    Launch_StudioWizard
End If

Dim test As String

test = "m,.q3"

Set_Clipboard_TextContents (test)

SendKeys "%f{ENTER}"
PauseForSeconds 0.6

SendKeys "^v"


End Sub



Sub finalize_with_StudioWizard(Optional documentID, Optional skipTmxCheck)
' vers 0.63

'On Error GoTo NoSuchID

Dim doctoGetF As String
Dim doctoGet As String
Dim docFileName As String

Dim NumLock As New NumLockClass
Dim numLockInitial As Boolean

' Note initial value of num lock key, we have problems changing it inadvertently
' after sendink keys around !!!
numLockInitial = NumLock.Value


If Documents.count > 0 Then
    
    If Is_GSC_Doc(ActiveDocument.Name) Then
        
        docFileName = ActiveDocument.Name
        doctoGetF = getGSCBaseName(ActiveDocument.Name)
        doctoGet = ProjectsBaseFolder & doctoGetF & "\" & docFileName
        
        If Dir(doctoGet) = "" Then
            If Dir(Replace(doctoGet, ".doc", ".docx")) <> "" Then
                doctoGet = Replace(doctoGet, ".doc", ".docx")
            End If
        End If
            
    Else
        GoTo StillEmpty
    End If
    
Else
    
StillEmpty:
    
    Dim doctogetS As String         ' Doc to get standard name
    
    ' if called without parameter, ask for input document number, if not... not
    If IsMissing(documentID) Then
    
        If UBound(Split(latestOpenedDocs, ",")) <> -1 Then
        
            doctogetS = InputBox("Please provide original language document standard name to finalise, up to second dot and original's language, if different from �EN�", _
                "Finalise which document", Split(latestOpenedDocs, ",")(UBound(Split(latestOpenedDocs, ","))))
        Else
            
            doctogetS = InputBox("Please provide original language document standard name to finalise, up to second dot and original's language, if different from �EN�", _
                "Finalise which document", "ST 12345/13 REV 1 EN")
            
        End If
    
    Else
        
        doctogetS = documentID
        
    End If
    
    ' correct for user not having supplied original's language, guess for english
    If Not IsValidLangIso(Right$(doctogetS, 2)) Then
        doctogetS = Trim(doctogetS) & " EN"
    End If
        
    doctoGetF = GSC_StandardName_WithLang_toFileName(doctogetS)
    
    If Not Is_GSC_Doc(doctoGetF) Then
        StatusBar = "Supplied string NOT valid GSC document name and language string, Please retry!"
        Exit Sub
    Else
        If InStr(1, latestOpenedDocs, doctogetS) = 0 Then
            latestOpenedDocs = latestOpenedDocs & IIf(latestOpenedDocs <> "", ",", "") & doctogetS
        End If
    End If
    
    
    docFileName = doctoGetF & ".doc"
    doctoGet = ProjectsBaseFolder & doctoGetF & "\" & docFileName
    
    ' Need check in project folder, see what the original is named. Could be ".doc" or ".docx"
    If Dir(doctoGet) = "" Then
        If Dir(Replace(doctoGet, ".doc", ".docx")) <> "" Then
            doctoGet = Replace(doctoGet, ".doc", ".docx")
        End If
        'but if no doc or docx files are found, let's see if file is not XLF file as a matter of fact !
        If Dir(Replace(doctoGet, ".doc", "." & Get_ULg & ".xlf")) <> "" Then
            doctoGet = Replace(doctoGet, ".doc", "." & Get_ULg & ".xlf")
        End If
    End If
        
    openedWithNoDoc = True
    
End If

OnceMore:

' Now see if you can switch to running StWiz process or need launch it
If Not Switch_to_StudioWizard Then
    Launch_StudioWizard
    PauseForSeconds 1.5
End If

' Activate window menu (left upper corner) and "restore" window
' (deminimize it if is)
'SendKeys ("% r")
'SendKeys "{NUMLOCK}"
' Choose command finalize
SendKeys "%f"
' And hit enter
SendKeys "{ENTER}"
PauseForSeconds 0.7

If doctoGet <> "" Then
    
    Set_Clipboard_TextContents doctoGet
    
    SendKeys "^v", True
    PauseForSeconds 0.2
    
    SendKeys "{ENTER}", True
        
    Clear_Clipboard_TextContents
    
End If

' Change back to initial value if case may be
If NumLock.Value <> numLockInitial Then
    NumLock.Toggle
End If

'PauseForSeconds 0.3

StatusBar = "Finalise with Studio Wizard: Waiting for StWiz to finish..."
PauseForSeconds (20)


' if called without optional parameter skipTmxCheck, by default it DOES check for it (and waits 25 seconds...)
' BUT called with that parameter, it skips checking (done in another part another time...)
If IsMissing(skipTmxCheck) Then

' PERHAPS Need check that tmx file has been exported and is in quicksave or in proper txm folder
' SEE if it works !!!

    Dim howmany As Byte
    
    StatusBar = "Finalise with Studio Wizard: Waiting for StWiz to finish..."
    PauseForSeconds (2)
    
tryagain:
    If Not exportTmx_existsOnT(doctoGetF) Then
        
        If howmany < 16 Then
        
            PauseForSeconds 1
            howmany = howmany + 1
        
            GoTo tryagain
        
        Else
            If Not exportTmx_IsBADTMX(doctoGetF) Then
                StatusBar = "Finalise with Studio Wizard: Tmx export was NOT found in 25 seconds, Please check manually!"
            End If
        End If
    
    Else
        StatusBar = "Finalise with Studio Wizard: Finalisation operation performed successfully!"
    End If
    
    
    Set NumLock = Nothing
    
    Exit Sub

Else
    
    Set NumLock = Nothing
    Exit Sub

End If

'_________________________________________

NoSuchID:
    studioWizard_PID = 0
    Err.Clear
    
End Sub
