Attribute VB_Name = "EPDocInsert"
' Main entry point for EP doc insert routine, it asks for EP ID
Public Sub Insert_EPDoc()

' save original user selection (or cursor position)
Dim originalSelection As Range
Set originalSelection = Selection.Range


Dim userReqID As String
userReqID = InputBox("Which EP doc ID to insert?" & vbcr & "(4 digits, but may omit leading 0s)", "EP Document Insert")


If userReqID <> "" Then
    
    Dim pFilePath As String
    pFilePath = Get_Parliament_Document(userReqID)
    
    If pFilePath <> "" Then
        
        MsgBox "Confirm insert at cursor position of text from file: " & vbCr & vbCr & pFilePath & " ?" & vbCr & vbCr & _
        "(It will be copied to your �D:\Docs�)", vbOKCancel + vbQuestion, "EP Document Insert"
        
        Dim fso As New Scripting.FileSystemObject
        fso.CopyFile pFilePath, "D:\Docs\", True
        
        PauseForSeconds (0.6)
        DoEvents
        
        Dim pFileName As String
        pFileName = fso.GetFileName(pFilePath)
        
        'Documents.Open ("D:\Docs\" & pFileName)
        Selection.InsertFile ("D:\Docs\" & pFileName)
        
        originalSelection.Select
        
        Call Delete_Parliament_Doc_Unneeded(userReqID)
        
    End If
    
End If

End Sub

Private Sub Delete_Parliament_Doc_Unneeded(ParliamentDocID As String)

If Selection.Information(wdWithInTable) Then
    Selection.Tables(1).Delete
End If

Dim tmpRange As Range
Set tmpRange = Selection.Paragraphs(1).Range

Dim lpRange As Range
Set lpRange = tmpRange.Paragraphs(tmpRange.Paragraphs.count).Range

Do While Not lpRange.Text Like "*_TA*" & ParliamentDocID & "*"
    
    tmpRange.MoveEnd wdParagraph, 1
    Set lpRange = tmpRange.Paragraphs(tmpRange.Paragraphs.count).Range
    
    ' Safety stop, so as to not go on seleting paragraphs until we end page/ doc...
    If tmpRange.Paragraphs.count > 5 Then
        If lpRange.Style = "AT Heading 2" Then
            StatusBar = "Parliament Doc Insert: could not identify EP headers to delete, proceed manually!"
            Exit Sub
        Else    ' It's been 8 paragraphs, could still not find Parl Doc title (style AT Heading 2), stopping...
            If tmpRange.Paragraphs.count = 8 Then
                StatusBar = "Parliament Doc Insert: could not identify EP headers to delete, proceed manually!"
                Exit Sub
            End If
        End If
    End If
    
Loop

tmpRange.Delete

End Sub


Private Function Get_Parliament_Document(DocumentID As String) As String

    Dim ctLanguageUnitCode As String
    ctLanguageUnitCode = Get_AutoUlg
    
    Dim ParliamentDoc_FileMask As String
    ParliamentDoc_FileMask = "*_" & ctLanguageUnitCode & ".docx"
    
    Dim ParliamentDoc_HostFolder As String
    If Len(DocumentID) < 4 Then
        DocumentID = Format(DocumentID, "0000")
    End If
    ParliamentDoc_HostFolder = Get_Parliament_Document_HostFolder(DocumentID)
    
    If ParliamentDoc_HostFolder <> "" Then
    
        Dim ParliamentDoc_FullPath_Mask As String
        ParliamentDoc_FullPath_Mask = ParliamentDoc_HostFolder & "\" & ParliamentDoc_FileMask
        
        Dim Parliament_Sought_FilePath As String
        Parliament_Sought_FilePath = bDir.bDir(ParliamentDoc_FullPath_Mask, , "ReturnFullPath")
        
        If Parliament_Sought_FilePath <> "" Then
            Get_Parliament_Document = Parliament_Sought_FilePath
            Exit Function
        Else
            Get_Parliament_Document = ""
        End If
        
    End If

End Function


Private Function Get_Parliament_Document_HostFolder(DocumentID As String) As String

    Const Parliament_Docs_Location_Mask As String = "M:\Documents Externes\Parlement\19\TA\*\*\"
           
    Dim pHostFolder As foundFilesStruct
    
    pHostFolder = bGDirAllFolders_Wild(Parliament_Docs_Location_Mask & DocumentID)
    
    
    If pHostFolder.count = 0 Then
        MsgBox "Found no host folder for Parliament doc ID " & userReqID, vbOKOnly + vbCritical, "Error, folder not found"
        Get_Parliament_Document_HostFolder = ""
        Exit Function
    ElseIf pHostFolder.count = 1 Then
        Get_Parliament_Document_HostFolder = pHostFolder.files(0)
    Else    ' possibly more folders found? Is that possible?
        MsgBox "Found multiple folders for Parliament doc ID " & userReqID, vbOKOnly + vbCritical, "Error, multiple folders!"
        Get_Parliament_Document_HostFolder = ""
        Exit Function
    End If

End Function
