VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmSpaceReplaceHelp 
   OleObjectBlob   =   "frmSpaceReplaceHelp.frx":0000
   Caption         =   "GSC RO CleanUp Help - �Space Replace�"
   ClientHeight    =   13125
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8355
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   6
End
Attribute VB_Name = "frmSpaceReplaceHelp"
Attribute VB_Base = "0{19654D97-536D-4AD7-A00F-4F27FA0CB2A7}{B5D7DD40-5519-4618-9A6C-EAD611E693E0}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False





Private Const SpRep_HelpLocation = "\Microsoft\Word\SpaceReplace\SpaceReplace_Help\"
Private Const SpRep_Prefix = "HelpSpaceReplace_"
Private Resized
' Change above constants to location and filenames prefix and
' change html files list to real list (or name accordingly, though improbable)
' to use with other html help files

Private Sub Image1_Click()
' Top button

Me.WebBrowser1.Navigate (Environ("AppData") & SpRep_HelpLocation & SpRep_Prefix & "Index.htm")

End Sub

Private Sub Image2_Click()
' Go Forward button

On Error GoTo ErrForward

' Normal web browser forward functionality, we choose to replace
' Me.WebBrowser1.GoForward

Dim navCorr(4) As String    ' navigation correspondence, used to navigate forward and backward
Dim whichIdx As Integer
Dim nextIs As String

' List of pages names to navigate, we go forward by finding out what index has the current page
' and increasing with one if we are below last element
navCorr(0) = "Index"
navCorr(1) = "Introduction"
navCorr(2) = "Installation"
navCorr(3) = "Usage"
navCorr(4) = "List"

whichIdx = GetString_fromArray(navCorr, CStr(Split(Split(Me.WebBrowser1.LocationName, "_")(1), ".")(0)))

If whichIdx <> -1 Then
    If whichIdx < 4 Then
        nextIs = Environ("AppData") & SpRep_HelpLocation & SpRep_Prefix & navCorr(whichIdx + 1) & ".htm"
        
        If Dir(nextIs) <> "" Then
            Me.WebBrowser1.Navigate (nextIs)
        End If
    End If
End If


Exit Sub

ErrForward:
    If Err <> 0 Then
        Err.Clear
        Exit Sub
    End If
End Sub

Function GetString_fromArray(TargetArray As Variant, StringToLookFor As String) As Integer
' Retrieve location index of given string into given array (-1 otherwise)

Dim gotstring As Boolean

If IsArray(TargetArray) Then
    
    For j = 0 To UBound(TargetArray)
        If StringToLookFor = TargetArray(j) Then
            gotstring = True
            GetString_fromArray = j     ' Found string, at index j
        End If
    Next j
    
    If gotstring = False Then
        GetString_fromArray = -1    ' String not found in array !
        Exit Function
    End If
    
Else
    MsgBox "Furnished array, " & TargetArray & ", is not an array !", vbOKOnly + vbCritical, "Error"
    GetString_fromArray = -1    ' String searched for not found in array !
    Exit Function
End If

End Function

Private Sub Image3_Click()
' Back button

On Error GoTo ErrBack

Me.WebBrowser1.GoBack

Exit Sub

ErrBack:
    If Err <> 0 Then
        Err.Clear
        Exit Sub
    End If
End Sub

Private Sub UserForm_Activate()
' Initial location

Dim fso As New Scripting.FileSystemObject
Dim ff As Scripting.File

If fso.FileExists(Environ("AppData") & SpRep_HelpLocation & SpRep_Prefix & "Index.htm") Then
    Me.WebBrowser1.Navigate (Environ("AppData") & SpRep_HelpLocation & SpRep_Prefix & "Index.htm")
Else
    Debug.Print "Not found " & Environ("AppData") & SpRep_HelpLocation & SpRep_Prefix
    MsgBox "Requested Help index file was not found ! Please call margama - 2942"
    Exit Sub
End If


End Sub

Private Sub UserForm_Resize()
' Resize browser and move buttons if user resizes userform

Me.WebBrowser1.Width = Me.Width - 4
Me.WebBrowser1.Height = Me.Height - 54

Me.Image3.Left = Me.Width - 56
Me.Image2.Left = Me.Width - 30

End Sub
