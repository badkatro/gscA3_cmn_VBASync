VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmXLFAutoProcess 
   OleObjectBlob   =   "frmXLFAutoProcess.frx":0000
   Caption         =   "Process XLF Doc"
   ClientHeight    =   4515
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5160
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   128
End
Attribute VB_Name = "frmXLFAutoProcess"
Attribute VB_Base = "0{9E7B6338-7F42-4DE2-94B2-F285142F1028}{5F39952C-D217-4278-AA83-59D516CBC519}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False



Private Sub cmdArchiveG_Click()


If tbDocNumInput.Text = "" Then
    
    Call ArchiveG   ' gonna ask for document number input from user or use current opened GSC doc
    
Else
    
    ' If user did not supply original language, determine it
    If Not IsValidLangIso(Right$(tbDocNumInput.Text, 2)) Then
        
        Dim projectNameTemplate As String
        
        ' Language code will be replaced by generic xx
        projectNameTemplate = GSC_StandardName_ToFileName(tbDocNumInput)
        
    Else
    
        projectNameTemplate = GSC_StandardName_WithLang_toFileName(tbDocNumInput)
        
    End If
        
    ' DID we get a GSC file name compliant response from user input doc ID?
    If Is_GSC_Doc(projectNameTemplate) Then
    
        ' Cause next function retrieves the right folder anyway
        Dim ProjectFolder As String
        ProjectFolder = getProjectFolderOf(projectNameTemplate)
        
        ' Project folder did NOT come back empty!
        If Not ProjectFolder = "" Then
            
            Dim projectName As String
            projectName = Split(ProjectFolder, "\")(UBound(Split(ProjectFolder, "\")))
                
            ' Let's ArchiveG project !
            Call ArchiveG(tbDocNumInput)
            Me.lbArchiveG.Caption = Replace(Me.lbArchiveG.Caption, "Ready", "Done")
                
        Else    ' ProjectFolder var returned empty!
        
            StatusBar = "ArchiveG_Click: Supplied doc ID not acceptable!"
            Me.lbArchiveG.Caption = Replace(Me.lbArchiveG.Caption, "Ready", "Failed")
            
        End If
        
    Else    ' ProjectNameTemplate is not GSC document compliant!
    
        StatusBar = "ArchiveG_Click: Supplied doc ID not acceptable!"
        Me.lbArchiveG.Caption = Replace(Me.lbArchiveG.Caption, "Ready", "Failed")
        
    End If

    'tbDocNumInput.Text = tbDocNumInput.Text & " EN"
        

        
End If


End Sub

Private Sub cmdAutoProcess_Click()

If tbDocNumInput.Text = "" Then
    MsgBox "Did you not forget something?"
Else
    
    If Not IsValidLangIso(Right$(tbDocNumInput.Text, 2)) Then
        tbDocNumInput.Text = tbDocNumInput.Text & " EN"
    End If
    
    ' Let's backup project
    If Is_GSC_Doc(GSC_StandardName_WithLang_toFileName(tbDocNumInput)) Then
        Call backupQ(tbDocNumInput, "autoOverwrite")
        Me.lbBackupQMessage.Caption = Replace(Me.lbBackupQMessage.Caption, "Ready", "Done")
    Else
        Me.lbBackupQMessage.Caption = Replace(Me.lbBackupQMessage.Caption, "Ready", "Failed")
    End If
    
    PauseForSeconds (0.3)
        
    ' Let's find tmx of project
    If Is_GSC_Doc(GSC_StandardName_WithLang_toFileName(tbDocNumInput)) Then
        Call HasTmx_CurrentDoc(tbDocNumInput)
        Me.lbTmxFindMessage.Caption = Replace(Me.lbTmxFindMessage.Caption, "Ready", "Done")
    Else
        Me.lbTmxFindMessage.Caption = Replace(Me.lbTmxFindMessage.Caption, "Ready", "Failed")
    End If
    
    PauseForSeconds (0.3)
    
    ' Let's Archive to G target xlf file!
    If Is_GSC_Doc(GSC_StandardName_WithLang_toFileName(tbDocNumInput)) Then
        Call ArchiveG(tbDocNumInput)
        Me.lbArchiveG.Caption = Replace(Me.lbArchiveG.Caption, "Ready", "Done")
    Else
        Me.lbArchiveG.Caption = Replace(Me.lbArchiveG.Caption, "Ready", "Failed")
    End If
    
    ' Let's open xlf preview link page in... Chrome !
    If Is_GSC_Doc(GSC_StandardName_WithLang_toFileName(tbDocNumInput)) Then
        Call Open_XLF_PreviewURL(tbDocNumInput)
        Me.lbOpenLinkMessage.Caption = Replace(Me.lbOpenLinkMessage.Caption, "Ready", "Done")
    Else
        Me.lbOpenLinkMessage.Caption = Replace(Me.lbOpenLinkMessage.Caption, "Ready", "Failed")
    End If
        
    PauseForSeconds (0.3)
        
    
    
End If


'' Let's copy to web out
'    If Is_GSC_Doc(GSC_StandardName_WithLang_toFileName(tbDocNumInput)) Then
'        Dim copyWOReturnCode As Integer
'
'        copyWOReturnCode = copyToWebOut(tbDocNumInput)
'
'        If copyWOReturnCode = 0 Then
'            Me.lbCopyWeboutMessage.Caption = Replace(Me.lbCopyWeboutMessage.Caption, "Ready", "Done")
'        Else
'            Me.lbCopyWeboutMessage.Caption = Replace(Me.lbCopyWeboutMessage.Caption, "Ready", "Failed")
'        End If
'    Else
'        Me.lbCopyWeboutMessage.Caption = Replace(Me.lbCopyWeboutMessage.Caption, "Ready", "Failed")
'        Me.lbCopyWeboutMessage.Caption = Me.lbCopyWeboutMessage.Caption & vbCr & "Please check input string above"
'    End If
'
'    PauseForSeconds (0.3)

'    ' Let's open project folder
'    If Is_GSC_Doc(GSC_StandardName_WithLang_toFileName(tbDocNumInput)) Then
'        Call openProjectFolder(tbDocNumInput)
'        Me.lbOpenPrFolderMessage.Caption = Replace(Me.lbOpenPrFolderMessage.Caption, "Ready", "Done")
'    Else
'        Me.lbOpenPrFolderMessage.Caption = Replace(Me.lbOpenPrFolderMessage.Caption, "Ready", "Failed")
'    End If



End Sub



Private Sub cmdAutoProcess_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

If KeyCode = 188 Then
    'Debug.Print KeyCode
    Call lbPrevDocID_Click
End If

End Sub

Private Sub cmdBackupQ_Click()

If tbDocNumInput.Text = "" Then
    Call backupQ
Else
    
    If Not IsValidLangIso(Right$(tbDocNumInput.Text, 2)) Then
        tbDocNumInput.Text = tbDocNumInput.Text & " EN"
    End If
    
    ' Let's backup project !
    If Is_GSC_Doc(GSC_StandardName_WithLang_toFileName(tbDocNumInput)) Then
        Call backupQ(tbDocNumInput)
        Me.lbBackupQMessage.Caption = Replace(Me.lbBackupQMessage.Caption, "Ready", "Done")
    Else
        Me.lbBackupQMessage.Caption = Replace(Me.lbBackupQMessage.Caption, "Ready", "Failed")
    End If
    
End If


End Sub


Private Sub cmdCopyWebout_Click()

If tbDocNumInput.Text = "" Then
    Call copyToWebOut
Else
    
    If Not IsValidLangIso(Right$(tbDocNumInput.Text, 2)) Then
        tbDocNumInput.Text = tbDocNumInput.Text & " EN"
    End If
    
    If Is_GSC_Doc(GSC_StandardName_WithLang_toFileName(tbDocNumInput)) Then
        Dim copyWOReturnCode As Integer
        
        copyWOReturnCode = copyToWebOut(tbDocNumInput)
        
        If copyWOReturnCode = 0 Then
            Me.lbCopyWeboutMessage.Caption = Replace(Me.lbCopyWeboutMessage.Caption, "Ready", "Done")
        Else
            Me.lbCopyWeboutMessage.Caption = Replace(Me.lbCopyWeboutMessage.Caption, "Ready", "Failed")
        End If
    Else
        Me.lbCopyWeboutMessage.Caption = Replace(Me.lbCopyWeboutMessage.Caption, "Ready", "Failed")
        Me.lbCopyWeboutMessage.Caption = Me.lbCopyWeboutMessage.Caption & vbCr & "Please check input string above"
    End If
    
End If

End Sub



Private Sub cmdFindTmx_Click()

If tbDocNumInput.Text = "" Then
    Call HasTmx_CurrentDoc
Else
    
    If Not IsValidLangIso(Right$(tbDocNumInput.Text, 2)) Then
        tbDocNumInput.Text = tbDocNumInput.Text & " EN"
    End If
    
    If Is_GSC_Doc(GSC_StandardName_WithLang_toFileName(tbDocNumInput)) Then
        Call HasTmx_CurrentDoc(tbDocNumInput)
        Me.lbTmxFindMessage.Caption = Replace(Me.lbTmxFindMessage.Caption, "Ready", "Done")
    Else
        Me.lbTmxFindMessage.Caption = Replace(Me.lbTmxFindMessage.Caption, "Ready", "Failed")
    End If
    
End If


End Sub


Private Sub cmdOpenLink_Click()

If tbDocNumInput.Text = "" Then
    Open_XLF_PreviewURL
Else    ' user entered file id in textbox (usual, default case)
    
    If Not IsValidLangIso(Right$(tbDocNumInput.Text, 2)) Then
        tbDocNumInput.Text = tbDocNumInput.Text & " EN"
    End If
    
    If Is_GSC_Doc(GSC_StandardName_WithLang_toFileName(tbDocNumInput)) Then
        
        Dim olRCode As Integer
        
        olRCode = Open_XLF_PreviewURL(tbDocNumInput)
        
        Select Case olRCode
            Case 0  ' Success
                Me.lbOpenLinkMessage.Caption = Replace(Me.lbOpenLinkMessage.Caption, "Ready", "Done")
                Me.lbOpenLinkMessage.Caption = Me.lbOpenLinkMessage.Caption & vbCr & " Operation performed successfully"
            Case 1  ' Project folder retrieval failed
                Me.lbOpenLinkMessage.Caption = Replace(Me.lbOpenLinkMessage.Caption, "Ready", "Failed")
                Me.lbOpenLinkMessage.Caption = Me.lbOpenLinkMessage.Caption & vbCr & " Could not retrieve project folder!"
            Case 2  ' Original xlf file retrieval failed
                Me.lbOpenLinkMessage.Caption = Replace(Me.lbOpenLinkMessage.Caption, "Ready", "Failed")
                Me.lbOpenLinkMessage.Caption = Me.lbOpenLinkMessage.Caption & vbCr & " Could not retrieve original xlf file!"
            Case 3  ' Copy operation verification of the original xlf did not succeed...
                Me.lbOpenLinkMessage.Caption = Replace(Me.lbOpenLinkMessage.Caption, "Ready", "Failed")
                Me.lbOpenLinkMessage.Caption = Me.lbOpenLinkMessage.Caption & vbCr & " Could not copy original xlf to temp location!"
            Case 4  ' Got empty link string from xlf file... could be a bundle xlf file, no links there
                Me.lbOpenLinkMessage.Caption = Replace(Me.lbOpenLinkMessage.Caption, "Ready", "Failed")
                Me.lbOpenLinkMessage.Caption = Me.lbOpenLinkMessage.Caption & vbCr & " XLF File is bundle, no preview URL!"
            Case 5  ' Chrome AND IE not found at expected locations error
                Me.lbOpenLinkMessage.Caption = Replace(Me.lbOpenLinkMessage.Caption, "Ready", "Failed")
                Me.lbOpenLinkMessage.Caption = Me.lbOpenLinkMessage.Caption & vbCr & " Could not retrieve original xlf file!"
            Case 6  ' Preview URL came back empty, title is not empty, but not bundle!
                Me.lbOpenLinkMessage.Caption = Replace(Me.lbOpenLinkMessage.Caption, "Ready", "Failed")
                Me.lbOpenLinkMessage.Caption = Me.lbOpenLinkMessage.Caption & vbCr & " Preview URL empty, Title not bundle!"
            Case 7  ' Preview URL and Title came back empty strings !
                Me.lbOpenLinkMessage.Caption = Replace(Me.lbOpenLinkMessage.Caption, "Ready", "Failed")
                Me.lbOpenLinkMessage.Caption = Me.lbOpenLinkMessage.Caption & vbCr & " Preview URL and TITLE are empty!"
            Case Else   ' Including error code 8, caught by error handler as unknown error
                Me.lbOpenLinkMessage.Caption = Replace(Me.lbOpenLinkMessage.Caption, "Ready", "Failed")
                Me.lbOpenLinkMessage.Caption = Me.lbOpenLinkMessage.Caption & vbCr & " Unknown error!"
        End Select
        
    Else    ' Could not convert input string into valid GSC filename with language!
        Me.lbOpenLinkMessage.Caption = Replace(Me.lbOpenLinkMessage.Caption, "Ready", "Failed")
        Me.lbOpenLinkMessage.Caption = Me.lbOpenLinkMessage.Caption & vbCr & " Check input string!"
    End If
    
End If


End Sub



Private Sub cmdOpenPrFolder_Click()

If tbDocNumInput.Text = "" Then
    Call openProjectFolder
Else
    
    If Not IsValidLangIso(Right$(tbDocNumInput.Text, 2)) Then
        tbDocNumInput.Text = tbDocNumInput.Text & " EN"
    End If
    
    If Is_GSC_Doc(GSC_StandardName_WithLang_toFileName(tbDocNumInput)) Then
        Call openProjectFolder(tbDocNumInput)
        Me.lbOpenPrFolderMessage.Caption = Replace(Me.lbOpenPrFolderMessage.Caption, "Ready", "Done")
    Else
        Me.lbOpenPrFolderMessage.Caption = Replace(Me.lbOpenPrFolderMessage.Caption, "Ready", "Failed")
    End If
    
End If


End Sub





Private Sub CommandButton4_Click()

Call lbPrevDocID_Click

End Sub

Private Sub CommandButton5_Click()

Call lbNextDocID_Click

End Sub

Private Sub cmdOpenWebOut_Click()

Call Shell("explorer.exe" & " " & "T:\TMs - Export\Web Out", vbNormalFocus)

End Sub


Private Sub Frame1_DblClick(ByVal Cancel As MSForms.ReturnBoolean)

Call Reset_All_Message_Boxes

End Sub


Sub Reset_All_Message_Boxes()

' And reset texts in all message labels

Me.lbBackupQMessage = Replace(Replace(Me.lbBackupQMessage, "Done", "Ready"), "Failed", "Ready")

Me.lbTmxFindMessage = Replace(Replace(Me.lbTmxFindMessage, "Done", "Ready"), "Failed", "Ready")

Me.lbArchiveG = Replace(Replace(Me.lbArchiveG, "Done", "Ready"), "Failed", "Ready")

Me.lbOpenLinkMessage = Replace(Replace(Me.lbOpenLinkMessage, "Done", "Ready"), "Failed", "Ready")
If InStr(1, Me.lbOpenLinkMessage, vbCr) > 1 Then    ' Also clean supplimentary message in OpenLink message label's caption
    Me.lbOpenLinkMessage = Left$(Me.lbOpenLinkMessage, InStr(1, Me.lbOpenLinkMessage, vbCr) - 1)
End If


Me.lbOpenPrFolderMessage = Replace(Replace(Me.lbOpenPrFolderMessage, "Done", "Ready"), "Failed", "Ready")

Me.lbCopyWeboutMessage = Replace(Replace(Me.lbCopyWeboutMessage, "Done", "Ready"), "Failed", "Ready")


End Sub


Private Sub lbClearInputBox_Click()

' Clear input textbox
Me.tbDocNumInput.Text = ""

Call Reset_All_Message_Boxes


End Sub


Private Sub lbClearInputBox_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.lbClearInputBox.SpecialEffect = fmSpecialEffectSunken

End Sub

Private Sub lbClearInputBox_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.lbClearInputBox.SpecialEffect = fmSpecialEffectFlat

End Sub


Private Sub lblMore_Click()

If Me.Height < 256 Then
    Me.Height = 392
    Me.lblMore.SpecialEffect = fmSpecialEffectSunken
Else    ' it's long... lets shorten it
    Me.Height = 255
    Me.lblMore.SpecialEffect = fmSpecialEffectFlat
End If

End Sub

Sub lbNextDocID_Click()

If UBound(Split(latestOpenedDocs, ",")) <> -1 Then
   
    If Me.tbDocNumInput = "" Then
   
        Me.tbDocNumInput.Text = Trim(Split(latestOpenedDocs, ",")(0))
   
    Else
        
        Dim ctIdx As Integer
        ctIdx = -1  '
        
        Dim loDocs
        
        loDocs = Split(latestOpenedDocs, ",")
        
        If UBound(loDocs) > -1 Then     ' Non empty latestOpenedDocs
            For j = 0 To UBound(loDocs)
                If Trim(loDocs(j)) = Trim(Me.tbDocNumInput.Text) Then
                    ctIdx = j
                    Exit For
                End If
            Next j
            
            If ctIdx <> -1 Then
                If UBound(loDocs) >= ctIdx + 1 Then
                    Me.tbDocNumInput.Text = loDocs(ctIdx + 1)
                Else
                End If
            End If
        Else
        End If
        
        
    End If
   
Else
End If

End Sub

Sub lbNextDocID_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.lbNextDocID.SpecialEffect = fmSpecialEffectSunken


End Sub


Private Sub lbNextDocID_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.lbNextDocID.SpecialEffect = fmSpecialEffectFlat

End Sub

Sub lbPrevDocID_Click()
 

If UBound(Split(latestOpenedDocs, ",")) <> -1 Then
   
    If Me.tbDocNumInput = "" Then
   
        Me.tbDocNumInput.Text = Trim(Split(latestOpenedDocs, ",")(UBound(Split(latestOpenedDocs, ","))))
   
    Else
        
        Dim ctIdx As Integer
        ctIdx = -1
        
        Dim loDocs
        
        loDocs = Split(latestOpenedDocs, ",")
        
        If UBound(loDocs) > -1 Then
            For j = UBound(loDocs) To 0 Step -1
                If Trim(loDocs(j)) = Trim(Me.tbDocNumInput.Text) Then
                    ctIdx = j
                    Exit For
                End If
            Next j
            
            If ctIdx > -1 Then
                If UBound(loDocs) >= 1 Then
                    If ctIdx > 0 Then Me.tbDocNumInput.Text = loDocs(ctIdx - 1)
                End If
            End If
        Else
        End If
        
        
    End If
   
Else
End If



End Sub


Private Sub lbPrevDocID_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.lbPrevDocID.SpecialEffect = fmSpecialEffectSunken

End Sub

Private Sub lbPrevDocID_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.lbPrevDocID.SpecialEffect = fmSpecialEffectFlat

End Sub


Private Sub lbReloadInputBox_Click()

If Documents.count > 0 Then
    If Is_GSC_Doc(ActiveDocument.Name) Then
        If UBound(Split(ActiveDocument.Name, ".")) >= 2 Then
            If Split(ActiveDocument.Name, ".")(2) = "txt" Then  ' XFL files are converted to text by the routine which WF employs to imports it
                
                Dim ctXLFText As String
                
                ctXLFText = ActiveDocument.Name
                
                Me.tbDocNumInput.Text = GSCFileName_ToStandardName(ctXLFText)
                Me.cmdAutoProcess.SetFocus
                
                Call Reset_All_Message_Boxes
                
                Documents(ctXLFText).Close (wdDoNotSaveChanges)
                
            End If
        End If
    End If
End If

End Sub


Private Sub tbDocNumInput_AfterUpdate()

' and reset texts in all message labels

Call Reset_All_Message_Boxes


End Sub


Private Sub UserForm_DblClick(ByVal Cancel As MSForms.ReturnBoolean)

' and reset texts in all message labels

Call Reset_All_Message_Boxes


End Sub

Private Sub UserForm_Initialize()


If Documents.count = 0 Then

    If UBound(Split(latestOpenedDocs, ",")) <> -1 Then

        Me.tbDocNumInput.Text = Split(latestOpenedDocs, ",")(UBound(Split(latestOpenedDocs, ",")))
        Me.cmdAutoProcess.SetFocus
    
    Else
    End If
    
Else   'we have a opened doc... For xlf files, since some 3 days ago, WF actually presents you with a text version of xlf file and opens that !
    
    If Is_GSC_Doc(ActiveDocument.Name) Then
        If UBound(Split(ActiveDocument.Name, ".")) >= 2 Then
            If Split(ActiveDocument.Name, ".")(2) = "txt" Then  ' xlf files are converted to text by the routine WF starts
                
                Dim ctXLFText As String
                ctXLFText = ActiveDocument.Name
                
                Me.tbDocNumInput.Text = GSCFileName_ToStandardName(ctXLFText)
                Me.cmdAutoProcess.SetFocus
                
                Documents(ctXLFText).Close (wdDoNotSaveChanges)
                
            End If
        End If
    End If
    
End If

End Sub
