Attribute VB_Name = "MouseWheelMod"
Option Explicit
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" _
   (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" _
      (ByVal hWnd As Long, ByVal nIndex As Long) As Long
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" _
      (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Declare Function SetActiveWindow& Lib "user32" (ByVal hWnd As Long)
Private Const GWL_STYLE As Long = (-16)           'The offset of a window's style
Private Const WS_SYSMENU As Long = &H80000        'Style to add a system menu
Private Const WS_MINIMIZEBOX As Long = &H20000    'Style to add a Minimize box on the title bar
Private Const WS_MAXIMIZEBOX As Long = &H10000    'Style to add a Maximize box to the title bar
'To be able to scroll with mouse wheel within Userform
Private Declare Function CallWindowProc Lib "user32.dll" Alias "CallWindowProcA" ( _
    ByVal lpPrevWndFunc As Long, ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, _
    ByVal lParam As Long) As Long

' ************************ TIMER NEEDED ************************************************************
Public Declare Function SetTimer Lib "user32" (ByVal hWnd As Long, ByVal nIDEvent As Long, _
    ByVal uElapse As Long, ByVal lpTimerFunc As Long) As Long

Public Declare Function KillTimer Lib "user32" (ByVal hWnd As Long, ByVal nIDEvent As Long) As Long

Public TimerID As Long
Public TimerSeconds As Single
'*************************** TIMER NEEDED ************************************************************

Private Const GWL_WNDPROC = -4
Private Const WM_MOUSEWHEEL = &H20A
Public LocalHwnd As Long
Public LocalPrevWndProc As Long
Public WordHwnd As Long

Public StartWidenedForm As Boolean     ' form assign, set to true before showing form to show it with opened options




'Dim myForm As MSForms.UserForm
Private Function WindowProc(ByVal Lwnd As Long, ByVal Lmsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
'To handle mouse events
Dim MouseKeys As Long
Dim Rotation As Long
If Lmsg = WM_MOUSEWHEEL Then
    MouseKeys = wParam And 65535
    Rotation = wParam / 65536
    'My Form s MouseWheel function
    frmAssign.MouseWheel Rotation
End If
WindowProc = CallWindowProc(LocalPrevWndProc, Lwnd, Lmsg, wParam, lParam)
End Function
Public Sub WheelHook(PassedFormCaption As String)
'To get mouse events in userform
On Error Resume Next
'Set myForm = PassedForm
LocalHwnd = FindWindow("ThunderDFrame", PassedFormCaption)
LocalPrevWndProc = SetWindowLong(LocalHwnd, GWL_WNDPROC, AddressOf WindowProc)
'SetActiveWindow (LocalHwnd)
Debug.Print "WheelHook: Form handle = " & LocalHwnd & "; Orig form WProc = " & LocalPrevWndProc
Debug.Print "WheelHook: WndProc now: " & GetWindowLong(LocalHwnd, GWL_WNDPROC)
Debug.Print "WheelHook: Word Hwnd: " & WordHwnd

End Sub
Public Sub WheelUnHook()
'To Release Mouse events handling
Dim WorkFlag As Long
On Error Resume Next
WorkFlag = SetWindowLong(LocalHwnd, GWL_WNDPROC, LocalPrevWndProc)
'Set myForm = Nothing

Debug.Print "WheelUnHook: Form handle = " & LocalHwnd & "; Orig form WProc = " & LocalPrevWndProc
Debug.Print "WheelUnhook: WndProc now: " & GetWindowLong(LocalHwnd, GWL_WNDPROC)

End Sub

Function QuickFocusSwitch() As Boolean

Dim switchToWord As Long
Dim switchToForm As Long

If WordHwnd <> 0 Then
    
    switchToWord = SetActiveWindow(WordHwnd)
    'PauseForSeconds (0.1)
    switchToForm = SetActiveWindow(LocalHwnd)
    
    If switchToWord <> 0 And switchToForm <> 0 Then
        QuickFocusSwitch = True
        Debug.Print "UserForm Activate: Focus Switch performed !"
    Else
        Debug.Print "UserForm Activate: Focus Switch NOT performed !"
    End If
Else
    Debug.Print "Word Hwnd was 0 ! Could not perform focus switch!"
End If

End Function

Sub TimerProc(ByVal hWnd As Long, ByVal uMsg As Long, _
ByVal nIDEvent As Long, ByVal dwTimer As Long)
    '~~> Update value in Sheet 1
    frmAssign.lblRealTime.Caption = Time
End Sub

'~~> Start Timer
Sub StartTimer()
    '~~ Set the timer for 1 second
    TimerSeconds = CLng(1000)
    TimerID = SetTimer(frmAssign.frmHwnd, 0&, TimerSeconds, AddressOf TimerProc)
    Debug.Print "StartTimer: TimerID = " & TimerID
End Sub

'~~> End Timer
Sub EndTimer()
    'On Error Resume Next
    Dim return_value As Long
    return_value = KillTimer(frmAssign.frmHwnd, TimerID)
    Debug.Print "EndTimer: KillTimer return = " & return_value
End Sub
