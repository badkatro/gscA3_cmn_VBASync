VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmgscroopt 
   OleObjectBlob   =   "frmgscroopt.frx":0000
   Caption         =   "GSC A3 Preferences"
   ClientHeight    =   6360
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   11235
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   420
End
Attribute VB_Name = "frmgscroopt"
Attribute VB_Base = "0{2E953F6F-5B95-41F4-BD2A-1E8E891CBEF8}{F71F65D9-F358-4A13-AFC1-6AF879D79F16}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False



Const EULanguageIsoCodes As String = "bg,cs,da,de,el,en,es,et,fi,fr,ga,hr,hu,it,lt,lv,mt,nl,pl,pt,ro,sk,sl,sv,xx"



Private Sub cbEnableDifferentReviewLocation_Click()


If Me.cbEnableDifferentReviewLocation.Value = False Then

    skipReviewBackupLocation.Enabled = False

    tbReviewBackupLocation.Enabled = False

    skipBrowseReviewsLabel.Enabled = False

Else
    
    skipReviewBackupLocation.Enabled = True

    tbReviewBackupLocation.Enabled = True

    skipBrowseReviewsLabel.Enabled = True
    
End If



End Sub




Private Sub cbxThemes_Change()

End Sub

Private Sub skipBrowseReviewsLabel_Click()

Dim chFdr As String

chFdr = gscrolib.GetDirectory("Please choose main folder for Bilingual review files backup")

If Not chFdr = "" Then
    Me.tbReviewBackupLocation = chFdr
End If


End Sub


Private Sub skipBrowseProjFdrLabel_Click()

Dim chFdr As String

chFdr = gscrolib.GetDirectory("Please choose main folder for SDLXliff files backup")

If Not chFdr = "" Then
    Me.tbProjFdrBackupLocation = chFdr
End If

End Sub


Private Sub skipOKBtn_Click()

Call WriteSettings_toFile

Unload Me

End Sub


Private Sub skipResetBtn_Click()

Dim gro_Sfile As New SettingsFileClass

If gro_Sfile.Exists Then
    
    Dim formSectionNames
    formSectionNames = gro_Sfile.get_SectionNames_fromForm(frmgscroopt)
    
    Dim lgU As String
    lgU = Right(GetNetworkDrive_Label("G"), 2)
    
    If lgU = "RO" Then
        Main.DontAutoSaveROSettings = True
    End If
    
    Call gro_Sfile.SetINISection(gro_Sfile.Path, IIf(Me.Tag <> "", Me.Tag, formSectionNames), "")
    
    Unload Me
    
    Load frmgscroopt
    frmgscroopt.Show
    
End If


End Sub


Private Sub UserForm_Activate()


Dim gro_Sfile As New SettingsFileClass

If gro_Sfile.Exists Then

    ' retrieve stored settings-file content (string)
    Dim StoredSettings
    
    If Me.Tag = "" Then
        Dim userformSections As String
        userformSections = gro_Sfile.get_SectionNames_fromForm(Me)
        
        StoredSettings = gro_Sfile.GetINISections(gro_Sfile.Path, userformSections, "�")
    Else
        StoredSettings = gro_Sfile.GetINISection(gro_Sfile.Path, Me.Tag, "�")
    End If
    
    ' DEBUGGING
    If IsArray(StoredSettings) Then
        Debug.Print "Got fol array of options from " & gro_Sfile.Path & ": " & vbCr & Join(StoredSettings(0), "|") & vbCr & Join(StoredSettings(1), "�")
    Else
        Debug.Print "Got fol options from " & gro_Sfile.Path & ": " & vbCr & StoredSettings
    End If
    
    If StoredSettings <> "" Then
        
        ' If we don't specifiy stored settings sep char, vbCr is assumed!
        ' One can use separator, but need check settings were returned as a separator containing string!
        If InStr(1, StoredSettings, "�") > 0 Then
            Call gro_Sfile.Update_FormOptions_fromSettingsFile(TargetUserform:=frmgscroopt, StoredSettings:=CStr(StoredSettings), StoredSettingsSeparator:="�")
        Else
            Call gro_Sfile.Update_FormOptions_fromSettingsFile(TargetUserform:=frmgscroopt, StoredSettings:=CStr(StoredSettings))
        End If
            
    Else    ' retrieved stored options are EMPTY!
    End If
    
Else    ' user settings file does NOT exist for current template, create it and store current options
    ' PROBABLY NOT NEEDED, as in case the default settings are left untouched by user, the various programs will in any case use the defaults.
    ' 2 settings are crucial: LanguageUnit and ProjFdrBackupLocation. Attempting use program which needs one of these will generate message and end execution.
End If

End Sub



Private Sub UserForm_Initialize()
' If settings file is found, set state of all controls to user desired state
' If settings file NOT found, create it and initialize to defaults
' (settings of controls at design time are the defaults)

Call PopulateLanguageCodes

Call PopulateThemes
Call SetThemeDefault    ' temporary solution, force selection of yellow for now.
' TODO: To be reviewed & corrected when theming mechanism in place

Call SetRODefaults


End Sub

Private Sub SetThemeDefault()

If Me.cbxThemes.listCount > 0 Then
    For i = 0 To Me.cbxThemes.listCount - 1
        If Me.cbxThemes.List(i) = "Yellow" Then
            Me.cbxThemes.ListIndex = i
            Exit For
        End If
    Next i
End If

End Sub

Private Sub PopulateThemes()

Dim fso As New Scripting.FileSystemObject

'C:\Users\margama\AppData\Roaming\Microsoft\Word\GSC_A3_Themes

Dim themesFolder As Scripting.Folder
Dim ctSubF As Scripting.Folder


If fso.FolderExists(Environ("appdata") & "\Microsoft\Word\GSC_A3_Themes") Then
    Set themesFolder = fso.GetFolder(Environ("appdata") & "\Microsoft\Word\GSC_A3_Themes")
    If themesFolder.SubFolders.count > 0 Then
        For Each ctSubF In themesFolder.SubFolders
            Me.cbxThemes.AddItem (ctSubF.Name)
        Next ctSubF
    End If
End If


End Sub


Private Sub PopulateLanguageCodes()

Dim EULanguageCodes As Variant

EULanguageCodes = Split(EULanguageIsoCodes, ",")

For i = 0 To UBound(EULanguageCodes)
    cbxLanguageIso.AddItem (UCase(EULanguageCodes(i)))
Next i


End Sub

Sub SetRODefaults()

If Main.DontAutoSaveROSettings = True Then Exit Sub

Dim gLabel As String
gLabel = GetNetworkDrive_Label("G")     ' If in DGA3, network drive G should be set to "LANGRO" or "LANGEN" or some such, with last 2 letters being ISO language code!

Dim pLgU As String
pLgU = Right(gLabel, 2)

If IsValidLangIso(pLgU) Then
    
    If pLgU = "RO" Then     ' AUTO set some important settings for RO colleagues, home comes first...
        
        If languageUnitNotSet Then
            
            frmgscroopt.cbxLanguageIso.Value = "RO"     ' Set language unit to RO
            frmgscroopt.tbProjFdrBackupLocation = "Q:\01-TRADUCERI"
                    
            Call WriteSettings_toFile   ' UNJUSTLY write defaults to settigns file for RO unit, we know the settings...
            
        End If
        
    End If
    
End If


End Sub

Function languageUnitNotSet() As Boolean

Dim sFile As New SettingsFileClass

If Not sFile.Exists Then
    
    languageUnitNotSet = True
    Exit Function
    
Else
    
    Dim stLgU As String
    stLgU = sFile.getOption("LanguageUnit", "GSC_A3_Options")
    
    If Not IsValidLangIso(stLgU) Then
        languageUnitNotSet = True
        Exit Function
    Else    ' we did get a language unit ISO
        If sUserName <> "margama" Then  ' developer loophole
        End If
    End If
    
End If

End Function


Sub WriteSettings_toFile()


Dim gro_Sfile As New SettingsFileClass

If Not gro_Sfile.Exists Then
    gro_Sfile.Create_Empty
End If

Dim newUserOptions As String
newUserOptions = gro_Sfile.txtGetFormOptions(frmgscroopt)

Dim newSettingsWritten As Boolean
'newSettingsWritten = gro_SFile.setSection(frmgscroopt.Tag, newUserOptions, "WriteNewSection")

Call gro_Sfile.SetINISection(gro_Sfile.Path, frmgscroopt.Tag, newUserOptions)


End Sub



Private Sub skipCancelBtn_Click()
' Cancel button (Might have to also try closing file stream of settings file ?)

Main.DontAutoSaveROSettings = False     ' Let frmgscopts form re-do its autu magic settings for RO unit...
Unload Me

End Sub

'****************************************************************************


















































'***************************************************************************
