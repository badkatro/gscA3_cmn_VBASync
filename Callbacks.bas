Attribute VB_Name = "Callbacks"
' ac, ad, bu, cg, cm, cp, da, de, ds, eg, lt, nc, np, re, rs, sn, st, xm, xt
Public Const NtkFolder As String = "Q:\10-TOOLS\NtkOutTest"                         '"T:\TMs - Export\Ntk Out"
Public Const Default_NonGSC_BackupFolder As String = "Q:\28-NonGSC-TMXArchive"      ' Bad Tmx Counter, moves tmxs into this folder
Public Const smallerBigTmx As Long = 409600 ' 699 kilobytes in bytes

Sub injectInitialsRun(control as IRibbonControl)
        
        Call DW_Inject_Initials
        
End Sub

Sub createMoretusRun(control as IRibbonControl)

        Call Master_CreateMoretus

End Sub

Sub insertEPDocRun(control as IRibbonControl)

        Call Insert_EPDoc

End Sub

'Callback for gscA3SettingsBt onAction
Sub gscA3SettingsRun(control As IRibbonControl)
    
    frmgscroopt.Show
    
End Sub

' Callback function used to retrieve icon of Bad Tmx Main Counter
Sub getTBTmxImage(control As IRibbonControl, ByRef image)


Dim ctUser As String
ctUser = sUserName

Dim sbMess As String

Dim iTotal_Bad_Alignments As Integer
iTotal_Bad_Alignments = count_Users_Bad_Tmxs(ctUser, sbMess)    ' can only return between 0 and 21, since it's gonna be used to compose the name
' of a picture to display on the ribbon, we only have 21 pictures in total for every font ! (21 being double exclamation point!)

StatusBar = sbMess


Dim whichFontSaved As String        ' to retrieve which font user saved in settings file
Dim whatTimerSaved As String        ' to retrieve value of timer saved by user

Dim sFile As New SettingsFileClass

If sFile.Exists Then
    If Not sFile.IsEmpty Then
        
        whichFontSaved = sFile.getOption("MainTimerFont", "BadTmxOptions")
        whatTimerSaved = sFile.getOption("MainTimerInterval", "BadTmxOptions")
        
        If whichFontSaved <> "" Then
        Else
            whichFontSaved = "Forte"    ' Default font
        End If
        
        If whatTimerSaved <> "" Then
        Else
            whatTimerSaved = "30"       ' Default timer, 30 minutes
        End If
        
    Else    ' settings file empty
        whichFontSaved = "Forte"    ' Default font
        whatTimerSaved = "30"       ' Default timer, 30 minutes
    End If
Else    ' settings file does not exist
    whichFontSaved = "Forte"    ' Default font
    whatTimerSaved = "30"       ' Default timer, 30 minutes
End If


Dim numPics_BaseFolder As String
numPics_BaseFolder = Environ("Appdata") & "\Microsoft\Word\BadTmxCounter_Fonts" & "\" & whichFontSaved    ' need use function to get option from conf file, where user saves it !


Dim ctCount_PicturePath As String
ctCount_PicturePath = numPics_BaseFolder & "\Num_" & CStr(iTotal_Bad_Alignments) & "S.gif"


If Dir(ctCount_PicturePath) <> "" Then
    Set image = LoadPicture(ctCount_PicturePath)
Else
    Set image = LoadPicture(numPics_BaseFolder & "\Num_21S.gif")
End If


StatusBar = sbMess & "Set new timer for " & Now + TimeValue("0:" & whatTimerSaved & ":00") & "..."          ' need write function to write/ retrieve option from config file
Application.OnTime Now + TimeValue("0:" & whatTimerSaved & ":00"), "RefreshRibbon"                          ' same here, with timer value!


End Sub


' Callback for open tmx exports/ alignments folder
Public Sub btmxFolderOpenRun(control As IRibbonControl)

' TODO: Modify to find out if bad alignments and/ or exports were identified for current user
' and open, in response, exports folder (as below), or bad alignments folder, or... why not, both !


Dim sFile As New SettingsFileClass

Dim dontShowAnnoyingMessages As String


If sFile.Exists Then
    
    If Not sFile.IsEmpty Then
        
        dontShowAnnoyingMessages = sFile.getOption("OpenTmxFolder_ShowMsgbox", "BadTmxOptions")
        
        ' Thinkin about overwriting it with default behaviour... SHOW messages !
        If dontShowAnnoyingMessages = "" Then
            dontShowAnnoyingMessages = "true"
        End If
        
    Else
        dontShowAnnoyingMessages = "true"
    End If
    
Else
    dontShowAnnoyingMessages = "true"
End If


If Dir(tmxBadAlignmentsFolder_onT & "*" & sUserName & "*") <> "" Or _
    Dir(tmxAlignmentsFolder_onT & "*" & sUserName & "*") <> "" Then
        Call Shell("explorer.exe " & tmxBadAlignmentsFolder_onT, vbNormalFocus)
ElseIf Dir(tmxBadExportsFolder_onT & "*" & sUserName & "*") <> "" Or _
    Dir(tmxExportsFolder_onT & "*" & sUserName & "*") <> "" Then
        Call Shell("explorer.exe " & tmxBadExportsFolder_onT, vbNormalFocus)
Else
    
    Dim resp As VbMsgBoxResult
    
    ' The options is selected - value is true, but option means "Don't show messages..."!
    If dontShowAnnoyingMessages = "True" Then
        resp = vbYes
    Else
        resp = MsgBox("You have NO exports or alignments of the BAD kind!" & vbCrCr & "Do you wish to open exports folder in any case?", vbYesNoCancel, "Why, Oh, why?")
    End If
    
    
    Select Case resp
        
        Case vbYes
            Call Shell("explorer.exe " & tmxBadExportsFolder_onT, vbNormalFocus)
        Case vbNo
            If MsgBox("How about the alignments folder then?", vbYesNo, "Why, Oh, why") = vbYes Then
                Call Shell("explorer.exe " & tmxBadAlignmentsFolder_onT, vbNormalFocus)
            End If
        Case Else
        
    End Select
            
End If

End Sub

' Callback routine for button to display clear HEC user form
Public Sub ShowClearHECForm(control As IRibbonControl)


frmClearHEC.Show


End Sub


' Callback routine for button to change autopilot state of Space Replace
Public Sub getSpRpAutopilotLabel(control As IRibbonControl, ByRef label)


Dim settingsFile As New SettingsFileClass

If Not settingsFile.Exists Then
    
    settingsFile.Create_Empty
    
    settingsFile.setOption "SpaceReplaceOptions", "AutoPilotState", "off"
    
    label = "Turn Autopilot ON"
    
Else    ' settings file exists
    
    If settingsFile.IsEmpty Then
        
        settingsFile.setOption "SpaceReplaceOptions", "AutoPilotState", "off"
        
    Else    ' settings file exists and is not empty
        
        Dim storedAPState As String
        ' retrieve stored option for AP
        storedAPState = settingsFile.getOption("AutoPilotState", "SpaceReplaceOptions")
        
        ' if it's not found
        If storedAPState = "" Then
            ' set default option (off)
            settingsFile.setOption "SpaceReplaceOptions", "AutoPilotState", "off"
            label = "Turn Autopilot ON"
            
        Else    ' settings file found, not empty, label state retrieved
            
            Select Case storedAPState
                Case "on"
                    label = "Turn Autopilot OFF"
                Case "off"
                    label = "Turn Autopilot ON"
            End Select
            
        End If
    End If
    
End If
    

End Sub

'Callback for webOWeboBt onAction
Sub OpenWeboutFolder_Run(control As IRibbonControl)

Call Shell("explorer.exe" & " " & "T:\TMs - Export\Web Out", vbNormalFocus)

End Sub

Sub copyDWCodesRun(control As IRibbonControl)

Call Copy_DWSubjCodes_toClipboard

End Sub


'Callback for webHistBt onAction
Sub webHistoryOpenRun(control As IRibbonControl)

StatusBar = "Not yet available, but it'll worth the wait..."

End Sub

'Callback for spcRepAutopilotBt onAction
Sub FlipSpRpAutopilot(control As IRibbonControl)

Call FlipSpRpAutopilot_Worker(control)
'Application.StatusBar = "Will be available in near future, Patience.."

End Sub

'Callback for alignFdrOpBt onAction
Sub OpenAlignmentFdr_Run(control As IRibbonControl)

Call Open_Alignments_Folder

End Sub

'Callback for weboutFdrOpBt onAction
Sub OpenWebout_Run(control As IRibbonControl)

Call Shell("explorer.exe" & " " & "T:\TMs - Export\Web Out", vbNormalFocus)


End Sub

'Callback for comdocFdrOpBt onAction
Sub OpenComDocFdr_Run(control As IRibbonControl)

StatusBar = "Soon, friend, soon..."

End Sub


'Callback for parensNumRemBt onAction
Sub removeParensRun(control As IRibbonControl)

'StatusBar = "It is available, just give it a few days..."
Call RemoveParens_FromSelection

End Sub


Sub btmxSettingsRun(control As IRibbonControl)

formBTmxOpt.Show

End Sub

'Callback for copyFootersBt onAction
Sub copyFootersRun(control As IRibbonControl)

frm_CopieFootere.Show

End Sub

'Callback for spellchkDoneBt onAction
Sub spellchkDoneRun(control As IRibbonControl)

Call Spellchecking_Done

End Sub

'Callback for leftAlignBt onAction
Sub JustifiedToLeftRun(control As IRibbonControl)

Dim tgRng As Range

Set tgRng = ActiveDocument.StoryRanges(wdMainTextStory)
'tgRng.Collapse (wdCollapseStart)

Dim didFootnotes As Boolean

Dim sbMessg As String
sbMessg = "Justified to left: "

Dim countOccurences As Integer

AndAgain:
With tgRng.Find
    
    .ClearFormatting
    .Wrap = wdFindStop
    .Forward = True
    .Format = True
    
    .ParagraphFormat.Alignment = wdAlignParagraphJustify
    .Replacement.ParagraphFormat.Alignment = wdAlignParagraphLeft
    
    .Execute Replace:=wdReplaceOne
    
    Do While .found = True
        tgRng.Collapse (wdCollapseEnd)
        .Execute Replace:=wdReplaceOne
        countOccurences = countOccurences + 1
    Loop
    
    '
    If countOccurences > 0 Then
        
        sbMessg = IIf(InStr(1, sbMessg, "occurences") > 0, sbMessg & " | Left-aligned " & countOccurences & " occurences (fn)", _
            sbMessg & "Left-aligned " & countOccurences & " occurences (main)")
        
    Else
        
        sbMessg = IIf(InStr(1, sbMessg, "occurences") > 0, sbMessg & " | Found 0 occurences (fn)", _
            sbMessg & "Found 0 occurences (main)")
        
    End If
    
    StatusBar = sbMessg
    
    '
    If ActiveDocument.Footnotes.count > 0 And Not didFootnotes Then
        Set tgRng = ActiveDocument.StoryRanges(wdFootnotesStory)
        didFootnotes = True
        countOccurences = 0     ' reset counter
        
        GoTo AndAgain
    End If
    
    
    
End With


End Sub

' callback for No Indents for Centered
Sub RemoveIndentsRun(control As IRibbonControl)

Dim tgRng As Range

Set tgRng = ActiveDocument.StoryRanges(wdMainTextStory)
'tgRng.Collapse (wdCollapseStart)

' If last paragraph is empty, leave it out !
tgRng.MoveEndWhile vbCr & Chr(12), wdBackward
tgRng.MoveStartWhile Chr(1) & Chr(13), wdForward


Dim sRng As Range
Set sRng = tgRng.Duplicate


Dim sbMessg As String
sbMessg = "Remove Indents: "

Dim countOccurences As Integer

AndAgain:
With sRng.Find
    
    .ClearFormatting
    .Wrap = wdFindStop
    .Forward = True
    .Format = True
    
    .ParagraphFormat.Alignment = wdAlignParagraphCenter
    
    .Replacement.ParagraphFormat.Alignment = wdAlignParagraphCenter
    
    .Replacement.ParagraphFormat.LeftIndent = 0
    .Replacement.ParagraphFormat.RightIndent = 0
    .Replacement.ParagraphFormat.FirstLineIndent = 0
    '.Replacement.ParagraphFormat.HangingPunctuation
    
    .Execute Replace:=wdReplaceNone
    
    Do While .found = True And sRng.InRange(tgRng)
        
        If (sRng.Paragraphs(1).LeftIndent > 0 And sRng.Paragraphs(1).LeftIndent < 860) Or _
            (sRng.Paragraphs(1).RightIndent > 0 And sRng.Paragraphs(1).RightIndent < 860) Or _
            (sRng.Paragraphs(1).FirstLineIndent > 0 And sRng.Paragraphs(1).FirstLineIndent < 860) Then
            
            'sRng.Select
            
            'tgRng.Select
            
            '.Execute Replace:=wdReplaceOne
            sRng.Paragraphs(1).Range.ParagraphFormat.LeftIndent = 0
            sRng.Paragraphs(1).Range.ParagraphFormat.RightIndent = 0
            sRng.Paragraphs(1).Range.ParagraphFormat.FirstLineIndent = 0
            countOccurences = countOccurences + 1
            
            sRng.Collapse (wdCollapseEnd)
            Set tgRng = ActiveDocument.Range(sRng.Start, tgRng.End)
            
        Else
            sRng.Collapse (wdCollapseEnd)
            .Execute
        End If
    Loop
    
    
    ' Report occurences count
    If countOccurences > 0 Then
        sbMessg = sbMessg & " | Removed indents from " & countOccurences & " occurences"
    Else
        sbMessg = sbMessg & " | Found 0 occurences"
    End If
    
    StatusBar = sbMessg
    
End With


End Sub


'Callback for footnotesRefBt onAction
Sub RepairFootnotesRun(control As IRibbonControl)

StatusBar = "Badly needed, I know... Will be one day..."

End Sub

'Callback for tmxCheckerBt onAction
Sub tmxCheckerRun(control As IRibbonControl)

Call HasTmx_CurrentDoc

End Sub

'Callback for hasAlignBt onAction
Sub HasAlignmentsRun(control As IRibbonControl)

Call CurrentDocument_hasAlignments

End Sub

Sub lblTBTmxRun(control As IRibbonControl)

Call RefreshRibbon

End Sub


Sub OpenXLFPreviewURL_Run(control As IRibbonControl)

Dim res As Integer

res = Open_XLF_PreviewURL


End Sub


Sub XLFAutoProcessRun(control As IRibbonControl)

frmXLFAutoProcess.Show

End Sub


' Callback for openFinalBt onAction
Sub openFinalRun(control As IRibbonControl)

Call openFinal

End Sub

'Callback for backupQBt onAction
Sub backupQRun(control As IRibbonControl)

Call backupQ

End Sub

'Callback for archiveGBt onAction
Sub archiveGRun(control As IRibbonControl)

Call ArchiveG

End Sub

' Callback for multiAlignBt onAction
Sub multiAlignmentRun(control As IRibbonControl)

Call Ruleaza_AlinieriM

End Sub

' Callback for RO Diacritics correct
Sub roDiacriticsCorrectRun(control As IRibbonControl)

Call Wrong_ToRightDiacritics_Replace

End Sub

' Callback for Launch Automate
Sub launchAutomateRun(control As IRibbonControl)

Call Launch_or_Switch_to_CARSAutomate

End Sub

' Callback for Close and Open in Automate
Sub openInAutomateRun(control As IRibbonControl)

Call OpenDoc_With_CARSAutomate

End Sub

' Callback for SplitDoc
Sub SplitDocRun(control As IRibbonControl)

DocSplit.Show


End Sub

'Callback for multiAlignBt onAction
Sub multiAlignmentsRun(control As IRibbonControl)

Call Ruleaza_AlinieriM

End Sub

'Callback for openPFBt onAction
Sub openProjectFolderRun(control As IRibbonControl)

Call openProjectFolder

End Sub

Sub getComDocRun(control As IRibbonControl)

frmGetComDoc.Show

End Sub

Sub convertNumberingRun(control As IRibbonControl)

Call AutoNumbers_ToManualNumbers_Convert

End Sub

Sub spaceReplaceRun(control As IRibbonControl)

Call Start_SpaceReplace

End Sub

Sub numberingToParensRun(control As IRibbonControl)

Dim withPagebreakConversion

withPagebreakConversion = MsgBox("Do you wish to also convert auto numbering to manual?", vbYesNo, "Answer please")


If withPagebreakConversion = vbYes Then
    Call Paranthesize_Paragraphs_Numbers
Else
    Call Paranthesize_Paragraphs_Numbers("NoPageBreakConversion")
End If

End Sub

' Callback for bfptmxBt (False positives button in Bad Tmx Checker group)
Sub bfptmxBtRun(control As IRibbonControl)
    
    Dim ctUser As String
    ctUser = sUserName
    
    Dim falsePositives_Tmxs As foundFilesStruct
    falsePositives_Tmxs = getUsers_falsePositives_Tmxs(ctUser)
    
    If falsePositives_Tmxs.count > 0 Then
        Call Backup_Alignments_fromArray(tmxBadAlignmentsFolder_onT, falsePositives_Tmxs.files)
    End If
    
    Dim falsePositives_Logs As foundFilesStruct
    falsePositives_Logs = getUsers_falsePositives_TxtLogs(ctUser)
    
    If falsePositives_Logs.count > 0 Then
        Call Delete_AllFiles_inArray(tmxBadAlignmentsFolder_onT, falsePositives_Logs.files)
    End If
    
    Call RefreshRibbon
    
End Sub

Function getUsers_falsePositives_Tmxs(User As String) As foundFilesStruct

Dim tmpTmxs As foundFilesStruct

tmpTmxs = bGDirAll(tmxBadAlignmentsFolder_onT & "SAVEPE*" & User & "*CL*BADTMX" & "|" & _
            tmxBadAlignmentsFolder_onT & "SAVEPE*" & User & "*P7*BADTMX" & "|" & _
            tmxBadAlignmentsFolder_onT & "SAVEPE*" & User & "*P8*BADTMX" & "|" & _
            tmxBadAlignmentsFolder_onT & "SAVEPE*" & User & "*CouncilMemory_1*BADTMX" & "|" & _
            tmxBadAlignmentsFolder_onT & "SAVEPE*" & User & "*CouncilMemory_2*BADTMX" & "|" & _
            tmxBadAlignmentsFolder_onT & "SAVEPE*" & User & "*CouncilMemory_3*BADTMX" & "|" & _
            tmxBadAlignmentsFolder_onT & "SAVEPE*" & User & "*CouncilMemory_6*BADTMX")

getUsers_falsePositives_Tmxs = tmpTmxs
    

End Function


Function getUsers_falsePositives_TxtLogs(User As String) As foundFilesStruct
' separately retrieve these orphaned logs (since their tmxs, we move em), so as to delete'em

Dim tmpTxtLogs As foundFilesStruct

tmpTxtLogs = bGDirAll(tmxBadAlignmentsFolder_onT & "SAVEPE*" & User & "*CL*txt" & "|" & _
            tmxBadAlignmentsFolder_onT & "SAVEPE*" & User & "*P7*txt" & "|" & _
            tmxBadAlignmentsFolder_onT & "SAVEPE*" & User & "*P8*txt" & "|" & _
            tmxBadAlignmentsFolder_onT & "SAVEPE*" & User & "*CouncilMemory_1*txt" & "|" & _
            tmxBadAlignmentsFolder_onT & "SAVEPE*" & User & "*CouncilMemory_2*txt" & "|" & _
            tmxBadAlignmentsFolder_onT & "SAVEPE*" & User & "*CouncilMemory_3*txt" & "|" & _
            tmxBadAlignmentsFolder_onT & "SAVEPE*" & User & "*CouncilMemory_6*txt")

getUsers_falsePositives_TxtLogs = tmpTxtLogs


End Function


Function getUsers_GSCDocs_BadTmxs(User As String) As foundFilesStruct

' "ac, ad, bu, cg, cm, cp, da, de, ds, eg, lt, nc, np, re, rs, sn, st, xm, xt"


Dim tmpTmxs As foundFilesStruct

 tmpTmxs = bGDirAll(tmxBadAlignmentsFolder_onT & "*" & User & "*_sn*BADTMX" & "|" & _
                    tmxBadAlignmentsFolder_onT & "*" & User & "*_st*BADTMX" & "|" & _
                    tmxBadAlignmentsFolder_onT & "*" & User & "*_cm*BADTMX" & "|" & _
                    tmxBadAlignmentsFolder_onT & "*" & User & "*_xm*BADTMX" & "|" & _
                    tmxBadAlignmentsFolder_onT & "*" & User & "*_bu*BADTMX" & "|" & _
                    tmxBadAlignmentsFolder_onT & "*" & User & "*_ad*BADTMX" & "|" & _
                    tmxBadAlignmentsFolder_onT & "*" & User & "*_xt*BADTMX")
    
getUsers_GSCDocs_BadTmxs = tmpTmxs

End Function

' Callback for bptmxBt (To be fixed tmxs button in Bad Tmx Checker group)
Sub bptmxBtRun(control As IRibbonControl)
    StatusBar = "TmxChecker: To-be-fixed tmxs action button not implemented yet..."
End Sub

' Callback for X (Brexit) tmxs in Bad Tmx Checker group
Sub bxtmxBtRun(control As IRibbonControl)
    
    'StatusBar = "TmxChecker: X tmxs action button not implemented yet..."
    
    Dim ctUser As String
    ctUser = sUserName

    Dim sbMess As String

    Dim ffs_X_Alignments As foundFilesStruct
    ffs_X_Alignments = getUsers_XAlignments(ctUser)
    
    Dim fso As New Scripting.FileSystemObject
    
    If ffs_X_Alignments.count > 0 Then
        
        For i = 0 To ffs_X_Alignments.count - 1
            
            Dim newXFilename As String
            
            Dim px As Integer
            Dim fn As String
            
            fn = ffs_X_Alignments.files(i)
            
            ' Finding the position of xm or xt in filename
            If InStr(1, fn, "_xm") > 0 Then
                px = InStr(1, fn, "_xm")
            ElseIf InStr(1, fn, "_xt") > 0 Then
                px = InStr(1, fn, "_xt")
            End If
            
            ' make necessary manipulations to properly rename file
            newXFilename = Mid$(fn, px + 1, Len(fn) - px)
            newXFilename = Replace(newXFilename, "Wholedoc.", "")
            newXFilename = Replace(newXFilename, "BADTMX", "")
            
            Dim failedCopyFiles As String
            
            If fso.FileExists(NtkFolder & "\" & newXFilename) Then
                
                If MsgBox("File " & NtkFolder & "\" & newXFilename & " already exists, Overwrite?", vbYesNo + vbQuestion, _
                    "Tmx file already exists") = vbNo Then
                    
                    GoTo SkipToNext
                    
                End If
                
            End If
            
            On Error Resume Next
            
            ' lets copy file
            fso.CopyFile tmxBadExportsFolder_onT & "\" & fn, NtkFolder & "\" & newXFilename, True
            
            ' copying files is dangerous... lets ward off
            If Err.Number <> 0 Then
                Debug.Print "Error " & Err.Number & " meaning " & Err.Description & _
                    " occ in " & Err.Source
                failedCopyFiles = IIf(failedCopyFiles = "", newXFilename, failedCopyFiles & "," & newXFilename)
                Err.Clear
            End If

SkipToNext:
        Next i
        
        ' Final message to user in case of trouble
        If failedCopyFiles <> "" Then
            MsgBox "Failed to copy files: " & failedCopyFiles & vbCr & vbCr & "Pleasecheck and try manually", vbOKOnly, _
                "Brexit X* Copy Errors"
        Else
            Call Delete_AllFiles_inArray(tmxBadAlignmentsFolder_onT, ffs_X_Alignments.files)
            Call RefreshRibbon
            StatusBar = "Count Tmxs: " & ffs_X_Alignments.count & " X* files processed"
        End If
        
        
    Else    ' Nothing to copy, no X files for this user
        StatusBar = "Count Tmxs: NO X* tmxs..."
    End If
    
    Set fso = Nothing
    
End Sub


Public Sub Backup_Alignments_fromArray(hostFolder As String, FileNames() As String)

    Dim sFile As New SettingsFileClass
    
    If sFile.Exists Then
        
        If Not (sFile.IsEmpty = ErrorFindingFile Or sFile.IsEmpty = FileEmpty) Then
            
            Dim ctBckFolder As String
            ctBckFolder = sFile.getOption("NonGscAlignmentsBackupFolder", "BadTmxOptions")
            
            ' Before first start of BadTmx manager's settings, we have no such saved setting. Use default!
            If ctBckFolder <> "" Then
                
            Else
                ctBckFolder = Default_NonGSC_BackupFolder
            End If
            
            
        Else    ' settings file empty or not unfindable
            ctBckFolder = Default_NonGSC_BackupFolder
        End If
        
    Else    ' settings file does not exist
        ctBckFolder = Default_NonGSC_BackupFolder
    End If
    
    Dim fso As New Scripting.FileSystemObject
    
    ' WHAT if, before setting the bad tmx nongsc backup folder, the default one does not exist?
    If Not fso.FolderExists(ctBckFolder) Then
        Dim userFolderChoice
        userFolderChoice = MsgBox("Default NON-GSC alignments backup folder does NOT exist! (" & ctBckFolder & ")" & vbCr & vbCr & _
            "Please click No to exit this form and create/ set custom named folder or click Yes to create default folder", vbYesNoCancel + vbInformation, "Default Non-Gsc alignments folder missing!")
           
        If userFolderChoice = vbYes Then
            fso.CreateFolder (ctBckFolder)
        ElseIf userFolderChoice = vbNo Or userFolderChoice = vbCancel Then
            StatusBar = "BAD Tmx Counter: ABORTED, Exiting..."
            Exit Sub
        End If
    End If
    
               
    If fso.FolderExists(hostFolder) Then
        
        Dim k As Integer    ' counter of files backedup
        
        For i = 0 To UBound(FileNames)
            
            If fso.FileExists(hostFolder & FileNames(i)) Then
                
                If fso.GetFile(hostFolder & FileNames(i)).Size > smallerBigTmx Then
                    Let k = k + 1   ' count em
                    
                    Dim newTmxFileName As String
                    
                    newTmxFileName = Split(FileNames(i), "_")(2)
                        
                        ' eliminate first, second and third parts separated by "_", NOT part of tmx filename
                    newTmxFileName = Replace(newTmxFileName, "BADTMX", "")    ' remove dead extension
                    
                    fso.CopyFile hostFolder & FileNames(i), ctBckFolder & "\" & newTmxFileName, True
                    DoEvents
                    
                End If
                
                fso.DeleteFile (hostFolder & FileNames(i))
                DoEvents
                
            End If
            
        Next i
        
    End If
    
    StatusBar = "Backed-up " & k & " BTmxs for " & sUserName
    
    Call PauseForSeconds(1)
    
    Set fso = Nothing
    Set sFile = Nothing

End Sub

Public Sub Delete_AllFiles_inArray(hostFolder As String, FileNames() As String)
    
    Dim fso As New Scripting.FileSystemObject
    
    If fso.FolderExists(hostFolder) Then
        For i = 0 To UBound(FileNames)
            If fso.FileExists(hostFolder & "\" & FileNames(i)) Then
                
                fso.DeleteFile (hostFolder & "\" & FileNames(i))
            End If
        Next i
    End If
    
    Set fso = Nothing
End Sub


Sub RawTmxFind()

Load frmRawTmxFinder

frmRawTmxFinder.Show

End Sub


Public Sub getBpTmxLabel(control As IRibbonControl, ByRef label)

Dim ctUser As String
ctUser = sUserName

Dim toBeFixedTmxNo As Integer

Dim sbMessage As String

toBeFixedTmxNo = getUsers_GSCDocs_BadTmxs(ctUser).count

label = toBeFixedTmxNo & "  Council (GSC) tmxs"


End Sub


Public Sub getBfpTmxLabel(control As IRibbonControl, ByRef label)

Dim ctUser As String
ctUser = sUserName

Dim noFalsePositives As Integer

noFalsePositives = getUsers_falsePositives_Tmxs(ctUser).count

label = noFalsePositives & "  Non-GSC tmxs"

End Sub


' Callback routine for getting label of X tmxs counter
Public Sub getBxTmxLabel(control As IRibbonControl, ByRef label)

Dim ctUser As String
ctUser = sUserName

Dim iTotal_X_Alignments As Integer
iTotal_X_Alignments = countUsers_XAlignments(ctUser)     '

label = iTotal_X_Alignments & "  DEPRECATED! (Brexit)"

End Sub

' Callback routine for Multiple Backup Checker
Public Sub masterBackupChecker()

frmBackupChecker.Show

End Sub


Public Sub masterProjectFoldersPruner()

frmProjectFoldersPruner.Show

End Sub
