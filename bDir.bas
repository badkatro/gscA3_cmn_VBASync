Attribute VB_Name = "bDir"
Option Compare Text

Private Const FILE_ATTRIBUTE_DIRECTORY As Integer = 16

Private Const MAX_PATH As Long = 260
Private SourceAddress As Long           ' To store the address of search handle returned from WIN API functions FindFirstFile and FindNextFile
Private LastSearchMask As String        ' To store the user supplied search mask for the above functions
Private ExclusionMask As Variant        ' (bDir) To store file exclusion mask after first bDir call (with parameter), cause subsequent calls are without parameter !

'Declare Function GlobalAlloc& Lib "kernel32" (ByVal wFlags As Long, ByVal dwBytes As Long)
'Declare Function GlobalUnlock& Lib "kernel32" (ByVal hMem As Long)
'Declare Function GlobalFree& Lib "kernel32" (ByVal hMem As Long)

Private Declare Function FindFirstFile& Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA)
Private Declare Function FindNextFile& Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA)
Private Declare Function FindClose& Lib "kernel32" (ByVal hFindFile As Long)

'Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (hpvDest As Any, hpvSource As Any, ByVal cbCopy As Long)

Public Type foundFilesStruct
    count As Integer
    files() As String
    sizeInKb As Long
End Type

Public Type foundFoldersStruct
    count As Integer
    folders() As String
End Type

Private Type FILETIME
    dwLowDateTime As Long
    dwHighDateTime As Long
End Type

Private Type WIN32_FIND_DATA
    dwFileAttributes As Long
    ftCreationTime As FILETIME
    ftLastAccessTime As FILETIME
    ftLastWriteTime As FILETIME
    nFileSizeHigh As Long
    nFileSizeLow As Long
    dwReserved0 As Long
    dwReserved1 As Long
    cFileName As String * MAX_PATH
    cAlternate As String * 14
End Type

Public Function bDir(Optional maskString, Optional listFolders, Optional returnFullPaths) As String
' Just like Dir function from Windows, function attempts to find and return the name of folder(s) and
' files complying to supplied mask. Also added possibility to supply file exclusion masks at the end of file search mask
' ie: bDir("*.doc<>1*.doc<>2*.doc") to return doc files, excluding those starting with 1 or 2

    Dim baseFolder As String, fileMask As String
    
    If Not IsMissing(maskString) Then    ' called with file mask
            
        ' if user supplied a file exclusion mask as well, save it for future use (subsequent calls of bDir)
        If InStr(1, maskString, "<>") > 0 Then
            ExclusionMask = Split(Right(maskString, Len(maskString) - InStr(maskString, "<>") - 1), "<>")
            maskString = Split(maskString, "<>")(0)     ' only take file search mask, exclusion mask apart
        End If
            
        ' only if maskString is present can we do this
        baseFolder = Left(maskString, InStrRev(maskString, "\") - 1)
            
        If Not IsMissing(listFolders) Then
        
            Dim firstFoundFolder As String
            
            firstFoundFolder = bGetFirstFolder(CStr(maskString))
            
            If IsArray(ExclusionMask) And SourceAddress <> 0 Then   ' we've got to be in the case: more than the first calling (just above line) & exclusion mask present
                Do While LikeAny(firstFoundFolder, ExclusionMask)
                    firstFoundFolder = bGetNextFolder(SourceAddress)
                Loop
            End If
            
            bDir = firstFoundFolder
            
            ' just tacking on the requested folder if need
            If Not bDir = "" And Not IsMissing(ReturnFullPaths) Then
                bDir = baseFolder & "\" & bDir
            End If
            
        Else    ' User wishes files
            
            Dim firstFoundFile As String
            firstFoundFile = bGetFirstFile(CStr(maskString))
            
            ' we HAVE to call GetFirstFile at least once, but we loop if unsatisfactory result!
            If IsArray(ExclusionMask) And SourceAddress <> 0 Then
                Do While LikeAny(firstFoundFile, ExclusionMask)
                    firstFoundFile = bGetNextFile(SourceAddress)
                Loop
            End If
                
            bDir = firstFoundFile
            
            ' adding requested folder at the end, theres lots a work done before!
            If Not bDir = "" And Not IsMissing(returnFullPaths) Then
                bDir = baseFolder & "\" & bDir
            End If
            
        End If
        
    Else    ' This is second or more callin, askin' for more files/ folders merely
        
        baseFolder = Left(LastSearchMask, InStrRev(LastSearchMask, "\") - 1)
        
        If Not IsMissing(listFolders) Then  ' user asks for folders
            
            If SourceAddress <> 0 Then
                
                Dim nextFoundFolder As String
                nextFoundFolder = bGetNextFolder(SourceAddress)
                
                If IsArray(ExclusionMask) Then
                    Do While LikeAny(nextFoundFolder, ExclusionMask)
                        nextFoundFolder = bGetNextFolder(SourceAddress)
                    Loop
                End If
                
                bDir = nextFoundFolder
                
                ' tacking on base folder if required
                If Not bDir = "" And Not IsMissing(ReturnFullPaths) Then
                    bDir = baseFolder & "\" & bDir
                End If
                
            Else
                bDir = "Usage: �bDir([maskString], [listFolders])�"
            End If
            
        Else    ' User asks for files
            
            If SourceAddress <> 0 Then  ' If user calls with no file mask before having called with one, deny all knowledge...
         
                Dim nextFoundFile As String
                nextFoundFile = bGetNextFile(SourceAddress)
                
                If IsArray(ExclusionMask) Then
                    Do While LikeAny(nextFoundFile, ExclusionMask)
                        nextFoundFile = bGetNextFile(SourceAddress)
                    Loop
                End If
                    
                bDir = nextFoundFile
                    
                ' Just tacking on the requested folder before file names
                If Not bDir = "" And Not IsMissing(ReturnFullPaths) Then
                    bDir = baseFolder & "\" & bDir
                End If
                
            Else    ' Punish the unbeliever
                bDir = "Usage: �bDir([maskString], [listFolders])�"
            End If
            
        End If
        
    End If

    Exit Function

'**********************************************************************************************************************************
' Error handler
ErrHnd:
    If Err.Number <> 0 Then
        Debug.Print "bDir function encountered Error " & Err.Number & " with description: " & Err.Description & _
            " In source " & Err.Source
        Err.Clear
        bDir = ""
    End If

End Function

Public Function bGDirAll(FileSearchMask As String, Optional listFolders, Optional getFileSizes, Optional ReturnFullPaths) As foundFilesStruct
' We're using returning struct for folder names as well, it's just an array of strings...

    Dim strFilesFound As String
    
    ' Allow users to search for multiple search masks all packed into ONE ! Yahooooo!
    If InStr(1, FileSearchMask, "|") > 0 Then
              
        Dim requestedFileMasks As Variant
        ' separate requested expressions in array
        requestedFileMasks = Split(FileSearchMask, "|")
        
        ' total files found, all expressions
        Dim tmpAllFoundFilesStruct As foundFilesStruct
        ' temporary, each of the collections of files found for each expression
        Dim tmpFoundFilesStruct As foundFilesStruct
        
        ' Bundle all found files pertaining to all requested file masks, one by one, into an all result found files struct
        For i = 0 To UBound(requestedFileMasks)
            
            If IsMissing(listFolders) Then
                ' no reason to brach this high above, its the only thing different between the two cases
                If IsMissing(ReturnFullPaths) Then
                    tmpFoundFilesStruct = bGDirAll(CStr(requestedFileMasks(i)))
                Else
                    tmpFoundFilesStruct = bGDirAll(CStr(requestedFileMasks(i)), , , ReturnFullPaths)
                End If
            Else
                ' no reason to brach this high above, its the only thing different between the two cases
                If IsMissing(ReturnFullPaths) Then
                    tmpFoundFilesStruct = bGDirAll(CStr(requestedFileMasks(i)), listFolders)
                Else
                    tmpFoundFilesStruct = bGDirAll(CStr(requestedFileMasks(i)), listFolders, , ReturnFullPaths)
                End If
            End If
            
            tmpAllFoundFilesStruct = AddToFilesStruct(tmpAllFoundFilesStruct, tmpFoundFilesStruct)
            
            ' and reset
            tmpFoundFilesStruct.count = 0: ReDim tmpFoundFilesStruct.files(0): tmpFoundFilesStruct.sizeInKb = 0
            
        Next i
        
        ' and EXIT!
        bGDirAll = tmpAllFoundFilesStruct
        
        Exit Function
    
    Else    ' user searches ONE file/folder mask
    
        ' Call initial search with or without folders param, as commanded
        If IsMissing(listFolders) Then
            If IsMissing(ReturnFullPaths) Then
                strFilesFound = bDir(FileSearchMask)
            Else
                strFilesFound = bDir(FileSearchMask, , "returnFullPaths")
            End If
        Else    ' requesting folders
            If IsMissing(ReturnFullPaths) Then
                strFilesFound = bDir(FileSearchMask, listFolders)
            Else
                strFilesFound = bDir(FileSearchMask, listFolders, "returnFullPaths")
            End If
        End If
    
    End If
    
    ' Now, if initial search returned a matching result
    If Not strFilesFound = "" Then     ' "." and ".." are NOT real folders !
        
        Dim filesFoundStr As foundFilesStruct
        
        If Not IsMissing(ReturnFullPaths) Then
            
            If Not IsMissing(listFolders) Then      ' in case of searching for folders
                ' we can not tolerate the crazy Windows so called folders "." and "..". Bastards!
                If Not Split(strFilesFound, "\")(UBound(Split(strFilesFound, "\"))) = "." And _
                    Not Split(strFilesFound, "\")(UBound(Split(strFilesFound, "\"))) = ".." Then
                        filesFoundStr.count = 1: ReDim filesFoundStr.files(0): filesFoundStr.files(0) = strFilesFound
                End If
                
            Else    ' in case of files, last component of retrieved file path may not be temp file
                
                If Not Split(strFilesFound, "\")(UBound(Split(strFilesFound, "\"))) Like "~*" Then
                    filesFoundStr.count = 1: ReDim filesFoundStr.files(0): filesFoundStr.files(0) = strFilesFound
                End If
                
            End If
            
        Else
        
            If Not strFilesFound = "." And Not strFilesFound = ".." And Not strFilesFound Like "~*" Then
                filesFoundStr.count = 1: ReDim filesFoundStr.files(0): filesFoundStr.files(0) = strFilesFound
            End If
        
        End If
        
        If Not IsMissing(getFileSizes) Then filesFoundStr.sizeInKb = GetSize_ofFile(strFilesFound, Left$(FileSearchMask, InStrRev(FileSearchMask, "\")))
        
    Else    ' or no result
        
        ' Error, nothing found in initial search
        filesFoundStr.count = 0: ReDim filesFoundStr.files(0): filesFoundStr.files(0) = ""
        
        bGDirAll = filesFoundStr
        Exit Function
        
    End If
    
    ' And to continue recording the rest of the results into structure
    If SourceAddress <> 0 Then  ' it should have been set by the above call to bDir with a mask param
        
        ' looking for files
        If IsMissing(listFolders) Then
            
            Dim nfile As String
            
            nfile = bDir
            
            Do While nfile <> "" And Not nfile Like "Usage:*"
                
                If Not nfile Like "~*" Then
                
                    filesFoundStr.count = filesFoundStr.count + 1
                    ReDim Preserve filesFoundStr.files(filesFoundStr.count - 1)
                    
                    If IsMissing(ReturnFullPaths) Then
                        filesFoundStr.files(filesFoundStr.count - 1) = nfile
                    Else
                        filesFoundStr.files(filesFoundStr.count - 1) = Left$(LastSearchMask, InStrRev(LastSearchMask, "\")) & nfile
                    End If
                    
                                            
                    If Not IsMissing(getFileSizes) Then filesFoundStr.sizeInKb = filesFoundStr.sizeInKb + GetSize_ofFile(nfile, Left$(FileSearchMask, InStrRev(FileSearchMask, "\")))
                                                                    
                    nfile = bDir
                
                Else    ' just advance in that case
                
                    nfile = bDir
                    
                End If
                            
            Loop
            
        Else    ' User wants folders
            
            Dim nfolder As String   ' next folder
            
            If IsMissing(ReturnFullPaths) Then
                nfolder = bDir(, listFolders)
            Else
                nfolder = bDir(listFolders:="listFolders", ReturnFullPaths:="returnFullPaths")
            End If
                
            
            Do While nfolder <> ""
                
                If Not IsMissing(ReturnFullPaths) Then  ' if full paths returned
                    
                    If Not Split(nfolder, "\")(UBound(Split(nfolder, "\"))) = "." And _
                        Not Split(nfolder, "\")(UBound(Split(nfolder, "\"))) = ".." Then
                        
                        filesFoundStr.count = filesFoundStr.count + 1
                        ReDim Preserve filesFoundStr.files(filesFoundStr.count - 1)
                        filesFoundStr.files(filesFoundStr.count - 1) = nfolder
                        
                    End If
                    
                Else    ' no full paths returned
                
                    If nfolder <> "." And nfolder <> ".." Then
                
                        filesFoundStr.count = filesFoundStr.count + 1
                        ReDim Preserve filesFoundStr.files(filesFoundStr.count - 1)
                        filesFoundStr.files(filesFoundStr.count - 1) = nfolder
                
                    End If
                
                End If
                            
                If IsMissing(ReturnFullPaths) Then
                    nfolder = bDir(, listFolders)
                Else
                    nfolder = bDir(listFolders:="listFolders", ReturnFullPaths:="returnFullPaths")
                End If
    
            Loop
            
        End If
    Else    ' kinda improbable to get here, but... better safe
        
        filesFoundStr.count = -1    ' Error, nothing found in initial search
        ReDim filesFoundStr.files(0): filesFoundStr.files(0) = ""
    
    End If
    
    bGDirAll = filesFoundStr

End Function

Public Function bGDir_AllFiles(FileSearchMask As String) As foundFilesStruct

' Returning a .count of -1 means the initial search did not return any results
' foundFilesStruct is s struct made of a count integer and an array of strings containing matching file names

    Dim fff As WIN32_FIND_DATA  ' First file found data
    Dim fsHnd As Long           ' First (file) search handle
    
    Dim returnedFiles As foundFilesStruct
    
    ' If we got a hit (search was successfull)
    If fsHnd <> 0 Then
        
        SourceAddress = fsHnd
        
        returnedFiles.count = 0
        ReDim returnedFiles.files(0)
        
        Dim SearchMask As String
        If InStr(1, FileSearchMask, "\") > 0 Then
            SearchMask = Split(FileSearchMask, "\")(UBound(Split(FileSearchMask, "\")))
        End If
        
        ' WIN API search function will return other files as well, not matching search expression...
        ' it uses the 8.3 representation of file name for comparing purposes
        If TrimToNull(fff.cFileName) Like SearchMask Then
            returnedFiles.files(0) = TrimToNull(fff.cFileName)
        Else
            returnedFiles.count = -1    ' in the next while loop, it will be incremented by one, when it finds a matching file...
        End If
        
        
        Dim fsNHnd As Long                          ' File search next handle
        Dim nff As WIN32_FIND_DATA                  ' Next file found
        fsNHnd = FindNextFile(SourceAddress, nff)
        
        ' Loop through all returned files to gather them all into our bucket
        Do While fsNHnd <> 0
            
            ' Don't increase count to our bucket and don't save file/ subfolder name unless name matches with search mask
            If TrimToNull(nff.cFileName) Like SearchMask Then
                returnedFiles.count = returnedFiles.count + 1
                ReDim Preserve returnedFiles.files(returnedFiles.count)
                returnedFiles.files(returnedFiles.count) = TrimToNull(nff.cFileName)
            End If
            
            nff.cFileName = ""                          ' clean file/ folder name of last hit, cause it's gonna bother us later
            fsNHnd = FindNextFile(SourceAddress, nff)   ' and move on to the next file
            
        Loop
        
        FindClose (fsNHnd)
        FindClose (fsHnd)
        SourceAddress = 0
        
    Else    ' No results to our search
    
        returnedFiles.count = 0
        ReDim returnedFiles.files(0)
        returnedFiles.files(0) = ""
        
        FindClose (fsHnd)               ' Dispose of search handle, we're exiting, no results to our search
        
    End If
        
    bGDir_AllFiles = returnedFiles

End Function

Public Function bGDirAllSubFolders_OfFolders(FoldersSearchMask As String, baseFolders() As String, Optional ReturnFullPaths) As foundFilesStruct

    Dim tmpBaseFolder As String
    
    Dim finalStruct As foundFilesStruct     ' to hold sum of files found, at the end
    Dim tmpStruct As foundFilesStruct       ' to be reused on every iteration, holding files/ folders of current folder
        
    If IsArray(baseFolders) Then
        
        If Not IsMissing(ReturnFullPaths) Then
            finalStruct = bGDirAllFolders(FoldersSearchMask, baseFolders(0), ReturnFullPaths)
        Else
            finalStruct = bGDirAllFolders(FoldersSearchMask, baseFolders(0))
        End If
        
        For i = 1 To UBound(baseFolders)
            
            If Not IsMissing(ReturnFullPaths) Then
                tmpStruct = bGDirAllFolders(FoldersSearchMask, baseFolders(i), ReturnFullPaths)
            Else
                tmpStruct = bGDirAllFolders(FoldersSearchMask, baseFolders(i))
            End If
            
            finalStruct = AddToFilesStruct(finalStruct, tmpStruct)
            
        Next i
    
    End If
    
    bGDirAllSubFolders_OfFolders = finalStruct
    
End Function

Public Function bGDirAllFolders(FoldersSearchMask As String, baseFolder As String, Optional ReturnFullPaths) As foundFilesStruct

' FoldersSearchMask should contain ONLY the names mask of folders to be found, in the shape of "cm*", eventually
' more than one such expression for cummulative search of all, such as "cm*|ds*|sn*"

    Dim tmpBaseFolder
    tmpBaseFolder = IIf(Right(baseFolder, 1) <> "\", baseFolder & "\", baseFolder)
    
    If InStr(FoldersSearchMask, "|") > 0 Then
        
        Dim fdrSU As Variant
        
        fdrSU = Split(FoldersSearchMask, "|")
        
        Dim tmpFdrStruct As foundFilesStruct
        Dim tmpFdrTmp As foundFilesStruct
        
        For i = 0 To UBound(fdrSU)
            
            If Not IsMissing(ReturnFullPaths) Then
                tmpFdrTmp = bGDirAll(tmpBaseFolder & fdrSU(i), "listFolders", , ReturnFullPaths)
            Else
                tmpFdrTmp = bGDirAll(tmpBaseFolder & fdrSU(i), "listFolders")
            End If
            
            tmpFdrStruct = AddToFilesStruct(tmpFdrStruct, tmpFdrTmp)
            
            tmpFdrTmp.count = 0: ReDim tmpFdrTmp.files(0)
            
        Next i
        
        bGDirAllFolders = tmpFdrStruct
        
    Else
        
        If Not IsMissing(ReturnFullPaths) Then
            tmpFdrTmp = bGDirAll(tmpBaseFolder & FoldersSearchMask, "listFolders", , ReturnFullPaths)
        Else
            tmpFdrTmp = bGDirAll(tmpBaseFolder & FoldersSearchMask, "listFolders")
        End If
        
        bGDirAllFolders = tmpFdrTmp
    
    End If

End Function

Public Function bGDirAllFolders_Wild(FoldersSearchMask As String) As foundFilesStruct

' More powerfull version of bGDirAllFolders, which allows you to use * or ? into any level of a path
' (so as to also get a collection of folders into which to search for some files later on), EXCEPT the first division
' of path by slash ! ("C:\Documents\MyPreciousOnes\Games*\ToyStoryDocs\Jessie*" ... so use star anywhere, but NOT first division,
' "C:\" in this case !

    Dim pathCompsArr As Variant
    pathCompsArr = Split(FoldersSearchMask, "\")
    
    Dim tmpFFStruct As foundFilesStruct     ' temp folders found struct
    Dim tmpFoldersFound() As String         ' to gather folders for final
    
    ReDim tmpFoldersFound(0)
    tmpFoldersFound(0) = pathCompsArr(0)
    
    ' outer loop, we go through all elements of path (levels of it)
    ' EXCEPT the drive letter! so as to check and get all folders if user used wildcard into it !
    For i = 1 To UBound(pathCompsArr)
        
        If InStr(1, pathCompsArr(i), "*") > 0 Or _
            InStr(1, pathCompsArr(i), "?") > 0 Then
            
            If UBound(tmpFoldersFound) = 0 Then
            
                tmpFFStruct = bGDirAllFolders(CStr(pathCompsArr(i)), tmpFoldersFound(0), "ReturnFullPaths")
                
                tmpFoldersFound = tmpFFStruct.files
                
            Else    ' tmpfolderfound has more than 1 element, we're already looking for subfolders of multiple base folders!
                
                tmpFFStruct = bGDirAllSubFolders_OfFolders(CStr(pathCompsArr(i)), tmpFoldersFound, "ReturnFullPaths")
                tmpFoldersFound = tmpFFStruct.files
                
            End If
            
        Else
            
            If i < UBound(pathCompsArr) Then
                tmpFoldersFound = AppendString_toArrayMembers(tmpFoldersFound, "\" & pathCompsArr(i))
            Else
                tmpFFStruct = bGDirAllSubFolders_OfFolders(CStr(pathCompsArr(i)), tmpFoldersFound, "ReturnFullPaths")
            End If
            
        End If
        
    Next i
    
    bGDirAllFolders_Wild = tmpFFStruct

End Function

Private Function bGetFirstFile(SearchMask As String) As String

    Dim maskString As String
    
    ' If we somehow got a fully qualified path, retain only last element to use for file/ subfolder name comparison
    If InStr(1, SearchMask, "\") Then
        maskString = Split(SearchMask, "\")(UBound(Split(SearchMask, "\")))
    End If
    
    Dim fHnd As Long
    Dim try As WIN32_FIND_DATA
    
    ' Run the silly Windows search function !
    fHnd = FindFirstFile(SearchMask, try)
    
    ' If we are returned a search handle, the above function found some matching files/folders...
    If fHnd <> 0 Then
            
        SourceAddress = fHnd            ' Store search handle address for later usage
        LastSearchMask = SearchMask     ' Store user supplied search mask
            
        If try.dwFileAttributes = 16 Or try.dwFileAttributes = 256 Then     ' 256 - temp file, 16 - folder
            bGetFirstFile = bGetNextFile(SourceAddress)
        Else
            ' Silly Microsoft peoples... they match the 8.3 representation of filename (old DOS limitations!)
            ' to search mask ! Backwards compatibilty my ass !
            Dim clFileName As String
            clFileName = TrimToNull(try.cFileName)
            
            If Not clFileName Like maskString Then
                bGetFirstFile = bGetNextFile(SourceAddress)
            Else
                bGetFirstFile = clFileName
            End If
        End If
        
    Else
        bGetFirstFile = ""
    End If

End Function

Private Function bGetNextFile(SearchHandle As Long) As String

    Dim try As WIN32_FIND_DATA
    Dim fHnd As Long
    
    Dim SearchMask As String
    If Not LastSearchMask = "" Then
        SearchMask = LastSearchMask
    Else
        Debug.Print "bGetNextFile: Empty LastSearchMask global variable!"
        bGetNextFile = ""
        Exit Function
    End If
    
    ' We can't use the fully qualified path for a search mask, we only need file/ subfolder name!
    If InStr(1, SearchMask, "\") > 1 Then
        maskString = Split(SearchMask, "\")(UBound(Split(SearchMask, "\")))
    Else
        maskString = SearchMask
    End If
    
    ' Attempt serch using Win API silly function !
    fHnd = FindNextFile(SearchHandle, try)
    
    If fHnd <> 0 Then
                  
        If try.dwFileAttributes = 16 Or try.dwFileAttributes = 256 Then     ' 256 - temp file, 16 - folder
            bGetNextFile = bGetNextFile(SearchHandle)
        Else
            ' Silly Microsoft peoples... makin' me write more code than necessary !
            
            Dim clFileName As String        ' Cleaned File Name, null char removed
            clFileName = TrimToNull(try.cFileName)
            
            If Not clFileName Like maskString Then
                bGetNextFile = bGetNextFile(SearchHandle)
            Else
                bGetNextFile = clFileName
            End If
        End If
        
    Else
        SourceAddress = 0   ' No more files, dispose of past search handle=s address !
        LastSearchMask = ""
        FindClose (SearchHandle)    ' And close the search handle in memory, so as not to pollute memory
        
        bGetNextFile = ""
    End If

End Function

Private Function bGetFirstFolder(SearchMask As String) As String

    Dim try As WIN32_FIND_DATA
    Dim fHnd As Long
    
    fHnd = FindFirstFile(SearchMask, try)
    
    
    If fHnd <> 0 Then
            
        SourceAddress = fHnd
        LastSearchMask = SearchMask
        
        Dim clFileName As String
        clFileName = TrimToNull(try.cFileName)
        
        If try.dwFileAttributes = FILE_ATTRIBUTE_DIRECTORY Then      ' 256 - temp file, 16 - folder
            bGetFirstFolder = clFileName
        Else
            bGetFirstFolder = bGetNextFolder(SourceAddress)
        End If
        
    Else
        bGetFirstFolder = ""
    End If

End Function

Private Function bGetNextFolder(SearchHandle As Long) As String

    Dim try As WIN32_FIND_DATA
    Dim fHnd As Long
    
    fHnd = FindNextFile(SearchHandle, try)
    
    If fHnd <> 0 Then
                      
        Dim clFileName As String
        clFileName = TrimToNull(try.cFileName)
        
        If try.dwFileAttributes = 16 Then     ' 256 - temp file, 16 - folder
            bGetNextFolder = clFileName
        Else
            bGetNextFolder = bGetNextFolder(SearchHandle)
        End If
        
    Else
        SourceAddress = 0           ' no more folders, dispose of past search handle's address !
        LastSearchMask = ""         ' dispose of latest search mask
        FindClose (SearchHandle)    ' and dispose of the search handle object in memory
        
        bGetNextFolder = ""
    End If

End Function

' ******************************************************************************************************************************************
' *********************************************** UTILITY FUNCTIONS ************************************************************************
' ******************************************************************************************************************************************

Private Function TrimToNull(Text As String) As String
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' This function returns the portion of Text that is to the left of the vbNullChar
' character (same as Chr(0)). Typically, this function is used with strings
' populated by Windows API procedures. It is generally not used for native VB Strings.
' If vbNullChar is not found, the entire Text string is returned.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Dim Pos As Integer
    Pos = InStr(1, Text, vbNullChar)
    If Pos > 0 Then
        TrimToNull = Left(Text, Pos - 1)
    Else
        TrimToNull = Text
    End If

End Function

Private Function bDirNext(hFindFile As Long) As String

If SourceAddress <> 0 Then
    
    Dim fnHnd As Long
    
    Dim tryN As WIN32_FIND_DATA
    
    fnHnd = FindNextFile(SourceAddress, tryN)
    
    If fnHnd <> 0 Then
        bDirNext = tryN.cFileName
    Else    ' Error
        SourceAddress = 0
        bDirNext = ""
    End If
    
End If

End Function

Private Function AddToFilesStruct(ByRef BaseFilesStruct As foundFilesStruct, ByRef SupplFilesStruct As foundFilesStruct) As foundFilesStruct

    If SupplFilesStruct.count > 0 Then
            
        For i = 1 To SupplFilesStruct.count
            
            BaseFilesStruct.count = BaseFilesStruct.count + 1
            
            ReDim Preserve BaseFilesStruct.files(BaseFilesStruct.count - 1)
            
            BaseFilesStruct.files(BaseFilesStruct.count - 1) = SupplFilesStruct.files(i - 1)
            
        Next i
            
    Else
    End If
    
    AddToFilesStruct = BaseFilesStruct

End Function

Private Function AppendStrings_toArrayMembers(TargetArray, StringsToAppend() As String, Optional Connector, Optional CopyBaseFolder) As Variant

    Dim tmpTgArr
    
    If IsArray(TargetArray) Then
            
        tmpTgArr = TargetArray
                   
        If IsArray(StringsToAppend) Then
            
            ' make place for new members, but also copy
            'ReDim Preserve tmpTgArr(UBound(tmpTgArr) + UBound(StringsToAppend) + 1)
            
            Dim initialBase
            initialBase = tmpTgArr(0)
            
            For n = 0 To UBound(StringsToAppend)
                
                ' If not on first iteration, make more room and optionally copy base folder to new element
                If n > 0 Then
                    ReDim Preserve tmpTgArr(n)
                    If Not IsMissing(CopyBaseFolder) Then tmpTgArr(n) = initialBase
                End If
                            
                If Not IsMissing(Connector) Then
                    tmpTgArr(n) = tmpTgArr(n) & Connector & StringsToAppend(n)
                Else
                    tmpTgArr(n) = tmpTgArr(n) & StringsToAppend(n)
                End If
                
            Next n
            
            ' but also copy initial base folder to rest of new members, if requested
    '        If Not IsMissing(CopyBaseFolder) Then
    '
    '            For k = 1 To UBound(tmpTgArr)
    '                tmpTgArr(k) = tmpTgArr(0)
    '            Next k
    '
    '        End If
            
            ' as well append all string members to all base members
    '        For l = 0 To UBound(tmpTgArr)
    '            For m = 0 To UBound(StringsToAppend)
    '                If Not IsMissing(Connector) Then
    '                    tmpTgArr(l) = tmpTgArr(l) & Connector & StringsToAppend(m)
    '                Else
    '                    tmpTgArr(l) = tmpTgArr(l) & StringsToAppend(m)
    '                End If
    '            Next m
    '        Next l
            
        End If
        
    Else
        AppendStrings_toArrayMembers = Null
        Exit Function
    End If
    
    AppendStrings_toArrayMembers = tmpTgArr

End Function

Private Function AppendString_toArrayMembers(TargetArray, StringToAppend) As Variant

    Dim tmpTgArr
    
    If IsArray(TargetArray) Then
            
        tmpTgArr = TargetArray
                   
        For i = 0 To UBound(tmpTgArr)
            
            tmpTgArr(i) = tmpTgArr(i) & CStr(StringToAppend)
            
        Next i
        
    Else
        AppendString_toArrayMembers = Null
        Exit Function
    End If
    
    AppendString_toArrayMembers = tmpTgArr

End Function

Private Function JoinArrUpTo(TargetArray, MaxLevel As Integer, Separator As String) As String

    Dim tmpStr As String
    
    If IsArray(TargetArray) Then
        
        If UBound(TargetArray) >= MaxLevel Then
            
            For i = 0 To MaxLevel
                
                tmpStr = IIf(tmpStr <> "", tmpStr & Separator & TargetArray(i), TargetArray(i))
                
            Next i
            
            JoinArrUpTo = tmpStr
            
        Else
            JoinArrUpTo = ""
            Exit Function
        End If
        
    Else
        JoinArrUpTo = ""
        Exit Function
    End If

End Function

' Strange function to compare a string with a string collection contained in second parameter
Function LikeAny(SourceString As String, CompareArray) As Boolean

    ' we only accept string arrays
    If Not IsArray(CompareArray) Then
        LikeAny = False
        Exit Function
    End If
    
    
    For i = 0 To UBound(CompareArray)
        If SourceString Like CompareArray(i) Then
            LikeAny = True
            Exit Function
        End If
    Next i

End Function

'****************************************************************************************************************************************
'****************************************************************************************************************************************
'****************************************************************************************************************************************

' Test function, can be safely eliminated
Private Sub testPrint_bDirAll()

    Dim res As foundFilesStruct
    
    res = bDirAll("T:\Docs\margama\cm05*.doc")
    
    If res.count > 0 Then
        Debug.Print "Found " & res.count & " files matching " & "T:\Docs\margama\st051*.doc:"
        For i = 1 To res.count
            Debug.Print res.files(i)
        Next i
    End If

End Sub

Private Sub testPrint_bDirAllFolders()

    Dim res As foundFilesStruct
    
    res = bGDirAll("T:\Prepared originals\cm014*", "FoldersNice!")
    
    If res.count > 0 Then
        Debug.Print "Found " & res.count & " files matching " & "T:\Docs\margama\cm014*:"
        For i = 1 To res.count
            Debug.Print res.files(i)
        Next i
    End If

End Sub
