VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} formBTmxOpt 
   OleObjectBlob   =   "formBTmxOpt.frx":0000
   Caption         =   "Bad TMX Manager"
   ClientHeight    =   2925
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5655
   StartUpPosition =   1  'CenterOwner
   Tag             =   "BadTmxOptions"
   TypeInfoVer     =   34
End
Attribute VB_Name = "formBTmxOpt"
Attribute VB_Base = "0{210F2F4B-8DC4-4A35-B121-DA2E3BE7EBBD}{604A9739-41B3-4013-B3CD-DD243E0FBB9B}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False




' Setting these two constant below to real folder path and real options file path, you may avoid using ONE settings file for
' one template, instead using that folder and that file with functions "getOption" and "setOption" (so that one userform to have its own settings file)
' May NOT use the other functions though, such as ""

Private optionsBaseFolderC As String
Private optionsFilenameC As String
'*********************************************************************************************************************************************************
Private optionsFileSectionNameC As String


'*
'*
'*

Public Property Get optionsFilename() As String

optionsFilename = optionsFilenameC

End Property


Public Property Let optionsFilename(input_optionsFilename As String)

optionsFilenameC = input_optionsFilename

End Property


Private Sub cbNoAnnoyingMsg_Click()
' options which, set (and saved into settings file), makes it so that
' the two message boxes do not appear when attempting to open tmx exports/ alignments folder(s)




End Sub



Private Sub skipBrowseToFolder_Click()

Dim newFolder As String

newFolder = gscrolib.GetDirectory("Please choose a new non GSC alignments location")


If newFolder <> "" Then
    
    Me.tbBackFolder = newFolder
    
    Dim sFile As New SettingsFileClass
    
    If Not sFile.IsEmpty = ErrorFindingFile Or sFile.IsEmpty = FileEmpty Then
        
        Dim ctFdr As String
        ctFdr = sFile.getOption(Me.tbBackFolder.Tag, Me.frmBTmxOpts.Tag)
        
        If ctFdr <> newFolder Then
            sFile.setOption Me.frmBTmxOpts.Tag, Me.tbBackFolder.Tag, newFolder
        End If
    
    Else    ' settings file empty or not found
        
        sFile.Create_Empty
        sFile.setOption Me.frmBTmxOpts.Tag, Me.tbBackFolder.Tag, newFolder
        
    End If
    
    Set sFile = Nothing
    
End If

End Sub

Private Sub UserForm_Activate()

End Sub

'*
'*
'*

Private Sub UserForm_Initialize()

' Set to default property
Me.optionsFilename = ""

' Put right values into font selection combo box
Call Initialize_cbMCFont

' Retrieve, if already set, options from settings file and change userform accordingly
Call setOptions_fromSettingsFile


End Sub


Private Sub cmdCancel_Click()
' Cancel button

Unload Me

End Sub

Private Sub cmdOK_Click()
' OK button, save all changes to settings text file

Me.Hide

Dim sFile As New SettingsFileClass

If sFile.Exists Then
    
    Dim tc As control
    Dim gottaRefresh As Boolean     ' flag - do we call refreshing routine?
    
    Dim newUserOptions As String
    newUserOptions = sFile.txtGetFormOptions(formBTmxOpt)
    
    Dim storedOptions As String
    storedOptions = sFile.GetINISection(sFile.Path, frmBTmxOpts.Tag)
    
    ' TODO - Still gotta check if it wouldnt be better to not simply retrieve options (from form, but also from file,
    ' but to retrieve SORTED options... (so as to be sure that we are not mistakenly detect unsorted same options as not the same!)
    If storedOptions <> newUserOptions Then
        gottaRefresh = True
        Call sFile.SetINISection(sFile.Path, frmBTmxOpts.Tag, newUserOptions)
    End If
    
    
    ' NO NEED WRITE EACH VALUE separately, nicer the above way!
    '    For Each tc In Me.Controls
    '        If Left(tc.Name, 3) = "frm" Then
    '
    '            Dim tf As MSForms.Frame
    '
    '            Set tf = tc
    '
    '            Dim ctSection As String
    '            ctSection = tc.Tag
    '
    '            For i = 0 To tf.Controls.count - 1
    '                If Left(tf.Controls(i).Name, 3) <> "lbl" And Left(tf.Controls(i).Name, 2) <> "sb" And Left(tf.Controls(i).Name, 4) <> "skip" Then
    '
    '                    If sFile.getOption(tf.Controls(i).Tag, ctSection) <> tf.Controls(i).Value Then
    '
    '                        sFile.setOption ctSection, tf.Controls(i).Tag, tf.Controls(i).Value
    '
    '                        gottaRefresh = True
    '
    '                    End If
    '
    '                End If
    '            Next i
    '
    '        End If
    '    Next tc
    
Else
End If

' And refresh if any settings changed
If gottaRefresh Then
    Call RefreshRibbon
End If


Set sFile = Nothing

End Sub

Private Sub sbTimerMod_SpinDown()
' Decrease timer interval value

If Me.tbTimerValue <> "" Then
    If CInt(Me.tbTimerValue) > 5 Then
        Me.tbTimerValue = CInt(Me.tbTimerValue) - 5
    ElseIf CInt(Me.tbTimerValue) >= 1 Then
        Me.tbTimerValue = CInt(Me.tbTimerValue) - 1
    Else
        Me.tbTimerValue = 0
    End If
End If

End Sub


Private Sub sbTimerMod_SpinUp()
' Increase timer value

If Me.tbTimerValue <> "" Then
    If Me.tbTimerValue <= 1435 Then
        Me.tbTimerValue = CInt(Me.tbTimerValue) + 5
    End If
End If

End Sub


Sub setOptions_fromSettingsFile()


Dim BadTmxSettingsFile As SettingsFileClass

Set BadTmxSettingsFile = New SettingsFileClass

' If we already have a saved options file for current template, we retrieve/ compare and set accordingly
If BadTmxSettingsFile.Exists Then
    
    Dim settingsFileFound As IsEmptyCode
    settingsFileFound = BadTmxSettingsFile.IsEmpty
    
    If settingsFileFound = FileEmpty Or settingsFileFound = ErrorFindingFile Then
        GoTo writeDefaultsJump
    Else    ' settings file found and non empty
    End If
    
        
    Dim tc As control

    Dim txtOptions() As String     ' Unidimensional string array, gather
    ReDim txtOptions(0)            '
    
    Dim i As Integer
    
    If Me.Controls.count > 0 Then
        
        ' Iterate through all controls
        For Each tc In Me.Controls
            ' but stop only on frames (name starts with frm)
            If Left$(tc.Name, 3) = "frm" Then
                
                ' If found a frame control, initiate a string in storage array
                'txtOptions(i) = "[" & tc.Tag & "]" & vbCr
                
                ' we need to access the controls collection of the frame object
                Dim tframe As MSForms.Frame
                Set tframe = tc
                
                Dim j As Integer
                
                ' We iterate then through all controls of said frame to gather tags into string
                For j = 0 To tframe.Controls.count - 1
                    
                    Dim cc As MSForms.control
                    
                    Set cc = tframe.Controls(j)
                    
                    ' Skip simple labels, they're for user info only
                    If Left(cc.Name, 3) <> "lbl" And Left(cc.Name, 2) <> "sb" And Left(cc.Name, 4) <> "skip" Then      ' exclude all labels and spin buttons from being processed
                        'txtOptions(i) = txtOptions(i) & tframe.Controls(j).Tag & " = " & tframe.Controls(j).Value & vbCr
                        
                        Dim storedCtOption As String
                        
                        storedCtOption = BadTmxSettingsFile.getOption(cc.Tag, tc.Tag)
                        
                        If storedCtOption <> tframe.Controls(j).Value Then
                            If storedCtOption <> "" Then
                                tframe.Controls(j).Value = storedCtOption
                            End If
                        End If
                        
                    End If
                    
                Next j
                
                ' and attempt to ward off saving global template
                If getTemplateIndex(MacroContainer) > 0 Then    ' 0 or less is template not found or other error
                    Application.Templates(getTemplateIndex(MacroContainer)).Saved = True
                End If
                
                ' Debugging, we look at current array string (we finised with current frame)
                'Debug.Print txtOptions(i)
                
                ' Having finished with current frame, we move on
                'i = i + 1
                'ReDim Preserve txtOptions(i)
                                       
            End If  ' No else, we only select frames
        Next tc
        
    Else    ' No controls on form, no settings file to write !
    End If
    
    
Else    ' NO saved options file for ct template

writeDefaultsJump:

    BadTmxSettingsFile.Create_Empty
    BadTmxSettingsFile.Write_DefaultsNew (Me)
    
End If



End Sub



Sub Initialize_cbMCFont()
' Put right values into font selection combo box

Dim fontOptions

fontOptions = Array("Cooper Black", "Forte", "Hello Denver", "Unicorn")

Me.cbMCFont.List() = fontOptions
Me.cbMCFont.ListIndex = 1   ' The default font is actually "Forte", at second position, position 1 in list


End Sub
