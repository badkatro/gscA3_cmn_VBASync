VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmBackupChecker 
   OleObjectBlob   =   "frmBackupChecker.frx":0000
   Caption         =   "BackupQ Projects Checker"
   ClientHeight    =   8910
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   5415
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   36
End
Attribute VB_Name = "frmBackupChecker"
Attribute VB_Base = "0{E9A5D27D-36C2-408F-81D1-F9E08062E199}{3E22BCC1-6BB0-4F6C-912F-DB262DEC71F5}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False
Private Sub cmdClearList_Click()

Me.TextBox1.Text = ""

End Sub

Private Sub cmdMakeDoc_Click()

Call Create_SuspectProjectFolders_Document
Unload Me

End Sub

Private Sub cmdOpenFolder_Click()

Dim selectedProjectID
selectedProjectID = Me.lbOutputList.List(Me.lbOutputList.ListIndex)

If selectedProjectID <> "" Then
    
    Dim targetProjFdr As String
    targetProjFdr = getProjectFolderOf(GSC_StandardName_WithLang_toFileName(CStr(selectedProjectID), , "IgnoreSuppliedLanguage"))
    
    If targetProjFdr <> "" Then
        Call Shell("explorer.exe" & " " & targetProjFdr, vbNormalFocus)
    Else
        MsgBox "Project folder NOT found for " & selectedProjectID
    End If
    
End If


End Sub


Private Sub cmdPrintList_Click()


Call Create_SuspectProjectFolders_Document("PrintOut")


End Sub

' without optional parameter, it just creates doc. With, it also prints and closes.
Sub Create_SuspectProjectFolders_Document(Optional PrintOut)


Documents.Add

Selection.HomeKey Unit:=wdStory
Selection.TypeText "The following project folders were NOT backed at " & Time & vbCr & vbCr


Dim itemsUnbacked
itemsUnbacked = GetListboxContent(Me.lbOutputList)


If IsArray(itemsUnbacked) Then
    For i = 0 To UBound(itemsUnbacked)
        Selection.InsertAfter (itemsUnbacked(i)) & vbCr
    Next i
End If


' Print or just make doc ?
If Not IsMissing(PrintOut) Then
    ActiveDocument.PrintOut
    ActiveDocument.Close SaveChanges:=False
End If


End Sub




Private Sub lbOutputList_Click()

End Sub


Private Sub lbOutputList_DblClick(ByVal Cancel As MSForms.ReturnBoolean)


End Sub


Private Sub TextBox1_Change()

' on change to input text box, reset results cues
Me.lblUnbackedProjects.Caption = "These projects are NOT backed up:"
Me.lbOutputList.Clear

End Sub


Private Sub cmdSnoop_Click()


' get the input project list from user
Dim projectsToCheck As String
projectsToCheck = TrimNonAlphaNums(Me.TextBox1.Text)

If projectsToCheck = "" Then
    Me.TextBox1.Text = "Please paste/ type some projects IDs to check!"
    Exit Sub
End If


' get the used separator character
Dim sep As String
sep = getUsedSeparator(projectsToCheck)

' count provided projects
Dim initialProjectsCount As Integer
initialProjectsCount = UBound(Split(projectsToCheck, sep)) + 1


' and investigate and return un-backed projects
Dim unbackedProjects As String
unbackedProjects = areProjects_BackedUp(projectsToCheck)


' which are also counted
Dim returnedProjectsCount As Integer
returnedProjectsCount = UBound(Split(unbackedProjects, sep)) + 1

' and present results into "Output" tab's text box
If unbackedProjects <> "" Then
    
    Me.mpBackupChecker.Value = 1    ' change over to oouput tab
    Me.lblUnbackedProjects.Caption = Me.lblUnbackedProjects.Caption & " (" & returnedProjectsCount & " / " & initialProjectsCount & ")"
    Call AddItemsToList(Me.lbOutputList, unbackedProjects)
    
Else    ' NO unbacked projects found !
    
    Me.mpBackupChecker.Value = 1    ' change over to oouput tab
    Me.lblUnbackedProjects.Caption = "NO un-backed projects found!"
    
End If


End Sub


Function AddItemsToList(TargetList As MSForms.ListBox, StringToWrite As String)

Dim sep As String
sep = getUsedSeparator(StringToWrite)

Dim itemsToWrite
itemsToWrite = Split(StringToWrite, sep)


For i = 0 To UBound(itemsToWrite)
    TargetList.AddItem (itemsToWrite(i))
Next i


End Function


Private Sub cmdCancel_Click()

Unload Me

End Sub


Function GetListboxContent(TargetListbox As MSForms.ListBox) As Variant


Dim tmpResult
ReDim tmpResult(0)

If TargetListbox.listCount > 0 Then
    
    For i = 0 To TargetListbox.listCount - 1    ' listCount is 1 based
        ReDim Preserve tmpResult(i)
        tmpResult(i) = TargetListbox.List(i)
    Next i
    
End If

GetListboxContent = tmpResult

End Function


Private Sub cmdBackUpList_Click()

If Me.lbOutputList.listCount > 0 Then
    
    Dim projectsToBackUp
    projectsToBackUp = GetListboxContent(Me.lbOutputList)
    
    Dim stillUnbacked As String
    stillUnbacked = backupProjectsList(projectsToBackUp)
    
    
    If stillUnbacked <> "" Then
        Me.lblUnbackedProjects.Caption = "These projects COULD NOT be backed up, please try manually:"
        Me.lbOutputList.Clear
        Call AddItemsToList(Me.lbOutputList, stillUnbacked)
    Else
        Me.lblUnbackedProjects.Caption = "All projects backed-up!"
        Me.lbOutputList.Clear
    End If
    
End If

End Sub


Private Sub TextBox1_DblClick(ByVal Cancel As MSForms.ReturnBoolean)


Dim clipText As String
clipText = GetClipboard

If clipText <> "" Then
    Me.TextBox1.Text = clipText
End If


End Sub

Private Sub UserForm_Initialize()
    
' initially switch over to first tab, indifferent of where the designer left it (saved file with)
Me.mpBackupChecker.Value = 0
    
End Sub
