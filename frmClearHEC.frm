VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmClearHEC 
   OleObjectBlob   =   "frmClearHEC.frx":0000
   Caption         =   "ClearHEC"
   ClientHeight    =   825
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   1830
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   3
End
Attribute VB_Name = "frmClearHEC"
Attribute VB_Base = "0{9394A728-45D6-4033-A19F-FD31998A650F}{FF943FCB-F983-4601-A22E-7D8E9B4388B7}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False



Public rr As Long
Public rp As Long


Sub UserForm_Activate()

Dim fSty As Long
Dim lp As POINTAPI

pCounter = 0
'rr = GetActiveWindow
rr = gscrolib.FindWindow("ThunderDFrame", "ClearHEC")
'Debug.Print "Clear HEC Form had handle " & rr

fSty = gscrolib.GetWindowLong(rr, gscrolib.GWL_STYLE)

SetWindowLong rr, gscrolib.GWL_STYLE, CLng(fSty And (Not &HC00000))

If Me.Height > 65 And Me.Height < 66 Then Me.Height = Me.Height - 15

'If gscrolib.GetCursorPos(lp) <> 0 Then
    Me.Left = PixelsToPoints(30, False)
    Me.Top = PixelsToPoints(150, True)
'End If

'Debug.Print "UserForm Activate's been run"
End Sub


Private Sub Label1_Click()
' Label1 - containing written explanation

Dim mr As Range
Dim FootNotesDone As Boolean

On Error GoTo ProbNoDoc

If Documents.count = 0 Then Unload Me

Set mr = ActiveDocument.StoryRanges(wdMainTextStory)

FootNotesToo:
    
    With mr.Find
        .ClearAllFuzzyOptions
        .ClearFormatting
        .Format = True
        .Font.Color = wdColorWhite
        .Replacement.Font.Color = wdColorBlack
        
        .Execute "", , , , , , True, wdFindStop, True, "", wdReplaceAll
        
    End With
    
    mr.Font.Animation = wdAnimationNone
    mr.HighlightColorIndex = wdNoHighlight

If FootNotesDone = False And ActiveDocument.Footnotes.count > 0 Then
    Set mr = ActiveDocument.StoryRanges(wdFootnotesStory)
    FootNotesDone = True
    GoTo FootNotesToo
End If

Unload Me

Exit Sub

ProbNoDoc:
    If Err <> 0 Then
        Err.Clear
        End
    End If

End Sub

Private Sub Label2_Click()
' Detailed, separate entries for removing highlight/ text effects/ text colors

Me.Hide

frmClearH_E_C.Show

End Sub

Private Sub Label3_Click()
' Close Clear HEC

Unload Me

End Sub

Private Sub UserForm_Click()
' main ClearHec command, clear Highlight, Text Effects and White Colored to black text

Dim mr As Range
Dim FootNotesDone As Boolean

On Error GoTo ProbNoDoc

If Documents.count = 0 Then Unload Me

Set mr = ActiveDocument.StoryRanges(wdMainTextStory)

FootNotesToo:
    
    With mr.Find
        .ClearAllFuzzyOptions
        .ClearFormatting
        .Format = True
        .Font.Color = wdColorWhite
        .Replacement.Font.Color = wdColorBlack
        
        .Execute "", , , , , , True, wdFindStop, True, "", wdReplaceAll
        
    End With
    
    mr.Font.Animation = wdAnimationNone
    mr.HighlightColorIndex = wdNoHighlight

If FootNotesDone = False And ActiveDocument.Footnotes.count > 0 Then
    Set mr = ActiveDocument.StoryRanges(wdFootnotesStory)
    FootNotesDone = True
    GoTo FootNotesToo
End If

Unload Me

Exit Sub

ProbNoDoc:
    If Err <> 0 Then
        Err.Clear
        End
    End If

End Sub
