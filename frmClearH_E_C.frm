VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmClearH_E_C 
   OleObjectBlob   =   "frmClearH_E_C.frx":0000
   Caption         =   "ClearH_E_C"
   ClientHeight    =   1320
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   1890
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   8
End
Attribute VB_Name = "frmClearH_E_C"
Attribute VB_Base = "0{E3B5C98F-6C54-4B6F-8D1E-A1151E7CDD2E}{A2B26A12-2132-4C07-98C5-A0CEE5AFA46E}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False









Private Sub Label1_Click()
' Clear highlight from selection or whole document

Me.Hide

If Documents.count = 0 Then End

Me.Label1.SpecialEffect = fmSpecialEffectSunken

'If Me.Height = 75 Then Me.Height = Me.Height + 12   ' compensate for sunken effect

If Selection.Type = wdSelectionNormal Then
    Selection.Range.HighlightColorIndex = wdNoHighlight
    StatusBar = "Highlight removed on selected text"
ElseIf Selection.Type = wdSelectionIP Then
    ActiveDocument.StoryRanges(wdMainTextStory).HighlightColorIndex = wdNoHighlight
    If ActiveDocument.Footnotes.count > 0 Then ActiveDocument.StoryRanges(wdFootnotesStory).HighlightColorIndex = wdNoHighlight
    StatusBar = "Highlight removed in whole document, footnotes included"
Else
    StatusBar = "Please select some text or place cursor in some"
End If

Me.Hide
frmClearHEC.Show

End Sub

Private Sub Label2_Click()
' Clear text colors, whole document or selection

Me.Hide

If Documents.count = 0 Then Unload Me

Me.Label2.SpecialEffect = fmSpecialEffectSunken

' When we remove title bar, we lose 15 from height
'If Me.Height = 75 Then Me.Height = Me.Height + 12


If Selection.Type = wdSelectionNormal Then
    Selection.Range.Font.Color = wdColorBlack
    StatusBar = "Text colors removed on selected text"
ElseIf Selection.Type = wdSelectionIP Then
    ActiveDocument.StoryRanges(wdMainTextStory).Font.Color = wdColorBlack
    If ActiveDocument.Footnotes.count > 0 Then ActiveDocument.StoryRanges(wdFootnotesStory).Font.Color = wdColorBlack
    StatusBar = "Text colors removed in whole document"
Else
    StatusBar = "Please select some text or place cursor in some"
End If

Me.Hide
frmClearHEC.Show


End Sub

Private Sub Label3_Click()
' Clear text effects, whole doc or selection

Me.Hide

If Documents.count = 0 Then Unload Me

Me.Label3.SpecialEffect = fmSpecialEffectSunken

'If Me.Height = 75 Then Me.Height = Me.Height + 12   ' compensate for sunken effect

If Selection.Type = wdSelectionNormal Then
    Selection.Range.Font.Animation = wdAnimationNone
    StatusBar = "Text animation removed on selected text"
ElseIf Selection.Type = wdSelectionIP Then
    ActiveDocument.StoryRanges(wdMainTextStory).Font.Animation = wdAnimationNone
    If ActiveDocument.Footnotes.count > 0 Then ActiveDocument.StoryRanges(wdFootnotesStory).Font.Animation = wdAnimationNone
    StatusBar = "Text animation removed in whole document"
Else
    StatusBar = "Please select some text or place cursor in some"
End If

Me.Hide
frmClearHEC.Show


End Sub

Private Sub Label4_Click()

Unload Me
frmClearHEC.Show

End Sub

Private Sub UserForm_Activate()

Dim fSty As Long
Dim lp As POINTAPI

pCounter = 0
'rr = GetActiveWindow
rr = gscrolib.FindWindow("ThunderDFrame", "ClearH_E_C")
'Debug.Print "ClearH_E_C form had handle " & rr

fSty = gscrolib.GetWindowLong(rr, gscrolib.GWL_STYLE)

SetWindowLong rr, gscrolib.GWL_STYLE, CLng(fSty And (Not &HC00000))

If Me.Height > 85 Then
    'Debug.Print "ClearH_E_C form was > 85"
    Me.Height = Me.Height - 17
    'Debug.Print "ClearH_E_C form was " & Me.Width
    Me.Width = Me.Width - 20
    'Debug.Print "Changed ClearH_E_C form width to " & Me.Width
Else
    Debug.Print "ClearH_E_C form was " & Me.Height
End If

'If gscrolib.GetCursorPos(lp) <> 0 Then
    Me.Left = PixelsToPoints(30, False)
    Me.Top = PixelsToPoints(150, True)
'End If

'Debug.Print "UserForm Activate's been run"

End Sub
