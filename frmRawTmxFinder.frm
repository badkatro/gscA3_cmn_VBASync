VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmRawTmxFinder 
   OleObjectBlob   =   "frmRawTmxFinder.frx":0000
   Caption         =   "Raw tmx finder (RO)"
   ClientHeight    =   9750
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5430
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   24
End
Attribute VB_Name = "frmRawTmxFinder"
Attribute VB_Base = "0{4DE17AAA-D257-4475-9D12-D42546928818}{D20D077E-F54E-4C89-BF10-06D0BE2F6815}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False

'Const ProjectsBaseFolder As String = "T:\Prepared originals"
Const rawFilesMask As String = "Background\raw*tmx"
Const backgroundFolder As String = "\Background"

Dim mafChrWid(32 To 127) As Double ' widths of printing characters
Dim msFontName  As String ' font name having these widths

Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" ( _
                ByVal hWnd As Long, _
                ByVal Msg As Long, ByVal wParam As Long, _
                ByVal lParam As Long) As Long


Private Sub cmdRefresh_Click()

Call Search_for_RawAlignments

Call SetListView_ProperWidth

End Sub

Private Sub UserForm_Initialize()

Call Search_for_RawAlignments

End Sub


Private Sub UserForm_Activate()
 
Call SetListView_ProperWidth

End Sub


Function Get_All_TPreparedProjects() As foundFilesStruct


Dim tmpFdrStructs As foundFilesStruct

tmpFdrStructs = bGDirAllFolders("cm*|da*|ds*|sn*|st*", ProjectsBaseFolder)


Get_All_TPreparedProjects = tmpFdrStructs


End Function


Sub CreateAlignmentsDocument(Optional PrintOut)


Documents.Add

Selection.HomeKey Unit:=wdStory

Selection.TypeText Me.lblTotalProjects.Caption & " with raw alignments at date: " & vbCr & CurrDTime & vbCr & vbCr

ActiveDocument.Tables.Add Range:=Selection.Range, NumRows:=1, NumColumns:= _
        2, DefaultTableBehavior:=wdWord9TableBehavior, AutoFitBehavior:= _
        wdAutoFitContent
    
    With Selection.Tables(1)
        If .Style <> "Table Contemporary" Then
            .Style = "Table Contemporary"
        End If
        .ApplyStyleHeadingRows = False
        .ApplyStyleLastRow = True
        .ApplyStyleFirstColumn = True
        .ApplyStyleLastColumn = True
    End With
    
    Selection.SplitTable
    ActiveWindow.ActivePane.View.ShowAll = Not ActiveWindow.ActivePane.View. _
        ShowAll
    ActiveWindow.View.TableGridlines = Not ActiveWindow.View.TableGridlines

Selection.GoTo What:=wdGoToTable, which:=1


For i = 1 To Me.ListView1.ListItems.count
    Selection.TypeText Me.ListView1.ListItems(i) & vbCr
Next i
Selection.TypeBackspace
Selection.MoveRight Unit:=wdCell, count:=1


For i = 1 To ListView1.ListItems.count
    Selection.TypeText Me.ListView1.ListItems(i).ListSubItems(1) & vbCr
Next i
Selection.TypeBackspace


' Print or just make doc ?
If Not IsMissing(PrintOut) Then
    ActiveDocument.PrintOut
    ActiveDocument.Close SaveChanges:=False
End If



End Sub


Private Sub cmdAssignPreparation_Click()

Load frmAssign

frmAssign.tbaDocuments = GSCFileName_ToStandardName(Me.ListView1.selectedItem.Text)

' Stop form from activating user-chosen section
frmAssign.AutoActivationDone = True
' and set focus to preparation section
Call frmAssign.Activate_Section(True, "ast", frmAssign.PrepAccentColor)

frmAssign.Show

End Sub


Private Sub cmdClipboardFilter_Click()

' reinitialise count label caption
Me.lblTotalProjects.Caption = "# projects found"


If IsClipboard_AListOf_CouncilDocuments Then
    Call PurgeListView_fromClipboard
    ' and reclean clipboard !
    Set_Clipboard_TextContents ("")
Else
    MsgBox "Clipboard does NOT contain a list of GSC documents, please retry after having copied one!"
End If

' and re-actualise total count
If Me.ListView1.ListItems.count = 1 Then
    If Me.ListView1.ListItems(1).Text <> "No projects found" Then
        Me.lblTotalProjects.Caption = Replace(Replace(Me.lblTotalProjects.Caption, "#", Me.ListView1.ListItems.count), "projects", "project")
    Else    ' still no elements found... we ignore list empty message!
        Me.lblTotalProjects.Caption = Replace(Me.lblTotalProjects.Caption, "#", "0")
    End If
Else
    Me.lblTotalProjects.Caption = Replace(Me.lblTotalProjects.Caption, "#", Me.ListView1.ListItems.count)
End If


End Sub


Sub PurgeListView_fromClipboard()


Dim soughtDocs As String

soughtDocs = Get_Clipboard_TextContents


Dim ctDocID As String


For i = 1 To Me.ListView1.ListItems.count
    
    ctDocID = GSCFileName_ToStandardName(Me.ListView1.ListItems(i), UseWFSuffix:="True")
    
    If InStr(1, soughtDocs, ctDocID) = 0 Then     ' if doc is NOT found in clip list... Eliminate! Exterminate!
        Me.ListView1.ListItems(i).Text = "-" & Me.ListView1.ListItems(i)
    End If
    
Next i


Dim listCount As Integer
listCount = Me.ListView1.ListItems.count

Dim j As Integer
j = 1   ' element we focus deleting ON. First 1, then after finding first non-deletable element, 2 and so on...

For i = 1 To listCount
    
    If Left$(Me.ListView1.ListItems(j).Text, 1) = "-" Then
        Me.ListView1.ListItems.Remove (j)
    Else    ' found first element non-deletable
        j = j + 1
        'i = i + 1
    End If

Next i

' to account for scrollbar acc to no of items found in list
Call SetListView_ProperWidth

'and signal to the use evr is still all right
If Me.ListView1.ListItems.count = 0 Then
    Me.ListView1.ListItems.Add Text:="No projects found"
End If


End Sub


Sub SetListView_ProperWidth()

' If we don't need scrollbar, increase second column width to cover its absence
If Me.ListView1.ListItems.count < 29 Then
    ' only increase second column width the first time ! (use could click refresh multiple times!)
    If Me.ListView1.Width - (Me.ListView1.ColumnHeaders(1).Width + Me.ListView1.ColumnHeaders(2).Width) > 3 Then
        Me.ListView1.ColumnHeaders(2).Width = Me.ListView1.ColumnHeaders(2).Width + 13
    End If
Else     ' found multiple items, scrollbar is present again!
    Me.ListView1.ColumnHeaders(2).Width = 60
End If


End Sub


Private Sub lblPrintAlDoc_Click()

Call CreateAlignmentsDocument(PrintOut:="Yes")

End Sub

Private Sub lblCreateAlDoc_Click()

Call CreateAlignmentsDocument

End Sub

Private Sub lblTotalProjects_Click()

End Sub


Sub Search_for_RawAlignments()

CurrDTime = Date & vbCr & Time

Me.ListView1.Refresh
Me.ListView1.View = lvwReport

' only add column headers once !
If Me.ListView1.ColumnHeaders.count = 0 Then
    Me.ListView1.ColumnHeaders.Add Text:="Project ID", Width:=140
    Me.ListView1.ColumnHeaders.Add Text:="RAW size", Width:=60
End If


' Debug
Dim t1
t1 = timer


Dim prepProjFdrs As foundFilesStruct
prepProjFdrs = Get_All_TPreparedProjects


' Debug
Dim t2
t2 = timer

Debug.Print "Looking for all types of Council docs on net took " & Format(CDbl(t2 - t1), "0.000") & " sec."
Debug.Print "Found, " & prepProjFdrs.count & " total project folders!"


' Debug
Dim t3
t3 = timer


' NEED to purge files acc to user sought files NOW, it takes TOO MUCH time to check in ALL of these for raw existence!
If IsClipboard_AListOf_CouncilDocuments Then
    
    Debug.Print "Clipboard IS GSC file list, Purging..."
    
    Dim soughtProjects As String
    soughtProjects = Replace(Get_Clipboard_TextContents, vbCrLf, "|")
    
    ' and RECLEAN clipboard !
    Set_Clipboard_TextContents ("")
    
    For i = 0 To prepProjFdrs.count - 1
        If InStr(1, soughtProjects, GSCFileName_ToStandardName(prepProjFdrs.files(i), UseWFSuffix:="True")) = 0 Then
            prepProjFdrs.files(i) = ""
        End If
    Next i
    '
    
    ' Debug
    Dim t4
    t4 = timer
    
    Debug.Print "Purgin' unnecessary projects acc to clipboard took " & Format(CDbl(t4 - t3), "0.000") & " sec."
Else
    Debug.Print "Clipboard did NOT contain a file list! Showing all raw-alignments containing projects!"
    If IsEmpty(t4) Then t4 = timer
    
End If


' Debug
Dim t5
t5 = timer


Dim tProj As Integer
Dim ctProj As foundFilesStruct
Dim fSize As String     ' formatted size


Dim fso As New Scripting.FileSystemObject

' precautionary, otherwise we cannot ewfresh with same routine!
Me.ListView1.ListItems.Clear

For i = 0 To prepProjFdrs.count - 1
    
    'Debug.Assert i < 824
    'Debug.Print "i = " & i
    
    If prepProjFdrs.files(i) <> "" Then
    
        If Dir(ProjectsBaseFolder & prepProjFdrs.files(i) & "\" & rawFilesMask) <> "" Then
            
            tProj = tProj + 1
            
            ' add folder name to first column
            Me.ListView1.ListItems.Add Text:=prepProjFdrs.files(i)
            
            ctProj = bGDirAll(FileSearchMask:=ProjectsBaseFolder & prepProjFdrs.files(i) & "\" & rawFilesMask, getFileSizes:=True)
            
            fSize = ctProj.sizeInKb
            
            If Len(fSize) > 3 Then
                fSize = Round(fSize / 1024, 1) & " Mb"
                Me.ListView1.ListItems(tProj).ForeColor = wdColorRed    ' MegaBytes are red!
            Else
                fSize = Round(fSize, 1) & " Kb"
                
                If val(fSize) > 699 Then Me.ListView1.ListItems(tProj).ForeColor = wdColorOrange    ' MegaBytes are red!
                
            End If
            
            Me.ListView1.ListItems(tProj).ListSubItems.Add Text:=fSize
            
        End If
        
    End If
    
Next i



' TIMING, remove later!
Dim t6
t6 = timer
Debug.Print "Looking for raw files in all non-empty hits took " & Format(CDbl(t6 - t5), "0.000") & " sec."

' update found projects label for user info
Me.lblTotalProjects.Caption = Replace(Me.lblTotalProjects.Caption, "#", tProj)


Dim t7
t7 = timer

' NO empty listview to user, not polite!
If Me.ListView1.ListItems.count = 0 Then Me.ListView1.ListItems.Add Text:="No projects found"


Debug.Print "Total processing time was: " & Format(CDbl(t7 - t1), "0.000") & " sec."

End Sub



Private Sub cmdOpenFolder_Click()

If Me.ListView1.ListItems.count > 0 Then
    If Me.ListView1.selectedItem.Text <> "" Then
        
        Dim tgFolder
        tgFolder = ProjectsBaseFolder & Me.ListView1.selectedItem.Text & backgroundFolder
        
        Call Shell("explorer.exe " & tgFolder, vbNormalFocus)
        
    End If
End If

End Sub







' shg 25 Feb 2008
'
'  Arial, Consolas, Calibri, Tahoma, Lucida Console, and Times New Roman fonts
'
Function StrWidth(s As String, sFontName As String, fFontSize As Double) As Double
     ' Returns the approximate width in points of a text string
     ' in a specified font name and font size
     
     ' Does not account for kerning
     
    Dim i       As Long
    Dim j       As Long
     
    If Len(sFontName) = 0 Then Exit Function
    If sFontName <> msFontName Then
        If Not InitChrWidths(sFontName) Then Exit Function
    End If
     
    For i = 1 To Len(s)
        j = Asc(Mid(s, i, 1))
        If j >= 32 Then
            StrWidth = StrWidth + fFontSize * mafChrWid(j)
        End If
    Next i
End Function


Function InitChrWidths(sFontName As String) As Boolean
    Dim i       As Long
     
    Select Case sFontName
    Case "Arial"
        For i = 32 To 127
            Select Case i
            Case 39, 106, 108
                mafChrWid(i) = 0.1902
            Case 105, 116
                mafChrWid(i) = 0.2526
            Case 32, 33, 44, 46, 47, 58, 59, 73, 91 To 93, 102, 124
                mafChrWid(i) = 0.3144
            Case 34, 40, 41, 45, 96, 114, 123, 125
                mafChrWid(i) = 0.3768
            Case 42, 94, 118, 120
                mafChrWid(i) = 0.4392
            Case 107, 115, 122
                mafChrWid(i) = 0.501
            Case 35, 36, 48 To 57, 63, 74, 76, 84, 90, 95, 97 To 101, 103, 104, 110 To 113, 117, 121
                mafChrWid(i) = 0.5634
            Case 43, 60 To 62, 70, 126
                mafChrWid(i) = 0.6252
            Case 38, 65, 66, 69, 72, 75, 78, 80, 82, 83, 85, 86, 88, 89, 119
                mafChrWid(i) = 0.6876
            Case 67, 68, 71, 79, 81
                mafChrWid(i) = 0.7494
            Case 77, 109, 127
                mafChrWid(i) = 0.8118
            Case 37
                mafChrWid(i) = 0.936
            Case 64, 87
                mafChrWid(i) = 1.0602
            End Select
        Next i
         
    Case "Consolas"
        For i = 32 To 127
            Select Case i
            Case 32 To 127
                mafChrWid(i) = 0.5634
            End Select
        Next i
         
    Case "Calibri"
        For i = 32 To 127
            Select Case i
            Case 32, 39, 44, 46, 73, 105, 106, 108
                mafChrWid(i) = 0.2526
            Case 40, 41, 45, 58, 59, 74, 91, 93, 96, 102, 123, 125
                mafChrWid(i) = 0.3144
            Case 33, 114, 116
                mafChrWid(i) = 0.3768
            Case 34, 47, 76, 92, 99, 115, 120, 122
                mafChrWid(i) = 0.4392
            Case 35, 42, 43, 60 To 63, 69, 70, 83, 84, 89, 90, 94, 95, 97, 101, 103, 107, 118, 121, 124, 126
                mafChrWid(i) = 0.501
            Case 36, 48 To 57, 66, 67, 75, 80, 82, 88, 98, 100, 104, 110 To 113, 117, 127
                mafChrWid(i) = 0.5634
            Case 65, 68, 86
                mafChrWid(i) = 0.6252
            Case 71, 72, 78, 79, 81, 85
                mafChrWid(i) = 0.6876
            Case 37, 38, 119
                mafChrWid(i) = 0.7494
            Case 109
                mafChrWid(i) = 0.8742
            Case 64, 77, 87
                mafChrWid(i) = 0.936
            End Select
        Next i
         
    Case "Tahoma"
        For i = 32 To 127
            Select Case i
            Case 39, 105, 108
                mafChrWid(i) = 0.2526
            Case 32, 44, 46, 102, 106
                mafChrWid(i) = 0.3144
            Case 33, 45, 58, 59, 73, 114, 116
                mafChrWid(i) = 0.3768
            Case 34, 40, 41, 47, 74, 91 To 93, 124
                mafChrWid(i) = 0.4392
            Case 63, 76, 99, 107, 115, 118, 120 To 123, 125
                mafChrWid(i) = 0.501
            Case 36, 42, 48 To 57, 70, 80, 83, 95 To 98, 100, 101, 103, 104, 110 To 113, 117
                mafChrWid(i) = 0.5634
            Case 66, 67, 69, 75, 84, 86, 88, 89, 90
                mafChrWid(i) = 0.6252
            Case 38, 65, 71, 72, 78, 82, 85
                mafChrWid(i) = 0.6876
            Case 35, 43, 60 To 62, 68, 79, 81, 94, 126
                mafChrWid(i) = 0.7494
            Case 77, 119
                mafChrWid(i) = 0.8118
            Case 109
                mafChrWid(i) = 0.8742
            Case 64, 87
                mafChrWid(i) = 0.936
            Case 37, 127
                mafChrWid(i) = 1.0602
            End Select
        Next i
         
    Case "Lucida Console"
        For i = 32 To 127
            Select Case i
            Case 32 To 127
                mafChrWid(i) = 0.6252
            End Select
        Next i
         
    Case "Times New Roman"
        For i = 32 To 127
            Select Case i
            Case 39, 124
                mafChrWid(i) = 0.1902
            Case 32, 44, 46, 59
                mafChrWid(i) = 0.2526
            Case 33, 34, 47, 58, 73, 91 To 93, 105, 106, 108, 116
                mafChrWid(i) = 0.3144
            Case 40, 41, 45, 96, 102, 114
                mafChrWid(i) = 0.3768
            Case 63, 74, 97, 115, 118, 122
                mafChrWid(i) = 0.4392
            Case 94, 98 To 101, 103, 104, 107, 110, 112, 113, 117, 120, 121, 123, 125
                mafChrWid(i) = 0.501
            Case 35, 36, 42, 48 To 57, 70, 83, 84, 95, 111, 126
                mafChrWid(i) = 0.5634
            Case 43, 60 To 62, 69, 76, 80, 90
                mafChrWid(i) = 0.6252
            Case 65 To 67, 82, 86, 89, 119
                mafChrWid(i) = 0.6876
            Case 68, 71, 72, 75, 78, 79, 81, 85, 88
                mafChrWid(i) = 0.7494
            Case 38, 109, 127
                mafChrWid(i) = 0.8118
            Case 37
                mafChrWid(i) = 0.8742
            Case 64, 77
                mafChrWid(i) = 0.936
            Case 87
                mafChrWid(i) = 0.9984
            End Select
        Next i
         
    Case Else
        MsgBox "Font name """ & sFontName & """ not available!", vbCritical, "StrWidth"
        Exit Function
    End Select
    msFontName = sFontName
    InitChrWidths = True
End Function


'=====================================================================
' Title: ListView_SetColumnWidth
'
' Purpose: Sends a LVM_SETCOLUMNWIDTH message to the listview control to
' set its column width
' for List and Report views
'
' Parameters:
'      lvw  - ListView control object where the column width will be set
'
'     [iCol] - Specifies the column for which the width will be set
'              only valid in Report view. Ignored for List view.
'
'     [vTxt] - Optional variant containing either a numeric value
'              representing the desired column width (in pixels) or a text
'              string where the column width will be set to match the width
'              of the passed in text string.
'
'              If in List view this parameter is not provided, then the
'              column width is set to the width of the ListView (lvw)
'              control.
'
'              If in Report view this parameter is not provided, the column
'              width is not set.
'=====================================================================

   
Sub ListView_SetColumnWidth(ByVal lvw As ListView, _
                            Optional ByVal iCol, _
                            Optional ByVal vTxt)

Const LVSCW_AUTOSIZE = -1
Const LVSCW_AUTOSIZE_USEHEADER = -2

Const LVM_FIRST = &H1000
Const LVM_GETCOLUMNWIDTH = LVM_FIRST + 29
Const LVM_SETCOLUMNWIDTH = LVM_FIRST + 30

Dim sm As Integer
Dim lWidth As Long

'
' Abort if the control's hWnd isn't set or if is not in
' the List or Report views.

If lvw.hWnd = 0& Or (lvw.View <> lvwList And lvw.View <> lvwReport) Then
Exit Sub
End If

'
' Remember the ScaleMode of the Form
' and set Form.ScaleMode to Pixels to match the ListView's intrinsic
' ScaleMode
'
'sm = lvw.Parent.ScaleMode
'lvw.Parent.ScaleMode = vbPixels

If IsMissing(iCol) Then
    If lvw.View = lvwList Then
        iCol = 0
    ElseIf lvw.View = lvwReport Then
        '
        'iCol must be provided in report view
        '
        lvw.Parent.ScaleMode = sm
        Exit Sub
    End If
End If

'
' Set the desired column width
' If a text string is provided set the column width
' based on the text
'
If IsMissing(vTxt) Then

vTxt = ""
     If lvw.View = lvwList Then
          '
          ' 5 millimeters (via the ScaleX statement) is tacked on to
          ' compensate for the 5 millimeter border that the parent
          ' class applies to the column width.
          '
          'lWidth = lvw.Width + ScaleX(5, vbMillimeters, vbPixels)
          lWidth = lvw.Width + PointsToPixels(MillimetersToPoints(5))
     ElseIf lvw.View = lvwReport Then
          '
          ' The vTxt parameter must be provided in Report view
          '
          lvw.Parent.ScaleMode = sm
          Exit Sub
     End If
Else
'
     ' 5 millimeters (via the ScaleX statement) is tacked on
     ' to compensate for the 5 millimeter border that the parent class
     ' applies to the column width
     '
     If IsNumeric(vTxt) Then
      '
      ' Numeric width is provided
      '
          'lWidth = vTxt + ScaleX(5, vbMillimeters, vbPixels)
          lWidth = vTxt + PointsToPixels(MillimetersToPoints(5))
     Else
          'lWidth = TextWidth(vTxt) + ScaleX(5, vbMillimeters, vbPixels)
          lWidth = PointsToPixels(StrWidth(CStr(vTxt), "Tahoma", 11)) + PointsToPixels(MillimetersToPoints(5))
     End If
End If

SendMessage ListView1.hWnd, LVM_SETCOLUMNWIDTH, CLng(iCol), lWidth
'lvw.Parent.ScaleMode = sm

End Sub


Private Sub Command1_Click()
   Dim Item As ListItem
   ListView1.View = lvwList
   Set Item = ListView1.ListItems.Add
   Item.Text = "This is a very long line of text"
   DoEvents
   ListView_SetColumnWidth ListView1

   ' To Run this in Visual Basic 4.0, comment out the above lines
   ' and uncomment the following lines
   '
   ' Dim Item As ListItem
   ' ListView1.View = lvwList
   ' ListView_SetColumnWidth ListView1
   ' Set Item = ListView1.ListItems.Add
   ' Item.Text = "This is a very long line of text"
End Sub

Private Sub UserForm_Layout()

'ListView_SetColumnWidth Me.ListView1, vTxt:="sn01234-re01ad01.en15"

End Sub
