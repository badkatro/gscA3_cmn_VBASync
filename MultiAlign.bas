Attribute VB_Name = "MultiAlign"
'************************************* TEMPORARY *************************************************
Public m, s, u As Byte                              ' Alinieri_Multiple
Public vv As Byte                                   ' Alinieri_Multiple
Public BaseDTime As String                          ' Alinieri_Multiple
Public CurrDTime As String                          ' Alinieri_Multiple
Private ExtraResults() As String                    ' Alinieri_Multiple
Public PosResults() As String                       ' Alinieri_Multiple
Private BaseCCell As Cell                           ' Alinieri_Multiple
Private BaseCCount As Byte                          ' Alinieri_Multiple
'************************************* TEMPORARY *************************************************


Const MProdBaseFolder As String = "M:\PROD\"

Public Type rawalignments
    rawAlignmentsNames() As String
    totalRawAlignmentsSize As Long
    rawAlignmentsCount As Integer
End Type

'******************************************************************************************************************************************
'********************************************* ROUTINES FOR NEW, PLANNED, MULTIPLE ALIGNMENTS CHECKING PROGRAM ****************************
'*********************************************************************** BEGIN ************************************************************
'
Function getAlignmentFolderOnT(Optional GSCFilename) As String

' 0.3 - modified to not bother the M drive to get the actual original language of original doc, who cares?
' After failing to find alignment folder first, it uses "*" joker to dir unknown language folder on "T:\Alignment Projects"

' Returns empty string for not found, folder name for found
'

' Safeguards... if not given optional parameters, it exits if opened doc is NOT a GSC doc
If IsMissing(GSCFilename) Then
    If Documents.count > 0 Then
        If Not Is_GSC_Doc(ActiveDocument.Name) Then
            StatusBar = "Check Alignment Folder: Current document NOT Council, Exiting..."
            getAlignmentFolderOnT = ""
            Exit Function
        Else
            Dim docToCheckBasename As String
            docToCheckBasename = getGSCBaseName(ActiveDocument.Name)
        End If
    Else
        StatusBar = "Check Alignment Folder: NO opened document, Exiting..."
        getAlignmentFolderOnT = ""
        Exit Function
    End If
Else
    docToCheckBasename = getGSCBaseName(CStr(GSCFilename))
End If


' In case supplied parameter or active doc is in unit's language, change to original language
Dim unitLang As String
'unitLang = LCase(Mid$(Environ("computername"), 3, 2))
unitLang = Get_ULg

' Perhaps to add supplimental measures to safeguard unit language
If Not IsValidLangIso(unitLang) Then
    StatusBar = "Check Alignment Folder: Failed to extract unit language from Computer name, Exiting..."
    getAlignmentFolderOnT = ""
    Exit Function
End If


' After building probable file find string
If Dir(ProjectsBaseFolder & docToCheckBasename, vbDirectory) <> "" Then
    
    'StatusBar = "Check Alignment Folder: NO opened document, Exiting..."
    getAlignmentFolderOnT = ProjectsBaseFolder & docToCheckBasename & backgroundFolder
Else
    
    ' Even if failed to find alignm folder and also having passed the unit language replacement test...
    ' we still try with * joker!
    If Dir(ProjectsBaseFolder & Replace(docToCheckBasename, unitLang, "*"), vbDirectory) <> "" Then
        
        Dim prjFolder As String
        prjFolder = Dir(ProjectsBaseFolder & Replace(docToCheckBasename, unitLang, "*"), vbDirectory)
        
        ' Now that we found a project folder, let's try for the background folder existence
        If Dir(ProjectsBaseFolder & prjFolder & backgroundFolder, vbDirectory) <> "" Then
            getAlignmentFolderOnT = ProjectsBaseFolder & prjFolder & backgroundFolder
        Else    ' No such folder
            StatusBar = "Check Alignment Folder: NO alignment folder on, Exiting..."
            getAlignmentFolderOnT = ""
        End If
    Else    ' No found'em project folder
        StatusBar = "Check Alignment Folder: NO alignment folder on, Exiting..."
        getAlignmentFolderOnT = ""
    End If
End If


End Function


Function hasAlignmentFolderOnT(GSCFilename As String) As Boolean

If getAlignmentFolderOnT(GSCFilename) = "" Then
    hasAlignmentFolderOnT = False
Else
    hasAlignmentFolderOnT = True
End If

End Function

Function getExportFolder(Optional GSCFilename) As String
' Verifies existence of exports folder inside of project folder for current document or supplied document

' Safeguards... if not given optional parameters, it exits if opened doc is NOT a GSC doc
If IsMissing(GSCFilename) Then
    If Documents.count > 0 Then
        If Not Is_GSC_Doc(ActiveDocument.Name) Then
            StatusBar = "Check Exports Folder: Current document NOT Council, Exiting..."
            getExportFolder = ""
            Exit Function
        Else
            Dim docToCheckBasename As String
            docToCheckBasename = getGSCBaseName(ActiveDocument.Name)
        End If
    Else
        StatusBar = "Check Exports Folder: NO opened document, Exiting..."
        getExportFolder = ""
        Exit Function
    End If
Else
    docToCheckBasename = getGSCBaseName(CStr(GSCFilename))
End If


' In case supplied parameter or active doc is in unit's language, change to original language
Dim unitLang As String
'unitLang = LCase(Mid$(Environ("computername"), 3, 2))
unitLang = Get_ULg

' Perhaps to add supplimental measures to safeguard unit language
If Not IsValidLangIso(unitLang) Then
    StatusBar = "Check Exports Folder: Failed to extract unit language from Computer name, Exiting..."
    getExportFolder = ""
    Exit Function
End If

' After building probable file find string
If Dir(ProjectsBaseFolder & docToCheckBasename, vbDirectory) <> "" Then
    
    If Dir(ProjectsBaseFolder & docToCheckBasename & exportsFolder, vbDirectory) <> "" Then
        'StatusBar = "Check Alignment Folder: NO opened document, Exiting..."
        getExportFolder = ProjectsBaseFolder & docToCheckBasename & exportsFolder
    Else
        getExportFolder = ""
    End If
    
Else
    
    ' Even if failed to find alignm folder and also having passed the unit language replacement test...
    ' we still try with * joker!
    If Dir(ProjectsBaseFolder & Replace(docToCheckBasename, unitLang, "*"), vbDirectory) <> "" Then
        
        Dim prjFolder As String
        prjFolder = Dir(ProjectsBaseFolder & Replace(docToCheckBasename, unitLang, "*"), vbDirectory)
        
        ' Now that we found a project folder, let's try for the background folder existence
        If Dir(ProjectsBaseFolder & prjFolder & exportsFolder, vbDirectory) <> "" Then
            getExportFolder = ProjectsBaseFolder & prjFolder & exportsFolder
        Else    ' No such folder
            StatusBar = "Check Alignment Folder: NO alignment folder on T, Exiting..."
            getExportFolder = ""
        End If
    Else    ' No found'em project folder
        StatusBar = "Check Alignment Folder: NO alignment folder on T, Exiting..."
        getExportFolder = ""
    End If
End If


End Function

Sub try_DebugDisplay_rawAlignmentInfo(GSCFilename As String)

Dim alf As String
alf = getAlignmentFolderOnT(GSCFilename)

If alf <> "" Then
    
    Dim toPrintString As String
    toPrintString = "FOUND Alignment Project for " & GSCFilename & " with following files:"
    
    Dim rawAlignmentsResult As rawalignments
    
    rawAlignmentsResult = get_RawAlignmentFiles_NamesAndTotalSize(GSCFilename)
    
    
    If rawAlignmentsResult.rawAlignmentsCount > 0 Then
        
        For i = 1 To rawAlignmentsResult.rawAlignmentsCount
            toPrintString = toPrintString & " " & rawAlignmentsResult.rawAlignmentsNames(i - 1)
        Next i
        
        Debug.Print toPrintString
        Debug.Print "Total file size is " & rawAlignmentsResult.totalRawAlignmentsSize \ 1024 & " kilobytes"
        
    Else
        Debug.Print "FOUND Alignment Project for " & GSCFilename & ", but NO RAW files!"
    End If
    
    
Else
    Debug.Print "NO Alignment Project found for " & GSCFilename
End If

End Sub

Function get_RawAlignmentFiles_NamesAndTotalSize(GSCFilename As String) As rawalignments
' rawAlignments - custom data type, consisting of a dynamic array os strings and a long (64 bit) integer
' The array is supposed to store file names of a certain type, the number - their total file size in bytes

Dim alignmentsProjectFolder As String

alignmentsProjectFolder = getAlignmentFolderOnT(GSCFilename)


If alignmentsProjectFolder = "" Then
    get_RawAlignmentFiles_NamesAndTotalSize.totalRawAlignmentsSize = 0
    get_RawAlignmentFiles_NamesAndTotalSize.rawAlignmentsCount = 0
    Exit Function
Else
    
    ' Main business
    
    Dim rawFileName As String
    rawFileName = Dir(alignmentsProjectFolder & "\raw*.tmx")
    Dim rawFileSize As Long
    
    
    If rawFileName <> "" Then
        
        Dim rawCounter As Integer
                
        rawFileSize = FileLen(alignmentsProjectFolder & "\" & rawFileName)
        
        ReDim get_RawAlignmentFiles_NamesAndTotalSize.rawAlignmentsNames(rawCounter)
        
        get_RawAlignmentFiles_NamesAndTotalSize.rawAlignmentsNames(rawCounter) = rawFileName
        get_RawAlignmentFiles_NamesAndTotalSize.rawAlignmentsCount = rawCounter + 1
        get_RawAlignmentFiles_NamesAndTotalSize.totalRawAlignmentsSize = rawFileSize
        
        rawFileName = Dir
        
        ' now add rest of files in loop
        Do While rawFileName <> ""
            
            rawCounter = rawCounter + 1
            rawFileSize = FileLen(alignmentsProjectFolder & "\" & rawFileName)

            ReDim Preserve get_RawAlignmentFiles_NamesAndTotalSize.rawAlignmentsNames(rawCounter)
            
            get_RawAlignmentFiles_NamesAndTotalSize.rawAlignmentsNames(rawCounter) = rawFileName
            get_RawAlignmentFiles_NamesAndTotalSize.rawAlignmentsCount = rawCounter + 1
            get_RawAlignmentFiles_NamesAndTotalSize.totalRawAlignmentsSize = get_RawAlignmentFiles_NamesAndTotalSize.totalRawAlignmentsSize + rawFileSize
            
            rawFileName = Dir
            
        Loop
        
    Else    ' Alignment Project folder not found, exiting
        get_RawAlignmentFiles_NamesAndTotalSize.totalRawAlignmentsSize = 0
        get_RawAlignmentFiles_NamesAndTotalSize.rawAlignmentsCount = 0
        Exit Function
    End If
    
End If

Exit Function

'_______________________________________________________________________________________
'
ErrorSettingFolder:
    If Err.Number <> 0 Then
        
        If Err.Number = 76 Then
            Debug.Print "get_RawAlignmentFiles_NamesAndTotalSize Error attempting to set folder object to " & alignmentsProjectFolder
        End If
        
        get_RawAlignmentFiles_NamesAndTotalSize.totalRawAlignmentsSize = 0
        Err.Clear
        Exit Function
        
    End If

End Function


Function IsClipboard_AListOf_CouncilDocuments() As Boolean      ' Weird name, ha ? But descriptive enough ... ! :)
' Useful in a specific context, need to know if a list of Council document names represents the text portion of the clipboard or not !

Dim clipText As String
Dim gscDocNames As Variant

If Get_Clipboard_TextContents <> "" Then
    
    clipText = Get_Clipboard_TextContents
    
    ' remove non-alpha numeric chars from beginning and end of input string
    clipText = TrimNonAlphaNums(clipText)
    
    If InStr(1, clipText, vbCr) > 0 Then
        gscDocNames = Split(Trim(clipText), vbCr)
        
        If IsArray(gscDocNames) Then
            If UBound(gscDocNames) > 0 Then
                ' Clean strings from vbcr
                For j = 0 To UBound(gscDocNames)
                    gscDocNames(j) = Replace(Replace(gscDocNames(j), Chr(13), ""), Chr(10), "")
                Next j
                
                ' HERE, iterate through all elements in gscDocNames to check if they're ALL GSC documents or not
                For j = 0 To UBound(gscDocNames)
                    If Is_GSC_Doc(GSC_StandardName_ToFileName(CStr(gscDocNames(j)))) Then
                        IsClipboard_AListOf_CouncilDocuments = True
                    Else
                        IsClipboard_AListOf_CouncilDocuments = False
                        Exit Function
                    End If
                Next j
                
            Else
                MsgBox "gscDocNames Array is empty ! Please debug !" & vbCrCr & _
                    "Err number " & Err.Number & " occured in " & Err.Source & vbCr & Err.Description, vbOKOnly + vbCritical, "Error"
                
                IsClipboard_AListOf_CouncilDocuments = False
                Exit Function
            End If
        Else
            ' Error ?
        End If
    Else
        ' No enters in clipboard string ? Not multiple docs then, how about a single one?
        gscDocNames = Replace(Replace(clipText, Chr(13), ""), Chr(10), "")
        
        If Not gscDocNames = "" Then
            If Is_GSC_Doc(GSC_StandardName_ToFileName(CStr(gscDocNames))) Then
                IsClipboard_AListOf_CouncilDocuments = True
            Else
                IsClipboard_AListOf_CouncilDocuments = False
            End If
        Else
            IsClipboard_AListOf_CouncilDocuments = False
        End If
        
    End If
    
Else
    ' MsgBox "Clipboard contains no text, sorry. Please re-copy the needed text", vbOKOnly + vbCritical, _
        "Error, no text in clipboard"
    ' Function returns False
    IsClipboard_AListOf_CouncilDocuments = False
End If

End Function



Sub Test_Clipboard_GSCDocs_Language_Retrieval()

Dim documentsInClipboard As String  ' how long can string be ? 256 chars I presume? Look into it !
Dim gscDocsArr As Variant

Dim ctDocMProdPath As String
Dim fso As New Scripting.FileSystemObject

' check user has copied documents list into clipboard
If IsClipboard_AListOf_CouncilDocuments Then
    
    ' retrieve it as text
    documentsInClipboard = Get_Clipboard_TextContents
    ' and clear clipboard
    Clear_Clipboard_TextContents
    
    ' split it and put it into string array
    gscDocsArr = Split(documentsInClipboard, vbCr)
    
    For i = 0 To ubouund(gscDocsArr)
        ctDocMProdPath = Build_MProd_Path(gscDocsArr(i))    ' Modify Build_MProd_Path, it needs to add path to file name (M:\Prod\13\st13 for instance)
        
        If getMProdFilename(ctDocMProdPath) <> "" Then      ' Write function which actually looks for file into proper folder, USE Dir, since it can accept wildcards.
                                                            ' IDEA --- why not use ?? instead of "xx" for default language code into previous function?
        Else                                                ' You can use it as file mask directly with Dir
                                                            ' We could even finally build an "string intersection" function... to show only diff in 2 strings !
            
            'Could not find MProd doc for current GSC Doc, which is valid in format. WHAT DO ? (Skip it?)
            '
            
        End If
        
    Next i
    
Else
    MsgBox "Clipboard is NOT a list of GSC documents!"
    Exit Sub
End If

End Sub

Function GetOriginalLanguage_forGSCDocuments(GSCDocumentsString As String, ByRef ResultsArray As Variant) As Boolean

End Function

Sub DebugPrint_Test()

Debug.Print Build_MProd_Path("SN 1125/12 REV 1")

End Sub






'
'**************************************************************** END *********************************************************************
'********************************************* ROUTINES FOR NEW, PLANNED, MULTIPLE ALIGNMENTS CHECKING PROGRAM ****************************
'******************************************************************************************************************************************


Sub Ruleaza_AlinieriM()
' version 0.8
' secondary to Alinieri_Multiple
'

frmRawTmxFinder.Show
'frm_Alinieri_Multiple2.Show

End Sub

Sub Alinieri_Multiple()
' version 0.8
' main routine
' depends on TblCreate, FindFFreeC, Compara, StdSufName, Extr_LBNum, Extr_Numbers
' Cauta si afiseaza intr-o lista tiparibila toate documentele in ale caror folder de alinieri (T:\Alignment Projects\...)
' se gasesc fisiere de memorii ale caror aliniere trebuie verificata/corectata (cu PEdit). Pentru coordonarea locala si
' asistenti care doresc sa sustina activitatea coordonarii locale.

Dim TFoldName As String, SufStr As String, DocCrt As String, Msg As String, DocType As String
Dim DocSuf As String, DocLang As String, docYear As String, DocStName As String, DocNum As String
Dim SlashPos As Byte, TabPos As Byte, i As Byte, j As Byte, k As Byte, DocNumL As Byte
Dim results() As String, SingleCN(10) As String
Dim Exista_Raw As Boolean, Verify As Boolean
Dim AlignSize As Long
Dim p As Byte, R As Byte


Dim NumSufCrt
Dim NumarLinii

Dim fs As Scripting.FileSystemObject
Set fs = New Scripting.FileSystemObject

Dim fso, f, ff, f1

For j = 1 To 10
    SingleCN(j) = j - 1
Next j

NumarLinii = ActiveDocument.Paragraphs.count
vv = 0
BaseDTime = ""
CurrDTime = ""


ReDim results(ActiveDocument.Paragraphs.count)

For i = 1 To (ActiveDocument.Paragraphs.count)
    ActiveDocument.Paragraphs(i).Range.Select
    DocCrt = Selection.Text
    SlashPos = InStr(1, DocCrt, "/")
    TabPos = InStr(1, DocCrt, vbTab)
    DocType = LCase(Left$(DocCrt, 2))
    DocNum = Mid$(DocCrt, 4, SlashPos - 4)
        If Len(DocNum) < 5 Then
            DocNum = String(5 - Len(DocNum), Chr(48)) & DocNum
        End If

    DocLang = LCase(Left$(Right$(DocCrt, 3), 2))
    docYear = Mid$(DocCrt, (SlashPos + 1), 2)

    If Mid$(DocCrt, SlashPos + 3, 1) <> vbTab Then
        SufStr = "-" & LCase(Mid$(DocCrt, SlashPos + 4, TabPos - (SlashPos + 4)))
        If gscrolib.Extr_LBNum(CStr(SufStr)) >= 2 Then
            NumSufCrt = gscrolib.Extr_Numbers(CStr(SufStr))
                For k = 1 To 4
                    If Len(NumSufCrt(k)) = 1 Then
                        SufStr = Replace(SufStr, NumSufCrt(k), "0" & NumSufCrt(k))
                    End If
                Next k
        Else
            For j = 1 To 10
                SufStr = Replace(SufStr, SingleCN(j), "0" & SingleCN(j))
            Next j
        End If
    
        If InStr(1, SufStr, "add") > 0 Or InStr(1, SufStr, "cor") > 0 Or InStr(1, SufStr, "ext") > 0 _
           Or InStr(1, SufStr, "rev") > 0 Then
                DocSuf = gscrolib.StdSufName(CStr(SufStr))
        Else
            DocSuf = SufStr
        End If
    Else
        DocSuf = ""
    End If
    
    DocStName = CStr(DocType & DocNum & DocSuf & "." & DocLang & docYear)
    results(i) = DocStName
Next i

Selection.Collapse
Selection.HomeKey (wdStory)

' Urmatorul bloc creaza un nou document si introduce in el resultatele sortate ale rutinei
' (lista cu numere de foldere standard care contin fisiere raw*.tmx) si printeaza si inchide documentul
' (pentru a face print unei liste mai lungi decat se poate afisa (mai mult de 50linii)

' Urmatoarea bucla sorteaza alfabetic elementele matricii "Results", daca optiunea corespondenta este activata.
' Utila in variate contexte
If frm_Alinieri_Multiple2.OptionButton2.Value = True Then
    Dim strTemp As String
    Dim X, Y As Integer
        For X = LBound(results) To (UBound(results) - 1)
            For Y = (X + 1) To UBound(results)
                If results(X) > results(Y) Then
                    strTemp = results(X)
                    results(X) = results(Y)
                    results(Y) = strTemp
                    strTemp = ""
                End If
            Next Y
        Next X
End If


Exista_Raw = False


If fs.FileExists("Q:\10-TOOLS\Word_Macros\alinieri_" & DatePart("m", Date) & "-" & _
    DatePart("d", Date) & ".doc") = False Then
    Documents.Add.SaveAs ("Q:\10-TOOLS\Word_Macros\alinieri_" & DatePart("m", Date) & "-" & DatePart("d", Date) _
    & ".doc")
    With ActiveDocument.PageSetup
        .Orientation = wdOrientLandscape
        .PageWidth = CentimetersToPoints(54)
        .PageHeight = CentimetersToPoints(54)
    End With
    Selection.HomeKey Unit:=wdStory
    Call TblCreate
Else
    Documents.Open ("Q:\10-TOOLS\Word_Macros\alinieri_" & DatePart("m", Date) & "-" & DatePart("d", Date) & ".doc")
End If

m = ActiveDocument.Tables.count


' Urmatorul bloc gaseste care este prima coloana libera a primului tabel
' din documentul curent si muta acolo cursorul, pentru a ne asigura ca nu suprascriem o coloana deja existenta
'
Call FindFFreeC
If Selection.Information(wdEndOfRangeColumnNumber) - 1 > 0 Then
    Set BaseCCell = ActiveDocument.Tables(m).Cell(2, (Selection.Information(wdEndOfRangeColumnNumber) - 1))
    BaseCCell.Select
    BaseCCount = Selection.Paragraphs.count
ElseIf m > 1 Then
    Set BaseCCell = ActiveDocument.Tables(m - 1).Cell(2, 10)
    BaseCCell.Select
    BaseCCount = Selection.Paragraphs.count
Else
    BaseCCount = 0
End If
     
i = 1
u = 0

frm_Alinieri_Multiple1.ListView1.Refresh
frm_Alinieri_Multiple1.ListView1.View = lvwReport

frm_Alinieri_Multiple1.ListView1.ColumnHeaders.Add Width:=140
frm_Alinieri_Multiple1.ListView1.ColumnHeaders.Add Width:=60

For i = 1 To NumarLinii
    Exista_Raw = False
    AlignSize = 0
    If fs.FolderExists("T:\Alignment Projects\" & CStr(results(i))) Then
        Set fso = fs.GetFolder("T:\Alignment Projects\" & CStr(results(i)))
        Set ff = fso.files
            For Each f1 In ff
                Verify = CStr(f1.Name) Like "raw*.tmx"
                    If Verify = True Then
                        Exista_Raw = True
                        AlignSize = AlignSize + f1.Size
                    End If
            Next
        If Exista_Raw = True Then
            u = u + 1
            ReDim Preserve PosResults(2, u)
            PosResults(1, u) = results(i)
            PosResults(2, u) = CStr(Round((AlignSize / 1024))) & " kb"
            
            frm_Alinieri_Multiple1.ListView1.ListItems.Add Text:=PosResults(1, u)
            frm_Alinieri_Multiple1.ListView1.ListItems(u).ListSubItems.Add Text:=PosResults(2, u)
        End If
    End If
Next i

o = 1: p = 1: R = 1: s = 0
    
If u = 0 Then
        MsgBox "Nu s-au gasit documente cu alinieri de facut!"
        Exit Sub
End If

If BaseCCount = 0 Or BaseCCount <> u Then
    Call FindFFreeC
    
    Selection.TypeText Text:=Date & vbCr & Time
    Selection.MoveDown Unit:=wdLine, count:=1
    For o = 1 To u
        Selection.TypeText (PosResults(1, o) & vbCr)
    Next o
    Selection.TypeBackspace
    
    Call Compara
Else
    Call Compara
    If s > 0 Then
        Call FindFFreeC
        Selection.TypeText Text:=Date & vbCr & Time
        Selection.MoveDown Unit:=wdLine, count:=1
            For o = 1 To u
                Selection.TypeText (PosResults(1, o) & vbCr)
            Next o
        Selection.TypeBackspace
    End If
End If

ActiveDocument.Close SaveChanges:=True

frm_Alinieri_Multiple1.Caption = "S-au gasit  " & u & "  documente cu alinieri de facut:"
If frm_Alinieri_Multiple2.OptionButton2.Value = True Then
        frm_Alinieri_Multiple1.Label3.Caption = "Rezultatele sunt sortate alfabetic dupa numele folderului/ documentului"
    Else
        frm_Alinieri_Multiple1.Label3.Caption = "Rezultatele sunt sortate dupa data de iesire din unitate (identic workflow)"
End If

frm_Alinieri_Multiple1.Show

'Set MyDoc = Documents.Open(filename:="tempalign.doc")
'
'For i = 1 To k
' Selection.TypeText (Results(i) & Chr(13))
'Next i
'
'ActiveDocument.PrintOut
'ActiveDocument.Close (SaveChanges)


' Msg = "Documentele dvs formatate" & Chr(13) & "dupa standardul Consiliului: " & Chr(13) & Chr(13)
' For i = 1 To ActiveDocument.Paragraphs.count
' Msg = Msg & Results(i) & Chr(13)
' Next i


'Sef fs = Nothing
Set fso = Nothing
Set f = Nothing
Set ff = Nothing
Set f1 = Nothing
    
End Sub
Sub Compara()
' version 0.5
' secondary to Alinieri_Multiple
'

Dim RExista As Boolean, RNuExista As Boolean
Dim PCount As Byte

CurrDTime = Date & vbCr & Time

If BaseCCount <> 0 Then
    BaseCCell.Select
    PCount = Selection.Paragraphs.count
    Selection.Collapse
    Selection.StartOf (wdColumn)
    Selection.Expand Unit:=wdCell
    Selection.MoveLeft Unit:=wdCharacter, count:=1, Extend:=wdExtend
    BaseDTime = Selection.Text
    BaseCCell.Select
    Selection.Collapse
    
    For R = 1 To u
        Selection.StartOf (wdCell)
        RExista = False
        RNuExista = False
        For p = 1 To PCount
            Selection.EndKey Unit:=wdLine, Extend:=wdExtend
            Selection.MoveLeft Unit:=wdCharacter, count:=1, Extend:=wdExtend
            
            If StrComp(PosResults(1, R), Selection.Text, vbTextCompare) = 0 Then
                RExista = True
                Exit For
                    Else
                RNuExista = True
            End If
        
            If p < PCount Then
                Selection.MoveDown Unit:=wdParagraph, count:=1
                Else
                    Selection.Collapse
            End If
        Next p
        If RExista = False Then
            s = s + 1
            ReDim Preserve ExtraResults(2, s)
            ExtraResults(1, s) = PosResults(1, R)
            ExtraResults(2, s) = PosResults(2, R)
            
            For w = 1 To frm_Alinieri_Multiple1.ListView1.ListItems.count
                If StrComp(ExtraResults(1, s), frm_Alinieri_Multiple1.ListView1.ListItems(w).Text, vbTextCompare) = 0 Then
                    frm_Alinieri_Multiple1.ListView1.ListItems(w).ForeColor = RGB(255, 4, 4)
                    frm_Alinieri_Multiple1.ListView1.ListItems(w).Bold = True
                    frm_Alinieri_Multiple1.ListView1.ListItems(w).ListSubItems(1).Bold = True
                    frm_Alinieri_Multiple1.ListView1.ListItems(w).ListSubItems(1).ForeColor = RGB(255, 4, 4)
                End If
            Next w
            
        End If
    Next R
Else
    Exit Sub
End If

On Error GoTo out
If UBound(ExtraResults, 2) >= 1 Then
    frm_Alinieri_Multiple1.Label4.Caption = "Documentele evidentiate cu rosu reprezinta noile alinieri -nu se regasesc in ultima cautare" _
    & vbCr & vbCr & "(" & BaseDTime & ")"
End If

out: Exit Sub

End Sub
Sub FindFFreeC()
' version 0.5
' secondary to Alinieri_Multiple
'

Dim l As Byte
Dim strCleaned As String, selectedText As String
Dim myCell As Cell

For l = 1 To ActiveDocument.Tables(m).Columns.count
    strCleaned = ""
    selectedText = ""
    Set myCell = ActiveDocument.Tables(m).Cell(Row:=1, Column:=l)
    myCell.Select
    Selection.MoveLeft Unit:=wdCharacter, count:=1, Extend:=wdExtend
    selectedText = Selection.FormattedText
    strCleaned = Application.CleanString(selectedText)
        If strCleaned <> "" And strCleaned <> Chr(13) Then
            If l < ActiveDocument.Tables(m).Columns.count Then
                Selection.MoveRight Unit:=wdCell
            ElseIf l = ActiveDocument.Tables(m).Columns.count Then
                Selection.EndKey Unit:=wdStory
                Selection.TypeParagraph
                Selection.TypeParagraph
                Call TblCreate
            End If
        Else
            Selection.Collapse
            Exit For
        End If
Next l

End Sub
Sub TblCreate()
' version 0.1
' secondary to Alinieri_Multiple
'

ActiveDocument.Tables.Add Range:=Selection.Range, NumRows:=2, NumColumns:= _
10, DefaultTableBehavior:=wdWord9TableBehavior, AutoFitBehavior:= _
wdAutoFitFixed
End Sub
