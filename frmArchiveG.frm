VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmArchiveG 
   OleObjectBlob   =   "frmArchiveG.frx":0000
   Caption         =   "Archive current document on G drive"
   ClientHeight    =   1980
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   6540
   StartUpPosition =   1  'CenterOwner
   Tag             =   "GSC_A3_Options"
   TypeInfoVer     =   14
End
Attribute VB_Name = "frmArchiveG"
Attribute VB_Base = "0{51525727-ADDA-4013-A91F-1B1317C0E2AA}{022F0861-AFF7-4B3D-A98B-FE52DF54F747}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False



Public NotSaved As Boolean          ' Current Document not saved?
Public archivePath As String        ' Path to save document to this folder
Public aDocument As String          ' Archive which document (filename format)

Public targetFileName As String


Private Sub CommandButton2_Click()
' Cancel
Unload Me

End Sub




Private Sub cbAutoPilot_Click()

' Set option to template settings file if not already there
Dim sFile As New SettingsFileClass

If Not sFile.Exists Then sFile.Create_Empty

If sFile.IsEmpty Then sFile.setOption Me.Tag, Me.cbAutoPilot.Tag, CStr(Me.cbAutoPilot.Value)

If Not sFile.getOption(Me.cbAutoPilot.Tag, Me.Tag) = CStr(Me.cbAutoPilot.Value) Then sFile.setOption Me.Tag, Me.cbAutoPilot.Tag, CStr(Me.cbAutoPilot.Value)

Set sFile = Nothing


End Sub

Private Sub cmdCancel_Click()
' Cancel
Unload Me

End Sub

Public Sub cmdOK_Click()
' OK button

Dim fso As New Scripting.FileSystemObject

Dim TargetFolder As String
Dim openedDoc As String



If Me.lblarchivePath.Caption <> "N/A" Then
    
    If archivePath = "" Then
        
        MsgBox "ArchiveG user form does NOT know which folder to user for archiving!" & vbCrCr & "Variable archivePath is empty!"
        Unload Me
        
    Else
        
        ' If AutoPilot is Off, first save document and then reset OK button and Form to proper state !
        If Me.cmdOK.Caption = "Save" Then
            
            ActiveDocument.Save
            
            Me.cmdOK.Caption = "OK"
            Me.Caption = Split(Me.Caption, " - ")(0)
            
        Else
            
            ' we copy target folder for archiving operation into local var
            TargetFolder = archivePath
            
            If fso.FolderExists(TargetFolder) Then
        
                ' Distinguish between no document opened or non-document opened and other cases
                If Documents.count = 0 Then
                    
                    openedDoc = Split(latestOpenedDocs, ",")(UBound(Split(latestOpenedDocs, ",")))
                    openedDoc = GSC_StandardName_WithLang_toFileName(openedDoc)
                    
                    Dim pfpath As String
                    pfpath = getProjectFolderOf(openedDoc)
                    
                    Dim origlgyear As String
                    Dim cttarglgyear As String
                    
                    origlgyear = Split(pfpath, ".")(1)
                    cttarglgyear = Split(openedDoc, ".")(1)
                    
                    Dim uLg As String       ' unit language
                    
                    uLg = Get_ULg
                    
                    ' Instead of hard coding...
                    If uLg <> "" Then
                        targetFileName = TargetFolder & "\" & Replace(openedDoc, cttarglgyear, origlgyear) & "." & uLg & ".xlf"
                    Else
                        MsgBox "Could not retrieve unit language from computer name!", vbOKOnly + vbCritical, "ArchiveG Error"
                        Unload Me
                        Exit Sub    ' Poss not necessary?
                    End If
    
                Else    ' opened document in Word
                                       
                    ' but txt web document or template (debugging) or non-saved document
                    If Right(ActiveDocument.Name, 3) = "dot" Or Right(ActiveDocument.Name, 4) = "dotm" Or _
                        Right(ActiveDocument.Name, 3) = "txt" Or ActiveDocument.Name Like "Document#" Or ActiveDocument.Name Like "Document##" Then
                            
                        openedDoc = Split(latestOpenedDocs, ",")(UBound(Split(latestOpenedDocs, ",")))
                        openedDoc = GSC_StandardName_WithLang_toFileName(openedDoc)
                            
                        pfpath = getProjectFolderOf(openedDoc)
                                            
                        origlgyear = Split(pfpath, ".")(1)
                        cttarglgyear = Split(openedDoc, ".")(1)
                        
                        uLg = Get_ULg
                       
                        targetFileName = TargetFolder & "\" & Replace(openedDoc, cttarglgyear, origlgyear) & "." & uLg & ".xlf"
                        
                    Else    ' real word document opened
                    
                        openedDoc = aDocument
                        
                        If (Right(openedDoc, 4) = ".doc") Then
                            targetFileName = TargetFolder & "\" & Replace(openedDoc, ".doc", ".docx")
                        Else
                            targetFileName = TargetFolder & "\" & openedDoc
                        End If
                        
                    End If
                    
                End If
                
                Me.Hide
                
                ' no document opened in Word
                If Documents.count = 0 Then
                    
                    Dim spath As String
                    spath = Get_Target_XLFPath(openedDoc)
                    
                    If spath = "" Then
                        MsgBox "File " & openedDoc & " was NOT found under T:\Prepared originals!"
                        Exit Sub
                    End If
                    
                    fso.CopyFile spath, targetFileName, True
                    
                    If fso.FileExists(targetFileName) Then
                        StatusBar = "ArchiveG: Document " & spath & " has been archived as " & targetFileName
                    End If
                    
                Else    ' opened doc in word
                    
                    ' but txt representation of web document
                    If Right(openedDoc, 3) = "dot" Or Right(openedDoc, 4) = "dotm" Or Right(openedDoc, 3) = "txt" Or _
                        openedDoc Like "Document#" Or openedDoc Like "Document##" Or (Right(openedDoc, 4) <> ".doc" And Right(openedDoc, 5) <> ".docx") Then
                        
                        spath = Get_Target_XLFPath(openedDoc)
                        
                        If spath = "" Then
                            MsgBox "File " & openedDoc & " was NOT found under T:\Prepared originals!"
                            Exit Sub
                        End If
                        
                        fso.CopyFile spath, targetFileName, True
                        
                        If fso.FileExists(targetFileName) Then
                            StatusBar = "ArchiveG: Document " & spath & " has been archived on G"
                        End If
                        
                    Else    ' real document opened
                        
                        Dim toOpenAgainDoc As String
                        toOpenAgainDoc = Documents(openedDoc).FullName
                        
                        Documents(openedDoc).SaveAs2 FileName:=targetFileName, FileFormat:=wdFormatXMLDocument, AddToRecentFiles:=False, CompatibilityMode:=wdWord2010
                        
                        ' let user decide whether to re-open closed document when archiving it on G
                        If Me.cbAutoClose.Value = False Then
                        
                            Documents(openedDoc).Close SaveChanges:=False
                            
                            Application.Documents.Open toOpenAgainDoc, AddToRecentFiles:=False
                        
                            ActiveWindow.ActivePane.View.Type = wdNormalView
                            ActiveWindow.ActivePane.View.Type = wdPrintView
                        
                        End If
                        
                        StatusBar = "ArchiveG: A copy of document " & openedDoc & " has been archived at " & TargetFolder & "!"
                        
                    End If
                    
                    Unload Me
                    
                End If
                
            Else
                Me.Hide
                MsgBox "Target folder does not exist!" & vbCr & vbCr & _
                    "Please contact margama(2942)", vbOKOnly + vbCritical, "No such target folder error!"
                Unload Me
            End If
            
            
        End If
    
    End If
    
Else
    StatusBar = ""
    Unload Me
End If




End Sub



Public Sub UserForm_Activate()


Dim fso As New Scripting.FileSystemObject

If Me.NotSaved Then
    If Documents.count > 0 Then
        If Not ActiveDocument.Saved Then
            
            Me.cmdOK.Caption = "Save"
            Me.Caption = Me.Caption & " - Please save current document!"
        
            Me.Repaint
            
        End If
    Else ' ????????????????????????
        
    End If
End If

If archivePath <> "" Then
    
    ' we were passed a destination folder, display it for user
    Me.lblarchivePath.Caption = archivePath
    
    Dim TargetFolder As String
    ' and save it in a variable for later usage
    If Me.lblarchivePath <> "N/A" Then TargetFolder = Me.lblarchivePath.Caption
        
    
    Dim ctdocument As String
    
    ' When trying to archive web documents, we don't normally have anything opened
    If Documents.count = 0 Then

StillEmpty:

        If aDocument <> "" Then
        
            ctdocument = aDocument
            'ctdocument = GSC_StandardName_WithLang_toFileName(aDocument)
            
            ' Check whether we're not trying to archive someth in another lang thatn unit language !
            If Not Is_ULg_Doc(ctdocument) Then
                MsgBox "Trying to archive document NOT in unit language !", vbOKOnly + vbExclamation, "Error, wrong language code"
                Unload Me
            End If
            
'            Dim pfpath As String
'            pfpath = getProjectFolderOf(ctdocument)
'
'            If pfpath = "" Then
'                MsgBox "Could not retrieve the project folder path for " & ctdocument, vbOKOnly + vbCritical, "ArchiveG Error"
'                Unload Me
'                Exit Sub    ' Poss not necessary?
'            End If
'
'            Dim origlgyear As String
'            Dim cttarglgyear As String
'
'            origlgyear = Split(pfpath, ".")(1)
'            cttarglgyear = Split(ctdocument, ".")(1)
'
'            Dim ulg As String       ' unit language
'            ulg = Get_ULg
'
'            ' Instead of hard coding...
'            If ulg <> "" Then
'                targetFileName = targetFolder & "\" & Replace(ctdocument, cttarglgyear, origlgyear) & "." & ulg & ".xlf"
'            Else
'                MsgBox "Could not retrieve unit language from computer name!", vbOKOnly + vbCritical, "ArchiveG Error"
'                Unload Me
'                Exit Sub    ' Poss not necessary?
'            End If
            
        Else    ' document to archive string is empty... should not be, so late in the game !
            Exit Sub
        End If
        
    Else    ' We have a loaded document in Word
        
        'sanity Check, we 're relyin on this variable to go on...
        If aDocument = "" Then
            MsgBox "frmArchiveG user form does not know which document to archive!" & vbCrCr & "aDocument variable is empty!"
            Exit Sub
        End If
                
        ' if document indicated is still opened, we proceed
        If Not Found_Opened_Document(aDocument) Then
            
            '
            If Right(aDocument, 3) = "dot" Or Right(aDocument, 4) = "dotm" Or _
                aDocument Like "Document#" Or aDocument Like "Document##" Then
                
                ' have an opened document, but only a template, or non-saved doc, treating as if empty
                GoTo StillEmpty
                
            Else    ' is opened document non-signifiant?
                
                ' Have an opened document, it's not found as opened doc, so... we're on a "still empty" scenario !
                If Right(ActiveDocument.Name, 3) = "dot" Or Right(ActiveDocument.Name, 4) = "dotm" Or _
                    ActiveDocument.Name Like "Document#" Or ActiveDocument.Name Like "Document##" Then
                    
                    GoTo StillEmpty
                
                Else
                    
                    MsgBox "Document " & aDocument & " was expected to opened, but it is NOT! Aborting..."
                    Unload Me
                    
                End If

            End If

        Else    ' found required document, we continue
            
            ' is opened document non-signifiant?
            If Right(aDocument, 3) = "dot" Or Right(aDocument, 4) = "dotm" Or _
                aDocument Like "Document#" Or aDocument Like "Document##" Then
                
                ' have an opened document, but only a template, or non-saved doc, treating as if empty
                GoTo StillEmpty
                        
            Else    ' significant document
            
                ctdocument = aDocument
                
                ' check for allowed extensions (maybe need fix the ".copy" case ? - for GSCTools's GetDocument)
                If Right(ctdocument, 4) <> "docx" And Right(ctdocument, 3) <> "txt" And Right(ctdocument, 3) <> "doc" Then
                    MsgBox "Wrong file extension, please check file name !", vbOKOnly + vbExclamation, "Error, not allowed extension!"
                    Unload Me
                    Exit Sub
                Else    ' given allowed extension, check for unit language document archiving
                    If Not Is_ULg_Doc(ctdocument) Then
                        MsgBox "Document " & ctdocument & " is NOT a unit language doc!", vbOKOnly + vbExclamation, "Error, archiving not allowed"
                        Unload Me
                        Exit Sub
                    End If
                End If
                
                ' starting from February 2015, we use "docx" instead of doc. We're writing this code in Jan 2015 though...
                'If CInt(Split(Date, "/")(1)) < 2 And CInt(Split(Date, "/")(2)) = 2015 Then
                    'targetFileName = Replace(targetFolder & "\" & ctdocument, ".docx", ".doc")
                'Else
                    'targetFileName = targetFolder & "\" & ctdocument
                'End If
                
            End If
        
        End If
    
    End If
 
    ' Warn user for file existence on G drive (imminence of overwriting)
    If fso.FileExists(archivePath & "\" & aDocument) Then
        ' Show file existence warning to user, but only in form is visible situation!
        If Me.Visible = True Then
            Me.Height = 147
        Else    ' ArchiveG form not visible, so demand user confirmation
            
            Select Case MsgBox("File " & archivePath & "\" & aDocument & " ALREADY exists! " & vbCr & vbCr & _
                "OVERWRITE?" & vbCr & "Clicking No will use the existing version, while clicking Cancel will abort the Archiving operation!", vbYesNoCancel + vbQuestion, "Already Archived!")
            
                Case vbCancel
                    StatusBar = "ArchiveG: CANCELED!"
                    ' CANCEL everything !
                    End
                Case vbNo
                    Unload frmArchiveG
            End Select
            
        End If
    Else
        Me.Height = 128
    End If
    
    Me.Repaint
    
End If

Set fso = Nothing
End Sub


Private Sub UserForm_Initialize()


Dim ctSavedAutoClose As String
ctSavedAutoClose = "False"

Dim ctSavedState As String
ctSavedState = "OFF"    ' Default


Dim Settings_File As New SettingsFileClass

With Settings_File
    If Not .Exists Then .Create_Empty
    If Not .IsEmpty Then
        ctSavedState = .getOption(Me.cbAutoPilot.Tag, Me.Tag)
        ctSavedAutoClose = .getOption(Me.cbAutoClose.Tag, Me.Tag)
    End If
    
End With

Me.cbAutoPilot.Value = IIf(ctSavedState = "True", True, False)
Me.cbAutoClose.Value = IIf(ctSavedAutoClose = "True", True, False)


End Sub

Private Sub UserForm_Terminate()


archivePath = ""

End Sub
