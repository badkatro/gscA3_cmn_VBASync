Attribute VB_Name = "Formatter"
Public Const maxOrphanLine As Integer = 26     ' text paragraphs last lines with fewer chars than this number are consedered orphaned


Sub Formatter_Run()

Dim fSettings As New SettingsFileClass

If Not fSettings.Exists Then
    fSettings.Create_Empty
End If

If fSettings.Path <> "" Then

End If



End Sub


Sub Check_andRestore_Key_Combos()



End Sub



Sub Interline_Decrease()

Dim res As Single

res = fInterline_Decrease

End Sub


Function fInterline_Decrease(Optional TargetRange, Optional HandleSpaces) As Single
' returning difference between original interline of paragraph or range
' and just set value (normally -0.05 if we're still above 1.0, but also
' 0 when we're at 1.0 and 1 when we were irregular and had to set it to 1.5

' Optional parameter HandleSpaces is to be used when we want the routine to reduce
' extra large space befores and afters (the goal is not to do it on every call of kerning decrease - like on normal everyday docs)

Dim ctR As Range

If Not IsMissing(TargetRange) Then
    Set ctR = TargetRange
Else
    If Selection.Type = wdSelectionIP Then
        Set ctR = Selection.Paragraphs(1).Range
    ElseIf Selection.Type = wdSelectionNormal Then
        Set ctR = Selection.Range
    End If
End If


' Get current interline
Dim ctL As Single

If ctR.ParagraphFormat.LineSpacing <> 9999999 Then
    'Debug.Print ctR.ParagraphFormat.LineSpacing
    ctL = PointsToLines(ctR.ParagraphFormat.LineSpacing)
Else    ' there is NO unitary interline value for selected paragraphs...
    'ctL = PointsToLines(get_Interline_ofFirst_NEPar(ctR))
    ctL = 1.55  ' next iteration we'll descend to 1,5
End If


' Don't decrease under "Single" setting!
If ctL = 1# Then
    StatusBar = "Min interline reached!"
    fInterline_Decrease = 0
Else
    ' decrease by 0.1 lines
    ctR.ParagraphFormat.LineSpacing = LinesToPoints(ctL - 0.05)
    ' first eliminate space before and after for paragraphs and empties
    
    If Not IsMissing(HandleSpaces) Then
    
        If count_EmptyParagraphs_inRange(ctR) = 0 Then
            
            If ctL >= 1.4 And ctL <= 1.5 Then
                Call set_Spaces_forParagraphs(get_NonEmptyParagraphs_inRange(ctR), "3", "12,6")
                fInterline_Decrease = 1
                Exit Function
            
            ElseIf ctL > 1.25 And ctL < 1.4 Then
                Call set_Spaces_forParagraphs(get_NonEmptyParagraphs_inRange(ctR), "3", "9,6")
            
            ElseIf ctL <= 1.25 Then
                Call set_Spaces_forParagraphs(get_NonEmptyParagraphs_inRange(ctR), "3", "6,6")
            End If
            
        End If
        
    End If
    
    
    Application.ScreenRefresh
    StatusBar = "Interline set to " & Round(PointsToLines(ctR.ParagraphFormat.LineSpacing), 2)
    fInterline_Decrease = -0.05
End If

End Function


Sub Interline_Increase()

Dim res As Single

res = fInterline_Increase

End Sub


Function fInterline_Increase(Optional TargetRange) As Single

Dim ctR As Range

If Not IsMissing(TargetRange) Then
    Set ctR = TargetRange
Else
    If Selection.Type = wdSelectionIP Then
        Set ctR = Selection.Paragraphs(1).Range
    ElseIf Selection.Type = wdSelectionNormal Then
        Set ctR = Selection.Range
    End If
End If

' Get current interline
Dim ctL As Single
If ctR.ParagraphFormat.LineSpacing <> 9999999 Then
    ctL = PointsToLines(ctR.ParagraphFormat.LineSpacing)
Else
    ctL = get_Interline_ofFirst_NEPar(ctR)
    If ctL = 0 Then ctL = 1.35  ' FAILSAFE - necessary ?
End If

' Don't decrease under "Single" setting!
If ctL = 2 Then
    StatusBar = "Max interline reached!"
    fInterline_Increase = 0
Else    ' decrease by 0.1 lines
    ctR.ParagraphFormat.LineSpacing = LinesToPoints(ctL + 0.05)
    
    Application.ScreenRefresh
    StatusBar = "Interline set to " & Round(PointsToLines(ctR.ParagraphFormat.LineSpacing), 2)
    fInterline_Increase = 0.05
End If

End Function


Function get_Interline_ofFirst_NEPar(TargetRange As Range) As Single
' first non empty paragraph's interline or 0 for "No non-empty paragraph found"

Dim tmpPar As Paragraph

Set tmpPar = get_First_NEPar(TargetRange)
    
If Not tmpPar Is Nothing Then
    get_Interline_ofFirst_NEPar = tmpPar.LineSpacing
Else
    get_Interline_ofFirst_NEPar = 0
End If


End Function


Function get_First_NEPar(TargetRange As Range) As Paragraph

Dim tmpPar As Paragraph

For Each tmpPar In TargetRange.Paragraphs
    If Not is_Empty_Paragraph(tmpPar) Then
        Set get_First_NEPar = tmpPar
        Exit Function
    End If
Next tmpPar

End Function


Sub Kerning_Decrease(Optional TargetParagraph)
' TargetParagraph is a paragraph object

Dim ctR As Range

If Not IsMissing(TargetParagraph) Then
    Set ctR = TargetParagraph.Range
Else
    If Selection.Type = wdSelectionIP Then
        
        Set ctR = Selection.Paragraphs(1).Range
        
    ElseIf Selection.Type = wdSelectionNormal Then
        Set ctR = Selection.Range
        
    End If
End If

' Char spacing irregular
If ctR.Font.Spacing = 9999999 Then
    ctR.Font.Spacing = 0
Else    ' Font char spacing regular
    If ctR.Font.Spacing = -0.2 Then
        StatusBar = "Min kerning reached!"
    Else    ' max tightness char spacing not reached
        ctR.Font.Spacing = ctR.Font.Spacing - 0.1
        StatusBar = "Kerning set to " & ctR.Font.Spacing
    End If
End If


End Sub


Sub Kerning_Increase(Optional TargetRange)


Dim ctR As Range

If Not IsMissing(TargetRange) Then
    Set ctR = TargetParagraph.Range
Else
    If Selection.Type = wdSelectionIP Then
        Set ctR = Selection.Paragraphs(1).Range
    ElseIf Selection.Type = wdSelectionNormal Then
        Set ctR = Selection.Range
    End If
End If


' Char spacing irregular
If ctR.Font.Spacing = 9999999 Then
    ctR.Font.Spacing = 0
Else    ' Font char spacing regular
    If ctR.Font.Spacing = 0.2 Then
        StatusBar = "Max kerning reached!"
    Else    ' max tightness char spacing not reached
        ctR.Font.Spacing = ctR.Font.Spacing + 0.1
        StatusBar = "Kerning set to " & ctR.Font.Spacing
    End If
End If



End Sub



Public Sub GetNumOfLines2()

Dim lngLinesCt As Long
Application.ScreenUpdating = False
Selection.Paragraphs(1).Range.Select
With Dialogs(wdDialogToolsWordCount)
lngLinesCt = .Lines
.Execute
End With
MsgBox CStr(lngLinesCt)
Selection.Collapse
Application.ScreenUpdating = True

End Sub


Sub Print_ctParagraph_LinesCount()

Debug.Print getCtParagraph_LinesCount

End Sub


Function get_CtParagraph_LinesCount() As Integer


Dim cageRange As Range

Set cageRange = Selection.Paragraphs(1).Range


Dim tmpRng As Range

Set tmpRng = cageRange.Duplicate
tmpRng.Collapse (wdCollapseStart)

Dim k As Integer

Let k = 1

Do While tmpRng.InRange(cageRange)
    
    Set tmpRng = tmpRng.GoToNext(wdGoToLine)
    
    'tmpRng.Select
    
    k = k + 1
    
Loop

get_CtParagraph_LinesCount = k - 1


End Function


Function get_Paragraphs_LinesCount(TargetParagraph As Paragraph) As Integer


' Please do not modify original user selection
Dim initialSelRange As Range
Set initialSelRange = Selection.Range


Dim cageRange As Range
Set cageRange = TargetParagraph.Range


Dim tmpRng As Range

Set tmpRng = cageRange.Duplicate
tmpRng.Collapse (wdCollapseStart)

Dim k As Integer

Let k = 1

Do While tmpRng.InRange(cageRange)
    
    Set tmpRng = tmpRng.GoToNext(wdGoToLine)
    
    'tmpRng.Select
    
    k = k + 1
    
Loop


' and restore selection
initialSelRange.Select

get_Paragraphs_LinesCount = k - 1

End Function


Function get_CtParagraphs_LastLineRange() As Range

Dim tmpRng As Range

Set tmpRng = Selection.Paragraphs(1).Range
'tmpRng.MoveEndWhile vbCr, wdBackward


tmpRng.Collapse (wdCollapseEnd)

Set tmpRng = tmpRng.GoToPrevious(wdGoToLine)
'tmpRng.MoveStart wdCharacter, 1

'tmpRng.Select

tmpRng.MoveEnd wdParagraph, 1
tmpRng.MoveEndWhile vbCr, wdBackward

Set get_CtParagraphs_LastLineRange = tmpRng

End Function


Function get_Paragraphs_LastLineRange(TargetParagraph As Paragraph) As Range


Dim tmpRng As Range

Set tmpRng = TargetParagraph.Range
'tmpRng.MoveEndWhile vbCr, wdBackward

tmpRng.Collapse (wdCollapseEnd)

Set tmpRng = tmpRng.GoToPrevious(wdGoToLine)
'tmpRng.MoveStart wdCharacter, 1
'tmpRng.Select

tmpRng.MoveEnd wdParagraph, 1
tmpRng.MoveEndWhile vbCr, wdBackward

Set get_Paragraphs_LastLineRange = tmpRng


End Function


Function get_ParagraphsIndex(TargetParagraph As Paragraph) As Integer

    get_ParagraphsIndex = ActiveDocument.Range(0, TargetParagraph.Range.End).Paragraphs.count

End Function


Function get_CtParagraphsIndex()

    get_CtParagraphsIndex = ActiveDocument.Range(0, Selection.End).Paragraphs.count

End Function


Function is_CtParagraph_Orphaned() As Boolean
' maxOrphanLine to be established more precisely upon practice

Dim lastLineLength As Integer

lastLineLength = Len(get_CtParagraphs_LastLineRange.Text)

If lastLineLength < maxOrphanLine Then
    is_CtParagraph_Orphaned = True
End If

End Function


Function is_Paragraph_Orphaned(TargetParagraph As Paragraph) As Boolean

If get_Paragraphs_LinesCount(TargetParagraph) = 1 Then is_Paragraph_Orphaned = False: Exit Function

If InStr(1, TargetParagraph.Range.Text, Chr(11)) > 0 Then is_Paragraph_Orphaned = False: Exit Function

Dim lastLineLength As Integer

lastLineLength = Len(get_Paragraphs_LastLineRange(TargetParagraph))

If lastLineLength < maxOrphanLine Then
    is_Paragraph_Orphaned = True
End If

End Function


Function get_CtPage_Range() As Range


Dim sRng As Range

Set sRng = Selection.Range

If sRng.Information(wdActiveEndPageNumber) = 1 Then
    
    Set sRng = ActiveDocument.Characters.First
    sRng.Collapse (wdCollapseStart)
        
Else
    Set sRng = sRng.GoTo(wdGoToPage, wdGoToPrevious)
    Set sRng = sRng.GoTo(wdGoToPage, wdGoToNext, 1)

End If

'sRng.Select

Dim eRng As Range

'Set eRng = sRng
Set eRng = sRng.Duplicate

If eRng.Information(wdActiveEndPageNumber) = eRng.Information(wdNumberOfPagesInDocument) Then
    Set eRng = ActiveDocument.Characters.Last
    eRng.Collapse (wdCollapseEnd)
Else
    Set eRng = eRng.GoTo(wdGoToPage, wdGoToNext)
End If

' and come back to previous page...
eRng.MoveEnd wdCharacter, -1

Set get_CtPage_Range = ActiveDocument.Range(sRng.Start, eRng.End)


End Function


Function does_Paragraph_tuck_OrphanLine(TargetParagraph As Paragraph) As Integer
' function to decrease kerning to -0.1 and then -0.2 and return whether
' said paragraph loses one line because of it or not (so as to call another one
' to restore the kerning to 0)
' Return value: difference in lines number

Dim initial_LinesCount As Integer
initial_LinesCount = get_Paragraphs_LinesCount(TargetParagraph)


'first attempt, check line tucking
Call Kerning_Decrease(TargetParagraph)


Dim ctParLines As Integer
ctParLines = get_Paragraphs_LinesCount(TargetParagraph)


If ctParLines = initial_LinesCount Then
    
    ' Second try
    Call Kerning_Decrease(TargetParagraph)
    
    ctParLines = get_Paragraphs_LinesCount(TargetParagraph)
        
End If

does_Paragraph_tuck_OrphanLine = ctParLines - initial_LinesCount


End Function


Function count_CtPage_Orphaned_Paragraphs() As Integer

Dim ctPageRange As Range

Set ctPageRange = get_CtPage_Range

count_CtPage_Orphaned_Paragraphs = count_Orphaned_Paragraphs_inRange(ctPageRange)

End Function


Function count_Orphaned_Paragraphs_inRange(TargetRange As Range) As Integer


If TargetRange.Paragraphs.count > 0 Then
    
    Dim tmpPar As Paragraph
    
    ' lets try collecting suspect paragraphs in here
    Dim tmpOrphanCount As Integer
    
    For Each tmpPar In TargetRange.Paragraphs
        
        If is_Paragraph_Orphaned(tmpPar) Then
            tmpOrphanCount = tmpOrphanCount + 1
        End If
        
    Next tmpPar
    
    count_Orphaned_Paragraphs_inRange = tmpOrphanCount
    
Else
    count_Orphaned_Paragraphs_inRange = 0
    Exit Function
End If


End Function


Function get_Orphaned_Paragraphs_inRange(TargetRange As Range) As Collection
' returning a paragraphs collection or empty collection... lets see if it works
' or passing to returning variant eventually


If TargetRange.Paragraphs.count > 0 Then
    
    Dim tmpPar As Paragraph
    
    ' lets try collecting suspect paragraphs in here
    Dim tmpResult As New Collection
    
    For Each tmpPar In TargetRange.Paragraphs
        
        If is_Paragraph_Orphaned(tmpPar) Then
            tmpResult.Add (tmpPar)
        End If
        
    Next tmpPar
    
    Set get_Orphaned_Paragraphs_inRange = tmpResult
    
Else
    Exit Function
End If


End Function


Function get_CtPage_Orphaned_Paragraphs() As Collection


Dim ctPageRange As Range

Set ctPageRange = get_CtPage_Range


If count_Orphaned_Paragraphs_inRange(ctPageRange) > 0 Then
    Set get_CtPage_Orphaned_Paragraphs = get_Orphaned_Paragraphs_inRange(ctPageRange)
Else
End If


End Function


Function count_EmptyParagraphs_inSelection() As Integer


Dim tmpCount As Integer


If Selection.Type = wdSelectionIP Then
    
    If is_Empty_Paragraph(Selection.Paragraphs(1)) Then
        tmpCount = 1
    End If
    
ElseIf Selection.Type = wdSelectionNormal Then
    
    Dim tmpPar As Paragraph
    For Each tmpPar In Selection.Paragraphs
        
        If is_Empty_Paragraph(tmpPar) Then
            tmpCount = tmpCount + 1
        End If
        
    Next tmpPar
    
Else    ' Selection other than normal or insertion point
End If

count_EmptyParagraphs_inSelection = tmpCount

End Function

Function count_EmptyParagraphs_inRange(TargetRange As Range) As Integer


Dim tmpCount As Integer

    
Dim tmpPar As Paragraph
For Each tmpPar In TargetRange.Paragraphs
    
    If is_Empty_Paragraph(tmpPar) Then
        tmpCount = tmpCount + 1
    End If
    
Next tmpPar
    

count_EmptyParagraphs_inRange = tmpCount

End Function

Function get_Selected_Paragraphs_Collection() As Collection


Dim tmpResult As New Collection

Dim tmpPar As Paragraph


If Selection.Paragraphs.count > 0 Then
    For Each tmpPar In Selection.Paragraphs
        tmpResult.Add (tmpPar)
    Next tmpPar
End If

Set get_Selected_Paragraphs_Collection = tmpResult


End Function


Function get_Paragraphs_Collection(TargetRange As Range) As Collection


Dim tmpResult As New Collection

Dim tmpPar As Paragraph


If TargetRange.Paragraphs.count > 0 Then
    For Each tmpPar In TargetRange.Paragraphs
        tmpResult.Add (tmpPar)
    Next tmpPar
End If

Set get_Paragraphs_Collection = tmpResult


End Function


Sub closeUp_Selected_Paragraphs()


Dim tmpCol As New Collection

Set tmpCol = get_Selected_Paragraphs_Collection

Call closeUp_Paragraphs(tmpCol, "3")


End Sub


Sub set_Paragraphs_InterlineTo(TargetParagraphs As Collection, SetInterlineValue As Single)
' Using the collection object instead of simply the Paragraphs property of a range has advantages:
' it allows for input of non-contiguous paragraphs !

Dim tmpPar As Object

For i = 1 To TargetParagraphs.count
    
    Set tmpPar = TargetParagraphs.Item(i)
    
    tmpPar.ParagraphFormat.LineSpacingRule = wdLineSpaceMultiple
    tmpPar.ParagraphFormat.LineSpacing = LinesToPoints(SetInterlineValue)
    
Next i


End Sub


Sub tightenUp_Range_Spaces(TargetRange As Range)
' setting spaces before and after to 0 for empty paragraphs and
' setting before space to 6 and after to 0 on non-empties


Dim empties As Collection

Set empties = get_EmptyParagraphs_inRange(TargetRange)

Dim texts As Collection

Set texts = get_NonEmptyParagraphs_inRange(TargetRange)

If empties.count > 0 Then
    Call closeUp_Paragraphs(empties, "3")   ' both parts to 0 space
    If texts.count > 0 Then
        Call set_Spaces_forParagraphs(texts, "3", "6,0")     ' both parts affected, before 6 pts, after 0 pts
    End If
Else    ' no empties, we might rely on before and after par spaces!
    If texts.count > 0 Then
        Call set_Spaces_forParagraphs(texts, "3", "6,6")     ' both parts affected, before 6 pts, after 0 pts
    End If
End If



End Sub


Sub tightenUp_Page_Spaces()
' setting spaces before and after to 0 for empty paragraphs and
' setting before space to 6 and after to 0 on non-empties

Dim ctPageRange As Range

Set ctPageRange = get_CtPage_Range


Dim empties As Collection

Set empties = get_EmptyParagraphs_inRange(ctPageRange)

Call closeUp_Paragraphs(empties, "3")


Dim texts As Collection

Set texts = get_NonEmptyParagraphs_inRange(ctPageRange)

Call set_Spaces_forParagraphs(texts, "3", "6,0")


End Sub


Sub set_Spaces_forParagraphs(TargetParagraphs As Collection, SetWhichSide As String, SetValues As String)
' Sets space before and space after to 0 for paragraphs in collection

' CloseWhichSide can be:
'   "CloseBefore", "CloseAfter", "CloseBeforeAfter" or "1", "2" or "3" resp
' SetValues can be: "6", "12" or "6,0" in case of "3" CloseWhichSide

Dim tmpPar As Object

For i = 1 To TargetParagraphs.count
    
    Set tmpPar = TargetParagraphs.Item(i)
    
    Select Case SetWhichSide
            
        Case "CloseBefore", "1"
            tmpPar.ParagraphFormat.SpaceBefore = CSng(SetValues)
        Case "CloseAfter", "2"
            tmpPar.ParagraphFormat.SpaceAfter = CSng(SetValues)
        Case "CloseBeforeAfter", "3"
            tmpPar.ParagraphFormat.SpaceBefore = CSng(Split(SetValues, ",")(0))
            tmpPar.ParagraphFormat.SpaceAfter = CSng(Split(SetValues, ",")(1))
            
    End Select
    
Next i


End Sub


Sub closeUp_Paragraphs(TargetParagraphs As Collection, CloseWhichSide As String)
' Sets space before and space after to 0 for paragraphs in collection

' CloseWhichSide can be:
'   "CloseBefore", "CloseAfter", "CloseBeforeAfter" or "1", "2" or "3" resp

Dim tmpPar As Object

For i = 1 To TargetParagraphs.count
    
    Set tmpPar = TargetParagraphs.Item(i)
    
    Select Case CloseWhichSide
            
        Case "CloseBefore", "1"
            tmpPar.ParagraphFormat.SpaceBefore = 0
        Case "CloseAfter", "2"
            tmpPar.ParagraphFormat.SpaceAfter = 0
        Case "CloseBeforeAfter", "3"
            tmpPar.ParagraphFormat.SpaceBefore = 0
            tmpPar.ParagraphFormat.SpaceAfter = 0
            
    End Select
    
Next i


End Sub


Function get_NonEmptyParagraphs_inRange(TargetRange As Range) As Collection


Dim tmpResult As New Collection

If TargetRange.Paragraphs.count = 1 Then
    
    If Not is_Empty_Paragraph(TargetRange.Paragraphs(1)) Then
        tmpResult.Add (TargetRange.Paragraphs(1))
    End If
    
Else
    
    Dim tmpPar As Paragraph
    For Each tmpPar In TargetRange.Paragraphs
        
        If Not is_Empty_Paragraph(tmpPar) Then
            tmpResult.Add (tmpPar)
        End If
        
    Next tmpPar
    
End If

Set get_NonEmptyParagraphs_inRange = tmpResult


End Function


Function get_NonEmptyParagraphs_inSelection() As Collection


Dim tmpResult As New Collection

If Selection.Type = wdSelectionIP Then
    
    If Not is_Empty_Paragraph(Selection.Paragraphs(1)) Then
        tmpResult.Add (Selection.Paragraphs(1))
    End If
    
ElseIf Selection.Type = wdSelectionNormal Then
    
    Dim tmpPar As Paragraph
    For Each tmpPar In Selection.Paragraphs
        
        If Not is_Empty_Paragraph(tmpPar) Then
            tmpResult.Add (tmpPar)
        End If
        
    Next tmpPar
    
Else    ' Selection other than normal or insertion point
End If

Set get_EmptyParagraphs_inSelection = tmpResult


End Function


Function get_EmptyParagraphs_inRange(TargetRange As Range) As Collection


Dim tmpResult As New Collection

If TargetRange.Paragraphs.count = 1 Then
    
    If is_Empty_Paragraph(TargetRange.Paragraphs(1)) Then
        tmpResult.Add (TargetRange.Paragraphs(1))
    End If
    
Else    ' more than 1 par in range
    
    Dim tmpPar As Paragraph
    For Each tmpPar In TargetRange.Paragraphs
        
        If is_Empty_Paragraph(tmpPar) Then
            tmpResult.Add (tmpPar)
        End If
        
    Next tmpPar
    
End If

Set get_EmptyParagraphs_inRange = tmpResult


End Function


Function get_EmptyParagraphs_inSelection() As Collection


Dim tmpResult As New Collection


If Selection.Type = wdSelectionIP Then
    
    If is_Empty_Paragraph(Selection.Paragraphs(1)) Then
        tmpResult.Add (Selection.Paragraphs(1))
    End If
    
ElseIf Selection.Type = wdSelectionNormal Then
    
    Dim tmpPar As Paragraph
    For Each tmpPar In Selection.Paragraphs
        
        If is_Empty_Paragraph(tmpPar) Then
            tmpResult.Add (tmpPar)
        End If
        
    Next tmpPar
    
Else    ' Selection other than normal or insertion point
End If

Set get_EmptyParagraphs_inSelection = tmpResult


End Function



Function is_Empty_Paragraph(TargetParagraph As Paragraph) As Boolean


If Replace(Replace(Replace(Replace(TargetParagraph.Range.Text, vbCr, ""), vbLf, ""), " ", ""), Chr(160), "") = "" Then
    
    is_Empty_Paragraph = True
    
Else

    is_Empty_Paragraph = False
    
End If


End Function


Function get_CtPages_LastParagraph() As Paragraph

Dim ctPageRange As Range

Set ctPageRange = get_CtPage_Range

Set get_CtPages_LastParagraph = ctPageRange.Paragraphs(ctPageRange.Paragraphs.count)

Set ctPageRange = Nothing

End Function


Function get_Ranges_LastParagraph(TargetRange As Range) As Paragraph

Set get_Ranges_LastParagraph = TargetRange.Paragraphs(TargetRange.Paragraphs.count)

End Function


Function is_Ranges_LastParagraph_Tucked(TargetRange As Range) As Boolean

Dim tmpPar As Paragraph

Set tmpPar = TargetRange.Paragraphs(TargetRange.Paragraphs.count)

If Not is_Paragraph_Spanning_2Pages(tmpPar) Then
    is_Ranges_LastParagraph_Tucked = True
End If


End Function


Function is_CtPages_LastParagraph_Tucked() As Boolean

Dim tmpPar As Paragraph

Set tmpPar = get_CtPages_LastParagraph

If Not is_Paragraph_Spanning_2Pages(tmpPar) Then
    is_CtPages_LastParagraph_Tucked = True
End If

End Function


Function is_Paragraph_Spanning_2Pages(TargetParagraph As Paragraph) As Boolean


Dim fCharRange As Range

Set fCharRange = TargetParagraph.Range.Characters.First


Dim lCharRange As Range

Set lCharRange = TargetParagraph.Range.Characters.Last


If lCharRange.Information(wdActiveEndPageNumber) > fCharRange.Information(wdActiveEndPageNumber) Then
    is_Paragraph_Spanning_2Pages = True
Else
    is_Paragraph_Spanning_2Pages = False
End If


End Function


Function does_SpaceShrinking_TuckPage(TargetRange As Range) As Boolean

If Not is_Ranges_LastParagraph_Tucked(TargetRange) Then
    
    ' setting empty paragraphs space before and after to 0
    Call tightenUp_Range_Spaces(TargetRange)
    
    ' get empty paragraphs collection from page range
    Dim empties As Collection
    Set empties = get_EmptyParagraphs_inRange(TargetRange)
    
    If empties.count > 0 Then
        ' and set interline of empties to 0
        Call set_Paragraphs_InterlineTo(empties, 1)
    End If
    
    ' and then check actions result and return result
    If is_Ranges_LastParagraph_Tucked(TargetRange) Then
        does_SpaceShrinking_TuckPage = True
        Application.ScreenRefresh
    End If
    
End If

End Function


Sub reset_Kerning_onParagraph(TargetParagraph As Paragraph)

TargetParagraph.Range.Font.Spacing = 0

End Sub


Function does_orphansKernShrinking_TuckPage(TargetRange As Range) As Boolean


If Not is_Ranges_LastParagraph_Tucked(TargetRange) Then
    
    ' we will affect only orphaned paragraphs in range
    Dim rangesOrphans As Collection
    
    Set rangesOrphans = get_Orphaned_Paragraphs_inRange(TargetRange)
    
    Dim tmpPar As Object
    
    For i = 1 To rangesOrphans.count
        
        Set tmpPar = rangesOrphans.Item(i)
        
        ' though tmpPar is supposed to be a paragraph, its in fact a range
        If does_Paragraph_tuck_OrphanLine(tmpPar.Paragraphs(1)) = 0 Then
            
            reset_Kerning_onParagraph (tmpPar.Paragraphs(1))
            
        End If
        
    Next i
    
    If is_Ranges_LastParagraph_Tucked(TargetRange) Then
        does_orphansKernShrinking_TuckPage = True
    End If
    Application.ScreenRefresh
    
End If


End Function


Function does_interlineShrinking_TuckPage(TargetRange As Range) As Boolean

If Not is_Ranges_LastParagraph_Tucked(TargetRange) Then
    
    ' also use optional parameter "HandleSpaces" so as to mod before and after paragraph spaces
    Do While fInterline_Decrease(TargetRange, "HandleSpaces") <> 0
        
        If is_Ranges_LastParagraph_Tucked(TargetRange) Then
            
            does_interlineShrinking_TuckPage = True
            Application.ScreenRefresh
            Exit Do
        
        End If
        
    Loop
    
End If

End Function


Sub magicPage_Fit()


Dim ctPageRange As Range

Set ctPageRange = get_CtPage_Range


If Not is_Ranges_LastParagraph_Tucked(ctPageRange) Then
    
    If Not does_SpaceShrinking_TuckPage(ctPageRange) Then
        
        If Not does_orphansKernShrinking_TuckPage(ctPageRange) Then
            
            If Not does_interlineShrinking_TuckPage(ctPageRange) Then
                StatusBar = "Magic Page Fit: FAILURE!"
            Else
                StatusBar = "Magic Page Fit: Interline Shrinking SUCCESS!"
            End If
            
        Else
            StatusBar = "Magic Page Fit: Orphans Kerning SUCCESS!"
        End If
        
    Else
        StatusBar = "Magic Page Fit: Space Shrinking SUCCESS!"
    End If
    Application.ScreenRefresh
    
End If


End Sub
