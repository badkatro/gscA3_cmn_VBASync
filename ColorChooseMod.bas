Attribute VB_Name = "ColorChooseMod"
Private Type CHOOSECOLOR
     lStructSize As Long
     hwndOwner As Long
     hInstance As Long
     rgbResult As Long
     lpCustColors As String
     flags As Long
     lCustData As Long
     lpfnHook As Long
     lpTemplateName As String
End Type

Dim CustomColors() As Byte

Private Declare Function ChooseColorAPI Lib "comdlg32.dll" Alias _
    "ChooseColorA" (pChoosecolor As CHOOSECOLOR) As Long

Private Declare Function FindWindow Lib "user32" _
    Alias "FindWindowA" (ByVal lpClassName As String, _
    ByVal lpWindowName As String) As Long

Private Function GetWinwordHwnd() As Long
    Dim hWnd As Long

    hWnd = FindWindow("opusApp", vbNullString)
    GetWinwordHwnd = hWnd
End Function


Function ColorPicker(OwnerHwnd As Long) As Long
' Displays a Windows color picker and returns a color code so you may use as necessary

ReDim CustomColors(0 To 16 * 4 - 1) As Byte

For i = LBound(CustomColors) To UBound(CustomColors)
    CustomColors(i) = 0
Next i

Dim cc As CHOOSECOLOR
Dim lReturn As Long, Rval As Long
Dim Gval As Long, Bval As Long

cc.lStructSize = Len(cc)
'cc.hwndOwner = GetWinwordHwnd()
cc.hwndOwner = OwnerHwnd
cc.hInstance = 0
cc.lpCustColors = StrConv(CustomColors, vbUnicode)
cc.flags = 0

' call the color picker
lReturn = ChooseColorAPI(cc)

If lReturn <> 0 Then
    
    ' extract the color values
    Rval = cc.rgbResult Mod 256
    Bval = Int(cc.rgbResult / 65536)
    Gval = Int((cc.rgbResult - (Bval * 65536) - Rval) / 256)

    ' display the values in the dialog title bar
    'Me.Caption = "RGB Value User Chose: R=" & Str$(Rval) & _
         "  G=" & Str$(Gval) & "  B=" & Str$(Bval)
    ' change the dialog background to that color
    ColorPicker = cc.rgbResult

    ' save the color values to send to the
    ' color picker for the next iteration
    CustomColors = StrConv(cc.lpCustColors, vbFromUnicode)
    ReDim CustomColors(0 To 16 * 4 - 1) As Byte
    For i = LBound(CustomColors) To UBound(CustomColors)
       CustomColors(i) = 0
    Next i
Else
    ColorPicker = 0
End If


End Function
