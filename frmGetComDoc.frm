VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmGetComDoc 
   OleObjectBlob   =   "frmGetComDoc.frx":0000
   Caption         =   "Open Commission Document"
   ClientHeight    =   1320
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5055
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   21
End
Attribute VB_Name = "frmGetComDoc"
Attribute VB_Base = "0{B661FB91-05A7-48D5-875F-28BD30A76ED1}{21E5DB54-210C-4AD3-9A97-7D39C86831AE}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False



'Const uLg As String = "RO"
Const CommissionBaseFolder As String = "M:\Documents Externes\Commission\"
Const ComFinalDoc_NewPathEnding As String = "\DOC\FINAL\1_*"
Const ComAnnexDoc_NewPathEnding As String = "\DOC\FINAL\2_*"

Const DDoc_NewPathEnding As String = "\DOC\1_*"     ' Ds are not using the FINAL folder ! (Except very few exceptions)
Const DAnnexDoc_NewPathEnding As String = "\DOC\2_*"

Const ComFinalDoc_NewPathBase As String = "\DOC\FINAL\"
Const DDoc_NewPathBase As String = "\DOC\"      '"\_DOC\"     ' OLD habit, used to do that before first week of 02/2017 ! Now they're not doing it anymore!
Const ComFinal2Doc_NewPathBase As String = "\DOC\FINAL*2\"

Private Current_ComDoc_BaseFolder As String         ' Variable to hold ct Com Doc base folder when needing to present multiple files to user for choosing



Private Sub lbMultipleFilesFound_Click()


Dim complete_FileToOpenPath As String


If Dir(Current_ComDoc_BaseFolder & "\" & lbMultipleFilesFound.List(lbMultipleFilesFound.ListIndex) & "\" & "*" & UCase(Get_ULg) & "*doc*") <> "" Then
    
    'Debug.Print "Found " & Dir(Current_ComDoc_BaseFolder & "\" & lbMultipleFilesFound.List(lbMultipleFilesFound.ListIndex) & "\" & "*RO*doc*") & _
        " in: " & Current_ComDoc_BaseFolder & "\" & lbMultipleFilesFound.List(lbMultipleFilesFound.ListIndex) & "\" & "*RO*doc*"
        
    complete_FileToOpenPath = Current_ComDoc_BaseFolder & "\" & lbMultipleFilesFound.List(lbMultipleFilesFound.ListIndex) & "\" & _
        Dir(Current_ComDoc_BaseFolder & "\" & lbMultipleFilesFound.List(lbMultipleFilesFound.ListIndex) & "\" & "*" & UCase(Get_ULg) & "*doc*")
    
    Me.Hide
    
    Documents.Open complete_FileToOpenPath, AddToRecentFiles:=False
    
    Unload Me

Else

    Debug.Print "GetComDoc lbMultipleFilesFound_Click event: NO FILE found for: Dir(" & Current_ComDoc_BaseFolder & "\" & lbMultipleFilesFound.List(lbMultipleFilesFound.ListIndex) & "\" & "*" & UCase(Get_ULg) & "*doc*" & ")"
    
End If


End Sub

Public Sub UserForm_Activate()


Dim strComDoc As String
Dim strCDPath As String

'frmGetComDoc.Caption = "GetComDoc"

If Documents.count > 0 Then

    ' Try to retrieve Commission Document ID string from Docuwrite metadata, see if current doc is DW or not...
    strComDoc = strGetComDocNo_fromDWMetadata
    
    ' If it is NOT a DW document (or a broken one... no meta for ComDoc)
    If strComDoc = "" Then
        
        ' manually get it from selection or cursor position
        strComDoc = strGetComDocNo_fromSelCtPar
        
    End If
    
    If strComDoc = "" Then Exit Sub     ' BAILOUT, user had not done selection yet!
    
    strCDPath = Build_CFOPath(strComDoc)
    
    Dim lb As Byte  ' position of last backslash in strCDPath
    lb = InStrRev(strCDPath, "\")
     
    If strCDPath = "" Then
        
        frmGetComDoc.txtComDocNo.Value = strComDoc
        frmGetComDoc.Caption = "GetComDoc - ComDoc path NOT FOUND!"
        frmGetComDoc.txtComDocNo.BackColor = wdColorRose     ' light red
        Exit Sub    ' No more checking, the returned ComDoc path is empty, not found!
        
    End If
        
    'If Left(Mid(strCDPath, lb + 1, Len(strCDPath) - lb), 3) = "DEC" Then    ' Special condition of selection for DEC documents, their final subfolder is a number composed of 2 digits, not 4, as for the others
    
    If IsNumeric(Left(Mid(strCDPath, lb + 1, Len(strCDPath) - lb), 4)) Or IsNumeric(Right(Mid(strCDPath, lb + 1, Len(strCDPath) - lb), 2)) Or _
       LCase$(Left(Mid(strCDPath, lb + 1, Len(strCDPath) - lb), 5)) = "annex" Then       ' Build_CFOPath can return "", but also mother directory of directory saught for, such as M\...\COM, since it found not COM(2020) 999 final, for instance
        frmGetComDoc.txtComDocNo.Value = strComDoc
    
        'frmGetComDoc.Caption = ""
        frmGetComDoc.Caption = "GetComDoc - ComDoc path exists"
        frmGetComDoc.txtComDocNo.BackColor = 12648384     ' light green
    
    Else    ' conditions for valid return of ComDoc path unsatisfied!
    
        frmGetComDoc.txtComDocNo.Value = strComDoc
        frmGetComDoc.Caption = "GetComDoc - ComDoc path NOT FOUND!"
        frmGetComDoc.txtComDocNo.BackColor = wdColorRose     ' light red
    
    End If
        
End If

End Sub


Private Function strGetComDocNo_fromSelCtPar() As String    ' Returns the ext doc id if

    '***********************************************************************************************************************
    ' ALL THE PART BELOW, dealing with finding Com Doc ID string in non-DW documents, perhaps MOVED into separate function?
    ' PROBABLY called something like "strGetComDocNo_fromSelCtPar"  - from selection or current paragraph that is !
    '***********************************************************************************************************************
        
    Dim tmpRng As Range
    Dim ComDocRng As Range
    
    ' from Selection already made
    If Selection.Type = wdSelectionNormal Then
        If Build_CFOPath(Selection.Text) <> "" Then
            
            strGetComDocNo_fromSelCtPar = Selection.Text

        Else
            strGetComDocNo_fromSelCtPar = ""    'FAIL
            
        End If
    ' or from cursor placed into or before first word of Com Doc ID string
    ElseIf Selection.Type = wdSelectionIP Then
        
        ' Check wether user simply placed cursor into Commision Doc Name
        Set tmpRng = Selection.Range
        tmpRng.MoveStart wdCharacter, -1
        
        If tmpRng.Text = " " Or tmpRng.Text = Chr(160) Then
            tmpRng.MoveStart wdCharacter, 1
            tmpRng.MoveEnd wdParagraph
        Else
            ' Check wether user simply placed cursor into Commision Doc Name
            tmpRng.MoveStart wdWord, -1
            tmpRng.MoveEnd wdParagraph
        End If
        
        Set ComDocRng = tmpRng.Words(1)
        
        'tmpRng.Select
        
        If InStr(1, tmpRng.Text, "final") > 0 Then
            ComDocRng.MoveEndUntil "final", wdForward
            ComDocRng.MoveEnd wdCharacter, 5
            
            ' what if, in the part of sentence that we captured, we find out that
            ' the wanted doc is actually an annex of some Commission document?
            If InStr(1, LCase(tmpRng.Text), "annex") > 0 Then
                ComDocRng.MoveEndWhile " Aanex", wdForward
                ComDocRng.MoveEnd wdWord, 1     ' now ComDocRng should contain the
                                                ' Annex word as well as the number of the annex
            Else
            End If
            
        Else
            If InStr(1, tmpRng.Text, "/") > 0 Then
                ComDocRng.MoveEndUntil "/", wdForward
                ComDocRng.MoveEnd wdCharacter, 3
            End If
        End If
        
        ' Build_CFOPath returns M:\Documents Externes\Commission if string appears to be Commision Doc Num, but doc type ss not found in
        ' Commission folders (COM, SWD, SEC, D, etc)
        Dim comPath As String
        comPath = Build_CFOPath(ComDocRng.Text)
        
        If comPath <> "" Then
            
            If comPath <> "M:\Documents Externes\Commission" Then
                strGetComDocNo_fromSelCtPar = ComDocRng.Text
            Else
                strGetComDocNo_fromSelCtPar = ""
            End If
            
        Else
            strGetComDocNo_fromSelCtPar = ""
        End If

    End If

Set tmpRng = Nothing: Set ComDocRng = Nothing

End Function


Private Sub cmdOK_Click()
' OK button

Dim uLg As String
uLg = Get_ULg   ' will complain if language unit unfoundable and abort all operations


Dim strComDocPath As String
Dim strComDocFileCompletePath As String

frmGetComDoc.Caption = "GetComDoc"

strComDocPath = Build_CFOPath(txtComDocNo.Value)

'
If Me.txtComDocNo.BackColor = 12648384 Then     ' Light green

    If strComDocPath = "" Then
        frmGetComDoc.Caption = "Document host folder NOT found!"
    Else
        strComDocFileCompletePath = Get_CFODoc(txtComDocNo.Value)
        
        If strComDocFileCompletePath <> "" Then
            frmGetComDoc.Caption = "Document FOUND!"
        Else
            
            ' let user choose which one
            If Me.Height > 95 Then
                Exit Sub
            Else
            
                Me.Hide
                
                Dim userChoice As VbMsgBoxResult
                
                userChoice = MsgBox("NO Commission document found for " & txtComDocNo.Value & vbCr & _
                    "Would you like to open the respective Commission folder ?" & vbCrCr & "Choose �Yes� to open folder, �No� to exit", _
                    vbYesNo + vbCritical, Get_ULg & " Document missing!")
                    
                If userChoice = vbYes Then
                    Call Shell("explorer.exe " & strComDocPath, vbNormalFocus)
                    Unload Me
                    Exit Sub
                Else
                    Unload Me
                    Exit Sub
                End If
            
            End If
            
        End If
        
        Documents.Open strComDocFileCompletePath, AddToRecentFiles:=False
        'PauseForSeconds (0.7)   ' Reasonable guess
        
        If Documents.count > 0 Then
            
            Dim username As String
            username = Environ("username")
            
            Dim prefAstBaseFolder As String
            Dim fso As New Scripting.FileSystemObject
            
            ' see what storage folder we have available
            If fso.FolderExists(astNetworkHomeBase & "\" & username) Then
                prefAstBaseFolder = astNetworkHomeBase & "\" & username
            Else     'Use backup AST base folder (D:\Docs)
                If Not fso.FolderExists(astBackupHomBase) Then
                    gscrolib.AhMsgBox_Show "Preffered backup AST docs base folder does not exist(" & astBackupHomBase & "), please report to margama(2942)", 6
                    Exit Sub
                Else    ' set pref folder to backup ast base folder
                    prefAstBaseFolder = astBackupHomBase
                End If
            End If
            
            StatusBar = "Saving to user folder..."
            ActiveDocument.SaveAs2 FileName:=prefAstBaseFolder & "\" & ActiveDocument.Name, FileFormat:=wdFormatXMLDocument, CompatibilityMode:=wdWord2010, AddToRecentFiles:=False
            
        End If
        Unload Me
        
    End If

ElseIf Me.txtComDocNo.BackColor = wdColorRose Then
    '
        
    Dim fileop As FileDialog
    Set fileop = Application.FileDialog(msoFileDialogOpen)
    
    ChangeFileOpenDirectory (strComDocPath)
    With fileop
        
        .InitialFileName = strComDocPath & "\" & "*" & LCase(uLg) & "*"
        .FilterIndex = 2
        
        .Show
        .Execute
    End With
    
    Unload Me
    
End If


End Sub


Private Sub lblFinal2_Click()

If Me.lblFinal2.SpecialEffect = fmSpecialEffectFlat Then
    Me.lblFinal2.SpecialEffect = fmSpecialEffectSunken
    Me.lblFinal2.BackColor = 8454016            ' Green, ON
Else
    Me.lblFinal2.SpecialEffect = fmSpecialEffectFlat
    Me.lblFinal2.BackColor = -2147483633        ' None
End If

End Sub

Private Sub lblRefreshCaption_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.lblRefreshCaption.ForeColor = wdColorOrange
Me.lblRefresh.BackColor = wdColorOrange
Me.lblRefresh.SpecialEffect = fmSpecialEffectSunken

End Sub

Private Sub lblRefreshCaption_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.lblRefreshCaption.ForeColor = wdColorBlack
Me.lblRefresh.BackColor = 65535     ' yellow
Me.lblRefresh.SpecialEffect = fmSpecialEffectFlat

End Sub


Private Sub lblRefresh_Click()
' Little refresh button (label in fact)

Call UserForm_Activate

End Sub

Private Sub lblRefresh_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.lblRefresh.SpecialEffect = fmSpecialEffectSunken
Me.lblRefreshCaption.ForeColor = wdColorOrange
Me.lblRefresh.BackColor = wdColorOrange

End Sub


Private Sub lblRefresh_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

Me.lblRefresh.SpecialEffect = fmSpecialEffectFlat
Me.lblRefreshCaption.ForeColor = wdColorBlack
Me.lblRefresh.BackColor = 65535     ' yellow

End Sub

Private Sub lblRefreshCaption_Click()

Me.UserForm_Activate

End Sub


Private Sub cmdCancel_Click()
' Cancel button

Unload Me

End Sub





Function strGetComDocNo_fromDWMetadata() As String


Dim gotV As Boolean

If Documents.count = 0 Then
    StatusBar = "Need document to process, Exiting..."
    Exit Function
End If

If ActiveDocument.Variables.count = 0 Then
    StatusBar = "No doc vars found, no metadata, Exiting..."
    Exit Function
End If

Dim v As Variable
For Each v In ActiveDocument.Variables
    If v.Name = "DocuWriteMetaData" Then
        gotV = True
        Exit For
    End If
Next v
' Now v in our DW metadata variable. Let's process it.

If Not gotV Then Exit Function

Dim comdoc As String
Dim tmpMetaArr() As String

tmpMetaArr = Split(v, "</metadata>")

For i = 0 To UBound(tmpMetaArr)
    If InStr(1, tmpMetaArr(i), "md_CommissionDocuments") > 0 Then
        If InStr(1, tmpMetaArr(i), "<text>") > 0 Then
            comdoc = Split(Split(tmpMetaArr(i), "<text>")(1), "</text>")(0)
        End If
        
        Exit For
    End If
Next i


strGetComDocNo_fromDWMetadata = comdoc

End Function



Function Build_CFOPath(cdpath As String) As String
' DESCRIPTION: supplied with a human-readable Commission document reference (ID) string, such as COM(2014) 254 final, or D032230/02 or others, return a
' string with main folder of such document from M drive, if such a folder exists
'
' version 0.2
'
' 0.2


Dim tfo1 As New FileSystemObject    ' temporary fileobject 1

Dim ts As String                    ' temporary string

Dim tf1 As Scripting.Folder                   ' temporary folder 1
Dim bf As Scripting.Folder                    ' base folder (to revert one step from the path and look for the right name of the last subfolder

Dim gotpath As Boolean


cdpath = Replace(cdpath, Chr(160), " ")

' Remove leading space before opening parantheses if found
If InStr(1, cdpath, " (") > 0 Then
    cdpath = Replace(cdpath, " (", "(")
End If

' Extract type, year and number from cdpath string, supplied string extracted from document
'(containing standardized human format com doc number, like COM(2014) 225 final Annex
If LCase(Left$(cdpath, 3)) = "cdr" Then

    Build_CFOPath = ""
    
ElseIf Left$(cdpath, 3) = "DEC" Then

    tt = Left$(cdpath, 3)
    ty = Right$(Split(cdpath, "/")(1), 2)
    tn = Format(Mid$(Split(cdpath, "/")(0), 4, (Len(cdpath) - 3)), "00")
    
ElseIf Left$(cdpath, 1) <> "D" Then         ' COM, SEC, C, SWD
    
    ' And also build a little string (ss) to be used to identify subfolder containing Commission file (like "annex" or "annex?1" - contains wildcards)
    If InStr(1, cdpath, "(") = 0 And InStr(1, cdpath, "/") = 0 Then     ' Absence of parantheses and slash means no official Commission Document number
        Build_CFOPath = ""
        Exit Function
    End If
    
    tt = Trim(Split(cdpath, "(")(0))                                                                                ' temporary (Commission document) type
    ty = Right$(Split(Split(cdpath, "(")(1), ")")(0), 2)                                                            ' temporary year
    tn = Format(Split(Trim(Replace(cdpath, Chr(160), " ")), " ")(1), "0000")    ' temporary number
        
ElseIf Left$(cdpath, 1) = "D" Then
    
    tt = Left$(cdpath, 1)
    ty = Right$(Trim$(DatePart("yyyy", Now)), 2)                   ' we use current year, D documents are very long runners... they come with 10 years or so behind... so they just place them in current year
    tn = Format(Mid$(Split(Replace(cdpath, Chr(160), " "), "/")(0), 2, Len(cdpath) - 1), "000000")  ' temporary number
    
End If


' Using the above data (type, year, number) and subfolder substring (could be empty as well),
' we try to determine the path of file in question
If tt = "DEC" Then
    
    ts = CommissionBaseFolder & ty & "\" & "BUDGET " & tt & "\" & "DEC " & tn
    
    If tfo1.FolderExists(ts) Then
        Build_CFOPath = ts
    Else
        ts = CommissionBaseFolder & ty & "\" & "BUDGET " & tt
        If tfo1.FolderExists(ts) Then
            Build_CFOPath = ts
        End If
    End If

ElseIf tt <> "C" And tt <> "COM" And tt <> "SEC" And tt <> "D" And tt <> "SWD" And tt <> "JOIN" Then
    Build_CFOPath = CommissionBaseFolder
Else    ' D's
    ts = CommissionBaseFolder & ty & "\" & tt & "\" & tt & tn
    
    If tfo1.FolderExists(ts) Then
        Build_CFOPath = ts
    Else
        
        If tfo1.FolderExists(CommissionBaseFolder & ty & "\" & tt) Then
            
            Set bf = tfo1.GetFolder(CommissionBaseFolder & ty & "\" & tt)
            
            For Each tf1 In bf.SubFolders
                If tf1.Name Like tn & "*" Then
                    Build_CFOPath = bf.Path & "\" & tf1.Name
                    gotpath = True
                    Exit For
                End If
            Next tf1
            
            If gotpath = False Then
                Build_CFOPath = bf
                Exit Function
            End If
        Else
            Build_CFOPath = CommissionBaseFolder
        End If
            
    End If
End If

End Function


Function Get_CFODoc(cdpath As String) As String
' version 0.80

' Returns the complete filename (path included) of the Commision document whose name it takes as parameter,
' namely cdpath, of the general standard Commission form "COM(2011) 679 final", D028493/01, SWD(2011) 256 final...
' 0.80 - Trying to fix D and D Annex files not being found!

Dim uLg As String
uLg = Get_ULg


Dim tfo2 As New Scripting.FileSystemObject
Dim tf2 As Scripting.Folder, tf3 As Scripting.File
Dim z As Integer, Y As String, tvs As String

tvs = Build_CFOPath(cdpath)

If tvs = "" Or tvs = CommissionBaseFolder Then GoTo jumpNullAndExit
    
Set tf2 = tfo2.GetFolder(tvs)
    

' See if we try open final 2 or not
Dim newpathBase As String
If Me.lblFinal2.SpecialEffect = fmSpecialEffectSunken Then
    newpathBase = ComFinal2Doc_NewPathBase
    ' little brats from Commission prob do folders manually, names of "final2" folder differ !!!
    Dim final2FolderName As String
    final2FolderName = Dir(tvs & Left$(newpathBase, Len(newpathBase) - 1), vbDirectory)
    ' TODO SHOULD probably include code to worry if the previous line returned empty ! (no final2 folder name found !)
    
    If final2FolderName <> "" Then
        If LCase$(final2FolderName) <> LCase$(Replace(Split(newpathBase, "\")(2), "*", "")) Then
            newpathBase = Replace(newpathBase, Split(newpathBase, "\")(2), final2FolderName)
        Else    'PROB never used?
            newpathBase = Replace(newpathBase, Split(newpathBase, "\")(2), final2FolderName)
        End If
    Else
        'HERE
    End If
    
Else
    ' Freakin Commission inconsistencies ! ONLY the D documents have an "_" in front of folder names !!!
    newpathBase = IIf(Left(cdpath, 1) = "D", DDoc_NewPathBase, ComFinalDoc_NewPathBase)
End If

' For OLD (prior to 07/2014) Commission documents (transmitted to us prior to that date), the files for the main doc of the package
' (they have annexes and such) will be straight into the main Commission document folder, referenced here as tf2.
If tf2.files.count = 0 Then
    
    ' Since we're in new folder structure, let's see if we need a annex doc or final doc... If it's the "final" document
    If InStr(1, LCase(cdpath), "annex") = 0 Then
    
        ' We're in this branch because we're in the NEW folder structure with current document!
        
        If Left(cdpath, 1) <> "D" Then

            If Dir(tvs & ComFinalDoc_NewPathEnding, vbDirectory) = "" Then GoTo jumpNullAndExit
          
            tvs = tvs & newpathBase & Dir(tvs & ComFinalDoc_NewPathEnding, vbDirectory)
        
            Set tf2 = tfo2.GetFolder(tvs)
            
        Else    ' For D docs, add freakin "_" in front of folder names !
            
            If Dir(tvs & DDoc_NewPathEnding, vbDirectory) = "" Then GoTo jumpNullAndExit
          
            tvs = tvs & newpathBase & Dir(tvs & DDoc_NewPathEnding, vbDirectory)
        
            Set tf2 = tfo2.GetFolder(tvs)
            
        End If
         
    Else    ' If it's the annex...
    
        tvs = tvs & newpathBase     ' Update tvs basefolder to contain the new path ending as well (to descend into "\DOC\FINAL" or "\DOC\FINAL*2")
        
        'If Dir(tvs & ComAnnexDoc_NewPathEnding, vbDirectory) = "" Then GoTo jumpNullAndExit
                  
        'tvs = tvs & newpathBase & Dir(tvs & ComAnnexDoc_NewPathEnding, vbDirectory)
        'Set tf2 = tfo2.GetFolder(tvs)
        
        Dim annexFoldersStr As foundFilesStruct     ' Annex folders struct, holds names of annexes folders from Com Doc base folder
        
        annexFoldersStr = bDir.bGDirAll(tvs & "*annex*", "listFolders")
        
        ' Annex string supplied from document, gotta find those folders...
        If annexFoldersStr.count > 0 Then
            
            ' Got annex folders, but one only. Unique annex supplied from Commission, for the moment
            If annexFoldersStr.count = 1 Then
            
                'If Dir(tvs & "\annex*", vbDirectory) = "" Then
                      
                tvs = tvs & annexFoldersStr.files(0)
                
                ' we computed the only annex folder path and name from Com Doc base folder, we try get folder
                ' Normally we don't get errors so late in the game, but...
                If tfo2.FolderExists(tvs) Then
                    Set tf2 = tfo2.GetFolder(tvs)
                Else    ' Prob gotta warn user and... exit?
                
                End If
                
            Else    ' Multiple annex subfolders found in Commission base folder!
                
                Dim annexPart As String     ' the part of string after the "annex" word
                
                annexPart = Split(LCase(cdpath), "annex")(1)
                
                
                Dim annexNums As ExtractedNumbers   ' the numbers contained therein
                
                annexNums = gscrolib.Extr_NumbersStruct(annexPart)
                
                
                ' If we get ONE annex num from document com doc spec string, we can  proceed with auto-identification:
                ' we match the num against those from the already extracted Comm annex subfolders!
                Select Case annexNums.count
                    
                    Case 1
                        
                        Dim identified_Right_Annex As foundFilesStruct    ' to store correctly identified annex, or nill
                        
'                        For l = 1 To annexFoldersStr.count
'                            'if annexFoldersStr.files(k-1)
'                        Next l

                        identified_Right_Annex = bDir.bGDirAll(tvs & "*annex*" & annexNums.Numbers(0), "listFolders")
                        
                        ' WE GOT IT! identified right annex, there's only one. HIT IT!
                        If identified_Right_Annex.count = 1 Then
                            
                        
                            tvs = tvs & identified_Right_Annex.files(0)
                            
                            ' check again and set folder
                            If Dir(tvs, vbDirectory) <> "" Then
                                Set tf2 = tfo2.GetFolder(tvs)
                            End If
                        
                        Else     ' Prob should add'em all to the textbox
                            
                            If identified_Right_Annex.count > 1 Then
                            
                                Dim expected_AnnexNameMask As String

                                    Dim tryTvs As String
                                    
                                    For i = 1 To identified_Right_Annex.count
                                    
                                        tryTvs = tvs & identified_Right_Annex.files(i - 1)
                                        
                                        expected_AnnexNameMask = tryTvs & "\*F#" & annexNums.Numbers(0) & "_*"
                                        
                                        If Dir(expected_AnnexNameMask) <> "" Then
                                            
                                            Dim annexName As String
                                            annexName = Dir(expected_AnnexNameMask)
                                            
                                            tvs = tryTvs
                                            Set tf2 = tfo2.GetFolder(tvs)
                                            
                                            Exit For
                                            
                                        End If
                                    
                                    Next i

                            
                            ElseIf identified_Right_Annex.count = 0 Then
                                
                                For i = 1 To annexFoldersStr.count
                                    
                                    
                                    tryTvs = tvs & annexFoldersStr.files(i - 1)
                                    
                                    expected_AnnexNameMask = tryTvs & "\*F#" & annexNums.Numbers(0) & "*"
                                    
                                    If Dir(expected_AnnexNameMask) <> "" Then
                                        
                                        annexName = Dir(expected_AnnexNameMask)
                                        
                                        tvs = tryTvs
                                        Set tf2 = tfo2.GetFolder(tvs)
                                        
                                        Exit For
                                        
                                    End If
                                    
                                Next i
                                
                            End If
                        
                        End If
                        
                    Case 0  ' Not good news for auto folder ID, cause we got no annex number from doc, but more than one annex subfolder from Com doc base folder...
                        
                        Current_ComDoc_BaseFolder = tvs     ' let userform know which base folder we use, for later, when user chooses to open one file
                        Me.Toggle_ExpandUserform    ' Expand userform vertically to show multi files selection part
                        
                        Me.Caption = Me.Caption & " - Choose annex to open..."
                        
                        ' Present the subfdrs to the user, human knows best !...
                        For k = 1 To annexFoldersStr.count
                            Me.lbMultipleFilesFound.AddItem (annexFoldersStr.files(k - 1))
                        Next k
                                
                    Case Is > 1     ' Probably exactly the same as Case 0, present all to user. BUT... Upon further study, perhaps we CAN auto-identify something. Or not. We'll see.
                            
                            ' we querry a folder with all those numbers in its name
                            identified_Right_Annex = bDir.bGDirAll(tvs & "*annex*" & Join(annexNums.Numbers, "*"), "listFolders")
                            
                            ' Surprise, only one found !
                            If identified_Right_Annex.count = 1 Then
                                
                                ' we founds proper file hosting folder, so we ammend folder string path
                                tvs = tvs & identified_Right_Annex.files(0)
                                
                                ' we check again and set folder to proper
                                If Dir(tvs, vbDirectory) <> "" Then
                                    
                                    Set tf2 = tfo2.GetFolder(tvs)
                                    
                                Else    ' Ooops!
                                End If
                                
                            Else    ' put'em up for display, user should choose
                            End If
                                                        
                End Select
                
            End If
        
        End If
        
    End If

Else     ' OLD Commission folder path structure

    ' the D types also have Annexes, the main documents are in the main Commission folder (tf2), but they have Annexes also...
    
    ' Since we're in old folder structure, let's see if we need a annex doc or final doc...
    
    ' If it's the "final" document
    If InStr(1, LCase(cdpath), "annex") = 0 Then
    
        ' We're in this branch because we're in the OLD folder structure with current document!
        If Dir(tvs, vbDirectory) = "" Then GoTo jumpNullAndExit
          
        'tvs = tvs & newpathBase & Dir(tvs & ComFinalDoc_NewPathEnding, vbDirectory)
        Set tf2 = tfo2.GetFolder(tvs)
         
    Else    ' If it's the annex...
        
        'Dim annexFoldersStr As foundFilesStruct
        
        annexFoldersStr = bDir.bGDirAll(tvs & "\*annex*", "listFolders")
        
        ' Annex string supplied from document, gotta find those folders...
        If annexFoldersStr.count > 0 Then
            
            If annexFoldersStr.count = 1 Then
            
            'If Dir(tvs & "\annex*", vbDirectory) = "" Then
                      
                tvs = tvs & "\" & annexFoldersStr.files(0)
                Set tf2 = tfo2.GetFolder(tvs)
                
            Else    ' Multiple files found !
                
                'Dim annexPart As String     ' the part of string after the "annex" word
                
                annexPart = Split(LCase(cdpath), "annex")(1)
                
                
                'Dim annexNums As ExtractedNumbers   ' the numbers contained therein
                
                annexNums = gscrolib.Extr_NumbersStruct(annexPart)
                
                ' PASTED CODe from above, trying to get annex folder for all possible cases... TO DO put it in a function!
                
                ' If we get ONE annex num from document com doc spec string, we can  proceed with auto-identification:
                ' we match the num against those from the already extracted Comm annex subfolders!
                Select Case annexNums.count
                    
                    Case 1
                        
                        'Dim identified_Right_Annex As foundFilesStruct    ' to store correctly identified annex, or nill
                        
'                        For l = 1 To annexFoldersStr.count
'                            'if annexFoldersStr.files(k-1)
'                        Next l

                        identified_Right_Annex = bDir.bGDirAll(tvs & "\" & "*annex*" & annexNums.Numbers(0), listFolders)
                        
                        ' WE GOT IT! identified right annex, there's only one. HIT IT!
                        If identified_Right_Annex.count = 1 Then
                            
                        
                            tvs = tvs & "\" & identified_Right_Annex.files(0)
                            
                            ' check again and set folder
                            If Dir(tvs, vbDirectory) <> "" Then
                                Set tf2 = tfo2.GetFolder(tvs)
                            End If
                        
                        Else     ' Prob should add'em all to the textbox
                            
                            If identified_Right_Annex.count > 1 Then
                            
                                'Dim expected_AnnexNameMask As String

                                    'Dim tryTvs As String
                                    
                                    For i = 1 To identified_Right_Annex.count
                                    
                                        tryTvs = tvs & "\" & identified_Right_Annex.files(i - 1)
                                        
                                        expected_AnnexNameMask = tryTvs & "\*F#" & annexNums.Numbers(0) & "_*"
                                        
                                        If Dir(expected_AnnexNameMask) <> "" Then
                                            
                                            'Dim annexName As String
                                            annexName = Dir(expected_AnnexNameMask)
                                            
                                            tvs = tryTvs
                                            Set tf2 = tfo2.GetFolder(tvs)
                                            
                                            Exit For
                                            
                                        End If
                                    
                                    Next i

                            
                            ElseIf identified_Right_Annex.count = 0 Then
                                
                                For i = 1 To annexFoldersStr.count
                                    
                                    
                                    tryTvs = tvs & annexFoldersStr.files(i - 1)
                                    
                                    expected_AnnexNameMask = tryTvs & "\*F#" & annexNums.Numbers(0) & "*"
                                    
                                    If Dir(expected_AnnexNameMask) <> "" Then
                                        
                                        
                                        annexName = Dir(expected_AnnexNameMask)
                                        
                                        tvs = tryTvs
                                        Set tf2 = tfo2.GetFolder(tvs)
                                        
                                        Exit For
                                        
                                    End If
                                    
                                Next i
                                
                            End If
                        
                        End If
                        
                    Case 0  ' Not good news for auto folder ID, cause we got no annex number from doc, but more than one annex subfolder from Com doc base folder...
                        
                        Current_ComDoc_BaseFolder = tvs     ' let userform know which base folder we use, for later, when user chooses to open one file
                        Me.Toggle_ExpandUserform    ' Expand userform vertically to show multi files selection part
                        
                        ' Let user know
                        Me.Caption = Me.Caption & " - Choose annex to open"
                        
                        ' Present the subfdrs to the user, human knows best !...
                        For k = 1 To annexFoldersStr.count
                            Me.lbMultipleFilesFound.AddItem (annexFoldersStr.files(k - 1))
                        Next k
                        
                        GoTo jumpNullAndExit
                                
                    Case Is > 1     ' Probably exactly the same as Case 0, present all to user. BUT... Upon further study, perhaps we CAN auto-identify something. Or not. We'll see.
                            
                            ' we querry a folder with all those numbers in its name
                            identified_Right_Annex = bDir.bGDirAll(tvs & "\" & "*annex*" & Join(annexNums.Numbers, "*"), "listFolders")
                            
                            ' Surprise, only one found !
                            If identified_Right_Annex.count = 1 Then
                                
                                ' we founds proper file hosting folder, so we ammend folder string path
                                tvs = tvs & "\" & identified_Right_Annex.files(0)
                                
                                ' we check again and set folder to proper
                                If Dir(tvs, vbDirectory) <> "" Then
                                    
                                    Set tf2 = tfo2.GetFolder(tvs)
                                    
                                Else    ' Ooops!
                                End If
                                
                            Else    ' put'em up for display, user should choose
                            End If
                                                        
                End Select
                
            End If
        
        Else    '
            
            GoTo jumpNullAndExit
            
        End If
        
    End If
    
End If
    

' Once tf2 (folder which should contain doc(x) files) set, let's count files corresponding to Unit language!
For Each tf3 In tf2.files
    If tf3.Name Like "*[!a-z,A-Z,0-9]" & LCase(uLg) & "[!a-z,A-Z,0-9]*" Or tf3.Name Like "*[!a-z,A-Z,0-9]" & UCase(uLg) & "[!a-z,A-Z,0-9]*" Then
        z = z + 1
        Y = Y & "|" & tf3.Name
    End If
Next tf3

' Having counted the files named as we wish, let's take appropriate action
If z = 1 Then
    Get_CFODoc = tvs & "\" & Split(Y, "|")(1)
    Exit Function
Else
    ' Found NONE or MORE files corresponding to mask... maybe for z > 1 we should present them to user for choosing ?
    
jumpNullAndExit:
    
    Get_CFODoc = ""
    Exit Function
End If

' CleanUp before leaving
Set tfo2 = Nothing: Set tf2 = Nothing: Set tf3 = Nothing

End Function



Function OldBuild_CFOPath(cdpath As String) As String
' DESCRIPTION: supplied with a human-readable Commission document reference (ID) string, such as COM(2014) 254 final, or D032230/02 or others, return a
' string with main folder of such document from M drive, if such a folder exists
'
' version 0.8
'
' 0.8 - crudely modify to identify case when Commission File Folder Path is NEW kind, with more subfolders in the way and react appropriately


Dim tfo1 As New FileSystemObject    ' temporary fileobject 1
Dim ts As String                    ' temporary string
Dim tf1 As Folder                   ' temporary folder 1
Dim bf As Folder                    ' base folder (to revert one step from the path and look for the right name of the last subfolder
Dim ss As String                    ' special subfolder, in case the doc to be attached is actually a "volume x/yy" (annex in fact)
Dim gotpath As Boolean

' Extract type, year and number from cdpath string, supplied string extracted from document
'(containing standardized human format com doc number, like COM(2014) 225 final Annex
If LCase(Left$(cdpath, 3)) = "cdr" Then

    Build_CFOPath = ""
    
ElseIf Left$(cdpath, 3) = "DEC" Then

    tt = Left$(cdpath, 3)
    ty = Right$(Split(cdpath, "/")(1), 2)
    tn = Format(Mid$(Split(cdpath, "/")(0), 4, (Len(cdpath) - 3)), "00")
    
ElseIf Left$(cdpath, 1) <> "D" Then         ' COM, SEC, C, SWD
    
    ' And also build a little string (ss) to be used to identify subfolder containing Commission file (like "annex" or "annex?1" - contains wildcards)
    If InStr(1, cdpath, "(") = 0 And InStr(1, cdpath, "/") = 0 Then     ' Presence of parantheses and slash means no official Commission Document number
        Build_CFOPath = ""
        Exit Function
    End If
    
    tt = Trim(Split(cdpath, "(")(0))                                    ' temporary (Commission document) type
    ty = Right$(Split(Split(cdpath, "(")(1), ")")(0), 2)                ' temporary year
    tn = Format(Split(Trim(Replace(cdpath, Chr(160), " ")), " ")(1), "0000")  ' temporary number
        
ElseIf Left$(cdpath, 1) = "D" Then
    
    tt = Left$(cdpath, 1)
    ty = Right$(Trim$(DatePart("yyyy", Now)), 2)                   ' we use current year, D documents are very long runners... they come with 10 years or so behind... so they just place them in current year
    tn = Format(Mid$(Split(Replace(cdpath, Chr(160), " "), "/")(0), 2, Len(cdpath) - 1), "000000")  ' temporary number
    
End If


' The following part, with the following two ifs, tries to build ss string, flag for whether we have "volume" or "annex" into ComDoc String
Dim tss As ExtractedNumbers
Dim annexPart As String
    
'older case, used to be used a lot, not so much lately
If InStr(1, LCase$(cdpath), "volume") > 0 Then
    
    annexPart = Right$(cdpath, (Len(cdpath) - InStr(1, LCase(cdpath), "volume") + 1))
    
    tss = Extr_NumbersStruct(cdpath)
    
    If tss.count = 1 Then
        ss = "volume?" & tss.Numbers(0)
        'tss = ""
    End If
    
End If

'newer case, Com Doc ID contains "annex" type word, in EN or FR, or plural
If InStr(1, LCase(cdpath), "annex") > 0 Then
    
    annexPart = Right$(cdpath, (Len(cdpath) - InStr(1, LCase(cdpath), "annex") + 1))
    
    tss = Extr_NumbersStruct(annexPart)
            
    If tss.count = 0 Or tss.count = 2 Then   ' has annex in com doc string, but no number
        
        ss = "annex|annexe"     ' common case for all other, since we're using it with * wildcard and
                                ' they can be "annex", "annexe", "annexes". Case matters not, we use convert
                                ' to lower case from supplied string anyway
        
    ElseIf tss.count = 1 Then
        ' We may have a problem here, with case saying "Annexes 1 to 9", but with Commission sub-folder being called "ANNEX", or "ANNEXE", or "ANNEXES" (check last option)
        ' Probably need use if tss.count = 2, since string will most prob contain 2 numbers into tss struct, the starting and stopping annexes numbers.. as in 1 and 9, for the example above
        ss = "annex*" & tss.Numbers(0)
        
    End If
    
End If



' Using the above data (type, year, number) and subfolder substring (could be empty as well),
' we try to determine the path of file in question
If tt = "DEC" Then
    
    ts = "M:\Documents Externes\Commission\" & ty & "\" & "BUDGET " & tt & "\" & "DEC " & tn
    
    If tfo1.FolderExists(ts) Then
        Build_CFOPath = ts
    Else
        ts = "M:\Documents Externes\Commission\" & ty & "\" & "BUDGET " & tt
        If tfo1.FolderExists(ts) Then
            Build_CFOPath = ts
        End If
    End If

ElseIf tt <> "C" And tt <> "COM" And tt <> "SEC" And tt <> "D" And tt <> "SWD" And tt <> "JOIN" Then
    Build_CFOPath = "M:\Documents Externes\Commission"
Else
    ' do we descend into a subfolder (variable ss) named "...volume X...", "annex" or not?
    If ss = "" Then
        ts = "M:\Documents Externes\Commission\" & ty & "\" & tt & "\" & tn
    Else
        
        ' Now to actually find the real name of the subfolder in question
        Dim tsss As String
        tsss = "M:\Documents Externes\Commission\" & ty & "\" & tt & "\" & tn
        If tfo1.FolderExists(tsss) Then
        
            Dim tsssSubs As foundFoldersStruct
            tsssSubs = countFoldersNamed(tsss, ss)
            
            ' safeguard for case where we get input string like "Annex 1", but folder is "Annex" !!!! (why, Lord, oh, why are people SOOOOO inconsisten !?!?!?!)
            ' DOWN with humans, booooooo ! Replace them with robots! Go, robots, GO !
            If tsssSubs.count = 0 Then
                If InStr(1, ss, "annex") > 0 Then
                    tsssSubs = countFoldersNamed(tsss, "annex*")
                Else
                    tsssSubs = countFoldersNamed(tsss, "volume*")
                End If
            End If
            
            ' Take action based on number of subfolders retrieved
            If tsssSubs.count = 0 Then
                ' no subfolder count acc to mask, perhaps gather all subfolders of tsss and present them to user?
                MsgBox "Found NO folders according to mask, please write corresponding code to present all subfolders to user for choosing !", vbOKOnly
            ElseIf tsssSubs.count = 1 Then
                ts = tfo1.GetFolder(tsss & "\" & tsssSubs.folders(0))
            Else
                ' multiple subfolders found, place them in userform list and present them to user
                MsgBox "Found MULTIPLE folders according to mask, please write corresponding code to present all subfolders to user for choosing !", vbOKOnly
            End If
            
        End If
    
    End If
    
    If tfo1.FolderExists(ts) Then
        Build_CFOPath = ts
    Else
        
        If tfo1.FolderExists("M:\Documents Externes\Commission\" & ty & "\" & tt) Then
            Set bf = tfo1.GetFolder("M:\Documents Externes\Commission\" & ty & "\" & tt)
            
            For Each tf1 In bf.SubFolders
                If tf1.Name Like tn & "*" Then
                    Build_CFOPath = bf.Path & "\" & tf1.Name
                    gotpath = True
                    Exit For
                End If
            Next tf1
            
            If gotpath = False Then
                Build_CFOPath = bf
                Exit Function
            End If
        Else
            Build_CFOPath = "M:\Documents Externes\Commission"
        End If
            
    End If
End If

End Function


Sub Toggle_ExpandUserform()

' Chosen sizes are: 85 for small and 280 for large
If Me.Height < 90 Then
    Me.Height = 280     ' Expand
Else
    Me.Height = 85     ' Shrink
End If


End Sub
