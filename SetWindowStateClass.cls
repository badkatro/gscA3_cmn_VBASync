VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "SetWindowStateClass"
Attribute VB_Base = "0{FCFB3D2A-A0FA-1068-A738-08002B3371B5}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False


    Private Declare Function FindWindow Lib "user32" Alias _
            "FindWindowA" (ByVal lpClassName As String, _
            ByVal lpWindowName As String) As Integer
    Private Declare Function GetWindowPlacement Lib _
            "user32" (ByVal hWnd As Long, _
            ByRef lpwndpl As WINDOWPLACEMENT) As Long
    Private Declare Function SetWindowPlacement Lib "user32" _
           (ByVal hWnd As Long, ByRef lpwndpl As WINDOWPLACEMENT) As Long
    Private Const SW_SHOWMINIMIZED As Integer = 2
    Private Const SW_SHOWMAXIMIZED As Integer = 3
    Private Const SW_SHOWNORMAL As Integer = 1

    Private Type POINTAPI
        X As Long
        Y As Long
    End Type

    Private Type RECT
        Left_Renamed As Long
        Top_Renamed As Long
        Right_Renamed As Long
        Bottom_Renamed As Long
    End Type

    Private Type WINDOWPLACEMENT
        length As Long
        flags As Long
        showCmd As Long
        ptMinPosition As POINTAPI
        ptMaxPosition As POINTAPI
        rcNormalPosition As RECT
    End Type

   
    Public Sub windowAction(ByVal classname As String, ByVal action As String)

        Dim app_hwnd As Long
        Dim wp As WINDOWPLACEMENT
        
        app_hwnd = FindWindow(vbNullString, classname)
        wp.length = Len(wp)
        
        GetWindowPlacement app_hwnd, wp

        Select Case action

            Case "Minimize"
                 wp.showCmd = SW_SHOWMINIMIZED
            Case "Maximize"

                 wp.showCmd = SW_SHOWMAXIMIZED

            Case "Restore"
                wp.showCmd = SW_SHOWNORMAL
        End Select
        
        SetWindowPlacement app_hwnd, wp

    End Sub
