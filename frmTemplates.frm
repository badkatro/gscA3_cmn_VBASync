VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmTemplates 
   OleObjectBlob   =   "frmTemplates.frx":0000
   Caption         =   "Available Templates"
   ClientHeight    =   5475
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   3930
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   8
End
Attribute VB_Name = "frmTemplates"
Attribute VB_Base = "0{2BFB49F7-A492-4B59-A493-A474057A7D90}{77A310C2-2DA7-4642-B0D1-F0A5BEF3D7DA}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False




Private Sub UserForm_Initialize()

Dim tt As Template
Dim i As Integer

VBAReCreate.MsgString = ""

For Each tt In Application.Templates
    Me.ListBox1.AddItem
    i = i + 1
    Me.ListBox1.List(i - 1) = tt.Name
Next tt

End Sub

Private Sub cmdOK_Click()

Dim SelectedTemplates As String
Dim selnum As Integer

Me.Hide

' Count selected items
For k = 0 To Me.ListBox1.listCount - 1
    If Me.ListBox1.Selected(k) Then
        selnum = selnum + 1
    End If
Next k

'**************************************** EXPORT PART ********************************************************************
' Call vb components export routine, with or without commom message
If selnum = 1 Then
    For j = 0 To Me.ListBox1.listCount - 1
        If Me.ListBox1.Selected(j) Then
            'SelectedTemplates = IIf(SelectedTemplates = "", Me.ListBox1.List(j), SelectedTemplates & "," & Me.ListBox1.List(j))
            
            Call VBAReCreate.VBAExportComponents(Me.ListBox1.List(j))
            
        End If
    Next j
ElseIf selnum > 1 Then
    For j = 0 To Me.ListBox1.listCount - 1
        If Me.ListBox1.Selected(j) Then
            'SelectedTemplates = IIf(SelectedTemplates = "", Me.ListBox1.List(j), SelectedTemplates & "," & Me.ListBox1.List(j))
            
            Call VBAReCreate.VBAExportComponents(Me.ListBox1.List(j), "CommonMessageBox")
            
        End If
    Next j
Else
    MsgBox "No template was selected, exiting...", vbOKOnly + vbInformation, "Please select at least one template"
    Unload Me
    Exit Sub
End If

' If commom message, public variable MsgString is now filled with templates which were exported and number of vb components exported from each (modules, forms and classes components)
If VBAReCreate.MsgString <> "" Then
    MsgBox "Templates" & vbCrCr & MsgString & vbCrCr & "were successfully exported.", vbOKOnly + vbInformation, "Export finished"
End If
'************************************ EXPORT PART ***********************************************************************

' If user checked it, we also recreate template from scratch and reimport content from backup folder
If Me.cbRecreate.Value = -1 Then    ' selected
    For i = 0 To Me.ListBox1.listCount - 1
        If Me.ListBox1.Selected(i) Then
            Call VBAReCreate.NewTemplateFromFolder(Me.ListBox1.List(i))
        End If
    Next i
End If


Unload Me

End Sub
