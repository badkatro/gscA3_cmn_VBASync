Attribute VB_Name = "SpRep"
Public ShowClearButton As Boolean      ' Flag - show or not the "button" (userform) to clear highlight/text effects
Public PreReplacePercent As Boolean    ' Warning /show stopper for percent no space
Public PreReplaceCelsius As Boolean    ' Warning /show stopper for celsius no space
Public PreReplaceDash As Boolean       ' Warning /show stopper for range with dash no space

Const DocsFolder As String = "C:\Documents and Settings\"                   ' Space Replace
Const OfficeDataFolder As String = "\AppData\Roaming\Microsoft\Word\"    ' Space Replace

Public racounter As Integer                         ' ListAllSubFolders
Public SpToHspSearchExp As SpaceToHardSpace_Cases   ' Replace_Spaces_withHard_Spaces
'

Private CurrentSectionOnly As Boolean       ' Space Replace, target current section only

Public Type SpaceToHardSpace_Cases                  ' Replace_Spaces_withHard_Spaces
    ' Celsius degree symbol represents here a non-breaking space, more precisely character ChrW(160)
    alinno As String    ' alineatul(ui/ele)�11
    artno As String     ' articol(ul/ele)�11
    celsius As String   ' 22�C
    dates As String     ' 1�martie�2012
    decis As String     ' decizia�CE/1225
    direct As String    ' directiva(ei)�CE 1225
    enrange As String   ' 25�� 28 (separated by en dash)
    emrange As String   ' 25�� 28 (separated by em dash)
    footrefs As String  ' Before footnote references
    journal As String   ' JO C�1234/2000, JO L�1234/1999
    liter As String     ' litera(ele/elor)�(b)
    midname As String   ' domnul(dl/doamna/dna) J.�PSalinger
    Number As String    ' nr.�12 (or nr.�1256/2006 - for regulations identifiers)
    pageno As String    ' p.�5  (page number 5)
    percent As String   ' 5�% (percent and perthousand)
    recom As String     ' recomandarea(ile/arilor)
    regul As String     ' regulamentul(ele/ui)�(CE/UE)
    regno As String     ' intended for nr.�1210/2003 (identifier of regulations, number and year) - to determine if still needed
    signedno As String  ' +�2, -�3, ��5
    thousand As String  ' 12�000�000 (thousands separator, for numbers 1000 and above)
End Type

Sub AutoPilot_SecCommand_Initialize()
' Sets caption of secondary auto-pilot switching command (in the popup menu on "GSC RO Cleanup" toolbar,
' (last control), third entry - according to the setting found in ini file

Debug.Print "AutoPilot_SecCommand_Initialize has been RAN"

'Dim spb As CommandBarButton, spbs As CommandBarButton
'Dim tpop As CommandBarPopup
'Dim tapl As CommandBarButton
'Set tpop = Application.CommandBars("GSC RO Cleanup").Controls(9)
'
'Set tapl = tpop.Controls(4)
'
'Dim csu As String
'csu = gscrolib.sUserName
'
'Dim fso As New Scripting.FileSystemObject
'Dim CurrentAutoPilotState As String         ' value read from ini file
'
'If fso.FileExists(DocsFolder & csu & _
'    OfficeDataFolder & "SpaceReplaceOptions.ini ") Then
'
'    CurrentAutoPilotState = System.PrivateProfileString(DocsFolder & csu & _
'        OfficeDataFolder & "SpaceReplaceOptions.ini", "SpaceReplaceOptions", "AutoPilot")
'
'    If CurrentAutoPilotState = "AutoOff" Then
'        Set spb = Application.CommandBars("GSC RO Cleanup").FindControl(Tag:="SpRep")
'        Set spbs = Application.CommandBars("SpaceIcons").Controls(1)    ' Icon with A (for "autopilot is on")
'        spbs.CopyFace: spb.PasteFace
'
'
'        tapl.Caption = "Turn �Space Replace� AutoPilot On"
'    ElseIf CurrentAutoPilotState = "AutoOn" Then
'        Set spb = Application.CommandBars("GSC RO Cleanup").FindControl(Tag:="SpRep")
'        Set spbs = Application.CommandBars("SpaceIcons").Controls(2)    ' Icon with A (for "autopilot is on")
'        spbs.CopyFace: spb.PasteFace
'
'        tapl.Caption = "Turn �Space Replace� AutoPilot Off"
'    End If
'
'End If
'
'Set spb = Nothing: Set spbs = Nothing
'Set tpop = Nothing: Set tapl = Nothing
'Set fso = Nothing
End Sub

Sub AutoPilot_Switch()
' Command to actually switch on or off the autopilot feature
' (executed when)

Dim spb As CommandBarButton, spbs As CommandBarButton
Dim tpop As CommandBarPopup, tapl As CommandBarButton
Set tpop = Application.CommandBars("GSC RO Cleanup").Controls(8)

Set tapl = tpop.Controls(3)

Dim csu As String
csu = gscrolib.sUserName

If tapl.Caption = "Turn �Space Replace� AutoPilot On" Then
    System.PrivateProfileString(DocsFolder & csu & _
        OfficeDataFolder & "SpaceReplaceOptions.ini", "SpaceReplaceOptions", "AutoPilot") = "AutoOn"
        
    tapl.Caption = "Turn �Space Replace� AutoPilot Off"
    
    Set spb = Application.CommandBars("GSC RO Cleanup").FindControl(Tag:="SpRep")
    Set spbs = Application.CommandBars("SpaceIcons").Controls(2)    ' Icon with A (for "autopilot is on")
    'spbs.CopyFace: spb.PasteFace
    ChangeButtonImage spb, "c:\gsc6\Word\Cont\Lro\rocket.bmp"
    Debug.Print vbCr & "Just changed image on SpaceReplace!" & vbCr
    
    ' Change main button tooltip to reflect real situation
    If InStr(1, spb.TooltipText, "OFF") > 0 Then
        spb.TooltipText = Replace(spb.TooltipText, "OFF", "ON")
    Else
        spb.TooltipText = spb.TooltipText & ". AutoPilot is ON"
    End If

    
ElseIf tapl.Caption = "Turn �Space Replace� AutoPilot Off" Then
    System.PrivateProfileString(DocsFolder & csu & _
        OfficeDataFolder & "SpaceReplaceOptions.ini", "SpaceReplaceOptions", "AutoPilot") = "AutoOff"
        
    tapl.Caption = "Turn �Space Replace� AutoPilot On"
    
    Set spb = Application.CommandBars("GSC RO Cleanup").FindControl(Tag:="SpRep")
    Set spbs = Application.CommandBars("SpaceIcons").Controls(1)    ' Icon with A (for "autopilot is on")
    spbs.CopyFace: spb.PasteFace
        
    ' change main button icon
    If InStr(1, spb.TooltipText, "ON") > 0 Then
        spb.TooltipText = Replace(spb.TooltipText, "ON", "OFF")
    End If
    
End If

Set tpop = Nothing: Set tapl = Nothing
Set spb = Nothing: Set spbs = Nothing
End Sub


Sub Start_SpaceReplace()
' Main entry point, decides to show options/launch form or not, dep. on user prefference

' Lux Mod: allow for user running only saved-as-default checks, even with autopilot on!

On Error GoTo SpaceError

Dim ahHwnd As Long
Dim count_hspafter As Integer                   ' final count for the below
Dim count_hspbefore As Integer                  ' If doc already contains segments with hard spaces falling in the cathegory we count,

If Documents.count = 0 Then
    StatusBar = "Space Replace: Cannot work without an opened document, Exiting..."
    Exit Sub
Else
    If ActiveDocument.Name Like "Document#" Or ActiveDocument.Name Like "Document##" Then
        If ActiveDocument.StoryRanges(wdMainTextStory).Characters.count < 4 Then
            StatusBar = "Space Replace: Current opened document is empty, Exiting..."
            Exit Sub
        End If
    End If
End If

' Before doing anything, clear find-replace text boxes
Call gscrolib.ClearFindReplace

' If document is biggish or big or really-big, then we need warn/ entertain user until form is shown
' (can be even as high as 7-8 seconds or more (for docs of 350 pages, see 800 pages)
If ActiveDocument.StoryRanges(wdMainTextStory).Information(wdActiveEndPageNumber) > 30 Then
    Application.ScreenUpdating = True
    Call AhMsgBox_Show(vbCr & "Running preliminary checks on document... Please wait...", , , 160, , wdColorLightGreen, wdColorBlack)
    Call gscrolib.PauseForSeconds(0.2)
    Application.ScreenRefresh
End If

't1 = Timer
count_hspbefore = count_replacement_segments    ' we need extract this number from the final count.
't2 = Timer
'Debug.Print "Count hard spaces before: " & t2 - t1 & " seconds"

Application.ScreenUpdating = False

gscrolib.ClearFindReplace      ' Clear find replace boxes

Dim tmr1 As Single  ' timer 1
Dim tmr2 As Single  ' timer 2

Dim csu As String   ' system user
csu = gscrolib.sUserName

Dim fso As New Scripting.FileSystemObject
Dim AutoPilotCurrentState As String         ' AutoOn Or AutoOff

Dim SavedDefaultChecks As String        ' what user chose to run as default (Lux Mod)

't3 = Timer

Dim sFile As New SettingsFileClass
Dim k As Integer

If Not sFile.Exists Then
    sFile.Create_Empty
    PauseForSeconds (0.6)
    DoEvents
End If


NeedCreateIt:

If sFile.Exists Then
    If Not sFile.IsEmpty Then
        
        AutoPilotCurrentState = sFile.getOption("AutoPilotState", "SpaceReplaceOptions")
        
        SavedDefaultChecks = sFile.getOption("DefaultChecks", "SpaceReplaceOptions")
        
        If AutoPilotCurrentState = "" Or SavedDefaultChecks = "" Then
            Call SpToHsp_Initialize
            gscrolib.PauseForSeconds (0.6)
            DoEvents
            GoTo NeedCreateIt
        End If
        
    Else    ' Settings file is empty
        
        k = k + 1
        
        If k = 0 Then
            
            Call SpToHsp_Initialize
        
            gscrolib.PauseForSeconds (0.6)
            DoEvents
            GoTo NeedCreateIt
            
        Else
            Debug.Print "Attempted to write default SpaceReplace settings failed, exiting..."
            End
        End If
    End If

Else    ' Settings file does not exist
    
    If k = 0 Then
        k = k + 1
        Call SpToHsp_Initialize
        gscrolib.PauseForSeconds (0.6)
        DoEvents
        GoTo NeedCreateIt
        
    Else
        Debug.Print "Attempted to create settings file failed, exiting..."
        End
    End If
    
End If


If AutoPilotCurrentState = "on" Then    ' Have to proceed and do all replacements, mark segments with default settings if on AutoPilot
    
    ' Verify if 5%, 5�C & 1-2 are found (no space) and if so, chage to spaced version (white space)
    Call Check_PreReplacement
    If SpRep.PreReplaceCelsius Or SpRep.PreReplacePercent Or SpRep.PreReplaceDash Then
        Call Do_PreReplacements
    End If
    
    ' Lux Mod - retrieve saved-as-default wished checks instead of just running them all
    
    tmr1 = timer
    Call Replace_Spaces_withHard_Spaces(SavedDefaultChecks, 0)     ' Do all replacements from 0 to 18
    tmr2 = timer                                                                                           ' and mark with "default" (setting)
                                                                                                        
    count_hspafter = count_replacement_segments - count_hspbefore
                                                                                                           
    StatusBar = "Space Replace: Spaces are now replaced with Hard Spaces in " & "all (highlighted) segments !" & count_hspafter & _
        " segments were processed in " & Format(tmr2 - tmr1, "0.00") & " seconds"
                                                                                                           
    ' Clear All button
    If ShowClearButton = True Then
        frmClearHEC.Show
    End If
    
Else    ' off state
    ahHwnd = gscrolib.FindWindow("ThunderDFrame", "FormHndTest")
    If ahHwnd > 0 Then
        'Call gscrolib.DestroyWindow(ahHwnd)
        Call gscrolib.PauseForSeconds(0.2)
        Application.ScreenUpdating = True
        Application.ScreenRefresh
    End If
    
    frmSpHardSpRpl.Show     ' Show options form, from where we can start program (AutoPilot is off)
End If

Application.ScreenUpdating = True
Application.ScreenRefresh

ActiveDocument.Characters(1).Select: Selection.Collapse

' Switch to print view and hide rulers at end, annoying !
If ActiveWindow.View <> wdPrintView Then
    ActiveWindow.View = wdPrintView
End If

ActiveWindow.DisplayRulers = False


Exit Sub

SpaceError:
    If Err.Number = 4248 Then
        Err.Clear
        StatusBar = "Space Replace: Not working without opened document!"
    Else
        MsgBox "Error " & Err.Number & " occured in " & Err.Source, vbOKOnly + vbCritical, "Please debug Start_SpaceReplace"
        Err.Clear
    End
End If

Set fso = Nothing
End Sub


Sub ChangeButtonImage(WhichButton As CommandBarButton, LoadPicturePath As String)
' Copied from VBA Help as reference, exploring whether to use it to actually change the image
' of space replace's main button when user turns AutoPilot On. (Cannot use clipboard as we do now,
' it leaves image in clipboard and bugs user)

    Dim picPicture As IPictureDisp
    Dim picMask As IPictureDisp

    Set picPicture = stdole.StdFunctions.LoadPicture(LoadPicturePath)
    'Set picMask = stdole.StdFunctions.LoadPicture( _
        "c:\images\mask.bmp")

    'Reference the first button on the first command bar
    'using a With...End With block.
    'With Application.CommandBars.FindControl(msoControlButton)
        'Change the button image.
        
        WhichButton.Picture = picButton

        'Use the second image to define the area of the
        'button that should be transparent.
        '.Mask = picMask
    End With
End Sub


Sub SpToHsp_Initialize()

Dim fso As New Scripting.FileSystemObject
Dim sfstr As TextStream
Dim csu As String

csu = gscrolib.sUserName

Dim ls As String
Dim SettingsFilePath As String

ls = CStr(gscrolib.GetListSeparatorFromRegistry)

With SpToHspSearchExp
    ' paragraph number (numbered subdivisions of articles in legal acts - "alineat" in Romanian)
    .alinno = "([aA]lineat[eiloru]{2" & ls & "4})( )(\([0-9a-z]{1" & ls & "}\))"
    ' Article number format [articolul(ui) XX]
    .artno = "([aA]rticol[eilu]{2" & ls & "4})( )([0-9]{1" & ls & "})"
    ' 5 �C (with white - normal space) changed to hard-spaced version
    .celsius = "([0-9]{1" & ls & "})( )(�C)"
    ' Romanian language months are composed only of letters in question
    .dates = "([0-9]{1" & ls & "2})( )([a-gil-pr-u]{3" & ls & "10})( )([0-9]{4})"
    ' Decision "decizia 1200/03/CE"
    .decis = "([dD]ecizi[aei]{1" & ls & "2})( )([0-9]{1" & ls & "4}/[0-9]{1" & ls & "4}/[ECU]{1" & ls & "3})"
    ' Directive "directiva CE/UE 1200"
    .direct = "([dD]irectiv[aei]{1" & ls & "2})( )([0-9/]{1" & ls & "9}[CE]{2" & ls & "3})"
    ' number range, separated by en dash
    .enrange = "([0-9]{1" & ls & "})( )([" & ChrW(8211) & "])([ 0-9]{2" & ls & "})"     ' 8211 = en dash
    ' number range, separated by em dash
    .emrange = "([0-9]{1" & ls & "})( )([" & ChrW(8212) & "][ 0-9]{2" & ls & "})"     ' 8212 = em dash
    ' footnotes references, replace space before with hard space before
    .footrefs = "exception"     ' we launch special routine, no find/ replace all
    ' JO C 0001/2000 & JO L 00002/2000, space to hard space
    .journal = "(JO [CL])( )([0-9/]{1" & ls & "})"
    ' "litera (a)", numbered (or littered ?!) lists (paragraph), subdivisions of "alineate" (numbered paragraphs in legal texts)
    .liter = "([lL]iter[aeilor]{1" & ls & "4})( )(\([a-z]{1" & ls & "}\))"                               '
    ' middle name, keep them together, as in J.�C. Penny
    .midname = "([A-Z].)( )([A-Z].)( [a-zA-Z]{2" & ls & "})"
    ' nr.�15 (possibly even nr.�1252/2000, as in regulation numbers - maybe others ?)
    .Number = "(nr.)( )([0-9/]{1" & ls & "})"       ' replace "\1^s\3"
    ' page number/ range, as in p.�5, p.�5-7
    .pageno = "([p]{1" & ls & "2}.)( )([-0-9" & ChrW(8211) & "]{1" & ls & "})"      ' 8211 = en dash
    ' 5�%, meaning replace white space with non-breaking space
    .percent = "([0-9,]{1" & ls & "})( )(%)"        ' percents with space, REMEMBER, we need change 5% into 5�% as well !!!
    ' recomandation, as in "Recomandarea 1212/03/CE"
    .recom = "([rR]ecomandare[a" & ChrW(259) & "ielor]{1,6})( )"
    ' regulation, as in "Regulamentul (CE) or (UE)"
    .regul = "([rR]egulamentu[ilu]{1" & ls & "3})( )(\([CEU]{2}\))"
    ' Regulation numbers format (nr. 1234/2000)
    .regno = "(nr.)( )([0-9]{1" & ls & "}/[0-9]{4})"
    ' Signed numbers, as in +�3, -�2 and �5 (space to hard space)
    .signedno = "([+-])( )([0-9]{1" & ls & "})"
    ' group separator for large numbers (thousands separator), as in 18�000, space to hard space
    .thousand = "([0-9]{1" & ls & "3})( )([0-9]{3})( )([0-9]{3})( )([0-9]{3})( )([0-9]{3})" ' Numbers over 1000, using white space as thousands separator
End With


Dim sFile As New SettingsFileClass

If sFile.Exists Then
    If Not sFile.IsEmpty Then
        If sFile.getOption("MarkFoundWith", "SpaceReplaceOptions") = "" Or _
            sFile.getOption("HighlightColour", "SpaceReplaceOptions") = "" Or _
            sFile.getOption("AutoPilotState", "SpaceReplaceOptions") = "" Or _
            sFile.getOption("DefaultChecks", "SpaceReplaceOptions") = "" Then
            
            sFile.setOption "SpaceReplaceOptions", "MarkFoundWith|HighlightColour|AutoPilotState|DefaultChecks", "Nothing|65535|off|1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1"
            DoEvents
            
        End If
        
    Else
        sFile.setOption "SpaceReplaceOptions", "MarkFoundWith|HighlightColour|AutoPilotState|DefaultChecks", "Nothing,65535|off|1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1"
        DoEvents
    End If
Else
    sFile.Create_Empty
    sFile.setOption "SpaceReplaceOptions", "MarkFoundWith|HighlightColour|AutoPilotState|DefaultChecks", "Nothing|65535|off|1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1"
    DoEvents
End If


End Sub


Sub Replace_Spaces_withHard_Spaces(WhichReplacements As String, MarkWithWhat As Integer)

' Lux Mod: allow for users executing only saved-as-default checks, even if autopilot is on

' Main routine, does actual find and replace work
' WhichReplacements: String consisting of 0's and -1s identifying which members of the
' arr_SearchStrings are to be actually searched for. (corresponding to checked items in frmSpHardSpRpl form's listbox)
' MarkWithWhat: 0 for Nothing, 1 for highlight (read colour from ini file) and 2 for marching red ants text effect

Dim hcol As Long, gcount As Single
Dim csu As String
Dim arr_SearchStrings(19) As String
Dim rngTemp As Range, rngTStory As Range
Dim MarkWithDefault As String               ' Used to extract default user preference from ini file. User select one option and then
                                            ' param MarkWithWhat is 1, 2 or 3. If user selects nothing, MarkWithWhat is 0, MarkWithThis will
                                            ' read from file and be used in the loop

csu = gscrolib.sUserName    'System (Windows) user name
ShowClearButton = False

Dim ls As String
ls = gscrolib.GetListSeparatorFromRegistry


' Check and exit if no doc opened or if current doc is not SGC (RO)
If Documents.count = 0 Then
    MsgBox "Please open a documents first", vbOKOnly, "Error, no open document!"
    Exit Sub
Else
'    If gscrolib.Is_GSC_Doc(ActiveDocument.Name) = False Or gscrolib.Is_ULg_Doc(ActiveDocument.Name) = False Then
'        MsgBox "Please run this macro on a (correctly named) SGC (Council) document, RO version!", _
'            vbOKOnly, "Not a SGC RO Document"
'        Exit Sub
'    End If
End If
' Check and initialitze space to hard spaces replacement search strings
If SpToHspSearchExp.alinno = "" Then Call SpToHsp_Initialize

arr_SearchStrings(0) = SpToHspSearchExp.thousand
arr_SearchStrings(1) = SpToHspSearchExp.Number
arr_SearchStrings(2) = SpToHspSearchExp.pageno
arr_SearchStrings(3) = SpToHspSearchExp.journal
arr_SearchStrings(4) = SpToHspSearchExp.midname
arr_SearchStrings(5) = SpToHspSearchExp.percent
arr_SearchStrings(6) = SpToHspSearchExp.celsius
arr_SearchStrings(7) = SpToHspSearchExp.enrange
arr_SearchStrings(8) = SpToHspSearchExp.emrange
'arr_SearchStrings(9) = SpToHspSearchExp.signedno

arr_SearchStrings(10) = SpToHspSearchExp.footrefs   ' calling external routine from grodocedit
arr_SearchStrings(11) = SpToHspSearchExp.dates
arr_SearchStrings(12) = SpToHspSearchExp.regul
'arr_SearchStrings(13) = SpToHspSearchExp.recom     ' not decided yet
arr_SearchStrings(14) = SpToHspSearchExp.decis
arr_SearchStrings(15) = SpToHspSearchExp.direct
arr_SearchStrings(16) = SpToHspSearchExp.alinno
arr_SearchStrings(17) = SpToHspSearchExp.artno
arr_SearchStrings(18) = SpToHspSearchExp.liter

arr_SearchStrings(19) = "\1^s\3^s\5^s\7^s\9"                               ' Special Replacement String for numbers, replacing all occurences of space with hard space,
                                                                           ' all the way to the 5th group of "000"s - in fact up to 9.(9)*10^14
'ActiveWindow.View = wdNormalView
Options.Pagination = False

   
If MarkWithWhat = 0 Then    ' Initialize MarkWithDefault from ini file, but only if needed (if user chose none with his/her small fat hands)
    
    Dim sFile As New SettingsFileClass
    
    If sFile.Exists Then
        If Not sFile.IsEmpty Then
            MarkWithDefault = sFile.getOption("MarkFoundWith", "SpaceReplaceOptions")
            hcol = sFile.getOption("HighlightColour", "SpaceReplaceOptions")
        Else
        
        End If
    Else
    
    End If
    
    'MarkWithDefault = System.PrivateProfileString(DocsFolder & csu & OfficeDataFolder & _
        "SpaceReplaceOptions.ini", "SpaceReplaceOptions", "MarkFoundWith")     ' marking choice
        
    'hcol = System.PrivateProfileString(DocsFolder & csu & OfficeDataFolder & _
        "SpaceReplaceOptions.ini", "SpaceReplaceOptions", "HighlightColour")   ' highlight color
    
ElseIf MarkWithWhat = 1 Then    ' User chose highlight
    
    hcol = frmSpHardSpRpl.lblOptHighlight.BackColor
    
End If

' Dangerous! Forcefully overwrite user choice for marking segments, if this is highlight, if document ALREADY contains highlight !
If DocHasHighlight(ActiveDocument) Then
    If MarkWithWhat = 0 Then
        If MarkWithDefault = "Highlight" Then
            Call AhMsgBox_Show("Document already contains highlighted text, switching to RED ANTS Effect !", , , 140, , wdColorWhite, wdColorBlack)
            Call gscrolib.PauseForSeconds(1)
            MarkWithDefault = "RedAnts"
        End If
    ElseIf MarkWithWhat = 1 Then
        Call AhMsgBox_Show("Document already contains highlighted text, switching to RED ANTS Effect !", , , 140, , wdColorBrightGreen, wdColorBlack)
        Call gscrolib.PauseForSeconds(1)
        MarkWithWhat = 3
    End If
End If

Options.DefaultHighlightColorIndex = switch(hcol = wdColorYellow, wdYellow, hcol = wdColorBrightGreen, wdBrightGreen, hcol = wdColorTurquoise, wdTurquoise, _
        hcol = wdColorPink, wdPink, hcol = wdColorBlue, wdBlue, hcol = wdColorRed, wdRed, hcol = wdColorDarkBlue, wdDarkBlue, _
        hcol = wdColorTeal, wdTeal, hcol = wdColorGreen, wdGreen, hcol = wdColorViolet, wdViolet, _
        hcol = wdColorDarkRed, wdDarkRed, hcol = wdColorDarkYellow, wdDarkYellow, hcol = wdColorGray50, wdGray50, _
        hcol = wdColorGray25, wdGray25, hcol = wdColorBlack, wdBlack)

Call RplAll("[ ]{2" & ls & "}", " ", True, , False, False, False, True, False, False)   ' all double spaces to single
Call ClrFndRpl

NowItShould:
If SpToHspSearchExp.alinno <> "" Then   ' Process non-empty array items only (place holders)
    ' Process main text story range on m = 1 and footnotes story range on m = 2
    For m = 1 To 2
        ' Select, in turn, main text story and footnotes story to act as target range
        If m = 1 Then
            Set rngTStory = ActiveDocument.StoryRanges(wdMainTextStory)
        Else
            If ActiveDocument.Footnotes.count > 0 Then
                Set rngTStory = ActiveDocument.StoryRanges(wdFootnotesStory)
            Else
                GoTo outloop
            End If
        End If
        
        ' Loop through all available expressions (at the moment) :(
        For k = 0 To UBound(arr_SearchStrings) - 1
            ' We only execute the find/replace operations the user has selected (parameter WhichReplacements is a string
            ' containing 0's ans -1's, corresponding with value of respective selected listbox entries from form
            If arr_SearchStrings(k) <> "" Then      ' avoid empty substrings (to be removed later)
                If Split(WhichReplacements, "-")(k) = "1" Then     ' Only if user chose to replace this case
                
                    If k <> 10 Then
                    
                        Set rngTemp = rngTStory.Duplicate
                
                        With rngTemp.Find
                            
                            .ClearAllFuzzyOptions
                            .ClearFormatting
                            .Forward = True
                            .Format = True
                            .Wrap = wdFindStop
                            .MatchWildcards = True
                            .Text = arr_SearchStrings(k)
                            '.Replacement.Font.Color = wdColorAutomatic      ' Safety restoration for some highlight cases
                            .Replacement.Text = "\1^s\3"
                            
                            Select Case MarkWithWhat
                                Case 0      ' Default
                                    Select Case MarkWithDefault
                                            
                                        Case "Highlight"
                                            .Replacement.Highlight = True
                                            If Options.DefaultHighlightColorIndex = wdDarkBlue Or _
                                                Options.DefaultHighlightColorIndex = wdDarkRed Or _
                                                Options.DefaultHighlightColorIndex = wdDarkYellow Or _
                                                Options.DefaultHighlightColorIndex = wdBlack Or _
                                                Options.DefaultHighlightColorIndex = wdGray50 Or _
                                                Options.DefaultHighlightColorIndex = wdViolet Or _
                                                Options.DefaultHighlightColorIndex = wdBlue Or _
                                                Options.DefaultHighlightColorIndex = wdRed Then
                                                .Replacement.Font.Color = wdColorWhite
                                            End If
                                        Case "RedAnts"
                                            .Replacement.Font.Animation = wdAnimationMarchingRedAnts
                                    End Select
                                Case 1      ' Highlight
                                    .Replacement.Highlight = True
                                    If Options.DefaultHighlightColorIndex = wdDarkBlue Or _
                                        Options.DefaultHighlightColorIndex = wdDarkRed Or _
                                        Options.DefaultHighlightColorIndex = wdDarkYellow Or _
                                        Options.DefaultHighlightColorIndex = wdBlack Or _
                                        Options.DefaultHighlightColorIndex = wdGray50 Or _
                                        Options.DefaultHighlightColorIndex = wdViolet Or _
                                        Options.DefaultHighlightColorIndex = wdBlue Or _
                                        Options.DefaultHighlightColorIndex = wdRed Then
                                        .Replacement.Font.Color = wdColorWhite
                                    End If
                                                                    
                                Case 3      ' Red Marching Ants font effect
                                    .Replacement.Font.Animation = wdAnimationMarchingRedAnts
                            End Select
                            
                            ' Alter replace expression for dates, where we have 5 sub-expressions, and for middle name case
                            ' (where we need expression 4 as well), while for the rest of them we only have 3 sub-expressions
                            If k = 11 Then
                                .Replacement.Text = .Replacement.Text & "^s\5"
                                .Execute Replace:=wdReplaceAll
                            ElseIf k = 4 Then
                                .Replacement.Text = .Replacement.Text & "\4"
                                .Execute Replace:=wdReplaceAll
                            ElseIf k = 7 Then
                                .Replacement.Text = "\1^s" & ChrW(8211) & "\4"  ' 8211 = en dash
                                .Execute Replace:=wdReplaceAll
                            ElseIf k = 0 Then
                                
                                .Replacement.Text = arr_SearchStrings(19)
                                
                                ' We search and replace all occurences of 5 groups of "000's"
                                For l = 0 To 3
                                    .Text = Left$(arr_SearchStrings(k), Len(arr_SearchStrings(k)) - l * 13)
                                    .Replacement.Text = Left(arr_SearchStrings(19), Len(arr_SearchStrings(19)) - l * 4)
                                    .Execute Replace:=wdReplaceAll
                                Next l
                                
                            Else    ' For k <> 0 And k <> 11 And k <> 4
                                .Execute Replace:=wdReplaceAll
                            End If
                            
                        End With
                    
                    Else    ' k = 10
                        If m = 1 Then   ' Don't call All_FootnoteRefs_Bold (which also add hard space before'em) for footnotes story range!
                            Call All_FootnoteRefs_HspBef_AndBold(True)  ' IT's a rule, stop asking people !
                        End If
                    End If  ' k <> 10
                    
                End If
                
            End If
        
        ActiveDocument.UndoClear
        Next k

outloop:
    Next m
        
Else
    Call SpToHsp_Initialize
    GoTo NowItShould
End If

Options.Pagination = True
ActiveDocument.Characters(1).Select: Selection.Collapse

' Change al double spaces into single spaces in main range
Do While Find("  ", True, 1)
    Call RplAll("  ", " ", True, 1)
Loop

' Launch a special form (non-modal) to allow user to clear markings with one stroke
' before archiving document
If MarkWithWhat <> 2 Or MarkWithWhat = 0 And MarkWithDefault <> "Nothing" Then
    ShowClearButton = True
End If

End Sub
Function count_replacement_segments()

Dim chars_before As Double
Dim chars_after As Double
Dim haveFootnotes As Boolean

Dim ls As String
ls = gscrolib.GetListSeparatorFromRegistry

' obtaining character number before the count
chars_before = ActiveDocument.StoryRanges(wdMainTextStory).Characters.count
' from footnotes as well
If ActiveDocument.Footnotes.count > 0 Then
    haveFootnotes = True
    chars_before = chars_before + ActiveDocument.StoryRanges(wdFootnotesStory).Characters.count
End If

' marking all changed segments with a # character
ActiveDocument.StoryRanges(wdMainTextStory).Find.Execute FindText:="([a-zA-Z0-9,.]{1" & ls & "}^s[0-9a-zA-Z%\(\)^s�/]{1" & ls & "})", _
    MatchWildcards:=True, ReplaceWith:="#\1", Replace:=wdReplaceAll
' footnotes as well
If haveFootnotes Then
    ActiveDocument.StoryRanges(wdFootnotesStory).Find.Execute FindText:="([a-zA-Z0-9,.]{1" & ls & "}^s[0-9a-zA-Z%\(\)^s�/]{1" & ls & "})", _
    MatchWildcards:=True, ReplaceWith:="#\1", Replace:=wdReplaceAll
End If

chars_after = ActiveDocument.StoryRanges(wdMainTextStory).Characters.count
' from footnotes as well
If haveFootnotes Then
    chars_after = chars_after + ActiveDocument.StoryRanges(wdFootnotesStory).Characters.count
End If

'ActiveDocument.Undo
'ActiveDocument.Undo

ActiveDocument.UndoClear

ActiveDocument.StoryRanges(wdMainTextStory).Find.Execute FindText:="#", ReplaceWith:="", Replace:=wdReplaceAll
If haveFootnotes Then
    ActiveDocument.StoryRanges(wdFootnotesStory).Find.Execute FindText:="#", ReplaceWith:="", Replace:=wdReplaceAll
End If

If chars_after > chars_before Then
    count_replacement_segments = chars_after - chars_before
Else
    count_replacement_segments = 0
End If


End Function
Sub Check_PreReplacement()
' version 0.55
' Called from initialization of main userform, frmSpHardSpRpl, to display warning/ prevent action
' if game-stopping cases are found (now, 5%, 5�C and 1-2)
' 0.55 - eliminate 1-2 case, in RO we DO denote ranges using - and no spaces!

' First reset the three public variables to false, before checking and setting them !
' (Too lazy to actually use property setting-reading of module instead of public variables, so sue me !
SpRep.PreReplaceCelsius = False: SpRep.PreReplacePercent = False: SpRep.PreReplaceDash = False

Call RplAll("([.+0-9]{4" & ls & "})(-)([.0-9]{7" & ls & "})", "\1^30\3", True, , False, False, False, True, False, False)   ' protect phone numbers by replacing dash with non-breaking dash
Call ClrFndRpl

' Look for and display warning/ action stopper if two exceedingly common eroneous cases are found
Dim pcnsp As Boolean, clnsp As Boolean, dsnsp As Boolean
ls = gscrolib.GetListSeparatorFromRegistry
' first the most common, percent without space before
pcnsp = ActiveDocument.StoryRanges(wdMainTextStory).Find.Execute(FindText:="([0-9,]{1" & ls & "})(%)", MatchWildcards:=True)
' if not found in main story, see if it's in the eventual footnotes story
If pcnsp = False Then
    If ActiveDocument.Footnotes.count > 0 Then: pcnsp = ActiveDocument.StoryRanges(wdFootnotesStory).Find.Execute(FindText:="([0-9,]{1" & ls & "})(%)", MatchWildcards:=True)
End If
' same for celsius without space
clnsp = ActiveDocument.StoryRanges(wdMainTextStory).Find.Execute(FindText:="([0-9,]{1" & ls & "})(�C)", MatchWildcards:=True)
If clnsp = False Then
    If ActiveDocument.Footnotes.count > 0 Then: clnsp = ActiveDocument.StoryRanges(wdFootnotesStory).Find.Execute(FindText:="([0-9,]{1" & ls & "})(�C)", MatchWildcards:=True)
End If
' same for range with dash, without space (1-2 martie) & en dash with no space
'dsnsp = ActiveDocument.StoryRanges(wdMainTextStory).Find.Execute(findtext:="([0-9]{1" & ls & "})([-" & ChrW(8211) & "])([0-9]{1" & ls & "})", MatchWildcards:=True)
'If dsnsp = False Then
    'If ActiveDocument.Footnotes.Count > 0 Then: dsnsp = ActiveDocument.StoryRanges(wdFootnotesStory).Find.Execute(findtext:="([0-9]{1" & ls & "})([-" & ChrW(8211) & "])([0-9]{1" & ls & "})", MatchWildcards:=True)
'End If

' If, as expected, some are found, stop show !
If pcnsp = True Or clnsp = True Or dsnsp = True Then
    frmSpHardSpRpl.Caption = "Errors found - clear to continue!"
    With frmSpHardSpRpl.lblWarn
        .Visible = True   ' Show warning
        .Caption = vbCr & frmSpHardSpRpl.lblWarn.Caption
        .ControlTipText = frmSpHardSpRpl.lblWarn.ControlTipText & vbCr & "Click to replace them with spaced version!"
    End With
    
    If pcnsp = True Then    ' percent no space found
        PreReplacePercent = True
        frmSpHardSpRpl.lblWarn.Caption = frmSpHardSpRpl.lblWarn.Caption & vbCrCr & "00%"
    End If
    If clnsp = True Then    ' celsius no space found
        If pcnsp = True Then
            frmSpHardSpRpl.lblWarn.Height = frmSpHardSpRpl.lblWarn.Height + 10
            frmSpHardSpRpl.lblWarn.Caption = frmSpHardSpRpl.lblWarn.Caption & vbCr & "00�C"
        Else
            frmSpHardSpRpl.lblWarn.Caption = frmSpHardSpRpl.lblWarn.Caption & vbCrCr & "00�C"
        End If
            
        PreReplaceCelsius = True
    End If
    If dsnsp = True Then    ' dash range no space
        If pcnsp = True Or clnsp = True Then
            frmSpHardSpRpl.lblWarn.Height = frmSpHardSpRpl.lblWarn.Height + 10
            frmSpHardSpRpl.lblWarn.Caption = frmSpHardSpRpl.lblWarn.Caption & vbCr & "1-2"
        Else
            frmSpHardSpRpl.lblWarn.Caption = frmSpHardSpRpl.lblWarn.Caption & vbCrCr & "1-2"
        End If

        PreReplaceDash = True
    End If
    
    frmSpHardSpRpl.lblWarn.Caption = frmSpHardSpRpl.lblWarn.Caption & vbCr & "found !"
    With frmSpHardSpRpl.cbtGo
        .Enabled = False
        .Font.Name = "Tahoma"
        .Font.Size = 8
        .Caption = "No Go"
        .WordWrap = True
    End With
    
End If


End Sub
Sub Do_PreReplacements()
' Before even launching anything, put a space before %, �C and change dash in ranges with en dash, also adding spaces before and after it,
' since it's forbidden to use them as such in Council docs

Dim Main As Range, foot As Range
Dim ls As String
ls = gscrolib.GetListSeparatorFromRegistry

Set Main = ActiveDocument.StoryRanges(wdMainTextStory)
' If found percent, pre-replace
If SpRep.PreReplacePercent Then
    Main.Find.Execute FindText:="([0-9,]{1" & ls & "})(%)", MatchWildcards:=True, ReplaceWith:="\1 \2", Replace:=wdReplaceAll
    
    If ActiveDocument.Footnotes.count > 0 Then
        Set foot = ActiveDocument.StoryRanges(wdFootnotesStory)
        foot.Find.Execute FindText:="([0-9,]{1" & ls & "})(%)", MatchWildcards:=True, ReplaceWith:="\1 \2", Replace:=wdReplaceAll
    End If
End If
' if found celsius, pre-replace
If SpRep.PreReplaceCelsius Then
    Main.Find.Execute FindText:="([0-9,]{1" & ls & "})(�C)", MatchWildcards:=True, ReplaceWith:="\1 \2", Replace:=wdReplaceAll
    
    If ActiveDocument.Footnotes.count > 0 Then
        Set foot = ActiveDocument.StoryRanges(wdFootnotesStory)
        foot.Find.Execute FindText:="([0-9,]{1" & ls & "})(�C)", MatchWildcards:=True, ReplaceWith:="\1 \2", Replace:=wdReplaceAll
    End If
End If
' if found dash, pre-replace
If SpRep.PreReplaceDash Then
    Main.Find.Execute FindText:="([0-9]{1" & ls & "})([-" & ChrW(8211) & "])([0-9]{1" & ls & "})", MatchWildcards:=True, _
        ReplaceWith:="\1 " & ChrW(8211) & " \3", Replace:=wdReplaceAll      ' 8211 = en dash
    
    If ActiveDocument.Footnotes.count > 0 Then
        Set foot = ActiveDocument.StoryRanges(wdFootnotesStory)
        foot.Find.Execute FindText:="([0-9]{1" & ls & "})([-" & ChrW(8211) & "])([0-9]{1" & ls & "})", MatchWildcards:=True, _
            ReplaceWith:="\1 " & ChrW(8211) & " \3", Replace:=wdReplaceAll
    End If
End If
' Green light interface
frmSpHardSpRpl.lblWarn.Caption = vbCr & " All" & vbCr & "Errors" & vbCr & "Cleared"
frmSpHardSpRpl.lblWarn.Height = 52
frmSpHardSpRpl.Caption = "GSC - �Space Replace� Options"
With frmSpHardSpRpl.cbtGo
    .Caption = "Go"
    .Font.Name = "HellasFun"
    .Font.Size = 18
    .Enabled = True
End With
' Send away warning box automatically
Call gscrolib.PauseForSeconds(1.5)
frmSpHardSpRpl.lblWarn.Visible = False

Set Main = Nothing
Set foot = Nothing

End Sub
Function DocHasHighlight(TargetDocument As Document) As Boolean
' Returns true if document contains highlight segments

Dim footNotesChecked As Boolean
Dim tdoc As Document
Set tdoc = TargetDocument

Dim trng As Range
Set trng = tdoc.StoryRanges(wdMainTextStory)

searchFt:
With trng.Find
    .ClearAllFuzzyOptions
    .ClearFormatting
    .Format = True
    .Highlight = True
    
    If .Execute(FindText:="", Forward:=True, Wrap:=wdFindStop, Format:=True, Replace:=wdReplaceNone, _
        MatchWholeWord:=False) Then
        
        If trng.Text <> vbCr And trng.Text <> vbLf And trng.Text <> vbCrLf Then
        
            trng.Select
            DocHasHighlight = True
        
        Else
            
            trng.HighlightColorIndex = wdNoHighlight
            GoTo searchFt
            
        End If
        
    Else
        If tdoc.Footnotes.count > 0 And Not footNotesChecked Then
            Set trng = tdoc.StoryRanges(wdFootnotesStory)
            footNotesChecked = True
            GoTo searchFt
        Else
            DocHasHighlight = False
        End If
    End If
End With

End Function
Sub DocHHigh_Driver()

If DocHasHighlight(ActiveDocument) Then
    MsgBox ActiveDocument.Name & " HAS highlight text !", vbOKOnly + vbCritical, "Danger !"
Else
    MsgBox ActiveDocument.Name & " HAS NO highlight text !", vbOKOnly + vbInformation, "Safe !"
End If

End Sub
