VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmSpHardSpRpl 
   OleObjectBlob   =   "frmSpHardSpRpl.frx":0000
   Caption         =   "GSC - �Space Replace� Options"
   ClientHeight    =   8295
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4890
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   157
End
Attribute VB_Name = "frmSpHardSpRpl"
Attribute VB_Base = "0{CD004EC7-EF4B-4566-A817-55D0DC4B46F4}{26A65D98-49E6-4A21-BCE5-CB5EC3F19D77}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False



Private MarkWithWhat As Integer         ' Chosen way of marking items, passed as parameter to Replace_Spaces_withHard_Spaces
Const DocsFolder As String = "C:\Documents and Settings\"
Const OfficeDataFolder As String = "\AppData\Roaming\Microsoft\Word\"

Private Sub cbCtSect_Change()

If Me.cbCtSect.Value = True Then
    
Else
    
End If

End Sub


Private Sub UserForm_Initialize()

Dim fso As New Scripting.FileSystemObject
Dim SearchCases(20) As String
Dim csu As String       ' current system user

csu = gscrolib.sUserName

MarkWithWhat = 0    ' By default, if it's not changed, it will not mark found/changed segments

SearchCases(0) = "---Compulsory Items (do not clear)---"
SearchCases(1) = "As thousands separator (�10" & ChrW(8226) & "000�)"
SearchCases(2) = "In �nr." & ChrW(8226) & "00� (nr. 12)"
SearchCases(3) = "In �p." & ChrW(8226) & "00� (p. 12)"
SearchCases(4) = "In �JO L(C)" & ChrW(8226) & "0000/1111�"
SearchCases(5) = "In �domnul J." & ChrW(8226) & "C. Dupont"

SearchCases(6) = "In �00" & ChrW(8226) & "%�"
SearchCases(7) = "In �00" & ChrW(8226) & "�C�"
SearchCases(8) = "In �00" & ChrW(8226) & ChrW(8211) & " 11� (en dash)"
SearchCases(9) = "In �00" & ChrW(8226) & ChrW(8212) & " 11� (em dash)"
SearchCases(10) = "In �+" & ChrW(8226) & "00�, �-" & ChrW(8226) & "00�" ' & ��" & ChrW(8226) & "00�

SearchCases(11) = "----Optional Items (select group)----"
SearchCases(12) = "Before footnote references"
SearchCases(13) = "In Dates (�1" & ChrW(8226) & "martie" & ChrW(8226) & "2012�)"
SearchCases(14) = "In �Regulamentul(ui)" & ChrW(8226) & "(CE)/(UE) nr." & ChrW(8226) & "xx/yy�"
SearchCases(15) = "In �Recomandarea(ii)" & ChrW(8226) & "XXXX/YY/CE"
SearchCases(16) = "In �Decizia(ei)" & ChrW(8226) & "XXXX/YY/CE"
SearchCases(17) = "In Directiva(ei)" & ChrW(8226) & "CE XXXX"
SearchCases(18) = "In �alineatul(ui/le)" & ChrW(8226) & "(00)�"
SearchCases(19) = "In �articolul(ui/le)" & ChrW(8226) & "(00)�"
SearchCases(20) = "In �litera(lor/le)" & ChrW(8226) & "(z)�"

t5 = timer

Dim sFile As New SettingsFileClass

If sFile.Exists Then
    If Not sFile.IsEmpty Then
        
        Me.lblOptHighlight.BackColor = sFile.getOption("HighlightColour", "SpaceReplaceOptions")
        
        Dim savedMarkage As String
        
        savedMarkage = sFile.getOption("MarkFoundWith", "SpaceReplaceOptions")
        
        Select Case savedMarkage
            Case "Highlight"
                Me.lblOptHighlight.SpecialEffect = fmSpecialEffectSunken
            Case "Nothing"
                Me.lblOptNothing.SpecialEffect = fmSpecialEffectSunken
            Case "RedAnts"
                Me.lblOptRedAnts.SpecialEffect = fmSpecialEffectSunken
        End Select
        
        
        Dim SavedDefaultChecks As String
        
        SavedDefaultChecks = sFile.getOption("DefaultChecks", "SpaceReplaceOptions")
        
        Dim defaultChecksArr As Variant
        
        defaultChecksArr = Split(SavedDefaultChecks, "-")
                
    Else
    End If
Else
End If


t6 = timer
'Debug.Print " frmSpHardSpRpl.Initialize, check ini existence, format form object acc: " & t6 - t5 & " seconds"

t7 = timer
' Populate listbox with array items
For k = 0 To 20
    Me.lbxSearchCases.AddItem
    Me.lbxSearchCases.List(k) = SearchCases(k)
Next k

' Select and deselect cases acc to saved prefs
For m = 1 To lbxSearchCases.listCount - 1  ' compulsory replacement cases (cf. to InterInstitutional formatting guide)
    If m < 11 Then
        Me.lbxSearchCases.Selected(m) = IIf(defaultChecksArr(m - 1) = "1", True, False)
    ElseIf m > 12 Then
        Me.lbxSearchCases.Selected(m) = IIf(defaultChecksArr(m - 2) = "1", True, False)
    ElseIf m = 12 Then   ' m = 11, Intermedary header
        Me.lbxSearchCases.Selected(m) = False   ' Force before footnote references to false ! It's apparntly
    End If
Next m
t8 = timer
'Debug.Print "Populate lists with items & autocheck all: " & t8 - t7 & " seconds"


' Select or deselect first header?
lbxSearchCases.Selected(0) = True
For i = 1 To 10
    If lbxSearchCases.Selected(i) = False Then
        lbxSearchCases.Selected(0) = False
        Exit For
    End If
Next i

' Select or deselect intermediary header?
lbxSearchCases.Selected(11) = True
For i = 11 To 20
    If lbxSearchCases.Selected(i) = False Then
        lbxSearchCases.Selected(11) = False
        Exit For
    End If
Next i


t9 = timer
Call SpRep.Check_PreReplacement   ' modify main userform config, disable go button, print warning/ advice to do pre-replacements first !
t10 = timer
'Debug.Print "Checking pre-replacement cases & signalling user: " & t10 - t9 & " seconds"

Set fso = Nothing
End Sub


Private Sub cbtGo_Click()
' GO button, starts program execution

Dim count_hspafter As Integer                       ' final count for the below
Dim count_hspbefore As Integer                      ' If doc already contains segments with hard spaces falling in the cathegory we count,
Let count_hspbefore = count_replacement_segments    ' we need extract this number from the final count.

Me.Hide

Dim tmr1 As Single, tmr2 As Single
Dim callwithparams As String

Dim tc As control
Dim tlb As MSForms.ListBox

Set tlb = Me.lbxSearchCases

For j = 1 To tlb.listCount - 1   ' entry 0 is first header, for compulsory items list
    If j <> 11 Then     ' 11 is the header for selecting second part of list
        'If tlb.Selected(j) Then ' if current item is checked
            If j < 20 Then
                callwithparams = callwithparams & -1 * CInt(CBool(tlb.Selected(j))) & "-"   ' we shy away from using -1 as truth, we prefer 1 !
            Else
                callwithparams = callwithparams & -1 * CInt(CBool(tlb.Selected(j)))
            End If
        'End If
    End If
Next j

tmr1 = timer
Call SpRep.Replace_Spaces_withHard_Spaces(callwithparams, MarkWithWhat)
tmr2 = timer
    
count_hspafter = count_replacement_segments - count_hspbefore

    Call gscrolib.msgbox_pausedisplay("Spaces are now replaced with Hard Spaces in all (highlighted) segments !" & _
        vbCrCr & count_hspafter & " segments were processed in " & Format(tmr2 - tmr1, "0.00") & " seconds.", "Replacing Done!", 3)
        
Unload Me

ActiveDocument.Characters(1).Select: Selection.Collapse

' Clear All button
If SpRep.ShowClearButton = True Then
    frmClearHEC.Show
End If

Set tc = Nothing
Set tch = Nothing
End Sub

Private Sub cbtSelectAll_Click()
' Select All checkboxes

Dim tc As control
Dim tch As MSForms.CheckBox

For Each tc In Me.Controls
    If Left$(tc.Name, 3) = "chk" Then
        Set tch = tc
        tch.Value = True
    End If
Next tc

End Sub

Private Sub cbtSelectNone_Click()
' Select none (check boxes)

Dim tc As control
Dim tch As MSForms.CheckBox

For Each tc In Me.Controls
    If Left$(tc.Name, 3) = "chk" Then
        Set tch = tc
        tch.Value = False
    End If
Next tc

End Sub

Private Sub cbtSetDefault_Click()
' Set preffered option for marking found segments as default option

Dim fso As New Scripting.FileSystemObject

Dim csu As String
csu = gscrolib.sUserName

Dim checkCount As Byte
checkCount = 0

Dim chosenOption As String          ' Fetch user preference for saving to prefs file (.ini)
Dim chosenHighlightColor As Long    ' Fetch highlight color, if any chosen

Dim tc As control                   ' Temporary UserForm control
Dim clbl As MSForms.label           ' Label control

' Identify user-chosen label (option)
For Each tc In Me.Controls
    If Left$(tc.Name, 3) = "lbl" Then   ' Parse labels only
        If Mid$(tc.Name, 4, 3) = "Opt" Then     'Identify only options labels, those on bottom of form
            Set clbl = tc
            
            ' If option (label) was clicked by user, fetch which option and which color (if highlight)
            If clbl.SpecialEffect = fmSpecialEffectSunken Then
                If InStr(1, tc.Name, "Highlight") > 0 Then      ' User chose highlight
                    chosenOption = "Highlight"
                    chosenHighlightColor = clbl.BackColor
                Else    ' or RedAnts /Nothing
                    chosenOption = Mid$(tc.Name, 7, (Len(tc.Name) - 6))     ' "RedAnts" or "Nothing"
                End If
            End If
        End If
    End If
Next tc


' Addagio, Sept 2015, Luxembourg mod: Add possibility to save to default settings cases which should be checked at
' userform initialization, read from settings file

Dim defaultChecks As String     ' Allow Lux guys to save their pref checks, these will be auto-marked to "on"


For i = 1 To lbxSearchCases.listCount - 1
    
    If i <> 11 Then     ' Intermediary header number, not an actual list item!
    
        If lbxSearchCases.Selected(i) = True Then
            defaultChecks = IIf(defaultChecks = "", 1, defaultChecks & "-1")
        Else
            defaultChecks = IIf(defaultChecks = "", 0, defaultChecks & "-0")
        End If
    
    End If
    
Next i


' Luxembourg mod


SettingsNow:

Dim sFile As New SettingsFileClass

If sFile.Exists Then
    ' empty or not, it still needs writing...
    sFile.setOption "SpaceReplaceOptions", "MarkFoundWith|HighlightColour|DefaultChecks", chosenOption & "|" & chosenHighlightColor & "|" & defaultChecks

Else    ' settings file does not exist !
    
    Call SpRep.SpToHsp_Initialize
    checkCount = checkCount + 1
    
    If checkCount > 1 Then MsgBox "Error encountered trying to create initialization file, please debug!", vbOKOnly + vbCritical, _
        "Infinite Loop Danger in cbtSetDefault_Click": Exit Sub
    
    GoTo SettingsNow
    
End If


Set sFile = Nothing

End Sub

Private Sub Image1_Click()
' Launch help window with help index preloaded

Call MakeForm_Resizable(frmSpaceReplaceHelp)

frmSpaceReplaceHelp.Show

End Sub

Sub MakeForm_Resizable(TargetUserform As UserForm)
    Dim UFHWnd As Long    ' HWnd of UserForm
    Dim WinInfo As Long   ' Values associated with the UserForm window
    Dim R As Long
    'Const GWL_STYLE = -16
    Const WS_SIZEBOX = &H40000
    Const WS_USER  As Long = &H4000
    Load TargetUserform ' Load the form into memory but don't make it visible
    
    UFHWnd = FindWindow("ThunderDFrame", TargetUserform.Caption)  ' find the HWnd of the UserForm
    If UFHWnd = 0 Then
        ' cannot find form
        Debug.Print "Userform " & TargetUserform.Name & "not found"
        Exit Sub
    End If
    
    WinInfo = GetWindowLong(UFHWnd, GWL_STYLE)      ' get the style word (32-bit Long)
    WinInfo = WinInfo Or WS_SIZEBOX                 ' set the WS_SIZEBOX bit
    R = SetWindowLong(UFHWnd, GWL_STYLE, WinInfo)   ' set the style word to the modified value.
    
End Sub


Private Sub lblOptHighlight_Click()
' Choose to mark found expressions with Highlight, choose which colour to use, save to prefs file

Dim fso As New Scripting.FileSystemObject

Me.Hide
frmHighlightChoose.Show

Me.lblOptHighlight.BackColor = frmHighlightChoose.chosenColor
If Me.lblOptHighlight.BackColor = wdColorDarkBlue Or _
    Me.lblOptHighlight.BackColor = wdColorDarkRed Or _
    Me.lblOptHighlight.BackColor = wdColorDarkYellow Or _
    Me.lblOptHighlight.BackColor = wdColorBlack Or _
    Me.lblOptHighlight.BackColor = wdColorGray50 Or _
    Me.lblOptHighlight.BackColor = wdColorViolet Or _
    Me.lblOptHighlight.BackColor = wdColorBlue Or _
    Me.lblOptHighlight.BackColor = wdColorRed Then
        Me.lblOptHighlight.ForeColor = wdColorWhite
Else
    Me.lblOptHighlight.ForeColor = wdColorBlack
End If
Me.lblOptHighlight.SpecialEffect = fmSpecialEffectSunken

Me.lblOptNothing.SpecialEffect = fmSpecialEffectFlat
Me.lblOptNothing.BorderStyle = fmBorderStyleSingle
Me.lblOptNothing.BorderColor = wdColorBlack

Me.lblOptRedAnts.SpecialEffect = fmSpecialEffectFlat
Me.lblOptRedAnts.BorderStyle = fmBorderStyleSingle
Me.lblOptRedAnts.BorderColor = wdColorRed

MarkWithWhat = 1

Me.Show

Set fso = Nothing
End Sub

Private Sub lblOptNothing_Click()
' Choose Nothing as marking for found expressions

Dim fso As New Scripting.FileSystemObject

Me.lblOptNothing.SpecialEffect = fmSpecialEffectSunken

Me.lblOptHighlight.SpecialEffect = fmSpecialEffectFlat

Me.lblOptRedAnts.SpecialEffect = fmSpecialEffectFlat
Me.lblOptRedAnts.BorderStyle = fmBorderStyleSingle
Me.lblOptRedAnts.BorderColor = wdColorRed

MarkWithWhat = 2

End Sub

Private Sub lblOptRedAnts_Click()
' Choose to Mark found expressions with "Marching Red Ants Effect"

Dim fso As New Scripting.FileSystemObject

Me.lblOptRedAnts.SpecialEffect = fmSpecialEffectSunken

Me.lblOptHighlight.SpecialEffect = fmSpecialEffectFlat
Me.lblOptNothing.SpecialEffect = fmSpecialEffectFlat
Me.lblOptNothing.BorderStyle = fmBorderStyleSingle
Me.lblOptNothing.BorderColor = wdColorBlack

MarkWithWhat = 3

End Sub

Private Sub lblWarn_Click()

Call SpRep.Do_PreReplacements

End Sub

Private Sub lbxSearchCases_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
' Group select by special list entry, refusal to deselect compulsory entries

Dim nitems As Integer

nitems = Me.lbxSearchCases.listCount - 1

If Me.lbxSearchCases.ListIndex = 11 Then    ' Optional items heading, select to select entire group
    If Me.lbxSearchCases.Selected(11) Then
        For n = 12 To nitems
            Me.lbxSearchCases.Selected(n) = True
        Next n
    Else
        For n = 12 To nitems
            Me.lbxSearchCases.Selected(n) = False
        Next n
    End If
ElseIf Me.lbxSearchCases.ListIndex < 11 Then    ' Compulsory items, replacing them is not optional
    
    'If gscrolib.sUserName <> "margama" And gscrolib.sUserName <> "badkat" Then     ' Special exemption for creator ( :) )
    
        'Me.lbxSearchCases.Selected(Me.lbxSearchCases.ListIndex) = True
        'StatusBar = "First 10 items are compulsory, cannot desselect !"
        
    'Else
        If Me.lbxSearchCases.ListIndex = 0 Then
            If Me.lbxSearchCases.Selected(0) Then
                For n = 1 To 10
                    Me.lbxSearchCases.Selected(n) = True
                Next n
            Else
                For n = 1 To 10
                    Me.lbxSearchCases.Selected(n) = False
                Next n
            End If
        End If

    'End If
End If

End Sub
