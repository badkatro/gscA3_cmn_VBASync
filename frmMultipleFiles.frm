VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmMultipleFiles 
   OleObjectBlob   =   "frmMultipleFiles.frx":0000
   Caption         =   "Found multiple final files"
   ClientHeight    =   4920
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   6420
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   13
End
Attribute VB_Name = "frmMultipleFiles"
Attribute VB_Base = "0{09942E16-A26C-42C7-A545-83B67643BE82}{55A713BC-6954-4139-890D-C60B8FF1D4B2}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False


Public finalDocFolder As String
Public toSaveFinalName As String        ' when user opens one document or one file among others
Public toSaveMergeName As String        ' when user clicks merge and open


' Purpose: builds and returns an array of items in the listbox provided as parameter
Function getListbox_ListItems(InputListbox As MSForms.ListBox) As Variant

Dim tmpArray

If InputListbox.listCount > 0 Then
        
    ReDim tmpArray(0)
    For i = 0 To InputListbox.listCount - 1
    
        ReDim Preserve tmpArray(i)
        
        tmpArray(i) = finalDocFolder & "\" & InputListbox.List(i)
        
    Next i
    
End If

getListbox_ListItems = tmpArray

End Function


Private Sub cmdCancel_Click()
' Cancel button

Unload Me

End Sub



Private Sub cmdMergeOpen_Click()
' Merge all files and open them

Me.Hide


Dim partsNum As Integer

' call merge documents, marking merging points or not
If Me.cbMarkSutures Then
    partsNum = MergeDocuments(getListbox_ListItems(frmMultipleFiles.lstFinalFilesFound), "MarkPartsSuture")
Else
    partsNum = MergeDocuments(getListbox_ListItems(frmMultipleFiles.lstFinalFilesFound))
End If

StatusBar = "All documents merged (" & partsNum & ")"


Unload Me

End Sub


Private Sub lstFinalFilesFound_Click()
' clicking in main file list (on one file name) OPENS the selected file and saves it in user folder

Dim fso As New Scripting.FileSystemObject
Dim FileToOpen As String

Dim username As String
username = Environ$("username")

If lstFinalFilesFound.listCount > 0 Then
    
    If lstFinalFilesFound.ListIndex <> -1 Then
            
        If fso.FileExists(finalDocFolder & "\" & lstFinalFilesFound.List(lstFinalFilesFound.ListIndex)) Then
            FileToOpen = finalDocFolder & "\" & lstFinalFilesFound.List(lstFinalFilesFound.ListIndex)
            
            Me.Hide
            Documents.Open FileName:=FileToOpen, AddToRecentFiles:=False
            
            If Right(toSaveFinalName, 1) <> "\" Then   ' it WILL T:\Docs\oancevi\, so ending in "\" for user oancevi
                ActiveDocument.SaveAs2 FileName:=toSaveFinalName, FileFormat:=wdFormatXMLDocument, CompatibilityMode:=wdWord2010, AddToRecentFiles:=False
            Else
                ActiveDocument.SaveAs2 FileName:=toSaveFinalName & FileToOpen, FileFormat:=wdFormatXMLDocument, CompatibilityMode:=wdWord2010, AddToRecentFiles:=False
            End If
            
            Call Wrong_ToRightDiacritics_Replace
            PauseForSeconds 0.8     ' for user to read final message from diacritics program
            
            StatusBar = "Open Final: FOUND and opened " & lstFinalFilesFound.List(lstFinalFilesFound.ListIndex) & " from " & Main.astNetworkHomeBase & "\" & username
            
            Unload Me
        Else
            gscrolib.AhMsgBox_Show "File " & lstFinalFilesFound.List(lstFinalFilesFound.ListIndex) & " NOT found!", 5
        End If
            
    End If
    
End If

End Sub


Private Sub UserForm_Activate()

' Display number of file parts to user
If Me.lstFinalFilesFound.listCount > 0 Then
    Me.lblPartsNum.Caption = Replace(Me.lblPartsNum.Caption, "X", Me.lstFinalFilesFound.listCount)
End If

End Sub
