Attribute VB_Name = "Main"
Public DontAutoSaveROSettings As Boolean    ' Cancels execution of SetRoDefaultd, from frmgscoptions form
Public Const MailTemplateRoot As String = "Translation_Attribution_2_"
Public Const GSCDocNamePattern As String = "([ABCDEFGLMNPRSTU]{2})?\s*(\d{1,5}/(?:\d{4}|\d{2}))\s*((?:(?:ADD|AMD|COR|EXT|REV|AD|AM|CO|EX|RE)\s*\d{1,2}\s*){0,4})"
Public Const messageScreenAdresses As String = "coordination.linguistique|dga3.translation-support|dga-cis.sdp-workflow|dga3.terminology-and-documentation|alessandro.maestri|ian.morris|laura.romero-diaz"
Public Const messageScreenMailServer As String = "@consilium.europa.eu"

Public Const messagesFoldername As String = "Messages"


Public Const MAX_PATH As Long = 260
Public SourceAddress As Long
Declare Function GlobalAlloc& Lib "kernel32" (ByVal wFlags As Long, ByVal dwBytes As Long)
Declare Function GlobalUnlock& Lib "kernel32" (ByVal hMem As Long)
Declare Function GlobalFree& Lib "kernel32" (ByVal hMem As Long)

Declare Function FindFirstFile& Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA)
Declare Function FindNextFile& Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA)
Declare Function FindClose& Lib "kernel32" (ByVal hFindFile As Long)

Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (hpvDest As Any, hpvSource As Any, ByVal cbCopy As Long)

Const bslash As String = "\"

Public Const ProjectsBaseFolder As String = "T:\Prepared originals\"
Public Const backgroundFolder As String = "\Background"
Public Const exportsFolder As String = "\Studio\TMs - Export"

Public Const preProcessedWBaseFolder As String = "W:\TWB\Prepared originals\"
Public Const alignmentsProjectsBaseFolder As String = "T:\Alignment Projects\"

Public Const studioTargetFolder As String = "\Studio\%lg%-%LG%"     ' LANGUIFIED! (Using �Languify� function to decode this to actual language unit of user)
Const studioFolder As String = "\Studio"
Const studioReviewFolder As String = "\Studio\External Review"
Const prezTmxMask As String = "\Studio\TMs - Export\*.tmxCLSTMX"    ' BackupQ
Const prezTmxMaskRenamed As String = "\Studio\TMs - Export\*.tmx"   ' BackupQ

Const msgFolderMask As String = "\messag*|\mesaj*|"     ' Will be used as a mask for possible/ probable folder names where user will store messages about document
                                                        ' The last pipe char is not unintended, it's a
                                                        
'Const backupBaseFolder As String = "Q:\01-TRADUCERI\"                      ' REPLACED with preferred folder, retrieved from settings file!
'Const xliffFilesMask As String = "\Studio\ro-RO\*.sdlxliff"                ' REPLACED with mechanism retrieval, from settings file or form
'Const reviewFileMask As String = "\Studio\External Review\ro-RO\*.doc*"
'Const msgFilesMask As String = "\*.msg|\*.htm"                             'REPLACED

                                                        
Public Const astNetworkHomeBase As String = "T:\Docs"
Public Const astBackupHomBase As String = "D:\Docs"

Public sRange As Range

Type controlPosition
    Left As Integer
    Top As Integer
End Type

Public Type FILETIME
  dwLowDateTime As Long
  dwHighDateTime As Long
End Type

Public Type WIN32_FIND_DATA
  dwFileAttributes As Long
  ftCreationTime  As FILETIME
  ftLastAccessTime As FILETIME
  ftLastWriteTime As FILETIME
  nFileSizeHigh  As Long
  nFileSizeLow   As Long
  dwReserved0   As Long
  dwReserved1   As Long
  cFileName    As String * MAX_PATH
  cAlternate    As String * 14
End Type

Public DocuwriteMetadata_FieldNames As New Scripting.Dictionary

Public Enum DWFields
    DocumentType = 0
    DocumentNumber = 1
    DocumentYear = 2
    DocumentSuffixes = 3
    UniqueHeading = 4
    HeadingText = 5
    DocumentGroup = 6
    DocumentLanguages = 7
    OriginalLanguages = 8
    InstitutionalFramework = 9
    DocumentDate = 10
    DocumentLocation = 11
    LanguagesInvolved = 12
    FirstRevNumber = 13
    Distribution = 14
    SubjectCodes = 15
    TypeOfHeading = 16
    InterinstitutionalFiles = 17
    SousEmbargo = 18
    Originator = 19
    Recipient = 20
    DateOfReceipt = 21
    PrecedingDocuments = 22
    CommissionDocuments = 23
    Subject = 24
    DG = 25
    Initials = 26
    CoverPageDocWithCouncilFooter = 27
    SourceDocLanguage = 28
    SourceDocType = 29
    SourceDocTitle = 30
    SourceDocIsCECDoc = 31
End Enum

Public MyRibbon As IRibbonUI

Dim bqsbar As String


Sub GSCRoInitialize(Ribbon As IRibbonUI)
    
    Debug.Print "GSCRoInitialize HAS been RAN !"
    Set MyRibbon = Ribbon
    
    Call DWPopup_MainButtonAdd  ' check and ammend custom popup menu for DW Meta Extract!
    
    Call Disable_NormalTemplate_SavingMessage
    'Call Check_MissingLanguageSetting
    
End Sub

Sub Disable_NormalTemplate_SavingMessage()

Application.Options.SaveNormalPrompt = False

End Sub


Sub RetrieveSavedPreferences()

Dim gro_Sfile As New SettingsFileClass

Dim a3OptionSections, a3Options, a3AdvOptions

If gro_Sfile.Exists Then
    If gro_Sfile.IsEmpty = FileNotEmpty Then
        a3OptionSections = gro_Sfile.GetINISectionNames(gro_Sfile.Path, ",")
        Debug.Print ""
    End If
End If

If IsEmpty(current_GSCA3_Settings) Then
End If

End Sub


Sub RefreshRibbon()

If Not MyRibbon Is Nothing Then
    MyRibbon.Invalidate
End If

End Sub

'**********************************************************************************************************************

Sub Launch_RawTmxFinder()

frmRawTmxFinder.Show

End Sub


Public Sub Initialize_Docuwrite_MetaNames()

With DocuwriteMetadata_FieldNames

    If .count <> 32 Then
        .RemoveAll
    End If

    .Add 0, "md_DocumentType"
    .Add 1, "md_DocumentNumber"
    .Add 2, "md_YearDocumentNumber"
    .Add 3, "md_Suffixes"
    .Add 4, "md_UniqueHeading"
    .Add 5, "md_HeadingText"
    .Add 6, "md_DocumentGroup"
    .Add 7, "md_DocumentLanguages"
    .Add 8, "md_OriginalLanguages"
    .Add 9, "md_InstitutionalFramework"
    .Add 10, "md_DocumentDate"
    .Add 11, "md_DocumentLocation"
    .Add 12, "md_SuffixLanguagesInvolved"
    .Add 13, "md_FirstRevNumber"
    .Add 14, "md_Distribution"
    .Add 15, "md_SubjectCodes"
    .Add 16, "md_TypeOfHeading"
    .Add 17, "md_InterinstitutionalFiles"
    .Add 18, "md_SousEmbargo"
    .Add 19, "md_Originator"
    .Add 20, "md_Recipient"
    .Add 21, "md_DateOfReceipt"
    .Add 22, "md_PrecedingDocuments"
    .Add 23, "md_CommissionDocuments"
    .Add 24, "md_Subject"
    .Add 25, "md_DG"
    .Add 26, "md_Initials"
    .Add 27, "md_CoverPageDocWithCouncilFooter"
    .Add 28, "md_SourceDocLanguage"
    .Add 29, "md_SourceDocType"
    .Add 30, "md_SourceDocTitle"
    .Add 31, "md_SourceDocIsCECDoc"
    
End With

End Sub


Sub HasTmx_CurrentDoc(Optional documentID)
' version 0.4

' 0.4 - modifying to use not the com adddin intended to check existence of tmx or badtmx in asyncronous way (failed by the way)

If Documents.count = 0 Then

StillEmpty:

    Dim tempDoc As String
    Dim luckyDoc As String
    
    If IsMissing(documentID) Then
    
        If UBound(Split(latestOpenedDocs, ",")) <> -1 Then
            luckyDoc = Split(latestOpenedDocs, ",")(UBound(Split(latestOpenedDocs, ",")))
        Else
            luckyDoc = "ST 12345/13 REV 1 EN"
        End If
        
        tempDoc = InputBox("Please provide original language document standard name to check, up to second dot and original's language, if different from �EN�", _
            "Search for which tmx?", luckyDoc)
            
    Else
        tempDoc = documentID
    End If
    
    ' Correct for user not having supplied original's language, guess for english
    If Not IsValidLangIso(Right$(tempDoc, 2)) Then
        tempDoc = Trim(tempDoc) & " EN"
    End If
    
    
    Dim docFileName As String
    
    docFileName = GSC_StandardName_WithLang_toFileName(tempDoc)
    
    ' Check complyness and retort
    If Not Is_GSC_Doc(docFileName) Then
        StatusBar = "Supplied string NOT valid GSC document name and language string, Please retry!"
        Exit Sub
    Else
        If InStr(1, latestOpenedDocs, tempDoc) = 0 Then
            latestOpenedDocs = latestOpenedDocs & IIf(latestOpenedDocs <> "", ",", "") & tempDoc
        End If
    End If
        
    'Call tmxFinderCall("T:\TMs - Export\Revised translations pending import\CouncilMaster", docFileName & ".ro.Wholedoc.tmx", 36000, 3000)
    Call tmxFind(getGSCBaseName(docFileName), 30, 3)
    
Else
    
    ' We treat as empty cases when we have a non-saved document opened or a template
    If Right(ActiveDocument.Name, 3) = "dot" Or Right(ActiveDocument.Name, 4) = "dotm" Or _
            ActiveDocument.Name Like "Document#" Or ActiveDocument.Name Like "Document##" Then
        
        GoTo StillEmpty
    Else
        'Call tmxFinderCall("T:\TMs - Export\Revised translations pending import\CouncilMaster", getGSCBaseName(ActiveDocument.Name) & ".ro.Wholedoc.tmx", 36000, 3000)
        Call tmxFind(getGSCBaseName(ActiveDocument.Name), 30, 3)
    End If

End If

End Sub


Sub tmxFind(GSCFilename As String, timer As Integer, checkInterval As Integer)

' version 0.2 - Repaired (hope) flaw in loop construct, printing wrong messages (alternating msgs)

Dim passCeiling As Byte
passCeiling = timer / checkInterval

Dim passCounter As Byte
passCounter = 1

Dim tmxNOTFoundMsg As String
tmxNOTFoundMsg = "Tmx Finder: export file for " & GSCFilename & " NOT FOUND, Waiting"


nextPass:
    ' First check whether the export is already there... shortest trip in happy (and expected) case
    If exportTmx_existsOnT(GSCFilename) Then
        
        StatusBar = "TMX Finder: export file for " & GSCFilename & " FOUND!"
        
    Else    ' export tmx NOT FOUND !
        
        ' Having not found a good tmx, let's see if it's not in the badtmx status already!
        Dim isBadTmx_RetCode As badTmxSituation
        isBadTmx_RetCode = exportTmx_IsBADTMX(GSCFilename)
        
        Dim sbMessage As String
        
        Select Case isBadTmx_RetCode
            
            Case IsBadTmx                   ' YES           ' Signal user and stop, Case elucidated
                sbMessage = "TMX Finder: export file is a BADTMX! Please take corrective measures!"
                StatusBar = sbMessage
                
            Case IsBadTmx_NoLog             ' YES           ' Signal user and STOP, Some mistery remains, Why is log empty or missing ?
                sbMessage = "TMX Finder: export file is a BADTMX, but LOG file is missing or empty, Please investigate/ redo finalisation!"
                StatusBar = sbMessage
                
            Case NotBadTmx_DoesNotApply     ' DON'T KNOW    ' Signal user and STOP !
                sbMessage = "TMX Finder: ERROR, current document/ supplied document id NOT GSC format, Please correct and retry!"
                StatusBar = sbMessage

            Case NotBadTmx                  ' NO            ' GO ON to check if it's still in Quicksave or Quicksave/Temp, in which case we wait !
                'sbMessage = "TMX Finder: export file"
                
                ' Advise user if tmx export is still in QuickSave folder
                If exportTmx_StillInQuickSave(GSCFilename) Then
                    
                    ' load big descriptive string on first passage only!
                    If sbMessage = "" Then
                        sbMessage = "Tmx Finder: export file for " & GSCFilename & " is STILL in QuickSave folder, Will WAIT"
                    End If
                    
                    StatusBar = sbMessage
                    'StatusBar = "Tmx Finder: export file for " & GSCFilename & " is STILL in QuickSave folder, Will WAIT..."
                    Call PauseForSeconds(0.3)   ' so user may actually read message (if he/ she is actually watching the status bar ! :) )
                    
                    ' Check if we're still under the desired number of passes and then check again...
                    If passCounter < passCeiling Then
                        
                        If passCounter > 1 Then sbMessage = sbMessage & ".": StatusBar = sbMessage
                        ' Do nothing for specified interval
                        Call PauseForSeconds(CSng(checkInterval))
                        passCounter = passCounter + 1   ' and increase pass counter
                        
                        GoTo nextPass
                    
                    Else    ' Reached pass counter's max value (We waited enough)
                    
                        If Not exportTmx_IsBADTMX(GSCFilename) Then   ' Sometimes it takes more than 30 seconds for the tmx export to make it to the proper folder...
                            StatusBar = "Tmx Finder: export was NOT found in 30 seconds, Please check manually (or run me again)!"
                        Else    ' Now it's time to also check whether the tmx export IS a bad tmx !
                            StatusBar = "Tmx Finder: export file for " & GSCFilename & " is a BADTMX!"
                        End If
                        
                    End If
                    
                Else
                
                    StatusBar = "TMX Finder: export file NOT FOUND, Either as GOOD or BAD TMX! Please investigate! | FAILURE!"
                    
                End If
                
        End Select
                                    
    End If

End Sub


Function ArchiveG(Optional documentID) As Integer
' vers 0.9

' 0.65 - Adding poss to archive to G a xlf file (with no file opened in word, of course)
' 0.66 - Adding optional parameter to supply document to processs, for remote launching
' 0.68 - Fix adding "RO" language code to input string... not useful, in fact damaging
' 0.69 - BUG fixed - frmArchiveG form was using the history string to get at demanded document (when called from XLFAutoProcess form)
'        instead of somehow getting the user-typed value. This caused trouble by using the last doc in the hist string instead of one the user had scrolled back to !
' 0.70 - Made ArchiveG 2000 ready.. joking... made it 1 feb 2015 ready (passing to docx format)
' 0.75 - BUGS, BUGS, BUGS...
' 0.76 - Pretty big bug... archiving original document when the current (unit language) doc being archived is in "compare side by side" with another doc !
'            Probably fixed (to be checked) by breaking side by side just after identifiying the doc to process !
' 0.77 - Related to 0.76, sometimes, when we have RO and EN docs opened, the EN gest archived !
' 0.80 - Trying to implement full autopilot, not showing confirmation form and printing complete message on statusbar
' 0.90 - Refusing to archive XM and XT LIMITE docs !


Dim fso As New Scripting.FileSystemObject
Dim aPath As String


' Safeguard in case of no opened document
If Documents.count = 0 Then

StillEmpty:
    ' Are we launched with parameter or without
    If IsMissing(documentID) Then
        
        Dim doctogetS As String         ' Doc to get standard name
         
        ' Lets ask for user input, but present either an example or the last used document
        If UBound(Split(latestOpenedDocs, ",")) <> -1 Then
            Dim luckyDoc As String
            luckyDoc = Split(latestOpenedDocs, ",")(UBound(Split(latestOpenedDocs, ",")))
        Else
            luckyDoc = "ST 12345/13 REV 1"
        End If
        
        doctogetS = InputBox("Please provide original language document standard name to finalise, up to second dot", _
            "Archive which document", luckyDoc)
    Else ' we were called with file to archive parameter
        doctogetS = documentID
    End If
    
    
    Dim uLg As String
    uLg = Get_ULg
    
    
    ' Check user supplied string for empty
    If Not doctogetS = "" Then
        ' Correct for user not having supplied original's language, guess for romanian
        If Not IsValidLangIso(Right$(doctogetS, 2)) Then
            doctogetS = Trim(doctogetS) & UCase(uLg)
        Else
            Select Case Right(doctogetS, 2)
            
            Case "EN"
                doctogetS = Replace(doctogetS, "EN", UCase(uLg))
            Case "FR"
                doctogetS = Replace(doctogetS, "FR", UCase(uLg))
            Case Else
            
            End Select
        End If
        
        Dim ctdocument As String
        ctdocument = GSC_StandardName_WithLang_toFileName(doctogetS)
    Else
        
        ArchiveG = 1        ' ERROR CODE - Attempted to archive user-inputted document, which is not GSC compliant !
        Exit Function
        
    End If
        
Else    ' One or more documents opened case
    
    Dim ctDoc As Document
    Set ctDoc = ActiveDocument
    
  
    If Is_GSC_Doc(ctDoc.Name) Then
        
        ' IMPORTANT safeguard
        If (getDocType_fromDocName(ctDoc.Name) = "XM" Or getDocType_fromDocName(ctDoc.Name) = "XT") And _
            Trim(UCase(DW_Get_Confidentiality(ctDoc))) = "LIMITE" Then
            StatusBar = "ArchiveG: FORBIDDEN to archive such document, Exiting..."
            End
        End If
            
        If Is_ULg_Doc(ctDoc.Name) Then
            
            ' Don't just check the root of the file name (as Is_GSC_Doc does), check rest of file name as well !
            If ctDoc.Name = getGSCBaseName(ctDoc.Name) & ".doc" Or _
                ctDoc.Name = getGSCBaseName(ctDoc.Name) & ".docx" Then
                
                'ctdocument = getGSCBaseName(ActiveDocument.Name)       ' Probably unwise, let's demand proper archiving name...
                ctdocument = ctDoc.Name
                
                ' Break Side by side view, if activated !
                If Documents.count > 1 Then     ' Will give an error otherwise !
                    ActiveDocument.Windows.BreakSideBySide
                End If
                Application.ScreenRefresh
                PauseForSeconds (0.1)
                
            Else
                MsgBox "Please name the current document (" & ctDoc.Name & ") to the proper archiving name and retry", vbOKOnly, "Improper archiving name!"
                Exit Function
            End If
            
            
        Else
            MsgBox "Sorry, current document is NOT a " & uLg & " document(or is incorrectly named), cannot archive it! " & vbCr & vbCr & _
                "Please check/ correct file name and retry!", vbOKOnly + vbCritical, "Document NOT in unit language"
            Exit Function
        End If
    Else
        ' We treat as empty cases when we have a non-saved document opened or a template
        If Right(ctDoc.Name, 3) = "dot" Or Right(ctDoc.Name, 4) = "dotm" Or _
            ctDoc.Name Like "Document#" Or ctDoc.Name Like "Document##" Then
            
            GoTo StillEmpty
        
        Else
        
            MsgBox "Sorry, current document (" & ctdocName & ") is NOT a Council document (or incorrectly named)" & vbCr & vbCr & _
                "Please check/ correct file name and retry!", vbOKOnly + vbCritical, "Not Council document!"
            Exit Function
        End If
        
    End If
End If

'**************************************************************************************************************************************************

If Not Is_GSC_Doc(ctdocument) Then
    StatusBar = "Supplied string NOT valid GSC document name and language string, Please retry!"
    Exit Function
Else
    If doctogetS <> "" Then
        If InStr(1, latestOpenedDocs, doctogetS) = 0 And _
            (InStr(1, latestOpenedDocs, Replace(doctogetS, uLg, "EN")) = 0 Or InStr(1, latestOpenedDocs, Replace(doctogetS, uLg, "FR")) = 0) Then
            
            Dim ctPF As String
            ctPF = getProjectFolderOf(ctdocument)
            
            If InStr(InStrRev(ctPF, "\"), ctPF, ".en") > 0 Then
                latestOpenedDocs = latestOpenedDocs & IIf(latestOpenedDocs <> "", ",", "") & IIf(InStr(1, doctogetS, uLg) = 0, doctogetS, Replace(doctogetS, uLg, "EN"))
            ElseIf InStr(InStrRev(ctPF, "\"), ctPF, ".fr") > 0 Then
                latestOpenedDocs = latestOpenedDocs & IIf(latestOpenedDocs <> "", ",", "") & IIf(InStr(1, doctogetS, uLg) = 0, doctogetS, Replace(doctogetS, uLg, "FR"))
            Else
            End If
            
        End If
    End If
End If

'**************************************************************************************************************************************************

aPath = buildGPath_fromFilename(ctdocument)     ' archiving path

If aPath <> "" Then
    
    ' Check if current document (if any) needs be saved and do so after asking user
    If Documents.count > 0 Then
        
        If Found_Opened_Document(ctdocument) Then
    
            If Not Documents(ctdocument).Saved Then
                ' If MsgBox("Would you like to save it now?" & vbCr & vbCr & "Click Yes to save it now, No to cancel archiving", vbYesNo, "Warning, document not saved!") = vbYes Then
                    ' Documents(ctdocument).Save
                ' Else
                    ' Exit Function
                ' End If
                
                ' Initiate usage of archiving (informational) userform
                frmArchiveG.NotSaved = True
                Load frmArchiveG
                

            End If
        
        Else    ' Soooo.... we're demanded to archive a doc which is not an opened doc!
                ' Meaning... it's a web doc probably!
            
            
            
        End If
        
    End If
    
    ' Initiate usage of archiving (informational) userform
    'Load frmArchiveG
    
    ' set the proper variables of it
    frmArchiveG.archivePath = aPath
    frmArchiveG.aDocument = ctdocument
    frmArchiveG.Caption = Replace(frmArchiveG.Caption, "current document", ctdocument)
    
    If frmArchiveG.cbAutoPilot.Value = False Then
        frmArchiveG.Show  ' ONLY show form if demanded (if autopilot is OFF)
    Else
        Call frmArchiveG.UserForm_Activate  ' EXPERIMENT... does it work to be called instead of autorunning when userform activating ? Oh, worry, oh worry !
        Call frmArchiveG.cmdOK_Click
    End If
    
Else
    MsgBox "Sorry, could not compute target directory for document " & ctdocument & vbCr & vbCr & _
        "Please contact margama(2942)", vbOKOnly + vbCritical, "Target folder path building error"
    Exit Function
End If

'******************************************************************************************************************************************************************************

Set fso = Nothing

End Function





Sub All_FootnoteRefs_HspBef_AndBold(InsertHardSpaceBefore As Boolean)
' version 0.7
' main routine
'
' Schimba toate referintele notelor de subsol (footnotes) la bold.
' 0.4 - Imbunatatita, sa stearga spatiul/iile sau hard spatiile de dinaintea acestora,
' lucru care oricum trebuie facut manual. Cheama iar Spellchecking done.
' 0.6 - Imbunatatita, intreaba despre spatiul de dinaintea referintelor, sa fie hard sau deloc?
' De asemenea, spatiu inserat dupa referinte in caz ca nu exista semn de punctuatie dupa.



Dim fn As Footnote
Dim dsr As StoryRanges
Set dsr = ActiveDocument.StoryRanges
Dim dcr As Range, cref As Range, crefC As Range
Dim hsp As Boolean

'hsp = MsgBox("Do you wish a hard (�non-breaking�) space before each footnote reference?", vbYesNo + vbQuestion, "Please choose spacing preference")
hsp = InsertHardSpaceBefore

'If hsp = 0 Then hsp = 7 ' 7 means VbNo, we choose for user since he dismissed the box without choosing... its a No-No! :D
   
   
    If Documents.count > 0 Then
        Set dcr = dsr.Item(wdMainTextStory)
            If dcr.Footnotes.count > 0 Then
                For Each fn In dcr.Footnotes
                    
                    On Error Resume Next
                    
                    fn.Reference.Style = "Footnote Reference"
                    
                    ' sometimes we try working on "deleted" footnotes references... just move on!
                    If Err.Number <> 0 Then
                        If Err.Number = 5825 Then   ' object has been deleted error
                            Err.Clear
                            GoTo skipfn
                        End If
                    End If
                    
                    fn.Reference.Bold = True
                    
                    Set cref = fn.Reference
                    Set crefC = cref.Duplicate
                    
                    ' Lets try to avoid footnote references with something other than printing characters (alphanumeric) as reference, first character only
                    If (AscW(cref.Characters(1).Text)) <> 13 Then
                    
                        ' Firstly, search/ remove/ reinsert spaces and hardspace from before reference.
                        'cref.Select
                        nspc = crefC.MoveStartWhile(" " & Chr(160), wdBackward)
                        If nspc < 0 Then
                            crefC.Collapse wdCollapseStart
                            'cref.Select
                            crefC.Delete count:=Abs(nspc)
                            If hsp Then crefC.InsertAfter (Chr(160))
                        Else
                            ' If our footnote reference is the very first character in the doc, no need to add hard space before it, ha ?
                            If Not (fn.Reference.Information(wdFirstCharacterColumnNumber) = 1 And _
                                fn.Reference.Information(wdFirstCharacterLineNumber) = 1) Then
                                    If hsp Then crefC.InsertBefore (Chr(160))
                            End If
                        End If
                        
                        'Secondly, weirdly, I know, there are footnote refs without space after'em, even if a WORD is following!
                        'Correct that!
                        Set crefC = cref.Duplicate
                        nscp = crefC.MoveEndWhile(",.:;!?", wdForward)
                        
                        If nscp > 0 Then    ' if we found punctuation mark,
                        ElseIf nscp = 0 Then
                            crefC.InsertAfter " "
                        Else    ' negative ? probably not possible, ha ?
                        End If
                    
                    End If
skipfn:
                Next fn
            End If
                   
        StatusBar = "All Footnote references set to bold!"
    End If

Call Spellchecking_Done

Set dsr = Nothing
Set dcr = Nothing
Set cref = Nothing
End Sub

Sub Spellchecking_Done()
' version 0.5
' main routine
'
' Mica rutina ce permite sa schimbam status-ul spell-checkingului unui document
' la "spellchecking done", atunci cind suntem siguri ca documentul nu are nevoie
' de un spellcheck "real"

Dim crng As Range

If Documents.count > 0 Then
    Application.ResetIgnoreAll
    
    For Each crng In ActiveDocument.StoryRanges
        crng.SpellingChecked = True
        crng.GrammarChecked = True
    Next crng
    
    StatusBar = "Spellchecking passed over!"
    
End If

End Sub

Sub Paranthesize_Paragraphs_Numbers(Optional NoPagebreakConversion)
' version 0.37
' main routine
'
' to be used to launch parantheses adding macro to selected/all paragraphs after selection


If Documents.count > 0 Then
    Set sRange = Nothing
    If gscrolib.Is_GSC_Doc(ActiveDocument.Name) Then  'And gscrolib.Is_ULg_Doc(ActiveDocument.Name) = False _
        'And Mid$(ActiveDocument.Name, InStr(1, ActiveDocument.Name, ".") + 5, 4) = ".doc" Then
            
            If Not IsMissing(NoPagebreakConversion) Then
                
                Call Paranteze_Alineate
                StatusBar = "Numbering modified to parens format on selection!"
                
            Else
                
                Call gscrolib.PBr_ToPRbBefore_Convert
                ActiveDocument.ConvertNumbersToText
                Call gscrolib.PBrBef_ToPBr_Convert
                Call Paranteze_Alineate
                StatusBar = "Numbering modified to parens format on selection!"
                
            End If

    Else
        MsgBox "Please run macro on a correctly called GSC document! Exiting.."
        Exit Sub
    End If
Else
    StatusBar = "No document opened, Exiting..."
    Exit Sub
End If

End Sub

Sub Paranteze_Alineate()
' version 0.61
' main routine
' depends on CautareP
' Schimba formatul numerelor alineatelor (subdiviziuni ale articolelor din actele legislative)
' din formatul englezesc in cel romanesc. Exemplu: din "1.[tab]" in "(1)[tab]". Functioneaza pe
' selectia curenta sau pe tot documentul curent, din locul unde se afla cursorul pana la capatul
' documentului. Sfatuim a se rula cu selectie (din cauza eventualelor anexe, unde pot exista liste
' numerotate).

' Insuficient testat. Pare sa ruleze fara probleme insa. Nu cauta sa inlocuiasca "a." (sau "a)" - nu mai stiu
' cum folosesc englezii) - in versiunea folosita de Romania - "(a)"
' 0.6 Adaugat si Cautare_Tab_Spatii
' 0.61 Adaugat la sfarsit Spellchecking_Done, pt a ignora cuvintele gresit ortografiate

Dim raspuns As Byte

If Selection.Type = wdSelectionNormal Then
    If Selection.Sections.count > 1 Then
        raspuns = MsgBox("To be searched text (selected text) spans over " & Selection.Sections.count & " selections." & vbCr _
        & "Are you SURE?", vbOKCancel + vbExclamation, "Don't worry, there's no sugar!")
            If raspuns = vbCancel Then Exit Sub
    End If
ElseIf Selection.Type = wdSelectionIP Then
    ' Set srange from cursor position to the end of document
    If MsgBox("No selection found, the program will change numbering format from cursor position till the end of the document" & vbCr & vbCr & _
        "Are you sure you wish to continue?", vbOKCancel + vbQuestion, "Change numbering till end of doc?") = vbOK Then
        Set sRange = ActiveDocument.Range(Selection.Range.Start, ActiveDocument.StoryRanges(wdMainTextStory).End)
    Else
        StatusBar = "Exiting..."
        Exit Sub
    End If
Else
    MsgBox "Please place cursor before the paragraph you wish modified, or select the desired target range and re-run!"
    Exit Sub
End If

Set sRange = Selection.Range

sRange.Find.ClearFormatting
sRange.Find.Replacement.ClearFormatting

Dim ls As String
ls = GetListSeparatorFromRegistry

Call Cautare_Tab_Spatii
Call CautareP2("([0-9]{1" & ls & "}[a-z]{1" & ls & "})(.)(^t)", "(\1)\3")
Call CautareP2("([0-9]{1" & ls & "})(.)(^t)", "(\1)\3")

'Call CautareP("^#^#^#^$^$^$.^t")
'Call CautareP("^#^#^#^$^$.^t")
'Call CautareP("^#^#^#^$.^t")
'Call CautareP("^#^#^#.^t")
'Call CautareP("^#^#^$^$^$.^t")
'Call CautareP("^#^#^$^$.^t")
'Call CautareP("^#^#^$.^t")
'Call CautareP("^#^#.^t")
'Call CautareP("^#^$^$^$.^t")
'Call CautareP("^#^$^$.^t")
'Call CautareP("^#^$.^t")
'Call CautareP("^#.^t")

Set sRange = Nothing

Call Spellchecking_Done

End Sub

Sub CautareP2(TextC As String, TextR As String)

' version 0.1
' secondary to Paranteze_Alineate
'
' Folosita de Paranteze_Alineate. Reprezinta cautarea in sine, care trebuie repetata de mai multe ori, cu diferite argumente.
' Nu reprezinta o rutina utilizabila in sine.

Debug.Print "CautareP2 has been called" & vbCr

With sRange.Find
    .Text = TextC
    .Replacement.Text = TextR
    .Forward = True
    .Wrap = wdFindStop
    .Format = False
    .MatchCase = False
    .MatchWholeWord = False
    .MatchWildcards = True
    .MatchSoundsLike = False
    .MatchAllWordForms = False
End With
sRange.Find.Execute Replace:=wdReplaceAll


End Sub

Sub CautareP(TextC As String)
' version 0.5
' secondary to Paranteze_Alineate
'
' Folosita de Paranteze_Alineate. Reprezinta cautarea in sine, care trebuie repetata de mai multe ori, cu diferite argumente.
' Nu reprezinta o rutina utilizabila in sine.

With sRange.Find
        .Text = TextC
        .Replacement.Text = "(^&)"
        .Forward = True
        .Wrap = wdFindStop
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    sRange.Find.Execute Replace:=wdReplaceAll
    With sRange.Find
        .Text = ".^t)"
        .Replacement.Text = ")^t"
        .Forward = True
        .Wrap = wdFindStop
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    sRange.Find.Execute Replace:=wdReplaceAll

End Sub
Sub Cautare_Tab_Spatii()
' version 0.1
' secondary to Paranteze_Alineate
'
' Folosita de Paranteze_Alineate. Cauta spatiu-tab si tab-spatiu si inlocuieste cu tab, in ambele cazuri.

With sRange.Find
        .Text = " ^t"
        .Replacement.Text = "^t"
        .Forward = True
        .Wrap = wdFindStop
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    sRange.Find.Execute Replace:=wdReplaceAll
With sRange.Find
        .Text = "^t "
        .Replacement.Text = "^t"
        .Forward = True
        .Wrap = wdFindStop
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    sRange.Find.Execute Replace:=wdReplaceAll

End Sub


Sub AutoNumbers_ToManualNumbers_Convert()

Dim userChoice As Integer
Dim rngTarget As Range
Dim osel As Range       ' save original selection

If Documents.count > 0 Then
    
    If Selection.Type <> wdSelectionNormal Then
        
        If Selection.Type = wdSelectionIP Then
        
            userChoice = MsgBox("NO selection found, the program will convert lists to manual numbering for whole document!" & vbCr & vbCr & _
                "Please press Yes to continue, No to cancel", vbYesNo + vbInformation, "Warning, targetting whole document!")
            
            If userChoice = vbNo Then
                StatusBar = "Convert Numbering: Canceled, Exiting..."
                Exit Sub
            End If
            
            Call gscrolib.PBr_ToPRbBefore_Convert
            ActiveDocument.ConvertNumbersToText
            Call gscrolib.PBrBef_ToPBr_Convert

            StatusBar = "Convert Numbering: Automatic numbering converted to manual in document!"
    
        Else
            
            MsgBox "Please select some text containing lists to be converted to manual numbering and retry!", vbOKOnly, "Selection missing"
            Exit Sub
            
        End If
    
    Else    ' Selection normal
    
        Set rngTarget = Selection.Range
        Set osel = Selection.Range
        Selection.Collapse
                
        Call gscrolib.PBr_ToPRbBefore_Convert
        rngTarget.ListFormat.ConvertNumbersToText
        Call gscrolib.PBrBef_ToPBr_Convert
        
        osel.Select
        
        StatusBar = "Convert Numbering: Automatic numbering converted to manual in selection!"
    
    End If
    
Else
    StatusBar = "Convert Numbering: NO opened document, Exiting..."
    Exit Sub
End If

Set osel = Nothing: Set rngTarget = Nothing
End Sub


' Purpose: get saved user prefered folder from saved settings file, providing default if not found ("D:\Docs")
Function getUsersSavedFolderPref(ActualProjectFolder As String) As String


Dim sFile As New SettingsFileClass


If sFile.Exists Then

    If sFile.IsEmpty = FileNotEmpty Then
        
        Dim shouldTDocs As String, shouldDDocs As String, shouldProjectFolder As String
        Dim chosenOption As String
        
        shouldTDocs = sFile.getOption("OpenFinalCopyDocToTDocsUserFolder", "GSC_A3_Options")
        
        If shouldTDocs = "False" Then
            
            shouldDDocs = sFile.getOption("OpenFinalCopyDocToDDocsFolder", "GSC_A3_Options")
            
            If shouldDDocs = "False" Then
                
                shouldProjectFolder = sFile.getOption("OpenFinalCopyDocToProjFdrFolder", "GSC_A3_Options")
                
                If shouldProjectFolder = "False" Then
                    chosenOption = "D:\Docs"    ' DEFAULT setting, kind of enforced
                Else    '
                    chosenOption = ActualProjectFolder
                End If
                
            Else    ' get DDocs option came back as true from saved settings file
                chosenOption = "D:\Docs"
            End If
            
        Else    ' get TDocs ast base folder came as true from settings file
            chosenOption = "T:\Docs"
        End If
        
    Else    ' Settings file empty or missing
        chosenOption = "D:\Docs"    ' DEFAULT setting, kind of enforced
    End If
    
Else    ' settings file non existent
    chosenOption = "D:\Docs"    ' DEFAULT setting, kind of enforced
End If


getUsersSavedFolderPref = chosenOption

Set sFile = Nothing

End Function


' Purpose: get prefered assistent user folder, using another function to retrieved eventual preference from file, providing default if not found ("D:\Docs")
Function getUsersFavouriteBaseFolder(ActualProjectFolder As String) As String


Dim fso As New Scripting.FileSystemObject

Dim username As String

Dim prefAstBaseFolder As String


Dim savedPrefFolder As String
savedPrefFolder = getUsersSavedFolderPref(ActualProjectFolder)


If savedPrefFolder = "T:\Docs" Then
    username = Environ$("username")
    If fso.FolderExists(savedPrefFolder & "\" & username) Then
        prefAstBaseFolder = savedPrefFolder & "\" & username
    Else    ' saved pref for TDocs, folder for current user does not exist!
        gscrolib.AhMsgBox_Show "Preffered AST docs base folder does not exist(" & savedPrefFolder & "\" & username & "), switching to default!", 3
        prefAstBaseFolder = "D:\Docs"
    End If
Else
    prefAstBaseFolder = savedPrefFolder
End If


getUsersFavouriteBaseFolder = prefAstBaseFolder

Set fso = Nothing


End Function


Sub openFinal()
' vers 0.92
' Routine finds project folder on T:\Prepared originals for current GSC document and, if found,
' attempts to find and copy to user AST home folder (T:\Docs\<WindowsUserName>) and then open the
' "final" document (version to be opened and worked on by AST)
'
' 0.78 - Add option to open finalised "bu" type doc, these are rtf files in fact
' 0.8  - Making Open Final not so bent on using T:\Docs\Username, will be obsoleted soon (will choose D:\Docs instead)
' 0.81 - Do not run DiacriticsReplace for language units other than intended! (RO)
' 0.9  - Finally, integrate merging of document parts!
' 0.91 - Re-add HR demanded feature: ignore files whose names correspond with mask in options advanced
' 0.92 - 0.91

Dim ProjectFolder As String
Dim TargetFolder As Scripting.Folder
Dim fso As New Scripting.FileSystemObject
Dim username As String


'*****************************************************************************************************************
'*****************************************************************************************************************
' NEED REPLACE THIS WITH STANDARD CODE FROM OTHER SUBS, REQUESTING USER INPUT STRING  ****************************
'*****************************************************************************************************************
'*****************************************************************************************************************
' Is there a project opened or is user kidding us?
If Documents.count = 0 Then
    StatusBar = "Open Final: No document opened, Exiting..."
    Exit Sub
End If

' And if so, is it Council doc?
If Not Is_GSC_Doc(ActiveDocument.Name) Then
    StatusBar = "Open Final: Current document NOT COUNCIL, Exiting..."
    Exit Sub
End If

ProjectFolder = getProjectFolderOf(ActiveDocument.Name)

' T:\Docs\username or D:\Docs? (TUser to be obsoleted soon...)
Dim prefAstBaseFolder As String

' using the retrieved project folder as input param for routine: if user saved "Project Folder root" as storage option, same folder will be returned!
prefAstBaseFolder = getUsersFavouriteBaseFolder(ProjectFolder)


If Not ProjectFolder = "" Then
    If fso.FolderExists(ProjectFolder & studioFolder) Then      ' current project is a Studio Project
        Set TargetFolder = fso.GetFolder(ProjectFolder & Languify(studioTargetFolder))
    Else    ' NOT Studio project
        Set TargetFolder = fso.GetFolder(ProjectFolder)
    End If
Else
    ' projectfolder returned empty for some reason
    StatusBar = "Open Final: Project folder NOT FOUND for " & ActiveDocument.Name & ", Exiting..."
    Exit Sub
End If

Dim toSaveFileName As String
Dim FileToOpen As Scripting.File

' Should it not be changed to target language name?
toSaveFileName = getGSCBaseName(ActiveDocument.Name) & ".finalised.docx"


Dim finalDocuments As foundFilesStruct
' modification to countFinalDocuments, we're making the mask expression we're feeding boolean as well - vertical bar "|" means or.
' (It's going to find and return, in the same structure, file conforming to one OR the other of the supplied expressions)

' Modification to include file exclusion pattern in file search
Dim setFile As New SettingsFileClass

Dim savedExclusionPattern As String
savedExclusionPattern = setFile.getOption("OpenFinalIgnoreMask", "GSC_A3_OptionsAdv")

If savedExclusionPattern <> "" Then
    finalDocuments = bGDirAll(TargetFolder.Path & "\*.docx<>" & savedExclusionPattern & "|" & _
                                TargetFolder.Path & "\*.doc<>" & savedExclusionPattern)
Else
    'finalDocuments = countFinalDocuments(TargetFolder.Path, "*.doc")
    finalDocuments = bGDirAll(TargetFolder.Path & "\*.docx|" & TargetFolder.Path & "\*.doc")
End If


' the above statement will bring ".doc" and ".docx" documents as well, because of the 8.3 automatic "backup" the Windows creates for all long names automatically.
' and which translates into the Dir command behaving thus: "Dir *.doc" returning "test.doc" and "test.docx" as well !!!
If finalDocuments.count = 0 Then    '
    
    ' Befor giving up (no files found), try see if BU document (rtf file)
    If savedExclusionPattern <> "" Then
        finalDocuments = bGDirAll(TargetFolder.Path & "\" & "*.rtf<>" & savedExclusionPattern)
    Else
        finalDocuments = bGDirAll(TargetFolder.Path & "\" & "*.rtf")
    End If
    
    If finalDocuments.count = 1 Then
        GoTo jumpForRTF
    ElseIf finalDocuments.count > 1 Then
        Call AhMsgBox_Show("Multiple �rtf� files found in project folder of " & TargetFolder.Name & ", please... check !")
        Exit Sub
    Else
    
        ' Warn user and offer to start up the "Finalise with StWiz" command
        ' and then perhaps behave just as the below section of the if, meaning if there is just one doc file in folder
    
        MsgBox "No �doc(x)� or �rtf� files found in Studio target folder, could you have forgotten to perform �Finalise doc� from Studio Wizard?" & vbCr & vbCr, vbOKOnly, _
            "NO doc(x) found!"
        Exit Sub
        ' HERE, we could launch Finalise with StdWiz for the user. However, he/ she should probably relaunch the "Open final"... �Finalise...� could take 2 minutes...
        ' the variable time it takes kinda makes it impossible to code around it...
        
    End If
    
    
ElseIf finalDocuments.count = 1 Then
    
    ' HERE, it could check whether the found doc file has the proper name... but what to compare against?
    ' Should we check against the document(s) present in the source subfolder ? (Studio/en-GB) or should we expect the
    ' correct file name composed of basename & .doc or docx extension ? - the latter subtracting from user's freedom of expression, of course.

jumpForRTF:
    
    If Not finalDocuments.files(0) = getProperOriginalFileName Then AhMsgBox_Show TextToShow:="Found one doc file only, but its filename is not right, so please make sure you did �Finalise doc� and that it is the right file"    ' _
        'NewWidth:=250, NewHeight:=70
            
    Set FileToOpen = fso.GetFile(TargetFolder & "\" & finalDocuments.files(0))
        
    Documents.Open FileName:=FileToOpen.Path, AddToRecentFiles:=False
        
        
    If username = "oancevi" Then
        ' Oancea work around. Forever miss you !
        toSaveFileName = FileToOpen.Name
    End If
                
    ActiveDocument.SaveAs2 FileName:=prefAstBaseFolder & "\" & toSaveFileName, FileFormat:=wdFormatXMLDocument, CompatibilityMode:=wdWord2010, AddToRecentFiles:=True
    
    ' ro unit replace diacritics. its a char code routine, useless for others
    If Get_AutoUlg = "ro" Then
        Call Wrong_ToRightDiacritics_Replace
        PauseForSeconds (0.8)   ' so user can read the final message displayed by diacritics program
    End If
    
    StatusBar = "Open Final: FOUND and opened  " & FileToOpen.Name & "  from " & prefAstBaseFolder


ElseIf finalDocuments.count > 1 Then
    
    
    ' add all final documents to a listbox and present it to user to choose which to open, if any
    Load frmMultipleFiles
    
    frmMultipleFiles.finalDocFolder = TargetFolder.Path
    
    ' NEVER TO REMOVE THIS, maybe Oancea will be back... :(
    If username <> "oancevi" Then
        frmMultipleFiles.toSaveFinalName = prefAstBaseFolder & "\" & toSaveFileName
        frmMultipleFiles.toSaveMergeName = prefAstBaseFolder & "\" & mergeName(toSaveFileName)
    Else
        frmMultipleFiles.toSaveFinalName = prefAstBaseFolder & "\"
    End If
    
    For i = 1 To finalDocuments.count
        frmMultipleFiles.lstFinalFilesFound.AddItem (finalDocuments.files(i - 1))
    Next i
    
    frmMultipleFiles.Show
    
Else    ' NO final documents found in folder!
    
    'gscrolib.AhMsgBox_Show "Could NOT find any *final* file in folder " & targetFolder.Path, 6
    
    ' Offer to open project folder, you've got it already captured in targetFolder
    Dim choiceOpenFolder As Integer
    choiceOpenFolder = MsgBox("Could NOT find any *final* file in folder " & TargetFolder.Path & vbCr & vbCr & _
        "Would you like to open project folder?", vbYesNo + vbInformation, "NO final files")
        
    If choiceOpenFolder = vbYes Then
        Call Shell("explorer.exe " & TargetFolder.Path, vbNormalFocus)
    Else
        StatusBar = "Open Final: Cancelling..."
        Exit Sub
    End If
End If


End Sub

Function Languify(InputString) As String

Dim uLg As String

uLg = Get_ULg

Dim tmpResult As String

tmpResult = Replace(InputString, "%lg%", uLg)
tmpResult = Replace(tmpResult, "%LG%", UCase(uLg))

Languify = tmpResult

End Function


Function getProperOriginalFileName() As String
' taking current original GSC document as starting point, it goes to the project folder and
' counting first the sdlxliff files, it tries to get the proper name of the original the user used the "Add doc"
' command on.

If Documents.count > 0 Then
    If Is_GSC_Doc(ActiveDocument.Name) Then
        
        If Not Is_ULg_Doc(ActiveDocument.Name) Then
            
            Dim ctProjectFolder As String
            ctProjectFolder = getProjectFolderOf(ActiveDocument.Name)
            
            Dim ctGSCBasename As String
            ctGSCBasename = getGSCBaseName(ActiveDocument.Name)
            
            If ctProjectFolder <> "" Then
                
                If Dir(ctProjectFolder & Languify(studioTargetFolder), vbDirectory) <> "" Then    ' if Studio target folder exists for current project
                    
                    Dim sdlxliffFilesStruct As foundFilesStruct
                    sdlxliffFilesStruct = countFinalDocuments(ctProjectFolder & Languify(studioTargetFolder), ctGSCBasename & "*doc*sdlxliff")
                    
                    If sdlxliffFilesStruct.count > 0 Then
                        
                        
                        If sdlxliffFilesStruct.count = 1 Then
                            
                            Dim fso As New Scripting.FileSystemObject
                            
                            Dim properName As String
                            properName = fso.GetBaseName(sdlxliffFilesStruct.files(0))
                            
                            getProperOriginalFileName = properName
                            Exit Function
                        
                        Else    ' would this be the situation of split files, I wonder ? I think so...
                                ' cause... an sdlxliff starting with the gsc basename of the project and continuing with doc (after one or more chars)
                        
                            
                        
                        End If
                        
                        
                    Else    ' No sdlxliff files found in Studio target folder...
                        getProperOriginalFileName = ""
                        Exit Function
                    End If
                    
                    
                    
                Else    ' IT COULD also NOT exist, there are still docs translated WITHOUT STUDIO... TO FILL IN !
                End If
                
            Else
                getProperOriginalFileName = ""
                Exit Function
            End If
            
        End If
        
    Else
        'StatusBar = "Get proper OR name: Current document is NOT a GSC doc, Exiting..."
        getProperOriginalFileName = ""      ' NO GO for non GSC docs
    End If
End If


End Function


Function countFinalDocuments(TargetFolder As String, fileMask As String) As foundFilesStruct
' version 0.55
' given a folder path and a file mask (as per dir statement specs, using wildcards), returns number of
' files matching pattern and their names in a special structure we called "foundFilesStruct"

Dim finalFiles As foundFilesStruct
Dim tmpFileFound As String

Dim fso As New Scripting.FileSystemObject
Dim fd As Scripting.Folder

' check if user supplied correct existing folder path
If fso.FolderExists(TargetFolder) Then
    Set fd = fso.GetFolder(TargetFolder)
Else
    Debug.Print "countFinalDocuments: received non existing folder name, namely " & TargetFolder
    countFinalDocuments.count = 0
    Exit Function
End If

Dim fileMaskPart As Variant
' If user supplies a file mask containing boolean or, we use first part to
'If InStr(1, fileMask, "|") > 0 Then
    fileMaskPart = Split(fileMask, "|")
'Else
    'fileMaskPart = fileMask
'End If


For i = 0 To UBound(fileMaskPart)

    ' look for first file according to supplied mask into target folder
    tmpFileFound = Dir(fd.Path & "\" & fileMaskPart(i))
    
    ' if using 3 and 4 chars extensions (like .doc and .docx), we need verify/ filter
    If Not tmpFileFound Like fileMaskPart(i) Then
        tmpFileFound = ""
    End If
    
    If tmpFileFound = "" Then
        ' some users might use capitals for "final" in file name. Their right...
        tmpFileFound = Dir(fd.Path & "\" & UCase$(fileMask))
        
        If tmpFileFound = "" Then
            countFinalDocuments.count = 0
            'Exit For
            GoTo skipThisOne
        Else    ' have tried capitals, that was it... proceed as usual
            GoTo allGood
        End If
    
    Else
    
allGood:
    
        finalFiles.count = finalFiles.count + 1
        ReDim Preserve finalFiles.files(finalFiles.count - 1)
        finalFiles.files(finalFiles.count - 1) = tmpFileFound
    End If
    
    ' now try to see if there are any more files conforming to specs in the folder
    ' and capture their names in specialised structure
    Do While (tmpFileFound) <> ""
        
        tmpFileFound = Dir
        
        If tmpFileFound <> "" Then
            finalFiles.count = finalFiles.count + 1
            ReDim Preserve finalFiles.files(finalFiles.count - 1)
            finalFiles.files(finalFiles.count - 1) = tmpFileFound
        End If
    Loop

skipThisOne:
Next i

' in case of no returned files, we'll get an error otherwise
If finalFiles.count <> 0 Then
    ' Attempt: sort file names array
    finalFiles.files = Sort_Array(finalFiles.files)
End If

countFinalDocuments = finalFiles

Set fso = Nothing: Set fd = Nothing
End Function


Function countFoldersNamed(TargetFolder As String, folderMask As String) As foundFoldersStruct
' version 0.4
' given a folder path and a file mask (as per dir statement specs, using wildcards), returns number of
' files matching pattern and their names in a special structure we called "foundFilesStruct"

Dim annexFolders As foundFoldersStruct
Dim tmpFoldersFound As String

Dim fso As New Scripting.FileSystemObject
Dim fd As Scripting.Folder

' check if user supplied correct existing folder path
If fso.FolderExists(TargetFolder) Then
    Set fd = fso.GetFolder(TargetFolder)
Else
    Debug.Print "countFoldersNamed: received non existing folder name, namely " & TargetFolder
    annexFolders.count = 0
    Exit Function
End If

Dim folderMaskPart As Variant
' If user supplies a file mask containing boolean or, we use first part to
'If InStr(1, fileMask, "|") > 0 Then
    folderMaskPart = Split(folderMask, "|")
'Else
    'folderMaskPart = folderMask
'End If


For i = 0 To UBound(folderMaskPart)

    ' look for first file according to supplied mask into target folder
    tmpFoldersFound = Dir(fd.Path & "\" & folderMaskPart(i), vbDirectory)
    
    ' if using 3 and 4 chars extensions (like .doc and .docx), we need verify/ filter
    'If Not tmpFoldersFound Like folderMaskPart(i) Then
        'tmpFoldersFound = ""
    'End If
    
    If tmpFoldersFound = "" Then
        ' some users might use capitals for "final" in file name. Their right...
        tmpFoldersFound = Dir(fd.Path & "\" & UCase$(folderMask), vbDirectory)
        
        If tmpFoldersFound = "" Then
            countFoldersNamed.count = 0
            'Exit For
            GoTo skipThisOne
        Else    ' have tried capitals, that was it... proceed as usual
            GoTo allGood
        End If
    
    Else
    
allGood:
    
        annexFolders.count = annexFolders.count + 1
        ReDim Preserve annexFolders.folders(annexFolders.count - 1)
        annexFolders.folders(annexFolders.count - 1) = tmpFoldersFound
    End If
    
    ' now try to see if there are any more files conforming to specs in the folder
    ' and capture their names in specialised structure
    Do While (tmpFoldersFound) <> ""
        
        tmpFoldersFound = Dir
        
        If tmpFoldersFound <> "" Then
            annexFolders.count = annexFolders.count + 1
            ReDim Preserve annexFolders.folders(annexFolders.count - 1)
            annexFolders.folders(annexFolders.count - 1) = tmpFoldersFound
        End If
    Loop

skipThisOne:
Next i


countFoldersNamed = annexFolders

Set fso = Nothing: Set fd = Nothing
End Function



Sub RemoveParens_FromSelection()

Dim trng As Range, trngf As Range
Dim sp As String

If Documents.count > 0 Then

    If Selection.Type = wdSelectionNormal Then

        Set trng = Selection.Range  ' save selection, next command distroys it
        Set trngf = trng.Duplicate
        
        Selection.Collapse          ' intentionally collapse it, it will happen anyway
    
        sp = gscrolib.GetListSeparatorFromRegistry

        If sp = "" Then
            MsgBox "Failed extracting list separator from registry, please debug ! (Call 2942)"
            Exit Sub
        End If

        trng.Select: Selection.Collapse

        With trngf.Find
        
            .ClearAllFuzzyOptions
            .ClearFormatting
            .Format = False
            .Forward = True
            .Wrap = wdFindStop
            .MatchWildcards = True
                        
            .Execute FindText:="[\(]([0-9]{1" & sp & "}[a-z]{1" & sp & "})[\)](^t)", ReplaceWith:="\1.\2", Replace:=wdReplaceAll
            .Execute FindText:="[\(]([0-9]{1" & sp & "})[\)](^t)", ReplaceWith:="\1.\2", Replace:=wdReplaceAll
            
        End With
        
        Call Spellchecking_Done
        trng.Select
        
        StatusBar = "Remove parens: Removed parantheses from current selection numbering!"
    Else
        StatusBar = "Remove parens: No selecton found, Correct and retry!"
    End If

'ActiveDocument.SpellingChecked = True
'ActiveDocument.GrammarChecked = True
Else
    StatusBar = "Remove parens: No document opened, Exiting..."
End If


Set trng = Nothing
Set trngf = Nothing
End Sub





Sub Copy_DWSubjCodes_toClipboard()


Dim dwcopyRes As Integer
dwcopyRes = DW_SubjectCodes_ToClipboard


Dim sbMess As String        ' Status Bar Message
sbMess = "Copy DW SubjectCodes: "


Select Case dwcopyRes
    Case 0
        sbMess = sbMess & "Subject codes copied to clipboard!"
    Case 1
        sbMess = sbMess & "NO Subject codes found!"
    Case 2
        sbMess = sbMess & "NO DOCUMENT opened, Exiting..."
    Case 3
        sbMess = sbMess & "NO DW Metadata found! Exiting..."
End Select

StatusBar = sbMess

End Sub



Function get_DocumentToBackup(Optional documentID, Optional RunSilently) As String


Dim bqsbar As String
Dim tmpResult As String


If Application.Documents.count > 0 Then
    
    If gscrolib.Is_GSC_Doc(ActiveDocument.Name) Then
        
        ' IMPORTANT SAFEGUARD for X* documents, forbidden to archive on G drive
        If (getDocType_fromDocName(ActiveDocument.Name) = "XM" Or getDocType_fromDocName(ActiveDocument.Name) = "XT") And _
            UCase(DW_Get_Confidentiality(ActiveDocument)) = "LIMITE" Then
            bqsbar = bqsbar & "FORBIDDEN to archive! Exiting..."
            If IsMissing(RunSilently) Then StatusBar = bqsbar: PauseForSeconds (0.7)    ' user wont see message cause END follows
            End
        End If
        
        ' eliminates 'copy' from tail and [1] - mark of IWI as well
        tmpResult = getGSCBaseName(ActiveDocument.Name)
                                       
                                       
    Else    ' Not GSC document name
    
        ' If we ran BackupQ from a non-saved newly created doc, we' re still running empty
        If ActiveDocument.Name Like "Document#" Or ActiveDocument.Name Like "Document##" Or _
            Right$(ActiveDocument.Name, 3) = "dot" Or Right$(ActiveDocument.Name, 4) = "dotm" Then
            GoTo StillEmpty
        Else
            mpmsg = ("Current doc is not GSC (or is incorrectly named)!" _
                & Chr(13) & "Please re-run after opening one")
            Call gscrolib.msgbox_pausedisplay(CStr(mpmsg), "Backup Project Error", 3)
            Exit Function
        End If
        
    End If
    
Else    ' No document opened. Ask user for doc ID and backup that

' We land here from above, having run macro from newly created unsaved document, equivalent to no document
StillEmpty:
    
    
    ' if we called it with parameter, use it !
    If IsMissing(documentID) Then
        If UBound(Split(latestOpenedDocs, ",")) <> -1 Then
    
            tmpResult = InputBox("Please provide original language document standard name to finalise, up to second dot and original's language, if different from �EN�", _
                "Backup which project", Split(latestOpenedDocs, ",")(UBound(Split(latestOpenedDocs, ","))))
        Else
        
            tmpResult = InputBox("Please provide original language document standard name to finalise, up to second dot and original's language, if different from �EN�", _
                "Backup which project", "ST 12345/13 REV 1 EN")
        
        End If
    Else
        tmpResult = documentID
    End If

    
    ' correct for user not having supplied original's language, guess for english
    If Not IsValidLangIso(Right$(tmpResult, 2)) Then
        tmpResult = Trim(tmpResult) & " XX"
    End If
    
    tmpResult = GSC_StandardName_WithLang_toFileName(tmpResult)
    
    If Not Is_GSC_Doc(tmpResult) Then
        If IsMissing(RunSilently) Then StatusBar = "Supplied string NOT valid GSC document name and language string, Please retry!"
        Exit Function
    Else
        If InStr(1, latestOpenedDocs, tmpResult) = 0 Then
            latestOpenedDocs = latestOpenedDocs & IIf(latestOpenedDocs <> "", ",", "") & tmpResult
        End If
    End If
    
End If

get_DocumentToBackup = tmpResult

End Function


' UseStandardName means using a project ID instead of a folder path, such as "SN 1235/18 ADD1"
Function isUsefulProjectFolder(TargetProjectFolder As String, Optional UseStandardName) As Boolean


Dim fd As Scripting.Folder

Dim fso As New Scripting.FileSystemObject


If IsMissing(UseStandardName) Then
    Set fd = fso.GetFolder(TargetProjectFolder)
Else    ' using standard name
    
    Dim projFdrPath As String
    projFdrPath = getProjectFolderOf(GSC_StandardName_WithLang_toFileName(TargetProjectFolder, , "IgnoreSuppliedLanguage"))
    
    If projFdrPath <> "" Then
        Set fd = fso.GetFolder(projFdrPath)
    Else    ' could not retrieve project folder... (getProjectFolderOf is not perfect... does not handle correctly multilanguage projects ie)
        isUsefulProjectFolder = False
        GoTo skipToEnd  ' and clean up
    End If
    
End If


If Not fd Is Nothing Then
    
    Dim hasStudio_Subfolder As Boolean
    Dim hasPotentially_Useful_Files As Boolean
    
    hasStudio_Subfolder = hasStudioFolder(fd)   ' does Studio subfolder exist and is non-zero ?
    
    ' if project has been worked on with Studio, this folder is going to be present.
    If hasStudio_Subfolder = True Then
        isUsefulProjectFolder = True
    Else    ' maybe non-empty non-Studio project folder?
        
        hasPotentially_Useful_Files = hasUsefulFiles(fd)
        
        If hasPotentially_Useful_Files = True Then
            isUsefulProjectFolder = True
        Else
            isUsefulProjectFolder = False
        End If
        
    End If

Else
    
    isUsefulProjectFolder = False
    
End If




skipToEnd:
    Set fso = Nothing
    Set fd = Nothing

End Function

' if supplied project folder does have non-zero size files within, it might be useful
Function hasUsefulFiles(TargetProjectFolder As Scripting.Folder) As Boolean

If TargetProjectFolder.files.count > 0 Then
    
    Dim totalSizeOfFiles As Long
    
    Dim ctFile As Scripting.File
    
    For Each ctFile In TargetProjectFolder.files
        totalSizeOfFiles = totalSizeOfFiles + ctFile.Size
    Next ctFile
    
    If totalSizeOfFiles > 0 Then
        hasUsefulFiles = True
    Else
        hasUsefulFiles = False
    End If
    
Else
    hasUsefulFiles = False
End If

Set ctFile = Nothing

End Function


' we check both presence of "Studio" subfolder as well as its positive size
Function hasStudioFolder(TargetProjectFolder As Scripting.Folder) As Boolean


Dim studioFolderPresence As Boolean

Dim studioFolderSizeNonZero As Boolean

Dim fso As New Scripting.FileSystemObject
 
Dim fds As Scripting.Folder


If Dir(TargetProjectFolder.Path & "\Studio", vbDirectory) <> "" Then
    
    Set fds = fso.GetFolder(TargetProjectFolder.Path & "\Studio")
    
    If fds.Size > 0 Then
        hasStudioFolder = True
    Else
        hasStudioFolder = False
    End If
    
Else
    hasStudioFolder = False
End If


Set fds = Nothing
Set fso = Nothing

End Function


Function backupQ(Optional documentID, Optional autoOverwrite, Optional RunSilently) As Integer
' Retrieves and uses option value associated with key "ProjFdrBackupLocation" from section "GSC_A3_Options"

' version 1.1.3
' main routine

' Macroul verifica existenta unui folder cu numele identic cu al documentului SGC, versiunea lingvistica originala,
' in locatia de productie a unitatii RO ("T:\Prepared originals"). Daca il gaseste, copie acest folder la locatia
' stabilita pentru arhivarea locala ("Q:\01-TRADUCERI\"). Macroul functioneaza si pentru folderele gresit denumite,
' cele care contin codul de limba "en" in locul codului din workflow.

' 0.40 - Adding possibility to backup folder by manually supplying project ID in textbox (added for xlf files)
' 0.41 - First attempt at getting less string from user, don't supply original's language if it's "EN"
' 0.42 - Modify to see non-named document (autocreated "DocumentX.docx" documents) as "no document"
' 0.43 - Modified to also backup the message files in a separate, "Messages" folder, if they exist
' 0.44 - Changed name of messages folder to "Mesaje", as requested
' 0.45 - Removed "Mesaje" from name of messages folder... they will be expected to be in root of project folder instead
' 0.46 - Added "memory" of last manually inputted document... primitive, but will be improved! (to remember and allow access to all recently inputted !)
' 0.47 - Added optional parameter, document id to process
' 0.48 - Added optional parameter, skip asking user about overwriting target folder content
' 0.49 - Adding autobackup of excel files found in root of project folder and "Studio/Ro-RO" subfolder
' 0.50 - Using bDir custom function to list/ search all ".msg" and *.htm files from any subfolder named "Messages|Mesaje" or in root project folder
' 0.60 - Major change, not disruptive to existing functionality. Adding mandatory backup of CL. P7TA and other tmxs to special folder
'        ("Q:\01-TRADUCERI\zzzOtherTmxs") (tmx memories which we do not keep centrally, DO come back to be re-aligned later!)
' 0.7  - Important change, safeguard against backing-up XM and XT Limite docs - NEVER do that !
' 0.8  - Adding year folder and type folder to save path... can't just dump all project folder into one folder!
' 0.81 - Making macro work with settings file to retrieve backup base folder
' 0.83 - Removing dependence of Docuwrite.Utils. What was I thinkin?
' 0.84 - Making excel files copy using mask function as well...
' 0.9  - Transform into function returning success level integer; Added optional param RunSilently for bundle running
' 1.1  - Presidency docs tmx backup added
' 1.1.1 - Prez docs list acception enlarged: added SN 40 range for Press Releases
' 1.1.2 - BackupQ renames Presidency docs tmx broken name (removing CLSTMX from extension & cleaning beginning of file name)
' 1.1.3 - BackupQ better behaviour when Prez tmx not there (detailed warning message with FAIL code)

' If wishing to save review files to diff location, use 2 following settings in settings storage (file)
' ReviewFilesBackupLocation=\\DM4\HOME\Docs\ReviewsBackup
' UseDifferentReviewFilesLocation = True


Dim fso As New Scripting.FileSystemObject

Dim fo, fd, so, sd, sfso, backupPath

Dim PointPos As Long, nameLength As Long
Dim ctdocument As String, Msg As String

Dim tname As String
Dim userChoice As Integer

Dim isStudio As Boolean
Dim backupSuccessLevel As Integer ' 1 - review files succeded; 2 - xliff backup succeded; 3 - both succeded!


bqsbar = "BackupQ: "
If IsMissing(RunSilently) Then Application.StatusBar = bqsbar

' we can pass along missing arguments, even if not supplied... missing therefore. The missing status will be kept in functions on aval
ctdocument = get_DocumentToBackup(documentID, RunSilently)


Dim docsType As String, docYear As String, docsLang As String
docsType = getDocType_fromDocName(ctdocument)
' 0.8 mod: adding year and doc type to path!
' 0.85 - put files into year subfolder acc to docs year, not current year!
docYear = getDocYear_fromDocName(ctdocument)
docsLang = getLanguage_fromDocName(ctdocument)



'**************************** NEED REPLACE WITH GETPROJECTFOLDEROF, right? ************************************
'**************************************************************************************************************

' Get project folder if named like current document, take other actions if not !
If fso.FolderExists(ProjectsBaseFolder & ctdocument) Then
    Set fo = fso.GetFolder(ProjectsBaseFolder & "\" & ctdocument)
Else
    ' Daca nu gasim folderul in limba documentului, cautam sa vedem daca exista in en sau fr
    If fso.FolderExists(ProjectsBaseFolder & Replace$(ctdocument, docsLang, "en")) Then
        tname = Replace$(ctdocument, docsLang, "en")
                
        bqsbar = bqsbar & " | Requested language folder NOT FOUND on T! Found and opened EN version folder!"
        If IsMissing(RunSilently) Then StatusBar = bqsbar
        
FRToo:
        newCtDocument = ctdocument: ctdocument = tname
        Set fo = fso.GetFolder(ProjectsBaseFolder & ctdocument)
        
    ElseIf fso.FolderExists(ProjectsBaseFolder & Replace$(ctdocument, docsLang, "fr")) Then
        tname = Replace$(ctdocument, docsLang, "fr")
        GoTo FRToo
    Else
        ' Nici folderele EN, nici FR nu exista pe T:\Prepared originals, deci iesim dupa ce avertizam userul
        mpmsg = "Folders  " & ctdocument & "  &  " & tname & " DONT EXIST on " & ProjectsBaseFolder & _
        Chr(13) & "Please run macro again with a GSC doc opened!"
        
        Call gscrolib.msgbox_pausedisplay(CStr(mpmsg), "Project folders not found!", 4)
        bqsbar = bqsbar & " | Backup Project: Macro execution canceled"
        If IsMissing(RunSilently) Then StatusBar = bqsbar
        
        Exit Function
        
    End If
End If

'**************************** NEED REPLACE WITH GETPROJECTFOLDEROF, right? ************************************
'**************************************************************************************************************


' Check if project is Studio project or not, need do different things if so
If fso.FolderExists(ProjectsBaseFolder & ctdocument & studioFolder) Then
    isStudio = True
End If


Dim sFile As New SettingsFileClass
' Settings file does not exist, no backup folder preference!
If Not sFile.Exists Then
    MsgBox "Please set projects backup folder using the �Preferences� button" & vbCr & vbCr & _
        "You will find it in GSC A3 ribbon tab, �GSC A3 Tools� controls group", vbOKOnly + vbCritical, "Settings file missing"
    End
End If

' get stored backup location base folder
Dim backupBaseFolder As String
' Mitigating for both ways of options userform workways

Dim gscA3OptionsArr
gscA3OptionsArr = sFile.cCache_Section_toMemory("GSC_A3_Options")

backupBaseFolder = sFile.cGetOption_fromMemory(gscA3OptionsArr, "ProjFdrBackupLocation")
'backupBaseFolder = sFile.getOption("ProjFdrBackupLocation", "GSC_A3_Options")

' CHECK FOLDER EXISTENCE AND FORMAT
If Not fso.FolderExists(backupBaseFolder) Or Not backupBaseFolder Like "?:\*" Then
    MsgBox "Please set projects backup folder using the �Preferences� button" & vbCr & vbCr & _
        "You will find it in GSC A3 ribbon tab, �GSC A3 Tools� controls group", vbOKOnly + vbCritical, "Backup base folder missing"
    End
End If


' create year folder if not exists
If Not fso.FolderExists(backupBaseFolder & "\" & docYear) Then
    fso.CreateFolder (backupBaseFolder & "\" & docYear)
End If
' create year - type folder
If Not fso.FolderExists(backupBaseFolder & "\" & docYear & "\" & docsType) Then
    fso.CreateFolder (backupBaseFolder & "\" & docYear & "\" & docsType)
End If


backupPath = backupBaseFolder & "\" & docYear & "\" & docsType & "\" & ctdocument

' IF target backup folder exists warn and overwrite
If fso.FolderExists(backupPath) Then
    
    Msg = "Folder " & ctdocument & " already exists on Q! Overwrite folder?" & Chr(13) _
            & "Identically named files will be overwritten!" & Chr(13) & Chr(13) & _
            "YES to overwrite, NO to exit and open folder"
   
    sfso = fso.GetFolder(backupPath).Size
    If sfso > 0 Then
        
        ' autoConfirm overwrite of target folder if requested
        If IsMissing(autoOverwrite) Then
            userChoice = MsgBox(Msg, vbYesNoCancel, "Warning!")
        Else
            userChoice = vbYes
        End If
        
        If userChoice = 2 Then bqsbar = bqsbar & _
            " | Backup Project: Execution canceled. Check destination folder contents and retry.": _
            If IsMissing(RunSilently) Then StatusBar = bqsbar: Exit Function
        If userChoice = 7 Then bqsbar = bqsbar & _
            " | Backup Project: Execution canceled. Check destination folder contents and retry.": _
            If IsMissing(RunSilently) Then StatusBar = bqsbar: Call Shell("explorer.exe " & backupPath, vbNormalFocus): Exit Function
    End If
            
End If


' Main business HERE
If isStudio Then
    
    backupSuccessLevel = 0
    
    ' Create backup destination folder
    If Not fso.FolderExists(backupPath) Then
        fso.CreateFolder backupPath
    End If
            
    ' Copy review files if parent folder exists
    If fso.FolderExists(ProjectsBaseFolder & ctdocument & studioReviewFolder) Then
        
        If Dir(ProjectsBaseFolder & ctdocument & getReviewFileMask) <> "" Then
            
            ' if user expressed wish, we backup review files at different path
            Dim diffReviewPath As String
            If sFile.cGetOption_fromMemory(gscA3OptionsArr, "UseDifferentReviewFilesLocation") = "True" Then
                diffReviewPath = sFile.cGetOption_fromMemory(gscA3OptionsArr, "ReviewFilesBackupLocation")
                fso.CopyFile ProjectsBaseFolder & ctdocument & getReviewFileMask, diffReviewPath, True
            Else    ' copy review files to default location
                fso.CopyFile ProjectsBaseFolder & ctdocument & getReviewFileMask, backupPath, True
            End If

            backupSuccessLevel = backupSuccessLevel + 1
        
            bqsbar = bqsbar & " | Review folder backed-up!"
            If IsMissing(RunSilently) Then StatusBar = bqsbar
        
        End If
        
    End If
    
    ' Backup xliff files as well
    If Dir(ProjectsBaseFolder & ctdocument & getXliffFilesMask) <> "" Then
        fso.CopyFile ProjectsBaseFolder & ctdocument & getXliffFilesMask, backupPath, True
        
        backupSuccessLevel = backupSuccessLevel + 2

        bqsbar = bqsbar & " | xliff files backed-up!"
        If IsMissing(RunSilently) Then StatusBar = bqsbar
        
    End If
    
    ' Backup tmx mem exports files for RO Presidency docs
    If Left(ctdocument, 4) = "sn30" Or Left(ctdocument, 4) = "np10" Or Left(ctdocument, 4) = "sn40" Then    ' Prez docs test
        
        ' SHOULD move this to a separate function?
        Dim oldName As String, newName As String
        Dim oldPath As String, newPath As String
        '
        If Dir(ProjectsBaseFolder & ctdocument & prezTmxMask) <> "" Or _
            Dir(ProjectsBaseFolder & ctdocument & prezTmxMaskRenamed) <> "" Then
            
            If Dir(ProjectsBaseFolder & ctdocument & prezTmxMask) <> "" Then
            
                oldName = Dir(ProjectsBaseFolder & ctdocument & prezTmxMask)
                newName = Replace(Dir(ProjectsBaseFolder & ctdocument & prezTmxMask), "CLSTMX", "")
                
                If UBound(Split(newName, "_")) = 3 Then
                    newName = Split(newName, "_")(3)
                Else
                    MsgBox "Presidency TMX name not conform to standard, please debug!" & _
                    "Backup will continue with tmx save, but incorrect name!", vbOKOnly + vbCritical
                End If
            Else
                oldName = Dir(ProjectsBaseFolder & ctdocument & prezTmxMaskRenamed)
                newName = oldName
            End If
            
            oldPath = ProjectsBaseFolder & ctdocument & "\Studio\TMs - Export\" & oldName
            newPath = ProjectsBaseFolder & ctdocument & "\Studio\TMs - Export\" & newName
            
            Name oldPath As newPath
            PauseForSeconds (0.2)   ' give time to OS to execute !?!
        
        Else        ' NO tmx found in folder, backupQ is being done before Studio Finalisation !
            
            'backupSuccessLevel = 0      ' No Prez tmx, this is a FAIL !
            MsgBox "Presidency document tmx missing! Please perform a Studio finalise BEFORE and run BackupQ again, to enable tmx backup!", _
                vbOKOnly + vbCritical, "Presidency TMX missing"
            
        End If
        
        If Dir(ProjectsBaseFolder & ctdocument & getPrezTmxFilesMask) <> "" Then
            fso.CopyFile ProjectsBaseFolder & ctdocument & getPrezTmxFilesMask, backupPath, True
            
            'backupSuccessLevel = backupSuccessLevel + 2
        
            bqsbar = bqsbar & " | PTmx backed-up!"
            If IsMissing(RunSilently) Then StatusBar = bqsbar
            
        End If
        
    End If
    
    ' Backup messages folder contents if it exists
    ' DOESN'T affect successlevel
    Dim msgPart As Variant
    msgPart = Split(getMsgFilesMask, "|")
    '
    Dim gotAtLeastOneMsg As Boolean         ' Logical flag for found message files within project folders
    'Dim msgCollection As New Collection     ' to return messages's file paths into
    
    Dim msgFilesPaths As foundFilesStruct
        
    For i = 0 To UBound(msgPart)
        'If Dir(projectsBaseFolder & ctdocument & msgPart(i)) <> "" Then
        msgFilesPaths = bDir.bGDirAll(ProjectsBaseFolder & ctdocument & "\" & messagesFoldername & msgPart(i), returnFullPaths:="True")
        
        If msgFilesPaths.count > 0 Then
            
                Dim msgFolderBase As String
                msgFolderBase = Left$(msgFilesPaths.files(0), InStrRev(msgFilesPaths.files(0), "\") - 1)
                
                ' We need to skip backing up items from the background folder, this one, a subfolder of project folder, contains bases memories. No backing up there.
                If Not InStr(1, msgFolderBase, "Background") > 0 Then   ' DO NOT attempt to backup htmls and msgs from Background folder!
                    
                    fso.CopyFile msgFolderBase & msgPart(i), backupPath, True
                        
                    If Not gotAtLeastOneMsg = True Then gotAtLeastOneMsg = True
                
                End If
                
                ' Do not add to existing collection, empty it first!
                'Set msgCollection = New Collection
                    
        End If
    Next i
    '
    ' And print message if we have at least
    If gotAtLeastOneMsg Then
        bqsbar = bqsbar & " | messages backed-up!"
        If IsMissing(RunSilently) Then StatusBar = bqsbar
    End If
    
    
    ' Backup xls files as well, found in main project folder
    ' DOES NOT affect success level!
    Dim xlsFilesFound As foundFilesStruct
    Dim xlsFilesMask As String
    
    xlsFilesMask = getXlsFilesMask
    
    
    Dim xlsPart As Variant
    xlsPart = Split(xlsFilesMask, "|")
    
    For i = 0 To UBound(xlsPart)
    
        xlsFilesFound = bDir.bGDirAll(FileSearchMask:=ProjectsBaseFolder & ctdocument & xlsPart(i), returnFullPaths:="ReturnFullPaths")
        
        ' Found some excel files!
        If xlsFilesFound.count > 0 Then
        
            Dim xlsFolderBase As String
            xlsFolderBase = Left$(xlsFilesFound.files(0), InStrRev(xlsFilesFound.files(0), "\") - 1)
        
            
            fso.CopyFile xlsFilesFound.files(l), backupPath & "\", True
            DoEvents
            'PauseForSeconds (0.1)   Necessary or not?
            
            Dim foundOneExcel As Boolean
            If Not foundOneExcel Then foundOneExcel = True
            
        Else
        End If
        
    Next i
    
    ' update message to user
    If foundOneExcel Then
        bqsbar = bqsbar & " | excels backed-up!"
        If IsMissing(RunSilently) Then StatusBar = bqsbar
    End If
    
    
    ' Display success/ failure message to user (StatusBar)
    Select Case backupSuccessLevel
        Case 1
            bqsbar = bqsbar & " | PARTIALLY successful!"
            If IsMissing(RunSilently) Then StatusBar = bqsbar
        Case 2
            bqsbar = bqsbar & " | PARTIALLY successful!"
            If IsMissing(RunSilently) Then StatusBar = bqsbar
        Case 3
            bqsbar = bqsbar & " | SUCCESS!"
            If IsMissing(RunSilently) Then StatusBar = bqsbar
        Case Else ' 0 maybe ? None succeded !
            mpmsg = "Project folder failed! (No review or xliff files found!" & vbCr & vbCr & _
                "Please call margama(2942)"
            Call gscrolib.msgbox_pausedisplay(CStr(mpmsg), "Backup FAILED!", 3)
    End Select
    
Else    ' Not Studio project, but we compare total files size in both folders
    
    fso.CopyFolder ProjectsBaseFolder & ctdocument, backupPath, True

    Set fd = fso.GetFolder(backupPath)
    so = fo.Size: sd = fd.Size
    
    ' Compare files size
    If so = sd Then
        backupSuccessLevel = 3  ' Total success
        bqsbar = bqsbar & " | SUCCESS! | Project folder backed-up!"
        If IsMissing(RunSilently) Then StatusBar = bqsbar
    Else    ' still a success, albeit backup is fatter than source folder!
        If sd > so Then
            backupSuccessLevel = 3  ' Still total success!
            bqsbar = bqsbar & " | SUCCESS! | Project folder backed-up! | Other files present in folder."
            If IsMissing(RunSilently) Then StatusBar = bqsbar
        End If
    End If
    
    ' Still valid operation, we copy whole folder, temp files to be deleted !
    If fso.FileExists(backupPath & "\" & "~*") = True Then
        fso.DeleteFile backupPath & "\" & "~*", True
    End If
    
End If

' return success code
backupQ = backupSuccessLevel


Set fso = Nothing
Set fo = Nothing: Set fd = Nothing
End Function



' If optional parameter BackupRootFolder is used, the function will not instantiate a SettingsFileClass object and try to retrieve the setting by itself
Function isProject_BackedUp(projectID As String, Optional UseStandardName, Optional BackupRootFolder As String) As Boolean


Dim fso As New Scripting.FileSystemObject
Dim fd As Scripting.Folder

Dim backupPath As String, ctdocument As String

Dim suppliedProjID As String

' upon user switching to using standard doc name, convert it to file name
If Not IsMissing(UseStandardName) Then
    suppliedProjID = GSC_StandardName_WithLang_toFileName(projectID, IgnoreSuppliedLanguage:="IgnoreSuppliedLanguage")
Else    ' or use it like so
    suppliedProjID = projectID
End If


Dim docsType As String
docsType = getDocType_fromDocName(suppliedProjID)

' 0.8 mod: adding year and doc type to path!
Dim docYear As String
'docYear = DatePart("yyyy", Date)
' 0.85 - put files into year subfolder acc to docs year, not current year!
docYear = getDocYear_fromDocName(suppliedProjID)


' separate definition from instantiation, it wont be done if user uses root folder parameter
Dim sFile As SettingsFileClass
' get stored backup location base folder
Dim backupBaseFolder As String


If Not IsMissing(BackupRootFolder) Then
    ' option designed to be used when wishing to get backing up status of several, many projects - eventually using iteration. Cause instantiating the settings file class
    ' on every passage through loop will... waste cpu power ! lotsa!
    backupBaseFolder = BackupRootFolder
    
Else    ' option designed to be used when wishing to find the backing status of ONE or several, but few projects... no iteration involved

    Set sFile = New SettingsFileClass
    ' Settings file does not exist, no backup folder preference!
    If Not sFile.Exists Then
        MsgBox "Please set projects backup folder using the �Preferences� button" & vbCr & vbCr & _
            "You will find it in GSC A3 ribbon tab, �GSC A3 Tools� controls group", vbOKOnly + vbCritical, "Settings file missing"
        End
    End If
    ' Mitigating for both ways of options userform workways
    backupBaseFolder = sFile.getOption("ProjFdrBackupLocation", "GSC_A3_Options")
    
End If


' CHECK FOLDER EXISTENCE AND FORMAT
If Not fso.FolderExists(backupBaseFolder) Or Not backupBaseFolder Like "?:\*" Then
    MsgBox "Please set projects backup folder using the �Preferences� button" & vbCr & vbCr & _
        "You will find it in GSC A3 ribbon tab, �GSC A3 Tools� controls group", vbOKOnly + vbCritical, "Backup base folder missing"
    End
End If

ctdocument = getGSCBaseName(suppliedProjID)

backupPath = backupBaseFolder & "\" & docYear & "\" & docsType & "\" & ctdocument

' if find we the original language folder in the backup archive, use the correct folder path, o'course!
If Dir(Replace(backupPath, ".xx", ".??"), vbDirectory) <> "" Then
    backupPath = backupBaseFolder & "\" & docYear & "\" & docsType & "\" & Dir(Replace(backupPath, ".xx", ".??"), vbDirectory)
End If



If Dir(backupPath, vbDirectory) <> "" Then
        
    Set fd = fso.GetFolder(backupPath)
    
    If fd.Size > 0 Then     ' quite possibly empty folder
        isProject_BackedUp = True
    Else
        isProject_BackedUp = False
    End If
    
Else     ' NO such folder, proj not backed up or backed in wrong path (missing year subfolder maybe?)

    isProject_BackedUp = False
    
End If



Set fd = Nothing
Set fso = Nothing

Set sFile = Nothing

End Function

Sub Test_areProjectsBackedUp()

Dim inputProj As String

inputProj = "SN 1142 2018 REV1 RO" & vbCr & _
"SN 1204 2018 REV2 RO" & vbCr & _
"SN 1309 2018 REV1 RO" & vbCr & _
"SN 2353 2018 INIT RO" & vbCr & _
"SN 2379 2018 INIT RO" & vbCr & _
"SN 2379 2018 REV1 RO" & vbCr & _
"SN 2428 2018 INIT RO" & vbCr & _
"SN 2429 2018 INIT RO" & vbCr & _
"SN 2514 2018 INIT RO" & vbCr & _
"SN 2518 2018 INIT RO" & vbCr & _
"SN 2538 2018 INIT RO" & vbCr & _
"SN 2540 2018 INIT RO" & vbCr & _
"SN 2568 2018 REV2 RO" & vbCr & _
"SN 2581 2018 REV1 RO" & vbCr & _
"SN 2601 2018 INIT RO" & vbCr & _
"SN 2608 2018 INIT RO" & vbCr & _
"SN 2609 2018 REV1 RO" & vbCr & _
"SN 2629 2018 INIT RO" & vbCr & _
"SN 2638 2018 INIT RO" & vbCr & _
"SN 2646 2018 INIT RO" & vbCr & _
"SN 5650 2017 REV1 RO" & vbCr & _
"SN 5677 2017 REV6 RO" & vbCr & _
"SN 5677 2017 REV7 RO" & vbCr & _
"SN 5898 2017 REV5 RO"


Dim notBacked As String

notBacked = areProjects_BackedUp(inputProj)

MsgBox "The following projects folders have NOT been backed-up:" & vbCr & vbCr & _
    notBacked


End Sub

Function getUsedSeparator(InputString) As String

If InStr(1, InputString, vbCr) > 0 Then
    sep = vbCr
ElseIf InStr(1, InputString, ",") > 0 Then
    sep = ","
ElseIf InStr(1, InputString, "|") > 0 Then
    sep = "|"
Else
    sep = vbCr   ' default to enter in case user does not supply !
End If

getUsedSeparator = sep

End Function

' sister function of isProject_BackedUp, using it to check and return a list of NOT backedup folders,
' behaviour changeable by using optional parameter (to return a list of backed up folders instead)
Function areProjects_BackedUp(projectsList As String, Optional ReturnBackedUpInstead) As String     ' Function will return the names of projects NOT backed-up, or empty string if all are


Dim sep As String                       'user supplied separator, accepting only , | or Enter (vbCr)
sep = getUsedSeparator(projectsList)


' Remove linefeed character
If InStr(1, projectsList, vbLf) > 0 Then
    projectsList = Replace(projectsList, vbLf, "")
End If


Dim projectsToCheck
projectsToCheck = Split(TrimNonAlphaNums(projectsList), sep)


Dim projBase As String
projBase = getProjectsBaseFolder


If projBase = "" Then
    MsgBox "Could not retrieve projects backup folder from settings file!" & vbCr & vbCr & "Please check setting existence, correct and retry!"
    areProjects_BackedUp = ""
    Exit Function
End If


' temp hold the names of not backed up projects for return
Dim tmpResult As String

For i = 0 To UBound(projectsToCheck)
    
    If IsMissing(ReturnBackedUpInstead) Then
        ' avoid very quick repetitive instantiation of settings file class, kills computer !
        ' by using the optional BackupRootFolder parameter of isProject_BackedUp !
        If Not isProject_BackedUp(CStr(projectsToCheck(i)), "UseStandardName", projBase) Then
            
            If isUsefulProjectFolder(CStr(projectsToCheck(i)), "UseStandardName") Then
                tmpResult = IIf(tmpResult = "", projectsToCheck(i), tmpResult & sep & projectsToCheck(i))
            End If
            
        End If
        
    Else    ' save either backedUp or NOT backedUp projects into temp results array
        If isProject_BackedUp(CStr(projectsToCheck(i)), "UseStandardName", projBase) Then
            tmpResult = IIf(tmpResult = "", projectsToCheck(i), tmpResult & sep & projectsToCheck(i))
        End If
    End If
Next i

areProjects_BackedUp = tmpResult


End Function



' returns: empty string for total success (all projects supplied backed up) or list of still unable to backup projects!
' projectsToBackUp is an array, but usually a variant array
Function backupProjectsList(projectsToBackUp) As String


'Dim sep As String
'sep = getUsedSeparator(projectsToBackUp)

'If InStr(1, projectsToBackUp, vbLf) > 0 Then
    'projectsToBackUp = Replace(projectsToBackUp, vbLf, "")
'End If


Dim projectsList
projectsList = projectsToBackUp     ' input param is array


If Not IsArray(projectsList) Then
    MsgBox "Could not retrieve list of projects to process, used separator: " & sep
    backupProjectsList = ""     ' Failure
    Exit Function
End If


Dim tmpSuccess As Integer       ' temp var to capture success of current operation
Dim tmpProjectName As String    ' temp var to hold reconstructed project folder name
Dim projectsStillNotBU As String

For i = 0 To UBound(projectsList)
    
    tmpProjectName = GSC_CarsNameFormat_ToStandardFormat(CStr(projectsList(i)), "EliminateSuppliedLanguage")
    tmpSuccess = backupQ(tmpProjectName, , "RunSilently")
    
    ' if failure, write to failure list
    If tmpSuccess = 0 Or tmpSuccess > 3 Then
        projectsStillNotBU = IIf(projectsStillNotBU = "", projectsList(i), projectsStillNotBU & vrCr & vbLf & projectsList(i))
    End If
    
Next i

' return result
backupProjectsList = projectsStillNotBU


End Function

Function getProjectsBaseFolder() As String

Dim sFile As New SettingsFileClass
If Not sFile.Exists Then
    MsgBox "Error 1:  Settings file non existent!"
    getProjectsBaseFolder = ""
    Exit Function
ElseIf sFile.IsEmpty <> FileNotEmpty Then
    MsgBox "Error 2: Settings file not found or empty!"
    getProjectsBaseFolder = ""
    Exit Function
End If


Dim gscA3OptionsArr
gscA3OptionsArr = sFile.cCache_Section_toMemory("GSC_A3_Options")


If Not IsArray(gscA3OptionsArr) Then
    MsgBox "Error 3"    ' got empty value from settings file instead of settings section array
    getProjectsBaseFolder = ""   ' should probably return: empty string if not using ReturnBackedUpInstead parameter, however, all received docs if using it !
    Exit Function
End If

If IsEmpty(gscA3OptionsArr) Then
    MsgBox "Error 4"    ' got empty value from settings file instead of settings section array
    getProjectsBaseFolder = ""   ' should probably return: empty string if not using ReturnBackedUpInstead parameter, however, all received docs if using it !
    Exit Function
End If


getProjectsBaseFolder = sFile.cGetOption_fromMemory(gscA3OptionsArr, "ProjFdrBackupLocation")


End Function


' retrieve supplied document name's year
Function getDocYear_fromDocName(docName As String, Optional getShortYear) As String
    
    If Not Is_GSC_Doc(docName) Then
        getDocYear_fromDocName = ""
        Exit Function
    End If
    
        
    Dim docNameParts
    docNameParts = Split(docName, ".")
    
    'st1253-re01ad02.en17.docx
    
        
    If UBound(docNameParts) <> 1 And UBound(docNameParts) <> 2 Then     ' no other cases possible for valid names!
        
        getDocYear_fromDocName = ""
        Exit Function
        
    End If
    
    
    ' supplied doc id without extension, valid
    If IsValidLangIso(Left(docNameParts(1), 2)) And IsNumeric(Mid(docNameParts(1), 3, 2)) Then
        
        Dim tmpYear As String
        tmpYear = Mid(docNameParts(1), 3, 2)
        
        If Not IsMissing(getShortYear) Then
            
            getDocYear_fromDocName = tmpYear
            
        Else
            ' need to complete the year, add missing 2 digits beginning
            If tmpYear > Right(DatePart("yyyy", Date), 2) Then
                tmpYear = "19" & tmpYear
            Else
                tmpYear = "20" & tmpYear
            End If
          
            getDocYear_fromDocName = tmpYear
            
        End If
        
    Else    ' the above was string validation for language - year combo, we do not accept any less...
        getDocYear_fromDocName = ""
        Exit Function
    End If
    
End Function


Function getXlsFilesMask() As String


Dim gro_Sfile As New SettingsFileClass

Dim storedXlsMaskString As String


If Not gro_Sfile.Exists Then
    
    storedXlsMaskString = frmgscroopt.tbXlsFilesMask.Text
    
    If storedXlsMaskString = "" Then
        MsgBox "Excel files copy mask string not found on GSCA3 preferences form!" & vbCr & vbCr & "Please set it using the Preferences button in GSCA3 Tab!", vbOKOnly, _
            "Xls files mask missing (Userform)"
        Exit Function
            
    End If
    
Else    ' Settings file exists
    
    storedXlsMaskString = gro_Sfile.getOption("XlsFilesCopyMask", "GSC_A3_OptionsAdv")
    
    If storedXlsMaskString = "" Then
        
        storedXlsMaskString = frmgscroopt.tbXlsFilesMask.Text
        
        If storedXlsMaskString = "" Then     ' What if it did not find the setting?
            
            MsgBox "Excel files copy mask string not found in settings file!" & vbCr & vbCr & "Please set it using the Preferences button in GSCA3 Tab!", vbOKOnly, _
            "Xls files mask missing"
            
            Exit Function
        
        End If
        
    End If
    
End If

'Debug.Print "Retrieved xls files mask as: " & storedXlsMaskString

getXlsFilesMask = storedXlsMaskString


End Function


Function getPrezTmxFilesMask() As String

Dim gro_Sfile As New SettingsFileClass

Dim storedPrezTmxFilesMask As String


If Not gro_Sfile.Exists Then
    
    storedPrezTmxFilesMask = frmgscroopt.tbPrezTmxFileMask.Text
    
    If storedPrezTmxFilesMask = "" Then
        MsgBox "", vbOKOnly, "Settings file NOT found! Please check and save settings using �Preferences� button on ribbon tab �GSCA3�"
        getPrezTmxFilesMask = ""
        Exit Function
    End If

Else
    
    storedPrezTmxFilesMask = gro_Sfile.getOption("PrezTmxFileCopyMask", "GSC_A3_OptionsAdv")
    
    If storedPrezTmxFilesMask = "" Then
        storedPrezTmxFilesMask = frmgscroopt.tbPrezTmxFileMask.Text
        
        If storedPrezTmxFilesMask = "" Then
            MsgBox "PrezTmx files copy mask not found in settings file. Please fill in file copy mask using the �Preference� button on ribbon tab �GSCA3�", _
                vbOKOnly, "PrezTmx mask not found!"
        End If
        
    End If
    
End If

getPrezTmxFilesMask = storedPrezTmxFilesMask


End Function


Function getMsgFilesMask() As String

Dim gro_Sfile As New SettingsFileClass

Dim storedMsgFilesMask As String


If Not gro_Sfile.Exists Then
    
    storedMsgFilesMask = frmgscroopt.tbMessageFilesMask.Text
    
    If storedMsgFilesMask = "" Then
        MsgBox "", vbOKOnly, "Settings file NOT found! Please check and save settings using �Preferences� button on ribbon tab �GSCA3�"
        getMsgFilesMask = ""
        Exit Function
    End If

Else
    
    storedMsgFilesMask = gro_Sfile.getOption("MessageFilesCopyMask", "GSC_A3_OptionsAdv")
    
    If storedMsgFilesMask = "" Then
        storedMsgFilesMask = frmgscroopt.tbMessageFilesMask.Text
        
        If storedMsgFilesMask = "" Then
            MsgBox "Message files copy mask not found in settings file. Please fill in file copy mask using tge �Preference� button on ribbon tab �GSCA3�", _
                vbOKOnly, "Message mask not found!"
        End If
        
    End If
    
End If

getMsgFilesMask = storedMsgFilesMask


End Function


Function getXliffFilesMask() As String
' Retrieves and returns option �XliffFilesCopyMask� from section "GSC_A3_Options"

Dim gro_Sfile As New SettingsFileClass

Dim storedXliffMaskString As String


If Not gro_Sfile.Exists Then
    
    storedXliffMaskString = frmgscroopt.tbXliffFilesMask.Text
    
    If storedXliffMaskString = "" Then
        MsgBox "Xliff files copy mask string not found on GSCA3 preferences form!" & vbCr & vbCr & "Please set it using the Preferences button in GSCA3 Tab!", vbOKOnly, _
            "Xliff files mask missing (Userform)"
        Exit Function
            
    End If
    
Else    ' Settings file exists
    
    storedXliffMaskString = gro_Sfile.getOption("XliffFilesCopyMask", "GSC_A3_OptionsAdv")
    
    If storedXliffMaskString = "" Then
        
        storedXliffMaskString = frmgscroopt.tbXliffFilesMask.Text
        
        If storedXliffMaskString = "" Then     ' What if it did not find the setting?
            
            MsgBox "Xliff files copy mask string not found in settings file!" & vbCr & vbCr & "Please set it using the Preferences button in GSCA3 Tab!", vbOKOnly, _
            "Xliff files mask missing"
            
            Exit Function
        
        End If
        
    End If
    
End If

' retrieve local language unit code and replace placehoders
Dim ul As String
ul = Get_ULg        ' unit language, also set in prefs settings form

storedXliffMaskString = Replace(storedXliffMaskString, "%lg%", ul)
storedXliffMaskString = Replace(storedXliffMaskString, "%LG%", UCase(ul))

Debug.Print "Retrieved xliff files mask as: " & storedXliffMaskString

getXliffFilesMask = storedXliffMaskString


End Function


'/**************************************************************************************************************************************************
'* Function retrieves the file mask for identifying (using a command similar with Windows' �dir� command) files to copy.
'* This function retrieves export for external review files path (relative to host "Project folder" path). It uses the SettingsFile class to do so.
'*/
Function getReviewFileMask() As String
' Retrieves and returns option �ReviewFilesCopyMask� from section "GSC_A3_Options"

Dim gro_Sfile As New SettingsFileClass

Dim storedReviewMaskString As String


If Not gro_Sfile.Exists Then
    
    storedReviewMaskString = frmgscroopt.tbReviewFilesMask.Text
    
    If storedReviewMaskString = "" Then
        MsgBox "Review files copy mask string not found on GSCA3 preferences form!" & vbCr & vbCr & "Please set it using the Preferences button in GSCA3 Tab!", vbOKOnly, _
            "Review files mask missing (Userform)"
        Exit Function
            
    End If
    
Else    ' Settings file exists
    
    storedReviewMaskString = gro_Sfile.getOption("ReviewFilesCopyMask", "GSC_A3_OptionsAdv")

    If storedReviewMaskString = "" Then     ' What if it did not find the setting?
        
        storedReviewMaskString = frmgscroopt.tbReviewFilesMask.Text
        
        If storedReviewMaskString = "" Then
            MsgBox "Review files copy mask string not found in settings file!" & vbCr & vbCr & "Please set it using the Preferences button in GSCA3 Tab!", vbOKOnly, _
                "Review files mask missing"
            Exit Function
        End If
        
    End If
    
End If


' retrieve local language unit code and replace placehoders
Dim ul As String
ul = Get_ULg        ' unit language, also set in prefs settings form

storedReviewMaskString = Replace(storedReviewMaskString, "%lg%", ul)
storedReviewMaskString = Replace(storedReviewMaskString, "%LG%", UCase(ul))

Debug.Print "Retrieved review files mask as: " & storedReviewMaskString

getReviewFileMask = storedReviewMaskString



End Function


Function buildGPath_fromFilename(FileName As String) As String
' documentName needs only be the actual file name, such as "st12345.en13.docx"
' if it returns empty string, it hasn't found corresponding path on G, or given document is not Council document

' check if Council document

Dim pBase As String
pBase = "G:\"

Dim pYear As String, pType As String, pIndex As String  'pYear - path year(i.e. 13), path type (i.e. st), path index (i.e. st05)

Dim fso As New Scripting.FileSystemObject


If Is_GSC_Doc(FileName) Then
    
    ' check if RO and exit if not
    If Not Is_ULg_Doc(FileName) Then
        
        buildGPath_fromFilename = ""
        StatusBar = "ArchiveG: " & FileName & " is NOT " & Get_ULg & ", Exiting..."
        Exit Function
        
    End If
    
    pYear = Right$(Split(FileName, ".")(1), 2)
    pType = Left$(Split(FileName, ".")(0), 2)
    pIndex = Left$(Split(FileName, ".")(0), 4)
    
    If fso.FolderExists(pBase & pYear & "\" & pType & "\" & pIndex) Then
        buildGPath_fromFilename = pBase & pYear & "\" & pType & "\" & pIndex
    Else
        Debug.Print "buildGPath_fromFilename: Folder " & pBase & pYear & "\" & pType & "\" & pIndex & " DOES NOT EXIST!"
        buildGPath_fromFilename = ""
    End If
    
Else
    buildGPath_fromFilename = ""
    StatusBar = "ArchiveG: " & FileName & " is not a Council document, Exiting..."
    Exit Function
End If


End Function


Function Get_Target_XLFPath(FileName As String) As String

Dim fso As New Scripting.FileSystemObject

Dim prf As String

prf = getProjectFolderOf(FileName)

If prf = "" Then
    Get_Target_XLFPath = ""
    Exit Function
End If

Dim origlgyear As String

origlgyear = Split(prf, ".")(1)

Dim ctlgyear As String

ctlgyear = Split(FileName, ".")(1)

Dim unitlg As String
'unitlg = sComputerName
'unitlg = LCase(Mid$(unitlg, 3, 2))
unitlg = Get_ULg


If fso.FileExists(prf & Languify(studioTargetFolder) & "\" & Replace(FileName, ctlgyear, origlgyear & "." & unitlg) & ".xlf") Then
    Get_Target_XLFPath = prf & Languify(studioTargetFolder) & "\" & Replace(FileName, ctlgyear, origlgyear & "." & unitlg) & ".xlf"
Else
    Get_Target_XLFPath = ""
End If


End Function


Function Get_Original_PrFXLFPath(FileName As String) As String

Dim fso As New Scripting.FileSystemObject

Dim prf As String

prf = getProjectFolderOf(FileName)

If prf = "" Then
    Get_Original_PrFXLFPath = ""
    Exit Function
End If

Dim origlgyear As String

origlgyear = Split(prf, ".")(1)

Dim ctlgyear As String

ctlgyear = Split(FileName, ".")(1)

Dim unitlg As String
'unitlg = sComputerName
'unitlg = LCase(Mid$(unitlg, 3, 2))
unitlg = Get_ULg


If fso.FileExists(prf & "\" & Replace(FileName, ctlgyear, origlgyear & "." & unitlg) & ".xlf") Then
    Get_Original_PrFXLFPath = prf & "\" & Replace(FileName, ctlgyear, origlgyear & "." & unitlg) & ".xlf"
Else
    Get_Original_PrFXLFPath = ""
End If


End Function


Function getProjectToOpenString(ByRef ProjectToOpenRS As String) As Boolean
' Returns empty string if T project folder does not exist for inputted GSC document ID String
' Returns project folder name found on T:\Prepared originals. Pops up a message box

Dim tempS As String

'tempS = InputBox("Please provide Council document ID String for project folder to open", "Open Which Project Folder?", _
    IIf(UBound(Split(latestOpenedDocs, ",")) <> -1, Split(latestOpenedDocs, ",")(UBound(Split(latestOpenedDocs, ","))), "ST 12345/13 REV 1 EN"))



If UBound(Split(latestOpenedDocs, ",")) <> -1 Then

    tempS = InputBox("Please provide Council document ID String for project folder to open, up to second dot and original's language, if different from �EN�", _
        "Open Which Project Folder?", Split(latestOpenedDocs, ",")(UBound(Split(latestOpenedDocs, ","))))
Else
    
    tempS = InputBox("Please provide Council document ID String for project folder to open, up to second dot and original's language, if different from �EN�", _
        "Open Which Project Folder?", "ST 12345/13 REV 1 EN")
    
End If



If Not IsValidLangIso(Right$(tempS, 2)) Then
    tempS = Trim(tempS) & " EN"
End If

'*****************************************************************************************************************
    'doctogetS = InputBox("Please provide original language document standard name to finalise, up to second dot and original's language, if different from �EN�", _
        "Finalise which document", IIf(UBound(Split(latestOpenedDocs, ",")) <> -1, Split(latestOpenedDocs, ",")(UBound(Split(latestOpenedDocs, ","))), "ST 12345/13 REV 1 EN"))
    
    ' correct for user not having supplied original's language, guess for english
    'If Not IsValidLangIso(Right$(doctogetS, 2)) Then
        'doctogetS = Trim(doctogetS) & " EN"
    'End If
        
    'doctoGetF = GSC_StandardName_WithLang_toFileName(doctogetS)
    
'    If Not Is_GSC_Doc(doctoGetF) Then
'        StatusBar = "Supplied string NOT valid GSC document name and language string, Please retry!"
'        Exit Sub
'    Else
'        If InStr(1, latestOpenedDocs, doctogetS) = 0 Then
'            latestOpenedDocs = latestOpenedDocs & "," & doctogetS
'        End If
'    End If
'*****************************************************************************************************************

' Checks, make sure user supplied some string and
If tempS = "" Then
    StatusBar = "Open Project Folder: Empty string supplied, Nothing to do, Exiting..."
    
    ProjectToOpenRS = ""
    getProjectToOpenString = False
    Exit Function
Else    ' make sure supplied string is consistent with requirements
    If Not Is_GSC_Doc(GSC_StandardName_WithLang_toFileName(tempS)) Then
        StatusBar = "Open Project Folder: Supplied string is NOT Standard Council document ID, please observe provided string example!"
        
        ProjectToOpenRS = ""
        getProjectToOpenString = False
        Exit Function
    Else
        If InStr(1, latestOpenedDocs, tempS) = 0 Then
            latestOpenedDocs = latestOpenedDocs & IIf(latestOpenedDocs <> "", ",", "") & tempS
        End If
    End If
End If


Dim projectFolderName As String
projectFolderName = getGSCBaseName(GSC_StandardName_WithLang_toFileName(tempS))  ' returned string has "xx" for language code
' but we need replace those with question marks, to act as a mask when "Dir"-ing in
'projectFolderName = Replace(projectFolderName, "xx", "??")

Dim projectFolderReturned As String
projectFolderReturned = Dir(ProjectsBaseFolder & projectFolderName, vbDirectory)

' Now we know supplied string is good, lets see if there's a project folder for that document
If projectFolderReturned = "" Then
    MsgBox "Open Project Folder failed to find a folder matching " & projectFolderName & " in " & _
        ProjectsBaseFolder, vbOKOnly + vbInformation, "NO Project Folder found!"

        ProjectToOpenRS = ""
        getProjectToOpenString = False
        Exit Function
Else
    ProjectToOpenRS = projectFolderReturned
    getProjectToOpenString = True
End If


End Function

Sub openProjectFolder(Optional ProjectToOpen)
' vers 0.45

Dim fso As New Scripting.FileSystemObject
Dim fileBaseName As String
Dim langBaseName As String

Dim ProjectToOpenS As String    ' file name type string, such as "sn01234-re.en13", ev with extension
Dim ProjectToOpenTry As String  ' String returned from user's input

If Not IsMissing(ProjectToOpen) Then
    If Not Is_GSC_Doc(CStr(ProjectToOpen)) Then
        If Is_GSC_Doc(GSC_StandardName_WithLang_toFileName(CStr(ProjectToOpen))) Then ProjectToOpenS = GSC_StandardName_WithLang_toFileName(CStr(ProjectToOpen))
    Else
        ProjectToOpenS = ProjectToOpen
    End If
Else
    If Documents.count > 0 Then
        If Is_GSC_Doc(ActiveDocument.Name) Then
            ProjectToOpenS = ActiveDocument.Name
        Else
            
NoDocOpened:
            ' Had an opened doc, was not GSC. Ask user.
            If getProjectToOpenString(ProjectToOpenTry) Then
                ProjectToOpenS = ProjectToOpenTry
            Else    ' But if user fails to provide correct string, warn and exit
                StatusBar = "Open Project Folder: Please provide proper project folder string! Project folder not found! Exiting..."
                Exit Sub
            End If
        End If
    Else
        GoTo NoDocOpened    ' same coding. Could've put it in a function, I guess. Lazy bum !
    End If
        
End If


' go forward only if there's a current document, exit if not
'If Documents.count > 0 Then
    
' check if current document is Council named, exit if not
If Is_GSC_Doc(ProjectToOpenS) Then
    
    If Right(ProjectToOpenS, 4) = ".doc" Or Right(ProjectToOpenS, 5) = ".docx" Then
        ' if so, get its base name (no extension)
        fileBaseName = fso.GetBaseName(ProjectToOpenS)
    Else
        fileBaseName = ProjectToOpenS
    End If
    
    ' and accomodate case of docs opened with gsc menu's "OpenDoc" (which have a second extension of ".COPY")
    If LCase(Right(fileBaseName, 5)) = ".copy" Then
        fileBaseName = fso.GetBaseName(fileBaseName)
    End If
    ' however, if user unwisely (but humanly) used macro from a Romanian document - we all have ! - accomodate it,
    ' switch language to EN (guess.. better than nothing!)
    If Is_ULg_Doc(ProjectToOpenS) Then
        fileBaseName = Replace(fileBaseName, "." & Get_ULg, ".en")
    End If
    
    ' now we have base name of current project. Check existence of such folder in unit's project folders base folder
    If fso.FolderExists(ProjectsBaseFolder & fileBaseName) Then
        StatusBar = "Open Project Folder: Success! Project folder found! Opening..."
        
        If fso.FolderExists(ProjectsBaseFolder & fileBaseName & Languify(studioTargetFolder)) Then
            Call Shell("explorer.exe" & " " & ProjectsBaseFolder & fileBaseName & Languify(studioTargetFolder), vbNormalFocus)
        Else
            Call Shell("explorer.exe" & " " & ProjectsBaseFolder & fileBaseName, vbNormalFocus)
        End If
        
    Else
        ' what about documents with original language other than en or fr?
        ' we're probably gonna find "en" folder instead, for sure! Let's handle it!
        langBaseName = Left$(Split(fileBaseName, ".")(1), 2)
        If InStr(1, "enfr", langBaseName) = 0 Then
            If fso.FolderExists(ProjectsBaseFolder & Replace(fileBaseName, "." & langBaseName, ".en")) Then
                fileBaseName = Replace(fileBaseName, "." & langBaseName, ".en")
                
                StatusBar = "Open Project Folder: " & fileBaseName & " not found, opening EN folder!"
                Call Shell("explorer.exe" & " " & ProjectsBaseFolder & fileBaseName, vbNormalFocus)
            End If
        Else
            ' project folder not found, but it's en or fr, so... signal and exit!
            StatusBar = "Open Project Folder: Project folder " & fileBaseName & " not found!, Exiting..."
            Exit Sub
        End If
    End If
    
    
Else
    StatusBar = "Open Project Folder: Current Document NOT Council, Exiting..."
    Exit Sub
End If
    
'Else
    'StatusBar = "Open Project Folder: No current document, Exiting..."
    'Exit Sub
'End If

End Sub


Sub TryNewClass()


Dim mySettingsFile As New SettingsFileClass

Debug.Print "Checking whether settings file Exists.. " & mySettingsFile.Exists
Debug.Print "Retrieving settings file Path.. " & mySettingsFile.Path


If Not mySettingsFile.Exists Then
        
    Debug.Print "Settings file missing, Creating..."
    
    mySettingsFile.Create_Empty
    
    Debug.Print "Now Exists = " & mySettingsFile.Exists
    Debug.Print "And Path = " & mySettingsFile.Path
    Debug.Print "Checking whether settings file is empty..." & mySettingsFile.IsEmpty
    
    mySettingsFile.Write_Defaults
    
Else
End If

End Sub


Sub TryNewClass_1()

Dim mySettingsFile As New SettingsFileClass

Debug.Print "Checking if settings file is empty... " & mySettingsFile.IsEmpty

Debug.Print "Writing default settings..."

Load frmgscroopt
mySettingsFile.Update_SettingsFile

Debug.Print "Getting content of settings file... " & mySettingsFile.Content

End Sub


Sub test_gscoptsForm()

frmgscroopt.Show

End Sub

Sub Run_CopieFootere()

frm_CopieFootere.Show

End Sub

'Sub CopieFootere()
'' version 0.95
'' main routine
''
'' 0.87 - Modificat rutina sa foloseasca numai obiectul range, in locul lui "selection", pentru a nu produce defilarea
'' rapida a documentului, ne-necesara.
'' 0.88 - Modificat sa noteze locatia bookmarkului "Init" (initiale), daca exista in footer si sa-l refaca dupa copiere, in footerul primei sectiuni
'' 0.90 - Remediere eroare de a copia mai multe rinduri din paragraful care contine "Anexa...", la inceputul fiecarei sectiuni,
'' in cazul in care exista "Shift+Enter", adica "LineFeed" (Chr(11)) in cadrul acestuia
'' 0.92 - Modificata pozitionarea ultimului tab, gresita. Legat footerul unei sectiuni de cel anterior si re-dezlegat, daca este sectiunea >=2,
'' si primul paragraf nu contine "anexa*". Produce footerele cu indicatia corecta, chiar si daca sint section-breakuri in cadrul unei anexe. :)
'' 0.93 - "Captuseste" cu indent (left sau right) paragrafele din componenta footerelor, daca marginile stg sau drpt sint mai mici de 2cm.
'' Muta taburile corespunzator daca modificam marginea stanga.
'' 0.935 - Nu aplica indent daca trebuie sa aplici negativ ! (vezi nota de mai sus)
'' 0.95 - Versiune post migrare, reimplementare. Vom schimba
'
'Dim mRng As Range
'Dim cfTrng As Range
'Dim lfIdx As Integer
'
'' Capture sections number in variable s
'frm_CopieFootere.Hide
'If ActiveDocument.Sections.count >= 2 Then
'    s = ActiveDocument.Sections.count
'Else
'    MsgBox "Documentul are decat o singura sectiune, Uitatu-v-ati daca footerul e corect?", , "Decat Una!": Exit Sub
'End If
'
'' If first section was indicated as source, link all sections' footers to previous and then unlink
'If frm_CopieFootere.OptionButton1.Value = True Then
'
'LinkM:
'    For i = 2 To s
'        ActiveDocument.Sections(i).Footers(wdHeaderFooterPrimary).LinkToPrevious = True
'    Next i
'
'    For i = 2 To s
'        ActiveDocument.Sections(i).Footers(wdHeaderFooterPrimary).LinkToPrevious = False
'    Next i
'
'Else    ' However, if last section's footer is source, we need to copy last section's footer to the first and then redo the above
'
'    Dim ls_rng As Range ' last section's footer range
'    Set ls_rng = ActiveDocument.Sections(s).Footers(wdHeaderFooterPrimary).Range    ' set range to last section footer
'
'    ls_rng.Copy     ' which content we copy
'    ActiveDocument.Sections(1).Footers(wdHeaderFooterPrimary).Range.Paste   ' and paste to first section
'
'    ' and unfortunatelly we also need to delete the last empty paragraph which also results extra...
'    Set cfTrng = ActiveDocument.Sections(1).Footers(wdHeaderFooterPrimary).Range
'    cfTrng.Collapse direction:=wdCollapseEnd
'    cfTrng.Delete wdCharacter, -1
'    Set cfTrng = Nothing
'
'    ' But now we are free to again link and unlink all sections, as above
'    ' So, Link
'    For i = 2 To s - 1
'        ActiveDocument.Sections(i).Footers(wdHeaderFooterPrimary).LinkToPrevious = True
'    Next i
'    ' and Unlink
'    For i = 2 To s - 1
'        ActiveDocument.Sections(i).Footers(wdHeaderFooterPrimary).LinkToPrevious = False
'    Next i
'End If
'
'' Now we switch to print layout view
'ActiveWindow.View.Type = wdPrintView
'ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument  ' and we go to main body
'ActiveWindow.View.Type = wdPrintView
'' and set a range to first character of document
'Set mRng = ActiveDocument.Range(0, 0)
'
'' We now loop throug all sections from the second to the last
'For i = 1 To s - 1
'    ' So, we skip from the first character of document to next section, thus getting second section's first char
'    ' We then expand to first paragraph. We are looking to see whether these sections contain "Anexa" and other similar
'    ' as one the firt paragraphs
'    Set mRng = mRng.GoToNext(wdGoToSection)
'    mRng.Expand wdParagraph
'    'mRng.Select
'
'        ' Having identified the first paragraph of second section, we skip the empty ones as many times as it takes
'isO:    If StrComp(mRng.Text, vbCr) = 0 Or StrComp(mRng.Text, Chr(11)) = 0 Then
'            mRng.MoveEndUntil vbCr
'            mRng.MoveEnd wdCharacter, 1
'            mRng.MoveStart wdCharacter, 1
'            'mRng.Select
'            GoTo isO
'        Else    ' else we exclude the last char from out range, we found the first non-empty char
'            mRng.MoveEnd wdCharacter, -1
'            'mRng.Select
'        End If
'
'    ' Having identified it, we capture its text
'    mTxt = LCase(mRng.Text)
'    'MsgBox "mTxt = " & mTxt    ' debugging
'
'    ' If paragraph in question spans multiple lines (contains "linefeed"), we retain first line only
'    lfIdx = InStr(1, mTxt, Chr(11)) ' Pozitia lui "shift+enter" in string
'    If lfIdx > 0 Then
'            mTxt = Left(mTxt, (lfIdx - 1))
'    End If
'
'        ' Now we compare the text of the first paragraph (or the first line of it) to our desired strings in RO
'        If mTxt Like "anex?" Or mTxt Like "anexa #" Or mTxt Like "anexa #*" Or mTxt Like "anexa ##" _
'            Or mTxt Like "anex? la anex?*" Or mTxt Like "anex?*" Then       ' These are all identified necessary cases
'
'            'mRng.Copy
'            ' Having found Annex or such in this section, we need to set a range to the proper place in the footer of it
'            Set mRng = ActiveDocument.Sections(i + 1).Footers(wdHeaderFooterPrimary).Range
'            mRng.Collapse wdCollapseStart
'            mRng.Move wdParagraph, 2
'            nrm = mRng.MoveEndUntil(vbTab)
'            'mRng.Select
'
'            ' We then check to determine if the second paragraph of footer does NOT already contain the needed text
'            If Len(nrm) > 4 Then
'                If LCase(mRng.Text) <> mTxt Then
'                    mRng.Collapse (wdCollapseStart)
'                    mRng.Delete wdCharacter, nrm
'                    mRng.InsertAfter UCase(mTxt)
'                    mTxt = ""
'                    mRng.Move wdParagraph, -2
'                End If
'            Else    ' If it doesn't, we simply insert copied text from first non-empty par
'                'mRng.PasteSpecial DataType:=wdPasteText
'                'mRng.Delete
'                mRng.Collapse (wdCollapseStart)
'                mRng.InsertAfter UCase(mTxt)
'                mTxt = ""
'                mRng.Move wdParagraph, -2
'            End If
'        Else
'            ' Daca primul paragraf nu contine "anex*" in sectiunea curenta, nu lasam gol footerul acesteia
'            ' - cel mai probabil trebuie legat de al sectiunii precedente!
'            ActiveDocument.Sections(i + 1).Footers(wdHeaderFooterPrimary).LinkToPrevious = True
'            ActiveDocument.Sections(i + 1).Footers(wdHeaderFooterPrimary).LinkToPrevious = False
'        End If
'Next i
'
'' For all sections from second on, if they're on landscape, we move the tabs
'' If they have non-standard left-right white margins, we compensate with left-right indent, so that white space should result at standard 2cm; we then re-align tabs
'Dim par1 As Paragraph, par2 As Paragraph
'For i = 1 To s - 1  ' looping through all sections from second onward
'
'    With ActiveDocument.Sections(i + 1).PageSetup
'
'        ' Compensam reducerea marginii din dreapta a paginii cu adaugarea aceleiasi dimensiuni de "right indent" la footer,
'        ' dar numai daca rezultatul este pozitiv (nu dorim sa aplicam indent negativ)
'        If CentimetersToPoints(2) - .RightMargin > 0 Then
'            .Parent.Footers(wdHeaderFooterPrimary).Range.ParagraphFormat.RightIndent = _
'                CentimetersToPoints(2) - .RightMargin
'        End If
'
'        Set par1 = .Parent.Footers(wdHeaderFooterPrimary).Range.Paragraphs(2)
'        Set par2 = .Parent.Footers(wdHeaderFooterPrimary).Range.Paragraphs(3)
'
'        ' Incercam sa facem la fel pentru marginea stanga, dar aici trebuie de asemenea recalculate pozitiile taburilor
'        If .LeftMargin < CentimetersToPoints(2) Then
'
'            ' Capturam diferenta in puncte pentru a o adauga ca left indent
'            lmd = CentimetersToPoints(2) - .LeftMargin
'
'            ' Adaugam intii left indent corespunzator, apoi restul
'            .Parent.Footers(wdHeaderFooterPrimary).Range.ParagraphFormat.LeftIndent = _
'            CentimetersToPoints(2) - .LeftMargin
'
'            ' Pozitiile taburilor au valori diferite, fct de orientarea paginii
'            If .Orientation = wdOrientPortrait Then
'
'                With par1.TabStops
'                    .ClearAll
'                    .Add CentimetersToPoints(8.5) + lmd, wdAlignTabCenter
'                    .Add CentimetersToPoints(13) + lmd, wdAlignTabCenter
'                    .Add CentimetersToPoints(17) + lmd, wdAlignTabRight
'                End With
'
'                With par2.TabStops
'                    .ClearAll
'                    .Add CentimetersToPoints(8.5) + lmd, wdAlignTabCenter
'                    .Add CentimetersToPoints(17) + lmd, wdAlignTabRight
'                End With
'
'
'            ElseIf .Orientation = wdOrientLandscape Then
'
'                With par1.TabStops
'                    .ClearAll
'                    .Add CentimetersToPoints(12.75) + lmd, wdAlignTabCenter
'                    .Add CentimetersToPoints(21) + lmd, wdAlignTabCenter
'                    .Add CentimetersToPoints(25.7) + lmd, wdAlignTabRight
'                End With
'
'                With par2.TabStops
'                    .ClearAll
'                    .Add CentimetersToPoints(12.75) + lmd, wdAlignTabCenter
'                    .Add CentimetersToPoints(25.7) + lmd, wdAlignTabRight
'                End With
'
'            End If
'        Else    ' Daca marginea stanga a paginii este egala cu 2 (sau mai mare !?!), atunci setam taburile la valorile normale pt landscape
'            If .Orientation = wdOrientLandscape Then
'                With par1.TabStops
'                    .ClearAll
'                    .Add CentimetersToPoints(12.75), wdAlignTabCenter
'                    .Add CentimetersToPoints(21), wdAlignTabCenter
'                    .Add CentimetersToPoints(25.7), wdAlignTabRight
'                End With
'
'                With par2.TabStops
'                    .ClearAll
'                    .Add CentimetersToPoints(12.75), wdAlignTabCenter
'                    .Add CentimetersToPoints(25.7), wdAlignTabRight
'                End With
'            End If
'
'        End If
'    End With
'Next i
'
'' Tabs realingnment finised, we again switch to print layout view and close any extra panes
'ActiveWindow.View.Type = wdPrintView
'ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument
'ActiveWindow.ActivePane.View.Type = wdPrintView
'ActiveWindow.ActivePane.View.Zoom.PageFit = wdPageFitFullPage
'' and move selection to first character of document
'Selection.HomeKey wdStory, wdMove
'
'End Sub


Sub test()

Dim sr As Range

For Each sr In ActiveDocument.StoryRanges
    Debug.Print sr.Characters.count
Next sr

End Sub

Sub testFindFraction()

Dim whichpart As Integer

whichpart = InputBox("Which part of doc do you want?", "Gimme")

Call FindFraction_ofTCDoc(whichpart)

End Sub

Sub FindFraction_ofTCDoc(DocumentFraction As Integer)

Dim t1

t1 = timer

Dim tcid As Long
tcid = ActiveDocument.ComputeStatistics(wdStatisticCharacters, True)

Dim quarterOftcid As Long
quarterOftcid = Round(tcid / DocumentFraction)

Dim fr As Range

Dim tcinSel As Long


Dim np As Integer
np = ActiveDocument.ComputeStatistics(wdStatisticPages)


Set fr = ActiveDocument.Range.GoTo(wdGoToPage, wdGoToAbsolute, Int(np / DocumentFraction))
fr.StartOf wdStory, wdExtend

Dim k As Integer

If fr.ComputeStatistics(wdStatisticCharacters) > Round(tcid / DocumentFraction) Then
    k = -1
Else
    k = 1
End If

If k = 1 Then

    For i = Int(np / DocumentFraction) To np
        
        Set fr = ActiveDocument.Range.GoTo(wdGoToPage, wdGoToAbsolute, i)
        
        'fr.Select
        
        fr.StartOf wdStory, wdExtend
        
        'fr.Select
        
        tcinSel = fr.ComputeStatistics(wdStatisticCharacters)
        
        Dim tfnc As Long
        Dim tfn As Footnote
        
        If fr.Footnotes.count > 0 Then
            For j = 1 To fr.Footnotes.count
                Set tfn = fr.Footnotes(j)
                tfnc = tfnc + tfn.Range.ComputeStatistics(wdStatisticCharacters)
            Next j
            
            tcinSel = tcinSel + tfnc
            tfnc = 0
        End If
        
        If tcinSel > quarterOftcid Then
            fr.Collapse wdCollapseEnd
                    
            Dim t2
            t2 = timer
            
            'Debug.Print DocumentFraction & "th of document jumped"
            
            Debug.Print "Current document has " & tcid & " chars." & vbCrCr & "Surpassed a " & DocumentFraction & "th of total chars in doc (" & _
            quarterOftcid & ") at beginning of page " & fr.Information(wdActiveEndAdjustedPageNumber) & vbCr & vbCr & _
            "Found value was " & tcinSel
            Exit For
        End If
        
        tcinSel = 0
        
    Next i

Else
    
        For i = Int(np / DocumentFraction) To 1 Step k
        
        Set fr = ActiveDocument.Range.GoTo(wdGoToPage, wdGoToAbsolute, i)
        
        'fr.Select
        
        fr.StartOf wdStory, wdExtend
        
        'fr.Select
        
        tcinSel = fr.ComputeStatistics(wdStatisticCharacters)
        
        'Dim tfnc As Long
        'Dim tfn As Footnote
        
        If fr.Footnotes.count > 0 Then
            For j = 1 To fr.Footnotes.count
                Set tfn = fr.Footnotes(j)
                tfnc = tfnc + tfn.Range.ComputeStatistics(wdStatisticCharacters)
            Next j
            
            tcinSel = tcinSel + tfnc
            tfnc = 0
        End If
        
        If tcinSel > quarterOftcid Then
            fr.Collapse wdCollapseEnd
                    
            'Dim t2
            t2 = timer
            
            'Debug.Print DocumentFraction & "th of document jumped"
            
            Debug.Print "Current document has " & tcid & " chars." & vbCrCr & "Surpassed a " & DocumentFraction & "th of total chars in doc (" & _
            quarterOftcid & ") at beginning of page " & fr.Information(wdActiveEndAdjustedPageNumber) & vbCr & vbCr & _
            "Found value was " & tcinSel
            Exit For
        End If
        
        tcinSel = 0
        
    Next i
    
End If

fr.Select


Debug.Print "Procedure took " & t2 - t1 & " seconds"

End Sub



Function Open_XLF_PreviewURL(Optional documentID) As Integer
' version 0.9
' Adjusted PreviewURL extraction to fit changed meta structure on 24/01/2019. Damn changes...

Dim tempDoc As String

' ask for user input and act accordingly or process parameter
If IsMissing(documentID) Then

    If UBound(Split(latestOpenedDocs, ",")) <> -1 Then

        tempDoc = InputBox("Please provide original language document standard name to check, up to second dot and original's language, if different from ?EN?", _
            "Open URL for which xlf?", Split(latestOpenedDocs, ",")(UBound(Split(latestOpenedDocs, ","))))
    Else
    
        tempDoc = InputBox("Please provide original language document standard name to check, up to second dot and original's language, if different from ?EN?", _
            "Open URL for which xlf?", "ST 12345/13 REV 1 EN")
    
    End If
Else
    tempDoc = documentID
End If


' Correct for user not having supplied original's language, guess for english
If Not IsValidLangIso(Right$(tempDoc, 2)) Then
    tempDoc = Trim(tempDoc) & " EN"
End If


Dim docFileName As String
docFileName = GSC_StandardName_WithLang_toFileName(tempDoc)

' check validity of supplied string and retort
If Not Is_GSC_Doc(docFileName) Then
    StatusBar = "Supplied string NOT valid GSC document name and language string, Please retry!"
    Exit Function
Else
    If InStr(1, latestOpenedDocs, tempDoc) = 0 Then
        latestOpenedDocs = latestOpenedDocs & IIf(latestOpenedDocs <> "", ",", "") & tempDoc
    End If
End If


Dim full_OrigFilename As String

Dim projFdr As String

projFdr = getProjectFolderOf(docFileName)
    

' We found the project folder of requested file
If Not projFdr = "" Then
    full_OrigFilename = Get_Original_PrFXLFPath(docFileName)
    
    Dim xlfFilename As String
    xlfFilename = Split(full_OrigFilename, "\")(3)
Else
    StatusBar = "Open_XLFLink: Could not find project folder of " & docFileName
    Open_XLF_PreviewURL = 1     ' ERROR CODE - Project folder retrieval function returned empty string (getProjectFolderOf function)
    Exit Function
End If
    

' Now that we found the xlf original, we copy and open it to get its xml data...
If Not full_OrigFilename = "" Then
    Dim fso As New Scripting.FileSystemObject
    fso.CopyFile full_OrigFilename, Environ("Temp") & "\" & xlfFilename, True
Else
    StatusBar = "Open_XLFLink: Could not find xlf original file for " & docFileName
    Open_XLF_PreviewURL = 2     ' ERROR CODE - Could not retrieve original xlf file from project folder (Get_Original_PrFXLFPath function)
    Exit Function
End If


Dim xlf_preview_url As String

' If we succeeded to copy the original xlf, we copy it and open and extract the metadata
If fso.FileExists(Environ("Temp") & "\" & xlfFilename) Then
    
    Dim xlfts As TextStream
    Set xlfts = fso.OpenTextFile(Environ("Temp") & "\" & xlfFilename)
    
    Dim gscMetadataString As String
    gscMetadataString = getGSCMeta(xlfts)
    
    xlf_preview_url = XLF_Get_PreviewURL(gscMetadataString)
Else
    ' Copy file above did not succeed... what do?
    StatusBar = "Open_XLFLink: Could not copy file " & xlfFilename & " to " & Environ("Temp") & " folder..."
    Open_XLF_PreviewURL = 3     ' ERROR CODE - The copy of the xlf original file we are processing does not exist after copy attempt above ! (fso.CopyFile full_OrigFilename, Env... above)
    Exit Function
End If


' Check and act acc with found link or not...
If xlf_preview_url <> "" Then
    
    Dim chromefilename As String
    chromefilename = "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"
    
    Dim xlf_preview_url_en As String, xlf_preview_url_ro As String
    
    Dim fspos As Integer    ' first slash position in url
    
    ' get the position of first slash in preview url...
    If Left$(xlf_preview_url, 7) = "http://" Then
        fspos = InStr(8, xlf_preview_url, "/")
    Else
        fspos = InStr(1, xlf_preview_url, "/")
    End If
    
    ' lets rebuild the en and ro links
    If Not fspos = 0 Then
        xlf_preview_url_en = Left$(xlf_preview_url, fspos - 1) & "/" & Right$(xlf_preview_url, Len(xlf_preview_url) - fspos)                                              ' +7 because of re-accounting for http!
        xlf_preview_url_ro = Left$(xlf_preview_url, fspos - 1) & "/" & Replace(Right$(xlf_preview_url, Len(xlf_preview_url) - fspos), "language=en", "language=" & Get_ULg)       ' +7 because of re-accounting for http!
    Else    ' Even possible !?!?!
    End If
    
    
    ' Aaannnd... lauch browser with link !!!
    If Dir(chromefilename) <> "" Then
        
        If Not xlf_preview_url_en = "" And Not xlf_preview_url_ro = "" Then
            
            StatusBar = "Open_XLFLink: Computed 2 links, Found Chrome, Launching..."
            Open_XLF_PreviewURL = 0     ' SUCCESS CODE - Finally... success !!! Error-free return code!
            
            Call Shell(chromefilename & " " & xlf_preview_url_ro, vbNormalFocus)
            
            PauseForSeconds (5) ' atempting to tame Chrome for displaying both languages pages...
            Call Shell(chromefilename & " " & xlf_preview_url_en, vbNormalFocus)
            
        Else
            
            StatusBar = "Open_XLFLink: Extracted link, Found Chrome, Launching..."
            Open_XLF_PreviewURL = 0     ' SUCCESS CODE - Finally... success !!! Error-free return code!
            Call Shell(chromefilename & " " & xlf_preview_url, vbNormalFocus)
            
        End If
        
        
        
        
    Else    ' Could not find Chrome executable file... try Internet Explorer or ...Bust !
        If Dir("C:\Program Files (x86)\Internet Explorer\iexplore.exe") <> "" Then
            Open_XLF_PreviewURL = 0     ' SUCCESS CODE - Finally... success !!! Error-free return code!
            Call Shell("C:\Program Files (x86)\Internet Explorer\iexplore.exe " & xlf_preview_url, vbNormalFocus)
            StatusBar = "Open_XLFLink: Could not find Chrome, using IE... BUT The new Council site is shown correctly in Chrome..."
        Else
            Open_XLF_PreviewURL = 5     ' ERROR CODE - Chrome AND Internet Explorer paths not found at expected location...
            MsgBox "Internet explorer NOT fount at " & vbCr & vbCr & "C:\Program Files (x86)\Internet Explorer\iexplore.exe"
        End If
    End If
    
Else     ' Could not extract link from txt xlf file...
    
    Dim xlf_title  As String
    
    xlf_title = XLF_Get_TitleMeta(gscMetadataString)
    
    If xlf_title <> "" Then
        
        If InStr(1, LCase(xlf_title), "bundle") > 0 Then
            'MsgBox "XLF file " & xlfFilename & " is a Bundle, could not find any preview URL!", vbOKOnly + vbInformation, "Bundle XLF"
            Open_XLF_PreviewURL = 4     ' ERROR CODE - Not properly an error, but we could not extract a non-empty link from file, it's a bundle..
        Else
            'MsgBox "Could not find preview URL in " & xlfFilename & " and file is NOT a bundle!", vbOKOnly + vbExclamation
            Open_XLF_PreviewURL = 6     ' ERROR CODE - Could not find preview url meta, title is non-empty but also not "bundle" !
        End If
        
    Else
        MsgBox "Could not find preview URL or TITLE in " & xlfFilename & "!", vbOKOnly + vbExclamation, "Please check jobslip"
        Open_XLF_PreviewURL = 7     ' ERROR CODE - Preview URL String is empty and so it title meta string !
    End If
    
End If

Exit Function


'**********************************************************************************************************************************
If Err.Number <> 0 Then
    Debug.Print "Open_XLF_PreviewUrl: Unexpected error number " & Err.Number & " occured, with description " & vbCr & _
        Err.Description
    Err.Clear
    Open_XLF_PreviewURL = 8     ' ERROR CODE - Unknown error occurrance
End If


End Function


Function getGSCMeta(ITextStream As TextStream) As String


Dim ctLine As String
Dim gscMeta As String


Do While Not ITextStream.AtEndOfStream

    ctLine = ITextStream.ReadLine
    ctLine = Mid$(ctLine, 4, Len(ctLine) - 3)
    
    Do While Not InStr(1, ctLine, "</mda:metadata>") > 0
    
        gscMeta = IIf(gscMeta = "", ctLine, gscMeta & vbCr & ctLine)
            
        ctLine = ITextStream.ReadLine
            
    Loop
    
    If TrimNonAlphaNums(Replace(ctLine, "</mda:metadata>", "")) <> "" Then
        ctLine = "</mda:metadata>"
    End If
    
    gscMeta = gscMeta & vbCr & ctLine & vbCr & "</xliff>"
    getGSCMeta = gscMeta
        
    Exit Do

Loop


If InStr(1, gscMeta, "<mda:metadata") = 0 Or _
    InStr(1, gscMeta, "</mda:metadata>") = 0 Then
    
    getGSCMeta = ""
    
End If


End Function


Function XLF_Get_PreviewURL(XLFString As String) As String


Dim XLFMeta As New MSXML2.DOMDocument

XLFMeta.LoadXML (XLFString)


Dim XLFMetadataNodes As IXMLDOMNodeList

Dim gscMetaNode As IXMLDOMNode
Set gscMetaNode = XLFMeta.SelectSingleNode("//xliff/mda:metadata")


If Not gscMetaNode Is Nothing Then
    Set XLFMetadataNodes = gscMetaNode.ChildNodes
Else
    XLF_Get_PreviewURL = ""
End If


Dim GSCWorkflowNodes As IXMLDOMNodeList

If XLFMetadataNodes.length = 2 Then

    If XLFMetadataNodes.Item(0).Attributes(0).Text = "GSCWorkflow" Then
        Set GSCWorkflowNodes = XLFMetadataNodes.Item(0).ChildNodes
    ElseIf XLFMetadataNodes.Item(1).Attributes(0).Text = "GSCWorkflow" Then
        Set GSCWorkflowNodes = XLFMetadataNodes.Item(1).ChildNodes
    Else
        XLF_Get_PreviewURL = ""
        Exit Function
    End If

Else
    XLF_Get_PreviewURL = ""     ' Pro'ly old web file, still in production. Wont have many, I reckon.
    Exit Function
End If



Dim XLFMetaNode As IXMLDOMNode

If GSCWorkflowNodes.length > 0 Then
    For Each XLFMetaNode In GSCWorkflowNodes
        
        If XLFMetaNode.Attributes(0).Text = "PreviewUrl" Then
            XLF_Get_PreviewURL = XLFMetaNode.Text
            Exit For
        End If
        
    Next XLFMetaNode
Else
    XLF_Get_PreviewURL = ""
End If

End Function


Function XLF_Get_TitleMeta(XLFString As String) As String


Dim XLFMeta As New MSXML2.DOMDocument

XLFMeta.LoadXML (XLFString)


Dim XLFMetadataNodes As IXMLDOMNodeList

Dim gscMetaNode As IXMLDOMNode
Set gscMetaNode = XLFMeta.SelectSingleNode("//xliff/gsc:MetaData")


If Not gscMetaNode Is Nothing Then
    Set XLFMetadataNodes = gscMetaNode.ChildNodes
Else
    XLF_Get_TitleMeta = ""
End If


Dim XLFMetaNode As IXMLDOMNode

If XLFMetadataNodes.length > 0 Then
    For Each XLFMetaNode In XLFMetadataNodes
        
        If XLFMetaNode.BaseName = "Title" Then
            XLF_Get_TitleMeta = XLFMetaNode.Text
            Exit For
        End If
        
    Next XLFMetaNode
Else
    XLF_Get_TitleMeta = ""
End If


End Function



Sub tmxFinderCall(baseFolder As String, tmxfilename As String, maxtimer As Long, timerinterval As Long)

Dim tmxFinderObj As Office.COMAddIn
Dim autoObj As Object

Set tmxFinderObj = Application.COMAddIns("tmxFinder")
Set autoObj = tmxFinderObj.Object

autoObj.RunCheckTmxExistence baseFolder, tmxfilename, maxtimer, timerinterval


End Sub


Function copyToWebOut(Optional documentID) As Integer
' returns 0 for succes of operation.
' 1 or more for errors


Dim webOutFolder As String
webOutFolder = "T:\TMs - Export\Web Out"

If Documents.count = 0 Then

StillEmpty:

    Dim tempDoc As String
    
    If IsMissing(documentID) Then
    
        If UBound(Split(latestOpenedDocs, ",")) <> -1 Then
    
            tempDoc = InputBox("Please provide original language document standard name to copy, up to second dot and original's language, if different from �EN�", _
                "Copy which xlf to Webout?", Split(latestOpenedDocs, ",")(UBound(Split(latestOpenedDocs, ","))))
        Else
        
            tempDoc = InputBox("Please provide original language document standard name to copy, up to second dot and original's language, if different from �EN�", _
                "Copy which xlf to Webout?", "ST 12345/13 REV 1 EN")
        
        End If
    Else
        tempDoc = documentID
    End If
    
    ' Correct for user not having supplied original's language, guess for english
    If Not IsValidLangIso(Right$(tempDoc, 2)) Then
        tempDoc = Trim(tempDoc) & " EN"
    End If
    
    
    Dim targetXLFFilename As String
    targetXLFFilename = GSC_StandardName_WithLang_toFileName(tempDoc)
    
    If Not targetXLFFilename = "" Then
    
        Dim targetXLFFilePath As String
        targetXLFFilePath = Get_Target_XLFPath(targetXLFFilename)
    
    
        If Not targetXLFFilePath = "" Then
            
            Dim tgXLFFullFilename As String
            tgXLFFullFilename = Split(targetXLFFilePath, "\")(UBound(Split(targetXLFFilePath, "\")))
            
            Dim fso As New Scripting.FileSystemObject
            fso.CopyFile targetXLFFilePath, webOutFolder & "\" & tgXLFFullFilename
            
            If Dir(webOutFolder & "\" & tgXLFFullFilename) <> "" Then
                copyToWebOut = 0
            Else
                copyToWebOut = 1    ' file not copied
            End If
            
        Else    ' target xlf file not found or other full target file path retrieval error, see function Get_Target_XLFPath
            copyToWebOut = 3
        End If
    
    Else    ' xlf human format doc id to file name conversion failed...
        copyToWebOut = 2
        
    End If
    
Else
    
    If Right(ActiveDocument.Name, 3) = "dot" Or Right(ActiveDocument.Name, 4) = "dotm" Or _
            ActiveDocument.Name Like "Document#" Or ActiveDocument.Name Like "Document##" Then
        
        GoTo StillEmpty
    
    Else
        copyToWebOut = 4    ' we had opened file, but was not ignorable file (template or non-saved doc). We do not run thus, xlfs cannnot be opened in word
    End If

    

End If


End Function


Sub TestSomeMem()

Dim myMem As Long

myMem = GlobalAlloc(GMEM_FIXED, CLng(4))

SourceAddress = myMem


Debug.Print "I've allocated memory in myMem at " & myMem

Dim ltest As Long

ltest = CLng(InputBox("Gimme"))

Call CopyMemory(ByVal myMem, ltest, 4)

Debug.Print "Now myMem is " & myMem

Call Display_MemValue

di& = GlobalUnlock(myMem)

End Sub


Sub Display_MemValue()


Dim valueTest As Long

If SourceAddress <> 0 Then
    Call CopyMemory(valueTest, ByVal SourceAddress, 4)
Else
    Debug.Print "Global memory block had already been freed!"
    Exit Sub
End If

Debug.Print "Reading value at " & SourceAddress & ", which is: " & valueTest

If MsgBox("Wanna clear the memory block?", vbYesNo) = vbYes Then
    SourceAddress = GlobalFree(CLng(SourceAddress))
End If

End Sub



Function bADir(maskString As String, Optional listFolders As VbFileAttribute) As foundFilesStruct    ' better Dir function
' A BETTER mouse trap - built my Tom Cat - no, just joking. It's built by badkatro.. no, just joking.
' IT's a better dir function, God damn it! The windows Dir is a joke !

' maskString NEEDS a base folder, completely qualified path, drive included.
' (We need to feed this to filesystem object's "" method

' maskString supports the following jokers: * -> for zero or more characters,
'                                           ? -> for one alpha character,
'                                           # -> for one numeric character

' listFolders parameter if a constant of type VBFileAttribute, but we only implement "vbDirectory" (or the lack of the parameter for files)

Dim fso As New Scripting.FileSystemObject

' If supplied string is not fully qualified path, we return empty and quit
If InStr(1, maskString, "\") = 0 Then
    bDir = ""
    Exit Function
End If

' Now, in maskString, we expect the last part of it (separated by backslash) to be either the name of a folder (or a mask for folder names) or
' a name of a file (or a mask for names of files). It's very unlikely we wish to use this function with something like "D:\foo*" as maskString,
' since we don't normally store files in the root of a drive (
Dim baseFolder As String
' lets separate the base folder, the one the user wishes to search into
baseFolder = Left$(maskString, InStrRev(Stringcheck:=maskString, Stringmatch:="\", Compare:=vbTextCompare) - 1)

' If basedir does not exist, we quit
If Dir(baseFolder, vbDirectory) = "" Then
    bDir = ""
    Exit Function
End If

' Is the user going to list subfolders or files?
If Not IsMissing(listFolders) Then
Else    ' Following part handling files searching part

End If



End Function


Sub testBDirAll_Count()

Dim res As foundFilesStruct

res = bDirAll("T:\Docs\margama\st062*")

Debug.Print res.count

End Sub

Sub testPrint_bDirAll()

Dim res As foundFilesStruct

res = bDirAll("T:\Docs\margama\st062*")

If res.count > 0 Then
    Debug.Print "Found " & res.count & " files matching " & "T:\Docs\margama\st062*:"
    For i = 1 To res.count
        Debug.Print res.files(i)
    Next i
End If

End Sub


Sub DW_Extract_AllMeta()

UserForm2.Show

End Sub




Sub AssignDocument()

'On Error GoTo ErrMain

frmAssign.Show

Exit Sub

'***************************
ErrMain:
    If Err.Number <> 0 Then
        MsgBox "Error no " & Err.Number & " occured in " & Err.Source & " with description: " & _
            Err.Description
        Err.Clear
    End If

End Sub


Sub AssignRevision()

' Let's load form first, we need some of its settings
Load frmAssign



Dim aInsp As Inspector

'Set aInsp = ActiveInspector

Dim aExp As Explorer

Set aExp = ActiveExplorer


Dim aMail As MailItem


If aExp.Selection.count = 1 Then
    
    If aExp.Selection.Item(1).Class = olMail Then
    
        Set aMail = aExp.Selection.Item(1)
    
    Else
        
        MsgBox "Please select an email if wishing to assign a revision with auto-translation fill-in, otherwise use <Assign Doc> button!", _
            vbOKOnly + vbExclamation, "No message selected"
        Exit Sub
        
    End If

Else

    MsgBox "Please select an email if wishing to assign a revision with auto-translation fill-in, otherwise use <Assign Doc> button!", _
        vbOKOnly + vbExclamation, "No message selected"
    Exit Sub
    
End If


Dim tradInfo(2) As String

Dim tradData As String  ' transl initials and combined deadline, as in the email

' cc to this person
tradInfo(0) = aMail.SenderName


Dim regexp As VBScript_RegExp_55.regexp
Set regexp = New VBScript_RegExp_55.regexp

regexp.IgnoreCase = True
regexp.Pattern = GSCDocNamePattern
regexp.Global = True


If regexp.test(aMail.Subject) Then
    Dim matS As VBScript_RegExp_55.MatchCollection
    Set matS = regexp.Execute(aMail.Subject)
    
    If matS.count > 1 Then
        
        'HERE, TODO: Prepare usage for multi-documents Revision assignments !
        
        Dim processDocsArr() As String
        
        Dim processDocs As String
        
        ReDim processDocsArr(matS.count - 1)
        
        ' more than one doc in subject, gather and process
        For i = 0 To UBound(processDocsArr)
            
            ' gather all docs into array to process
            processDocsArr(i) = Trim(matS.Item(i).Value)
                                    
            ' lets replace short suffixes by long ones
            If Contains_Short_Suffixes(processDocsArr(i)) Then
                processDocsArr(i) = GSCFileName_ToStandardName(GSC_StandardName_ToFileName(processDocsArr(i)))
            End If

        Next i
        
        processDocs = Join(processDocsArr, ", ")
        
        tradInfo(1) = processDocs
        
    Else
        If Contains_Short_Suffixes(matS(0)) Then
            'TODO: Finally build a proper function to convert between short version and long version suffixes, this here below is shamefull!
            tradInfo(1) = GSCFileName_ToStandardName(GSC_StandardName_ToFileName(matS(0)))
        Else
            tradInfo(1) = matS(0)
        End If
    End If
    
    
    
End If


Dim messageBody As Word.Document

Set messageBody = aMail.GetInspector.WordEditor


If messageBody.Bookmarks.count > 0 Then
    
    If messageBody.Bookmarks.Exists("tradInit") Then
        
        If messageBody.Bookmarks("tradInit").Range.Text <> "" Then
            
            tradData = messageBody.Bookmarks("tradInit").Range.Text & "|"
            ' and we extract translation deadline as well
            If messageBody.Bookmarks.Exists("tradDeadline") Then
                
                If messageBody.Bookmarks("tradDeadline").Range.Text <> "" Then
                    
                    
                    If frmAssign.chNoTradTimesInRev Then
                    
                        tradData = tradData & Trim(Split(messageBody.Bookmarks("tradDeadline").Range.Text, "-")(0))
                    
                    Else    ' lets try to accomodate manually added time (in email) as well (bookmark text incomplete)
                        
                        Dim emailTradDeadline As String
                        Dim emailTradDRange As Range
                        
                        Set emailTradDRange = messageBody.Bookmarks("tradDeadline").Range.Duplicate
                        emailTradDRange.Expand (wdCell)
                        emailTradDRange.MoveEndWhile vbCr & vbLf & Chr(7), wdBackward
                        
                        'tradData = tradData & messageBody.Bookmarks("tradDeadline").Range.Text
                        tradData = tradData & emailTradDRange.Text
                    
                    End If
                    
                End If
                
                tradInfo(2) = tradData
                
            End If
                    
        End If
                
    End If
    
End If


frmAssign.tbaDocuments = tradInfo(1)   ' set doc name
frmAssign.ccTo = tradInfo(0)           ' set cc destinatary
If tradInfo(2) <> "" Then
    frmAssign.traInitData = tradInfo(2)    ' set translation info
Else
    frmAssign.traInitData = "BogusTradInfo"
End If

'If frmAssign.chNoTradTimesInRev.Value = "False" Then
    ' cause we wanna get the trad time carried into rev mail
    'frmAssign.traDeadlineTime.Enabled = True
'End If

frmAssign.Show

End Sub



Sub CreateMessage_fromAssignTemplate()

Dim assTemplatePath As String

If Application.Name = "Outlook" Then
    assTemplatePath = Environ("Appdata") & "\Microsoft\Templates\" & MailTemplateRoot & "7.oft"
Else
    assTemplatePath = Environ("WordTemplates") & "\" & MailTemplateRoot & "7.oft"
End If

If Dir(assTemplatePath) <> "" Then
    Application.CreateItemFromTemplate(assTemplatePath).Display
Else
    MsgBox "Sorry, default Document Assignment template not found!" & vbCr & vbCr & _
        "(" & assTemplatePath & ")"
End If

End Sub


Sub Check_MissingLanguageSetting()

Dim storedLanguageSetting As String

Dim sFile As New SettingsFileClass


If Not sFile.Exists Then
    
    If MsgBox("This seems to be the first time starting the GSC A3 macros" & vbCr & vbCr & _
        "Please change language unit setting to your unit language ISO code", vbOKCancel, "Missing language code") <> vbOK Then

        Debug.Print "Missing language unit setting, Quitting..."

    End If
    
    frmgscroopt.Show
    
Else
    
    storedLanguageSetting = sFile.getOption("LanguageUnit", "GSC_A3_Options")

    If storedLanguageSetting = "XX" Or storedLanguageSetting = "" Then

        If MsgBox("This seems to be the first time starting the GSC A3 macros" & vbCr & vbCr & _
            "Please change language unit setting to your unit language ISO code", vbOKCancel, "Missing language code") <> vbOK Then

            Debug.Print "User canceled action, Quitting..."

        End If

        frmgscroopt.Show

    Else
        Debug.Print "Retrieved language setting: " & storedLanguageSetting
    End If

End If

End Sub


Sub Show_GSCA3_Options()

frmgscroopt.Show

End Sub


Sub FileSaveAs()


On Error GoTo ErrorSavingAsCustom

Dim fsa As FileDialog

Dim username As String
username = Environ("username")

If Documents.count > 0 Then
        
    Set fsa = Application.FileDialog(msoFileDialogSaveAs)
        
    If Is_GSC_Doc(ActiveDocument.Name, SilentMode) Then
        
        If Is_ULg_Doc(ActiveDocument.Name) Or _
            InStr(1, LCase$(ActiveDocument.Name), "final") > 0 Or _
            InStr(1, LCase$(ActiveDocument.Name), "merge") Then
                        
            StatusBar = "Custom Saving GSC document..."
                
            If username <> "" Then
                
                Dim wDir As String
                                
                If Dir("T:\Docs\" & username, vbDirectory) <> "" Then
                    wDir = "T:\Docs\" & username
                ElseIf Dir("D:\Docs", vbDirectory) <> "" Then
                    wDir = "D:\Docs"
                End If
                
                ChangeFileOpenDirectory (wDir)
                fsa.InitialView = msoFileDialogViewDetails
                    
                If InStr(1, LCase$(ActiveDocument.Name), "final") > 0 Or InStr(1, LCase$(ActiveDocument.Name), "merge") > 0 Then
                    Dim bname As String
                    Dim unitLang As String
                    
                    unitLang = Get_ULg      ' will also complain if not unit lang retrieval successfull
                    'unitLang = LCase(Mid$(Environ("computername"), 3, 2))
                    bname = getGSCBaseName(ActiveDocument.Name)
                    
                    fsa.InitialFileName = wDir & "\" & Replace(bname, Right$(bname, 4), unitLang & Right(bname, 2))
                    
                Else
                    fsa.InitialFileName = wDir & "\" & ActiveDocument.Name
                End If
                
            End If
        Else
            fsa.InitialFileName = ActiveDocument.Name
        End If
    Else
        fsa.InitialFileName = ActiveDocument.Name
    End If
        
    
    fsa.Show
    fsa.Execute
        
    
End If


Set fsa = Nothing

Exit Sub

'_______________________________________________________________________________________________
'
ErrorSavingAsCustom:
    If Err.Number <> 0 Then
        Debug.Print "Custom FileSaveAs: Error number " & Err.Number & " with description " & Err.Description & " occured in " & Err.Source
        Err.Clear
        
        StatusBar = "Error Custom Saving, Launching normal SaveAs..."
        
        If Documents.count > 0 Then
            Application.FileDialog(msoFileDialogSaveAs).Show
        End If
    
    End If

End Sub



Function mergeName(InitialName As String) As String

Dim tmpName As String
Dim blownName

blownName = Split(InitialName, ".")

blownName(2) = "merge"  ' ADD specific merging indication in the name

mergeName = Join(blownName, ".")

    
End Function


' Merge routine for Word documents, taking a file names (full path) array and creating new doc
' Function returns the number of total parts merged.
' Optional parameter MarkPartsSuture creates bookmarks at all suture points, called "MergeX-Y", where X & Y are first part num, second part num
Function MergeDocuments(ByRef InputDocuments, Optional MarkPartsSuture) As Integer

' Sanity check. We need array input
If Not IsArray(InputDocuments) Then
    StatusBar = "MergeDocuments: InputDocuments parameter must be an array of document paths! Exiting..."
    Exit Function
End If


Dim tmpCount As Integer     ' parts counter

Dim fso As New Scripting.FileSystemObject, ff As Scripting.File
Dim ctDoc As Document, ctRange As Range

Dim sortedInputDocuments
sortedInputDocuments = Sort_Array(InputDocuments)   ' CANNOT avoid sorting doc names array, Windows is serving files in a folder in order of 'found in disk sector' !


' Loop through file names, open first and insert all the others!
For i = 0 To UBound(InputDocuments)
        
    Set ff = fso.GetFile(InputDocuments(i))
    
    If i = 0 Then
    
        ' open first document
        Set ctDoc = Documents.Open(FileName:=ff.Path, AddToRecentFiles:=False)
        
        tmpCount = tmpCount + 1
    
    Else    ' but insert all other docs except first
        
        ' define insertion range
        Set ctRange = ctDoc.Paragraphs.Last.Range
        ctRange.Collapse (wdCollapseEnd)
        
        ' count current part
        tmpCount = tmpCount + 1
            
        ' bookmark merging point in doc, optional
        If Not IsMissing(MarkPartsSuture) Then
            ctRange.Bookmarks.Add ("Merge_" & tmpCount - 1 & "_" & tmpCount)
        End If
        
        ' include next file
        ctRange.InsertFile (ff.Path)
        
    End If
    
    ' and fix last paragraph, if it's style is "Final line", make it "Normal"
    If i = UBound(InputDocuments) Then
        ' Do not force a page break after last part !
        Call remove_LastParagraphs_FinalLine(ctDoc)
    Else
        Call remove_LastParagraphs_FinalLine(ctDoc, "ForcePageBreak")
    End If
        
Next i

' change name, confer safety to peoples
ctDoc.SaveAs2 FileName:=frmMultipleFiles.toSaveMergeName    ' full doc path name, including modded file name (adding "merge") and full preffered path

' report number of doc parts merged
MergeDocuments = tmpCount

End Function


Sub remove_LastParagraphs_FinalLine(TargetDocument As Document, Optional ForcePageBreak)

' remove final closing "Final Line" if it exists
    If TargetDocument.Paragraphs.Last.Style = "Final Line" Or TargetDocument.Paragraphs.Last.Style = "Ligne Final" Then
        TargetDocument.Paragraphs.Last.Style = "Normal"
        
        ' also insert page break at then end of doc, if user asked
        If Not IsMissing(ForcePageBreak) Then
            If Not isPageBreakParagraph(TargetDocument.Paragraphs.Last) Then
                TargetDocument.Paragraphs.Last.Range.InsertBefore (Chr(12)) ' force inserting of page break at end of part
            End If
        End If
    End If
    
End Sub
