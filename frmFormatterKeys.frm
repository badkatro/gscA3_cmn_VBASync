VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmFormatterKeys 
   OleObjectBlob   =   "frmFormatterKeys.frx":0000
   Caption         =   "Formatter Keyboard Shortcuts"
   ClientHeight    =   7965
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   4335
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
   TypeInfoVer     =   115
End
Attribute VB_Name = "frmFormatterKeys"
Attribute VB_Base = "0{A4E084C1-F234-4112-BE6E-EFA41319B588}{C8608616-77E6-49C1-B5A8-9091D61D7D18}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False






Private Sub lblKDMod1_Click()

' left mouse button click, cycle modifier values (Ctrl, Alt or Shift)
Call Cycle_Modifier_Value_of(Me.lblKDMod1)

' have to also update tag value of corr general label of section...
' that's where we compose string of Mod1+Mod2+Key !
Call Update_TagValue_of(Me.lblKDMod1)

End Sub

Private Sub lblKDMod1_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

' right mouse button, enable or disable current label modifier
If Button = 2 Then
    Call Toggle_Activation_State_of(Me.lblKDMod1)
    Call Update_TagValue_of(Me.lblKDMod1)
End If

End Sub


Sub Update_TagValue_ofTextbox(TargetTextbox As MSForms.TextBox)



Dim ctSectLabel As MSForms.label
Set ctSectLabel = Me.Controls("lbl" & Mid$(TargetTextbox.Name, 4, 2) & "Label")

Dim splitTag As Variant
splitTag = Split(ctSectLabel.Tag, "+")



' only do it if state of ct mod has not been changed to disabled!
If TargetTextbox.Text <> "" Then
    
    splitTag(2) = TargetTextbox.Text

Else    ' User just deleted key for ct action? Protest !
    
    splitTag(2) = "Null"
    
End If


ctSectLabel.Tag = Join(splitTag, "+")
    
' Debug
Debug.Print label; ctSectLabel.Name & " Tag = "; ctSectLabel.Tag

End Sub


Sub Update_TagValue_of(TargetLabel As MSForms.label)

' left mouse button mouse up event, update label tag with new value combo (Mod1 + Mod2 + Key)

Dim ctSectLabel As MSForms.label
Set ctSectLabel = Me.Controls("lbl" & Mid$(TargetLabel.Name, 4, 2) & "Label")

Dim splitTag As Variant
splitTag = Split(ctSectLabel.Tag, "+")

' only do it if state of ct mod has not been changed to disabled!
If TargetLabel.SpecialEffect = fmSpecialEffectSunken Then
    
    ' from which control are we calling? Mod1, Mod2 or Key?
    If InStr(1, TargetLabel.Name, "1") > 0 Then
        splitTag(0) = TargetLabel.Caption
    ElseIf InStr(1, TargetLabel.Name, "2") > 0 Then
        splitTag(1) = TargetLabel.Caption
    ElseIf InStr(1, TargetLabel.Name, "Key") > 0 Then
        splitTag(2) = TargetLabel.Caption
    End If

Else    ' current calling label (TargetLabel) is disabled!

    ' from which control are we calling? Mod1, Mod2 or Key?
    If InStr(1, TargetLabel.Name, "1") > 0 Then
        splitTag(0) = "Null"
    ElseIf InStr(1, TargetLabel.Name, "2") > 0 Then
        splitTag(1) = "Null"
    ElseIf InStr(1, TargetLabel.Name, "Key") > 0 Then
        splitTag(2) = "Null"
    End If
        
End If


ctSectLabel.Tag = Join(splitTag, "+")
    
' Debug
Debug.Print label; ctSectLabel.Name & " Tag = "; ctSectLabel.Tag


End Sub


Private Sub lblKDMod2_Click()

Call Cycle_Modifier_Value_of(Me.lblKDMod2)
' have to also update tag value of corr general label of section...
' that's where we compose string of Mod1+Mod2+Key !
Call Update_TagValue_of(Me.lblKDMod2)

End Sub


Private Sub lblKDMod2_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 2 Then
    Call Toggle_Activation_State_of(Me.lblKDMod2)
    Call Update_TagValue_of(Me.lblKDMod2)
End If

End Sub



Private Sub lblKIMod1_Click()

Call Cycle_Modifier_Value_of(Me.lblKIMod1)
' have to also update tag value of corr general label of section...
' that's where we compose string of Mod1+Mod2+Key !
Call Update_TagValue_of(Me.lblKIMod1)

End Sub

Private Sub lblKIMod1_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 2 Then
    Call Toggle_Activation_State_of(Me.lblKIMod1)
End If

End Sub


Private Sub lblKIMod2_Click()

Call Cycle_Modifier_Value_of(Me.lblKIMod2)
' have to also update tag value of corr general label of section...
' that's where we compose string of Mod1+Mod2+Key !
Call Update_TagValue_of(Me.lblKIMod2)

End Sub

Private Sub lblKIMod2_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 2 Then
    Call Toggle_Activation_State_of(Me.lblKIMod2)
End If

End Sub


Private Sub lblIDMod1_Click()

Call Cycle_Modifier_Value_of(Me.lblIDMod1)
Call Update_TagValue_of(Me.lblIDMod1)

End Sub

Private Sub lblIDMod1_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 2 Then
    Call Toggle_Activation_State_of(Me.lblIDMod1)
    Call Update_TagValue_of(Me.lblIDMod1)
End If

End Sub


Private Sub lblIDMod2_Click()

Call Cycle_Modifier_Value_of(Me.lblIDMod2)

End Sub

Private Sub lblIDMod2_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 2 Then
    Call Toggle_Activation_State_of(Me.lblIDMod2)
End If

End Sub


Private Sub lblIIMod1_Click()

Call Cycle_Modifier_Value_of(Me.lblIIMod1)
Call Update_TagValue_of(Me.lblIIMod1)

End Sub

Private Sub lblIIMod1_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 2 Then
    Call Toggle_Activation_State_of(Me.lblIIMod1)
    Call Update_TagValue_of(Me.lblIIMod1)
End If

End Sub

Private Sub lblIIMod2_Click()

Call Cycle_Modifier_Value_of(Me.lblIIMod2)
Call Update_TagValue_of(Me.lblIIMod2)

End Sub

Private Sub lblIIMod2_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

If Button = 2 Then
    Call Toggle_Activation_State_of(Me.lblIIMod2)
    Call Update_TagValue_of(Me.lblIIMod2)
End If

End Sub


Sub Cycle_Modifier_Value_of(TargetLabel As MSForms.label)

If TargetLabel.SpecialEffect = fmSpecialEffectSunken Then
    
    Select Case TargetLabel.Caption
        Case "Ctrl"
            TargetLabel.Caption = "Alt"
        Case "Alt"
            TargetLabel.Caption = "Shift"
        Case "Shift"
            TargetLabel.Caption = "Ctrl"
    End Select
    
    ' avoid double identic mod! ("Alt+Alt+a")
    If InStr(1, TargetLabel.Name, "2") > 0 Then
        
        If Me.Controls(Replace(TargetLabel.Name, "2", "1")).Caption = _
            TargetLabel.Caption Then
            
            Call Cycle_Modifier_Value_of(TargetLabel)
            
        End If
        
    End If
    
End If


End Sub


Sub Toggle_Activation_State_of(TargetLabel As MSForms.label)

If TargetLabel.SpecialEffect <> fmSpecialEffectSunken Then
    TargetLabel.SpecialEffect = fmSpecialEffectSunken
    TargetLabel.ForeColor = wdColorBlack
Else    ' need disable current mod. DONT if the other mod is disabled too !
    If InStr(1, TargetLabel.Name, "2") > 0 Then     ' only disable 2nd mod !
        TargetLabel.SpecialEffect = fmSpecialEffectEtched
        TargetLabel.ForeColor = wdColorGray15
    End If
End If


End Sub



Private Sub tboIDKey_Change()

Call Update_TagValue_ofTextbox(Me.tboIDKey)

End Sub

Private Sub tboIDKey_Exit(ByVal Cancel As MSForms.ReturnBoolean)

Call Check_Emptyness_of(Me.tboIDKey)

End Sub

Private Sub tboIIKey_Change()

Call Update_TagValue_ofTextbox(Me.tboIIKey)

End Sub

Private Sub tboIIKey_Exit(ByVal Cancel As MSForms.ReturnBoolean)

Call Check_Emptyness_of(Me.tboIIKey)

End Sub

Private Sub tboKDKey_Change()

Call Update_TagValue_ofTextbox(Me.tboKDKey)

End Sub


Private Sub tboKDKey_Exit(ByVal Cancel As MSForms.ReturnBoolean)

Call Check_Emptyness_of(Me.tboKDKey)

End Sub


Sub Check_Emptyness_of(TargetTextbox As MSForms.TextBox)

If TargetTextbox.Text = "" Then

    MsgBox "Current action key can NOT be empty! "
    
    If InStr(1, TargetTextbox.Name, "KD") > 0 Or _
        InStr(1, TargetTextbox.Name, "KI") > 0 Then
        TargetTextbox.Text = "a"
    Else
        TargetTextbox.Text = "y"
    End If
    
End If

End Sub


Private Sub tboKIKey_Change()

Call Update_TagValue_ofTextbox(Me.tboKIKey)

End Sub


Private Sub tboKIKey_Exit(ByVal Cancel As MSForms.ReturnBoolean)

Call Check_Emptyness_of(Me.tboKIKey)

End Sub


Private Sub UserForm_Initialize()

Call Sync_Form_ToSavedOptions

'CustomizationContext = ActiveDocument
'KeyBindings.Add KeyCode:=BuildKeyCode(wdKeyControl, _
'    wdKeyShift, wdKeyU), KeyCategory:=wdKeyCategoryMacro, _
'    Command:="Macro1"
'MsgBox KeyBindings.Key(BuildKeyCode(wdKeyControl, _
'    wdKeyShift, wdKeyU)).Command

End Sub


Sub Sync_Form_ToSavedOptions()



End Sub


Private Sub cmdSave_Click()



End Sub


Private Sub CommandButton1_Click()



End Sub
