Attribute VB_Name = "RUETitlesExtract"
Public Const studioTargetReviewFolder As String = "\Studio\External Review\%lg%-%LG%"
Private Const frmProjPruner_ClosedHeight = 114
Private Const frmProjPruner_ExpandedHeight = 154


Public Sub Master_RUETitles_Extract_Go()


Application.ScreenUpdating = False


Dim RootFolder As String

' browse to a folder of user choosing to search for GSC project folders in its subfolders
RootFolder = gscrolib.GetDirectory("Please select root folder")

' has user selected root folder?
If RootFolder = "" Then

    MsgBox "Have not selected any root folder, Quitting..."
    Exit Sub
    
End If


Dim src_tgt_filepairs As Variant

' collect all GSC project folders from provided root
src_tgt_filepairs = get_GSC_FilePairs(RootFolder)

If Not IsArray(src_tgt_filepairs) Then
    ' have we found any GSC folder in root?
    If src_tgt_filepairs = Empty Then
              
        MsgBox "Could not find any GSC project subfolders in " & RootFolder & ", Quitting..."
        Exit Sub
              
    End If
End If


Dim addedDocsNum

' open all files inventoried in 2D array and build a docx table with resulting titles and initials
addedDocsNum = Open_andExtract_Titles(src_tgt_filepairs)


Application.ScreenUpdating = True
Application.ScreenRefresh


MsgBox "Processed subject from " & UBound(src_tgt_filepairs) + 1 & " files, added " & addedDocsNum, vbInformation + vbOKOnly, "Subject extraction done"


Exit Sub


If Err.Number <> 0 Then
    MsgBox "Error " & Err.Number & " described: " & Err.Description & " in " & Err.Source
    Err.Clear
    Resume
End If


End Sub


Function get_DWMeta_fromDoc_OXML(TargetDocument As Document) As String

End Function



Function Open_andExtract_Titles(FilesToOpenArray) As Integer


Dim orDoc As Document, tgDoc As Document


Dim tmpAllDocsData() As String

Dim docData As String


For i = 0 To UBound(FilesToOpenArray)
    
    Set orDoc = Documents.Open(FileName:=FilesToOpenArray(i)(0))
    Set tgDoc = Documents.Open(FileName:=FilesToOpenArray(i)(1))
    
    docData = GSCFileName_ToStandardName(orDoc.Name, "SpacedSuffix")
    
    docData = docData & "|" & DW_Get_Subject(orDoc)
    docData = docData & "|" & DW_Get_Subject(tgDoc)
    
    docData = docData & "|" & Get_Project_Authors(getProjectFolderOf(getGSCBaseName(orDoc.Name)), "optimiseForInitials")
    
    ReDim Preserve tmpAllDocsData(i)
    tmpAllDocsData(i) = docData
    docData = ""
    
    orDoc.Close
    tgDoc.Close
    
Next i


Dim rowsWritten

rowsWritten = Create_DocsSubjectDocument(tmpAllDocsData)

Open_andExtract_Titles = rowsWritten


End Function


Function Create_DocsSubjectDocument(DocumentsData) As Integer       ' returning number of rows written


Dim resDoc As Document

Set resDoc = Documents.Add

resDoc.PageSetup.Orientation = wdOrientLandscape

resDoc.Tables.Add resDoc.StoryRanges(wdMainTextStory), 1, 4


With resDoc.Tables(1)
    
    .PreferredWidthType = wdPreferredWidthPercent
    .PreferredWidth = 100
    
    .Columns(1).PreferredWidthType = wdPreferredWidthPercent
    .Columns(2).PreferredWidthType = wdPreferredWidthPercent
    .Columns(3).PreferredWidthType = wdPreferredWidthPercent
    .Columns(4).PreferredWidthType = wdPreferredWidthPercent
    
    .Columns(1).PreferredWidth = 15
    .Columns(2).PreferredWidth = 37
    .Columns(3).PreferredWidth = 37
    .Columns(4).PreferredWidth = 11
    
    .Style = "Light Shading - Accent 1"
    .Range.ParagraphFormat.LineSpacingRule = wdLineSpaceSingle
    
End With


For i = 0 To UBound(DocumentsData)
    
    If i <> 0 Then resDoc.Tables(1).Rows.Add
    If DocumentsData(i) <> "" Then
        resDoc.Tables(1).Rows(resDoc.Tables(1).Rows.count).Cells(1).Range.Text = Split(DocumentsData(i), "|")(0)
        resDoc.Tables(1).Rows(resDoc.Tables(1).Rows.count).Cells(2).Range.Text = Split(DocumentsData(i), "|")(1)
        resDoc.Tables(1).Rows(resDoc.Tables(1).Rows.count).Cells(3).Range.Text = Split(DocumentsData(i), "|")(2)
        resDoc.Tables(1).Rows(resDoc.Tables(1).Rows.count).Cells(4).Range.Text = Split(DocumentsData(i), "|")(3)
    End If
    
Next i

resDoc.SpellingChecked = True


Create_DocsSubjectDocument = resDoc.Tables(1).Rows.count


End Function



Function get_GSC_FilePairs(RootFolder) As Variant


Dim gsc_Folders As Variant

gsc_Folders = get_GSC_Folders(CStr(RootFolder))


If Not IsArray(gsc_Folders) Then
    If gsc_Folders = Empty Then
        MsgBox "get_GSC_FilePairs: Got NO GSC file pairs from root folder ! (" & RootFolder & ")"
        get_GSC_FilePairs = Empty
        Exit Function
    End If
Else    ' we got an array, but ubound is 0... one element only
    If UBound(gsc_Folders) = 0 Then
        MsgBox "get_GSC_FilePairs: Got ONE file pair from root folder ! (" & RootFolder & ")"
        get_GSC_FilePairs = Empty
        Exit Function
    End If
End If


Dim tmpResults

tmpResults = get_GSC_FilePairs_forProjects(gsc_Folders)


get_GSC_FilePairs = tmpResults


End Function


Function get_GSC_FilePairs_forProjects(ProjectFoldersArray) As Variant


If Not IsArray(ProjectFoldersArray) Then
    MsgBox "get_GSC_FilePairs_forProjects: Project folders parameter was NOT an array !"
    get_GSC_FilePairs_forProjects = Empty
    Exit Function
End If


Dim ctFiles As Variant
Dim tmpResult As Variant

Dim k As Integer
Let k = -1


ReDim tmpResult(0)
Dim nss2 As Integer     ' Non studio <2 files projects count

For i = 0 To UBound(ProjectFoldersArray)
    
    If i > 100 Then Exit For    ' DEBUGGING...
    
    ctFiles = get_GSC_FilePair(ProjectFoldersArray(i))
    
    If IsArray(ctFiles) Then
        
        If UBound(ctFiles) >= 1 Then
            
            Let k = k + 1
            ReDim Preserve tmpResult(k)
            
            tmpResult(k) = ctFiles
            
            'tmpResult(k, 0) = ctFiles(0)
            'tmpResult(k, 1) = ctFiles(1)
            
        End If
    
    Else
        nss2 = nss2 + 1
        Dim nssProjects As String
        
        nssProjects = IIf(nssProjects = "", ProjectFoldersArray(i) & vbCr, nssProjects & ProjectFoldersArray(i) & vbCr)
        
    End If
    
Next i

Documents.Add.StoryRanges(wdMainTextStory).InsertAfter ("NON STUDIO PROJECTS WITH 0 or 1 docxs in root:" & vbCr & nssProjects)
Debug.Print "Got " & k + 1 & " pairs of GSC files (&" & nss2 & " Non-Studio projs with <2 docxs)"

get_GSC_FilePairs_forProjects = tmpResult


End Function



Function get_GSC_FilePair(ProjectFolder, Optional optimiseForInitials) As Variant


If ProjectFolder = "" Then get_GSC_FilePair = "": Exit Function


Dim uLg As String
uLg = Get_ULg


Dim fso As New Scripting.FileSystemObject

Dim ctPF As Scripting.Folder

Set ctPF = fso.GetFolder(ProjectFolder)


Dim tmpRes As Variant


If fso.FolderExists(ProjectFolder & "\Studio") Then
    
    ' optimise for opening pair of files, only source and target docxs
    If IsMissing(optimiseForInitials) Then
    
        If fso.FolderExists(ProjectFolder & "\Studio\en-GB") And _
            fso.FolderExists(ProjectFolder & Languify(studioTargetFolder)) Then
            
            If Dir(ProjectFolder & "\Studio\en-GB" & "\*.docx") <> "" And _
                Dir(ProjectFolder & Languify(studioTargetFolder) & "\*.docx") <> "" Then
                
                ReDim tmpRes(1)
                
                tmpRes(0) = fso.GetFile(ProjectFolder & "\Studio\en-GB" & "\" & Dir(ProjectFolder & "\Studio\en-GB" & "\*.docx")).Path
                tmpRes(1) = fso.GetFile(ProjectFolder & Languify(studioTargetFolder) & "\" & Dir(ProjectFolder & Languify(studioTargetFolder) & "\*.docx")).Path
                
            End If
            
        End If
    
    ' optimise for initials gets the files most likely to retrieve correct initials of authors
    ' for both translation and revision, that is external review file and source docx document
    Else    ' optimise for initials extracting, not for opening files (only docxs)
        
        ' external review file is always more precise than files in target language folder
        If fso.FolderExists(ProjectFolder & Languify(studioTargetReviewFolder)) Then
                    
            ' if external review file exists, use it for revisor extraction
            If Dir(ProjectFolder & Languify(studioTargetReviewFolder) & "\*.docx") <> "" Then
                
                ReDim tmpRes(1)
                
                tmpRes(1) = fso.GetFile(ProjectFolder & Languify(studioTargetReviewFolder) & "\" & Dir(ProjectFolder & Languify(studioTargetReviewFolder) & "\*.docx")).Path
                
            Else    ' no file found in external review folder
                    
                If Dir(ProjectFolder & Languify(studioTargetFolder) & "\*.docx") <> "" Then
                
                    ReDim tmpRes(1)
                
                    tmpRes(1) = fso.GetFile(ProjectFolder & Languify(studioTargetFolder) & "\" & Dir(ProjectFolder & Languify(studioTargetFolder) & "\*.docx")).Path
                    
                End If
                
            End If
            
            tmpRes(0) = fso.GetFile(ProjectFolder & "\Studio\en-GB" & "\" & Dir(ProjectFolder & "\Studio\en-GB" & "\*.docx")).Path
            
            ' fso.FolderExists(ProjectFolder & "\Studio\en-GB") And _
            fso.FolderExists(ProjectFolder & "\Studio\ro-RO") Then
        
        Else    ' no external review folder
            
            If Dir(ProjectFolder & Languify(studioTargetFolder) & "\*.docx") <> "" Then
                
                ReDim tmpRes(1)
                
                tmpRes(1) = fso.GetFile(ProjectFolder & Languify(studioTargetFolder) & "\" & Dir(ProjectFolder & Languify(studioTargetFolder) & "\*.docx")).Path
                
                ' ONLY if found RO file do we look for EN one
                If Dir(ProjectFolder & "\Studio\en-GB" & "\*.docx") <> "" Then
                
                    tmpRes(0) = fso.GetFile(ProjectFolder & "\Studio\en-GB" & "\" & Dir(ProjectFolder & "\Studio\en-GB" & "\*.docx")).Path
                Else
                    ' counting studio no english doc file
                    Debug.Print ProjectFolder; ": - SNEF"
                    
                End If
            
            Else
                'counting studio no romanian doc file
                Debug.Print ProjectFolder; ": - SNRF"
            
            End If
            
        End If
        
    End If

Else    ' NON-STUDIO FOLDERS
    
        
    Dim projectName As String   ' ct project folder name only, no path
    projectName = Split(ProjectFolder, "\")(UBound(Split(ProjectFolder, "\")))
    
    Dim tgProjectName As String
    
    ' Two sanity checks:
    ' 1 - no target language original projects allowed ! (reverse translation)
    If InStr(1, projectName, "." & uLg) > 0 Then
        GoTo skipThisOne
    End If
    
    ' 2 - if Non-studio folder has less than 2 docx files...
    If bDir.bGDirAll(ProjectFolder & "\*.docx").count < 2 Then
        GoTo skipThisOne
    End If
    

    ' On second thought, three checks needed:
    ' 3 - only doing en and fr originals !
    If InStr(1, projectName, ".en") = 0 And InStr(1, projectName, ".fr") = 0 Then
        GoTo skipThisOne
    End If
    ' and process them correctly!
    If InStr(1, projectName, ".en") > 0 Then
        tgProjectName = Replace(projectName, ".en", "." & uLg)
    ElseIf InStr(1, projectName, ".fr") > 0 Then
        tgProjectName = Replace(projectName, ".fr", "." & uLg)
    End If
        
    
    
    'first we see if we have a likely target lg file: properly named or "final"
    If Dir(ProjectFolder & "\" & tgProjectName & ".docx") <> "" Then
        
       ReDim tmpRes(0)
       tmpRes(0) = ProjectFolder & "\" & Dir(ProjectFolder & "\" & tgProjectName & ".docx")
    
    ' no prop named target language file, try for "final" doc name
    ElseIf Dir(ProjectFolder & "\" & Left(projectName, Len(projectName) - 5) & "*final.docx") <> "" Then
            
        ReDim tmpRes(0)
        tmpRes(0) = ProjectFolder & "\" & Dir(ProjectFolder & "\" & Left(projectName, Len(projectName) - 5) & "*final.docx")
        
    End If
    
    
    ' If we found target file, also gather prop named source file
    If IsArray(tmpRes) Then
       
        If Dir(ProjectFolder & "\" & projectName & ".docx") <> "" Then
            
            ReDim Preserve tmpRes(1)
            tmpRes(1) = ProjectFolder & "\" & Dir(ProjectFolder & "\" & projectName & ".docx")
            
        End If
       
    End If
    
    
    
End If

' Jump Point to skip processing this project folder for incompatibility reasons !
skipThisOne:

'Debug.Print "Counted " & ns0 & " Non-Studio projects with 0 or 1 and " & nsb2 & " proj with 3 or more docx files!"
get_GSC_FilePair = tmpRes


End Function



Function get_GSC_Folders(RootFolder As String) As Variant


Dim fso As New Scripting.FileSystemObject

If Not fso.FolderExists(RootFolder) Then
    
    MsgBox "Provided root folder does not exist, Quitting..."
    get_GSC_Folders = Empty
    Exit Function
    
End If


Dim rFdr As Scripting.Folder

Set rFdr = fso.GetFolder(RootFolder)

If rFdr.SubFolders.count = 0 Then
    MsgBox "Found no subfolder to provided root folder (" & RootFolder & "), Quitting..."
    get_GSC_Folders = Empty
    Exit Function
End If


Dim tmpResults As Variant

ReDim tmpResults(0)

Dim n As Integer
Let n = 0

Dim k As Integer

Let k = -1  ' first found result will see this increase to 0


Dim tmpFdr As Scripting.Folder

For Each tmpFdr In rFdr.SubFolders
    
    If Is_GSC_Doc(tmpFdr.Name) Then
        
        Let k = k + 1
        
        If k > 100 Then Exit For
        
        ReDim Preserve tmpResults(k)
        
        tmpResults(k) = tmpFdr.Path
    
    Else    'Count and collect badly named folders
        
        Let n = n + 1
        
        If n = 1 Then
            Debug.Print "Found following badly named subfolders: "
        Else
            Debug.Print tmpFdr.Name
        End If
        
        
    End If
    
Next tmpFdr


Debug.Print "Captured " & k & " GSC project folders (&" & n & " badly named) - " & k + n & " in total"


get_GSC_Folders = tmpResults


End Function


Sub Show_CtDocument_Initials()

projf = getProjectFolderOf(ActiveDocument.Name)

If projf = "" Then MsgBox "Project folder not found !": Exit Sub

gscrolib.AhMsgBox_Show "Document done by " & vbCr & vbCr & Get_Project_Authors(projf, "optimiseForInitials"), 3, , , 100

End Sub


Function Get_Document_Initials(TargetDocument As Document) As String
' Using Get_Project_Authors function. Using settings file from Outlook Assign Documents macros.

Dim projAuthors As String

projAuthors = Replace(Get_Project_Authors(getProjectFolderOf(TargetDocument.Name), "optimiseForInitials"), " ", "")


If UBound(Split(projAuthors, "/")) = 2 Then
    
    Dim oSettings As New SettingsFileClass
    
    Dim usersData() As String
    
    ReDim usersData(1, 1, 0)
    
    Dim astInit, astUsers, adInit, adUsers
    
    astInit = oSettings.getOption("astInitList", "AssignOptions")
    adInit = oSettings.getOption("adInitList", "AssignOptions")
    astUsers = oSettings.getOption("astUsersList", "AssignOptions")
    adUsers = oSettings.getOption("adUsersList", "AssignOptions")
    
    ' First dimension no 1 (# 0) - AST DATA
    For i = 0 To UBound(Split(astInit, ","))
        
        ReDim Preserve usersData(1, 1, i)
        
        usersData(0, 0, i) = Split(astInit, ",")(i)
        usersData(0, 1, i) = Split(astUsers, ",")(i)
        
        usersData(1, 0, i) = Split(adInit, ",")(i)
        usersData(1, 1, i) = Split(adUsers, ",")(i)
        
    Next i
    
    ' First dimension no 2 (# 1) - AD DATA
    For j = i To UBound(Split(adInit, ","))
        
        ReDim Preserve usersData(1, 1, j)
                
        usersData(1, 0, j) = Split(adInit, ",")(j)
        usersData(1, 1, j) = Split(adUsers, ",")(j)
        
    Next j
    
    
    Dim tmpProjInitials As String
    tmpProjInitials = ChangeAuthors_toInitials(projAuthors, usersData)
    
    Debug.Print "Alinuta, my love!"
    
Else

    Get_Document_Initials = ""
End If


End Function


Function ChangeAuthors_toInitials(authors, UsersArray) As String

If Not IsArray(UsersArray) Then
    ChangeAuthors_toInitials = ""
Else
    
    Dim splitAuthors
    splitAuthors = Split(authors, "/")
    
    For i = 0 To UBound(splitAuthors)
        
        If i <> 2 Then  ' Admins (there aint no princes and paupers in the RO Unit!)
            
            Dim ctUsersIndex
            ctUsersIndex = Get_Index_ofArray_Entry(UsersArray(0), CStr(LCase(splitAuthors(i))))
            
        Else            ' Asistants
            
            
            
        End If
        
    Next i
    
End If

End Function


Function Get_Project_Authors(ProjectFolderPath, Optional optimiseForInitials, Optional useCurrentUserAsAst) As String

If ProjectFolderPath = "" Then Get_Project_Authors = "": Exit Function

Dim src_tgt_files   ' array

If Not IsMissing(optimiseForInitials) Then
    src_tgt_files = get_GSC_FilePair(ProjectFolderPath, optimiseForInitials)
Else
    src_tgt_files = get_GSC_FilePair(ProjectFolderPath)
End If


Dim targetDocx As String
targetDocx = Get_TargetDoc_forProject(ProjectFolderPath)


If Not IsMissing(useCurrentUserAsAst) Then

    Dim gscSettings As New SettingsFileClass

    If Not gscSettings.IsEmpty = FileNotEmpty Then
        Debug.Print ""
    End If
    
    Dim astInit As String
    astInit = gscSettings.getOption("astInitList", "AssignOptions")
    
    Dim ctAst
    ctAst = Application.UserInitials
    
    
    If ctAst = "" Then
        ctAst = InputBox("Please input your Workflow initials", "Word user initials missing")
    End If
    
    
    If Not astInit = "" Then
        
        If InStr(1, astInit, ctAst) = 0 Then
            Debug.Print "Get_Project_Authors: Word user initials not in ast initials string in gscro settings file!"
        End If
        
    Else    ' user does not have ast inits in gscro settings... not his/her fault
        Debug.Print "Get_Project_Authors: ast initials string missing from gscro settings file!"
    End If

Else
    ctWdUser = Get_FileOwner_Property(Get_TargetDoc_forProject(ProjectFolderPath))
End If



If IsArray(src_tgt_files) Then

        'Get_Project_Authors = Replace(Get_FileOwner_Property(CStr(src_tgt_files(0))), "CONSILIUM\", "") & "/ " & _
            revisionAuthor & "/ " & Replace(Get_FileOwner_Property(CStr(src_tgt_files(1))), "CONSILIUM\", "")

    Get_Project_Authors = Replace(Get_FileOwner_Property(CStr(src_tgt_files(0))) & "/ " & Get_FileOwner_Property(CStr(src_tgt_files(1))) & "/ " & ctWdUser, "CONSILIUM\", "")

Else

    Get_Project_Authors = ""
    
End If


End Function


Function Get_TargetSDLXliff(ProjectFolderPath) As String

If Dir(ProjectFolderPath & getXliffFilesMask) <> "" Then
    Get_TargetSDLXliff = ProjectFolderPath & Languify(studioTargetFolder) & "\" & Dir(ProjectFolderPath & getXliffFilesMask)
Else
    Get_TargetSDLXliff = ""
End If


End Function


Function Get_TargetDoc_forProject(ProjectFolderPath) As String

If Dir(ProjectFolderPath & Languify(studioTargetFolder) & "\*.docx") <> "" Then
    Get_TargetDoc_forProject = ProjectFolderPath & Languify(studioTargetFolder) & "\" & Dir(ProjectFolderPath & Languify(studioTargetFolder) & "\*.docx")
Else
    Get_TargetDoc_forProject = ""
End If

End Function


Function Get_FileOwner_Property(FilePath As String) As String

Dim sShell As Shell32.Shell
Dim sSF As Shell32.Folder
Dim sFI As Shell32.ShellFolderItem
Set sShell = New Shell32.Shell

Dim fileHostFolder As String
Dim FileName As String

If FilePath = "" Then Get_FileOwner_Property = "": Exit Function

fileHostFolder = Left(FilePath, InStrRev(FilePath, "\") - 1)
FileName = Split(FilePath, "\")(UBound(Split(FilePath, "\")))


Set sSF = sShell.NameSpace(fileHostFolder)
Set sFI = sSF.ParseName(FileName)


Get_FileOwner_Property = sFI.ExtendedProperty("Owner")


End Function

Sub Print_DateCreated_Property(FilePath As String)

Debug.Print get_DateCreated_Property(FilePath)

End Sub



' MAIN entry point into project folders pruner, to list all fossil project folders into new document
Function Print_Tprepared_ProjFolders_OT(OlderThanDate As String) As String

Dim res As Variant

res = get_TPrepared_ProjFolders_OT(OlderThanDate)

Dim tmpList As String


If IsArray(res) Then
    Debug.Print "Found & " & UBound(res) + 1 & " old TPrepared project folders:"
    For i = 0 To UBound(res)
        tmpList = IIf(tmpList = "", res(i) & vbCr, tmpList & res(i) & vbCr)
    Next i
    Documents.Add.Content.InsertAfter ("Found " & UBound(res) + 1 & " TPrepared project folders" & vbCr & "OLDER THAN: " & OlderThanDate & vbCr & vbCr & tmpList)
    Print_Tprepared_ProjFolders_OT = "Found " & UBound(res) + 1 & " TPrepared project folders older than: " & OlderThanDate
Else
    Debug.Print "Found NO folders on TPrepared older than " & OlderThanDate
    Print_Tprepared_ProjFolders_OT = "Found NO folders on TPrepared older than " & OlderThanDate
End If


End Function


' expand userform if not already and print to special message label
Sub PrintToFormMessage(InputMessage As String)

If frmProjectFoldersPruner.Height < frmProjPruner_ExpandedHeight Then
    frmProjectFoldersPruner.Height = frmProjPruner_ExpandedHeight
Else
End If

frmProjectFoldersPruner.lbMessage.Caption = InputMessage

End Sub


Sub Delete_TPrepared_ProjectFolders_OT(OlderThanDate As String, Optional RestrictToNumber, Optional MessageToForm)
' Optional parameter MessageToForm prints messages while

Dim res As Variant

res = get_TPrepared_ProjFolders_OT(OlderThanDate)   ' array of found project folders


Dim tmpList As String


If IsArray(res) Then
    
    Debug.Print "Found & " & UBound(res) + 1 & " TPrepared project folders older than " & OlderThanDate
    If Not IsMissing(MessageToForm) Then PrintToFormMessage ("Found & " & UBound(res) + 1 & " TPrepared project folders older than " & OlderThanDate)

    Dim delRes As Variant    ' a list of folders NOT deleted or empty string for success!
    
    If Not IsMissing(RestrictToNumber) Then
        If Not IsMissing(MessageToForm) Then
            delRes = DeleteAllFolders(res, RestrictToNumber, MessageToForm)
        Else    ' optional param MessageToForm was not provided
            delRes = DeleteAllFolders(res, RestrictToNumber)
        End If
    Else    ' optional param restrict to number was not provided
        If Not IsMissing(MessageToForm) Then
            delRes = DeleteAllFolders(res, , MessageToForm)
        Else
            delRes = DeleteAllFolders(res)
        End If
    End If
    
Else
    Debug.Print "Found NO folders on TPrepared older than " & OlderThanDate
    If Not IsMissing(MessageToForm) Then PrintToFormMessage ("Found NO folders on TPrepared older than " & OlderThanDate)
End If


' did we get an array of not-deleted folders?
If IsArray(delRes) Then
    If UBound(delRes) > 0 Then
    
        If Not IsMissing(RestrictToNumber) Then
            MsgBox "Routine was unable to delete " & UBound(delRes) + 1 & " folders from TPrepared (from" & RestrictToNumber & ":" & vbCr & vbCr & delRes
        Else
            MsgBox "Routine was unable to delete " & UBound(delRes) + 1 & " folders from TPrepared (from" & UBound(res) & ":" & vbCr & vbCr & delRes
        End If
    
    Else    ' no returned unable-to-delete folders!
        
        If delRes(0) <> "" Then
            MsgBox "Routine was unable to delete " & UBound(delRes) + 1 & " folders from TPrepared (from" & UBound(res) & ":" & vbCr & vbCr & delRes
        Else     'no errors returned
            If Not IsMissing(RestrictToNumber) Then
                MsgBox "Successfully deleted " & RestrictToNumber & " old project folders!", vbOKOnly + vbInformation, "Success!"
            Else
                MsgBox "Successfully deleted " & UBound(res) + 1 & " old project folders!", vbOKOnly + vbInformation, "Success!"
            End If
        End If
        
    End If
Else    ' no errors returned
    MsgBox "Found NO folders on TPrepared older than " & OlderThanDate
End If



End Sub



Function get_TPrepared_ProjFolders_OT(OlderThanDate As String) As Variant
' Function returns an array of folder paths of whom both the date of creation and
' date of last modification is older than specified date in string format
' Expected string format: "01/11/2015")


Dim fso As New Scripting.FileSystemObject

Dim baseFolder As Scripting.Folder

Set baseFolder = fso.GetFolder("T:\Prepared originals")


Dim tmpResult

Dim fdr As Scripting.Folder

Dim fdrDateCreated As Date
Dim fdrDateModified As Date

Dim k As Integer
Let k = -1

For Each fdr In baseFolder.SubFolders
    
    If Is_GSC_Doc(fdr.Name, "SilentMode") Then
        
        fdrDateCreated = get_FdrDateCreated_Property(fdr.Path)
        fdrDateModified = get_FdrDateModified_Property(fdr.Path)
        
        If DateDiff("d", fdrDateCreated, CDate(OlderThanDate)) > 0 And _
            DateDiff("d", fdrDateModified, CDate(OlderThanDate)) > 0 Then
            
            Let k = k + 1   ' 0 on first hit
            
            ' DEBUGGING
            'If k > 100 Then Exit For
            
            If Not IsArray(tmpResult) Then
                ReDim tmpResult(k)
            Else
                ReDim Preserve tmpResult(k)
            End If
            
            tmpResult(k) = fdr.Path
            
        End If
        
    End If
    
Next fdr

get_TPrepared_ProjFolders_OT = tmpResult

End Function



Function get_FdrDateModified_Property(FolderPath As String, Optional ReturnString) As Variant

Dim fso As New Scripting.FileSystemObject
Dim f As Scripting.Folder


If Dir(FolderPath, vbDirectory) <> "" Then
    
        
    Set f = fso.GetFolder(FolderPath)
    
    If Not IsMissing(ReturnString) Then
        get_FdrDateModified_Property = CStr(Split(f.DateCreated, " ")(0))
    Else
        get_FdrDateModified_Property = CDate(Split(f.DateCreated, " ")(0))
    End If
    
   
Else
    get_FdrDateModified_Property = ""
End If

End Function


Function get_FdrDateCreated_Property(FolderPath As String) As String

Dim fso As New Scripting.FileSystemObject
Dim f As Scripting.Folder


If Dir(FolderPath, vbDirectory) <> "" Then
    
        
    Set f = fso.GetFolder(FolderPath)
    get_FdrDateCreated_Property = Split(f.DateCreated, " ")(0)
    
   
Else
    get_FdrDateCreated_Property = ""
End If

End Function



Function get_DateModified_Property(FilePath As String) As String

Dim fso As New Scripting.FileSystemObject
Dim f As Scripting.File

Dim fileHostFolder As String
Dim FileName As String


fileHostFolder = Left(FilePath, InStrRev(FilePath, "\") - 1)
FileName = Split(FilePath, "\")(UBound(Split(FilePath, "\")))


If Dir(fileHostFolder, vbDirectory) <> "" Then
    
    If Dir(FilePath) <> "" Then
        
        Set f = fso.GetFile(FilePath)
        get_DateModified_Property = Split(f.DateCreated, " ")(0)
        
        
    Else
        get_DateModified_Property = ""
    End If
    
Else
    get_DateModified_Property = ""
End If

End Function


Function get_DateCreated_Property(FilePath As String) As String

Dim fso As New Scripting.FileSystemObject
Dim f As Scripting.File

Dim fileHostFolder As String
Dim FileName As String


fileHostFolder = Left(FilePath, InStrRev(FilePath, "\") - 1)
FileName = Split(FilePath, "\")(UBound(Split(FilePath, "\")))


If Dir(fileHostFolder, vbDirectory) <> "" Then
    
    If Dir(FilePath) <> "" Then
        
        Set f = fso.GetFile(FilePath)
        get_DateCreated_Property = Split(f.DateCreated, " ")(0)
        
        
    Else
        get_DateCreated_Property = ""
    End If
    
Else
    get_DateCreated_Property = ""
End If

End Function


Sub Print_ExtendedProperty(FilePath As String, PropertyName As String)


Dim sShell As Shell32.Shell
Dim sSF As Shell32.Folder
Dim sFI As Shell32.ShellFolderItem
Set sShell = New Shell32.Shell

Dim fileHostFolder As String
Dim FileName As String

If FilePath = "" Then Exit Sub

fileHostFolder = Left(FilePath, InStrRev(FilePath, "\") - 1)
FileName = Split(FilePath, "\")(UBound(Split(FilePath, "\")))


Set sSF = sShell.NameSpace(fileHostFolder)
Set sFI = sSF.ParseName(FileName)

Dim ep As String

Debug.Print sFI.ExtendedProperty(PropertyName)


End Sub


Function Get_FileOwner_Property_NONFUNCTIONAL(FilePath) As String

Dim arrHeaders(266)

Dim fileHostFolder As String
Dim FileName As String

fileHostFolder = Left(FilePath, InStrRev(FilePath, "\") - 1)

FileName = Split(FilePath, "\")(UBound(Split(FilePath, "\")))



Set objShell = CreateObject("Shell.Application")
Set objFolder = objShell.NameSpace(FilePath)

For i = 0 To 265
    arrHeaders(i) = objFolder.GetDetailsOf(objFolder.Items, i)
Next


Set strFileName = objFolder.Items.Item(1)


'For Each strFileName In objFolder.Items
'For i = 0 To 265
    If objFolder.GetDetailsOf(strFileName, 10) <> "" Then   ' 10 is index of "Owner"
        'Debug.Print i & vbTab & arrHeaders(i) & ": " & objFolder.GetDetailsOf(strFileName, i)
        Get_FileOwner_Property = objFolder.GetDetailsOf(strFileName, 10)
    End If
 'Wscript.Echo i & vbTab & arrHeaders(i) & ": " & objFolder.GetDetailsOf(strFileName, i)
'Next i
'Next

End Function

Function regexMatch(InputString As String, MatchPattern As String) As Boolean

    Dim regEx As New regexp
    
        With regEx
            .Global = True
            '.MultiLine = True
            .IgnoreCase = False
            .Pattern = MatchPattern
        End With

        If regEx.test(InputString) Then
            regexMatch = True
        Else
            regexMatch = False
        End If

End Function


Function DeleteAllFolders(InputFoldersArray, Optional RestrictToNumber, Optional MessageToForm) As Variant
' Function could return list of folders it did not succeed in deleting or empty string for succes?
' Optional parameter MessageToForm makes routine write back to calling userform user messages (like progress reports)

Dim fo As New Scripting.FileSystemObject

Dim fd As Scripting.Folder

Dim tmpRes() As String
ReDim tmpRes(0)


If IsArray(InputFoldersArray) Then

    For i = 0 To UBound(InputFoldersArray)
        
        If Not IsMissing(RestrictToNumber) Then
            If i >= RestrictToNumber Then Exit For
        End If
        
        If Dir(InputFoldersArray(i), vbDirectory) <> "" Then
            
            Set fd = fo.GetFolder(InputFoldersArray(i))
            
            On Error GoTo ErrDeletingF

            
            Debug.Print "Deleting " & fd.Path
            If Not IsMissing(MessageToForm) Then PrintToFormMessage ("Deleting " & fd.Path)
            
            fd.Delete Force:=True
            
            DoEvents
            
            Call GSCRo_Lib.PauseForSeconds(0.7) ' NECESSARY?
            
        End If
        
SkipNextFolder:
    Next i

End If

DeleteAllFolders = tmpRes


Exit Function

'**************************************************

ErrDeletingF:
    If Err.Number <> 0 Then
        Debug.Print "Error " & Err.Number & ": " & Err.Description & " for del fdr: " & InputFoldersArray(i)
        
        If tmpRes(0) <> "" Then ReDim Preserve tmpRes(UBound(tmpRes) + 1)
        tmpRes(UBound(tmpRes)) = InputFoldersArray(i)
        'tmpRes = IIf(tmpRes = "", InputFoldersArray(i), tmpRes & vbCr & InputFoldersArray(i))
        Err.Clear
        GoTo SkipNextFolder
    End If


End Function


Function TableSort_byColumn(TargetTable As Table, ColumnNumber As Integer, ExcludeTableHeader As Boolean) As Boolean

If Not TargetTable.Uniform Then
    TableSort_byColumn = False
Else
    TargetTable.Sort ExcludeTableHeader, ColumnNumber, wdSortFieldAlphanumeric, wdSortOrderAscending
    TableSort_byColumn = True
End If

End Function


Function TablePrune_RedundantRows(TargetTable As Table) As Integer


Dim ctRowKey As String

Dim k As Integer

' what if?
If TargetTable.Rows.count = 1 Then
    TablePrune_RedundantRows = 0
    Exit Function
End If


For i = 1 To TargetTable.Rows.count - 1
    
    ctRowKey = TrimNonAlphaNums(TargetTable.Rows(i).Cells(1).Range.Text)
    
    If TrimNonAlphaNums(TargetTable.Rows(i + 1).Cells(1).Range.Text) = ctRowKey Then
    
        Do

            TargetTable.Rows(i + 1).Delete
            k = k + 1
            
            ' after deleting a table row, we're forced to consider whether our current index is now
            ' the last row in table (dangerous to go to i + 1, it's not part of table anymore)
            If i = TargetTable.Rows.count Then Exit For
            
        Loop While TrimNonAlphaNums(TargetTable.Rows(i + 1).Cells(1).Range.Text) = ctRowKey
        
    End If
    
    
Next i


TablePrune_RedundantRows = k

End Function
