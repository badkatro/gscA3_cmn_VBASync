Attribute VB_Name = "VBAReCreate"
'***************** COMMENT 1 & 2
Public MsgString As String

Public Sub TemplatesExport()

frmTemplates.Show

End Sub
Public Sub VBAExportComponents(TargetTemplate As String, Optional CommonMessageBox)

Dim tt As Template
Dim FoundTarget As Boolean
Dim TargetVBP As VBProject

Dim tcomp As VBComponent
Dim tfo As New Scripting.FileSystemObject
Dim ExportFolder As String

Dim vbcompCt As Integer
Dim expExt As String
Dim td As Document

Dim TemplateIsOpened As Boolean
Dim tobeclosed As Document
'*************************************************************************************************************


If tfo.FolderExists("D:\VBA\Automatic Exports") Then
    ExportFolder = "D:\VBA\Automatic Exports"
Else
    ExportFolder = gscrolib.GetDirectory("Please select destination folder for exporting target template." & vbCrCr & _
        "If you create and assign a folder called �Automatic Exports� under �D:\VBA�, it will be used automatically henceforth.")
End If


For Each tt In Application.Templates
    If tt.Name = TargetTemplate Then
        FoundTarget = True
        Exit For
    End If
Next tt

For Each td In Documents
    If td.Name = tt.Name Then
        TemplateIsOpened = True
    End If
Next td

Application.ScreenUpdating = False
If Not TemplateIsOpened Then
    Set tobeclosed = tt.OpenAsDocument
End If

' If supplied template name is actually a template, verify and create export destination folder
If FoundTarget Then
    
    If Not tfo.FolderExists(ExportFolder & "\" & tt.Name) Then
        tfo.CreateFolder (ExportFolder & "\" & tt.Name)
    End If
    
    ExportFolder = tfo.CreateFolder(ExportFolder & "\" & tt.Name & "\" & DatePart("yyyy", Now) & "-" & DatePart("m", Now) & "-" & _
            DatePart("d", Now) & "_" & Fix(timer))       ' 2012-01-01_56200  - where last number is number of seconds elapsed since midnight
    
    Set TargetVBP = tt.VBProject
    
    On Error GoTo ErrExport
    
    ' Export forms, modules and classes
    For Each tcomp In TargetVBP.VBComponents
        
        If tcomp.Type = vbext_ct_MSForm Then
            expExt = ".frm"
        ElseIf tcomp.Type = vbext_ct_StdModule Then
            expExt = ".bas"
        ElseIf tcomp.Type = vbext_ct_ClassModule Then
            expExt = ".cls"
        Else
            expExt = ""
        End If
        
        tcomp.Export (ExportFolder & "\" & tcomp.Name & expExt)
        vbcompCt = vbcompCt + 1
        
    Next tcomp
    
    Call ReferencesExport(tt.Name, ExportFolder)
    
Else
    MsgBox "Template " & TargetTemplate & " was not found!", vbOKOnly + vbCritical, "Wrong template name"
    Exit Sub
End If

If Not tobeclosed Is Nothing Then
    tobeclosed.Close wdDoNotSaveChanges
End If
Application.ScreenUpdating = True

If Not IsMissing(CommonMessageBox) Then
    MsgString = IIf(MsgString = "", tt.Name & " (" & vbcompCt & ")", MsgString & vbCr & tt.Name & " (" & vbcompCt & ")")
Else
    MsgBox vbcompCt & " visual basic components were exported to" & vbCrCr & ExportFolder, vbOKOnly + vbInformation, "Export finished!"
End If


Exit Sub

'******************************************************************************************************************************
ErrExport:
    If Err <> 0 Then
        If Err.Number = 50289 Then
            MsgBox "Template " & tt.Name & " could not be exported, VBProject is protected!", vbOKOnly + vbCritical, "Export failed"
            Err.Clear
            Exit Sub
        Else
            MsgBox "Error " & Err.Number & " occured in " & Err.Source & " with description: " & vbCrCr & _
                Err.Description, vbOKOnly + vbCritical, "Error while exporting template"
        End If
    End If

End Sub

Public Sub NewTemplateFromFolder(TemplateName As String)

Dim baseFolder As String, TargetFolder As String, DestinationFolder As String
Dim tfo As New Scripting.FileSystemObject
Dim tfd As Scripting.Folder

Dim NewTpl As Document
Dim impF As Integer
Dim NewTplVBComp As VBComponents
Dim tf As Scripting.File

Dim refnms, refnmsFile As String, refFile As String
Dim refnstr As TextStream
Dim cuig As String, majmin As String
Dim cpth As String

Dim OrigTpl As Template

' Base folder setting
If tfo.FolderExists("D:\VBA\Automatic Exports") Then
    baseFolder = tfo.GetFolder("D:\VBA\Automatic Exports").Path
Else
    baseFolder = gscrolib.GetDirectory("Please select base folder from which to recreate template. Base folder must contain subfolders named as the templates to be recreated.")
End If
' Destination folder setting
If tfo.FolderExists(baseFolder & "\Recreated Templates") Then
    DestinationFolder = baseFolder & "\Recreated Templates"
Else
    DestinationFolder = gscrolib.GetDirectory("Please select destination folder for recreated templates.")
End If

' Target folder and backup folder identification or user prompt
If tfo.FolderExists(baseFolder & "\" & TemplateName) Then
    
    Set tfd = tfo.GetFolder(baseFolder & "\" & TemplateName)
    
    If tfd.SubFolders.count > 0 Then
        If tfd.SubFolders.count > 1 Then
            
            If frmTemplates.cbLatest = -1 Then
                ' HERE, Write function to return latest backup folder
                TargetFolder = GetLatestBackupFolder(tfd.Path)
            Else
                ' HERE, Populate and display a listbox in a form so that user may choose which backup folder to use
                ' TO DO
            End If
        
        Else
            ' Get unique subfolder
            If Dir(tfd.Path & "\*", vbDirectory) <> "" Then
                Do
                    usf = Dir
                Loop While Left$(usf, 1) = "."
            End If
                
            TargetFolder = tfo.GetFolder(tfd.Path & "\" & usf)
            
        End If
    Else
        MsgBox "Backup folder of template " & TemplateName & " is empty!"
        Exit Sub
    End If
    
Else
    MsgBox "No backup folder found bearing target template name found!"
    Exit Sub
End If

' Set folder object to identified desired backup folder
Set tfd = tfo.GetFolder(TargetFolder)
TemplateName = tfo.GetBaseName(TemplateName) & "." & tfd.Name & "." & tfo.GetExtensionName(TemplateName)

' Create empty template and save it under proper folder with proper name
Set NewTpl = Documents.Add(): NewTpl.SaveAs FileName:=DestinationFolder & "\" & TemplateName, FileFormat:=wdFormatXMLTemplateMacroEnabled
' Retrieve vbcomponents collection of new template
Set NewTplVBComp = NewTpl.VBProject.VBComponents

On Error GoTo ErrImp
' Iterate source files and import forms, bas(ic) modules and classes
' (HERE, TO DO SOMETHING ABOUT ThisDocument module ?)
For Each tf In tfd.files
    If tfo.GetExtensionName(tf.Name) = "frm" Or tfo.GetExtensionName(tf.Name) = "bas" Or _
        tfo.GetExtensionName(tf.Name) = "cls" Then
        
        impF = impF + 1
        NewTplVBComp.Import (tf.Path)
    End If
Next tf
' Save, signal user and close newly created template
NewTpl.Save

'*********************************** Import references from backup files **********************************************

refnmsFile = tfd.Path & "\" & Split(TemplateName, ".")(0) & ".ref.nms"
refFile = tfd.Path & "\" & Split(TemplateName, ".")(0) & ".ref"

If tfo.FileExists(refnmsFile) Then
    Set refnstr = tfo.OpenTextFile(refnmsFile)
    refnms = Split(refnstr.ReadLine, ",")

    For i = 0 To UBound(refnms)
        If System.PrivateProfileString(refFile, refnms(i), "Type") = "0" Then
            If NoReferenceFound(NewTpl.Name, CStr(refnms(i))) Then
                cuig = System.PrivateProfileString(refFile, refnms(i), "GUID")
                majmin = System.PrivateProfileString(refFile, refnms(i), "MajMin")
                NewTpl.VBProject.References.AddFromGuid cuig, Split(majmin, ",")(0), Split(majmin, ",")(1)
            End If
        Else    ' Type 1
            If NoReferenceFound(NewTpl.Name, CStr(refnms(i))) Then
                cpth = System.PrivateProfileString(refFile, refnms(i), "FullPath")
                NewTpl.VBProject.References.AddFromFile (cpth)
            End If
        End If
    Next i
End If
' ********************************** Import references from backup files *************************************************

NewTpl.SaveAs (NewTpl.Path & "\" & NewTpl.Name & ".new")    ' Need new name now for new template, Word will not open as document original template, it thinks the doc is already opened...
For Each OrigTpl In Application.Templates
    If OrigTpl.Name = TemplateName Then
        If InStr(1, OrigTpl.Path, "Automatic Exports") = 0 Then
            OrigTplFound = True
            Exit For
        End If
    End If
Next OrigTpl

' If found, open as document and copy organizer items - styles, commandbars, auto text entries.
If OrigTplFound Then
    Dim OrigTplD As Document
    Set OrigTplD = OrigTpl.OpenAsDocument
    CustomizationContext = OrigTpl
    ' copy toolbars
    If OrigTplD.CommandBars.count > 0 Then
        Dim tcmd As CommandBar
        For Each tcmd In OrigTplD.CommandBars
            If Not tcmd.BuiltIn Then
                If tcmd.Context = OrigTpl.FullName Then
                    Application.OrganizerCopy OrigTplD.FullName, NewTpl.FullName, tcmd.Name, wdOrganizerObjectCommandBars
                End If
            End If
        Next tcmd
    End If
    ' copy styles   -- Does not seem necessary --
'    If OrigTplD.Styles.count > 0 Then
'        Dim tst As Style
'        For Each tst In OrigTplD.Styles
'            'If tst.Type = wdStyleTypeCharacter Or tst.Type = wdStyleTypeParagraph Then
'                'If tst.InUse And Not tst.BuiltIn Then
'                    If Not tst.Type = wdStyleTypeList Then
'                        Application.OrganizerCopy OrigTplD.FullName, NewTpl.FullName, tst.NameLocal, wdOrganizerObjectStyles
'                    End If
'                'End If
'            'End If
'        Next tst
'    End If
    
    If OrigTpl.AutoTextEntries.count > 0 Then
        Dim tauto As AutoTextEntry
        For Each tauto In OrigTpl.AutoTextEntries
            Application.OrganizerCopy OrigTpl.FullName, NewTpl.FullName, tauto.Name, wdOrganizerObjectAutoText
        Next tauto
    End If
    OrigTplD.Close (wdDoNotSaveChanges)
    ' Copy original template's vbproject name, if different from default name
    If OrigTpl.VBProject.Name <> "TemplateProject" Then NewTpl.VBProject.Name = OrigTpl.VBProject.Name
End If
' HERE, we could remedy situation with new name for newly created template, ie ".new" extension added
NewTpl.SaveAs (Replace(NewTpl.FullName, ".new", "")): gscrolib.PauseForSeconds (0.5)
tfo.DeleteFile (NewTpl.FullName & ".new")

MsgBox "Template " & NewTpl.Name & " was successfully created under " & vbCr & vbCr & _
    DestinationFolder & vbCr & vbCr & impF & " VB Components were imported", vbOKOnly + vbInformation, _
    "Template recreation finished"
NewTpl.Close (wdSaveChanges)
'
Exit Sub
'********************************************************************************************************************
ErrImp:
    If Err.Number <> 0 Then
        MsgBox "Error " & Err.Number & " has occured in " & Err.Source & vbCrCr & _
            "with description:" & vbCr & Err.Description
        Err.Clear
    End If
End Sub


Private Function GetLatestBackupFolder(BaseFolderPath As String) As String


Dim sF As Scripting.Folder

Dim fo As New Scripting.FileSystemObject

If Not fo.FolderExists(BaseFolderPath) Then
    GetLatestBackupFolder = ""
    Exit Function
End If

If fo.GetFolder(BaseFolderPath).SubFolders.count > 1 Then
    
    Dim tmpDate As String
    
    Dim fdrs As foundFilesStruct
    
    fdrs = bDir.bGDirAll(BaseFolderPath & "\*", "listFolders")
    
    If Not fdrs.count = 0 Then
        
        If Not fdrs.count = 1 Then
            
            Dim mostRecentFoldername As String
            mostRecentFoldername = Get_MostRecentFoldername_fromFoundFdrsStr(fdrs)
            
        Else    ' ONE subfolder found!
        
        End If
        
    Else    ' NO soubfolders (backup subfolders) found !
    
    End If
    
    
    
Else     'path separated before calling this function
End If

GetLatestBackupFolder = BaseFolderPath & "\" & mostRecentFoldername

End Function


Private Function Get_MostRecentFoldername_fromFoundFdrsStr(InputFolderStruct As foundFilesStruct) As String


Dim mostRecent As String
    
mostRecent = InputFolderStruct.files(0)


For i = 1 To InputFolderStruct.count - 1

    If DateDiff("d", Split(InputFolderStruct.files(i), "_")(0), Split(mostRecent, "_")(0)) < 0 Then
        
        mostRecent = InputFolderStruct.files(i)
        
    Else     ' found bkp folder from same day, compare second split part, time number (nr seconds)
    
        If DateDiff("d", Split(InputFolderStruct.files(i), "_")(0), Split(mostRecent, "_")(0)) = 0 Then
            
            ' Found more recent same-day bckp folder
            If Split(InputFolderStruct.files(i), "_")(1) > Split(mostRecent, "_")(1) Then
                
                mostRecent = InputFolderStruct.files(i)
                
            Else    ' CANNOT  be equal. Only less or more.
            End If
        Else    ' if it's less, we ignore, further in the past it is
        End If
    
    End If
        
Next i

Get_MostRecentFoldername_fromFoundFdrsStr = mostRecent


End Function


Private Function NoReferenceFound(TargetTemplate As String, ReferenceName As String) As Boolean

Dim td As Document
Dim tr As Reference
Dim TemplateOpened As Boolean

For Each td In Documents
    If td.Name = TargetTemplate Then
        TemplateOpened = True
        Exit For
    End If
Next td

If TemplateOpened Then
    For Each tr In td.VBProject.References
        If tr.Name = ReferenceName Then
            NoReferenceFound = False
            Exit Function
        End If
    Next tr
Else
    MsgBox "Please open template " & TargetTemplate & " to continue with verification of existence of references !"
    Exit Function
End If

NoReferenceFound = True

End Function

Private Sub ReferencesExport(TargetTemplate As String, Optional ExportFolder)
' Purpose: export references of input template to file under default or user supplied folder

Dim tfo As New Scripting.FileSystemObject
Dim expF As String

Dim tt As Template
Dim FoundTarget As Boolean
Dim tvbp As VBProject
Dim tref As Reference

' Verify if supplied argument is a template
For Each tt In Application.Templates
    If tt.Name = TargetTemplate Then
        FoundTarget = True
        Exit For
    End If
Next tt

For Each td In Documents
    If td.Name = tt.Name Then
        TemplateIsOpened = True
    End If
Next td

Application.ScreenUpdating = False
If Not TemplateIsOpened Then
    Set tobeclosed = tt.OpenAsDocument
End If

' If supplied template name is actually a template, verify and create export destination folder
If FoundTarget Then
    
    If IsMissing(ExportFolder) Then
        expF = gscrolib.GetDirectory("Please select destination folder for exporting references of supplied template (" & _
            tt.Name & ")")
    Else
        expF = ExportFolder
    End If
    
    ' Create export destination references file
    Dim pstr As TextStream, nstr As TextStream
    Dim refExpFile As String
    Dim refnms As String
    
    refExpFile = expF & "\" & Split(tt.Name, ".")(0) & ".ref"
    Set pstr = tfo.CreateTextFile(refExpFile, True, False)
    Set nstr = tfo.CreateTextFile(refExpFile & ".nms", True, False)
        
    On Error GoTo ErrRef
    Set tvbp = tt.VBProject
    
    ' Check for broken references and warn/ exit (user fixes and we retries)
    For Each tref In tvbp.References
        If tref.IsBroken Then
            MsgBox "Project has broken references, please correct and retry", vbOKOnly + vbCritical, "Broken references found!"
            Exit Sub
        End If
    Next tref
    
    ' Record reference details into file, reference names into separate file
    For Each tref In tvbp.References
        If Not tref.BuiltIn Then
            If tref.Name <> "stdole" And tref.Name <> "Office" And _
                tref.Name <> "MSForms" Then     ' Automatically activating references, when inserting a user form,
                                                ' creating a module
                
                refnms = IIf(refnms = "", tref.Name, refnms & "," & tref.Name)  ' jot down names of references into variable
                
                pstr.WriteLine ("[" & tref.Name & "]")
                If tref.Description <> "" Then pstr.WriteLine ("Description=" & tref.Description)
                
                If tref.GUID <> "" Then
                    pstr.WriteLine ("GUID=" & tref.GUID)
                    pstr.WriteLine ("MajMin=" & tref.Major & "," & tref.Minor)
                End If
                
                pstr.WriteLine ("FullPath=" & tref.FullPath)
                pstr.WriteLine ("Type=" & tref.Type)
            End If
        End If
    Next tref
    
    If refnms <> "" Then
        nstr.WriteLine (refnms)
    End If
    
    MsgBox "References were successfully exported into " & vbCrCr & refExpFile, vbOKOnly + vbInformation, "References exported"
    
    pstr.Close: nstr.Close
    
End If

Exit Sub

'********************************************************************************************
ErrRef:
    If Err.Number <> 0 Then
        MsgBox "Error" & Err.Number & " occured in " & Err.Source & vbCrCr & Err.Description
        Err.Clear
        Exit Sub
    End If

End Sub
