VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "NumLockClass"
Attribute VB_Base = "0{FCFB3D2A-A0FA-1068-A738-08002B3371B5}"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_TemplateDerived = False
Attribute VB_Customizable = False
' Type declaration
Private Type OSVERSIONINFO
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As Long
    szCSDVersion As String * 128
End Type

' API declarations
Private Declare Function GetVersionEx Lib "kernel32" _
    Alias "GetVersionExA" _
    (lpVersionInformation As OSVERSIONINFO) As Long

Private Declare Sub keybd_event Lib "user32" _
    (ByVal bVk As Byte, _
    ByVal bScan As Byte, _
    ByVal dwflags As Long, ByVal dwExtraInfo As Long)

Private Declare Function GetKeyboardState Lib "user32" _
    (pbKeyState As Byte) As Long

Private Declare Function SetKeyboardState Lib "user32" _
    (lppbKeyState As Byte) As Long

'Constant declarations
Const VK_NUMLOCK = &H90
Const VK_SCROLL = &H91
Const VK_CAPITAL = &H14
Const KEYEVENTF_EXTENDEDKEY = &H1
Const KEYEVENTF_KEYUP = &H2
Const VER_PLATFORM_WIN32_NT = 2
Const VER_PLATFORM_WIN32_WINDOWS = 1

Property Get Value() As Boolean
'   Get the current state
    Dim Keys(0 To 255) As Byte
    GetKeyboardState Keys(0)
    Value = Keys(VK_NUMLOCK)
End Property

Property Let Value(boolVal As Boolean)
    Dim o As OSVERSIONINFO
    Dim Keys(0 To 255) As Byte
    o.dwOSVersionInfoSize = Len(o)
    GetVersionEx o
    GetKeyboardState Keys(0)
'   Is it already in that state?
    If boolVal = True And Keys(VK_NUMLOCK) = 1 Then Exit Property
    If boolVal = False And Keys(VK_NUMLOCK) = 0 Then Exit Property
'   Toggle it
    If o.dwPlatformId = VER_PLATFORM_WIN32_WINDOWS Then '(Win95)
        'Toggle numlock
       Keys(VK_NUMLOCK) = IIf(Keys(VK_NUMLOCK) = 0, 1, 0)
        SetKeyboardState Keys(0)
    ElseIf o.dwPlatformId = VER_PLATFORM_WIN32_NT Then ' (WinNT)
        'Simulate Key Press
        keybd_event VK_NUMLOCK, &H45, KEYEVENTF_EXTENDEDKEY Or 0, 0
        'Simulate Key Release
        keybd_event VK_NUMLOCK, &H45, KEYEVENTF_EXTENDEDKEY Or _
          KEYEVENTF_KEYUP, 0
    End If
End Property

Sub Toggle()
'   Toggles the state
    Dim o As OSVERSIONINFO
    o.dwOSVersionInfoSize = Len(o)
    GetVersionEx o
    Dim Keys(0 To 255) As Byte
    GetKeyboardState Keys(0)
    If o.dwPlatformId = VER_PLATFORM_WIN32_WINDOWS Then '(Win95)
        'Toggle numlock
        Keys(VK_NUMLOCK) = IIf(Keys(VK_NUMLOCK) = 0, 1, 0)
        SetKeyboardState Keys(0)
    ElseIf o.dwPlatformId = VER_PLATFORM_WIN32_NT Then ' (WinNT)
        'Simulate Key Press
        keybd_event VK_NUMLOCK, &H45, KEYEVENTF_EXTENDEDKEY Or 0, 0
        'Simulate Key Release
        keybd_event VK_NUMLOCK, &H45, KEYEVENTF_EXTENDEDKEY Or _
          KEYEVENTF_KEYUP, 0
    End If
End Sub
